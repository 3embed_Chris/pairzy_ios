//
//  AppConstant.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 03/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class AppConstant: NSObject {
    
    static var deviceId: String {
        
        if let ID: String = UIDevice.current.identifierForVendor?.uuidString {
            return ID
        }
        else {
            return "iPhone_Simulator_ID"
        }
    }
    
    /// This method is used to get current time.
    /// - Returns: Returns Encoded String
    static func getCurrentTimeWithdate() -> String {
        
        let currentDateTime = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone!
        let dateInStringFormated: String = dateFormatter.string(from: currentDateTime)
        return dateInStringFormated
    }
    
}

class alert
{
    func msg(message: String, title: String )
    {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertView.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
}


extension UIViewController
{
    func alertWith(title: String,and message: String, buttonTitle1 :String, buttonTitle2 : String, doneButtonPressed:@escaping (Bool) -> Void, at controller:UIViewController)
    {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertView.addAction(UIAlertAction(title: buttonTitle1,
                                          style: UIAlertActionStyle.default,
                                          handler: {(alert: UIAlertAction!) in doneButtonPressed(true) }))
        
        alertView.addAction(UIAlertAction(title: buttonTitle2,
                                          style: UIAlertActionStyle.destructive,
                                          handler: {(alert: UIAlertAction!) in doneButtonPressed(false) }))
        
        controller.present(alertView, animated: true, completion: nil)
    }
}

//Messages Keys
struct Message {
    
    static let Network_Not_Reachable    = "NETWORK NOT REACHABLE"
    static let UserNameMsg              = "Please enter username."
    static let EmailMsg                 = "Please enter valid email id."
    static let EmailLengthMsg           = "Please enter your email id."
    static let DateOfBirth              = "Please enter Date of birth."
    static let Gender                   = "Please enter your gender."
    static let Height                   = "Please enter your height."
    static let Password                 = "Please enter your password."
    static let ConfirmPassword          = "Please enter your confirm password."
    static let MobileNumber             = "Please enter your mobile number."
    static let passwordNotmatching = "The passwords must match"
    static let ERROR                    = "Error"
    static let TNC                      = "Accept T&c Policy."
    static let PassWdNotMatched         = "Password Not Matched."
    static let AgeMessage               = "This app is for 18+ only."
    static let tokenMissingError        = "Something went wrong please try again login, or contact for support"
    static let selectGenderMsg          = "Select Your Gender"
    static let ErrorUser                = "User data not Found"
    static let ErrornotAuthorised       = "User not Authorised"
    static let ErrorHeaderMissng        = "Header is Missing"
    static let ErrorMobileRegister      = "User Mobile Number already register"
    static let tAndC = "I agree to Terms and conditions "
    static let termsAndCondition = "Terms and conditions"
}

//NSUserDeafults Keys
struct LoginResponse {
    static let errorFlag                 = "errFlag"
    static let data                      = "data"
    static let errorMessage              = "errorMessage"
}

//NSUserDeafults Keys
struct SignupResponse {
    static let errorFlag                  = "errorFlag"
    static let data                       = "data"
    static let errorMessage               = "message"
    
}

//NSUserDeafults Keys
struct Keys {
    static let awsAccessKey = "AKIAIHIC37PKC32GPBAQ"
    static let awsSecretKey = "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"
    static let photoUploadBucket = "datumv3"
    static let videoUploadBucket = "sync1to1"
}

//Common Request Params
struct  REQUEST{
    
    static let kSessionToken                 = "ent_sess_token"
    static let kDeviceId                     = "ent_dev_id"
    static let kFBId                         = "ent_fbid"
    static let kUserFBId                     = "ent_user_fbid"
    static let kMobileOtp                    = "otp"
}

//Identifier For Sigue
struct IDENTIFIER {
    
    static let LOGIN                          = "LoginVC"
    static let SIGNUP                         = "SignupVC"
    static let TermAndConditions              = "TermAndContions"
    static let CountryPicker                  = "CountryPicker"
    static let MobileVC                       = "MobileVC"
    static let GotoResetPasword               = "gotoReset"
    static let ResetToCountrypicker           = "resetToCountrypicker"
    static let ResetTOnewpassowrd             = "resetTOnewpassowrd"
    static let VerificationToPic              = "toCameraViewControllerSegue"
    
}

struct SERVICE {
    
    static let BASE_URL                       = "http://54.202.103.53:5006/User/"
    static let CHAT_URL                       = "http://54.202.103.53:5010/"
    static let TermAndCondition               = "http://35.154.123.162/Terms.html"
}

//Request Params For Login/Signup
struct ServiceInfo {
    
    static let Fname                            = "fName"
    static let Email                            = "email"
    static let DateOfBirth                      = "dob"
    static let Gender                           = "gender"
    static let Password                         = "pwd"
    static let CountCode                        = "cCode"
    static let PushToken                        = "pToken"
    static let DeviceType                       = "device_type"
    static let PrefSex                          = "pref_sex"
    static let MobileNumber                     = "mob"
    static let Height                           = "height"
    static let DeviceMake                       = "dMake"
    static let DeviceModel                      = "dModel"
    static let LocationName                     = "locationName"
    static let Latitude                         = "lat"
    static let Longitude                        = "long"
    static let FirbaseID                        = "fIberUUId"
    static let profileVideo                     = "pVideo"
    static let otherfileVideo                   = "oVideo"
    static let profilePic                       = "pPic"
    static let otherProfilePic                  = "otherPics"
    static let deviceType                       = "dType"
}

// All API Names
struct APINAMES {
    static let LOGIN                            = "Login"
    static let SIGNUP                           = "Register"
    static let MOBILECODE                       = "OTP"
    static let RESENDCODE                       = "ResendOTP"
    static let FORGOTPASSWORD                   = "ForgetPassword"
    static let RESETPASSWORD                    = "Password"    
    
    
}

//LandingPage scroll view Messages
struct ScrollMsg {
    
    static let MsgArray1                        = ["Meet singles near you!", "Unlimited interaction", "Match with members near you."]
    
    static let MsgArray2                        = ["200,000 daters and growing", "with as many as people as you like, for free!", ""]
    //let SCREEN_WIDTH:  Int = Int(UIScreen.main.bounds.size.width)
}

//Font Faimly
struct FontFiamly {
    
    static let gothamBookItalic = "GothamRounded-BookItalic"
    static let gothamMediumItalic = "GothamRounded-MediumItalic"
    static let gothamBoldItalic = "GothamRounded-BoldItalic"
    static let gothamLight = "GothamRounded-Light"
    static let gothamMedium = "GothamRounded-Medium"
    static let gothamBold = "GothamRounded-Bold"
    static let gothamLightItalic = "GothamRounded-LightItalic"
    static let gothamBook = "GothamRounded-Book"

//    static let
}

//Font Gender
struct Gender {
    
    static let GenderArray                      = ["Male", "Female", "Transgender"]
}


//All Response KEYS

struct ResponseKey {
    
    static let errorFlag                    = "errorFlag"
    static let error                        = "error"
    static let errorMessage                 = "errorMessage"
    static let message                      = "message"
    static let data                         = "data"
    
    //register respose keys    
    
    static let token                        = "token"
    static let userID                       = "userId"
    static let mobileNo                     = "mobileNo"
    
    
    
    
}


//All Request Params
struct RequestParams{
    
    static let MobileNo                     = "mobileNo"
    static let VCode                        = "otp"
    
    
    //forgotpassword req params
    static let firstname                   = "firstName"
    static let email                       = "email"
    
    //resetpassword req params
    static let otp                         = "otp"
    static let password                    = "pwd"
    
}

//All NSUserdefaultkey
struct SyUserdefaultKeys {
    
    static let userMobileNo                         = "mobileNum"
    static let userToken                            = "token"
    static let userID                               = "userID"
    static let kNSUSessionTokenKey                  = "TOKEN"
    static let kNSUAppJoinedDateKey                 = "Joined"
    static let kNSUPushNotificationTokenKey         = "DeviceToken"
    static let kNSUUUID                             = "UUID"
    static let KNSProfilePic                        = "profilePic"
    static let KNDeviceName                         = "deviceName"
}


