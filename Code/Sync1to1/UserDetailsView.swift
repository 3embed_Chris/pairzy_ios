//
//  UserDetailsView.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 03/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class UserDetailsView: UIView {
    
    struct Constant {
        static let lowerAge = 18
        static let upperAge = 58
        static let fieldSpacing = 70
        static let heightBigUnit = "ft"
        static let heightSmallUnit = "inches"
        static let dateFormat = "MMMM dd, yyyy"
    }
    
    var ft:String = "1 ft"
    var cn:String = "0 inches"
    
    var activeTextField : UITextField?
    var tableViewOutlet : UITableView!
    
    @IBOutlet weak var firstNameView: UIView!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var dateOfBirthView: UIView!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var dateOfBirthTextField: UITextField!
    @IBOutlet weak var dateOfBirthButtonOutlet: UIButton!
    
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var genderButtonOutlet: UIButton!
    
    @IBOutlet weak var heightView: UIView!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var heighTextField: UITextField!
    @IBOutlet weak var heightButtonOutlet: UIButton!
    
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var confirmPasswordLabel: UILabel!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var countryFlagImageView: UIImageView!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var countryCodeButtonlOutlet: UIButton!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    @IBOutlet weak var emailIDView: UIView!
    @IBOutlet weak var emailIDLabel: UILabel!
    @IBOutlet weak var emailIDTextField: UITextField!
    
    var checked: Bool = false
    var prefLowerAge: Int = 0
    var prefUpperAge: Int = 0
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.firstNameTextField.becomeFirstResponder()
    }
    
    func viewWillAppear()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(UserDetailsView.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UserDetailsView.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func viewWillDisappear()
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func checkMendatoryFields() -> Bool {
        
        if (firstNameTextField.text?.characters.count)! == 0{
            self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.UserNameMsg )
            return false
        }
        else if (dateOfBirthTextField.text?.characters.count)! == 0{
            self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.DateOfBirth )
            return false
        }
        else if (genderTextField.text?.characters.count)! == 0{
            self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.Gender )
            return false
        }
        else if (passwordTextField.text?.characters.count)! == 0{
            self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.Password )
            return false
        }
        else if (confirmPasswordTextField.text?.characters.count)! == 0{
            self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.ConfirmPassword )
            return false
        }
        else if (phoneNumberTextField.text?.characters.count)! == 0{
            self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.MobileNumber )
            return false
        }
            
        else if (self.emailIDTextField.text?.characters.count)! == 0 {
            self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.EmailLengthMsg)
            return false
        }
            
        else if confirmPasswordTextField.text != passwordTextField.text{
            self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.PassWdNotMatched )
            return false
        }
            
        else if !checked {
            self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.TNC)
            return false
        }
            
        else if !(self.passwordTextField.text == self.confirmPasswordTextField.text) {
            
            self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.PassWdNotMatched)
            return false
        }
            
        else if !self.validateEmail(withString: self.emailIDTextField.text!) {
            self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.EmailLengthMsg)
            return false
        }
        else{
            return true
        }
    }
    
    func validateEmail(withString email: String) -> Bool {
        let emailString: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailString)
        return emailTest.evaluate(with: email)
    }
    
    
    /*
     *  This is called to check the user age (18+ or not).
     *
     *  @return             NO if age is less than 18 else YES.
     */
    
    func checkUserAge() -> Bool {
        
        let now = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constant.dateFormat
        let calendar : NSCalendar = NSCalendar.current as NSCalendar
        let dobString: String = dateOfBirthTextField.text!
        if let birthday = dateFormatter.date(from: dobString)
        {
            let ageComponents = calendar.components(.year, from: birthday, to: now as Date, options: [])
            let age = ageComponents.year!
            
            prefLowerAge = age - 5
            prefUpperAge = age + 5
            if prefLowerAge < Constant.lowerAge {
                prefLowerAge = Constant.lowerAge
            }
            if prefUpperAge > Constant.upperAge {
                prefUpperAge = Constant.upperAge
            }
            if age < Constant.lowerAge {
                return false
            }
            else {
                return true
            }
        }
        else
        {
            return false
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            let contentInsets = UIEdgeInsetsMake(0,0, keyboardSize.height, 0)
            self.tableViewOutlet.contentInset = contentInsets
            self.tableViewOutlet.scrollIndicatorInsets = contentInsets
            
            var aRect = self.frame
            aRect.size.height -= keyboardSize.height
            if (activeTextField != nil){
                if !aRect.contains(activeTextField!.frame.origin)
                {
                    self.tableViewOutlet.scrollRectToVisible(activeTextField!.frame, animated: true)
                }
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        let contentInsets = UIEdgeInsets.zero
        self.tableViewOutlet.contentInset = contentInsets
        self.tableViewOutlet.scrollIndicatorInsets = contentInsets
    }
}

extension UserDetailsView: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch  textField
        {
        case firstNameTextField:
            if (firstNameTextField.text?.characters.count)! > 0{
                self.firstNameLabel.isHidden = false
            }
            else{
                self.firstNameLabel.isHidden = true
                self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.UserNameMsg )
            }
            firstNameTextField.resignFirstResponder()
            
            break
            
        case emailIDTextField:
            if (emailIDTextField.text?.characters.count)! > 0{
                if !self.validateEmail(withString: self.emailIDTextField.text!){
                    self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.EmailMsg)
                } else {
                    self.emailIDLabel.isHidden = false
                }
            }
            else{
                self.emailIDLabel.isHidden = true
                self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.EmailLengthMsg)
            }
            emailIDTextField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()
            break
            
        case passwordTextField:
            if (passwordTextField.text?.characters.count)! > 0{
                self.passwordLabel.isHidden = false;
            }
            else{
                self.passwordLabel.isHidden = true
                self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.Password)
            }
            passwordTextField.resignFirstResponder()
            confirmPasswordTextField.becomeFirstResponder()
            break
            
        case confirmPasswordTextField:
            if (confirmPasswordTextField.text?.characters.count)! > 0{
                self.confirmPasswordLabel.isHidden = false;
            }
            else if confirmPasswordTextField.text != passwordTextField.text{
                self.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.PassWdNotMatched)
            }
            else{
                self.confirmPasswordLabel.isHidden = true
                self.showErrorMessage(titleMsg: Message.ERROR, andMessage:Message.ConfirmPassword)
            }
            confirmPasswordTextField.resignFirstResponder()
            phoneNumberTextField.becomeFirstResponder()
            break
            
        case phoneNumberTextField:
            
            phoneNumberTextField.resignFirstResponder()
            break
            
        default: break
            
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        switch  textField {
            
        case firstNameTextField:
            if (newString.characters.count) > 0{
                self.firstNameLabel.isHidden = false;
            }
            else{
                self.firstNameLabel.isHidden = true
            }
            break
            
        case passwordTextField:
            if (newString.characters.count) > 0{
                self.passwordLabel.isHidden = false;
            }
            else{
                self.passwordLabel.isHidden = true
            }
            break
        case confirmPasswordTextField:
            if (newString.characters.count) > 0{
                self.confirmPasswordLabel.isHidden = false;
            }
            else{
                self.confirmPasswordLabel.isHidden = true
            }
            break
        default: break
        }
        return true
    }
}
