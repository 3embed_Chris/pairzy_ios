//
//  MyProfileVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 02/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import AudioToolbox
import VGPlayer
import AVKit
import AccountKit


class MyProfileVC: UIViewController {
    
    var sectionHeaderTitles = [String]()
    var userBasicDetailsTitles = [String]()
    var userBasicDetailsValues = [String]()
    var switchTitles = [String]()
    var userInstaProfile:InstagramProfile? = nil
    var instaId = ""
    var instaToken = ""
    var myPreference:[PreferencesModel] = [PreferencesModel]()
    var userMoments:[MomentsModel]? = nil
    var userProfile:Profile? = nil

    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var prefTableView: UITableView!
    @IBOutlet weak var photoIndicatorCollection: UICollectionView!
    @IBOutlet weak var scrollViewForImages: UIScrollView!
    @IBOutlet weak var galleryBtn: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var header: UIView!
    
   var tableHeaderViewHeight : CGFloat = 400.00

    //var tableHeaderViewHeight : CGFloat = 375.00
    var selectedAdreess:Location? = nil
    var headerMaskLayer : CAShapeLayer!
    let myProfileVM = MyProfileVM()
    let instaVM = InstagramVM()
    var userOthersPic = [String]()
    var locationObj:Location? = nil
    let verifyCodeVM = VerifyCodeVM()
    var localplayer : VGPlayer?
    var type = 10

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = StringConstants.myProfile()
        LandingVM.sharedInstance().accountKitInitialization()

        self.titleLabel.textColor = UIColor.clear
        headerView.backgroundColor = UIColor.clear
        sectionHeaderTitles = ["",StringConstants.aboutYou(),StringConstants.moments(),StringConstants.myVertues(),StringConstants.myVitals(),StringConstants.myVices(),StringConstants.other(),StringConstants.showInstaPhotos()]
        userBasicDetailsTitles = [StringConstants.gender(),StringConstants.age(),StringConstants.phone(),StringConstants.email(),StringConstants.location()]
        userBasicDetailsValues = [StringConstants.NA(),StringConstants.NA(),StringConstants.NA(),StringConstants.NA(),StringConstants.NA()]
        switchTitles = [StringConstants.dontShowAge(),StringConstants.dontShowDistatnce()]
        getUserData()
        didGetInstaResponse()
        myPreference  = Helper.getPreference()
        prefTableView.estimatedRowHeight = 50
        header.frame = CGRect(x: 0, y: 0, width: prefTableView.frame.size.width, height: prefTableView.frame.size.width)
        addNotifications()
        setupHeaderView()
        updateHeaderConstraints()
        updateScrollView()
    }
    
    /// adding observers for events.
    func addNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(refreshProfileImage), name:NSNotification.Name(rawValue:NSNotificationNames.notificationToUpdateProfileImage), object: nil)
    }
    
    @objc func refreshProfileImage(notification: NSNotification) {
        getUserData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        didGetResponse()
        getInstaDetail()
        myProfileVM.getProfile()
        myPreference  = myProfileVM.profileDetails.myPreferences               //Helper.getPreference()
        photoIndicatorCollection.reloadData()
        prefTableView.reloadData()
        backButton.tintColor = UIColor.white
        self.navigationController?.setNavigationBarHidden(true, animated:true)
        scrollToCell()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopVideoPlay()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.header.translatesAutoresizingMaskIntoConstraints = true
    }
    
    func updateScrollView(){
        self.removeSubviews()
        var imageName = ""
        if self.myProfileVM.profileDetails.gender == "Male" {
            imageName = "man_default1"
        }else {
            imageName = "woman_default1"
        }
        
        for i in 0..<self.myProfileVM.profileDetails.allMedia.count {
            let x = self.view.frame.size.width * CGFloat(i)
            if i == 0 && myProfileVM.profileDetails.hasProfileVideo == 1{
                let videoView1 = UIView()
                videoView1.frame = CGRect(x: x, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.width)
                videoView1.contentMode = .scaleAspectFill
                videoView1.clipsToBounds = true
                scrollViewForImages.contentSize.width = self.view.frame.size.width * CGFloat(i + 1)
                if scrollViewForImages.subviews.count != 0 {
                    if let view = scrollViewForImages.subviews[0] as? UIView {
                        showVideoForUrl(videoUrl: self.myProfileVM.profileDetails.allMedia[i],videoView: view)
                    }else {
                        scrollViewForImages.addSubview(videoView1)
                        showVideoForUrl(videoUrl: self.myProfileVM.profileDetails.allMedia[i],videoView: videoView1)
                    }
                } else if scrollViewForImages.subviews.count == 0 {
                    scrollViewForImages.addSubview(videoView1)
                    showVideoForUrl(videoUrl: self.myProfileVM.profileDetails.allMedia[i],videoView: videoView1)
                }
                
            }else {
                let imageView = UIImageView()
                imageView.frame = CGRect(x: x, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.width)
                imageView.contentMode = .scaleAspectFill
                imageView.clipsToBounds = true
                let url = URL(string: self.myProfileVM.profileDetails.allMedia[i])
                imageView.kf.setImage(with:url, placeholder:UIImage.init(named:imageName))
                scrollViewForImages.contentSize.width = self.view.frame.size.width * CGFloat(i + 1)
                imageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(tapped(_ :)))
                imageView.addGestureRecognizer(tap)
                scrollViewForImages.addSubview(imageView)
            }
            
        }
    }
    
    func showVideoForUrl(videoUrl:String,videoView: UIView) {
        if(videoUrl.count > 5) {
            let url = URL(string:videoUrl)
            if url != nil {
                localplayer = VGPlayer(URL: url!)
            }
            
            localplayer?.delegate = self
            localplayer?.displayView.tag = 2018
            videoView.addSubview((localplayer?.displayView)!)
            localplayer?.displayView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.width)
            localplayer?.gravityMode = .resizeAspectFill
            localplayer?.displayView.contentMode = .scaleAspectFill
            localplayer?.displayView.bringSubview(toFront:(localplayer?.displayView.playButtion)!)
            localplayer?.backgroundMode = .proceed
            localplayer?.displayView.backgroundColor = UIColor.black
            localplayer?.displayView.delegate = self
            localplayer?.displayView.snp.makeConstraints { [weak videoView] (make) in
                guard let strongSelf = videoView else { return }
                make.edges.equalTo(strongSelf)
            }
            
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapped(_ :)))
            localplayer?.displayView.addGestureRecognizer(tap)
            localplayer?.displayView.isUserInteractionEnabled = true
        }
    }
    
    
    func getInstaDetail(){
        let userData = Database.shared.fetchResults()
        if !userData.isEmpty
        {
            if let userDetails = userData.first
            {
                if let instaId = userDetails.instaGramProfileId {
                    self.instaId = instaId
                }
                if let instaToken = userDetails.instaGramToken  {
                    self.instaToken = instaToken
                    
                    
                }
                if self.instaId.count != 0 && self.instaToken.count != 0 {
                    self.instaVM.instaProfile.updateTokenId(instaId: self.instaId, instaAuth: self.instaToken)
                    //  self.userInstaProfile
                    self.instaVM.getUserMedia()
                }
            }
            
        }
    }
    
    // method for tableview header
    ///contains image view and name of the profile.
    func setupHeaderView()
    {
        self.header.translatesAutoresizingMaskIntoConstraints = true
        header = prefTableView.tableHeaderView
        prefTableView.tableHeaderView = nil
        prefTableView.addSubview(header)
        
        prefTableView.contentInset = UIEdgeInsets(top: tableHeaderViewHeight, left: 0, bottom: 0, right: 0)
        prefTableView.contentOffset = CGPoint(x: 0, y: -tableHeaderViewHeight)
        
        // cut away the header view
        headerMaskLayer = CAShapeLayer()
        headerMaskLayer.fillColor = UIColor.black.cgColor
        header.layer.mask = headerMaskLayer
        
        let effectiveHeight = prefTableView.frame.size.width
        prefTableView.contentInset = UIEdgeInsets(top: effectiveHeight, left: 0, bottom: 0, right: 0)
        prefTableView.contentOffset = CGPoint(x: 0, y: -effectiveHeight)
        
        updateHeaderView()
    }
    
    
    
    /// method for update header with image.
    func updateHeaderView()
    {
        let effectiveheight = tableHeaderViewHeight
        var headerRect = CGRect(x: 0, y: -effectiveheight, width: prefTableView.bounds.width, height: prefTableView.frame.size.width)
        
        if prefTableView.contentOffset.y < -effectiveheight{
            headerRect.origin.y = prefTableView.contentOffset.y
            headerRect.size.height = -prefTableView.contentOffset.y
        }
        
        header.frame = headerRect
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: headerRect.width, y: 0))
        path.addLine(to: CGPoint(x: headerRect.width, y: headerRect.height))
        path.addLine(to: CGPoint(x: 0, y: headerRect.height - 0.0))
        headerMaskLayer?.path = path.cgPath
        
        scrollViewForImages.layoutIfNeeded()
        scrollViewForImages.updateConstraints()
        updateImagesHeight()
    }
    
    func updateImagesHeight() {
        
        for subView in scrollViewForImages.subviews {
            
            //updating the frame for images.
            if let imageView = subView as? UIImageView {
                var oldFrame = imageView.frame
                oldFrame.size.height = scrollViewForImages.frame.size.height
                imageView.frame = oldFrame
                scrollViewForImages.layoutIfNeeded()
            }
            else {
                
                //updating the frame for video
                let videoSuperView = subView
                if let disPlayView = videoSuperView.viewWithTag(2018) {
                    var oldFrame = videoSuperView.frame
                    oldFrame.size.height = scrollViewForImages.frame.size.height
                    videoSuperView.frame = oldFrame
                    
                    localplayer?.displayView.frame = oldFrame
                    localplayer?.displayView.layoutIfNeeded()
                    scrollViewForImages.layoutIfNeeded()
                }
            }
        }
    }
    
    
    /// removes subview
    func removeSubviews(){
        let subViews = self.scrollViewForImages.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
    }
    
    
    
    func updateHeaderConstraints(){
        
        self.header.translatesAutoresizingMaskIntoConstraints = false
        
        prefTableView.addConstraint(NSLayoutConstraint(item: header, attribute: .top, relatedBy: .equal, toItem: prefTableView, attribute: .top, multiplier: 1, constant: -tableHeaderViewHeight))
        
        prefTableView.addConstraint(NSLayoutConstraint(item: header, attribute: .leading, relatedBy: .equal, toItem: prefTableView, attribute: .leading, multiplier: 1, constant: 0))
        
        prefTableView.addConstraint(NSLayoutConstraint(item: header, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: tableHeaderViewHeight))
        
        prefTableView.addConstraint(NSLayoutConstraint(item: header, attribute: .width, relatedBy: .equal, toItem: prefTableView, attribute: .width, multiplier: 1, constant: 0))
        
        
    }
    
    
    @IBAction func viewMyMomentsAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: SegueIds.myProfileToMomentVC, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIds.myProfileToMomentVC{
            let momentVC = segue.destination as! MomentsViewController
            momentVC.momentMedia = self.userMoments!
            momentVC.isFromMyProfile = true
        }
    }
    
    
    @IBAction func datumPlusAction(_ sender: Any) {
        openInAppPurchaseView()
        
    }
    
    @IBAction func connectToInstaGram(_ sender: Any) {
        let instaLoginVc = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "instaVC") as! InstagramLoginVC
        if self.userInstaProfile != nil {
            
        } else {
            let navigationController = UINavigationController(rootViewController: instaLoginVc)
            self.navigationController?.present(navigationController, animated:true, completion:nil)
        }
    }
    
    
    @IBAction func doneButtonAction(_ sender: Any) {
        
        let userData = Database.shared.fetchResults()
        var params = [String:Any]()
        if let userDetails = userData.first {
            var dontShowAge = 0
            var dontShowDist = 0
            
            //if Helper.isSubscribedForPlans() {
                if let ageSwitchBtn = view.viewWithTag(1000) as? UISwitch {
                    dontShowAge = ageSwitchBtn.isOn.hashValue
                    if ageSwitchBtn.isOn {
                        dontShowAge = 1
                    }else{
                        dontShowAge = 0
                    }
                }
                
                if let distSwitch = view.viewWithTag(1001) as? UISwitch {
                    dontShowDist = distSwitch.isOn.hashValue
                    if distSwitch.isOn {
                        dontShowDist = 1
                    }else{
                        dontShowDist = 0
                    }
                }
          //  }
            
            let locDetails:[String:Any?] = Helper.getUserLocation()
            var latitude = 0.0
            var longitude = 0.0
            let hasValue:String = locDetails["hasLocation"] as! String
            if Helper.getIsFeatureExist(){
                let featureLoc:Location =  Helper.getFeatureLocation()
                latitude = featureLoc.latitude
                longitude = featureLoc.longitude
                
            }else {
                if(hasValue == "1") {
                    if let lat = locDetails["lat"] as? Double {
                        latitude = lat
                    }
                    if let long = locDetails["long"] as? Double {
                        longitude = long
                    }
                }
            }
            
            params = [ServiceInfo.Gender:  userDetails.gender!,
                      ServiceInfo.DateOfbirth: userDetails.dateOfBirthTimeStamp! ,
                      ServiceInfo.phoneNumber: userDetails.mobileNumber!,
                      ServiceInfo.email: userDetails.emailId!,
                      ServiceInfo.emailId: userDetails.profilePictureURL!,
                      ServiceInfo.aboutYou: userDetails.aboutYou!,
                      ServiceInfo.latitude: latitude,
                      ServiceInfo.longitude: longitude,
                      ServiceInfo.dontShowMyAge: dontShowAge,
                      ServiceInfo.dontShowMyDist: dontShowDist,
                      ServiceInfo.type: type] as [String : Any]
        }
        myProfileVM.updateProfile(params: params)
        
    }
    
    @IBAction func playBtnAction(_ sender: Any) {
        
        if(self.localplayer?.state == .playFinished) {
            self.localplayer?.displayView.replayButton.sendActions(for: .touchUpInside)
        } else {
            self.localplayer?.displayView.playButtion.sendActions(for: .touchUpInside)
        }
        
        
    }
    
    
    @IBAction func switchAction(_ sender: UISwitch) {
        if sender.tag == 1000 {
            if sender.isOn == true {
                //openInAppPurchaseView()
                sender.isOn = false
            }
            
        }else {
            if sender.isOn == true {
                //openInAppPurchaseView()
                sender.isOn = false
            }
        }
        if  Helper.isSubscribedForPlans() {
            if sender.tag == 1000 {

            }else {

            }

        }else {

            if sender.tag == 1000 {
                if sender.isOn == true {
                   // openInAppPurchaseView()
                    sender.isOn = false
                }

            }else {
                if sender.isOn == true {
                    //openInAppPurchaseView()
                    sender.isOn = false
                }
            }
        }
    }
    
    func stopVideoPlay() {
        if((localplayer) != nil) {
            localplayer?.pause()
        }
    }
    
    func openInAppPurchaseView() {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = InAppPuchasePopupView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.loadInAppPurchaseView()
        window.addSubview(videoView)
    }
    
    
    /// fetching data from core data.
    func getUserData() {
        let userData = Database.shared.fetchResults()
        
        if !userData.isEmpty
        {
            if let userDetails = userData.first
            {
                var userName = ""
                if let name = userDetails.first_name {
                    sectionHeaderTitles[0] = name
                    userName = name
                }
                
                if userDetails.gender != "" {
                    if let gender = userDetails.gender {
                        userBasicDetailsValues[0] = gender
                    }
                }
                
                
                if let dob = userDetails.dateOfBirthTimeStamp as String? {
                    if(dob != "") {
                        userBasicDetailsValues[1] = String(Helper.dateFromMilliseconds(timestamp:String(dob)).age)
                        
                        sectionHeaderTitles[0] = userName+", " + userBasicDetailsValues[1]
                        
                    }
                }
                
                
                if let phoneNumber = userDetails.mobileNumber {
                    userBasicDetailsValues[2] = phoneNumber
                }
                
                if let emailId = userDetails.emailId {
                    userBasicDetailsValues[3] = emailId
                }
                if let otherPics = userDetails.otherPics as? [String] {
                    self.userOthersPic = otherPics
                }
                if let urlString = userDetails.profilePictureURL {
                    let url = URL(string: urlString)
                    myProfileVM.profileDetails.profilePicture = urlString
                    // self.scrollViewForImages.subviews.removeAll()
                    self.updateScrollView()
                }
 
            }
        }
        
        if Helper.getIsFeatureExist() {
            let loc = Helper.getFeatureLocation()
            
            let address = loc.city + "," + loc.state + "," + loc.country
            userBasicDetailsValues[4] = address
            
            if address.count < 10 {
                userBasicDetailsValues[4] = loc.locationDescription
                if loc.locationDescription.count == 0 {
                    userBasicDetailsValues[4] = loc.secondaryText
                }
            }
            self.selectedAdreess = loc
        }else {
            
            
            let location = Utility.getCurrentAddress()
            let featureLoc:Location =  Helper.getFeatureLocation()
            userBasicDetailsValues[4]  = location.city + "," + location.state + "," + location.country
            self.selectedAdreess = location
        }
        
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    /// edit profile button action.
    ///
    /// - Parameter sender: edit profile.
    @IBAction func editButtonAction(_ sender: Any) {
       // self.performSegue(withIdentifier: SegueIds.myProfileToEditProfile, sender: nil)
        //creates object of GenderPickerViewController and push that ViewController
            let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:"editProfileVc") as! EdiitProfileViewController
            self.navigationController?.pushViewController(userBasics, animated: true)
       
    }
    
    
}
extension MyProfileVC: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5+myPreference.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return userBasicDetailsTitles.count+1
        case 1:                //abouty you
            return 2
        case 2:
            if self.userMoments != nil{
                if self.userMoments!.count != 0 {
                    return 2
                }
            }
            return 0
        case (5+myPreference.count)-1:
            return 3                     // share
        case (5+myPreference.count)-2:      // Switch cell
            return switchTitles.count+1
            
        default:
            // case 2,3,4:
            if myPreference.count != 0 {
                return myPreference[section-3].arrayOfSubPreference.count+1
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            return
        }
        switch indexPath.section {
        case 0:
            updateBasicDetails(selectedRow: indexPath.row)
            break
        case 1:
            break
        case 2:
            break
        case (5+myPreference.count)-1:
            break
        case (5+myPreference.count)-2:
            break
        default:
            print("")
            //case 2,3,4:
            let singlePrefDetails = myPreference[indexPath.section-3].arrayOfSubPreference[indexPath.row-1]
            let prefTypeId = singlePrefDetails.prefTypeId
            switch prefTypeId {
            case 1,2,10:
                self.preferenceTableVC(selectedRow:indexPath.row-1, selectedPref:indexPath.section-3)
            case 5:
                self.pushTotextFieldTypeVC(selectedRow:indexPath.row-1, selectedPref:indexPath.section-3)
                
            default:
                print("")
            }
            print("no need to implement")
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {

        switch section {
        case 1:
            return 0
        case 2:
            return 0
        case (5+myPreference.count)-1:
            return 100
        default:
            return 30
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sectionSeperator")
        if(tableView.numberOfSections-1 == section) {
            let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 100))
            returnedView.backgroundColor = .clear
            return returnedView
        }
        return cell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "myProfHeader") as! SectionHeaderCell
                cell.updateHeader(title: sectionHeaderTitles[indexPath.section])
                cell.viewAllButtonOutlet.isHidden = true
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "myProfileOtherInfo", for: indexPath) as! ProfileOtherInfoCell
            cell.optionLabel.text =  userBasicDetailsTitles[indexPath.row-1]
            cell.selectedLabel.text = userBasicDetailsValues[indexPath.row-1]
            if indexPath.row == 5 {
                if selectedAdreess != nil {
                    cell.updateLocation(location: selectedAdreess!)
                }
            }
            return cell
        case 2:
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "myProfHeader") as! SectionHeaderCell
                cell.headingLabel.text = "Moments"
                cell.headingLabel.textColor = Colors.PrimaryText
                cell.headingLabel.font = UIFont (name: CircularAir.Bold, size: 20)
               // cell.separator.isHidden = true
                cell.viewAllButtonOutlet.isHidden = false
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MomentsViewCell", for: indexPath) as! MomentsViewCell
                cell.profileMoments = self.myProfileVM.profileDetails.moments
                cell.isFromMyProfile = true
                cell.update(self)
                if (self.userMoments?.count)! >= 4 {
                    cell.collectionViewHeight.constant = (cell.momentCollectionView.frame.size.width/3) * 2
                }else{
                    cell.collectionViewHeight.constant = cell.momentCollectionView.frame.size.width/3
                    
                }
                return cell
            }
        case 1:
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "myProfHeader") as! SectionHeaderCell
                cell.updateHeader(title: sectionHeaderTitles[indexPath.section])
                cell.viewAllButtonOutlet.isHidden = true
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AboutTableCell", for: indexPath) as! AboutTableCell
            let userData = Database.shared.fetchResults()
            cell.delegate = self
            var aboutYou = ""
            if let about = userData[0].aboutYou {
                aboutYou = about
            }
            cell.updateCell(about: aboutYou )
           return cell
        case (5+myPreference.count)-2: //case 6
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "myProfHeader") as! SectionHeaderCell
                cell.updateHeader(title: sectionHeaderTitles[indexPath.section])
                cell.viewAllButtonOutlet.isHidden = true
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileSwitchCell", for: indexPath) as! MyProfileSwitchCell
            cell.proileSwitch.tag = indexPath.row-1 + 1000
            let userData = Database.shared.fetchResults()
            var statusSelected = false
            if indexPath.row == 1{
                statusSelected = userData[0].showMyAge
            }
            else {
                statusSelected = userData[0].showMyDist
            }
            cell.updateCell(title: switchTitles[indexPath.row-1],status: statusSelected)
            return cell
        case (5+myPreference.count)-1: //case 7
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "myProfHeader") as! SectionHeaderCell
                cell.updateHeader(title: sectionHeaderTitles[indexPath.section])
                cell.viewAllButtonOutlet.isHidden = true
               // cell.separator.isHidden = true
                return cell
            }
            
            if indexPath.row == 1 {
                
                if self.userInstaProfile != nil{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "myProfileCollection", for: indexPath) as! ProfileCollectionCell
                    cell.profileDelegate = self
                    cell.updateInstaMedia(userInstaMedia: self.userInstaProfile!)
                    return cell
                }
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileCell", for: indexPath) as! MyProfileCell
                cell.instagramLabel.text = StringConstants.connectInstagram()
                cell.instagramLabel.textColor = Colors.AppBaseColor
                return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "datumPlusCell", for: indexPath) as! MyProfileCell
                cell.datumPlusBtn.titleLabel!.numberOfLines = 2
                cell.datumPlusBtn.setTitle(StringConstants.datumPlus(), for: .normal)
                cell.controlProfileLabel.text = StringConstants.controlYourProf()
                return cell
            }
            
        default:
            //case 3,4,5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "myProfileOtherInfo", for: indexPath) as! ProfileOtherInfoCell
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "myProfHeader") as! SectionHeaderCell
                cell.updateHeader(title: sectionHeaderTitles[indexPath.section])
                cell.viewAllButtonOutlet.isHidden = true
                return cell
            }
            
            let selectedPrefTitle = myPreference[indexPath.section-3].arrayOfSubPreference[indexPath.row-1]
            var valueForPref = "NA"
            var setAttribute = false
            let valuesCount = myPreference[indexPath.section-3].arrayOfSubPreference[indexPath.row-1].selectedValues.count
            if valuesCount > 0 {
                valueForPref = myPreference[indexPath.section-3].arrayOfSubPreference[indexPath.row-1].selectedValues[0]
            }
            if valuesCount > 1 && myPreference[indexPath.section-3].arrayOfSubPreference[indexPath.row-1].prefTypeId != 5 {
                valueForPref = valueForPref+"+\(valuesCount-1)"
                setAttribute = true
            }
            cell.updateValues(option:selectedPrefTitle.prefSubTitle,selectedOp:valueForPref)
            if setAttribute {
                cell.selectedLabel.attributedText = Helper.getAttributedText(text: valueForPref, subText: "+\(valuesCount-1)", color: Colors.blueColor)
            }
            return cell
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2{
            if indexPath.row == 0{
                return 60
            }else{
                if self.userMoments != nil{
                    if ((self.userMoments?.count)!) >= 4  {
                        return ((self.view.frame.size.width/3)*2)
                    }else
                    {
                        return ((self.view.frame.size.width/3))
                    }
                }else{
                    return UITableViewAutomaticDimension
                }
            }
            return UITableViewAutomaticDimension
                
    }
    if(indexPath.section == 1) {
            if indexPath .row == 0 {
                return UITableViewAutomaticDimension
            }
            return 80
        } else {
            
            if indexPath.section == 7{
                if indexPath.row == 1 {
                    if self.userInstaProfile != nil{
                        if (userInstaProfile?.userMedia.count)! >= 6 {
                            return ((120)*2 + 60)
                        } else if(userInstaProfile?.userMedia.count)! == 0{
                            return UITableViewAutomaticDimension
                        }
                        else
                        {
                            return ((120) + 60)
                        }
                    }else {
                        return UITableViewAutomaticDimension
                    }
                } else {
                    return UITableViewAutomaticDimension
                }
            }
            
        }
        return UITableViewAutomaticDimension
    }
    //creates object of GenderPickerViewController and push that ViewController
    func pushVCToGenderVc() {
        let userBasics =  Helper.getSBWithName(name: "Main").instantiateViewController(withIdentifier:StoryBoardIdentifier.genderPickerSI) as! GenderPickerViewController
        userBasics.forEditProfile = true
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
    func openPoneNumberVc() {
        let phoneNumber = Helper.getSBWithName(name: "Main").instantiateViewController(withIdentifier: StoryBoardIdentifier.phoneNumberSI) as! PhoneNumberViewController
        phoneNumber.phoneNumVM.forEditProfile = true
        let navigationController = UINavigationController(rootViewController: phoneNumber)
        navigationController.isNavigationBarHidden = true
        self.navigationController?.present(navigationController, animated: true, completion: nil)
        
    }
    
    //  LocationVC
    
    func openLocationVC() {
        let changeLocationVC = Helper.getSBWithName(name: "DatumTabBarControllers").instantiateViewController(withIdentifier: "Location") as! PassportLocationVC
        changeLocationVC.delegate = self
        
        self.navigationController?.pushViewController(changeLocationVC, animated: true)
    }
    
    
    func updateBasicDetails(selectedRow:Int){
        //0 is header
        if(selectedRow == 0 || selectedRow == 3 || selectedRow == 5 || selectedRow == 2) {
            if(selectedRow == 1) {
                //   pushVCToGenderVc()
            }
            
            if(selectedRow == 3) {
                //openPoneNumberVc()
                loginWithPhone()
            }
            
            if selectedRow == 5 {
                openLocationVC()
            }
            return
        }
        
        var selectedType = detailsType.EmailAddress
        switch selectedRow {
        case 0: break
        //selectedType = detailsType.
        case 1:
            return
        //    selectedType = detailsType.dateOfBirth
        case 3:
            selectedType = detailsType.EmailAddress
        default:
            break
        }
        
        self.view.endEditing(true)
        let userBasics = Helper.getSBWithName(name: "Main").instantiateViewController(withIdentifier:StoryBoardIdentifier.userBasicSI) as! UserBaiscDetailsVC
        let userData = Database.shared.fetchResults()
        userBasics.basicDetails = selectedType
        userBasics.isVcForEdit = true
        userBasics.forEditProfile = true
        
        userBasics.email = userData[0].emailId!
        userBasics.valueForEditProfile = userData[0].dateOfBirthTimeStamp!             //userBasicDetailsValues[selectedRow]
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
    
    /// creates object of gende
    func preferenceTableVC(selectedRow:Int,selectedPref:Int) {
        let userBasics = Helper.getSBWithName(name: "Main").instantiateViewController(withIdentifier:StoryBoardIdentifier.PreferenceTable) as! PreferencesTableVC
        userBasics.isForEditProfile = true
        userBasics.currentPrefCategoryIndex = selectedPref
        userBasics.currentPrefSubCategoryIndex = selectedRow
        userBasics.profileDetile = myPreference
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
    func pushTotextFieldTypeVC(selectedRow:Int,selectedPref:Int){
        let userBasics = Helper.getSBWithName(name: "Main").instantiateViewController(withIdentifier:StoryBoardIdentifier.TextFieldTypeVC) as! PreferencesTFVC
        userBasics.screenIndex = 0
        userBasics.isForEditProfile = true
        userBasics.isVcForSignUp = false
        userBasics.currentPrefCategoryIndex = selectedPref
        userBasics.currentPrefSubCategoryIndex = selectedRow
        userBasics.profileDetile = myPreference
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
    func pushToNextVC(){
        
    }
    
}
extension MyProfileVC:UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
         if scrollView == scrollViewForImages {
            if myProfileVM.profileDetails.hasProfileVideo == 1 {
                if scrollView.contentOffset.x <= 180{
                    self.playBtn.isHidden = false
                }else {
                    self.playBtn.isHidden = true
                }
            }else {
                self.playBtn.isHidden = true
                stopVideoPlay()
            }
            
        }
        let movedOffset: CGFloat = scrollView.contentOffset.y
        if scrollView == prefTableView {
            
            self.view.endEditing(true)
            let pageWidth: CGFloat = 400         //171 - 64
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            if movedOffset >= -46 {
                Helper.setShadow(sender: headerView)
                self.titleLabel.textColor = Colors.PrimaryText    //.withAlphaComponent(ratio)
                headerView.backgroundColor = Colors.SecondBaseColor//.withAlphaComponent(ratio)
                if  ratio <= 1 && ratio >= 0{
                    self.titleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle)*ratio)
                }
                if movedOffset > pageWidth {
                    self.titleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle))
                }
                // galleryBtn.isSelected = true
                backButton.tintColor = Colors.AppBaseColor
                galleryBtn.tintColor = Colors.AppBaseColor
                
            }else {
                self.titleLabel.textColor = UIColor.clear
                headerView.backgroundColor = UIColor.clear
                Helper.removeShadow(sender: headerView)
                // galleryBtn.isSelected = false
                galleryBtn.tintColor = UIColor.white
                backButton.tintColor  = UIColor.white
            }
            updateHeaderView()
        }
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.myProfileVM.profileDetails.currentPhotoIndex  = Int(pageNumber)
        photoIndicatorCollection.reloadData()
        if scrollView == scrollViewForImages {
            if myProfileVM.profileDetails.hasProfileVideo == 1 {
                if scrollView.contentOffset.x == 0 {
                    self.playBtn.isHidden = false
                }else {
                    self.playBtn.isHidden = true
                }
            }else {
                self.playBtn.isHidden = true
                stopVideoPlay()
            }
        }
        
    }
    
}

extension MyProfileVC : PassportLocationVCDelegate {
    
    func didSelectAddress(address: Location) {
        selectedAdreess = address
        prefTableView.reloadData()
        Helper.saveFeaturLocation(data: address)
        let dbObj = Database.shared
        dbObj.userDataObj.latitude =  String(address.latitude)
        dbObj.userDataObj.longitude = String(address.longitude)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
    }
    
    func didGetResponse(){
        self.myProfileVM.MyProfileVM_resp.subscribe(onNext: {   success in
            if success == MyProfileVM.ResponseType.updateProfile {
                let dbObj = Database.shared
                self.userMoments = self.myProfileVM.profileDetails.moments
                self.userProfile = self.myProfileVM.profileDetails
                if let ageSwitchBtn = self.view.viewWithTag(1000) as? UISwitch {
                    dbObj.userDataObj.showMyAge = ageSwitchBtn.isOn
                }
                
                if let distSwitch = self.view.viewWithTag(1001) as? UISwitch {
                    dbObj.userDataObj.showMyDist = distSwitch.isOn
                }
                
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                self.dismiss(animated: true, completion: nil)
            }else {
                self.myPreference  = self.myProfileVM.profileDetails.myPreferences
                self.photoIndicatorCollection.reloadData()
                self.userMoments = self.myProfileVM.profileDetails.moments
                let dbObj = Database.shared
                dbObj.userDataObj.mobileNumber = self.myProfileVM.profileDetails.mobileNumber
                self.userBasicDetailsValues[2] = self.myProfileVM.profileDetails.mobileNumber
                self.userBasicDetailsValues[1] = self.myProfileVM.profileDetails.userAge

                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                //self.scrollViewForImages.subviews.removeAll()
                self.updateScrollView()
                self.prefTableView.reloadData()
                
            }
            
            
        }).disposed(by: self.myProfileVM.disposeBag)
        
    }
    
    func createScrollViewWithImages() {
        //only first time and after getting response.
    }
    
    func didGetInstaResponse(){
        
        self.instaVM.instagramVM_resp.subscribe(onNext:{ success in
            
            if success.0 == InstagramVM.ResponseType.getUserMedia {
                self.userInstaProfile = success.1
                self.instaToken = (self.userInstaProfile?.instaAuthentication)!
                self.instaId = (self.userInstaProfile?.instaId)!
                
                self.prefTableView.reloadData()
            }
            
        }).disposed(by: self.instaVM.disposeBag)
    }
    
    
    @objc func tapped(_ sender: UIGestureRecognizer){
        print("tapped")
        let locPoint = sender.location(in:sender.view)
        
        
        if(locPoint.x > self.view.frame.size.width/2) {
            // show next image
            if(self.myProfileVM.profileDetails.allMedia.count > 1) {
                if(self.myProfileVM.profileDetails.currentPhotoIndex+1 != self.myProfileVM.profileDetails.allMedia.count) {
                    self.myProfileVM.profileDetails.currentPhotoIndex = self.myProfileVM.profileDetails.currentPhotoIndex+1
                    self.scrollToCell()
                    self.myProfileVM.firstImage = self.myProfileVM.firstImage + 1
                    photoIndicatorCollection.reloadData()
                } else {
                    scrollViewForImages.shakeView()
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                }
            } else {
                scrollViewForImages.shakeView()
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            }
        } else {
            // show previous image
            if(self.myProfileVM.profileDetails.allMedia.count != 0) {
                if(self.myProfileVM.profileDetails.currentPhotoIndex != 0) {
                    self.myProfileVM.profileDetails.currentPhotoIndex = self.myProfileVM.profileDetails.currentPhotoIndex-1
                    
                    self.scrollToCell()
                    self.myProfileVM.firstImage = self.myProfileVM.firstImage - 1
                    photoIndicatorCollection.reloadData()
                } else {
                    scrollViewForImages.shakeView()
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                }
            } else {
                scrollViewForImages.shakeView()
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            }
        }
    }
    
    func scrollToCell(){
        
        let window = UIApplication.shared.keyWindow!
        let x = window.frame.size.width * CGFloat(myProfileVM.profileDetails.currentPhotoIndex)
        scrollViewForImages.contentOffset.x = x
        if myProfileVM.profileDetails.hasProfileVideo == 1 {
            stopVideoPlay()
            if x == 0 {
                self.playBtn.isHidden = false
            }else {
                self.playBtn.isHidden = true
            }
            
        }else {
            self.playBtn.isHidden = true
        }
    }
    
}



// MARK: - UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension MyProfileVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.myProfileVM.profileDetails.allMedia.count == 0 {
            return 1
        }
        return self.myProfileVM.profileDetails.allMedia.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let Cell:PhotoIndicatorCell = collectionView.dequeueReusableCell(withReuseIdentifier: "myProfileIndicator", for: indexPath) as! PhotoIndicatorCell
        Cell.indicatorView.tag = indexPath.row
        let viewIn = UIView()
        viewIn.tag = self.myProfileVM.firstImage
        
        if(indexPath.row <= self.myProfileVM.profileDetails.currentPhotoIndex) {
            Cell.indicatorView.backgroundColor = UIColor.white
        } else {
            Cell.indicatorView.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        }
        
        return Cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize.zero
        
        
        let cellSize = Int(collectionView.frame.size.width) - (((self.myProfileVM.profileDetails.allMedia.count)-1)*5)
        var cellWidth = cellSize
        if self.myProfileVM.profileDetails.allMedia.count != 0 {
            cellWidth = (cellSize)/(self.myProfileVM.profileDetails.allMedia.count)
        }
        let cellHight = collectionView.frame.size.height
        size.height =  cellHight
        size.width  =  CGFloat(cellWidth)
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
}

extension MyProfileVC:AboutTableCellDelegate {
    func isTextChange() {
        type = 4
    }
    
  
}

extension MyProfileVC:profileCellDelegate {
    func tappedOnInstaMedia(atIndex: Int, mediaArray: [String]) {
        let imageVc:ImageVideoPreViewController = self.storyboard?.instantiateViewController(withIdentifier: "imageVideoPreViewVc") as! ImageVideoPreViewController
        imageVc.userPhotoArray = mediaArray
        imageVc.isPreviewForImage = true
        imageVc.selectedIndex = atIndex
        self.present(imageVc, animated: true, completion:nil)
    }
}


extension MyProfileVC: VGPlayerDelegate {
    func vgPlayer(_ player: VGPlayer, playerFailed error: VGPlayerError) {
        print(error)
    }
    
    func vgPlayer(_ player: VGPlayer, stateDidChange state: VGPlayerState) {
        print("player State ",state)
        if (player.state == .playing) {
            self.playBtn.isSelected = true
            self.playBtn.setImage(UIImage(), for: .normal)
        } else {
            self.playBtn.isSelected = false
            self.playBtn.setImage(#imageLiteral(resourceName: "PlayButtonImage"), for: .normal)
        }
    }
    
    func vgPlayer(_ player: VGPlayer, bufferStateDidChange state: VGPlayerBufferstate) {
        print("buffer State", state)
    }
    
}

extension MyProfileVC: VGPlayerViewDelegate {
    func vgPlayerView(_ playerView: VGPlayerView, willFullscreen fullscreen: Bool) {
        print(fullscreen)
    }
    
    func vgPlayerView(didTappedClose playerView: VGPlayerView) {
        if playerView.isFullScreen {
            playerView.exitFullscreen()
        } else {
            //self.navigationController?.popViewController(animated: true)
        }
    }
    
    func vgPlayerView(didDisplayControl playerView: VGPlayerView) {
        //        UIApplication.shared.setStatusBarHidden(!playerView.isDisplayControl, with: .fade)
    }
}
extension MyProfileVC: AKFViewControllerDelegate {
    
    //Login with phone number button action
    func loginWithPhone(){
        let id = UUID.init().uuidString
        let userData = Database.shared.fetchResults()
        var ph = ""
        var cc = ""
        if let userDetails = userData.first {
            
            cc = userDetails.countryCode!
            ph = userDetails.mobileNumber!.replacingOccurrences(of: cc, with: "")
            
            
            
            
        }
        
        let phoneNumber = PhoneNumber.init(countryCode: cc, phoneNumber: ph)
        let vc = (LandingVM.sharedInstance()._accountKit?.viewControllerForPhoneLogin(with: phoneNumber, state: id))!
        vc.isSendToFacebookEnabled = true
        self.prepareLoginViewController(loginViewController: vc)
        self.present(vc as UIViewController, animated: true, completion: nil)
    }
    
    
    //MARK: - Helper Methods
    
    func prepareLoginViewController(loginViewController: AKFViewController) {
        loginViewController.delegate = self
        
        //Costumize the theme
        let theme:Theme = Theme.default()
        theme.headerBackgroundColor = Colors.AppBaseColor
        theme.headerTextColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        theme.iconColor = UIColor(red: 0.325, green: 0.557, blue: 1, alpha: 1)
        theme.inputTextColor = UIColor(white: 0.4, alpha: 1.0)
        theme.statusBarStyle = .default
        theme.textColor = UIColor(white: 0.3, alpha: 1.0)
        theme.titleColor = UIColor(red: 0.247, green: 0.247, blue: 0.247, alpha: 1)
        theme.buttonBackgroundColor = Colors.AppBaseColor
        theme.iconColor = Colors.AppBaseColor
        loginViewController.setTheme(theme)
    }
    
    
    
    func viewController(viewController: UIViewController!, didCompleteLoginWithAccessToken accessToken: AccessToken!, state: String!) {
        print("did complete login with access token \(accessToken.tokenString) state \(state)")
    }
    
    // handle callback on successful login to show authorization code
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
        print("didCompleteLoginWithAuthorizationCode")
    }
    
    func viewControllerDidCancel(_ viewController: (UIViewController & AKFViewController)!) {
        // ... handle user cancellation of the login process ...
        print("viewControllerDidCancel")
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didFailWithError error: Error!) {
        // ... implement appropriate error handling ...
        print("\(viewController) did fail with error: \(error.localizedDescription)")
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWith accessToken: AccessToken!, state: String!) {
        print("didCompleteLoginWith")
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        verifyPhoneNumber()
    }
    
    
    
    func verifyPhoneNumber(){
        
        LandingVM.sharedInstance()._accountKit.requestAccount { (account, error) in
            
            if error == nil {
                // let accountId = account?.accountID
                let locAuth = LocalAuthentication(data: [:])
                
                if let currentEmail = account?.emailAddress{
                    locAuth.email = currentEmail
                }
                
                if let phone = account?.phoneNumber {
                    locAuth.phoneNumber = phone.phoneNumber
                }
                if let contryCode = account?.phoneNumber?.countryCode {
                    locAuth.countryCode = "+" + contryCode
                }
                let selectedValue = locAuth.countryCode + locAuth.phoneNumber
                
                let dbObj = Database.shared
                dbObj.userDataObj.mobileNumber = selectedValue
                self.userBasicDetailsValues[2] = selectedValue
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                self.isPhoneNumberRegistered(locAuths: locAuth)
            }
        }
    }
    
    
    func isPhoneNumberRegistered(locAuths: LocalAuthentication){
        let phoneVM = PhoneNumViewModel()
        phoneVM.locAuth = locAuths
        phoneVM.forEditProfile = true
        phoneVM.requestForPhoneNumberCheck()
        phoneVM.phNum_response.subscribe(onNext: { success in
            self.updatePhoneNumber(data: locAuths)
        }).disposed(by: phoneVM.disposeBag)
    }
    
    func updatePhoneNumber(data: LocalAuthentication){
        self.verifyCodeVM.locAuth = data
        self.verifyCodeVM.updatePhoneNumber(otp:"111111")
        self.verifyCodeVM.verifyCodeVM_resp.subscribe(onNext: { success in
            if success == VerifyCodeVM.UserType.respFalse {
                Helper.showAlertWithMessage(withTitle: StringConstants.Error(), message: self.verifyCodeVM.message, onViewController: self)
            }else{
                self.prefTableView.reloadData()
            }
        }).disposed(by: self.verifyCodeVM.disposeBag)
    }
    
}
