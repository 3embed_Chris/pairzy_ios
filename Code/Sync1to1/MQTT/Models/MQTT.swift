//
//  MQTTModule.swift
//  MQTT Chat Module
//
//  Created by Dinesh Guptha Bavirisetti on 11/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient
import Foundation

/// This class is a model for interacting with the MQTTClient library.
class MQTT : NSObject {
        
    /// Shared instance object for gettting the singleton object
    static let sharedInstance = MQTT()
    
    /**
     tells about current connection, true if it is conected or false if it is disconnected.
     
     
     let sharedInstance = MQTT.sharedInstance.manager.state
     
     
     */
    var isConnected = false
    
    ///current session object will going to store in this.
    var mqttSession: MQTTSession!
    
    ///Used for running the task in the background.
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    /// MQTT delegate object, its going to store the objects of the delegate receiver class.
    let mqttMessageDelegate = MQTTDelegate()
    
    
    /// Used for creating the initial connection.
    func createConnection() {
        
        /// Observer for app coming in foreground.
        NotificationCenter.default.addObserver(self, selector: #selector(MQTT.reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        registerBackgroundTask()
        //userId
        
        guard let userID = Helper.getMQTTID() else { return }
        if userID != "mqttId" {
            if userID.count>1 {
                ///creating connection with the proper client ID.
                self.connect(withClientId: userID)
            }
        }
    }
    
    func disconnectMQTTConnection() {
        if self.isConnected {
            self.isConnected = false
            mqttSession.disconnect()
        }
    }
    
    /// Used for subscribing the channel
    ///
    /// - Parameter channelName: current channel which you want to subscribe.
    func subscribeTopic(withTopicName topicName : String,withDelivering delivering:MQTTQosLevel ) {
        let topicToSubscribe = topicName
        self.subscribe(topic: "\(topicToSubscribe)", withDelivering: delivering)
    }
    
    /// Used for Unsubscribing the channel
    ///
    /// - Parameter channelName: current channel which you want to Unsubscribing.
    func unsubscribeTopic(topic : String) {
        
        mqttSession.unsubscribeTopic(topic) { (error) in
            
        }
    }
    
    /// Used for subscribing the channel
    ///
    /// - Parameters:
    ///   - topic: name of the current topic (It should contain the name of the topic with saperators)
    ///
    /// eg- Message/UserName
    ///   - Delivering: Type of QOS // can be 0,1 or 2.
    fileprivate func subscribe(topic : String, withDelivering Delivering : MQTTQosLevel) {
        
        mqttSession.subscribe(toTopic: topic, at: Delivering) { (error, subscriptionArray) in
           
        }
    }
    
    
    /// Used for reinstate the background task
    @objc func reinstateBackgroundTask() {
        if (backgroundTask == UIBackgroundTaskInvalid) {
            registerBackgroundTask()
        }
    }
    
    ///Here I am registering for the background task.
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    ///Before background task ending this method is going to be called.
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
    
    /// Used for pubishing the data in between channels.
    ///
    /// - Parameters:=
    ///   - jsonData: Data in JSON format.
    ///   - channel: current channel name to publish the data to.
    ///   - messageID: current message ID (this ID should be unique)
    ///   - Delivering: Type of QOS // can be 0,1 or 2.
    ///   - retain: true if you wanted to retain the messages or False if you don't
    ///   - completion: This will going to return MQTTSessionCompletionBlock.
    
    func publishData(wthData jsonData: Data, onTopic topic : String, retain : Bool, withDelivering delivering : MQTTQosLevel) {
        if mqttSession != nil{
            mqttSession.publishData(jsonData, onTopic: topic, retain:retain , qos: delivering) { (error) in
                if let error = error {
                   print("error\(error)")
                }
            }
        }
    }
    
    /// Used for connecting with the server.
    ///
    /// - Parameter clientId: current Client ID.
    func connect(withClientId clientId :String) {
        
        let host = SERVICE.MQTT_URL
        let port: UInt32 = SERVICE.MQTT_PORT
        
        guard let newSession = MQTTSession.init(clientId: clientId) else {
            fatalError("Could not create MQTTSession")
        }
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject:["_id":clientId,"status":0]  , options: .prettyPrinted)
            
            mqttSession = newSession
            newSession.delegate = mqttMessageDelegate as MQTTSessionDelegate
            newSession.keepAliveInterval = 45
            newSession.clientId = clientId
            newSession.cleanSessionFlag = false
            //newSession.willQoS = MQTTSession.MQTTQosLevel.
            newSession.willRetainFlag = false
            newSession.willMsg = jsonData
            newSession.willTopic = "IosUserStatus"
         //   newSession.auth = false
            newSession.willFlag = true
            newSession.connect(toHost: host, port: port, usingSSL: false) { (error) in
                if let error = error {
                    print("Session is unable to connect \(error)")
                  //  Helper.showAlertWithMessageOnwindow(withTitle: "Error", message:"Session is unable to connect \(error)")
                    self.isConnected = false
                } else {
                    print("Connected")
                    self.isConnected = true
                }
            }
        }
        catch {
            
            
            
            
        }
        
    }
}

