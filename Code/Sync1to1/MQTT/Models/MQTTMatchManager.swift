//
//  MQTTMatchManager.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 09/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient
import UserNotifications

class MQTTMatchManager: NSObject {

    let mqttModel = MQTT.sharedInstance
    static let sharedInstance = MQTTMatchManager()
  
    //MARK:- SUBSCRIBE FOR TOPICS.
    
    
    /// METHOD to subscribe for delete user
    ///
    /// - Parameters:
    ///   - data: data is the dictonary which contains profiles details.
    ///   - topic: topic is  name of recived topic (ChatAppConstants.MQTT.getProfiles)
    func subscribeToDeletedUserFromAdmin() {
     let getDeleteUserTopic = MQTTTopic.deleteUser+Helper.getMQTTID()!
        mqttModel.subscribeTopic(withTopicName: getDeleteUserTopic, withDelivering: .exactlyOnce)
    }
    
    /// METHOD to subscribe for pro user
    ///
    /// - Parameters:
    ///   - data: data is the dictonary which contains profiles details.
    ///   - topic: topic is  name of recived topic (ChatAppConstants.MQTT.getProfiles)
    func subscribeToProUserFromAdmin() {
        let getDeleteUserTopic = MQTTTopic.proUser+Helper.getMQTTID()!
        mqttModel.subscribeTopic(withTopicName: getDeleteUserTopic, withDelivering: .exactlyOnce)
    }
    
    func subscribeToUserTopic() {
        let getDeleteUserTopic = Helper.getMQTTID()!
        mqttModel.subscribeTopic(withTopicName: getDeleteUserTopic, withDelivering: .exactlyOnce)
    }
    
    /// METHOD FOR SUBSCRIBING MATCH TOPIC.
    ///
    /// - Parameter userID: is the current logged in userid.
    func subscribeToMatchesTopic() {
        let getMatchesTopic = ChatAppConstants.MQTT.getMatches+Helper.getMQTTID()!
        mqttModel.subscribeTopic(withTopicName: getMatchesTopic, withDelivering: .atLeastOnce)
    }
    
    
    /// METHOD FOR refreshing free likes.
    ///
    /// - Parameter userID: is the current logged in userid.
    func subscribeToUpdateFreeLikesTopic() {
        let getMatchesTopic = ChatAppConstants.MQTT.getnewLikes+Helper.getMQTTID()!
        mqttModel.subscribeTopic(withTopicName: getMatchesTopic, withDelivering: .atLeastOnce)
    }
    
    
    /// METHOD FOR SUBSCRIBING get profiles TOPIC.
    ///
    /// - Parameter userID: is the current logged in userid.
    func subscribeToGetProiflesTopic() {
        let getProfilesTopic = ChatAppConstants.MQTT.getProfiles+Helper.getMQTTID()!
        mqttModel.subscribeTopic(withTopicName: getProfilesTopic, withDelivering: .exactlyOnce)
    }
    
    
    /// METHOD FOR SUBSCRIBING get Unmatch TOPIC.
    ///
    /// - Parameter userID: is the current logged in userid.
    func subscribeToUnMatchProfile() {
        let unMatchProfileTopic = ChatAppConstants.MQTT.unMatchProfile+Helper.getMQTTID()!
        mqttModel.subscribeTopic(withTopicName: unMatchProfileTopic, withDelivering: .exactlyOnce)
    }
    
    /// METHOD FOR SUBSCRIBING get boostupdate TOPIC.
    ///
    /// - Parameter userID: is the current logged in userid.
    func subscribeToBoostUpdates() {
        let unMatchProfileTopic = ChatAppConstants.MQTT.boostupdate+Helper.getMQTTID()!
        mqttModel.subscribeTopic(withTopicName: unMatchProfileTopic, withDelivering: .exactlyOnce)
    }
    
    /// METHOD FOR SUBSCRIBING get profiles TOPIC.
    ///
    /// - Parameter userID: is the current logged in userid.
    func subscribeToBannedUserProfile() {
        let bannedProfileTopic = ChatAppConstants.MQTT.bannedUser+Helper.getMQTTID()!
        mqttModel.subscribeTopic(withTopicName: bannedProfileTopic, withDelivering: .exactlyOnce)
    }
    
    /// METHOD FOR SUBSCRIBING get campagin TOPIC.
    ///
    /// - Parameter userID: is the current logged in userid.
    func subscribeToCampagain() {
        var campaginTopic = ChatAppConstants.MQTT.campaign+Helper.getMQTTID()!
        mqttModel.subscribeTopic(withTopicName: campaginTopic, withDelivering: .exactlyOnce)
        campaginTopic = ChatAppConstants.MQTT.campaignAll
        mqttModel.subscribeTopic(withTopicName: campaginTopic, withDelivering: .exactlyOnce)
    }
    
    //MARK:- RECIVED TOPIC MESSAGES.
    
    /// method will trigger when any match found for current logged in user.
    ///
    /// - Parameters:
    ///   - data: data is the dictonary which contains matched user details.
    ///   - topic: topic is  name of recived topic (ChatAppConstants.MQTT.getMatches)
    func gotMatches(withData data : [String : Any], inTopic topic : String) {
        self.sendDataThroughNotification(nameOfNotification:NSNotificationNames.notificationForMatchUser , detials: data)
    }
    
    
    /// METHOD will trigger when unmatch user .
    ///
    /// - Parameters:
    ///   - data: data is the dictonary which contains profiles details.
    ///   - topic: topic is  name of recived topic (ChatAppConstants.MQTT.getProfiles)
    func gotUnMatch(withData data : [String : Any], inTopic topic : String) {
        if let tagetId = data["targetId"] as? String {
            Helper.deleteUserChat(userDeatils:tagetId)
        };  self.sendDataThroughNotification(nameOfNotification:NSNotificationNames.notificationForUnmatchProfile , detials: data)
    }
    
    
    /// METHOD will trigger when unmatch user .
    ///
    /// - Parameters:
    ///   - data: data is the dictonary which contains profiles details.
    ///   - topic: topic is  name of recived topic (ChatAppConstants.MQTT.getProfiles)
    func gotBannedTopic(withData data : [String : Any], inTopic topic : String) {
        self.sendDataThroughNotification(nameOfNotification:NSNotificationNames.notificationForBannedProfile , detials: data)
    }
    
    /// METHOD will trigger when new profiles found.
    ///
    /// - Parameters:
    ///   - data: data is the dictonary which contains profiles details.
    ///   - topic: topic is  name of recived topic (ChatAppConstants.MQTT.getProfiles)
    func gotProfiles(withData data : [String : Any], inTopic topic : String) {
        self.sendDataThroughNotification(nameOfNotification:NSNotificationNames.notificationForGetProfiles , detials: data)
    }
    
    /// METHOD will trigger when new profiles found.
    ///
    /// - Parameters:
    ///   - data: data is the dictonary which contains profiles details.
    ///   - topic: topic is  name of recived topic (ChatAppConstants.MQTT.getProfiles)
    func receivedCampaginData(withData data : [String : Any], inTopic topic : String) {
        self.sendDataThroughNotification(nameOfNotification:NSNotificationNames.notificationForCampagin , detials: data)
    }
    

    
    
    /// METHOD will trigger when user deleted from admi
    ///
    /// - Parameters:
    ///   - data: data is the dictonary which contains profiles details.
    ///   - topic: topic is  name of recived topic (ChatAppConstants.MQTT.getProfiles)
    func gotDeletedUserTopic(withData data : [String : Any], inTopic topic : String) {
        self.sendDataThroughNotification(nameOfNotification:NSNotificationNames.deletedUserFromAdmin , detials: data)
    }
    
    
    /// METHOD will trigger when user updated to pro from admin
    ///
    /// - Parameters:
    ///   - data: data is the dictonary which contains profiles details.
    ///   - topic: topic is  name of recived topic (ChatAppConstants.MQTT.getProfiles)
    func gotproUserUpdateTopic(withData data : [String : Any], inTopic topic : String) {
        if let count = data[profileData.count] as? [String:Any]{
            Helper.saveCountsObj(data: count)
        }
        
        if (data["data"] as? [String:Any]) != nil {
            Helper.saveIsUserPurchased(flag: true)
        }
    }
    
    
    /// METHOD will trigger when user got new likes
    ///
    /// - Parameters:
    ///   - data: data is the dictonary which contains profiles details.
    ///   - topic: topic is  name of recived topic (ChatAppConstants.MQTT.getProfiles)
    func gotNewLikesTopic(withData data : [String : Any], inTopic topic : String) {
       
        if let remainingLikes = data["newLikes"] as? String{
            if let hasLikes = Int(remainingLikes) {
                Helper.updateRemainingLikes(likes:hasLikes)
            }
        }
        
        if let remainingLikes = data["newLikes"] as? Int{
            Helper.updateRemainingLikes(likes:remainingLikes)
        }
    }
    
    /// METHOD will trigger when user got boost details
    ///
    /// - Parameters:
    ///   - data: data is the dictonary which contains profiles details.
    ///   - topic: topic is  name of recived topic (ChatAppConstants.MQTT.getProfiles)
    func gotboostupdateTopic(withData data : [String : Any], inTopic topic : String) {
        Helper.updateBoostDetails(boostDetails:data)
        self.sendDataThroughNotification(nameOfNotification:NSNotificationNames.notificationForBoostUpdate , detials: data)
    }
    
    func updatedCoins(withData data : [String : Any], inTopic topic : String) {
        self.sendDataThroughNotification(nameOfNotification:NSNotificationNames.updateCoinFromAdmin , detials: data)
    }

    
    /// METHOD will trigger when user got boost details
    ///
    /// - Parameters:
    ///   - data: data is the dictonary which contains profiles details.
    ///   - topic: topic is  name of recived topic (ChatAppConstants.MQTT.getProfiles)
    func gotUnBlockUpdate(withData data : [String : Any], inTopic topic : String) {
        self.sendDataThroughNotification(nameOfNotification:NSNotificationNames.unBlockUpdate , detials: data)
    }
    
    func gotBlockUpdate(withData data : [String : Any], inTopic topic : String) {
        self.sendDataThroughNotification(nameOfNotification:NSNotificationNames.blockUpdate , detials: data)
    }
    
    func sendDataThroughNotification(nameOfNotification:String,detials:[String:Any]) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:nameOfNotification), object: detials)
    }
    
    
    
    
}


