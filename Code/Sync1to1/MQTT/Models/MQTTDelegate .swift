

//
//  MessageNotificationDelegate.swift
//  MQTT Chat Module
//
//  Created by Dinesh Guptha Bavirisetti on 31/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient

class MQTTDelegate: NSObject, MQTTSessionDelegate {
    
    
    func newMessage(_ session: MQTTSession!, data: Data!, onTopic topic: String!, qos: MQTTQosLevel, retained: Bool, mid: UInt32) {
        
        do {
            
            guard let dataObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
            
            let messageType = dataObj["messageType"] as? String
            
            
            if topic.range(of:ChatAppConstants.MQTT.messagesTopicName) != nil {
                MQTTChatManager.sharedInstance.getNewMessage(withData: dataObj, in: topic)
            }
            else if topic.range(of:ChatAppConstants.MQTT.acknowledgementTopicName) != nil {
                MQTTChatManager.sharedInstance.getNewAcknowladgment(withData: dataObj, in: topic)
            }
            else if topic.range(of:ChatAppConstants.MQTT.onlineStatus) != nil {
                MQTTChatManager.sharedInstance.gotOnlineStatus(withData: dataObj, withTopic:topic)
            }
            else if topic.range(of:ChatAppConstants.MQTT.typing) != nil {
                MQTTChatManager.sharedInstance.gotTypingStatus(withData: dataObj, withTopic:topic)
            }
            else if topic.range(of: ChatAppConstants.MQTT.getChats) != nil {
                MQTTChatManager.sharedInstance.gotChats(withData: dataObj, inTopic: topic)
            }
            else if topic.range(of: ChatAppConstants.MQTT.getMessages) != nil {
                MQTTChatManager.sharedInstance.gotMessages(withData: dataObj, inTopic: topic)
            }
            else if messageType == "match/user" {
                MQTTMatchManager.sharedInstance.gotMatches(withData: dataObj, inTopic: topic)
            }
            else if messageType == "unMatch" {
                MQTTMatchManager.sharedInstance.gotUnMatch(withData: dataObj, inTopic: topic)
            }
            else if messageType == "bannedUser" {
                MQTTMatchManager.sharedInstance.gotBannedTopic(withData: dataObj, inTopic: topic)
            }
            else if topic.range(of: ChatAppConstants.MQTT.getProfiles) != nil {
                MQTTMatchManager.sharedInstance.gotProfiles(withData: dataObj, inTopic: topic)
            }else if (topic.range(of: MQTTTopic.callsAvailability) != nil ) || (topic.range(of: MQTTTopic.calls) != nil){
                MQTTCallManager.didRecieve(withMessage: dataObj, inTopic: topic)
            }else if topic.range(of:ChatAppConstants.MQTT.messagesTopicName) != nil {
                MQTTChatManager.sharedInstance.getNewMessage(withData: dataObj, in: topic)
            }
            else if messageType == "deleteUser" {
                MQTTMatchManager.sharedInstance.gotDeletedUserTopic(withData: dataObj, inTopic: topic)
            }
            else if  messageType == "proUser" {
                MQTTMatchManager.sharedInstance.gotproUserUpdateTopic(withData: dataObj, inTopic: topic)
            }else if messageType == "newLikes" {
                MQTTMatchManager.sharedInstance.gotNewLikesTopic(withData: dataObj, inTopic: topic)
            }else if messageType == "boost" {
                MQTTMatchManager.sharedInstance.gotboostupdateTopic(withData: dataObj, inTopic: topic)
            }else if messageType == "block" {
                MQTTMatchManager.sharedInstance.gotBlockUpdate(withData: dataObj, inTopic: topic)
            }else if messageType == "unBlock" {
                MQTTMatchManager.sharedInstance.gotUnBlockUpdate(withData: dataObj, inTopic: topic)
            }else if messageType == "AdminCoin" {
                MQTTMatchManager.sharedInstance.updatedCoins(withData: dataObj, inTopic: topic)
            }
            
            
            
        } catch let jsonError {
            print("Error !!!\(jsonError)")
        }
    }
    
    func messageDelivered(_ session: MQTTSession!, msgID: UInt16) {
        print("Message delivered")
        guard let userID = self.getUserID() else { return }
        session.persistence.deleteAllFlows(forClientId: userID)
    }
    
    fileprivate func getUserID() -> String? {
        let userData = Database.shared.fetchResults().first
        guard let mqttID = userData?.userId  else { return nil }
        if mqttID == "mqttId" {
            return nil
        }
        return mqttID
    }
    
    
    
    
    func handleEvent(_ session: MQTTSession!, event eventCode: MQTTSessionEvent, error: Error!) {
        
        switch eventCode {
        case .connected:
            
            print("connected now")
            guard let userID  = Helper.getMQTTID() else {return}
            let mqtt = MQTT.sharedInstance
            
            
            if  let isCallgoingOn = UserDefaults.standard.object(forKey: "iscallgoingOn") as? Bool {
                if mqtt.isConnected == false || isCallgoingOn == false {
                    MQTTCallManager.sendcallAvilibilityStatus(status: 1, topic: MQTTTopic.callsAvailability + userID)
                }
            }
            
            if mqtt.isConnected == false {
                mqtt.isConnected = true
            }
            
            ///Subscribing Message Channel
            let msgTopic = ChatAppConstants.MQTT.messagesTopicName + userID
            mqtt.subscribeTopic(withTopicName: msgTopic, withDelivering: .atLeastOnce)
            
            ///Subscribing Acknowledgment Channel
            let ackTopic = ChatAppConstants.MQTT.acknowledgementTopicName+userID
            mqtt.subscribeTopic(withTopicName: ackTopic, withDelivering: .exactlyOnce)
            MQTT.sharedInstance.subscribeTopic(withTopicName: MQTTTopic.calls+userID, withDelivering: .atLeastOnce)
            
            let state = UIApplication.shared.applicationState
            if state == .active{
                
                MQTTChatManager.sharedInstance.sendOnlineStatus(withOfflineStatus: false)
            }else if state == .background {
                MQTTChatManager.sharedInstance.sendOnlineStatus(withOfflineStatus: true)
            }
            
            
            MQTTMatchManager.sharedInstance.subscribeToUpdateFreeLikesTopic()
            MQTTMatchManager.sharedInstance.subscribeToUserTopic()
            
            
        case .connectionClosed:
            //  MQTT.sharedInstance.isConnected = false
            guard let userID  = Helper.getMQTTID() else {return}
            MQTT.sharedInstance.connect(withClientId: userID)
            // MQTT.sharedInstance.mqttSession.connect()
            print("connection is closed")
        default:
            print("no states")
            
        }
        
    }
    
}
