//
//  ReScheduleVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 28/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxAlamofire
import Kingfisher
import EventKit
import KRProgressHUD


class ReScheduleVC: UIViewController {
    
    @IBOutlet weak var firatProfileView: UIView!
    @IBOutlet weak var secondProfView: UIView!
    @IBOutlet weak var secondProfPic: UIImageView!
    @IBOutlet weak var firstProfPic: UIImageView!
    @IBOutlet weak var screenTitle: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var placeTextF: UITextField!
    @IBOutlet weak var timeTextF: UITextField!
    @IBOutlet weak var suggestTimeLabel: UILabel!
    @IBOutlet weak var timeSelectView: UIView!
    @IBOutlet weak var backBtnTop: NSLayoutConstraint!
    @IBOutlet weak var suggestPlaceHeight: NSLayoutConstraint!
    @IBOutlet weak var heart1: HeartBeatingImageView!
    @IBOutlet weak var heart2: HeartBeatingImageView!
    @IBOutlet weak var heart3: HeartBeatingImageView!
    @IBOutlet weak var heart4: HeartBeatingImageView!
    @IBOutlet weak var headTitleLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var topCloseBtn: UIButton!
    @IBOutlet weak var suggestPlaceLabel: UILabel!
   

    var popUpFor:CoinsPopupFor = .forAll
    let createDateVM = CreateDateVM()
    let coinConfig = Helper.getCoinConfig()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        suggestPlaceLabel.text = StringConstants.suggestAPlace()
        suggestTimeLabel.text = StringConstants.suggestTime()
        placeTextF.placeholder = StringConstants.selectHere()
        timeTextF.placeholder = StringConstants.selectHere()
        updateView()
        dateAPIsResponse()
        
    }
    
    func setTime(fromDate date: Date, andTime time:Date){
        let  date:Date = Date(timeIntervalSince1970: Double(createDateVM.resheduleProfile.dateTime))
        self.timeTextF.text = CalenderUtility.getDateAndTime(fromDate: date)
        
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        heart1.startHeartBeatAnimation()
        heart2.startHeartBeatAnimation()
        heart3.startHeartBeatAnimation()
        heart4.startHeartBeatAnimation()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.headTitleLabel.textColor = UIColor.clear
        headerView.isHidden = true
        topCloseBtn.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateLocation), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForLocationUpdate), object: nil)
        
        self.navigationController?.isNavigationBarHidden = true
        initialSetup()
        
        switch createDateVM.dateType {
        
        case .videoDate:
            self.headTitleLabel.text = StringConstants.planVideoDate()
            self.screenTitle.text = StringConstants.planVideoDate()
            break
        case .audioDate:
            self.headTitleLabel.text = StringConstants.planAudioDate()
             self.screenTitle.text = StringConstants.planAudioDate()
            break
        case .inPersonDate:
             self.headTitleLabel.text = StringConstants.planInPersonDate()
             self.screenTitle.text = StringConstants.planInPersonDate()
            break
        case .none:
            self.headTitleLabel.text = StringConstants.planInPersonDate()
            self.headTitleLabel.text = StringConstants.planInPersonDate()
            break
        }
        
    }
    
    func initialSetup() {
        titleLabel.text = String(format: StringConstants.suggestNewPlace(), (createDateVM.resheduleProfile.nameOfTheProfile))
        //"Suggest a new place and time to meet.\n will notify you if \(createDateVM.resheduleProfile.nameOfTheProfile) confirms."
        
        
        if createDateVM.isFromMatch == false {
            self.setTime(fromDate: createDateVM.previousSelectedDate, andTime: createDateVM.previousSelectedDate)
        }
        
        if createDateVM.isForCallDate {
            suggestPlaceHeight.constant = 0
        }else {
            suggestPlaceHeight.constant = 70
        }
        
        
        
        let newUrl1 = URL(string: createDateVM.userData[0].profilePictureURL!)
        firstProfPic.kf.setImage(with:newUrl1, placeholder:UIImage.init(named:"02m"))
        
        if createDateVM.isFromMatch {
            let newUrl1 = URL(string: createDateVM.matchedProfile.profilePic)
            secondProfPic.kf.setImage(with:newUrl1, placeholder:UIImage.init(named:"02m"))
        }
        else {
            let newUrl = URL(string: createDateVM.resheduleProfile.profilePicutre)
            secondProfPic.kf.setImage(with:newUrl, placeholder:UIImage.init(named:"02m"))
        }
        
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        if(self.createDateVM.dateFromChat) {
            self.navigationController?.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func suggestPlaceAction(_ sender: Any) {
        performSegue(withIdentifier: String(describing: ChooseLocationVC.self), sender: self)
        
    }
    
    @IBAction func suggestTimeAction(_ sender: Any) {
        performSegue(withIdentifier: String(describing: DatePickerVC.self), sender: self)
        
        
    }
    
    
    ///Done Button Action
    ///
    /// - Parameter sender: IButton
    @IBAction func DoneBtnAction(_ sender: Any) {
        self.popUpFor = .forDate
        
        if timeTextF.text?.count == 0 && placeTextF.text?.count == 0{
            Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: StringConstants.pleaseScheduleDate(), onViewController:self)
            return
        }
        
        if createDateVM.isFromMatch == false {
            
            
            if createDateVM.dateType == .audioDate || createDateVM.dateType == .videoDate {
                
                if createDateVM.newSelectedDate == nil {
                    Helper.showAlertWithMessage(withTitle: StringConstants.reschedule(), message: StringConstants.pleaseSuggestTime(), onViewController: self)
                    return
                }
                
            }else {
                if createDateVM.newSelectedDate == nil && createDateVM.newSelectedAddress == nil {
                    
                    Helper.showAlertWithMessage(withTitle: StringConstants.reschedule(), message: StringConstants.suggestPlaceAndTime(), onViewController: self)
                    return
                    
                }
            }
            
        }
        
        if Helper.isUserHasMinimumCoins(minValue: coinsFor.createDate) {
            if Helper.isShowCoinsDeductPopup(popUpFor: .forDate) {
                 //createDateVM.dateType
                var profileName = ""
            var coins = 100
            let coinConfig = Helper.getCoinConfig()
                var date = ""
              
                switch createDateVM.dateType {
                case .audioDate:
                    coins = coinConfig.coinsForCallDate
                    date = StringConstants.aAudioDate()
                    break
                case .videoDate:
                    coins = coinConfig.coinsForVideoDate
                    date = StringConstants.aVideoDate()
                    break
                case .inPersonDate:
                    coins = coinConfig.coinsForInPerson
                     date = StringConstants.inPersionDate()
                    break
                default:
                    break
                }
                if createDateVM.isFromMatch {
                    profileName = createDateVM.matchedProfile.name
                }
                else {
                  profileName = createDateVM.resheduleProfile.nameOfTheProfile
                }
                let mainTitle = String(format: StringConstants.spendCoinsToRequest(), coins,date,profileName)
                showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coins"), mainTitle: mainTitle, subTitle: "" , buttnTitle: StringConstants.confirmDate(), dontShowHide: true)
            }
            else {
                self.coinBtnPressed(sender: sender as! UIButton)
            }
        } else {
            showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coinEmpty"), mainTitle: StringConstants.CoinsGotOver(), subTitle: StringConstants.NoEnoughCoins() + StringConstants.dateLower(), buttnTitle: StringConstants.BuyCoins(), dontShowHide: true)
        }
        
        
    }
    
    
    func showCoinsPopupForChat(icon: UIImage, mainTitle: String, subTitle: String, buttnTitle: String, dontShowHide: Bool){
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let coinsView = spendSomeCoinPopUp(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        coinsView.loadPopUpView()
        coinsView.coinsButton.titleLabel?.font = UIFont(name: CircularAir.Bold , size: 22)
        coinsView.updatePopup(icon: icon, mainTitle: mainTitle, subTitle: subTitle, buttnTitle: buttnTitle, dontShowHide: dontShowHide)
        coinsView.spendSomeCoinDelegate = self
        window.addSubview(coinsView)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == String(describing: DatePickerVC.self) {
            
            if let viewController: DatePickerVC = segue.destination as? DatePickerVC {
                viewController.ModeDate = false
                viewController.Response_Picker.subscribe(onNext: { success in
                    print(success)
                    self.createDateVM.newSelectedDate = success
                    
                    guard let dateString:String = CalenderUtility.getDateAndTime(fromDate: success) else {
                        return
                    }
                    self.timeTextF.text = dateString
                })
            }
        }
        else if segue.identifier == String(describing: ChooseLocationVC.self) {
            
            if let locationVC:ChooseLocationVC = segue.destination as? ChooseLocationVC {
                locationVC.delegate = self
                locationVC.locationVM.isFromMatch = true
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gotoCoinBalanceScreen()
    {
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "addCoin") as! AddCoinsVC
        userBasics.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.pushViewController(userBasics, animated: true)
        return
    }
    
    
    
}

extension ReScheduleVC: spendSomeCoinPopUpDelegate {
    
    
    func coinBtnPressed(sender: UIButton){
        
        if self.popUpFor == .forDate {
            Helper.showCoinsDeductPopup(flag: false, popUpFor: .forDate)
        }
        
        // check for no coins
        let title = sender.title(for: .normal)
        if title == StringConstants.BuyCoins() {
            self.gotoCoinBalanceScreen()
            return
        }
        
        
        
        if createDateVM.newSelectedDate == nil {
            createDateVM.newSelectedDate = createDateVM.previousSelectedDate
        }
        if createDateVM.newSelectedAddress == nil {
            createDateVM.selectedAdreess?.latitude = createDateVM.resheduleProfile.latitude
            createDateVM.selectedAdreess?.longitude = createDateVM.resheduleProfile.longitude
            createDateVM.selectedAdreess?.name = createDateVM.resheduleProfile.placeName
        }
        
        
//        let epochValueForSelectedDate = (createDateVM.newSelectedDate?.timeIntervalSince1970)!*1000
//
//        let  presentTime = Date().timeIntervalSince1970*1000
//
//        if epochValueForSelectedDate/1000 - presentTime/1000 < 7200 {
//            Helper.showAlertWithMessage(withTitle: "Error", message: "Please shedule the date atleast after two hours from present time.", onViewController:self)
//            return
//        }
        
     //   AudioHelper.sharedInstance.playCoinsSound()
        
        
        if createDateVM.isFromMatch {
        
            self.createDateVM.createNewDate()
            
        } else {
            
            let selectedDateAndTime:Date = createDateVM.newSelectedDate!
            let epochValueForSelectedDate = selectedDateAndTime.timeIntervalSince1970*1000
            self.createDateVM.setDateStatus(profile:createDateVM.resheduleProfile , dateStatus:3, newDate:epochValueForSelectedDate)
        }
        
        
        
    }
}



// MARK: - Observers for APIs
extension ReScheduleVC {
    
    
    func dateAPIsResponse(){
        
        self.createDateVM.CreateDateVM_resp.subscribe(onNext:{ success in
            switch success {
            case CreateDateVM.ResponseType.dateCreated:
                
                if(self.createDateVM.dateFromChat) {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    (UIApplication.shared.delegate?.window??.rootViewController as? TabBarController)?.selectedIndex = 2
                    self.navigationController?.dismiss(animated: true, completion: nil)
                }
                Helper.showAlertWithMessage(withTitle: StringConstants.success(), message: StringConstants.dateCreated(), onViewController: self)
                DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                    let name = NSNotification.Name(rawValue: "updateActionsCountForAds")
                    NotificationCenter.default.post(name: name, object: self, userInfo: nil)
                }
                break
                
                
            case CreateDateVM.ResponseType.dateResponseType:  // confirmed or rejected or canceled
                self.dismiss(animated: true, completion: nil)
                break
                
                
            case CreateDateVM.ResponseType.faild:
                Helper.showAlertWithMessage(withTitle:StringConstants.error(),message: self.createDateVM.errorMsg, onViewController:self)
                break
                
            }
        }).disposed(by: createDateVM.disposeBag)
        
    }
    
    func updateView(){
        firatProfileView.layer.cornerRadius = firatProfileView.frame.size.height/2
        secondProfPic.layer.cornerRadius = secondProfPic.frame.size.height/2
        
        secondProfView.layer.cornerRadius = secondProfView.frame.size.height/2
        firstProfPic.layer.cornerRadius = firstProfPic.frame.size.height/2
        
        Helper.setShadow(sender: firatProfileView )
        Helper.setShadow(sender: secondProfView )
        placeTextF.text = createDateVM.resheduleProfile.placeName
    }
    
    
}


// MARK: - ChooseLocationVCDelegate
extension ReScheduleVC: ChooseLocationVCDelegate {
    
    func didSelectAddress(address: Location) {
        createDateVM.selectedAdreess = address
        createDateVM.newSelectedAddress = address
        if address.name.count != 0 {
            self.placeTextF.text = address.name
        }else {
            placeTextF.text = address.fullText
        }
        
    }
    
    @objc func updateLocation(notification: NSNotification){
        let selectedLoctaion = notification.object as! Location
        self.createDateVM.selectedAdreess = selectedLoctaion
        createDateVM.newSelectedAddress = selectedLoctaion
        if selectedLoctaion.fullText.count != 0 && selectedLoctaion.fullText != " "{
            self.placeTextF.text = selectedLoctaion.fullText
        }else{
            self.placeTextF.text = selectedLoctaion.locationDescription
        }
        
    }
    
}




// MARK: - UIScrollViewDelegate
extension ReScheduleVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let movedOffset: CGFloat = scrollView.contentOffset.y
        
        self.view.endEditing(true)
        let pageWidth: CGFloat = 88
        let ratio: CGFloat = (movedOffset) / (pageWidth)
        print(movedOffset)
        if ratio > 0 {
            if movedOffset >= 20 {
                self.headTitleLabel.textColor = Colors.PrimaryText.withAlphaComponent(ratio)
                headerView.backgroundColor = Helper.getUIColor(color: "#ffffff")//.withAlphaComponent(ratio)
                Helper.setShadow(sender: headerView)
                if ratio <= 1 && ratio >= 0{
                    self.headTitleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle)*ratio)
                }
                if movedOffset > 88 {
                    self.headTitleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle))
                }
                headerView.isHidden = false
                topCloseBtn.isHidden = false
            }
        }else {
            
            self.headTitleLabel.textColor = UIColor.clear
            headerView.backgroundColor = UIColor.clear
            Helper.removeShadow(sender: headerView)
            topCloseBtn.isHidden = true
            headerView.isHidden = true
        }
    }
    
    
}





