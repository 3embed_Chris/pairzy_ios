//
//  HeartBeatingImageView.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 27/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class HeartBeatingImageView: UIImageView {
    
    struct Constant {
        static let animationDuration = 0.3
        static let currentValueOfImage = 1.0
        static let transforimgValueOfImage = 0.7
        static let animationKey = "animateOpacity"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func startHeartBeating(){
        self.image = #imageLiteral(resourceName: "heart_icon")
        let animation = CABasicAnimation(keyPath: Constants.animationKeyForScale)
        animation.duration = Constant.animationDuration
        animation.repeatCount = Float.greatestFiniteMagnitude
        animation.autoreverses = true
        animation.fromValue = Constant.currentValueOfImage
        animation.toValue = Constant.transforimgValueOfImage
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        self.layer.add(animation, forKey: Constant.animationKey)
    }
}
