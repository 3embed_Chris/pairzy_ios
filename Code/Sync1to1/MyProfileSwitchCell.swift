//
//  MyProfileSwitchCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 02/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class MyProfileSwitchCell: UITableViewCell {

    @IBOutlet weak var switchTitle: UILabel!
    @IBOutlet weak var proileSwitch: UISwitch!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func updateCell(title:String,status:Bool){
        switchTitle.text = title
        proileSwitch.isOn = status
//        if  Helper.isSubscribedForPlans(){
//            proileSwitch.isOn = status
//        }else {
//            proileSwitch.isOn =  false
//        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
