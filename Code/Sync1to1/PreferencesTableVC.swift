//
//  PreferencesTableVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 28/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class PreferencesTableVC: UIViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var screenTitle: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var topLineWidth: NSLayoutConstraint!
    @IBOutlet weak var preferNotToSay: UILabel!
    @IBOutlet weak var preferNotToSayBtn: UIButton!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var preferNotToSayView: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var topLineView: UIView!
    
    @IBOutlet weak var backButtonOutlet: UIButton!
    var screenIndex = 0
    var currentPrefCategoryIndex = 0
    var currentPrefSubCategoryIndex = 0
    
    var nextPrefCategoryIndex = 0
    var nextPrefSubCategoryIndex = 0
    
    var isFirstTime = false
    var isForEditProfile = false
    
    var totalCount = 0
    var profileDetile = [PreferencesModel]()
    var myPreference:[PreferencesModel] = Helper.getPreference()
    let screenWidth = UIScreen.main.bounds.size.width
    var isNotPrefer = false
    
    var selectedIndex:IndexPath = IndexPath(row: 300, section: 0)
    
    let preferenceVM = PreferenceVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPreferenceType()
        checkTotalCount()
        didGetResponse()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if isForEditProfile {
            skipBtn.isHidden = true
            self.myPreference = profileDetile
        }else {
            skipBtn.isHidden = false
        }
        
        if isFirstTime{
            self.backButtonOutlet.isHidden = true
        }else {
            self.backButtonOutlet.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isFirstTime{
            self.backButtonOutlet.isHidden = true
        }else {
            self.backButtonOutlet.isHidden = false
        }
        
   
     
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func nextButtonAction(_ sender: Any) {
        nextButtonTapped(isNextButton: true)
    }
    
    
    func nextButtonTapped(isNextButton:Bool) {
        
        if(isNextButton) {
            preferenceVM.updatePreference()
            updateLocalPrefsWithSelectedValues(selectedValues:preferenceVM.selectedValuesObj)
        }
        
        
    }
    
    
    @IBAction func skipBtnaction(_ sender: Any) {
        // self.gotoNextCat()
        Helper.makeTabbarAsRootVc(forVc: self)
    }
    
    
    func gotoNextCat() {
        
        if screenIndex+1 == totalCount {
            Helper.makeTabbarAsRootVc(forVc: self)
            
        }

        else if currentPrefSubCategoryIndex == (myPreference[currentPrefCategoryIndex].arrayOfSubPreference.count - 1) {
            //go to next category
            nextPrefSubCategoryIndex = 0
            
            nextPrefCategoryIndex = currentPrefCategoryIndex + 1
            if myPreference[nextPrefCategoryIndex].arrayOfSubPreference.count == 0 {
                if nextPrefCategoryIndex < totalCount {
                     nextPrefCategoryIndex += 1
                }
            }
             getoNextVC()
            
        }
        else {
            
            // go to next subCategory in current category.
            nextPrefSubCategoryIndex = currentPrefSubCategoryIndex+1
            nextPrefCategoryIndex = currentPrefCategoryIndex
            getoNextVC()
        }
        
    }
    
    
    
    
    func getoNextVC(){
        let prefModel = myPreference[nextPrefCategoryIndex].arrayOfSubPreference[nextPrefSubCategoryIndex
        ]
        switch prefModel.prefTypeId {
        case 1,2,10,5:
            pushToNextPref()
            
        default:
            print("")
        }
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func preferNotToSayAction(_ sender: UIButton) {
        
        if sender.currentImage == #imageLiteral(resourceName: "notPreferUnSelected") {
            preferNotToSayBtn.setImage(#imageLiteral(resourceName: "notPreferSelected"), for: .normal)
            isNotPrefer = true
            nextBtn.isEnabled = true
            
        }else if sender.currentImage == #imageLiteral(resourceName: "notPreferSelected") {
            isNotPrefer = false
            preferNotToSayBtn.setImage(#imageLiteral(resourceName: "notPreferUnSelected"), for: .normal)
            nextBtn.isEnabled = false
        }
        preferenceVM.selectedValuesObj.removeAll()
        mainTableView.reloadData()
        
    }
    
    func checkTotalCount(){
        totalCount = 0
        let category = myPreference.count
        for i in 0..<category{
            totalCount = (totalCount +  myPreference[i].arrayOfSubPreference.count)
        }
    }
    
    
    func setupPreferenceType()
    {
        //fetching crrent pref details
        skipBtn.setTitle(StringConstants.skip(), for: .normal)
        let categoryCount =  myPreference[currentPrefCategoryIndex].arrayOfSubPreference.count
        
        let CurrentPref =  myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex]
        
        screenTitle.text = CurrentPref.prefMainTitle.localizedCapitalized
        subTitle.text = CurrentPref.prefSubTitle.localizedCapitalized
        preferenceVM.selectedPrefId = CurrentPref.prefId
        preferenceVM.selectedValuesObj = CurrentPref.selectedValues
        subTitle.textColor = Colors.AppBaseColor
        topLineView.backgroundColor = Colors.AppBaseColor
        
        if isForEditProfile {
            self.skipBtn.isHidden = true
        }else {
            skipBtn.isHidden = false
        }
        
        if isForEditProfile {
            topLineWidth.constant = 0
        }else {
            
            topLineWidth.constant =  screenWidth/CGFloat(categoryCount - currentPrefSubCategoryIndex)
        }
        
        if CurrentPref.prefTypeId == 1 || CurrentPref.prefTypeId == 2 {
            preferNotToSayView.isHidden = false
        }
        else {
            preferNotToSayView.isHidden = true
        }
        
        nextBtn.isEnabled = false
    }
    
}
extension PreferencesTableVC {
    
    func didGetResponse(){
        
        preferenceVM.preferenceVM_resp.subscribe(onNext:{ success in
            if success  {
              
                if(self.isForEditProfile) {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.gotoNextCat()
                }
                
                
            }
        }).disposed(by: preferenceVM.disposeBag)
    }
}

extension PreferencesTableVC {
    
    /// based on the preference type update the screen
    func setUpPreferenceType(){
        
        //fetching crrent pref details
        let categoryCount =  myPreference[currentPrefCategoryIndex].arrayOfSubPreference.count
        let CurrentPref =  myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex]
        screenTitle.text = CurrentPref.prefMainTitle
        subTitle.text = CurrentPref.prefSubTitle
        topLineWidth.constant =  screenWidth/CGFloat(categoryCount - currentPrefSubCategoryIndex)
        skipBtn.isHidden = false
        if CurrentPref.prefTypeId == 1 || CurrentPref.prefTypeId == 2 {
            preferNotToSayView.isHidden = false
        }
        else {
            preferNotToSayView.isHidden = true
        }
        
    }
    
    
    func pushToNextPref() {
        //check if pref exists or not
        if(screenIndex == self.totalCount) {             //
            // go to home screen.
            // add same check text field case also
             Helper.makeTabbarAsRootVc(forVc: self)
            
        }else{
         if(self.myPreference[nextPrefCategoryIndex].arrayOfSubPreference[nextPrefSubCategoryIndex].prefTypeId == 5) {
                let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.TextFieldTypeVC) as! PreferencesTFVC
                
                userBasics.screenIndex = screenIndex+1
                userBasics.currentPrefCategoryIndex = self.nextPrefCategoryIndex
                userBasics.currentPrefSubCategoryIndex = self.nextPrefSubCategoryIndex
                self.navigationController?.pushViewController(userBasics, animated: true)
            } else {
                
                let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: StoryBoardIdentifier.PreferenceTable) as! PreferencesTableVC
                userBasics.screenIndex = screenIndex+1
                userBasics.currentPrefCategoryIndex = self.nextPrefCategoryIndex
                userBasics.currentPrefSubCategoryIndex = self.nextPrefSubCategoryIndex
                
                self.navigationController?.pushViewController(userBasics, animated: true)
            }
        }
    }
    
    
    func updateLocalPrefsWithSelectedValues(selectedValues:[String]) {
        
        let CurrentPref =  myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex]
        if  var data = UserDefaults.standard.object(forKey: "preferences") as? [String:Any] {
            if  var allCategories = data[ApiResponseKeys.myPreferences] as? [[String:Any]] {
                print("after update SelectedValues:",allCategories)
                for (categoryIndex,singleCategory) in allCategories.enumerated() {
                    let loopCatTitle = singleCategory["title"] as! String
                    if(CurrentPref.prefMainTitle == loopCatTitle) {
                        if var subCats = singleCategory["data"] as? [[String:Any]] {
                            for (subCatIndex,subCat) in subCats.enumerated() {
                                var newSubCat = subCat
                                if let prefId =  subCat[ApiResponseKeys.preferenceId] as? String {
                                    if prefId == CurrentPref.prefId {
                                        newSubCat["selectedValues"] = selectedValues
                                        subCats[subCatIndex] = newSubCat
                                        allCategories[categoryIndex]["data"] = subCats
                                        data[ApiResponseKeys.myPreferences] = allCategories
                                        Helper.savePreferences(data:data)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    /// creates object of UserBaiscDetailsVC and push that viewController
    func pushVCForType() {
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.userBasicSI) as! UserBaiscDetailsVC
        //userBasics.basicDetails = .Work
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
}
