//
//  RequestModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 22/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class RequestModel: NSObject {
    var ApiRequesDetails:[String:Any] = [:]

    func loginRequestDetails(phoneNumber:String,password:String) -> RequestModel {
        
        var pushToken = ServiceInfo.constantPushToken
        if let token = UserDefaults.standard.value(forKey: SyUserdefaultKeys.kNSUPushNotificationTokenKey) as? String
        {
            pushToken = token
        }
        
        var appVersion = "1.0"
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            appVersion = version
        }
        
        let userIp = Helper.getUserIP()
        var localTimeZoneName: String { return TimeZone.current.identifier }
        
        let locDetails:[String:Any?] = Helper.getUserLocation()
        var latitude = 0.0
        var longitude = 0.0

        if Platform.isSimulator {
            print("Running on Simulator")
            latitude = 13.028694
            longitude = 77.589564
        } else {
            let hasValue:String = locDetails["hasLocation"] as! String
            
            if Helper.getIsFeatureExist(){
                if(hasValue == "1") {
                    let featureLoc:Location =  Helper.getFeatureLocation()
                    latitude = featureLoc.latitude
                    longitude = featureLoc.longitude
                }
            }else {
                
                if(hasValue == "1") {
                    if let lat = locDetails["lat"] as? Double {
                        latitude = lat
                    }
                    if let long = locDetails["long"] as? Double {
                         longitude = long
                    }
                }
            }
            
        }
        
        let params =    [
            ServiceInfo.phoneNumberWIthCode:phoneNumber as AnyObject,
            ServiceInfo.password:password as AnyObject,
            ServiceInfo.PushToken:pushToken as AnyObject,
            ServiceInfo.DeviceType:"1" as AnyObject,
            ServiceInfo.deviceMakeKey:UIDevice.current.name as AnyObject,
            ServiceInfo.deviceModelKey:Helper.DeviceModel,
            ServiceInfo.deviceId:UIDevice.current.identifierForVendor?.uuidString as AnyObject,
            ServiceInfo.iPAddress:userIp,
            ServiceInfo.latitude            :latitude,
            ServiceInfo.longitude           :longitude,
            ServiceInfo.timeZone            :localTimeZoneName,
            ServiceInfo.appVersion           :appVersion,
            ServiceInfo.modelNumber          : UIDevice.current.model,
            ServiceInfo.deviceOs          : UIDevice.current.systemVersion
            ] as [String: AnyObject]
        
        self.ApiRequesDetails = params
        
        return self
    }
    
    func signUpRequestDetails(signupDetails:[String:Any]) -> RequestModel {
        self.ApiRequesDetails = signupDetails
        return self
    }
    
    
    
    func emailVerifyDetails(verifyEmail: [String:Any]) -> RequestModel {
        self.ApiRequesDetails = verifyEmail
        return self
    }

    func verificationCodeRequestDetails(verifyCodeDetails:[String:Any]) -> RequestModel {
        self.ApiRequesDetails = verifyCodeDetails
        return self
    }
    
    func ResetPasswordRequestDetails(Details:[String:Any]) -> RequestModel {
        self.ApiRequesDetails = Details
        return self
    }
    
    func likeProfileRequestDetails(Details:[String:Any]) -> RequestModel {
        self.ApiRequesDetails = Details
        return self
    }
    
    func dislikeProfileRequestDetails(Details:[String:Any]) -> RequestModel {
        self.ApiRequesDetails = Details

        return self
    }
    
    func updateMediaDetails(media:[String:Any]) -> RequestModel {
        self.ApiRequesDetails = media
        return self
    }
    
    func updateProfile(detail:[String:Any]) -> RequestModel{
        self.ApiRequesDetails = detail
        return self
    }
 
    func faceBookLoginDetail(fbDetails:LocalAuthentication) -> RequestModel{
        
        
        var gender = 0
        
        if fbDetails.genderString == "female" {
            gender = 2
        }
        else if fbDetails.genderString == "male" {
            gender = 1
        }
        
        var voipPushToken = ""
        
        if let voipPush = UserDefaults.standard.value(forKey:"callPushToken") as? String {
            voipPushToken = voipPush
        }
        
        var apnsPushToken = ""
        if let apnsPush = UserDefaults.standard.value(forKey:"deviceTokenString") as? String {
            apnsPushToken = apnsPush
        }
        
        
        var pushToken = ServiceInfo.constantPushToken
        if let token = UserDefaults.standard.value(forKey: SyUserdefaultKeys.kNSUPushNotificationTokenKey) as? String
        {
            pushToken = token
        }
        
        let dateOfB = fbDetails.dateOfBirth           //Date()
        let dateOfBTimeStamp = dateOfB.millisecondsSince1970
        let params = [ServiceInfo.PushToken:pushToken as AnyObject,
                      ServiceInfo.Fname:fbDetails.name,
                      ServiceInfo.FbId: fbDetails.id,
                      ServiceInfo.DateOfbirth: dateOfBTimeStamp ?? 0.0,         //fbDetails.dateOfBirth,
                      ServiceInfo.Gender: fbDetails.gender,
                      ServiceInfo.DeviceType:"1" as AnyObject,
                      ServiceInfo.voipiospush: voipPushToken,
                      ServiceInfo.apnsPush: apnsPushToken,
                      ServiceInfo.deviceMakeKey:UIDevice.current.name as AnyObject,
                      ServiceInfo.deviceModelKey: Helper.DeviceModel,
                      ServiceInfo.deviceId:UIDevice.current.identifierForVendor?.uuidString as AnyObject,
                      ServiceInfo.profilePic: fbDetails.profilePic,
                      ServiceInfo.profileVideo : fbDetails.profileVideo,
                      ServiceInfo.email: fbDetails.email] as [String : Any]
        self.ApiRequesDetails = params
        return self
    }
}
