//
//  ResponseModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 22/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class ResponseModel: NSObject {

    var statusCode :Int = 0
    var response:[String:Any] = [:]
    
    init(statusCode:Int,response:[String:Any]) {
        self.statusCode = statusCode
        self.response = response
    }
    
}
