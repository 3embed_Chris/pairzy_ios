//
//  API.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 28/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class API: NSObject {
    
    enum ErrorCode: Int {
        case Success                = 200
        case PreconditionFailed     = 412
        case BadRequest             = 400
        case InternalServerError    = 500
        case NotFound               = 404
        case Created                = 201
        case UnprocessableEntity    = 422
        case Banned                 = 403
        case dublicate              = 409
        case Unauthorized           = 401
        case NotAllowed             = 405
        
        
    }
    
    class func requestGETURL(serviceName: String,
                             withStaticAccessToken: Bool,
                             success:@escaping (JSON) -> Void,
                             failure:@escaping (NSError) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        
        let strURL = SERVICE.BASE_URL + serviceName
        let headers = getAuthHeader(staticToken: withStaticAccessToken)
        
        Alamofire.request(strURL,
                          method:.get,
                          parameters: nil,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(completionHandler: { response in
                            
                            if response.result.isSuccess {
                                var resJson = JSON(response.result.value!)
                                if let _: [String: Any] = resJson.dictionaryObject {
                                    resJson.dictionaryObject?[LoginResponse.code] = response.response?.statusCode
                                    success(resJson)
                                }
                                else {
                                    success([:])
                                }
                            }
                            else {
                                
                                if let error : NSError = response.result.error as NSError? {
                                    failure(error)
                                }
                            }
                          })
        
    }
    
    
    class func createDynamicUrl(success:@escaping (JSON) -> Void,
                                failure:@escaping (NSError) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        
        let params = ["longDynamicLink":"https://adminapiv2.findamatch.online/documentation&apn=com.datum.com&isi=931927850&ibi=com.pipelineAI.pairzy",
                      "suffix": [
                        "option": "UNGUESSABLE"
            ]] as [String : Any]
        
        //        let params = ["dynamicLinkInfo":[
        //            "dynamicLinkDomain":"pipelineai.page.link",
        //            "link":"https://projects.invisionapp.com/share/VNA3NS4RE#/screens/222736995",
        //            "iosInfo": [
        //                        "iosBundleId": "com.chaseking.sync1to1",
        ////                        "iosFallbackLink": "https://projects.invisionapp.com/share/VNA3NS4RE#/screens/222736995",
        //                        "iosAppStoreId": "1342156032"
        //                        ]]] as [String : Any]
        
        let strURL = "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyDhkab1RzSLe3sltRbYzbtAqV1b4nyEjIs"
        
        let headers = getAuthHeader(staticToken: true)
        
        Alamofire.request(strURL,
                          method:.post,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(completionHandler: { response in
                            
                            if response.result.isSuccess {
                                var resJson = JSON(response.result.value!)
                                if let _: [String: Any] = resJson.dictionaryObject {
                                    resJson.dictionaryObject?[LoginResponse.code] = response.response?.statusCode
                                    success(resJson)
                                }
                                else {
                                    success([:])
                                }
                            }
                            else {
                                
                                if let error : NSError = response.result.error as NSError? {
                                    failure(error)
                                }
                            }
                          })
        
        
    }
    
    class func requestPOSTURL(serviceName : String,
                              withStaticAccessToken: Bool,
                              params : [String : Any]?,
                              success:@escaping (JSON) -> Void,
                              failure:@escaping (Error) -> Void) {
        
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        
        let strURL = SERVICE.BASE_URL + serviceName
        let headers = getAuthHeader(staticToken: withStaticAccessToken)
        
        Alamofire.request(strURL,
                          method:.post,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(completionHandler: { response in
                            
                            if response.result.isSuccess {
                                var resJson = JSON(response.result.value!)
                                if let _: [String: Any] = resJson.dictionaryObject {
                                    resJson.dictionaryObject?[LoginResponse.code] = response.response?.statusCode
                                    success(resJson)
                                }
                                else {
                                    success([:])
                                }
                            }
                            else {
                                
                                if let error : NSError = response.result.error as NSError? {
                                    failure(error)
                                }
                            }
                          })
        
        
    }
    
    class func requestDELETEURL(serviceName : String,
                              withStaticAccessToken: Bool,
                              params : [String : Any]?,
                              success:@escaping (JSON) -> Void,
                              failure:@escaping (Error) -> Void) {
        
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        
        let strURL = SERVICE.BASE_URL + serviceName
        let headers = getAuthHeader(staticToken: withStaticAccessToken)
        
        Alamofire.request(strURL,
                          method:.delete,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(completionHandler: { response in
                            
                            if response.result.isSuccess {
                                var resJson = JSON(response.result.value!)
                                if let _: [String: Any] = resJson.dictionaryObject {
                                    resJson.dictionaryObject?[LoginResponse.code] = response.response?.statusCode
                                    success(resJson)
                                }
                                else {
                                    success([:])
                                }
                            }
                            else {
                                
                                if let error : NSError = response.result.error as NSError? {
                                    failure(error)
                                }
                            }
                          })
        
        
    }
    
    
    class func requestPUTURL(serviceName : String,
                               withStaticAccessToken: Bool,
                               params : [String : Any]?,
                               success:@escaping (JSON) -> Void,
                               failure:@escaping (Error) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        
        let strURL = SERVICE.BASE_URL + serviceName
        let headers = getAuthHeader(staticToken: withStaticAccessToken)
        Alamofire.request(strURL,
                          method:.put,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(completionHandler: { response in
                            
                            
                            if response.response?.statusCode == 401 {
                                Helper.changeRootVcInVaildToken()
                                return
                            }
                            
                            
                            
                            
                            if response.result.isSuccess {
                                var resJson = JSON(response.result.value!)
                                if let _: [String: Any] = resJson.dictionaryObject {
                                    resJson.dictionaryObject?[LoginResponse.code] = response.response?.statusCode
                                    success(resJson)
                                }
                                else {
                                    success([:])
                                }
                            }
                            else {
                                
                                if let error : NSError = response.result.error as NSError? {
                                    failure(error)
                                }
                            }
                          })
        
        
    }
    
    
    class func requestPatchURL(serviceName : String,
                               withStaticAccessToken: Bool,
                               params : [String : Any]?,
                               success:@escaping (JSON) -> Void,
                               failure:@escaping (Error) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        
        let strURL = SERVICE.BASE_URL + serviceName
        let headers = getAuthHeader(staticToken: withStaticAccessToken)
        Alamofire.request(strURL,
                          method:.patch,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(completionHandler: { response in
                            
                            
                            if response.response?.statusCode == 401 {
                                Helper.changeRootVcInVaildToken()
                                return
                            }
                            
                            
                            
                            
                            if response.result.isSuccess {
                                var resJson = JSON(response.result.value!)
                                if let _: [String: Any] = resJson.dictionaryObject {
                                    resJson.dictionaryObject?[LoginResponse.code] = response.response?.statusCode
                                    success(resJson)
                                }
                                else {
                                    success([:])
                                }
                            }
                            else {
                                Helper.hideProgressIndicator()
                                if let error : NSError = response.result.error as NSError? {
                                    failure(error)
                                }
                            }
                          })
        
        
    }
    
    class func requestGETURLWithParams(serviceName: String,
                                       params:[String : Any]?,
                                       success:@escaping (JSON) -> Void,
                                       failure:@escaping (NSError) -> Void) {
        
        let strURL = "" + serviceName
        let headers = ["authorization":"Basic \("user:3Embed".encodeToBase64())"] as [String: String]
        
        Alamofire.request(strURL,
                          method:.get,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(completionHandler: { response in
                            
                            
                            if response.result.isSuccess {
                                
                                let resJson = JSON(response.result.value!)
                                success(resJson)
                            }
                            if response.result.isFailure {
                                
                                let error : NSError = response.result.error! as NSError
                                failure(error)
                            }
                          })
    }
    
    //put
    
    class func requestPutURL(serviceName : String,
                             params : [String : Any]?,
                             success:@escaping (JSON) -> Void,
                             failure:@escaping (Error) -> Void) {
        
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        
        let strURL = SERVICE.BASE_URL + serviceName
        let headers = ["authorization":"KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj"] as [String: String]
        
        Alamofire.request(strURL,
                          method:.put,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(completionHandler: { response in
                            
                            
                            //Success
                            if response.result.isSuccess {
                                
                                let statusCode = (response.response?.statusCode)!
                                
                                switch statusCode  {
                                    
                                case 200,201:
                                    
                                    let resJson = JSON(response.result.value!)
                                    success(resJson)
                                    
                                    break
                                    
                                default : break
                                }
                            }
                          })
    }
    
    class func getAuthHeader(staticToken:Bool) -> [String:String] {
       // var params: [String: String] = [ServiceInfo.language: "1"]
        let language = Helper.getSelectedLanguage()
        var params: [String: String] = [ServiceInfo.language: language.code]

        if(staticToken) {
            params["authorization"] =  Keys.authorizationForLogin
        } else {
            params["authorization"] = UserLoginDataHelper.getUserToken()
        }
        return params
    }
}
