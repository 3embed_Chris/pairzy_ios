//
//  InstagramApi.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 23/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Alamofire
import SwiftyJSON

class InstagramApi: NSObject {
    
    class func requestGETURL(serviceName: String,
                             success:@escaping (JSON) -> Void,
                             failure:@escaping (NSError) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        Alamofire.request(serviceName,
                          method:.get,
                          parameters: nil,
                          encoding: JSONEncoding.default,
                          headers: [:]).responseJSON(completionHandler: { response in
                            
                            if response.result.isSuccess {
                                var resJson = JSON(response.result.value!)
                                if let _: [String: Any] = resJson.dictionaryObject {
                                    resJson.dictionaryObject?[LoginResponse.code] = response.response?.statusCode
                                    success(resJson)
                                }
                                else {
                                    success([:])
                                }
                            }
                            else {
                                
                                if let error : NSError = response.result.error as NSError? {
                                    failure(error)
                                }
                                Helper.hideProgressIndicator()
                            }
                          })
    }
    
}
