//
//  AuthorizationHeader.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class AuthorizationHeader: NSObject {
    static func getAuthHeader(staticToken:Bool) -> [String:String] {
        //var params: [String: String] = [ServiceInfo.language: "1"]
        let language = Helper.getSelectedLanguage()
        var params: [String: String] = [ServiceInfo.language: language.code]
        if(staticToken) {
            params["authorization"] =  Keys.authorizationForLogin
        } else {
            params["authorization"] = UserLoginDataHelper.getUserToken()
        }
       // params["Content-Type"] = "application/json"
        return params
    }
}
