//
//  CoinsAPICalls.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 01/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxAlamofire
import Alamofire


class CoinsAPICalls: NSObject {
    
    let disposeBag = DisposeBag()
    let subject_response = PublishSubject<ResponseModel>()
    
    // coinHistory
    func requestForGetCoinHistory() {
        self.requestGetAPI(apiName: APINAMES.CoinHistory)
    }
    
    //coinPlans
    func requestForGetCoinPlans() {
        self.requestGetAPI(apiName: APINAMES.CoinPlans)
    }
    
    //currentCoinBalance
    func requestForGetCoinBalance() {
        self.requestGetAPI(apiName: APINAMES.CurrentBalance)
    }
    
    //subscription plans
    func requestForGetPlans() {
        self.requestGetAPI(apiName: APINAMES.Plan)
    }
    
    //CoinConfig
    func getCoinConfig() {
        self.requestGetAPI(apiName: APINAMES.coinConfig)
    }
    
    //Plan History
    func requestForGetPlanHistory(){
        self.requestGetAPI(apiName: APINAMES.PlanHistory)
    }
    
    
    ///Post
    func requestToSubscribe(data: [String:Any]){
        self.requestPostAPI(requestData: data, apiName: APINAMES.Subscription)
        
    }
    
    func requestToPostCoinPlan(data: [String:Any]){
        self.requestPostAPI(requestData: data, apiName: APINAMES.CoinPlans)
        
    }
    
    
    
    
    func requestGetAPI(apiName:String) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.get,strURL,parameters:[:],headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
        
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                Helper.hideProgressIndicator()
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
    
    
    
    func requestPostAPI(requestData:[String:Any],apiName:String)  {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.post,strURL,parameters: requestData,headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                Helper.hideProgressIndicator()
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
            
                
            }, onError: { (error) in
                Helper.hideProgressIndicator()
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
    
}
