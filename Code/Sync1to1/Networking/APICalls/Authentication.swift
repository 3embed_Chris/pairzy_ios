//
//  Authentication.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 21/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import RxSwift
import RxAlamofire

class Authentication: NSObject {
    
    let disposeBag = DisposeBag()
    let subject_response = PublishSubject<ResponseModel>()
    
    func getCloudinaryCredential(){
        self.requestGetAPI(apiName: APINAMES.getCloudinaryDetails)
    }
    
    func getVersion(){
        let fullAPIName = APINAMES.getVersion + "2"
        self.requestGetAPI(apiName: fullAPIName)
    }
    
    //Email Login API
    func updateNewPassword(newPasswordData:RequestModel){
        self.requestAPI(requestData: newPasswordData,apiName: APINAMES.newPassword)
    }
    
    func postVerifyOTPForEmailLogin(otpDetails: RequestModel){
        self.requestAPI(requestData:otpDetails,apiName: APINAMES.verifyOTP)
    }
    
    func patchEmailIdExistsVerificaiton(emailId: [String:Any]){
        self.patchAPI(apiName: APINAMES.emailIdExistsVerificaiton, requestDetails: emailId)
    }
    
    func loginWithEmail(emailDetails:RequestModel){
        self.requestAPI(requestData:emailDetails,apiName: APINAMES.emailLogin)
    }
    
    
    func forgotPassword(emailId: RequestModel){
        
        self.requestAPI(requestData: emailId,apiName: APINAMES.forgotPassword)
    }
    
    
    func requestToPostProfile(profileDetail: RequestModel){
        self.requestAPI(requestData: profileDetail, apiName: APINAMES.profile)
    }
    
    
    func requestToPatchProfile(profileDetail: RequestModel){
        self.requestAPI(requestData: profileDetail, apiName: APINAMES.profile)
    }
    
    func requestToPatchMobileNumber(profileDetail: [String:Any]){
        self.patchAPI(apiName: APINAMES.MobileNumber, requestDetails: profileDetail)
    }
    
    
    func requestLogin(loginData:RequestModel) {
        self.requestAPI(requestData:loginData , apiName:APINAMES.LOGIN)
    }
    
    func requestSignUp(signupData:RequestModel)  {
         self.requestAPI(requestData:signupData , apiName:APINAMES.SIGNUP)
    }
    
    func requestEmailCheck(emailData:RequestModel)  {
     self.patchAPI(apiName:APINAMES.emailIdExists,requestDetails:emailData.ApiRequesDetails)
    }
    
    func requestPhoneNumberCheck(phoneData:RequestModel)  {            //done
      self.patchAPI(apiName:APINAMES.phoneNumberExists,requestDetails:phoneData.ApiRequesDetails)
    }
    
    func requestVerificationCode(verificationCodeData:RequestModel) {
        self.requestAPI(requestData:verificationCodeData , apiName:APINAMES.VERIFYOTP)
    }
    
    func requestForOtp(verificationCodeData:RequestModel) {
        self.requestAPI(requestData:verificationCodeData , apiName:APINAMES.RequestOtp)
    }
    
    func requestSetNewPassword(newPasswordDetails:RequestModel) {
        self.requestAPI(requestData:newPasswordDetails , apiName:APINAMES.RESETPASSWORD)
    }
    
    func requestForDeactivateAccount(reasonForDeactivation:String) {
        self.patchAPI(apiName:APINAMES.deactivateAccount,requestDetails:["reason":reasonForDeactivation])
    }
    
    func requestForGetAccountReasons() {
        self.requestGetAPI(apiName: APINAMES.deactivateReasons)
    }
    
    func requestForGetPreference(){
        self.requestGetAPI(apiName: APINAMES.Preferences)
    }
    
    
    func requestForGetFbId(apiName:String){
        self.requestGetAPI(apiName:apiName)
    }
    
    func requestForFaceBookLogin(userDetail:RequestModel){
        self.requestAPI(requestData: userDetail , apiName: APINAMES.FaceBookLogin)
        
    }
    func requestIsRegisteredFBId(fbId:String){
        let apiName = APINAMES.checkFbId + fbId
        self.requestGetAPI(apiName: apiName)
        
    }
    
    func patchPreferences(selectedPref:[String:Any]){
        self.patchAPI(apiName:APINAMES.Preferences,requestDetails: selectedPref)
        
    }

    func requestForIssueConatact(type:Int,details:RequestModel) {
        var nameOftheAPI = ""
        switch type {
        case 0:
          nameOftheAPI = APINAMES.reportAnIssue
            break
        case 1:
            nameOftheAPI = APINAMES.makeASuggestion
            break
        case 2:
            nameOftheAPI = APINAMES.askAQuestion
            break
        default: break
            //NONE OF THE ABOVE.
        }
        
        self.requestAPI(requestData:details , apiName:nameOftheAPI)
    }
    
    
    func requestForIRateApi(details:RequestModel) {
        self.requestAPI(requestData:details , apiName:APINAMES.postRatingForApp)
    }

    func requestResetPassword(resetPassDetails:RequestModel,byEmail:Bool) {
        if(byEmail) {
            self.requestAPI(requestData:resetPassDetails , apiName:APINAMES.RESETPASSWORDEMAIL)
        } else {
            self.requestAPI(requestData:resetPassDetails , apiName:APINAMES.RESETPASSWORDPHONE)
        }
    }

    
    func requestAPI(requestData:RequestModel,apiName:String)  {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
         
        }
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        RxAlamofire.requestJSON(.post,strURL,parameters:requestData.ApiRequesDetails,headers:headers)
            .debug()
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                Helper.hideProgressIndicator()
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                self.subject_response.onError(error)
                Helper.hideProgressIndicator()
            })
            
            .disposed(by: disposeBag)
        }
    
    func requestGetAPI(apiName:String) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.get,strURL,parameters:[:],headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                Helper.hideProgressIndicator()
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                        
                    }
                }
            }, onError: { (error) in
                self.subject_response.onError(error)
                 Helper.hideProgressIndicator()
            })
            
            .disposed(by: disposeBag)
    }
    
    func patchAPI(apiName:String,requestDetails:[String:Any])  {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
        // return
        }
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.patch,strURL,parameters:requestDetails,headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                           self.subject_response.onNext(response)
                        }
                        
                        
                         Helper.hideProgressIndicator()
                    }
                }
                
            }, onError: { (error) in
                self.subject_response.onError(error)
                
                Helper.hideProgressIndicator()
                
            })
            
            .disposed(by: disposeBag)
    }
}
