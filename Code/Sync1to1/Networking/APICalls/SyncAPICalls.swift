//
//  SyncAPICalls.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 13/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import RxSwift
import RxAlamofire

class SyncAPICalls: NSObject {
    
    let disposeBag = DisposeBag()
    let subject_response = PublishSubject<ResponseModel>()
    
    
    func requestToSetSyncDate(requestData:RequestModel) {
        self.requestPostAPI(requestData:requestData , apiName: APINAMES.setSyncDate)
    }
    
    func requestToSetDateStatus(requestData:RequestModel) {
        self.requestPostAPI(requestData:requestData , apiName: APINAMES.setDateStatus)
    }
    
    func requestToGetNowProfiles(offset: Int,limit: Int) {

        self.requestGetAPI(apiName: APINAMES.CurrentDates ,offset:offset, limit: limit)
    }
    
    func requestToGetFutureProfiles() {
        self.requestGetAPI(apiName: APINAMES.getFutureProfiles,offset: 0, limit: 20)
    }
    
    func requestForPastProfiles(offset: Int,limit: Int) {
        self.requestGetAPI(apiName: APINAMES.getPastDates,offset:offset, limit: limit)
    }
    
    func rateForVideoCall(requestData:RequestModel) {
        self.requestPostAPI(requestData: requestData, apiName: APINAMES.setRatingForVideoCall)
    }
    
    
    func requestForEndDate(requestData:RequestModel) {
        self.requestPostAPI(requestData: requestData, apiName: APINAMES.completedate)
    }
    
    func requestGetAPI(apiName:String,offset:Int,limit:Int) {
        
//        var urlComponents = NSURLComponents(string: "https://www.google.de/maps/")!
//
//        urlComponents.queryItems = [
//            NSURLQueryItem(name: "q", value: String(51.500833)+","+String(-0.141944)),
//            NSURLQueryItem(name: "z", value: String(6))
//        ]
//        urlComponents.URL // returns https://www.google.de/maps/?q=51.500833,-0.141944&z=6
     
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
       //added Query
        let strURLTo = SERVICE.BASE_URL + apiName          //+ apiextension + "/" + limit
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        var urlComponents = URLComponents(string: strURLTo)
        urlComponents?.queryItems = [URLQueryItem(name: "offset", value: String(offset)),URLQueryItem(name: "limit", value: String(limit))]
        let strURL  = urlComponents?.url
        RxAlamofire.requestJSON(.get,strURL!,parameters:[:],headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
    
    func requestPostAPI(requestData:RequestModel,apiName:String)  {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.post,strURL,parameters:requestData.ApiRequesDetails,headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                Helper.hideProgressIndicator()
                
            }, onError: { (error) in
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
}
