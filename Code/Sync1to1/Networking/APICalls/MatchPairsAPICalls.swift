//
//  MatcherAPICalls.swift
//  Datum
//
//  Created by Rahul Sharma on 11/09/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import RxSwift
import RxAlamofire

class MatchPairsAPICalls: NSObject {

    
    let disposeBag = DisposeBag()
    let subject_response = PublishSubject<ResponseModel>()
    
    func requestForMatchedPairs(offset:Int ,limit:Int) {
        self.requestGetAPI(apiName: APINAMES.matchedPairs, offset:offset, limit:limit)
    }
    
    ///Post
    func requestlikeAndUnlikePostMatchedPairsResponse(data: [String:Any]){
        self.requestPostlikeAndUnlikePostMatchedPairsResponse(requestData: data, apiName: APINAMES.userActions)
        
    }
    
    func requestForGetLeaderboard(offset:Int ,limit:Int) {
        self.requestGetAPI(apiName: APINAMES.leaderBoard, offset:offset, limit:limit)
    }
    /// handleFriendRequestResponse
    ///
//    /// - Parameter response: response
//    func handleFriendRequestResponse(response: ResponseModel){
//        guard let statusCode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode) else {
//            return
//        }
//        if statusCode == API.ErrorCode.Success {
//            if let profileResponse = response.response["data"] as? [[String:Any]] {
//                self.friendRequestListData  = profileResponse.map {
//                    Profile.fetchprofileDataObj(userDataDictionary: $0)
//                }
//            }
//            self.friendsVM_resp.onNext((.friendRequestResponse,true))
//        } else {
//            if let msg = response.response["message"] as? String {
//                self.message = msg
//            }
//            self.friendsVM_resp.onNext((.friendRequestResponse,false))
//        }
//    }
    
//
//    func requestGetAPI(apiName:String,offset:Int, limit:Int)  {
//        if NetworkReachabilityManager()?.isReachable == false {
//            Helper.hideProgressIndicator()
//            let Alert = alert()
//            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
//            return
//        }
//        let strURLTo = SERVICEPYTHON.BASE_URL + apiName
//        var urlComponents = URLComponents(string: strURLTo)
//        urlComponents?.queryItems = [URLQueryItem(name: "offset", value: String(offset)),URLQueryItem(name: "limit", value: String(limit))]
//        let strURL  = urlComponents?.url
//
//        func getAuthHeader(staticToken:Bool) -> [String:String] {
//            //var params: [String: String] = [ServiceInfo.language: "1"]
//            let location = Helper.getDeviceLocation()
//            let language = Helper.getSelectedLanguage()
//            var params: [String: String] = [ServiceInfo.language: language.code]
//            if(staticToken) {
//                params["authorization"] =  Keys.authorizationForLogin
//            } else {
//                params["authorization"] = UserLoginDataHelper.getUserToken()
//            }
//            params["city"] = location.city
//            return params
//        }
//
//        let headers = getAuthHeader(staticToken: false)
//
//
//        RxAlamofire.requestJSON(.get,strURL!,parameters:[:],headers:headers)
//
//            .debug()
//
//            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
//
//                if let dict = bodyResponse as? [String: AnyObject] {
//                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
//                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
//                        self.subject_response.onNext(response)
//                    }
//                }
//
//            }, onError: { (error) in
//                self.subject_response.onError(error)
//            })
//
//            .disposed(by: disposeBag)
//    }
//
    
    
    
    
    func
        requestGetAPI(apiName:String,offset:Int, limit:Int)  {
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        let strURLTo = SERVICEPYTHON.BASE_URL + apiName
        var urlComponents = URLComponents(string: strURLTo)
        urlComponents?.queryItems = [URLQueryItem(name: "offset", value: String(offset)),URLQueryItem(name: "limit", value: String(limit))]
        let strURL  = urlComponents?.url
        
        func getAuthHeader(staticToken:Bool) -> [String:String] {
            
            let language = Helper.getSelectedLanguage()
            var params: [String: String] = [ServiceInfo.language: language.code]
            if(staticToken) {
                params["authorization"] =  Keys.authorizationForLogin
            } else {
                params["authorization"] = UserLoginDataHelper.getUserToken()
            }
            //var params: [String: String] = [ServiceInfo.language: "1"]
//            let locDetails:[String:Any?] = Helper.getUserLocation()
//            var latitude = 0.0
//            var longitude = 0.0
//            let hasValue:String = locDetails["hasLocation"] as! String
            if Helper.getIsFeatureExist(){
                let featureLoc:Location =  Helper.getFeatureLocation()
                //            latitude = featureLoc.latitude
                //            longitude = featureLoc.longitude
                params["city"] = featureLoc.city
                params["latitude"] = String(featureLoc.latitude)
                params["longitude"] = String(featureLoc.longitude)
                
            }else {
                let location = Helper.getDeviceLocation()
                params["city"] = location.city
                params["latitude"] = String(location.latitude)
                params["longitude"] = String(location.longitude)
            }
            
            return params
        }
        
        let headers = getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.get,strURL!,parameters:[:],headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
    
    /// Returns Response For the Pending Friend request for User
    ///
    /// - Parameters:
    ///   - requestData: Takes Two Parameter 1: TargetUserId 2: Status Code: statusCode 1 for accept,2 for deny
    ///   - apiName: /friendRequestResponse
    func requestPostlikeAndUnlikePostMatchedPairsResponse(requestData:[String:Any],apiName:String)  {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        let strURL = SERVICEPYTHON.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.post,strURL,parameters: requestData,headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                Helper.hideProgressIndicator()
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                Helper.hideProgressIndicator()
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
    
    
}


struct SERVICEPYTHON {
    static let BASE_URL = "https://pyapi.pairzy.com/"
    
}
