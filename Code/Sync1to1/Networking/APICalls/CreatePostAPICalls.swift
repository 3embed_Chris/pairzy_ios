//
//  CreatePostAPICalls.swift
//  Datum
//
//  Created by Rahul Sharma on 31/05/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxAlamofire
import Alamofire


class CreatePostAPICalls: NSObject {
    
    let disposeBag = DisposeBag()
    let subject_response = PublishSubject<ResponseModel>()

    
    func requestToPostBio(postParams: [String:Any]){
       self.requestAPI(requestData: postParams, apiName: APINAMES.userPost)
    }
    
    func requestForGetPost(){
        self.requestGetAPI(apiName:APINAMES.userPost)
    }
    func requestForGetLikersName(postParams: String){
        self.requestGetAPI(apiName: APINAMES.userPostLike + postParams)
    }
    
    func requestForLikeAndUnlikePost(postParams: [String:Any]){
        self.requestAPI(requestData: postParams, apiName: APINAMES.userPostLikeUnlike)
    }
   
    func requestForCommentPost(postParams: [String:Any]){
        self.requestAPI(requestData: postParams, apiName: APINAMES.userPostComment)
    }
    func requestForGettingComment(postId: String){
        self.requestGetAPI(apiName: APINAMES.getuserPostComment + postId)
    }
    
    func deleteUserPost(requestData: [String:Any]) {
        self.requestPutAPI(requestData: requestData, apiName: APINAMES.userPost)
    }
    
    func requestToGetReportPostReasons() {
        self.requestGetAPI(apiName: APINAMES.getReportPostReasons)
    }
    
    func requestAPI(requestData:[String:Any],apiName:String)  {
        
     
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
        }
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        RxAlamofire.requestJSON(.post,strURL,parameters: requestData,headers:headers)
            .debug()
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                Helper.hideProgressIndicator()
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
            }, onError: { (error) in
                self.subject_response.onError(error)
                Helper.hideProgressIndicator()
            })
            
            .disposed(by: disposeBag)
    }
    
    
    
    
    func requestGetAPI(apiName:String) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.get,strURL,parameters:[:],headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }

    func requestPutAPI(requestData:[String:Any],apiName:String){
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.put,strURL,parameters:requestData,headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
   
}
