//
//  ProfileAPICalls.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 23/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxAlamofire
import Alamofire

class ProfileAPICalls: NSObject {
    let disposeBag = DisposeBag()
    let subject_response = PublishSubject<ResponseModel>()
    
    
    func requestForGetProfiles(offset:Int ,limit:Int) {
        self.requestGeWithOffsettAPI(apiName:APINAMES.getProfiles,offset:offset , limit:limit)
    }
    
    
    func requestForGetLikesByBoost() {
        requestGetAPI(apiName:APINAMES.boostLikes)
    }
    
    func requestForUnMatchProfile() {
        
    }
    
    func requestForGetProfle(){
        self.requestGetAPI(apiName:APINAMES.profile)
    }
    
    func requestLikeProfile(requestLikeProfile:RequestModel) {
        self.requestPostAPI(requestData: requestLikeProfile, apiName: APINAMES.likeProfile)
    }
    
    func requestDisLikeProfile(requestDisLikeProfile:RequestModel) {
        self.requestPostAPI(requestData: requestDisLikeProfile, apiName: APINAMES.disLikeProfile)
    }
    
    func requestSuperLikeProfile(requestDisLikeProfile:RequestModel) {
        self.requestPostAPI(requestData: requestDisLikeProfile, apiName: APINAMES.superLikeProfile)
    }
    
    
    func updateProfile(requestData: RequestModel) {
        
        self.requestPutAPI(requestData: requestData, apiName: APINAMES.profile)
        
    }
    
    
    func rewindProfiles(){
        self.requestPatchAPI(requestData: [:], apiName: APINAMES.rewind)
        
    }
    
    
    func boostProfile(){
        self.requestPatchAPI(requestData: [:], apiName: APINAMES.boost)
        
    }
    
    
    func requestGetAPI(apiName:String) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.get,strURL,parameters:[:],headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
    
    
    
    func requestGeWithOffsettAPI(apiName:String,offset:Int,limit:Int) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        let locDetails:[String:Any?] = Helper.getUserLocation()
        var latitude = 0.0
        var longitude = 0.0
        let hasValue:String = locDetails["hasLocation"] as! String
        
        if Helper.getIsFeatureExist(){
            let featureLoc:Location =  Helper.getFeatureLocation()
            latitude = featureLoc.latitude
            longitude = featureLoc.longitude
            
        }else {
            if(hasValue == "1") {
                if let  lat = locDetails["lat"] as? Double {
                     latitude = lat
                }
                if let long = locDetails["long"] as? Double {
                 longitude = long
                }
            }
            
        }
        let params = [ ServiceInfo.latitude: latitude,
                       ServiceInfo.longitude: longitude] as [String : Any]
     
      
        //added Query
        let strURLTo = SERVICE.BASE_URL + apiName          //+ apiextension + "/" + limit
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        var urlComponents = URLComponents(string: strURLTo)
        urlComponents?.queryItems = [URLQueryItem(name: "offset", value: String(offset)),URLQueryItem(name: "limit", value: String(limit))]
        let strURL  = urlComponents?.url
        
        RxAlamofire.requestJSON(.get,strURL!,parameters:[:],headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
    
    
    func requestPostAPI(requestData:RequestModel,apiName:String)  {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.post,strURL,parameters:requestData.ApiRequesDetails,headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
    
    
    
    func requestPutAPI(requestData:RequestModel,apiName:String){
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.put,strURL,parameters:requestData.ApiRequesDetails,headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
    
    
    
    func requestPatchAPI(requestData:[String:Any],apiName:String)  {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.patch,strURL,parameters:requestData,headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
    
    
}
