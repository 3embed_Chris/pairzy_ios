//
//  MatchPreferncesAPICalls.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 29/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import RxSwift
import RxAlamofire

class MatchPreferncesAPICalls: NSObject {
    let disposeBag = DisposeBag()
    let subject_response = PublishSubject<ResponseModel>()
    
    func requestGetMatchPrefernces() {
        self.requestGetAPI(apiName:APINAMES.Matchpreferences)
    }
    
    
    func requestAPI(requestData:RequestModel,apiName:String)  {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.post,strURL,parameters:requestData.ApiRequesDetails,headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
    
    
    func requestGetAPI(apiName:String) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        let strURL = SERVICE.BASE_URL + apiName
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        
        RxAlamofire.requestJSON(.get,strURL,parameters:[:],headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
}
