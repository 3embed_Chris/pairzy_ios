//
//  ProspectAPICalls.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 25/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxAlamofire
import Alamofire

class ProspectAPICalls: NSObject {
    
    let disposeBag = DisposeBag()
    let subject_response = PublishSubject<ResponseModel>()
    
    func requestForOnlineUsers(offset:Int,limit:Int) {
        self.requestGetAPI(apiName: APINAMES.OnlineUsers,offset:offset,limit:limit)
    }
    
    func requestForSuperLikedBy(offset:Int,limit:Int) {
        self.requestGetAPI(apiName: APINAMES.SupperLikesBy,offset:offset,limit:limit)
    }
    
    func requestForMySuperLikes(offset:Int,limit:Int){
        self.requestGetAPI(apiName: APINAMES.MySuperLikes,offset:offset,limit:limit)
    }
    
    func requestForLikesBy(offset:Int,limit:Int){
        self.requestGetAPI(apiName: APINAMES.LikesBy,offset:offset,limit:limit)
    }
    
    func requestForMyLikes(offset:Int,limit:Int){
        self.requestGetAPI(apiName: APINAMES.MyLikes,offset:offset,limit:limit)
    }

    func requestForRecentVisitors(offset:Int,limit:Int){
        self.requestGetAPI(apiName: APINAMES.RecentVisitores,offset:offset,limit:limit)
    }
    
    func requestForMyUnLikes(offset:Int,limit:Int){
        self.requestGetAPI(apiName: APINAMES.MyUnLikes,offset:offset,limit:limit)
    }
    
    
    func requestGetAPI(apiName:String,offset:Int,limit:Int) {
        
        
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hideProgressIndicator()
            let Alert = alert()
            Alert.msg(message: StringConstants.Network_Not_Reachable(), title: StringConstants.error())
            return
        }
        
        let strURLTo = SERVICE.BASE_URL + apiName          //+ apiextension + "/" + limit
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        
        var urlComponents = URLComponents(string: strURLTo)
        urlComponents?.queryItems = [URLQueryItem(name: "offset", value: String(offset)),URLQueryItem(name: "limit", value: String(limit))]
        let strURL  = urlComponents?.url
        
        RxAlamofire.requestJSON(.get,strURL!,parameters:[:],headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                Helper.hideProgressIndicator()
                
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if HeaderResponse.statusCode == 401 {
                            Helper.changeRootVcInVaildToken()
                        }else{
                            self.subject_response.onNext(response)
                        }
                    }
                }
                
            }, onError: { (error) in
                Helper.hideProgressIndicator()
                self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
    }
}
