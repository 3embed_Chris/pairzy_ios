//
//  AboutTableCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 02/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit




protocol AboutTableCellDelegate{
    
    func isTextChange()
}




class AboutTableCell: UITableViewCell,UITextViewDelegate {

    @IBOutlet weak var textViewCountLbael: UILabel!
    @IBOutlet weak var aboutTextView: KMPlaceholderTextView!
    var delegate: AboutTableCellDelegate?
    var aboutText = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        updateTextLength()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(textDidChange),
                                               name: NSNotification.Name.UITextViewTextDidChange,
                                               object: nil)
        aboutTextView.delegate = self
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count // for Swift use count(newText)
        let dbObj = Database.shared
        dbObj.userDataObj.aboutYou = newText
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        return numberOfChars < 251
    }
    
    @objc func textDidChange() {
        self.updateTextLength()
    }
    
    func updateTextLength() {
        self.textViewCountLbael.text = String(self.aboutTextView.text.count).appending("/250")
    }
    
    func updateCell(about: String) {
        aboutTextView.text = about
        aboutText = about
        }

    func textViewDidEndEditing(_ textView: UITextView) {
        if aboutText != textView.text{
            delegate?.isTextChange()
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }


    
}
