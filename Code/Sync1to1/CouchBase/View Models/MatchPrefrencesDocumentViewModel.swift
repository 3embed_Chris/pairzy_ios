//
//  MatchPrefrencesDocumentViewModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 06/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class MatchPrefrencesDocumentViewModel: NSObject {
    var couchbase = Couchbase.sharedInstance
    
    init(couchbase: Couchbase) {
        self.couchbase = couchbase
    }
    
    func saveMatchesDocId(matchDocId:String) {
        
        // Userdefaults helps to store session data locally
        let defaults = UserDefaults.standard
        defaults.set(matchDocId, forKey:  "userPrefData/")
        
        defaults.synchronize()
    }
    
    func createMatchesDocument(withMatchesData matchesData: [Any]?) -> String? {
        guard let matchesData = matchesData else { return nil}
        var matches = [Any]()
        for contact in matchesData {
            let matchDict = self.getMatchPrefObject(fromData: contact as! MatchPrefernceModel)
            matches.append(matchDict)
        }
        let matchDocID = couchbase.createDocument(withProperties: ["prefArray":matches])
        return matchDocID
    }
    
    func getMatchData(forDocId:String) -> [String:Any]? {
        let matchData = couchbase.getData(fromDocID: forDocId)
        return matchData
    }
    
    func updateMatchDocument(withMatchDocumentId docId:String, newData: [Any]?){
        guard let matchesData = newData else { return }
        var matches = [Any]()
        for contact in matchesData {
            let matchDict = self.getMatchPrefObject(fromData: contact as! MatchPrefernceModel)
            matches.append(matchDict)
        }
        couchbase.updateData(data: ["prefArray":matches], toDocID: docId)
    }
    
    func removeDataFromDocument(withMatchDocumentId docId:String) {
        couchbase.deleteDocument(withDocID: docId)
    }
    
    fileprivate func getMatchPrefObject(fromData data:MatchPrefernceModel) -> Any {
        var optionalValues = [Any]()
        if(data.optionsForMenu.count > 0) {
            optionalValues = data.optionsForMenu
        } else {
            optionalValues = data.optinalUnits
        }
        
        var selctedValues = [Any]()
        if(data.selectedValuesForMenu.count > 0) {
            selctedValues = data.selectedValuesForMenu
        } else {
            selctedValues = data.selectedValues
        }
        
        
        var optionalUnits = [Any]()
        
        if(data.optinalUnits.count > 0){
            optionalUnits = data.optinalUnits
        }

        let params = [ prefernceDetails.OptionsValue:optionalValues,
                       prefernceDetails.selectedUnit:data.selectedUnit,
                       prefernceDetails.prefIcon:data.prefernceIcon,
                       prefernceDetails.prefId:data.prefernceId,
                       prefernceDetails.typeOfPreference:data.prefernceType,
                       prefernceDetails.PreferenceTitle:data.prefernceTitle,
                       prefernceDetails.selectedValue:selctedValues,
                       prefernceDetails.optionsUnits:optionalUnits
            ] as [String : Any]
        return params as Any
    }
}
