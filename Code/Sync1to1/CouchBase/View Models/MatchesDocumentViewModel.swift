//
//  MatchesDocumentViewModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 27/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class MatchesDocumentViewModel: NSObject {
    var couchbase = Couchbase.sharedInstance
    
    init(couchbase: Couchbase) {
        self.couchbase = couchbase
    }
    
    func saveMatchesDocId(matchDocId:String) {
        
        // Userdefaults helps to store data locally
        let defaults = UserDefaults.standard
        defaults.set(matchDocId, forKey:  "userMatchData/")
        defaults.synchronize()
    }
    
    func createMatchesDocument(withMatchesData matchesData: [Any]?) -> String? {
        guard let matchesData = matchesData else { return nil}
        var matches = [Any]()
        for contact in matchesData {
            let matchDict = self.getMatchObject(fromData: contact as! [String : Any])
            matches.append(matchDict)
        }
        let matchDocID = couchbase.createDocument(withProperties: ["matchesArray":matches])
        return matchDocID
    }
    
    func getMatchData(forDocId:String) -> [String:Any]? {
        let matchData = couchbase.getData(fromDocID: forDocId)
        return matchData
    }

    func updateMatchDocument(withMatchDocumentId docId:String, newData: [Any]?){
        
            guard let matchesData = newData else { return }
            var matches = [Any]()
            for contact in matchesData {
                let matchDict = self.getMatchObject(fromData: contact as! [String : Any])
                matches.append(matchDict)
            }
        couchbase.updateDataForMatches(data: ["matchesArray":matches], toDocID: docId)
    }
    
    func removeDataFromDocument(withMatchDocumentId docId:String) {
        couchbase.deleteDocument(withDocID: docId)
    }
    
    
    fileprivate func getMatchObject(fromData data:[String:Any]) -> Any {
        let params = [
                       "messageId":data["messageId"],
                       "totalUnread":data["totalUnread"],
                       "messageType":data["messageType"],
                       "isBlocked":data["isBlocked"],
                       "onlineStatus":data["onlineStatus"],
                       "receiverIdentifier":data["receiverIdentifier"],
                       "receiverId":data["receiverId"],
                       "payload":data["payload"],
                       "timestamp":data["timestamp"],
                       "initiated":data["initiated"],
                       "isSupperLikedMe":data["isSupperLikedMe"],
                       "chatId":data["chatId"],
                       "recipientId":data["recipientId"],
                       "isMatchedUser":data["isMatchedUser"],
                       "firstName":data["firstName"],
                       "profilePic":data["profilePic"],
                       "chatInitiatedBy":data["chatInitiatedBy"]
                      ]
        return params as Any
    }
    
    
//    profileData.Fname:data[profileData.Fname],
//    profileData.profilePic:data[profileData.profilePic],
//    profileData.opponentId:data[profileData.opponentId],
//    profileData.Email:data[profileData.Email],
//    profileData.DateOfBirth:data[profileData.DateOfBirth],
//    profileData.Height:data[profileData.Height],
//    profileData.profileVideo:data[profileData.profileVideo],
//    profileData.Gender:data[profileData.Gender],
//    profileData.distance:data[profileData.distance],
//    profileData.isSupperLikedMe:data[profileData.isSupperLikedMe],
//    profileData.isMatched:data[profileData.isMatched],
}
