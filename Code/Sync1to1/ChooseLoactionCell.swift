//
//  ChooseLoactionCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 01/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class ChooseLoactionCell: UITableViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var mainTitle: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var starIcon: UIImageView!
    @IBOutlet weak var doller: UILabel!
    @IBOutlet weak var ratingDistView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
   // func updateCell(data: ChooseLocationModel){
    func updateCell(data: Location){
        self.mainTitle.text = data.name
        self.subTitle.text = data.fullText
        
        self.distanceLabel.text = data.distance + StringConstants.km()
       self.doller.text = "$$$"
        
        
    }
    func updateAddressCell(data: Location){
      
        self.distanceLabel.text = ""
        self.doller.text = ""
        if data.secondaryText.count == 0 {
            self.mainTitle.text = data.fullText
        }else {
            self.mainTitle.text  = data.secondaryText
        }
        
        
        subTitle.text = data.locationDescription
        if data.locationDescription.sorted().count == 0 {
            subTitle.text = data.fullText
        }
//        if data.fullText.count < 13 {
//            currentAddress.text = location.fullText
//
//            if location.secondaryText.count == 0 {
//                myCurrentLocLabel.text = location.state + ", " + location.country
//            }else {
//                myCurrentLocLabel.text = location.secondaryText
//            }
//        }
        
        
    
    }
    
}
