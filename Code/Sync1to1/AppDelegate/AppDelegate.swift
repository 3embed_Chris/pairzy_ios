

//
//  AppDelegate.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 01/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

//com.chaseking.sync1to1

import UIKit
import CoreData
import Firebase
import NotificationCenter
import UserNotifications
import CallKit
import PushKit
import CocoaLumberjack
import RxAlamofire
import SwiftyJSON
import StoreKit
import CoreLocation
import Fabric
import Crashlytics
import RxSwift
import GoogleMobileAds
import GoogleMaps
import ReachabilitySwift
import RxReachability
import AVKit
import Kingfisher
import AdSupport
import TapjoyAdapter
import KRProgressHUD


struct appDelegetConstant {
    static let incomingViewController = "IncomingCallViewController"
    static let window = UIApplication.shared.keyWindow!
}

@UIApplicationMain

class AppDelegate: UIResponder{

    var callProviderDelegate: CallProviderDelegate?
    var window: UIWindow?
   // var p:TJPlacement!
    let locationManager = CLLocationManager()
    let disposeBag = DisposeBag()
    var dictionary:[String:Any] = [:]
    let reachability = Reachability()
    var isNetworkThere : Bool = true
    var chatVcObject : ChatViewController? = nil
    var isUserWatchedFullVideo = false
    var latestVersion = ""
   // var transactionObserver: SKPaymentTransactionObserver!
    
    fileprivate let mqttManagerInstance = MQTTChatManager.sharedInstance
    
    
    /// method for redirecting to tabbar or login/signup page.
    ///checking if user logged in or not.
    ///if login is there redirecting to TabBarController otherwise to landing page.
    //By changing windows rootviewcontroller.
    func redirectingStoryBoard(){
        let userData = Database.shared.fetchResults()
        if !userData.isEmpty && UserLoginDataHelper.hasToken() {
            if Helper.getMQTTID() != nil {
                if (userData.first?.profilePictureURL?.count)! < 5  {
                    makeLoginAsRootVc()
                } else {
                    makeHomeAsRootVc()
                    
                }
            } else {
            
                makeLoginAsRootVc()
            }
        }
        else {
            makeLoginAsRootVc()
        }
    }
    
    enum Params: String {
        case link = "Link Value"
        case source = "Source"
        case medium = "Medium"
        case campaign = "Campaign"
        case term = "Term"
        case content = "Content"
        case bundleID = "App Bundle ID"
        case fallbackURL = "Fallback URL"
        case minimumAppVersion = "Minimum App Version"
        case customScheme = "Custom Scheme"
        case iPadBundleID = "iPad Bundle ID"
        case iPadFallbackURL = "iPad Fallback URL"
        case appStoreID = "AppStore ID"
        case affiliateToken = "Affiliate Token"
        case campaignToken = "Campaign Token"
        case providerToken = "Provider Token"
        case packageName = "Package Name"
        case androidFallbackURL = "Android Fallback URL"
        case minimumVersion = "Minimum Version"
        case title = "Title"
        case descriptionText = "Description Text"
        case imageURL = "Image URL"
        case otherFallbackURL = "Other Platform Fallback URL"
    }
    
    
    //MARK: - Google MAP
    func initMap() {
        GMSServices.provideAPIKey(GoogleKeys.GoogleMapKey)
      //  GMSPlacesClient.provideAPIKey(GoogleKeys.GoogleMapKey)
        let manager = LocationManager.shared
        manager.start()
    }
    
    
    /// making rootviewcontroller as login.
    func makeLoginAsRootVc() {
        
        if (Helper.getMQTTID() != nil) {
            UserLoginDataHelper.removeUserToken()
            Database.shared.deleteUserData()
            
            removeDataFromCouch()
        }
        
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        self.window?.rootViewController = storyBoard.instantiateViewController(withIdentifier: "navController")
    }

    //removing matches data from database.
    func removeDataFromCouch() {
        
        let matchesDocumentVm = MatchesDocumentViewModel(couchbase: Couchbase())
        guard  let docId = UserDefaults.standard.object(forKey:"userMatchData/") as? String else {return}
        matchesDocumentVm.removeDataFromDocument(withMatchDocumentId: docId)
        UserDefaults.standard.removeObject(forKey: "userMatchData/")
        UserDefaults.standard.synchronize()
    }
    
    /// making rootviewcontroller as tabbar.
    func makeHomeAsRootVc() {
        
        
        //connecting to MQTT if user logged in already.
        // getMQTTID returns the userID of logged in user.
        //if userid is availabel then connecting to MQTT.
        if let userID = Helper.getMQTTID() {
            MQTT.sharedInstance.connect(withClientId: userID)
        }
        
        self.checkForUpdateLocation()
        
        self.createDynamicLinkWithMetaTags()
        
        let storyBoard = UIStoryboard.init(name: "DatumTabBarControllers", bundle: nil)
        self.window?.rootViewController = storyBoard.instantiateViewController(withIdentifier: "TabBarController")
    }
    
    func oneSkySetup(){
        OneSkyOTAPlugin.provideAPIKey(OneSky.PublicKey, apiSecret: OneSky.SecretKey, projectID: OneSky.AppId)
        let language = Helper.getSelectedLanguage()
        OneSkyOTAPlugin.setDebug(true)
        OneSkyOTAPlugin.setLanguage(language.code)
        OneSkyOTAPlugin.checkForUpdate()
    }
    
    //MARK: FCM TOKEN REFRESH
    
    /// method will trigger if fcm token updates.
    ///
    /// - Parameter notification: notification gives new fcm token details.
   @objc func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = InstanceID.instanceID().token() {
            UserDefaults.standard.set(refreshedToken, forKey: SyUserdefaultKeys.kNSUPushNotificationTokenKey)
        }
    }
    
    func subscribeForPushNotificatiionTopic() {
        if let userID = Helper.getMQTTID() {
            var topicName = "/topics/" + userID
            Messaging.messaging().subscribe(toTopic: topicName)
            topicName = "Message/" + userID //chat messages
            Messaging.messaging().subscribe(toTopic: topicName)
        }
    }
    
    func unsubscribeForPushNotificatiionTopic() {
        if let userID = Helper.getMQTTID() {
            var topicName = "/topics/" + userID
            Messaging.messaging().unsubscribe(fromTopic: topicName)
            topicName = "Message/" + userID //chat messages
            Messaging.messaging().unsubscribe(fromTopic: topicName)
        }
    }

    
    /// customizing nav bar tint color
    func configurenavitionBar() {
        UINavigationBar.appearance().tintColor = UIColor.init(red: (0.0/255.0), green: (171.0/255.0), blue:(249.0/255.0), alpha: 1)
    }
    
    
    // MARK: - Core Data Saving support
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Sync1to1")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                //fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    //MARK: - VIDEO  CALL
    
    func showIncomingCallingScreen(callData:[String:Any]) {
        let window = UIApplication.shared.keyWindow!
        
        //if UIApplication.shared.applicationState == .active{
            
            if CallTypes.audioCall == callData["callType"] as! String {
                
                let audioMediaType = AVMediaType.audio
                let audioAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: audioMediaType)
                switch audioAuthorizationStatus {
                case .authorized: break
                case .denied :
                     Helper.showAlertWithMessageOnwindow(withTitle:"Oops" , message:AppConstant.audioPermissionMsg)
                case .notDetermined:
                    AVCaptureDevice.requestAccess(for:AVMediaType.audio, completionHandler: { isGranted in
                        if isGranted == true {
                        } else {
                            Helper.showAlertWithMessageOnwindow(withTitle:"Oops", message: AppConstant.audioPermissionMsg)
                        }})
                case .restricted: break
                }
                
                
                //show incoming audioview
                let incomingAudioView = IncomingAudiocallView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
                incomingAudioView.tag = 16
                incomingAudioView.playSound("ringtone", loop: 10)
                
                var name = ""
                if let callerName = callData["callerName"] as? String{name  = callerName} else{name  = "" }
                incomingAudioView.userName.text = name
                incomingAudioView.setCallId(messageData: callData)
                incomingAudioView.userImageView.kf.setImage(with: URL(string:callData["callerImage"] as! String ), placeholder: #imageLiteral(resourceName: "02m"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
                })
                
                if chatVcObject != nil {
                    incomingAudioView.chatViewObj = chatVcObject
                    chatVcObject?.inputToolbar.isHidden = true
                    chatVcObject?.inputToolbar.contentView?.textView?.resignFirstResponder()
                }
                window.addSubview(incomingAudioView);
            }
            else{
                
                //show incoming videoView
                let incomingVideoview = IncomingVideocallView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
                incomingVideoview.tag = 17
                incomingVideoview.playSound("ringtone", loop: 10)
                
                var name = ""
                if let callerName = callData["callerName"] as? String{name  = callerName} else{name  = "" }
                
                incomingVideoview.userName.text = name
                incomingVideoview.addCameraView()
                incomingVideoview.incomingVidLbl.text = "ringing" + " " + "\(callData["callerIdentifier"] as! String)"
                incomingVideoview.setMessageData(messageData: callData)
                
                if chatVcObject != nil {
                    incomingVideoview.chatViewObj = chatVcObject
                    chatVcObject?.inputToolbar.contentView?.textView?.resignFirstResponder()
                    chatVcObject?.inputToolbar.isHidden = true
                }
                window.addSubview(incomingVideoview)

            }

       // }
        
    }
    
    func setupDynamicLinkForApp() {
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = "shareProfile"
    }
    
    @objc func checkForPendingCalls(){
        
//        UserDefaults.standard.set(false, forKey: "iscallgoingOn")
//        UserDefaults.standard.synchronize()
        
        let strURL = SERVICE.BASE_URL + APINAMES.pendingCalls
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        RxAlamofire.requestJSON(.get,strURL,parameters:[:],headers:headers)
            .debug()
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        if (respDict["data"] != nil) {
                            //send busy status
                            guard let userID = Helper.getMQTTID() else { return }
                            
                            if  let isCallgoingOn = UserDefaults.standard.object(forKey: "iscallgoingOn") as? Bool {
                                if isCallgoingOn == false {
                                    UserDefaults.standard.set(true, forKey: "iscallgoingOn")
                                    UserDefaults.standard.synchronize()
                                    MQTTCallManager.sendcallAvilibilityStatus(status: 0, topic: MQTTTopic.callsAvailability + userID)
                                    let appdelete = UIApplication.shared.delegate as! AppDelegate
                                    appdelete.showIncomingCallingScreen(callData: respDict["data"] as! [String:Any])
                                }
                            }
                        }
                    }
                }
                
            }, onError: { (error) in
                
            })
        .disposed(by: disposeBag)
    }
    
    
    
    
    func redeemCoinsForVideoAd() {
        
        Helper.showProgressIndicator(withMessage:"Updating coins ...")
        
        let details = ["coinAmount":200]
        API.requestPOSTURL(serviceName: APINAMES.earnCoinsForVideoAd,
                           withStaticAccessToken:false,
                           params: details,
                           success: { (response) in
                            Helper.hideProgressIndicator()
                            if (response.null != nil){
                                return
                            }
                            
                            self.handleEarnCoinsApi(response: response.dictionaryObject!)
        },
                           failure: { (Error) in
                            Helper.hideProgressIndicator()
        })
    }

    /// handle handle Post Preferences API Response
    ///
    /// - Parameter response: respnse details.
    func handleEarnCoinsApi(response:[String:Any]) {
        let statuscode = response["code"] as! Int
        switch (statuscode) {
        case 200:
            
            if let data = response["closingBalance"] as? Int {
                Helper.saveCoinBalance(coinBalance:data)
                let notificationName = NSNotification.Name(rawValue: NSNotificationNames.notificationForRefreshCoins)
                NotificationCenter.default.post(name: notificationName, object: self, userInfo: nil)
                KRProgressHUD.dismiss() {
                    self.showNewCoinsPopUp()
//                    self.showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coin_add_popup"), mainTitle: StringConstants.congrats, subTitle: "Coins added to your wallet successfully!!.", buttnTitle: StringConstants.done, dontShowHide: true)
                }
            }
            
            Helper.hideProgressIndicator()
        case 401:
            Helper.changeRootVcInVaildToken()
            break
        default:
            Helper.hideProgressIndicator()
        }
    }

    
    func createDynamicLinkWithMetaTags() {
        let userData = Database.shared.fetchResults()
        if !userData.isEmpty
        {
            var longLink:URL?
            // general link params
            let linkString = "https://appapi.pairzy.com/documentation#!/searchResult/"
            var profileId:String = ""
            if let userDetails = userData.first {
                if let urlString:String = userDetails.userId {
                    profileId = urlString
                } }
            profileId = linkString.appending(profileId)
            guard let link = URL(string: profileId) else { return }
            let components = DynamicLinkComponents(link: link, domain:"pairzy.page.link")
            
            let bundleID = "com.pipelineAI.pairzy"
            // iOS params
            let iOSParams = DynamicLinkIOSParameters(bundleID: bundleID)
            iOSParams.fallbackURL = URL.init(string: "https://itunes.apple.com/us/app/datum-dating-and-chatting-app/id931927850?ls=1&mt=8")
            components.iOSParameters = iOSParams
            
            
            let androidParams = DynamicLinkAndroidParameters(packageName:"com.datum.com")
            androidParams.fallbackURL = URL.init(string:"https://play.google.com/store/apps/details?id=com.datum.com")
            components.androidParameters = androidParams
            
            // social tag params
            let socialParams = DynamicLinkSocialMetaTagParameters()
            socialParams.title = "Datum | Swipe.match.Chat."
            socialParams.descriptionText = "Pairzy - soon we will update our app descrption stay tuned guys!!"
            socialParams.imageURL = URL.init(string: "https://s3.amazonaws.com/sellr/file_1517929760.png")
            
            
            components.socialMetaTagParameters = socialParams
            
            longLink = components.url
            print(longLink?.absoluteString ?? "")
            
            components.shorten { (shortURL, warnings, error) in
                // Handle shortURL.
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                UserDefaults.standard.setValue(shortURL?.absoluteString, forKey:SyUserdefaultKeys.userDeepLink)
                UserDefaults.standard.synchronize()
                print(shortURL?.absoluteString ?? "")
            }
        }
    }
    
    
    func reachabilityChanged(_ isReachability: Bool) {
        
        if isReachability == true {
            let mqttModel = MQTT.sharedInstance
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                mqttModel.createConnection()
            }
            isNetworkThere = true
            DDLogDebug("Network is rechable now")
        } else {
            DDLogDebug("Network not reachable")
            isNetworkThere = false
            Helper.showAlertWithMessageOnwindow(withTitle:"Oops" , message:"Check your internet connection")
        }
        
    }
}

  //MARK:-  APPLICATION DELEGATE METHODS

extension AppDelegate:UIApplicationDelegate {
  
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if UserDefaults.standard.value(forKey: "FirstLaunchFlag") != nil {
            
            ///if false make it true
            if !(UserDefaults.standard.value(forKey: "FirstLaunchFlag") != nil){
                UserDefaults.standard.set(true, forKey:"FirstLaunchFlag")
                UserDefaults.standard.synchronize()
            }
        }else {
            //set True
            //add this key and set true
            UserDefaults.standard.set(true, forKey:"FirstLaunchFlag")
            UserDefaults.standard.synchronize()
            
        }
        
        UserDefaults.standard.set(false, forKey: "iscallgoingOn")
        UserDefaults.standard.synchronize()
        
        Helper.updateChatOpenStatus(isChatOpen:false)
        self.registerPushNotification()
        //DDLog For printing log.
        //instead of print for fast debugging.
        DDLog.add(DDASLLogger.sharedInstance)
        DDLog.add(DDTTYLogger.sharedInstance)
        self.initMap()
        
        Helper.hidePushNotifications(hidePush: false)
        
        oneSkySetup()
        SKPaymentQueue.default().add(AppStoreKitHelper())

        // configuring  firebase for push notifiations.
        FirebaseApp.configure()
        API.createDynamicUrl(success: { (response) in
                            if (response.null != nil){
                                return
                            }
                             print(response)
                            },
                           failure: { (Error) in
                            print(Error)
        })
        
        //adding timer for location update. location will update for every 1 minute
        _ = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector:#selector(checkForUpdateLocation), userInfo:nil, repeats:true)
        
        //adding timer for online status update. online status will update for every 2 minutes
         _ = Timer.scheduledTimer(timeInterval: 2*60.0, target: self, selector:#selector(updateOnlineStatusOfUser), userInfo:nil, repeats:true)
        
        //registering for push notifications.
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self as UNUserNotificationCenterDelegate
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        //registering for apple push notifications.
        application.registerForRemoteNotifications()
  
       //observer for network.
        Reachability.rx.isReachable
            .subscribe(onNext: { isReachable in
                if isReachable == true {self.reachabilityChanged(isReachable)}
                else{self.reachabilityChanged(isReachable)}
            }).addDisposableTo(disposeBag)
        do{
            try reachability?.startNotifier()
        }catch{
            DDLogDebug("could not start reachability notifier")
        }
        
        //fabric for crashlytics
        //notifying if any crashes while app is in distrubution.
        Fabric.with([Crashlytics.self])
 
        //setting whole app navigation bar in same color - Datum base color.
        self.configurenavitionBar()
        
        // Ask for Authorisation from the User.
        // Location For use in foreground.
        //fetching  user location details.
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        //FCM DELEGATES.
        Messaging.messaging().delegate = self
        
        //adding observer for token refresh.
        //tokenRefreshNotification is the metod to get updated token.
        //tokenRefreshNotification will trigger when fcm token updates.
        NotificationCenter.default.addObserver(self, selector:
            #selector(tokenRefreshNotification), name:
            NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        
       // redirecting to landing screen or tabbar based on condition.
        self.redirectingStoryBoard()
//        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
//        self.window?.rootViewController = storyBoard.instantiateViewController(withIdentifier: "splashBoard")
//
        
        //FETCHING IP details ofthe user.
        //it will helpful when user disable the location or if app unable to fetch users location.
        Helper.fetchIpDetails()
        
        
        // Initialize the Google Mobile Ads SDK.
        GADMobileAds.configure(withApplicationID: Admob.appId)
        GADRewardBasedVideoAd.sharedInstance().delegate = self

        //setUp for dynamic liking by firebase.
        self.setupDynamicLinkForApp()
        
       // requesting new ad from admob.
      // ads we are using for user to earn coins by watching videos.
       requestForAddFromAdmob()
        
        return true
    }
    
    @objc func updateOnlineStatusOfUser() {
        if UIApplication.shared.applicationState == .active{
            mqttManagerInstance.sendOnlineStatus(withOfflineStatus: false)
        }
    }
    
    
    func requestForAddFromAdmob() {
        let request = GADRequest()
        let extras = GADMTapjoyExtras()
        extras.debugEnabled = false
        request.register(extras)
        
        Tapjoy.connect(TapJoy.appId)
        
        
        guard let p:TJPlacement = TJPlacement.placement(withName:"AppOpen", delegate: self ) as? TJPlacement else {  return  }
        p.requestContent()
        GADRewardBasedVideoAd.sharedInstance().load(request,
                                                    withAdUnitID: Admob.rewardVideoUnitId)
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .failed:
                queue.finishTransaction(transaction)
                print("Transaction Failed \(transaction)")
            case .purchased, .restored:
                queue.finishTransaction(transaction)
                print("Transaction purchased or restored: \(transaction)")
            case .deferred, .purchasing:
                print("Transaction in progress: \(transaction)")
            }
        }
    }
    
    func openNewVideoAdd() -> Bool {
        if GADRewardBasedVideoAd.sharedInstance().isReady {
            if let controller = self.window?.rootViewController {
                GADRewardBasedVideoAd.sharedInstance().present(fromRootViewController: controller)
                return true
            }
        }
        return false
    }
    
    func newAdAvailable() -> Bool {
        if GADRewardBasedVideoAd.sharedInstance().isReady {
             return true
        }
        return false
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        mqttManagerInstance.sendOnlineStatus(withOfflineStatus: true)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        mqttManagerInstance.sendOnlineStatus(withOfflineStatus: true)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        mqttManagerInstance.sendOnlineStatus(withOfflineStatus: false)
        checkForPendingCalls()
        checkAppVersion()
        OneSkyOTAPlugin.checkForUpdate()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        mqttManagerInstance.sendOnlineStatus(withOfflineStatus: false)
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        //  self.saveContext()
        
        UserDefaults.standard.set(false, forKey: "iscallgoingOn")
        UserDefaults.standard.synchronize()
        
        if let userID = Helper.getMQTTID(){
            MQTTCallManager.sendcallAvilibilityStatus(status: 1, topic: MQTTTopic.callsAvailability + userID)
        }
        mqttManagerInstance.sendOnlineStatus(withOfflineStatus: true)
    }
    
}

 //MARK: - CLLOCATION MANAGER DELEGATE.

extension AppDelegate:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        let userLocationDetails = ["lat":(locValue.latitude),
                                   "long":(locValue.longitude)]
        Helper.saveUserLocation(userLocationDetails: userLocationDetails)
        
    }
    
    
    
    @objc func checkForUpdateLocation() {
        let userData = Database.shared.fetchResults()
        
        if !userData.isEmpty && UserLoginDataHelper.hasToken() {
            if Helper.getMQTTID() != nil {
                if (userData.first?.profilePictureURL?.count)! < 5  {
                    // no profile picture so logout.
                } else {
                    self.updateUserLocation()
                }
            }
        }
    }
    
    func updateUserLocation() {
        
        
        let locDetails:[String:Any?] = Helper.getUserLocation()
        
        var localTimeZoneName: String { return TimeZone.current.identifier }


        var latitude = 0.0
        var longitude = 0.0
        var isPassportUser = false
        var featAddress = ""

        if Platform.isSimulator {
            print("Running on Simulator")
            latitude = 13.028694
            longitude = 77.589564
        } else {
            let hasValue:String = locDetails["hasLocation"] as! String
            
            if Helper.getIsFeatureExist(){
                
                let featureLoc:Location =  Helper.getFeatureLocation()
                featAddress =  featureLoc.city + ", " + featureLoc.state + ", " + featureLoc.country
                latitude = featureLoc.latitude
                longitude = featureLoc.longitude
                if featAddress.count < 10 {
                    featAddress = featureLoc.locationDescription
                    
                    if featAddress.count < 10 {
                        featAddress = featureLoc.secondaryText
                    }
                }
                
                isPassportUser = true
            }else {
                if let lat = locDetails["lat"] as? Double {
                    latitude = lat
                }
                if let long = locDetails["long"] as? Double {
                    longitude = long
                }
                let location = Utility.getCurrentAddress()
                featAddress = location.city + ", " + location.state + ", " + location.country
                isPassportUser = false
            }
        }
        guard featAddress.count > 10 else {
            return
            
        }
        
        let userIp = Helper.getUserIP()
        
        let params =    [
            ServiceInfo.iPAddress:userIp,
            ServiceInfo.latitude            :latitude,
            ServiceInfo.longitude           :longitude,
            ServiceInfo.timeZone            :localTimeZoneName,
            ServiceInfo.isPassportLocation : isPassportUser,
            "address": featAddress] as [String: AnyObject]
        
        API.requestPatchURL(serviceName: APINAMES.updateLocation,
                            withStaticAccessToken:false,
                            params: params,
                            success: { (response) in
        },
                            failure: { (Error) in
                                
        })
    }
}

//MARK: - Admob ADS DELEGATE.

extension AppDelegate:GADRewardBasedVideoAdDelegate {
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didRewardUserWith reward: GADAdReward) {
        print("Reward received with currency: \(reward.type), amount \(reward.amount).")
        isUserWatchedFullVideo = true
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd:GADRewardBasedVideoAd) {
        print("Reward based video ad is received.")
        sendUpdateForAdAvailable()
    }
    
    func rewardBasedVideoAdDidOpen(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Opened reward based video ad.")
    }
    
    func rewardBasedVideoAdDidStartPlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad started playing.")
    }
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad is closed.")
       checkForAddCoins()
    }
    
    func showNewCoinsPopUp(){
        let window = UIApplication.shared.keyWindow!
        let coinsView = RewardPopup(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        coinsView.loadPopUpView()
        window.addSubview(coinsView)
    }
    
    func checkForAddCoins() {
        if(isUserWatchedFullVideo) {
            isUserWatchedFullVideo = false
            self.redeemCoinsForVideoAd()
        }
        self.requestForAddFromAdmob()
        sendUpdateForAdAvailable()
    }
    
    func sendUpdateForAdAvailable() {
        //to refresh button state
        let notificationName = NSNotification.Name(rawValue:NSNotificationNames.notificationForNewADAvailable)
        NotificationCenter.default.post(name: notificationName, object: self, userInfo:nil)
    }
    
    func showCoinsPopupForChat(icon: UIImage, mainTitle: String, subTitle: String, buttnTitle: String, dontShowHide: Bool){
        let window = UIApplication.shared.keyWindow!
        let coinsView = spendSomeCoinPopUp(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        coinsView.loadPopUpView()
        coinsView.updatePopup(icon: icon, mainTitle: mainTitle, subTitle: subTitle, buttnTitle: buttnTitle, dontShowHide: dontShowHide)
        window.addSubview(coinsView)
    }
    
    func rewardBasedVideoAdWillLeaveApplication(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad will leave application.")
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didFailToLoadWithError error: Error) {
        print("Reward based video ad failed to load.")
        self.requestForAddFromAdmob()
    }
}

//MARK: - PUSH NOTIFICATION DELEGATE.

extension AppDelegate : UNUserNotificationCenterDelegate{
    
    //UnUsernotification delegates
    //for displaying notification when app is in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void){
        //willPresent notification callled
    
        //If you don't want to show notification when app is open, do something here else and make a return here.
        //Even you you don't implement this delegate method, you will not see the notification on the specified controller. So, you have to implement this delegate and make sure the below line execute. i.e. completionHandler.
        
        let userInfo = notification.request.content.userInfo as! [String: Any]
        
        if let notificationType = userInfo["type"] as?  String {
            if(notificationType == "1" || notificationType == "2") {
                print("both interested in each other")
                let notificationName = NSNotification.Name(rawValue:NSNotificationNames.notificationForRefreshMatchMakerTab)
                NotificationCenter.default.post(name: notificationName, object: self, userInfo:nil)
                
            } else if(notificationType == "7" || notificationType == "8" || notificationType == "9" || notificationType == "10") {
                // 7  - Sameer would like to schedule a sync date with you. Please confirm, reschedule or reject.
                //method triggering for refresh future tab automatically in sync dates.
                let notificationName = NSNotification.Name(rawValue:NSNotificationNames.notificationForRefreshFutureTab)
                NotificationCenter.default.post(name: notificationName, object: self, userInfo:["requestImmediately":0])
            }
            else if(notificationType == "30") {
                // 7  - Sameer would like to schedule a sync date with you. Please confirm, reschedule or reject.
                //method triggering for refresh future tab automatically in sync dates.
                let notificationName = NSNotification.Name(rawValue:NSNotificationNames.notificationForCampagin)
                NotificationCenter.default.post(name: notificationName, object: self, userInfo:userInfo)
            }
            else if(notificationType == "31") {
                // NewsFeed Update Notification
                let notificationName = NSNotification.Name(rawValue:NSNotificationNames.updateNewsFeedDetails)
                NotificationCenter.default.post(name: notificationName, object: self, userInfo:userInfo)
            }
            
            
            if(Helper.isHidePush()) {
                return
            }
            completionHandler([.alert, .badge, .sound])
        }
        
        if let notificationType = userInfo["gcm.notification.type"] as?  String {
            //chat message
            if(notificationType == "20") {
                
            }
            if(Helper.isHidePush()) {
                return
            }
            completionHandler([.alert, .badge, .sound])
        }
    }
    
    
    func handlePushNotification(notificationDetails:[String:Any]) {
        print(notificationDetails)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NSNotificationNames.notificationFortappedOnPushNotification), object:self, userInfo:notificationDetails)
    }
    
     // For handling tap and user actions
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void){
        //didreceive notification called
        
         //Called when a notification is delivered to a foreground app.
        let userInfo = response.notification.request.content.userInfo as! [String:Any]
        checkForSubscription(userInfo: userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        //didReceiveRemoteNotification: method
        handlePushNotification(notificationDetails: userInfo as! [String : Any])
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        print("app opened through deep link")
        return application(app, open: url,
                           sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                           annotation: "")
        
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if (DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url)) != nil {
            // Handle the deep link. For example, show the deep-linked content or
            // apply a promotional offer to the user's account.
            // ...
            
            print("app opened through deep link")
            return true
        }
        return false
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        
        print("app opened not through deep link ")

        if let incomingURL = userActivity.webpageURL {
            let handleLink = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL, completion: { (dynamicLink, error) in
                if let dynamicLink = dynamicLink, let _ = dynamicLink.url {
                    print("Your Dynamic Link parameter: \(dynamicLink.url!)")
                    let notificationName = NSNotification.Name(rawValue:NSNotificationNames.notificationForDynamicLink)
                    let userInfo = ["receivedLink":(dynamicLink.url!.absoluteString)]
                    NotificationCenter.default.post(name: notificationName, object: self, userInfo:userInfo)
                } else {
                    // Check for errors
                }
            })
            return handleLink
        }
        return false
    }
    
    //Application Notificationsss Methode
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //\n\ngot push notification \n\n\n
        Messaging.messaging().apnsToken = deviceToken
        subscribeForPushNotificatiionTopic()
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        UserDefaults.standard.setValue(deviceTokenString, forKey: "deviceTokenString")
        UserDefaults.standard.synchronize()
        print("Device token: \(deviceTokenString)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification notification: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //did ReceiveRemoteNotificationsssssss
    }
    
    func checkForSubscription(userInfo:[String:Any]){
         handlePushNotification(notificationDetails: userInfo)
    }
    
    func openInAppPurchaseView() {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = InAppPuchasePopupView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.loadInAppPurchaseView()
        window.addSubview(videoView)
    }
}

extension AppDelegate:MessagingDelegate {
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
    }
}

extension AppDelegate:TJPlacementDelegate {

}
extension AppDelegate {
    
    func checkAppVersion(){
        
        DispatchQueue.global().async {
            do {
                let update = try self.isUpdateAvailable()
                if update == true {
                    print("update version")
                    let apiCall = Authentication()
                    apiCall.getVersion()
                    apiCall.subject_response
                        .subscribe(onNext: {response in
                            self.handleGetVersionResponse(responseModel: response)
                        }, onError: {error in
                            
                        }).disposed(by:self.disposeBag)
                }else {
                    if UserDefaults.isFirstLaunch(){
                        print("First launch")
                        //Show alert to update version
                        self.alertForUpdateVersion()
                    } else {
                        print("Not first launch")
                    }
                }
            } catch {
                print(error)
            }
        }
    }
    
    func handleGetVersionResponse(responseModel: ResponseModel){
        
        let statuscode:API.ErrorCode = API.ErrorCode(rawValue: responseModel.statusCode)!
        
        switch statuscode {
        case .Success:
            if  let data = responseModel.response["data"] as? [String:Any] {
                guard let isManditory = data["isMandatory"] as? Bool else {
                    return
                    
                }
                if isManditory {
                    alertForUpgradeNow()
                }else{
                    if UserDefaults.isFirstLaunch(){
                        print("First launch")
                        //Show alert to update version
                        self.alertForUpdateVersion()
                    } else {
                        print("Not first launch")
                    }
                }
            }
        default:
            break
        }
        
    }
    
    
    func alertForUpgradeNow(){
        let appStoreUrl = "https://itunes.apple.com/us/app/datum-for-video-dating/id931927850?mt=8&uo=4"
        let msgTitle = StringConstants.updateAvailable()
        let msg = String(format: StringConstants.aNewVersionAvailable(), latestVersion)
        
        let alert = UIAlertController(title: msgTitle, message: msg, preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: StringConstants.upgradeNow(), style: UIAlertAction.Style.default, handler: { action in
            UIApplication.shared.openURL(URL(string: appStoreUrl)!)
        }))
        
        // show the alert
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
    
    func alertForUpdateVersion(){
        let appStoreUrl = "https://itunes.apple.com/us/app/datum-for-video-dating/id931927850?mt=8&uo=4"
        
        let msgTitle = StringConstants.updateAvailable()
        let msg = String(format: StringConstants.newVersionAvailable(),latestVersion)
        
        let alert = UIAlertController(title: msgTitle, message: msg, preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: StringConstants.update(), style: UIAlertAction.Style.default, handler: { action in
            UIApplication.shared.openURL(URL(string: appStoreUrl)!)
        }))
        alert.addAction(UIAlertAction(title: StringConstants.cancel(), style: UIAlertAction.Style.cancel, handler: nil))
        
        // show the alert
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
    
    
    func isUpdateAvailable() throws -> Bool {
        
        guard let BundleId = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String else {
            throw VersionError.invalidBundleInfo
        }
        let VersionAppStore = "http://itunes.apple.com/lookup?bundleId=\(BundleId)"
        
        guard let url = URL(string: VersionAppStore) else {
            throw VersionError.invalidBundleInfo
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            throw VersionError.invalidResponse
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            latestVersion = version
            let appVersion =  Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            return version != appVersion
        }
        throw VersionError.invalidResponse
    }
    
    
    enum VersionError: Error {
        case invalidResponse, invalidBundleInfo
    }
    
}

extension AppDelegate: PKPushRegistryDelegate{
    
    
    /// VoIP push registration
    func registerPushNotification() {
        
        let mainQueue = DispatchQueue.main
        // Create a push registry object
        let voipRegistry: PKPushRegistry = PKPushRegistry(queue: mainQueue)
        // Set the registry's delegate to self
        voipRegistry.delegate = self
        // Set the push type to VoIP
        voipRegistry.desiredPushTypes = [PKPushType.voIP]
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate credentials: PKPushCredentials, for type: PKPushType){
        
        let deviceTokenString = credentials.token.reduce("", {$0 + String(format: "%02X", $1)})
        DDLogDebug("ppush device toketn \(deviceTokenString)")
        UserDefaults.standard.setValue(deviceTokenString, forKey: "callPushToken")
        UserDefaults.standard.synchronize()
    }
    
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType){
        
        if UIApplication.shared.applicationState == .inactive || UIApplication.shared.applicationState == .background{
            guard type == .voIP else { return }
            guard let dict:[String:Any] = payload.dictionaryPayload["data"]! as? [String : Any] else { return }
            //let msgdict = dict["message"]! as! [String:Any]

            guard let uuidString = dict["callerIdentifier"] as? String else { return}
            guard let callID = dict["callId"] as? String else { return}

            callProviderDelegate = CallProviderDelegate.init()
            callProviderDelegate?.callID = callID
            callProviderDelegate?.messageData = dict
             // calleeName = self.getNameFormDatabase(num: uuidString)
            var callerName = ""
            if let calleeName = dict["callerName"] as? String {
                callerName = calleeName
            }
            if  let isCallgoingOn = UserDefaults.standard.object(forKey: "iscallgoingOn") as? Bool {
                if isCallgoingOn == false {
                    UserDefaults.standard.set(true, forKey: "iscallgoingOn")
                    UserDefaults.standard.synchronize()
                    
                    guard let userID = Helper.getMQTTID() else { return }
                    MQTTCallManager.sendcallAvilibilityStatus(status: 0, topic: MQTTTopic.callsAvailability + userID)
                    callProviderDelegate?.displayIncomingcall(uuid : UUID() , handel: callerName , hasVideo: false, complition: { (error) in
                        DDLogDebug("error calllleeeeeeeeeeeeeeee\(String(describing: error))")
                        if error != nil{
                            guard let userID = Helper.getMQTTID() else { return }
                            MQTTCallManager.sendcallAvilibilityStatus(status: 1, topic: MQTTTopic.callsAvailability + userID)
                            UserDefaults.standard.set(false, forKey: "iscallgoingOn")
                            UserDefaults.standard.synchronize()
                        }
                    })
                    
                }
            }

        }
    }
    
    
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType){
        
        DDLogDebug("didFail ......push")
    }
    
}
extension UserDefaults {
    
    static func isFirstLaunch() -> Bool {
        let firstLaunchFlag = "FirstLaunchFlag"
        
        if !UserDefaults.standard.bool(forKey: firstLaunchFlag) {
            UserDefaults.standard.set(true, forKey: firstLaunchFlag)
            // standardUserDefaults().synchronize()
            return true
        }
        return false
    }
    
}
