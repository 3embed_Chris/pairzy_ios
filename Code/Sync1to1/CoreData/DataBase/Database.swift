//
//  Database.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 12/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

final class Database{
    
    // Can't init is singleton
    private init() { }
    
    // MARK: Shared Instance
    
    static let shared = Database()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
 
    
    let instaVM = InstagramVM()
    
    // MARK: Local Variable
    
    var userDataObj:UserDetails!{
        let results = self.fetchResults()
        if results.count>0{
            return results[0]
        }
        else{
            return UserDetails(context: context)
        }
    }
    
    func fetchResults() -> [UserDetails]{
        do {
            if let userDetails = try context.fetch(UserDetails.fetchRequest()) as? [UserDetails]{
                return userDetails
            }
        } catch {
            
        }
        return []
    }
    
    
    func addUserdataToDatabase(withUserdata userdata :User){
        self.userDataObj.first_name = userdata.firstName
        self.userDataObj.countryCode = userdata.countryCode
        //        self.userDataObj.dateOfBirth = Helper.dateFromMilliseconds(timestamp:String(userdata.dateOfBirth))
        self.userDataObj.gender = userdata.gender
        self.userDataObj.heightInCM = userdata.heightInCM
        self.userDataObj.latitude = userdata.latitude
        self.userDataObj.longitude = userdata.longitude
        self.userDataObj.mobileNumber = userdata.mobileNumber
        self.userDataObj.otherPics = userdata.otherPics as NSObject
        self.userDataObj.otherVideos = userdata.otherVideos as NSObject
        self.userDataObj.preferred_sex = userdata.preferredSex
        self.userDataObj.userId = userdata.userId
        self.userDataObj.profileVideoUrl = userdata.profileVideo
        self.userDataObj.profilePictureURL = userdata.profilePicture
        self.userDataObj.dateOfBirthTimeStamp = userdata.dateOfBirthTimeStamp
        self.userDataObj.emailId = userdata.emailID
        self.userDataObj.aboutYou = userdata.aboutYou
        self.userDataObj.work = userdata.work
        self.userDataObj.job = userdata.job
        self.userDataObj.education = userdata.education
        self.userDataObj.showMyAge = userdata.dontShowMyAge
        self.userDataObj.showMyDist = userdata.dontShowMyDist
        self.userDataObj.featureLocation = userdata.featureLocation as NSObject
        self.userDataObj.currentLocation = userdata.currentLocation as NSObject
        self.userDataObj.userOtherLocations = userdata.userOtherLocations as NSObject
        
        self.userDataObj.instaGramProfileId = userdata.instaGramProfileId
        self.userDataObj.instaGramToken = userdata.instaGramToken
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        if userdata.instaGramProfileId.count != 0 && userdata.instaGramToken.count != 0 {
            self.instaVM.instaProfile.updateTokenId(instaId: userdata.instaGramProfileId, instaAuth: userdata.instaGramToken)
            
            
            let userInstaDetails = [ServiceInfo.InstaGramToken: userdata.instaGramProfileId,
                                       ServiceInfo.InstaGramProfileId: userdata.instaGramToken]
            
            UserDefaults.standard.setValue(userInstaDetails, forKey:"instaLoggedDetails")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.setValue( userdata.instaGramToken, forKey:"instaAuthentication")
            UserDefaults.standard.synchronize()
            
        }
    
    }
    
    
    
    func deleteUserData() {
        context.delete(userDataObj)
        (UIApplication.shared.delegate as! AppDelegate)
    }
}
