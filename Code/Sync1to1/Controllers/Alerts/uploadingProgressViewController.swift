//
//  uploadingProgressViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 06/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import AWSS3
import AWSCore

protocol VideoUploadingDoneDelegate {
    func videoUploadCompleted()
}

class uploadingProgressViewController: UIViewController {
    
    @IBOutlet weak var alertTitleOutlet: UILabel!
    @IBOutlet weak var progressPercentageOutlet: UILabel!
    @IBOutlet weak var progressViewOutlet: UIView!
    var videoUploadingDoneDelegate: VideoUploadingDoneDelegate?
    
    var prgressUploader :AWSS3TransferManagerUploadRequest?
    var progress: Float = 0.0
    var progressView : UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let margin:CGFloat = 8.0
        progressView = UIProgressView(frame: CGRect(x: margin, y: 16.0, width: progressViewOutlet.frame.width - margin * 2.0 - 50, height: 2.0))
        progressView.progress = progress
        var color:UIColor!
        if #available(iOS 10.0, *) {
            color = UIColor(displayP3Red: 0.0, green: 0.659, blue: 1.00, alpha: 1.0)
        } else {
            color = UIColor.blue
        }
        progressView.tintColor = color
        DispatchQueue.main.async{
            self.progressViewOutlet.addSubview(self.progressView)
            self.progressView.superview?.bringSubview(toFront: self.progressView)
        }
        self.showProgress()
    }
    
    func showProgress(){
        if let uploadRequest = prgressUploader{
            uploadRequest.uploadProgress = {(bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) -> Void in
                let floatOne:Float = Float(totalBytesSent)
                let floatTwo:Float = Float(totalBytesExpectedToSend)
                let currentProgress = floatOne.round(2)/floatTwo.round(2)
                print(currentProgress)
                DispatchQueue.main.async {
                    self.progressView.setProgress(currentProgress, animated: true)
                    self.progressPercentageOutlet.text = "\(Float(currentProgress*100).round(2))%"
                    
                    if totalBytesSent == totalBytesExpectedToSend
                    {
                        if (self.videoUploadingDoneDelegate != nil){
                            self.videoUploadingDoneDelegate?.videoUploadCompleted()
                        }
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
