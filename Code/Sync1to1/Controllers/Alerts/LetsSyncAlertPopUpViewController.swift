//
//  LetsSyncAlertPopUpViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 30/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

protocol LetsSyncAlertButtonPressedDelegate { //getting delegate methods for the button actions
    func nowButtonPressed()
    func LaterButtonPressed()
}

class LetsSyncAlertPopUpViewController: UIViewController {

    var letsSyncAlertButtonPressedDelegate : LetsSyncAlertButtonPressedDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nowButtonAction(_ sender: Any)
    {
        if (letsSyncAlertButtonPressedDelegate != nil){
            letsSyncAlertButtonPressedDelegate?.nowButtonPressed()
        }
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func laterButtonAction(_ sender: Any)
    {
        if (letsSyncAlertButtonPressedDelegate != nil){
            letsSyncAlertButtonPressedDelegate?.LaterButtonPressed()
            self.dismiss(animated: false, completion: nil)
        }
    }

}
