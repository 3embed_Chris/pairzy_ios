//
//  CameraViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 23/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import AVKit
import UIKit
import AVFoundation
import AudioToolbox

class CameraViewController: UIViewController {
    
    @IBOutlet weak var cameraView: UIView!
    var boxView:UIView!
    
    struct Constant {
        static let AlertMessage = "Do you want to set this as your profile picture?"
        static let AlertTitle = "Profile Picture"
        static let AlertOptionNonDestructiveTitle = "Yes"
        static let AlertOptionDestructiveTitle = "No"
        static let DispatchQueueName = "VideoDataOutputQueue"
        static let toVideoControllerSegue = "to Video Controller Segue"
        static let toCustomAlertSegue = "cameraAlertPopUpSegue"
    }
    
    @IBOutlet weak var previouslySelectedViewOutlet: UIView!
    @IBOutlet weak var previouslySelectedImageOutlet: UIImageView!
    @IBOutlet weak var imageViewOutlet: UIImageView!
    
    var videoDataOutput: AVCaptureVideoDataOutput!
    var videoDataOutputQueue: DispatchQueue!
    var previousImage: UIImage!
    var previewLayer:AVCaptureVideoPreviewLayer!
    var captureDevice : AVCaptureDevice!
    let session = AVCaptureSession()
    var currentFrame: CIImage!
    let stillImageOutput = AVCaptureStillImageOutput()
    var done = false
    var userData : UserData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setupAVCapture()
        if !done {
            session.startRunning()
        }
        stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
        if session.canAddOutput(stillImageOutput) {
            session.addOutput(stillImageOutput)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        session.stopRunning()
        done = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var shouldAutorotate: Bool {
        if (UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft ||
            UIDevice.current.orientation == UIDeviceOrientation.landscapeRight ||
            UIDevice.current.orientation == UIDeviceOrientation.unknown) {
            return false
        }
        else {
            return true
        }
    }
    @IBAction func cameraButtonTapped(_ sender: Any)
    {
        if let videoConnection = stillImageOutput.connection(withMediaType: AVMediaTypeVideo) {
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                if let image = UIImage(data: imageData!){
                    self.imageViewOutlet.image = image
                    self.previousImage = image
                    AudioServicesPlaySystemSound(1108)
                    self.session.stopRunning()
                    self.showCustomPopUp()
                }
            }
        }
    }
    
    @IBAction func flipCameraAction(_ sender: Any) {
        
        if self.session.isRunning{
            session.beginConfiguration()
            if let currentCameraInput = session.inputs.first{
                session.removeInput(currentCameraInput as! AVCaptureInput)
                
                var newCamera:AVCaptureDevice!
                switch (currentCameraInput as! AVCaptureDeviceInput).device.position {
                case .back:
                    newCamera = self.cameraWith(position: .front)
                    
                case .front:
                    newCamera = self.cameraWith(position: .back)
                    
                default:
                    newCamera = self.cameraWith(position: .front)
                }
                captureDevice = newCamera
            }
            
            updateSession()
            session.commitConfiguration()
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
    }
    
    func cameraWith(position: AVCaptureDevicePosition) -> AVCaptureDevice?
    {
        if let devices:[AVCaptureDevice] = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) as? [AVCaptureDevice]{
            for device in devices{
                if device.position == position
                {
                    return device
                }
            }
        }
        return AVCaptureDevice()
    }
    
    @IBAction func previouslySelectedImageButtonAction(_ sender: Any)
    {
        if (previousImage != nil){
            self.previouslySelectedViewOutlet.isHidden = false
            self.previouslySelectedImageOutlet.image = previousImage
            self.cameraView.isHidden = true
            self.showCustomPopUp()
        }
    }
    
    func showCustomPopUp(){
        self.performSegue(withIdentifier: Constant.toCustomAlertSegue, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Constant.toVideoControllerSegue{
            let controller = segue.destination as! VideoRecorderViewController
            controller.userProfilePic = self.previousImage
            controller.userData = self.userData
        }
        else if segue.identifier == Constant.toCustomAlertSegue{
            let controller = segue.destination as! MessageAndTwoButtonsViewController
            controller.titleString = Constant.AlertTitle
            controller.messageInAlert = Constant.AlertMessage
            controller.nonDestructiveButtonTitleString = Constant.AlertOptionNonDestructiveTitle
            controller.destructiveButtonTitleString = Constant.AlertOptionDestructiveTitle
            controller.alertButtonPressedDelegate = self
        }
    }
    
    func uploadImageToAmazonwith(image:UIImage)
    {
        AmazonHelper.shared.upload(image: image, isThumbnail: false, completion:{ (isUploadSuccessful,fileurl) in
            if isUploadSuccessful
            {
                let dbObj = Database.shared
                dbObj.userDataObj.profilePictureURL = fileurl
            }
            else{
                print("\(fileurl)")
            }
        })
    }
}


// AVCaptureVideoDataOutputSampleBufferDelegate protocol and related methods
extension CameraViewController:  AVCaptureVideoDataOutputSampleBufferDelegate{
    func setupAVCapture(){
        session.sessionPreset = AVCaptureSessionPreset1280x720
        if #available(iOS 10.0, *) {
            guard let device = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera,
                                                             mediaType: AVMediaTypeVideo,
                                                             position: .front) else{
                                                                return
            }
            captureDevice = device
            beginSession()
            done = true
        } else {
            // Fallback on earlier versions
        }
    }
    
    func updateSession(){
        var err : NSError? = nil
        var deviceInput:AVCaptureDeviceInput?
        do {
            deviceInput = try AVCaptureDeviceInput(device: captureDevice)
        } catch let error as NSError {
            err = error
            deviceInput = nil
        }
        
        if err != nil {
            print("error: \(String(describing: err?.localizedDescription))")
        }
        if self.session.canAddInput(deviceInput){
            self.session.addInput(deviceInput)
        }
    }
    
    func beginSession(){
        var err : NSError? = nil
        var deviceInput:AVCaptureDeviceInput?
        do {
            deviceInput = try AVCaptureDeviceInput(device: captureDevice)
        } catch let error as NSError {
            err = error
            deviceInput = nil
        }
        
        if err != nil {
            print("error: \(String(describing: err?.localizedDescription))")
        }
        if self.session.canAddInput(deviceInput){
            self.session.addInput(deviceInput)
        }
        
        videoDataOutput = AVCaptureVideoDataOutput()
        videoDataOutput.alwaysDiscardsLateVideoFrames=true
        videoDataOutputQueue = DispatchQueue(label: Constant.DispatchQueueName)
        videoDataOutput.setSampleBufferDelegate(self, queue:self.videoDataOutputQueue)
        if session.canAddOutput(self.videoDataOutput){
            session.addOutput(self.videoDataOutput)
        }
        videoDataOutput.connection(withMediaType: AVMediaTypeVideo).isEnabled = true
        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.session)
        self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspect
        
        let rootLayer: CALayer = self.cameraView.layer
        rootLayer.masksToBounds = true
        self.previewLayer.frame = rootLayer.bounds
        rootLayer.addSublayer(self.previewLayer)
        session.startRunning()
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        currentFrame =   self.convertImageFromCMSampleBufferRef(sampleBuffer)
    }
    
    // clean up AVCapture
    func stopCamera(){
        session.stopRunning()
        done = false
    }
    
    func convertImageFromCMSampleBufferRef(_ sampleBuffer:CMSampleBuffer) -> CIImage{
        let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)!
        let ciImage:CIImage = CIImage(cvImageBuffer: pixelBuffer)
        return ciImage
    }
}

extension CameraViewController : AlertButtonPressedDelegate{
    
    func yesButtonPressed()
    {
        self.uploadImageToAmazonwith(image:self.previousImage!)
        UIImageWriteToSavedPhotosAlbum(self.previousImage!, nil, nil, nil)
        self.performSegue(withIdentifier: Constant.toVideoControllerSegue, sender: nil)
    }
    
    func noButtonPressed()
    {
        self.session.startRunning()
        self.previouslySelectedViewOutlet.isHidden = true
        self.cameraView.isHidden = false
    }
}
