//
//  VideoRecorderViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 24/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyCam
import AVFoundation
import AVKit
import AWSS3
import AWSCore

class VideoRecorderViewController: SwiftyCamViewController {
    
    @IBOutlet weak var videoButtonOutlet: UIButton!
    @IBOutlet weak var videoPlayerOutlet: UIView!
    @IBOutlet weak var cameraViewOutlet: UIView!
    @IBOutlet weak var videoFunctionallityButtonOutlet: UIView!
    
    typealias progressBlock = () -> ()
    
    var action: progressBlock? = { }
    var prgressUploader :AWSS3TransferManagerUploadRequest?
    var flipCameraButton: UIButton!
    var userProfilePic:UIImage!
    var flashButton: UIButton!
    var captureButton: SwiftyRecordButton!
    var player: AVPlayer?
    var playerController : AVPlayerViewController?
    var userData:UserData!
    
    struct Constant {
        static let AlertMessage = "Do you want to set this as your profile video?"
        static let AlertTitle = "Profile Video"
        static let AlertOptionNonDestructiveTitle = "Yes"
        static let AlertOptionDestructiveTitle = "No"
        static let toPreviewControllerSegue = "to Preview Controller Segue"
        static let videoAlertPopUpSegue = "videoAlertPopUpSegue"
        static let toProgressIndicatorPopUp = "toProgressIndicatorPopUpSegue"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cameraDelegate = self
        maximumVideoDuration = 10.0
        shouldUseDeviceOrientation = true
        addButtons()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc private func cameraSwitchAction(_ sender: Any) {
        switchCamera()
    }
    
    func showCustomPopUp(){
        self.performSegue(withIdentifier: Constant.toProgressIndicatorPopUp, sender: nil)
    }
    
    @objc private func toggleFlashAction(_ sender: Any) {
        flashEnabled = !flashEnabled
        
        if flashEnabled == true {
            flashButton.setImage(#imageLiteral(resourceName: "flash"), for: UIControlState())
        } else {
            flashButton.setImage(#imageLiteral(resourceName: "flashOutline"), for: UIControlState())
        }
    }
    
    private func addButtons() {
        captureButton = SwiftyRecordButton(frame: CGRect(x: view.frame.midX - 37.5, y: view.frame.height - 100.0, width: 75.0, height: 75.0))
        self.view.addSubview(captureButton)
        captureButton.delegate = self
        
        flipCameraButton = UIButton(frame: CGRect(x: (((view.frame.width / 2 - 37.5) / 2) - 15.0), y: view.frame.height - 74.0, width: 30.0, height: 23.0))
        flipCameraButton.setImage(#imageLiteral(resourceName: "flipCamera"), for: UIControlState())
        flipCameraButton.addTarget(self, action: #selector(cameraSwitchAction(_:)), for: .touchUpInside)
        self.view.addSubview(flipCameraButton)
        
        let test = CGFloat((view.frame.width - (view.frame.width / 2 + 37.5)) + ((view.frame.width / 2) - 37.5) - 9.0)
        
        flashButton = UIButton(frame: CGRect(x: test, y: view.frame.height - 77.5, width: 18.0, height: 30.0))
        flashButton.setImage(#imageLiteral(resourceName: "flashOutline"), for: UIControlState())
        flashButton.addTarget(self, action: #selector(toggleFlashAction(_:)), for: .touchUpInside)
        self.view.addSubview(flashButton)
    }
    
    
    /*
     
     amazon video upload
     AmazonwapperClass.shared.upload(videoStr: Url!, completion:{(success,url)in
     print ("jayesh here =%@",url)
     })
     } else {
     debugPrint("\(error)")
     }
     */
    
    func playVideo(videoUrl : URL){
        player = AVPlayer(url: videoUrl)
        playerController = AVPlayerViewController()
        
        guard player != nil && playerController != nil else {
            return
        }
        playerController!.showsPlaybackControls = false
        playerController!.player = player!
        playerController!.view.frame = view.frame
        
        if !self.videoPlayerOutlet.subviews.contains(playerController!.view)
        {
            self.videoPlayerOutlet.addSubview(playerController!.view)
            self.videoPlayerOutlet.bringSubview(toFront: self.videoFunctionallityButtonOutlet)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player!.currentItem)
    }
    
    @IBAction func playButtonAction(_ sender: Any)
    {
        if self.videoButtonOutlet.isSelected{
            self.player?.pause()
        }else{
            self.player!.play()
        }
        self.videoButtonOutlet.isSelected = !self.videoButtonOutlet.isSelected
        
    }
    
    @IBAction func retakeButtonAction(_ sender: Any)
    {
        self.videoPlayerOutlet.isHidden = true
        self.player?.pause()
    }
    
    @IBAction func doneButtonAction(_ sender: Any)
    {
        self.uploadVideoToAmazon()
        self.showCustomPopUp()
    }
    
    @objc fileprivate func playerItemDidReachEnd(_ notification: Notification) {
        if self.player != nil {
            self.player!.seek(to: kCMTimeZero)
            self.videoButtonOutlet.isSelected = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constant.toPreviewControllerSegue{
            let controller = segue.destination as! PreviewViewController
            controller.userProfilePic = self.userProfilePic
            controller.userData = self.userData
        }
        else if segue.identifier == Constant.videoAlertPopUpSegue{
            let controller = segue.destination as! MessageAndTwoButtonsViewController
            controller.titleString = Constant.AlertTitle
            controller.messageInAlert = Constant.AlertMessage
            controller.nonDestructiveButtonTitleString = Constant.AlertOptionNonDestructiveTitle
            controller.destructiveButtonTitleString = Constant.AlertOptionDestructiveTitle
            controller.alertButtonPressedDelegate = self
        }
        else if segue.identifier == Constant.toProgressIndicatorPopUp{
            let controller = segue.destination as! uploadingProgressViewController
            controller.prgressUploader = self.prgressUploader
            controller.videoUploadingDoneDelegate = self
        }
    }
    
    func getVideoUrl() -> URL? {
        let asset = self.player?.currentItem?.asset
        if asset == nil {
            return nil
        }
        if let urlAsset = asset as? AVURLAsset {
            return urlAsset.url
        }
        return nil
    }
    
    func addVideoURLToDatabase(withURL fileUrl : String,andUserData userData: UserData){
        let dbObj = Database.shared
        dbObj.videoURL = fileUrl
    }
    
    func uploadVideoToAmazon(){
        if let videoUrl = self.getVideoUrl(){
            let url = videoUrl
            AmazonHelper.shared.uploadVideo(videoURL: url,
                                            onStart: { (progressRequest) in
                                                self.prgressUploader = progressRequest
            },
                                            completion:{ isUploadSuccessful,fileurl in
                                                if isUploadSuccessful{
                                                    print("\(fileurl)")
                                                    self.addVideoURLToDatabase(withURL: fileurl, andUserData: self.userData)
                                                }
                                                else{
                                                    print("\(fileurl)")
                                                }
            })
        }
        else{
            print("failed to get the video URL from Player")
        }
    }
    
}

extension VideoRecorderViewController : SwiftyCamViewControllerDelegate{
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        //        print("Did Begin Recording")
        captureButton.growButton()
        captureButton.showCircleProgress()
        UIView.animate(withDuration: 0.25, animations: {
            self.flashButton.alpha = 0.0
            self.flipCameraButton.alpha = 0.0
        })
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        //        print("Did finish Recording")
        captureButton.shrinkButton()
        captureButton.removeCircleProgress()
        UIView.animate(withDuration: 0.25, animations: {
            self.flashButton.alpha = 1.0
            self.flipCameraButton.alpha = 1.0
        })
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
        
        self.videoPlayerOutlet.isHidden = false
        self.view.bringSubview(toFront: self.videoPlayerOutlet)
        self.playVideo(videoUrl: url)
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFocusAtPoint point: CGPoint) {
        let focusView = UIImageView(image: #imageLiteral(resourceName: "focus"))
        focusView.center = point
        focusView.alpha = 0.0
        view.addSubview(focusView)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            focusView.alpha = 1.0
            focusView.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        }, completion: { (success) in
            UIView.animate(withDuration: 0.15, delay: 0.5, options: .curveEaseInOut, animations: {
                focusView.alpha = 0.0
                focusView.transform = CGAffineTransform(translationX: 0.6, y: 0.6)
            }, completion: { (success) in
                focusView.removeFromSuperview()
            })
        })
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didChangeZoomLevel zoom: CGFloat) {
        print(zoom)
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didSwitchCameras camera: SwiftyCamViewController.CameraSelection) {
        print(camera)
    }
}

extension VideoRecorderViewController : AlertButtonPressedDelegate{
    
    func yesButtonPressed()
    {
        uploadVideoToAmazon()
    }
    
    func noButtonPressed()
    {
        
    }
}

extension VideoRecorderViewController : VideoUploadingDoneDelegate {
    
    func videoUploadCompleted() {
        
        self.performSegue(withIdentifier: Constant.toPreviewControllerSegue, sender: nil)
    }
}

