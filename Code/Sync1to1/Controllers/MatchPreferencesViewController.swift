//
//  MatchPreferencesViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 26/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MARKRangeSlider

class MatchPreferencesViewController: UIViewController {
    
    struct Constants {
        static let Inch_In_CM:Float = 2.54
        static let numberOfInchesInFeet:Float = 12
        static let Miles_In_Km :CGFloat = 0.6214
        static let Km_In_Miles :CGFloat = 1.6094
        static let lowestCMValue:CGFloat = 120
        static let highestCMValue : CGFloat = 310
        static let lowestMilesDistanceValue:CGFloat = 0
        static let highestMilesDistanceValue:CGFloat = 1000
        static let highestKMDistanceValue:CGFloat = 1600
        static let lowestAgeValue:Int = 18
        static let highestAgeValue:Int = 60
        static let minimumDistanceBetweenTwoPointsInSlider:CGFloat = 2
        static let feetValue = "feet"
        static let cmValue = "cm"
        static let kmValue = "km"
        static let milesValue = "miles"
        static let cancelButtonTitle = "Cancel"
        static let toTabBarControllerSegue = "toTabBarController"
    }
    
    
    @IBOutlet weak var preferredGenderOutlet: UILabel!
    
    @IBOutlet weak var ageSliderOutlet: MARKRangeSlider!
    @IBOutlet weak var ageValueLabelOutlet: UILabel!
    
    @IBOutlet weak var heightSliderOutlet: MARKRangeSlider!
    @IBOutlet weak var heightValueLabelOutlet: UILabel!
    @IBOutlet weak var heightSwitchSegmentControlOutlet: UISegmentedControl!
    
    @IBOutlet weak var distanceValueLabelOutlet: UILabel!
    @IBOutlet weak var distanceSwitchSegmentControlOutlet: UISegmentedControl!
    @IBOutlet weak var distanceSliderOutlet: MARKRangeSlider!
    
    var isComingFromSignUp = false
    var userData : UserData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureAgeSlider()
        self.configureHeightSlider(minValue: Constants.lowestCMValue, maxValue: Constants.highestCMValue)
        self.configureDistanceSliderwith(minValue: Constants.lowestMilesDistanceValue, maxValue: Constants.highestMilesDistanceValue)
    }
    
    func configureAgeSlider()
    {
        ageSliderOutlet.addTarget(self, action: #selector(ageSliderMoved(slider:)), for: UIControlEvents.valueChanged)
        ageSliderOutlet.rangeImage = #imageLiteral(resourceName: "progress_line")
        ageSliderOutlet.setMinValue(CGFloat(Constants.lowestAgeValue), maxValue: CGFloat(Constants.highestAgeValue))
        ageSliderOutlet.setLeftValue(CGFloat(Constants.lowestAgeValue), rightValue: CGFloat(Constants.highestAgeValue))
        ageSliderOutlet.minimumDistance = Constants.minimumDistanceBetweenTwoPointsInSlider
        ageValueLabelOutlet.text = "\(Constants.lowestAgeValue) - \(Constants.highestAgeValue)+"
    }
    
    func configureHeightSlider(minValue:CGFloat, maxValue : CGFloat)
    {
        heightSliderOutlet.addTarget(self, action: #selector(heightSliderMoved(slider:)), for: UIControlEvents.valueChanged)
        heightSliderOutlet.rangeImage = #imageLiteral(resourceName: "progress_line")
        heightSliderOutlet.setMinValue(Constants.lowestCMValue, maxValue: Constants.highestCMValue)
        heightSliderOutlet.setLeftValue(minValue, rightValue: maxValue)
        
        if heightSwitchSegmentControlOutlet.selectedSegmentIndex == 0
        {
            let minFeetValue = self.getFeetAndInchesFrom(centimeters: Float(minValue))
            let maxFeetValue = self.getFeetAndInchesFrom(centimeters: Float(maxValue))
            
            heightValueLabelOutlet.text = "\(minFeetValue) - \(maxFeetValue) \(Constants.feetValue)"
        }
        else if heightSwitchSegmentControlOutlet.selectedSegmentIndex == 1
        {
            heightValueLabelOutlet.text = "\(Int(minValue)) - \(Int(maxValue)) \(Constants.cmValue)"
        }
    }
    
    func configureDistanceSliderwith(minValue:CGFloat, maxValue : CGFloat)
    {
        distanceSliderOutlet.addTarget(self, action: #selector(distanceSliderMoved(slider:)), for: UIControlEvents.valueChanged)
        distanceSliderOutlet.rangeImage = #imageLiteral(resourceName: "progress_line")
        if distanceSwitchSegmentControlOutlet.selectedSegmentIndex == 0
        {
            distanceSliderOutlet.setMinValue(Constants.lowestMilesDistanceValue, maxValue: Constants.highestMilesDistanceValue)
            distanceSliderOutlet.setLeftValue(minValue, rightValue: maxValue)
            distanceValueLabelOutlet.text = "\(Int(minValue)) - \(Int(maxValue)) \(Constants.milesValue)"
        }
        else if distanceSwitchSegmentControlOutlet.selectedSegmentIndex == 1
        {
            distanceSliderOutlet.setMinValue(Constants.lowestMilesDistanceValue, maxValue:Constants.highestKMDistanceValue)
            distanceSliderOutlet.setLeftValue(minValue, rightValue: maxValue)
            distanceValueLabelOutlet.text = "\(Int(minValue)) - \(Int(maxValue)) \(Constants.kmValue)"
        }
    }
    
    func openGenderPreferences(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let closure = { (action: UIAlertAction!) -> Void in
            let index = alert.actions.index(of: action)
            
            if index != nil {
                self.preferredGenderOutlet.text = Gender.GenderArray[index!]
            }
        }
        
        for i in 0 ..< Gender.GenderArray.count {
            alert.addAction(UIAlertAction(title: Gender.GenderArray[i], style: .default, handler: closure))
        }
        
        alert.addAction(UIAlertAction(title: Constants.cancelButtonTitle, style: .destructive, handler: {(_) in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func ageSliderMoved(slider : MARKRangeSlider)
    {
        let intMinValue = Int(slider.leftValue)
        let intMaxValue = Int(slider.rightValue)
        
        self.ageValueLabelOutlet.text = "\(intMinValue) - \(intMaxValue)"
        if intMaxValue == Constants.highestAgeValue
        {
            self.ageValueLabelOutlet.text = "\(intMinValue) - \(intMaxValue)+"
        }
    }
    
    func heightSliderMoved(slider : MARKRangeSlider)
    {
        if heightSwitchSegmentControlOutlet.selectedSegmentIndex == 0
        {
            let minFeetValue = self.getFeetAndInchesFrom(centimeters: Float(slider.leftValue))
            let maxFeetValue = self.getFeetAndInchesFrom(centimeters: Float(slider.rightValue))
            
            heightValueLabelOutlet.text = "\(minFeetValue) - \(maxFeetValue) \(Constants.feetValue)"
        }
        else if heightSwitchSegmentControlOutlet.selectedSegmentIndex == 1
        {
            heightValueLabelOutlet.text = "\(Int(slider.leftValue)) - \(Int(slider.rightValue)) \(Constants.cmValue)"
        }
    }
    
    func distanceSliderMoved(slider : MARKRangeSlider)
    {
        let intMinValue = Int(slider.leftValue)
        let intMaxValue = Int(slider.rightValue)
        
        if distanceSwitchSegmentControlOutlet.selectedSegmentIndex == 0{
            self.distanceValueLabelOutlet.text = "\(intMinValue) - \(intMaxValue) \(Constants.milesValue)"
        }
        else if distanceSwitchSegmentControlOutlet.selectedSegmentIndex == 1{
            self.distanceValueLabelOutlet.text = "\(intMinValue) - \(intMaxValue) \(Constants.kmValue)"
        }
    }
    
    @IBAction func heightSegmentControlPressed(_ sender: Any)
    {
        self.configureHeightSlider(minValue: CGFloat(heightSliderOutlet.leftValue), maxValue: CGFloat(heightSliderOutlet.rightValue))
    }
    
    @IBAction func genderPickerButtonAction(_ sender: Any)
    {
        self.openGenderPreferences()
    }
    
    @IBAction func doneButtonAction(_ sender: Any)
    {
        if isComingFromSignUp{
            self.performSegue(withIdentifier: Constants.toTabBarControllerSegue, sender: userData)
        }else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func distanceSegmentControlPressed(_ sender: Any)
    {
        if distanceSwitchSegmentControlOutlet.selectedSegmentIndex == 0{
            
            let minValue = distanceSliderOutlet.leftValue*Constants.Miles_In_Km
            var maxValue = distanceSliderOutlet.rightValue*Constants.Miles_In_Km
            if maxValue>Constants.highestMilesDistanceValue
            {
                maxValue = Constants.highestMilesDistanceValue
            }
            self.configureDistanceSliderwith(minValue: minValue, maxValue: maxValue)
        }
        else if distanceSwitchSegmentControlOutlet.selectedSegmentIndex == 1
        {
            let minValue = distanceSliderOutlet.leftValue*Constants.Km_In_Miles
            var maxValue = distanceSliderOutlet.rightValue*Constants.Km_In_Miles
            if maxValue>Constants.highestKMDistanceValue
            {
                maxValue = Constants.highestKMDistanceValue
            }
            self.configureDistanceSliderwith(minValue: minValue, maxValue: maxValue)
        }
    }
    
    func getFeetAndInchesFrom(centimeters:Float) ->String
    {
        let numInches = roundf(centimeters/Constants.Inch_In_CM)
        let feet:Int = Int(numInches/Constants.numberOfInchesInFeet)
        let inches:Float = roundf(numInches.truncatingRemainder(dividingBy: Constants.numberOfInchesInFeet))
        return String(format: "\(feet)'\(Int(inches))\"")
    }
}
