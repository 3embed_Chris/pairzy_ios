//
//  PreviewViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 25/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase

class PreviewViewController: UIViewController {
    
    struct Constant {
        static let cancelButtonTitle = "Cancel"
        static let signingUpMessage = "Loading..."
        static let constantPushToken = "iOS Simulator Push Token"
        static let currentLocation = "Bangalore"
        static let currentLatitude = "10.344"
        static let currentLongitude = "13.566"
        static let apiRequiredDateFormat = "dd/MM/yyyy"
        static let presentFiltersSegue = "presentFiltersSegue"
    }
    
    var userData : UserData!
    var progressHUD:ProgressHUD?
    
    @IBOutlet weak var imaegViewOutlet: UIImageView!
    @IBOutlet weak var nameLabelOutlet: UILabel!
    @IBOutlet weak var sexAndHeightLabelOutlet: UILabel!
    
    var userProfilePic:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (userProfilePic != nil){
            self.imaegViewOutlet.image = userProfilePic
        }
        if (userData != nil){
            if userData.firstName != ""{
                let dobInYear = userData.dateOfBirth.age
                self.nameLabelOutlet.text = "\(userData.firstName.capitalized), \(dobInYear)"
            }
            
            if userData.gender != "", userData.heightInCM != 0.00{
                let heightInFeet = HeightConverter.getFeetAndInchesFrom(centimeters: userData.heightInCM)
                self.sexAndHeightLabelOutlet.text = "\(userData.gender), \(heightInFeet)"
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constant.presentFiltersSegue{
            let navController = segue.destination as! UINavigationController
            let controller = navController.childViewControllers.first as! MatchPreferencesViewController
            controller.isComingFromSignUp = true
            controller.userData = self.userData
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editButtonAction(_ sender: Any) {
    }
    
    @IBAction func doneButtonAction(_ sender: Any)
    {
       
//        self.sendSignupRequest()
        
        let dbObj = Database.shared
        dbObj.addUserdataToDatabase(withUserdata: self.userData)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        self.progressHUD?.hide()
        self.performSegue(withIdentifier: Constant.presentFiltersSegue, sender: nil)
    }
    
    
    func sendSignupRequest(){
        
        progressHUD = ProgressHUD(text:Constant.signingUpMessage)
        self.view.addSubview(progressHUD!)

        
        
        //guard let user = Auth.auth().currentUser else { return }
        var pushTocken = ""
        if  let push = InstanceID.instanceID().token(){
            pushTocken = push
        }else {
            pushTocken = Constant.constantPushToken}
        
        
        let params =    [
            ServiceInfo.Fname               :self.userData.firstName,
            ServiceInfo.DateOfBirth         :self.userData.dateOfBirth,
            ServiceInfo.Password            :self.userData.password,
            ServiceInfo.Gender              :self.userData.gender,
            ServiceInfo.MobileNumber        : 8079004749 as NSNumber ,    ///self.userData.mobileNumber
            ServiceInfo.PushToken           :pushTocken,
            ServiceInfo.DeviceType          :"1",
            ServiceInfo.Email               :self.userData.emailID,
            ServiceInfo.DeviceModel         :UIDevice.current.systemName,
            ServiceInfo.Height              :self.userData.heightInCM,
            ServiceInfo.DeviceMake          :UIDevice.current.model,
            ServiceInfo.LocationName        :Constant.currentLocation,
            ServiceInfo.Latitude            :Constant.currentLatitude,
            ServiceInfo.Longitude           :Constant.currentLongitude,
            ServiceInfo.FirbaseID           : "jjbjbj",// user.uid,
            ServiceInfo.CountCode           : 91 as NSNumber ,
            ServiceInfo.profilePic          : "" ,
            ServiceInfo.profileVideo        : "" ,
            ]  as [String:Any]
        

        
        AFWrapper.requestPOSTURL(serviceName: APINAMES.SIGNUP, params: params, success:{ (response) in
            
            print("preview response =\(response)")
            
            //Do sign up method here.
           // let userId = user.uid // firebase UID
            
        }, failure: {(response) in
            
             self.progressHUD?.hide()
            print("preview response =\(response)")
            
        })
        
        
        
    }
}

