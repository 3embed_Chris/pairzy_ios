//
//  ChatAPI.swift
//  MQTT Chat Module
//
//  Created by Dinesh Guptha Bavirisetti on 31/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Locksmith
import CocoaLumberjack
import RxSwift
import Alamofire
import RxCocoa

class ChatAPI: NSObject {
    
    func getChats(withPageNo pageNo : String, withCompletion:@escaping ([String : Any]) -> Void) {
        let strURL =  APINAMES.getChats+"/"+pageNo
        API.requestGETURL(serviceName: strURL,
                           withStaticAccessToken:false,
                           success: { (response) in
                           print(response)
                            
        },
                           failure: { (Error) in
                           
        })
    }
    
    
    func getMessages(withUrl:String) {
        API.requestGETURL(serviceName: withUrl,
                          withStaticAccessToken:false,
                          success: { (response) in
                            
              print(response)
        },
                          failure: { (Error) in
                             print(Error.localizedDescription
                            )
        })
    }
    
    func deleteChat(withSecretID secretID: String?, andRecipientID recipientID : String?, success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)  {
//        var strURL = AppConstants.getChats
//        if let recipientID = recipientID {
//            if recipientID.count>0 {
//                strURL = strURL+"/"+recipientID
//            }
//        }
//        if let secretID = secretID {
//            if secretID.count>0 {
//                strURL = strURL+"/"+secretID
//            }
//        }
//
//        guard let keyDict = Locksmith.loadDataForUserAccount(userAccount: ChatAppConstants.keyChainAccount) else {return}
//        guard  let token = keyDict["token"] as? String  else {return}
//        if token.count>1 {
//            let headerParams = ["authorization":AppConstants.authorization,
//                                "token":token]
//            AFWrapper.requestDeleteURL(serviceName: strURL, params: nil, header: headerParams,
//                                       success: { (response) in
//                                        guard let responseData = response.dictionaryObject else { return }
//                                        print("Response",responseData)
//                                        success(responseData)
//            },
//                                       failure: { (customErrorStruct) in
//                                        print("Error",customErrorStruct)
//                                        failure(customErrorStruct)
//            })
//        }
    }
    
    func sendNotification(withText text: String, andTitle title: String, toTopic topic: String, andData data:[String : String] ) {
        let url = "https://fcm.googleapis.com/fcm/send"
        let header = ["Authorization":ChatAppConstants.mGoogleAPIKeyFCM]
        let selfID = Helper.getMQTTID()!
        
        let userID = selfID // Yours ID
        
        let name = Helper.getUserName()
        let profilePic = Helper.getUserProfilePic()
        
        let notificationParams = [ "body" : "\(text)",
            "title" : "\(title)",
            "sound": "default","type":"30",
            MatchedProfileData.firstLikedId:userID,
            MatchedProfileData.firstLikedByName:name,
            MatchedProfileData.firstLikedByPhoto:profilePic]
        
        
        let notificationData = ["body":userID]
        
//        let changeTopicName = topic.replace(target: "/", withString: "")
        
        
        
        let params : [String : Any] = ["condition": "'\(topic)' in topics",
            "priority" : "high",
            "notification" : notificationParams,
            "data":notificationData,
            ] as [String : Any]
        
        AFWrapper.requestPOSTURL(serviceName: url, withHeader: header, params: params, success:{ (response) in
            guard let responseData = response.dictionaryObject else { return }
            print("Response",responseData)
        },
                                 failure: { (customErrorStruct) in
                                    print("Error",customErrorStruct)
        })
    }
}

class MessageAPI : NSObject {
    
    func getMessages(withUrl messageUrl : String) {
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
        AFWrapper.requestGETURL(serviceName: messageUrl, withHeader: headers as! [String : String],
                                success: { (response) in
                                    guard let responseData = response.dictionaryObject else { return }
                                    print("Response",responseData)
        },
                                failure: { (customErrorStruct) in
                                    print("Error",customErrorStruct)
        })
    }
    
    func sendNotification(withText text: String, andTitle title: String, toTopic topic: String, andData data:[String : String]) {
        guard let selfID = Helper.getMQTTID() else { return  }
        let url = "https://fcm.googleapis.com/fcm/send"
        let header = ["Authorization":AppConstant.mGoogleLegaceyServerKey]
        guard let secretID = data["secretID"] else { return }
        let userID = ["receiverID":selfID,
                      "secretID":secretID] as [String : String]
        
        let notificationParams = [ "body" : "\(text)",
            "title" : "\(title)",
            "sound": "default"]
        let notificationData = ["body":userID]
        let params : [String : Any] = ["condition": "'\(topic)' in topics",
            "priority" : "high",
            "notification" : notificationParams,
            "data":notificationData
            ] as [String : Any]
        
        AFWrapper.requestPOSTURL(serviceName: url, withHeader: header, params: params, success:{ (response) in
            guard let responseData = response.dictionaryObject else { return }
            print("Response",responseData)
        },
                                 failure: { (customErrorStruct) in
                                    print("Error",customErrorStruct)
        })
    }
}
