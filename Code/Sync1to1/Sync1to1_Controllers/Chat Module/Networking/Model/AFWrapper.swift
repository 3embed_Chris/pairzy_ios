//
//  AFWrapper.swift
//  Sales Paddock
//
//  Created by 3Embed on 15/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AFWrapper: NSObject {
    
    
    class func requestPOSTURL(serviceName : String,withHeader header : [String : String], params : [String : Any]?, success:@escaping (JSON) -> Void, failure:@escaping (CustomErrorStruct) -> Void) {
        if !Rechability.isConnectedToNetwork() {
            return
        }
        
        
        Alamofire.request(serviceName, method:.post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: {
            response in
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                var statusCode = 0
                if let statsCode = resJson["code"].int{
                    statusCode = statsCode
                } else if let statsCode = resJson["statusCode"].int{
                    statusCode = statsCode
                }
                switch statusCode
                {
                case 137 :
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: "You have entered same phone number 3 or more times. Please try a new number", code: statusCode)
                    failure(error)
                case 138 :
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                case 139 :
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: "You have tried three or more failed attempts.", code: statusCode)
                    failure(error)
                case 140 :
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: "Abuse of device", code: statusCode)
                    failure(error)
                case 200..<204:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                case 204:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 205..<300:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                    
                case 300..<440:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 440: break;
                    
                default: break;
                }
            }
            if response.result.isFailure {
                guard let error = response.result.error else { return }
                let customError = CustomErrorStruct(localizedTitle: "Error", localizedDescription: error.localizedDescription, code: 500)
                failure(customError)
            }
        })
    }
    
    class func requestGETURL(serviceName : String,withHeader header : [String:String], success:@escaping (JSON) -> Void, failure:@escaping (CustomErrorStruct) -> Void) {
        if !Rechability.isConnectedToNetwork() {
            return
        }
        let strURL = SERVICE.BASE_URL + serviceName
        Alamofire.request(strURL, method:.get, parameters:nil, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: {
            response in
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                var statusCode = 0
                if let statsCode = resJson["code"].int{
                    statusCode = statsCode
                } else if let statsCode = resJson["statusCode"].int{
                    statusCode = statsCode
                }
                switch statusCode
                {
                case 200..<204:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                case 204:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 205..<300:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                    
                case 300..<440:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 440: break
                    
                default: break
                }
            }
            if response.result.isFailure {
                guard let error = response.result.error else { return }
                let customError = CustomErrorStruct(localizedTitle: "Error", localizedDescription: error.localizedDescription, code: 500)
                failure(customError)
            }
        })
    }
    
    class func requestDeleteURL(serviceName : String, withHeader header : [String:String], success:@escaping (JSON) -> Void, failure:@escaping (CustomErrorStruct) -> Void) {
        if !Rechability.isConnectedToNetwork() {
            return
        }
        let strURL = SERVICE.BASE_URL + serviceName
        Alamofire.request(strURL, method:.delete, parameters:nil, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: {
            response in
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                var statusCode = 0
                if let statsCode = resJson["code"].int{
                    statusCode = statsCode
                } else if let statsCode = resJson["statusCode"].int{
                    statusCode = statsCode
                }
                switch statusCode
                {
                case 200..<204:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                case 204:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 205..<300:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                    
                case 300..<440:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 440: break
                    
                default: break
                }
            }
            if response.result.isFailure {
                guard let error = response.result.error else { return }
                let customError = CustomErrorStruct(localizedTitle: "Error", localizedDescription: error.localizedDescription, code: 500)
                failure(customError)
            }
        })
    }
    
    class func updloadPhoto(withPhoto photo: UIImage, andName name : String,success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        let strURL = ChatAppConstants.uploadProfilePic
        AFWrapper.updatePhotoByUsingMultipart(serviceName: strURL, image: photo, andName:name, success: { (response) in
            success(response)
        }, failure: { (error) in
            failure(error)
        })
    }
    
    class func updatePhotoByUsingMultipart(serviceName : String, image:UIImage, andName name :String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        if !Rechability.isConnectedToNetwork() {
            return
        }
        let strURL = ChatAppConstants.uploadMultimediaURL + serviceName
        let imageData = UIImageJPEGRepresentation(image, 0.4)
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(imageData!, withName: "photo", fileName: "\(name).jpeg", mimeType: "image/*|video/*|audio/*")
        },
                         usingThreshold:UInt64.init(),
                         to:strURL,
                         method:.post,
                         headers:nil,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseString(completionHandler: { response in
                               // let resJson = JSON(response.result.value!)
                                    guard let resJson:JSON = JSON(response.result.value as? Any) else {
                                        NSLog("Result value in response is nil")
                                       // completionHandler(response: nil)
                                        return
                                    }
                                    
                                    success(resJson)
                                })
                            case .failure(let encodingError):
                                failure(encodingError)
                            }
        })
    }
    
    class func updateVideoByUsingMultipart(serviceName : String, videoURL:NSURL, success:@escaping (JSON) -> Void, failure:@escaping (NSError) -> Void) {
        if !Rechability.isConnectedToNetwork() {
            return
        }
        let strURL = ChatAppConstants.constantURL + serviceName
        let mediaURL = videoURL as URL
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(mediaURL, withName: "photo", fileName: "video123.mp4", mimeType: "image/*|video/*|audio/*")
        },
                         usingThreshold:UInt64.init(),
                         to:strURL,
                         method:.post,
                         headers:nil,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseString(completionHandler: { response in
                                    
                                })
                            case .failure(let encodingError): break
                            }
        })
    }
    
    class func updload(withMedia mediaURL: NSURL, andName name : String, withExtension mediaExtension : String, progress : @escaping (Progress) -> Void, success:@escaping (String) -> Void, failure:@escaping (Error) -> Void) {
        let strURL = ChatAppConstants.uploadProfilePic //ChatAppConstants.uploadVideos
        let selfID = Helper.getMQTTID()!
        let mediaName = "\(selfID)000\(name)\(mediaExtension)"
        AFWrapper.updateMediaByUsingMultipart(serviceName: strURL, mediaURL: mediaURL, andName: mediaName, progress: progress, success: { (response) in
            success(mediaName)
        }, failure: { (error) in
            failure(error)
        })
    }
    
    private class func updateMediaByUsingMultipart(serviceName : String, mediaURL:NSURL, andName name:String, progress : @escaping (Progress) -> Void, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        if !Rechability.isConnectedToNetwork() {
            Helper.hideProgressIndicator()
            return
        }
        let strURL = ChatAppConstants.uploadMultimediaURL + serviceName //ChatAppConstants.uploadProfilePic   /
        let mediaURL = mediaURL as URL
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(mediaURL, withName: "photo", fileName: name, mimeType: "image/*|video/*|audio/*")
        },
                         usingThreshold:UInt64.init(),
                         to:strURL,
                         method:.post,
                         headers:nil,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.uploadProgress(closure: { (progres) in
                                    progress(progres)
                                })
                                upload.responseString(completionHandler: { response in
                                    if response.result.isSuccess {
                                        let resJson = JSON(response.result.value!)
                                        success(resJson)
                                    } else {
                                        let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: "Unable to upload the image", code: 403)
                                        failure(error)
                                    }
                                })
                            case .failure(let encodingError):
                                
                                print("****")
                                print("\(encodingError.localizedDescription)")
                                failure(encodingError)
                            }
        })
    }
    
    class func requestPutURL(serviceName : String,withParameters params : [String:Any], header : [String : String], success:@escaping (JSON) -> Void, failure:@escaping (CustomErrorStruct) -> Void) {
        if !Rechability.isConnectedToNetwork() {
            return
        }
        Alamofire.request(serviceName, method:.put, parameters:params, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: {
            response in
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                var statusCode = 0
                if let statsCode = resJson["code"].int{
                    statusCode = statsCode
                } else if let statsCode = resJson["statusCode"].int{
                    statusCode = statsCode
                }
                switch statusCode
                {
                case 200..<204:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                case 204:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 205..<300:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                    
                case 300..<440:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 440: break
                    
                default: break
                }
            }
            if response.result.isFailure {
                guard let error = response.result.error else { return }
                let customError = CustomErrorStruct(localizedTitle: "Error", localizedDescription: error.localizedDescription, code: 500)
                failure(customError)
            }
        })
    }
}
