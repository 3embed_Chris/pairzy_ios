//
//  ImageMessageViewModal.swift
//  MQTT Chat Module
//
//  Created by Sachin Nautiyal on 18/01/2018.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher
import UICircularProgressRing

class ImageMessageViewModal : NSObject {
    
    /// Message Instance for storing image Object.
    let message : Message
    
    /// Flag for maintaining the image status.
    var mediaState:MediaStates {
        return self.message.mediaStates
    }
    
    /// Current image message type, It can be image, gif, doodle, sticker.
    var msgType : String!
    var isGroup:Bool = false
    var groupMembers:[[String:Any]]?
    var gpImage:String?
    
    /// Initiaizing the message object with the Message object.
    ///
    /// - Parameter message: Message Object
    init(withMessage message: Message) {
        self.message = message
    }
    
    /// This will create image object, with passed image.
    ///
    /// - Parameter image: Image you want to send to the receiver.
    fileprivate func CreateImageMessageObject(withImage image : UIImage, replyingMsg : Message?, isReplyingMessage : Bool?) {
        let userDocVMObject = UsersDocumentViewModel(couchbase: Couchbase.sharedInstance)
        // fetching user data from user doc.
        guard let userData = userDocVMObject.getUserData() else { return }
        
        /// Creating data from image.
        let imgData: NSData = NSData(data: UIImageJPEGRepresentation((image), 0.2)!)
        
        /// Fetching image size.
        let imageSize = imgData.length
        var params = [String :Any]()
        params["from"] = self.message.messageFromID! as Any
        params["to"] = self.message.messageToID! as Any
        params["payload"] = self.message.mediaURL! as Any
        params["toDocId"] = self.message.messageDocId! as Any
        params["timestamp"] = self.message.timeStamp! as Any
        params["id"] = self.message.timeStamp! as Any
        params["type"] = msgType as Any
        params["thumbnail"] = self.message.thumbnailData! as Any
        params["dataSize"] = imageSize as Any
        params["userImage"] = userData["userImageUrl"]! as Any
        params["name"] = userData["userName"]! as Any
        params["receiverIdentifier"] = userData["userName"]! as Any
        //Sending image to receiver.
        
        if isReplyingMessage == true, let replyMsg = replyingMsg {
            if let pPload = replyMsg.messagePayload, let pFrom = replyMsg.messageFromID, let prIdentifier = replyMsg.receiverIdentifier, let msgId = replyMsg.timeStamp, let previousType = replyMsg.messageType {
                params["previousPayload"] = pPload as Any
                params["previousFrom"] = pFrom as Any
                params["replyType"] = msgType as Any
                params["type"] = "10" as Any
                params["previousReceiverIdentifier"] = prIdentifier as Any
                params["previousId"] = msgId as Any
                params["previousType"] = "\(previousType.hashValue)" as Any
                if previousType == .replied {
                    if let pType = replyMsg.repliedMessage?.replyMessageType {
                        params["previousType"] = "\(pType.hashValue)" as Any
                    }
                }
                if previousType == .image || previousType == .doodle || previousType == .video {
                    if let tData = replyMsg.thumbnailData {
                        params["previousPayload"] = tData
                    }
                } else if previousType == .location {
                    params["previousPayload"] = "Location"
                }
                else if previousType == .replied {
                    if let repliedMsg = replyMsg.repliedMessage {
                        if repliedMsg.replyMessageType == .image || repliedMsg.replyMessageType == .doodle || repliedMsg.replyMessageType == .video {
                            if let tData = replyMsg.thumbnailData {
                                params["previousPayload"] = tData
                            }
                        } else if repliedMsg.replyMessageType == .location {
                            params["previousPayload"] = "Location"
                        }
                    }
                }
            }
        }
        self.sendImageToReceiver(withData: params)
    }
    
    
    
    func sendMessageThroughAPI(requestParms:[String:Any]) {
        
        Helper.showProgressIndicator(withMessage:StringConstants.sendingMsg())
        
        API.requestPOSTURL(serviceName: APINAMES.sendMsgWithOutMatch,
                           withStaticAccessToken:false,
                           params: requestParms,
                           success: { (response) in
                            print("response:")
                            if let statuscode = response["code"] as? Int {
                                if statuscode == 401 {
                                    Helper.changeRootVcInVaildToken()
                                }
                            }
                            
                            Helper.hideProgressIndicator()
        },
                           failure: { (Error) in
                            Helper.hideProgressIndicator()
        })
    }
    
    /// Send message to receiver.
    ///
    /// - Parameter data: Contains all the details of the message.
    fileprivate func sendImageToReceiver(withData data: [String :Any]) {
        //self.
        if isGroup == false{
         MQTTChatManager.sharedInstance.sendMessage(toChannel:"\(ChatAppConstants.MQTT.messagesTopicName)\(self.message.messageToID!)", withMessage: data, withQOS: .atLeastOnce)
        }else {
            
//            DispatchQueue.global().async {
//             guard let userID = Helper.getMQTTID() else { return }
//                guard let groupMems = self.groupMembers else {return}
//                var msg = data
//                msg["userImage"] = self.gpImage ?? ""
//                for member in  groupMems {
//                    if member["memberId"] as? String   == userID{} else {
//                        guard   let reciverID = member["memberId"] as? String else {return}
//
//                        MQTTChatManager.sharedInstance.sendGroupMessage(toChannel:"\(reciverID)" , withMessage: msg, withQsos: .atLeastOnce)
//                    }
//                }
//                MQTTChatManager.sharedInstance.sendGroupMessageToServer(toChannel: "\(self.message.messageToID!)", withMessage: msg, withQsos: .atLeastOnce)
//            }
            
        }
    }
    
    /// Update media state for message to the chat Document.
    ///
    /// - Parameters:
    ///   - messageObj: Modified message object.
    ///   - docID: Document ID.
    fileprivate func updateMediaStates(forMessage messageObj : Message, andDocID docID: String) {
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: Couchbase.sharedInstance)
        chatsDocVMObject.updateMediaStatesForMessage(inChatDocID: docID, withMessage: messageObj)
    }
    
    /// Upload image to server and send the message to the receiver.
    ///
    /// - Parameters:
    ///   - uploadingBlock: Will return the message object with uploading states.
    ///   - completionBlock: Will return the message object with complete media states.
    func uploadImage(withChatDocID chatDocId: String, replyingMsg: Message?, isReplyingMessage: Bool?, progress : @escaping (Progress) -> Void, Uploadcompletion : @escaping(Bool) -> Void ) {
        let msgCopy = self.message
        switch msgCopy.mediaStates! {
        case .uploaded:
            break
        case .downloaded:
            break
        case .uploading :
            break
        case .downloading :
            break
        case .uploadCancelledBeforeStarting:
            break

        default:
        var existingImage : UIImage!
        self.getImage { (image) in
            existingImage = image
            // Uplaoding started Show in the message.
            let msgCopy = self.message
            msgCopy.mediaStates = .uploading
            // After getting image from cache, changing the message status and starting upload.
            self.updateMediaStates(forMessage: msgCopy, andDocID: chatDocId)
            self.uploadImage(existingImage, progress: progress, completion: { (url) in
                // Upload image to database message.
                if let imgURl = url {
                    let msgCopy = self.message
                    msgCopy.mediaURL = imgURl.toBase64()
                    msgCopy.mediaStates = .uploaded
                    self.updateMediaStates(forMessage: msgCopy, andDocID: chatDocId)
                    // Send image message to the receiver.
                    self.CreateImageMessageObject(withImage: existingImage, replyingMsg: replyingMsg, isReplyingMessage: isReplyingMessage)
                    Uploadcompletion(true)
                } else {
                    let msgCopy = self.message
                    msgCopy.mediaStates = .notUploaded
                    self.updateMediaStates(forMessage: msgCopy, andDocID: chatDocId)
                    Uploadcompletion(false)
                }
            })
        }
    }
    }
    
    /// Used for Upl                                                          oading the image to server
    ///
    /// - Parameters:
    ///   - image: Image you want to update it again.
    ///   - completion: If succeeded then will contain URL or nil.
    private func uploadImage(_ image: UIImage, progress : @escaping (Progress) -> Void, completion: @escaping (String?) -> Void) {
        let name  = arc4random_uniform(900000) + 100000
        let timeStamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))
        AFWrapper.updloadPhoto(withPhoto: image, andName: "\(name)\(timeStamp)", success: { (response) in
            let fileName = "\(name)\(timeStamp).jpeg"
            let url = "\(AppConstant.uploadImageURLExtension)/\(fileName)"
            completion(url)
        }, failure: { (error) in
            completion(nil)
        })
    }
    
    /// Used for re-uploading the image to server
    ///
    /// - Parameters:
    ///   - image: Image you want to update it again.
    ///   - completion: If succeeded then it will contain URL or nil.
    func retryUploadingimage(withProgress progress: @escaping (Progress) -> Void, completion: @escaping (String?) -> Void) {
        self.getImage { (img) in
            if let image = img {
                self.uploadImage(image, progress: progress, completion: completion)
            }
        }
    }
    
    func getRepliedImage(withCompletion completion: @escaping (UIImage?) -> Void) {
        guard let uniqueID = self.message.repliedMessage?.previousId else { return }
        var imageName = "hola"+"\(uniqueID)"+".jpg"
        if self.message.messageType == .gif || self.message.messageType == .sticker {
            imageName = "hola"+"\(uniqueID)"+".gif"
        }
        if self.message.messageType == .replied {
            if let repliedMsg = self.message.repliedMessage {
                if repliedMsg.replyMessageType == .gif || repliedMsg.replyMessageType == .sticker {
                    imageName = "hola"+"\(uniqueID)"+".gif"
                }
            }
        }
        
        if ImageCache.default.imageCachedType(forKey: imageName).cached {
            ImageCache.default.retrieveImage(forKey: imageName, options: nil, completionHandler: { image, _ in
                completion(image)
            })
        } else {
            completion(nil)
        }
    }
        
    func getImage(withCompletion completion: @escaping (UIImage?) -> Void) {
        guard let uniqueID = self.message.uniquemessageId else { return }
        var imageName = "hola"+"\(uniqueID)"+".jpg"
        if self.message.messageType == .gif || self.message.messageType == .sticker {
            imageName = "hola"+"\(uniqueID)"+".gif"
        }
        if self.message.messageType == .replied {
            if let repliedMsg = self.message.repliedMessage {
                if repliedMsg.replyMessageType == .gif || repliedMsg.replyMessageType == .sticker {
                    imageName = "hola"+"\(uniqueID)"+".gif"
                }
            }
        }
        
        if ImageCache.default.imageCachedType(forKey: imageName).cached {
            ImageCache.default.retrieveImage(forKey: imageName, options: nil, completionHandler: { image, _ in
                completion(image)
            })
        } else {
            completion(nil)
        }
    }
}
