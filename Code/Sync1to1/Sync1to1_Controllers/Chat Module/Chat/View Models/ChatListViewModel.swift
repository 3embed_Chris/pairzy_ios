//
//  ChatListViewModel.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 01/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import UserNotifications
import CocoaLumberjack

class ChatListViewModel: NSObject {
    
    public var chats: [Chat]
    
    struct  Constants {
        static let cellIdentifier = "ChatListTableViewCell"
        static let errorMsg = "Something went wrong"
        static let notificationNotAllowedError = "Notifications not allowed"
    }
    
    let couchbaseObj = Couchbase.sharedInstance
    
    var userName : String? {
        guard let userName = Helper.getUserName() as? String else { return nil }
        return userName
    }
    
    init(withChatObjects chats : [Chat]) {
        /// Sorting chat by their last message date timestamp.
        let shortedChats = chats.sorted { (chat1, chat2) -> Bool in
            guard let uniqueChatID1 = Int64(chat1.lastMessageDate), let uniqueChatID2 = Int64(chat2.lastMessageDate) else { return false }
            return  uniqueChatID1 > uniqueChatID2
        }
        self.chats = shortedChats
    }
    
    func setupNotificationSettings() {
        // Specify the notification types.
        let center = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound, .badge];
        center.requestAuthorization(options: options) {
            (granted, error) in
            if !granted {
                DDLogDebug(Constants.errorMsg)
            }
        }
    }
    
    func unreadChatsCounts() -> Int {
        var unreadChats = [Chat]()
        for chat in self.chats {
            if chat.hasNewMessage {
                unreadChats.append(chat)
            }
        }
        return unreadChats.count
    }
    
    func authCheckForNotification () {
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { (settings) in
            if settings.authorizationStatus != .authorized {
                // Notifications not allowed
                DDLogDebug(Constants.notificationNotAllowedError)
            }
        }
    }
    
    func getNumberOfRows() -> Int {
        return self.chats.count
    }
    
    func deleteChat(fromIndexPath indexPath :IndexPath) {
        self.chats.remove(at: indexPath.row)
    }
    
    func deleteRow(fromIndexPath indexPath :IndexPath, success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void) {
        let dataObject = self.chats[indexPath.row]
        let chatVMObj = ChatViewModel(withChatData: dataObject)
        chatVMObj.deleteChat(success: { response in
            success(response)
        }) { error in
            failure(error)
        }
    }
    
    func setUpTableViewCell(indexPath : IndexPath, tableView : UITableView) -> UITableViewCell {
        let dataObject = self.chats[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "DatumChatTableCell") as! ChatListTableViewCell
        cell.chatVMObj = ChatViewModel(withChatData: dataObject)
        return cell
    }
}
