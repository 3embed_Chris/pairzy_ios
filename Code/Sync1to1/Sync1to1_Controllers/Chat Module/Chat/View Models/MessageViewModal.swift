//
//  MessageViewModal.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 23/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import JSQMessagesViewController

class MessageViewModal {
    
    let couchbaseObj = Couchbase.sharedInstance
    let message : Message!
    
    let selfID = Helper.getMQTTID()
    
    /// Initiaizing the message object with the Message object.
    ///
    /// - Parameter message: Message Object
    init(withMessage message: Message) {
        self.message = message
    }
    
    func addLastDateAndTime(toCell cell: JSQMessagesCollectionViewCell) -> JSQMessagesCollectionViewCell {
        cell.timeLabelOutlet?.attributedText = self.setDateTime()
        cell.currentStatusOutlet?.attributedText = self.setReadStatus()
        cell.backgroundColor = .clear
        return cell
    }
    
    func setDay()  -> NSAttributedString {
        guard let timestamp = self.message.timeStamp else { return NSAttributedString() }
        let dayStr = DateHelper().getDateString(fromTimeStamp: timestamp)
        let attribute = [kCTForegroundColorAttributeName : UIColor.darkGray]
        let str = NSAttributedString(string: dayStr, attributes: attribute as [NSAttributedStringKey : Any])
        return str
    }
    
    func setDateTime()  -> NSAttributedString {
        guard let timestamp = message.timeStamp else { return NSAttributedString() }
        let lastmsgDate = DateHelper().getDateObj(fromTimeStamp: timestamp)
        let date = DateHelper().lastMessageInHours(date: lastmsgDate)
        let attribute = [kCTForegroundColorAttributeName : UIColor.black]
        let str = NSAttributedString(string: date, attributes: attribute as [NSAttributedStringKey : Any])
        return str
    }
    
    func setReadStatus() -> NSAttributedString {
        let attribute = [kCTForegroundColorAttributeName : UIColor.black]
        if message.isSelfMessage {
            var str = NSAttributedString(string: "✔︎", attributes: attribute as [NSAttributedStringKey : Any])
            if message.messageStatus == "1" {
                let attribute = [kCTForegroundColorAttributeName : UIColor.blue]
                str = NSAttributedString(string: "✔︎", attributes: attribute as [NSAttributedStringKey : Any])
                return str
            }
            if message.messageStatus == "2" {
                let attribute = [kCTForegroundColorAttributeName : UIColor.blue]
                str = NSAttributedString(string: "✔︎✔︎", attributes: attribute as [NSAttributedStringKey : Any])
                return str
            }
            if message.messageStatus == "3" {
                let attribute = [kCTForegroundColorAttributeName : UIColor.red]
                str = NSAttributedString(string: "✔︎✔︎", attributes: attribute as [NSAttributedStringKey : Any])
                return str
            }
            return str
        }
        let str = NSAttributedString(string: "", attributes: attribute as [NSAttributedStringKey : Any])
        return str
    }
    
}
