//
//  ChatViewController.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 10/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//


import Foundation
import UIKit
import JSQMessagesViewController
import Kingfisher
import CocoaLumberjack
import RxCocoa
import RxSwift
import ReachabilitySwift
import Contacts
import AVKit
import Locksmith

class ChatViewController: JSQMessagesViewController {
    
    /// Used Constants for default values.
    struct Constants {
        
        /// Default message size.
        static let messagePageSize = "100"
        
        /// To Counter Offer Controller Segue
        static let toCounterOfferSegue = "toCounterOfferSegue"
        
        /// To Location Picker Controller Segue
        static let toLocationPicker = "toLocationPicker"
        
        /// To Paypal View Controller Segue.
        static let toPaypalViewcontroller = "toPaypalViewcontroller"
        
        /// To Paypal Web View Controller Segue.
        static let paypalWebViewSegue = "paypalWebViewSegue"
        
        /// To image controller segue.
        static let imageControllerSegue = "imageControllerSegue"
        
        /// To Showing Location Segue.
        
//        static let toContactDetailsSegue = "toContactDetailsSegue"
//        static let chatTogroupInfoSegue = "chatToGroupInfoView"
//        static let documentViewerSegue = "documentViewerSegue"
    }
    
 
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var TitleLabelOutlet: UILabel!
    @IBOutlet weak var currentChatStatusLabelOutlet: UILabel!
    @IBOutlet var titleView: UIView!
    @IBOutlet weak var audiobutton: UIBarButtonItem!
    @IBOutlet weak var videoButton: UIBarButtonItem!
    
    let matchesVm = MatchMakerVM()
   
    var userHasActiveDate = 0
    var selecteDateType:DateType = .none
    var animateOnlyOnce = true
    
    let couchbaseObj = Couchbase.sharedInstance
    let selfID = Helper.getMQTTID()
    let incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor.white)
    let outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor.white)
    let userDefault = UserDefaults.standard
    let mqttChatManager = MQTTChatManager.sharedInstance
    let disposebag = DisposeBag()
   // let recordingView = SKRecordView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width , height: 200))
    
    var refresher : UIRefreshControl!
    var changedFrame : CGRect = CGRect(x: 0, y: UIScreen.main.bounds.height-100, width: UIScreen.main.bounds.width, height: 60) // For replied message.
    var chatsDocVMObject : ChatsDocumentViewModel! {
        return ChatsDocumentViewModel(couchbase: couchbaseObj)
    }
    var chatViewModelObj : ChatViewModel?
    var chatDocID : String!
    var messages = [Message]()
    var receiverID :String!
    var registerNum : String!
    var userName : String!
    var isTypingVisible = false
    var userStatus : String!
    var favoriteObj  :Profile?
    var contactModel : ContactchatViewModel?
    var locationViewModel : LocationPickerViewModel?
    var doodleViewModel: DoodleViewModel?
    var documentViewModel:DocumentViewModel?
    var camerabtn:UIButton?
    var contactobj:CNContact?
    var isGroup:Bool = false
    var isuserBlock:Bool = false
    var callAPIForGetChats = true
    var groupInfo:[String:Any]?
    var groupDocumentID:String?
    var groupImage:String?
    var groupMembers: [[String:Any]]?
    var isActiveGroup:Bool = true
    var isReplying : Bool = false
    var replyingMessage : Message?
    var footerView:UIView?
    var currentPlayingAudioPlayer : AudioPlayerManager?
    var userID : String?
    var profileImage : String?
    var selectedTimerIndex = 0
    
    @IBOutlet weak var datebtnWidthConst: NSLayoutConstraint!
    @IBOutlet weak var datebtnHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateButtonOutlet: UIButton!
    @IBOutlet weak var attachBtn: UIButton!
    @IBOutlet weak var moreBtn: UIButton!
    
    static var iii = 1
    
    
    
    //MARK: View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkIsThereAnyPlayer()
        self.registerXIBs()
        
        let appdele = UIApplication.shared.delegate as! AppDelegate
        appdele.chatVcObject = self
        self.initialSetup()
        self.setTitleName()
        self.removeReplyView()
        self.additionalContentInset.top = 400
        Helper.hidePushNotifications(hidePush: true)
        self.requestForReportUserReasons(userClicked: false)
        self.inputToolbar.contentView?.textView?.placeHolder = StringConstants.newMsg()
        navigationItem.hidesBackButton = true
        Helper.addDefaultNavigationGesture(VC:self)
       // self.addBackgroundImage()
        resheduleBtn!.addTarget(self, action:#selector(tappedOnDateReshduelBtn), for: .touchUpInside)

        if(self.favoriteObj?.isMatchedUser == 0) {
            self.datebtnWidthConst.constant = 64
            self.datebtnHeightConstraint.constant = 26
            self.dateButtonOutlet.setTitleColor(UIColor.black, for: .normal)
            self.dateButtonOutlet.layer.borderColor = Colors.coinBalanceBtnBorder.cgColor
            self.dateButtonOutlet.layer.borderWidth = 1
            self.dateButtonOutlet.layer.cornerRadius = 5
            self.dateButtonOutlet.layer.cornerRadius = 12
            self.dateButtonOutlet.clipsToBounds = true
            updateCoinBalance()
            self.dateButtonOutlet.layoutIfNeeded()
        }
        
        checkAnyActiveDateIsThere()
        
    }
    
    func checkIsThereAnyPlayer() {
        let audioSession = AVAudioSession.sharedInstance()
        if audioSession.isOtherAudioPlaying {
            _ = try? audioSession.setCategory(AVAudioSessionCategoryAmbient, with: AVAudioSessionCategoryOptions.mixWithOthers)
        }
    }
    
    
    func checkAnyActiveDateIsThere() {
        
        if let hasUpcomingDates = UserDefaults.standard.value(forKey:"upcomingDatesDetails") {
            if let profileResponse = hasUpcomingDates as? [AnyObject] {
                for (index, _) in profileResponse.enumerated() {
                    let profileDet = profileResponse[index] as! [String:Any]
                     print("")
                    if let dateUserId = profileDet["opponentId"] as? String {
                        if dateUserId == self.receiverID {
                            userHasActiveDate = 1
                            let respDate = SyncDate.init(profileDetails:profileDet)
                            updateDateDetails(dateDetails:respDate)
                        }
                    }
                }
            }
        }
        
        
        if let hasDates = UserDefaults.standard.value(forKey:"pendingDateDatesDetails") {
            if let profileResponse = hasDates as? [AnyObject] {
                for (index, _) in profileResponse.enumerated() {
                    let profileDet = profileResponse[index] as! [String:Any]
                    print("")
                    if let dateUserId = profileDet["opponentId"] as? String {
                        if dateUserId == self.receiverID {
                            // user has an active upcoming Date.
                            print("found an date")
                            userHasActiveDate = 2
                            let respDate = SyncDate.init(profileDetails:profileDet)
                            updateDateDetails(dateDetails:respDate)
                        }
                    }
                }
            }
        }
    }
    
   @objc func tappedOnDateReshduelBtn() {
        print("tappped on reshedule btn")
    if(userHasActiveDate == 1 || userHasActiveDate == 2) {
        //there is upcoming date availble.
        //there is pending date availble.
        
        self.tabBarController?.selectedIndex = 2
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when){
            let notificationName = NSNotification.Name(rawValue: "openDateTab")
            NotificationCenter.default.post(name: notificationName, object: self.userHasActiveDate, userInfo: nil)
        }
        self.navigationController?.popViewController(animated: true)
    }
    }
    
    
    func updateDateDetails(dateDetails:SyncDate) {
        // user has an active upcoming Date.
        
        //1 - is for video date.
        //2- is for meet date.
        //3 - is for audio date.
        
        print("found an date")
        self.resheduleBtn?.setTitle(StringConstants.view(), for: .normal)
        resheduleBtn?.setTitleColor(Colors.AppBaseColor, for: .normal)
        self.heightForDateView?.constant = 64
        self.resheduleBtn?.isHidden = false
        self.viewForDateDetails?.isHidden = false
        if dateDetails.dateType == 2 {
            self.dateTypeLabel?.text = dateDetails.placeName
        } else {
            self.dateTypeLabel?.text = dateDetails.requestedFor
        }
       
        let  date:Date = Date(timeIntervalSince1970: Double(dateDetails.dateTime))
        //adding date and time string.
        let fullDateTime = CalenderUtility.getDateAndTime(fromDate: date)
        self.dateTimeLabel?.text = fullDateTime
    }
    
   @objc func updateCoinBalance(){
        
        self.dateButtonOutlet.setImage(#imageLiteral(resourceName: "dollar"), for: .normal)
        self.dateButtonOutlet.titleLabel?.font = UIFont (name: CircularAir.Bold, size:12)
        
        var balance = " 0"
        if let getBal = UserDefaults.standard.object(forKey: "coinBalance") as? Int {
            if getBal > 1000 {
                let floatVal = Float(Float(getBal)/1000.0)
                let inter = floor(floatVal)
                let decimal = floatVal.truncatingRemainder(dividingBy: 1)
                if decimal == 0.0{
                    balance = "\(Int(inter))k"
                }else {
                    balance = "\(Float(Float(getBal)/1000.0))k"
                }
            } else {
                balance = " " + String(getBal)
            }
            self.dateButtonOutlet.setTitle(balance, for: .normal)
        } else {
            self.dateButtonOutlet.setTitle(" 0", for: .normal)
        }
    }
    
    func setTitleName() {
        self.userName = self.getReceiverName()
        self.TitleLabelOutlet.text = self.userName
        if chatViewModelObj?.secretID != "" && chatViewModelObj != nil{
            self.TitleLabelOutlet.attributedText = String.addImageToString(text: self.userName, image: #imageLiteral(resourceName: "Share_Fb"))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.receiverID = self.getReceiverID()
        guard let revID = self.receiverID else { return }
        self.disablePushNotificaation(forUser: self.receiverID)
        mqttChatManager.subscribeTypingChannel(withUserID: self.receiverID)
        mqttChatManager.subscibeToLastSeenChannel(withUserID: self.receiverID)
        
        if self.isGroup {
            if isActiveGroup {
                self.inputToolbar.isHidden = false
            }
        }else {
            if isuserBlock{
                self.inputToolbar.isHidden = true
            }
        }
        self.hideToolBarr()
        
//        self.heightForDateView!.constant = 64
//        self.view.layoutIfNeeded()
//        UIView.animate(withDuration: 5.0, animations:{
//            self.heightForDateView!.constant = 0
//            self.view.layoutIfNeeded()
//        }, completion:nil)
        
        
        if let receiverImageUrl = getProfilePic() {
            if animateOnlyOnce {
                animateOnlyOnce = false
                
                //start animation.
                if(self.messages.count <= 0) {
                    
                    let fullView = UIView(frame:(self.collectionView?.bounds)!)
                    self.collectionView?.backgroundView = fullView
                    
                    let animateView = UIView(frame: CGRect(x: 20, y:((self.collectionView?.frame.size.height)!/2)-125, width:(self.collectionView?.frame.size.width)! - 40, height:250))
                    fullView.addSubview(animateView)
                    
                    let profileImage = UIImageView(frame:CGRect(x: animateView.frame.size.width/2-75, y: 50, width:150, height:150))
                    profileImage.layer.cornerRadius = 75
                    profileImage.contentMode = .scaleAspectFit
                    profileImage.clipsToBounds = true
                    profileImage.layoutIfNeeded()
                    
                    if (self.favoriteObj?.isMatchedUser == 1) {
                        let titleTextLabel = UILabel(frame:CGRect(x:20, y:0, width: animateView.frame.size.width-40, height:30))
                        titleTextLabel.textAlignment = NSTextAlignment.center
                        titleTextLabel.font = UIFont(name: "CircularAirPro-Bold",
                                                     size: 24.0)
                        titleTextLabel.numberOfLines = 1
                        titleTextLabel.adjustsFontSizeToFitWidth=true
                        titleTextLabel.minimumScaleFactor=0.5;
                        titleTextLabel.text = "You Paired with ".appending(getReceiverName()!)
                        animateView.addSubview(titleTextLabel)
                        
                        let messageLabel = UILabel(frame:CGRect(x:20, y:210, width: animateView.frame.size.width-40, height:60))
                        messageLabel.font = UIFont(name: "CircularAirPro-Book",
                                                   size: 20.0)
                        messageLabel.numberOfLines = 0
                        titleTextLabel.adjustsFontSizeToFitWidth=true
                        messageLabel.minimumScaleFactor=0.5;
                        //messageLabel.text = areYouWaiting()
                        //"Tell them why you swiped right"
                        messageLabel.text = " "
                        messageLabel.textAlignment = NSTextAlignment.center
                        animateView.addSubview(messageLabel)
                    }
                    
                    let url = URL(string : receiverImageUrl)
                    profileImage.kf.setImage(with: url, placeholder:#imageLiteral(resourceName: "02m"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
                    animateView.addSubview(profileImage)
                    
                    
                    let rightSideHeart = UIImageView(frame:CGRect(x: profileImage.frame.origin.x-45, y:40, width:45, height:75))
                    rightSideHeart.image =  #imageLiteral(resourceName: "chatNonConverstaion")
                    animateView.addSubview(rightSideHeart)
                    
                    
                    let leftSideHeart = UIImageView(frame:CGRect(x: profileImage.frame.origin.x+150, y:140, width:45, height:75))
                    leftSideHeart.image =  #imageLiteral(resourceName: "chatNonConverstaionRight")
                    animateView.addSubview(leftSideHeart)
                    
                    
                    
                    animateView.transform = CGAffineTransform(scaleX: 0, y:0)
                    UIView.animate(withDuration:1.0, delay: 0.0, usingSpringWithDamping: 0.5,
                                   initialSpringVelocity: 0.5, options: [], animations:
                        {
                            animateView.transform = CGAffineTransform(scaleX: 1.0, y:1.0)
                    }, completion:nil)
                }
            }
        }
         checkForBlock()
    }
    
    func findIsThisUserIsMatched() {
        var allMatchesList = ""
        let matchesDocumentVm = MatchesDocumentViewModel(couchbase: Couchbase())
        
        let matchesDocIdKey = "userMatchData/"
        let matchDocID = UserDefaults.standard.value(forKey: matchesDocIdKey)
        
        if let mDocID = matchDocID as? String {
            let matchesData = matchesDocumentVm.getMatchData(forDocId:mDocID)
            
            if let profileResponse = matchesData!["matchesArray"] as? [AnyObject] {
                for (index, _) in profileResponse.enumerated() {
                    let profileDetails = profileResponse[index] as! [String:Any]
                    if let userid = profileDetails["opponentId"] as? String {
                        allMatchesList = allMatchesList.appending(userid).appending(",")
                    } else  if let userid = profileDetails["_id"] as? String {
                        allMatchesList = allMatchesList.appending(userid).appending(",")
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Helper.updateChatOpenStatus(isChatOpen:true)
       

        self.inputToolbar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        
        self.collectionView?.backgroundColor = Helper.getUIColor(color:"F4F5F8")
        self.addRefresher()
        registerObservers()
        if let isNetwork = (UIApplication.shared.delegate as? AppDelegate)?.isNetworkThere {
            if isNetwork {
                if self.audiobutton != nil {
                    self.audiobutton.isEnabled  = true
                    self.videoButton.isEnabled = true }
                if  !isuserBlock  {
                    self.currentChatStatusLabelOutlet.isHidden = false }
            } else {
                if self.audiobutton != nil {
                    self.audiobutton.isEnabled = false
                    self.videoButton.isEnabled = false}
                if  !isuserBlock  {
                    self.currentChatStatusLabelOutlet.isHidden = true }
            }
        }
        Reachability.rx.isReachable
            .subscribe(onNext: { isReachable in
                if isReachable == true {
                    if self.audiobutton != nil {
                        self.audiobutton.isEnabled  = true
                        self.videoButton.isEnabled = true}
                    if  !self.isuserBlock  {
                        self.currentChatStatusLabelOutlet.isHidden = false}
                } else {
                    if self.audiobutton != nil {
                        self.audiobutton.isEnabled = false
                        self.videoButton.isEnabled = false}
                    if  !self.isuserBlock  {
                        self.currentChatStatusLabelOutlet.isHidden = true}
                }
            }).addDisposableTo(disposebag)
        
        if !isGroup {
            self.setTitleName()
        }
        self.tabBarController?.tabBar.isHidden = true
        //self.setupVoiceRecordUI()
        if self.isGroup {
            if isActiveGroup {
                self.inputToolbar.isHidden = false
            }
        } else {
            if isuserBlock{
                self.inputToolbar.isHidden = true
            }
        }
        
        self.automaticallyScrollsToMostRecentMessage = true
        self.addCameraBtn()
        //self.addBackgroundImage()
        self.jsq_register(forNotifications: true)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardFrameWillChange(notification:)), name: .UIKeyboardWillChangeFrame, object: nil)
        
        
       if(self.favoriteObj?.isMatchedUser == 0) {
            updateCoinBalance()
        }
        checkForBlock()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let chatDocID = self.chatDocID {
            self.updateChatForReadMessage(toDocID: chatDocID, isControllerAppearing: false)
        }
        if let receiverID = self.receiverID {
            mqttChatManager.unsubscribeTypingChannel(withUserID: receiverID)
            mqttChatManager.unsubscibeToLastSeenChannel(withUserID: receiverID)
        }
        self.automaticallyScrollsToMostRecentMessage = false
        
        if let player = self.currentPlayingAudioPlayer {
            player.stop(clearQueue: true)
        }
        
        
        self.inputToolbar.isHidden = true
    }
    
    
    
    @IBAction func openProifleButtonAction(_ sender: Any) {
        openReceiverProfile()
    }
    
    
    func openReceiverProfile() {
        if let profileId  = getReceiverID() {
            let profileVc = Helper.getSBWithName(name:"DatumTabBarControllers").instantiateViewController(withIdentifier:StoryBoardIdentifier.profileVc) as! ProfileDetailVC
            profileVc.profileDeatilVM.dataAvailable = false
            profileVc.profileDeatilVM.showProfileByID = false
            profileVc.profileFromChat = true
            profileVc.profileDeatilVM.profileDetails.userId = profileId
            self.navigationController?.present(profileVc, animated: true, completion:nil)
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        Helper.updateChatOpenStatus(isChatOpen:false)
        Helper.hidePushNotifications(hidePush: false)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UserDefaults.standard.set("", forKey: UserDefaultsKeys.isUserOnchatscreen)
        UserDefaults.standard.synchronize()
        removeObservers()
        if let tabbar = self.tabBarController as? TabBarController {
            tabbar.enablePushNotification()
        }
        
        //For seceret Chat Only
        if chatViewModelObj?.secretID != "" && chatViewModelObj != nil{
            //save last Time visited screen & delete it when Chat is deleted..
            UserDefaults.standard.set(Date().timeIntervalSince1970, forKey: "\(String(describing: chatViewModelObj?.secretID))+chatCloseTime")
        }
    }
    
    
    
    @objc func keyboardFrameWillChange(notification: NSNotification) {
        let keyboardEndFrame = ((notification.userInfo! as NSDictionary).object(forKey: UIKeyboardFrameEndUserInfoKey)! as AnyObject).cgRectValue
        let animationCurve = UIViewAnimationCurve(rawValue: ((notification.userInfo! as NSDictionary).object(forKey:UIKeyboardAnimationCurveUserInfoKey)! as AnyObject).integerValue)
        let animationDuration: TimeInterval = ((notification.userInfo! as NSDictionary).object(forKey:UIKeyboardAnimationDurationUserInfoKey)! as AnyObject).doubleValue
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(animationDuration)
        UIView.setAnimationCurve(animationCurve!)
        
        var newFrame = self.view.frame
        let keyboardFrameEnd = self.view.convert(keyboardEndFrame!, to: nil)
        if keyboardFrameEnd.origin.y == self.view.frame.size.height {
            newFrame.origin.y = (keyboardFrameEnd.origin.y-100)
        } else {
            newFrame.origin.y = (keyboardFrameEnd.origin.y-60)
        }
        newFrame.size.height = 60
        changedFrame = newFrame
        self.updateReplyViewFrame()
        UIView.commitAnimations()
    }
    
    
    
    
    
    //MARK: - ADD Chat background Wallpaper
    func addBackgroundImage() {
        if let dict = UserDefaults.standard.object(forKey: UserDefaultsKeys.colourKey) as? [String:Any] {
            if dict["index"] as! String == "0" {
                self.view.backgroundColor = UIColor.init(patternImage: UIImage.init(named: dict["colour"] as! String )!)
            } else if dict["index"] as! String == "1" {
                self.view.backgroundColor =  Helper.hexStringToUIColor(hex: dict["colour"] as! String )
            } else if dict["index"] as! String == "2" {
                let image = UIImage.init(data: dict["colour"] as! Data)
                self.view.backgroundColor = UIColor.init(patternImage: image!)
            } else {
                self.view.backgroundColor = UIColor.init(patternImage: #imageLiteral(resourceName: "defaultBackground"))
            }
        } else {
            self.view.backgroundColor = UIColor.init(patternImage: #imageLiteral(resourceName: "Ellipse 96"))
        }
    }
    
   @objc func inputToolbarCameraCliked() {
        
        if chatViewModelObj?.secretID != "" && chatViewModelObj != nil{
            self.seceretChatTimerSheet()
        }else {
            self.openLivecamera()
        }
        
    }
    
    @IBAction func moreButtonAction(_ sender: Any) {
        //check coins availabel or not.  For creating date user need 100 coins.
        openAlert()
    }

    
    @IBAction func attachmentButtonAction(_ sender: Any) {
        
        if(favoriteObj?.isMatchedUser == 1) {
           openAttachementSheet()
            return
        }
        
        //check coins availabel or not.  For creating date user need 100 coins.
        if let getBal = UserDefaults.standard.object(forKey: "coinBalance") as? Int {
            if getBal >= 100 {
                //user has coins allow user to create date.
                openAttachementSheet()
            } else {
                // has lessthan 100 coins.
                Helper.showAlertWithMessage(withTitle:StringConstants.Oops(), message:StringConstants.noCoinsForMessage(), onViewController:self)
            }
        } else {
            // no coins available.
            Helper.showAlertWithMessage(withTitle:StringConstants.Oops(), message:StringConstants.noCoinsForMessage(), onViewController:self)
        }
    }
    
    @IBAction func dateButtonAction(_ sender: Any) {
        
        if(self.favoriteObj?.isMatchedUser == 0) {
            let storyBoard = Helper.getSBWithName(name:"DatumTabBarControllers")
            
            let userBasics = storyBoard.instantiateViewController(withIdentifier: "addCoin") as! AddCoinsVC
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(userBasics, animated: true)
        } else {
            //check coins availabel or not.  For creating date user need 100 coins.
            if let getBal = UserDefaults.standard.object(forKey: "coinBalance") as? Int {
                if getBal >= 100 {
                    //user has coins allow user to create date.
                    showPopup()
                } else {
                    // has lessthan 100 coins.
                    Helper.showAlertWithMessage(withTitle:StringConstants.Oops(), message:StringConstants.noCoinsForDate(), onViewController:self)
                }
            } else {
                // no coins available.
                Helper.showAlertWithMessage(withTitle:StringConstants.Oops(), message:StringConstants.noCoinsForDate(), onViewController:self)
            }
        }
    }
    
    func showPopup(){
        self.inputToolbar.isHidden = true
        let window = UIApplication.shared.keyWindow!
//        window.endEditing(true)
        let syncView = dateCallPopUpView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        syncView.loadPopUpView()
        syncView.datePopUpDelegate = self
        window.addSubview(syncView)
    }
    
    func registerObservers() {
     
        let nameOfObserVer = NSNotification.Name(rawValue: "updateCoinsBal")
        NotificationCenter.default.setObserver(self, selector: #selector(updateCoinBalance), name: nameOfObserVer, object: nil)

        var name = NSNotification.Name(rawValue: "LastSeen")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.receivedLastSeenStatus(notification:)), name: name, object: nil)
        
        
        name = NSNotification.Name(rawValue: "Typing")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.receivedTypingStatus(notification:)), name: name, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(textViewTextDidChangeNotification), name: NSNotification.Name.UITextViewTextDidChange, object: self.inputToolbar.contentView?.textView)
        
        name = NSNotification.Name(rawValue: "MessageNotification" + selfID!)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.receivedMessage(notification:)), name: name, object: nil)
        
        name = NSNotification.Name(rawValue: "PullToRefresh")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.stopRefresher(notification:)), name: name, object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateGroupInfo), name: NSNotification.Name(rawValue: "updateGroupInfoScreen"), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(blockUpdate), name: NSNotification.Name(rawValue: NSNotificationNames.blockUpdate), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(unBlockUpdate), name: NSNotification.Name(rawValue: NSNotificationNames.unBlockUpdate), object: nil)
        self.textViewTextDidChangeNotification()
    }
    
    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func registerXIBs() {
        super.collectionView?.register(UINib(nibName: "SentLocationCell", bundle: nil), forCellWithReuseIdentifier: "SentLocationCell")
        
        super.collectionView?.register(UINib(nibName: "SentContactMessageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SentContactMessageCollectionViewCell")
        
        super.collectionView?.register(UINib(nibName: "ReceivedContactCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ReceivedContactCollectionViewCell")
        
        super.collectionView?.register(UINib(nibName: "ReceivedLocationCell", bundle: nil), forCellWithReuseIdentifier: "ReceivedLocationCell")
        
        self.collectionView?.register(UINib(nibName: "SNTextMessageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SNTextMessageCollectionViewCell")
        
        super.collectionView?.register(UINib(nibName: "ImageReceivedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageReceivedCollectionViewCell")
        
        super.collectionView?.register(UINib(nibName: "ImageSentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageSentCollectionViewCell")
        
        super.collectionView?.register(UINib(nibName: "StickerReceivedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "StickerReceivedCollectionViewCell")
        
        super.collectionView?.register(UINib(nibName: "StickerSentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "StickerSentCollectionViewCell")
        
        super.collectionView?.register(UINib(nibName: "VideoReceivedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VideoReceivedCollectionViewCell")
        
        super.collectionView?.register(UINib(nibName: "VideoSentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VideoSentCollectionViewCell")
        
        super.collectionView?.register(UINib(nibName: "AudioReceivedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AudioReceivedCollectionViewCell")
        
        super.collectionView?.register(UINib(nibName: "AudioSentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AudioSentCollectionViewCell")
        
        super.collectionView?.register(UINib(nibName: "AudioSentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AudioSentCollectionViewCell")
        
        super.collectionView?.register(UINib(nibName: "DocumentSentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DocumentSentCollectionViewCell")
        
        super.collectionView?.register(UINib(nibName: "DocumentReceivedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DocumentReceivedCollectionViewCell")
        
        // Registering Replied Cells
        
        //Replied Received Text
        super.collectionView?.register(UINib(nibName: "ReceivedTextCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ReceivedTextCollectionViewCell")
        
        //Replied Sent Text
        super.collectionView?.register(UINib(nibName: "SentTextCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SentTextCollectionViewCell")
        
        //Replied Audio Received
        super.collectionView?.register(UINib(nibName: "RepliedAudioReceivedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RepliedAudioReceivedCollectionViewCell")
        
        // Replied Audio Sent
        super.collectionView?.register(UINib(nibName: "RepliedAudioSentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RepliedAudioSentCollectionViewCell")
        
        // Replied Received Contact
        super.collectionView?.register(UINib(nibName: "RepliedReceivedContactCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RepliedReceivedContactCollectionViewCell")
        
        // Replied Sent Contact
        super.collectionView?.register(UINib(nibName: "RepliedSentContactMessageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RepliedSentContactMessageCollectionViewCell")
        
        // Replied Document Received
        super.collectionView?.register(UINib(nibName: "RepliedDocumentReceivedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RepliedDocumentReceivedCollectionViewCell")
        
        // Replied Document Sent
        super.collectionView?.register(UINib(nibName: "RepliedDocumentSentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RepliedDocumentSentCollectionViewCell")
        
        // Replied Image Received
        super.collectionView?.register(UINib(nibName: "RepliedImageReceivedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RepliedImageReceivedCollectionViewCell")
        
        //Replied Image Sent
        super.collectionView?.register(UINib(nibName: "RepliedImageSentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RepliedImageSentCollectionViewCell")
        
        //Replied Received Location
        super.collectionView?.register(UINib(nibName: "RepliedReceivedLocationCell", bundle: nil), forCellWithReuseIdentifier: "RepliedReceivedLocationCell")
        
        //Replied Sent Location
        super.collectionView?.register(UINib(nibName: "RepliedSentLocationCell", bundle: nil), forCellWithReuseIdentifier: "RepliedSentLocationCell")
        
        //Replied Sticker Received
        super.collectionView?.register(UINib(nibName: "RepliedStickerReceivedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RepliedStickerReceivedCollectionViewCell")
        
        //Replied Sticker Sent
        super.collectionView?.register(UINib(nibName: "RepliedStickerSentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RepliedStickerSentCollectionViewCell")
        
        // Replied Video Sent
        super.collectionView?.register(UINib(nibName: "RepliedVideoSentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RepliedVideoSentCollectionViewCell")
        
        //Replied Video Received
        super.collectionView?.register(UINib(nibName: "RepliedVideoReceivedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RepliedVideoReceivedCollectionViewCell")
        
        //Deleted Received
        super.collectionView?.register(UINib(nibName: "DeletedReceivedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DeletedReceivedCollectionViewCell")
        
        //Deleted Sent
        super.collectionView?.register(UINib(nibName: "DeletedSentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DeletedSentCollectionViewCell")
        
        //GpMessage Type
        super.collectionView?.register(UINib(nibName: "GpMessageTypeCell", bundle: nil), forCellWithReuseIdentifier: "GpMessageTypeCell")
    }
    
    /// Used for showing a pull to refresh for refreshing/pulling the chat.
    func addRefresher() {
        self.refresher = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.collectionView!.addSubview(refresher)
    }
    
    @objc func loadData() {
        var timeStamp = Int64(floor(Date().timeIntervalSince1970 * 1000))
        if let ts = self.messages.first?.messageId {
            if let tStamp = Int64(ts) {
                timeStamp = tStamp
            }
        }
        
        mqttChatManager.subscribeToGetMessageTopic(withUserID: self.selfID!)
        self.fetchMessages(withTimeStamp: "\(timeStamp)")
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5.0) {
            self.refresher.endRefreshing()
        }
    }
    
    private func setupInvitedFlag() {
        
    }
    
    private func isUserInitallyComing() -> Bool {
        if let chatid = self.chatViewModelObj?.chatID {
            if chatid.count == 24 {
                return true
            } else {
                return false
            }
        }
        return false
    }
    
    /// Calls when the notification received for getting chats.
    ///
    /// - Parameter notification: Notification with the chat details.
   @objc func stopRefresher(notification: NSNotification) {
        self.refresher.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            Helper.hideProgressIndicator()
        }
        let userInfo = notification.userInfo as! [String: Any]
//        if ((userInfo["chatId"] as? String) == (self.chatViewModelObj?.chatID)) {
//            self.setUpCollectionView(withChatDocID: self.chatDocID)
//        }
        self.setUpCollectionView(withChatDocID: self.chatDocID)
        DispatchQueue.main.async {
            self.scrollToBottom(animated: true)
        }
    }
    
    fileprivate func fetchMessages(withTimeStamp timeStamp: String) {
        self.chatViewModelObj?.receiverID = self.getReceiverID()
        if let hasChatDocId = favoriteObj?.chatDocIDFromAPI as? String {
             self.chatViewModelObj?.getMessages(withTimeStamp: timeStamp, andPageSize: Constants.messagePageSize,chatID:hasChatDocId)
        }
    }
    
    //TextView did Changed text here
    @objc func textViewTextDidChangeNotification() {
        if (self.inputToolbar.contentView?.textView?.text.count == 0 ) {
            //recordingView.isHidden = false
            self.inputToolbar.contentView?.rightBarButtonItem?.isEnabled = false
            camerabtn?.isHidden = false
        }else{
           // recordingView.isHidden = true
            camerabtn?.isHidden = true
           // recordingView.backgroundColor = UIColor.clear
            self.inputToolbar.contentView?.rightBarButtonItem?.isEnabled = true
        }
    }
    
    func setupVoiceRecordUI() {
//        recordingView.delegate = self
//        recordingView.recordingImages = [UIImage(named: "rec1")!,UIImage(named: "rec2")!,UIImage(named: "rec3")!,UIImage(named: "rec4")!,UIImage(named: "rec5")!,UIImage(named: "rec6")!]
//        self.inputToolbar.addSubview(recordingView)
//
//        let vConsts = NSLayoutConstraint(item:self.recordingView , attribute: .bottom, relatedBy: .equal, toItem: self.inputToolbar, attribute: .bottom, multiplier: 1.0, constant: -8)
//
//        let hConsts = NSLayoutConstraint(item: self.recordingView, attribute: .trailing, relatedBy: .equal, toItem: self.inputToolbar, attribute: .trailing, multiplier: 1.0, constant: -19)
//
//        self.inputToolbar.addConstraints([hConsts])
//        self.inputToolbar.addConstraints([vConsts])
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
        //self.setupVoiceRecordUI()
    }
    
    fileprivate func initialSetup() {
        if let userID = Helper.getMQTTID() {self.userID  = userID} else {self.userID = ""}
        self.receiverID = self.getReceiverID()
        self.registerNum = self.getRegisterNum()
        self.isGroup = self.getIsGroup()
        
        if self.isGroup == true{
            self.navigationItem.rightBarButtonItems = []
            self.getgpDocIdfromgroupDatabase(groupID:self.receiverID)
        }
        
        
        self.inputToolbar.contentView?.rightBarButtonItemWidth = 56
        self.inputToolbar.contentView?.leftBarButtonItemWidth = 0
        self.inputToolbar.contentView?.rightBarButtonItem?.setImage(#imageLiteral(resourceName: "Chatsend"), for: .normal)
        
        self.collectionView?.collectionViewLayout.incomingAvatarViewSize = CGSize(width:0.0, height:0.0)
        self.collectionView?.collectionViewLayout.outgoingAvatarViewSize = CGSize(width:0.0, height:0.0)
        self.collectionView?.collectionViewLayout.springinessEnabled = false
        self.automaticallyScrollsToMostRecentMessage = true
        if let favoriteObj = favoriteObj {
            let newchatDocId = favoriteObj.userId
            if newchatDocId.count>0 {
                
                
                guard let chatDocID = self.getChatDocID() else { return }
                self.chatDocID = chatDocID
                UserDefaults.standard.set(chatDocID, forKey: UserDefaultsKeys.isUserOnchatscreen)
                UserDefaults.standard.synchronize()
                self.setUpCollectionView(withChatDocID: self.chatDocID)
                self.updateChatForReadMessage(toDocID: self.chatDocID, isControllerAppearing: true)
            }
        } else {
            guard let chatDocID = self.getChatDocID() else { return }
            self.chatDocID = chatDocID
            UserDefaults.standard.set(chatDocID, forKey: UserDefaultsKeys.isUserOnchatscreen)
            UserDefaults.standard.synchronize()
            self.setUpCollectionView(withChatDocID: self.chatDocID)
            self.updateChatForReadMessage(toDocID: self.chatDocID, isControllerAppearing: true)
        }
        
        self.isuserBlock = self.getIsUserBlock()
        if self.isGroup {
            if !isActiveGroup  {
                addFooterView()
            }
        }else {
            if isuserBlock {
                addFooterView()
                self.enableCalls(isEnable: isuserBlock)
            }
        }
                
        //Show unsend text Message in TextView
        if self.chatDocID != nil {
            if let text =  UserDefaults.standard.object(forKey: self.chatDocID)  as? String {
                if text != "" {
                    //self.inputToolbar.contentView?.textView?.text = text
                    self.inputToolbar.contentView?.rightBarButtonItem?.isEnabled = true
                    self.inputToolbar.contentView?.rightBarButtonItemWidth = 56
                    self.inputToolbar.contentView?.leftBarButtonItemWidth = 0
                    self.inputToolbar.contentView?.rightBarButtonItem?.setImage(#imageLiteral(resourceName: "Chatsend"), for: .normal)
                }
            }
        }
       
        if let profilePIc = self.getProfilePic() {
            let url = URL(string : profilePIc)
            self.userImageView.image = #imageLiteral(resourceName: "02m")
            self.userImageView.kf.setImage(with: url, placeholder:#imageLiteral(resourceName: "02m"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
            self.userImageView.layoutIfNeeded()
        }
    }
    
    private func enableCalls(isEnable:Bool) {
        if isEnable {
            self.audiobutton.isEnabled = true
            self.videoButton.isEnabled = true
            self.currentChatStatusLabelOutlet.isHidden = true
        }else{
            self.audiobutton.isEnabled = false
            self.videoButton.isEnabled = false
            self.currentChatStatusLabelOutlet.isHidden = false
        }
    }
    
    
    /// Disable notifications whenever you are in the same chat controller.
    ///
    /// - Parameter user: current user id.
    private func disablePushNotificaation(forUser user : String) {
        userDefault.set(user, forKey: "currentUserId")
        userDefault.synchronize()
    }
    
    
    
    @IBAction func titlebuttonCliked(_ sender: Any) {
        if self.isGroup  == true{
            self.performSegue(withIdentifier: SegueIds.chatToGroupInfoView, sender: self)
        }else {
            self.performSegue(withIdentifier: SegueIds.chatTouserDetails, sender: self)}
    }
    
    func sendTypingStatus() {
        if let selfID = selfID {
            mqttChatManager.sendTyping(toUser: selfID)
        }
    }
    
    override func senderId() -> String {
        guard let selfID = selfID else { return ""}
        return selfID
    }
    
    func getReceiverID() -> String? {
        if  let favoriteObj = favoriteObj {
            return  favoriteObj.userId
        } else if let chatViewModelObj = chatViewModelObj {
            return chatViewModelObj.userID
        }
        return nil
    }
    
    func getReceiverName() -> String? {
        if isGroup == true {
            return chatViewModelObj?.groupName
        }
        
        if  let favoriteObj = favoriteObj {
            return  favoriteObj.firstName
        } else if let chatViewModelObj = chatViewModelObj {
            return chatViewModelObj.name
        }
        return nil
    }
    
    func getUserStatus() -> String? {
        if  let favoriteObj = favoriteObj {
            return  AppConstant.defaultStatus
        } else if chatViewModelObj != nil {
            return AppConstant.defaultStatus
        }
        return nil
    }
    
    func getRegisterNum() -> String? {
        if  let favoriteObj = favoriteObj {
            return  favoriteObj.userId
        } else if let chatViewModelObj = chatViewModelObj {
            return chatViewModelObj.userID
        }
        return nil
    }
    
    func getIsGroup() -> Bool {
        if let chatViewModelObj = chatViewModelObj {
            return chatViewModelObj.isGroupChat
        }
        return false
    }
    
    func getIsUserBlock() -> Bool {
        if let chatViewModelObj = chatViewModelObj {
            return chatViewModelObj.isUserBlock
        }
        return false
    }
    
    
    func getProfilePic() -> String? {
        
        if self.groupImage != nil{
            return self.groupImage
        }
        
        if let favoriteObj = favoriteObj{
            return  favoriteObj.profilePicture
        }else if let chatViewModelObj = chatViewModelObj{
            if chatViewModelObj.chat.image == ""{
                return " "
            }
            return  chatViewModelObj.chat.image
        }
        return nil
    }
    
    func getChatViewModalObj(fromChatDocID chatDocID : String) -> ChatViewModel? {
        guard let chatObj = chatsDocVMObject.getChatObj(fromChatDocID: chatDocID) else { return nil }
        return ChatViewModel(withChatData: chatObj)
    }
    
    
    /// This method is for getting/creating the chat without having any message.
    ///
    /// - Returns: chat document ID
    func getChatDocID() -> String? {
        if let chatViewModelObj = chatViewModelObj {
            return chatViewModelObj.docID
        } else if let favoriteObj = favoriteObj {
            let individualChatDocVMObject = IndividualChatViewModel(couchbase: couchbaseObj)
            guard let chatDocID = individualChatDocVMObject.getChatDocID(withreceiverID: favoriteObj.userId, andSecretID: "", withContactObj: favoriteObj, messageData: nil, destructionTime: nil, isCreatingChat: false) else {
                DDLogDebug("error in creating chatdoc \(self)")
                return nil
            }
            if let chatObj = chatsDocVMObject.getChatObj(fromChatDocID: chatDocID) {
                self.chatViewModelObj = ChatViewModel(withChatData: chatObj)
            }
            return chatDocID
        } else if let receiverID = self.getReceiverID(){
            let individualChatDocVMObject = IndividualChatViewModel(couchbase: couchbaseObj)
            guard let chatDocID = individualChatDocVMObject.getChatDocID(withreceiverID: receiverID, andSecretID: "", withContactObj: nil, messageData: nil, destructionTime: nil, isCreatingChat: false) else {
                DDLogDebug("error in creating chatdoc \(self)")
                return nil
            }
            if let chatObj = chatsDocVMObject.getChatObj(fromChatDocID: chatDocID) {
                self.chatViewModelObj = ChatViewModel(withChatData: chatObj)
            }
            return chatDocID
        }
        return nil
    }
    
    
    
    func getTimedifference() -> TimeInterval{
        if let appCloseTime = UserDefaults.standard.object(forKey: "appCloseTime") as? TimeInterval {
            let currentTime = Date().timeIntervalSince1970
            let diff = currentTime - appCloseTime
            return diff
        }
        return 0
    }
    
    func getScreenTimediffrence() -> TimeInterval {
        
        if let screenCloseTime = UserDefaults.standard.object(forKey: "\(String(describing: chatViewModelObj?.secretID))+chatCloseTime") as? TimeInterval {
            let currentTime = Date().timeIntervalSince1970
            let diff = currentTime - screenCloseTime
            return diff
        }
        return 0
        
    }
    
    func setUpCollectionView(withChatDocID chatDocID: String) {
        self.messages = self.chatsDocVMObject.getMessages(withChatDocID: self.chatDocID)
        
        //First time load messages check for secert Chat and dTime For Messages
        
        //check for dtime for each message
        
        if chatViewModelObj?.secretID != "" && chatViewModelObj != nil{
            var tempArr = self.messages
            for  msg  in tempArr {
                
                
                if msg.dTime != 0 && msg.isSelfMessage == true && (msg.gpMessageType == "") && msg.messageStatus == "3"{
                    //if message is seen then delete it from db and ChatUI For send Messagess
                    
                    if getTimedifference() > TimeInterval(msg.dTime) {
                        print("dTime is less so delete me")
                        if self.messages.contains(msg) {
                            let index = self.messages.index(of: msg)
                            self.messages.remove(at: index!)
                            self.chatsDocVMObject.deletePerticularMessageInDataBase(messageID: msg.messageId, docID: self.chatDocID)
                        }
                    }else  if getTimedifference() < TimeInterval(msg.dTime){
                        
                        print("hey change my dTime ..because my time is not over")
                        let newdTime  = msg.dTime - Int(getTimedifference())
                        self.chatsDocVMObject.secretChatTimer(docID:self.chatDocID , messageID: msg.messageId, dTime:newdTime)
                    }
                    
                }else if msg.isSelfMessage == false && msg.dTime != 0 && (msg.gpMessageType == "") {
                    //delete recived Message
                    if getScreenTimediffrence() < TimeInterval(msg.dTime){
                        
                        print("hey change my dTime ..because my time is not over")
                        let newdTime  = msg.dTime - Int(getTimedifference())
                        self.chatsDocVMObject.secretChatTimer(docID:self.chatDocID , messageID: msg.messageId, dTime:newdTime)
                    }
                }
                
            }
            
        }
        
        
        if self.messages.count == 0 && callAPIForGetChats {
            callAPIForGetChats = false
            self.loadData()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5.0) {
                Helper.hideProgressIndicator()
            }
        }
        self.collectionView?.reloadData()
    }
    
    override func senderDisplayName() -> String {
        return (UIDevice.current.identifierForVendor?.uuidString)!
    }
    
    func showdefaultAlert(title : String ,message : String){
        let alert =  UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction.init(title: "OK", style: .default, handler: nil)
        alert.addAction(action1)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Navigtion items Button action
    @IBAction func audioBtnAction(_ sender: Any) {
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        switch cameraAuthorizationStatus {
        case .denied:
            self.showdefaultAlert(title: "Oops", message: AppConstant.cameraPermissionMsg)
            return
        case .authorized:   let audioMediaType = AVMediaType.audio
        let audioAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: audioMediaType)
        switch audioAuthorizationStatus {
        case .authorized: self.startAudioCall()
        case .denied : self.showdefaultAlert(title: "Oops", message: AppConstant.audioPermissionMsg)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for:AVMediaType.audio, completionHandler: { isGranted in
                if isGranted == true {
                    OperationQueue.main.addOperation({
                        self.startAudioCall()
                    })
                }else{
                    self.showdefaultAlert(title: "Oops", message: AppConstant.audioPermissionMsg)
                }})
        case .restricted: break
            }
        case .restricted: break
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                if granted {
                    AVCaptureDevice.requestAccess(for:AVMediaType.audio, completionHandler: { isGranted in
                        if isGranted == true {
                            OperationQueue.main.addOperation({
                                self.startAudioCall()
                            })
                        } else {
                            OperationQueue.main.addOperation({
                                self.showdefaultAlert(title: "Oops", message: AppConstant.audioPermissionMsg)
                            })
                        }
                    })
                } else {
                    OperationQueue.main.addOperation({
                        self.showdefaultAlert(title: "Oops", message: AppConstant.cameraPermissionMsg)
                    })
                    return
                }
            }
        }
    }
    
    @IBAction func videoBtnAction(_ sender: Any) {
        let cameraMediaType = AVMediaType.audio
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        switch cameraAuthorizationStatus {
        case .denied:
            self.showdefaultAlert(title: "Oops", message: AppConstant.audioPermissionMsg)
            return
        case .authorized:   let audioMediaType = AVMediaType.audio
        let audioAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: audioMediaType)
        switch audioAuthorizationStatus {
        case .authorized:  self.startVideoCall()
        case .denied : self.showdefaultAlert(title: "Oops", message: AppConstant.audioPermissionMsg)
        case .notDetermined: AVCaptureDevice.requestAccess(for:AVMediaType.audio, completionHandler: { isGranted in
            if isGranted == true {
                OperationQueue.main.addOperation({
                    self.startVideoCall()
                })
            } else {
                self.showdefaultAlert(title: "Oops", message: AppConstant.audioPermissionMsg)
            }
        })
        case .restricted: break
            }
        case .restricted: break
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                if granted {
                    AVCaptureDevice.requestAccess(for:AVMediaType.audio, completionHandler: { isGranted in
                        if isGranted == true {
                            OperationQueue.main.addOperation({
                                self.startVideoCall()
                            })
                        }else{
                            OperationQueue.main.addOperation({
                                self.showdefaultAlert(title: "Oops", message: AppConstant.audioPermissionMsg)
                            })
                            return
                        }
                    })
                } else {
                    OperationQueue.main.addOperation({
                        self.showdefaultAlert(title: "Oops", message: AppConstant.cameraPermissionMsg)
                    })
                    return
                }
            }
        }
    }
    
    func startAudioCall(){
        if Helper.checkCallGoingOn() == true{
            return
        }
        guard let ownID = Helper.getMQTTID() else { return }
        MQTTCallManager.sendcallAvilibilityStatus(status: 0, topic: MQTTTopic.callsAvailability + ownID)
        UserDefaults.standard.set(true, forKey: "iscallBtnCliked")
        guard let userID = self.receiverID else{ return  }
        guard let registerNum = self.registerNum else {return}
        
        let dict = ["callerId": userID,
                    "callType" : CallTypes.audioCall,
                    "registerNum": registerNum,
                    "callId": randomString(length: 100),
                    "callerIdentifier": ""] as [String:Any]
        
        UserDefaults.standard.set(dict, forKey: "storeIndexPath")
        DDLogDebug("subscribe channel = \(MQTTTopic.callsAvailability + userID)")
        MQTT.sharedInstance.subscribeTopic(withTopicName: MQTTTopic.callsAvailability + userID , withDelivering: .atLeastOnce)
        let window = UIApplication.shared.keyWindow!
        self.inputToolbar.contentView?.textView?.resignFirstResponder() //[textView resignFirstResponder];
        let audioView = AudioCallView(frame: CGRect(x:0, y:0, width: window.frame.width, height: window.frame.height))
        audioView.tag = 15
        audioView.userNameLbl.text = String(format:"%@",self.userName)
        audioView.chatViewObj = self
        audioView.callerID = userID
        audioView.playSound("calling", loop: 40)
        audioView.setMessageData(messageData: dict)
        if let  profileIm = self.getProfilePic() {
            if profileIm.count == 0 { audioView.backImageView.backgroundColor = .black } else {
                audioView.backImageView.backgroundColor = .clear
            }
        }
        
        
        
        if let profilepic = self.getProfilePic() as? String {
            audioView.backImageView.kf.setImage(with: URL(string: profilepic), placeholder: #imageLiteral(resourceName: "02m"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
            })
            
            audioView.userImageView.kf.setImage(with: URL(string:profilepic), placeholder: #imageLiteral(resourceName: "02m"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
            })
            
        } else {
            audioView.userImageView.image = #imageLiteral(resourceName: "02m")
            audioView.backImageView.image = #imageLiteral(resourceName: "02m")
        }
        
        
        
       
        self.inputToolbar.isHidden  = true
        window.addSubview(audioView);
        window.bringSubview(toFront: audioView)
    }
    
    func startVideoCall(){
        if Helper.checkCallGoingOn() == true{
            return
        }
        guard let ownID = Helper.getMQTTID() else { return }
        MQTTCallManager.sendcallAvilibilityStatus(status: 0, topic: MQTTTopic.callsAvailability + ownID)
        UserDefaults.standard.set(true, forKey: "iscallBtnCliked")
        guard let userID = self.receiverID else{ return  }
        guard let registerNum = self.registerNum else {return}
        
        let dict = ["callerId": userID,
                    "callType" : CallTypes.videoCall ,
                    "registerNum": registerNum,
                    "callId": randomString(length: 100),
                    "callerIdentifier": ""] as [String:Any]
        
        UserDefaults.standard.set(dict, forKey: "storeIndexPath")
        MQTT.sharedInstance.subscribeTopic(withTopicName: MQTTTopic.callsAvailability + userID , withDelivering: .atLeastOnce)
        
        //for VideoCall
        let window = UIApplication.shared.keyWindow!
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
        let videoView = IncomingVideocallView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.tag =  17
        videoView.setCallId()
        videoView.otherCallerId = userID
        videoView.chatViewObj = self
        videoView.playSound("calling", loop: 40)
        videoView.calling_userName.text = String(format:"%@",self.userName)
        
        
        if let hasProfileImage = self.getProfilePic() {
            videoView.userImageView.kf.setImage(with: URL(string: (self.getProfilePic())!), placeholder: #imageLiteral(resourceName: "02m"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
            })
        } else {
            videoView.userImageView.image = #imageLiteral(resourceName: "02m")
        }
        
        videoView.addCameraView()
        self.inputToolbar.isHidden  = true
        window.addSubview(videoView)
    }
    
    //MARK: Preperfor segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIds.chatTouserDetails {
            let contactINfo = segue.destination as? ContactInfoTableViewController
            if let registerNum = self.registerNum {contactINfo?.userRegisterNum = registerNum}else {contactINfo?.userRegisterNum = "" }
            if let userID = self.receiverID { contactINfo?.userID = userID }else { contactINfo?.userID = "" }
            if let userName = self.userName {contactINfo?.userName = userName } else {contactINfo?.userName = "" }
            if let userImage = self.getProfilePic() {contactINfo?.userImage = userImage } else {contactINfo?.userImage = "" }
            if let userStatus = self.getUserStatus() {contactINfo?.userStatus = userStatus } else {contactINfo?.userStatus = AppConstant.defaultStatus }
            contactINfo?.chatVMObj = chatViewModelObj
            contactINfo?.chatsDocVMObject = self.chatsDocVMObject
            contactINfo?.isBlockUser = isuserBlock
            contactINfo?.clearChatBlock = { isClear in
                if self.chatDocID == nil {return}
                self.messages = self.chatsDocVMObject.getMessages(withChatDocID: self.chatDocID)
                self.collectionView?.reloadData()
            }
            contactINfo?.blockUser = {isBlock in
                if isBlock == true {
                    self.addFooterView()
                    self.isuserBlock  = isBlock
                    self.enableCalls(isEnable:  self.isuserBlock)
                }
                else {
                    self.removeFooterView()
                    self.enableCalls(isEnable:  self.isuserBlock)
                    self.isuserBlock  = isBlock
                }
                
            }
            
        }
        else if segue.identifier == SegueIds.chatTogifsegue {
            let gifstickerView  = segue.destination as? GifStickerCollectionViewController
            gifstickerView?.isGiphy = sender as? Bool
            gifstickerView?.delegate = self
        }
        else if segue.identifier == SegueIds.chatVCToshowLocation {
            if let controller = segue.destination as? LocationViewerViewController, let latLongStr = sender as? String {
                controller.currentLatLong = latLongStr
            }
        } else if segue.identifier == SegueIds.chatVCTotoContactDetails {
            
            if let controller = segue.destination as? ContactDetailsViewController, let contactVMObj = sender as? ContactMessageViewModal {
                controller.contactVMObj = contactVMObj
            }
        } else if segue.identifier == SegueIds.chatVCTodocumentViewer {
            if let controller = segue.destination as? DocumentViewerViewController, let documentVMObj = sender as? DocumentMessageViewModal {
                controller.docMVMObj = documentVMObj
            }
        }
        else if segue.identifier == SegueIds.chatVCTogroupInfo {

        }
    }
    
    //Hide inputToolbar if callView coming
    func hideToolBarr() {
        if let videoView = appDelegetConstant.window.viewWithTag(17) as? IncomingVideocallView{
            videoView.chatViewObj = self
            //self.inputToolbar.isHidden = true
            self.inputToolbar.contentView?.textView?.resignFirstResponder()
        }
    }
    
    
    //ADD CameraButton
    func addCameraBtn() {
        if camerabtn == nil {
            if self.view.frame.size.width == 320 {
                camerabtn = UIButton.init(frame:CGRect.init(x: (self.inputToolbar.contentView?.textView?.frame.size.width)! + (self.inputToolbar.contentView?.textView?.frame.origin.x)! - 70 , y: 4, width: 35, height: 35))
            }
            else if (self.view.frame.size.width == 414){
                camerabtn = UIButton.init(frame:CGRect.init(x: (self.inputToolbar.contentView?.textView?.frame.size.width)! + (self.inputToolbar.contentView?.textView?.frame.origin.x)! + 20, y: 4, width: 35, height: 35))
            }
            else {
                camerabtn = UIButton.init(frame:CGRect.init(x: (self.inputToolbar.contentView?.textView?.frame.size.width)! + (self.inputToolbar.contentView?.textView?.frame.origin.x)! - 10 , y: 4, width: 35, height: 35))
            }
            camerabtn?.setImage(#imageLiteral(resourceName: "cameraOff"), for: .normal)
            camerabtn?.setImage(#imageLiteral(resourceName: "cameraOn"), for: .highlighted)
            if chatViewModelObj?.secretID != "" && chatViewModelObj != nil{
                camerabtn?.setImage(#imageLiteral(resourceName: "timer_Icon"), for: .normal)
                camerabtn?.setImage(#imageLiteral(resourceName: "timer_Icon"), for: .highlighted)
            }
            
            camerabtn?.addTarget(self, action: #selector(inputToolbarCameraCliked), for: .touchUpInside)
            self.inputToolbar.contentView?.textView?.addSubview(camerabtn!)
        }
    }
    
    
    
    //MARK: - ADD FooterView
    //ADD FooterView
    func addFooterView(){
        if footerView == nil {
            footerView = UIView.init(frame: CGRect.init(x: 0, y: UIScreen.main.bounds.size.height - 60, width: UIScreen.main.bounds.size.width, height: 60))
            footerView?.backgroundColor = UIColor.white
            let lbl = UILabel.init(frame: CGRect.init(x: 10, y: 0, width:UIScreen.main.bounds.size.width - 10 , height: 50))
            lbl.text = self.isGroup ? StringConstants.noLongerParticipant() : StringConstants.youCantSendMsg()
            lbl.adjustsFontSizeToFitWidth = true;
            lbl.minimumScaleFactor=0.5;
            lbl.textAlignment = .center
            footerView?.addSubview(lbl)
            self.inputToolbar.isHidden = true
            self.view.addSubview(footerView!)
        }
    }
    
    
    @objc func blockUpdate(){
        showBlockedView(message: StringConstants.youAreBlocked())
    }
    @objc func unBlockUpdate(){
        dateButtonOutlet.isEnabled = true
        attachBtn.isEnabled = true
        //  moreBtn.isEnabled = true
        self.inputToolbar.isHidden = false
        removeFooterView()
    }
    
    
    func checkForBlock(){
        if favoriteObj != nil {
            if (favoriteObj?.isBlockedByOther == 1) || (favoriteObj?.isUserBlocked == 1) {
                
               if favoriteObj?.isBlockedByOther == 1 {
                    showBlockedView(message: StringConstants.youAreBlocked())
                }
                if favoriteObj?.isUserBlocked == 1 {
                    showBlockedView(message: StringConstants.msgBlockedByYou())
                }
                self.inputToolbar.isHidden = true
                dateButtonOutlet.isEnabled = false
                attachBtn.isEnabled = false
               // moreBtn.isEnabled = false
            }else{
                dateButtonOutlet.isEnabled = true
                attachBtn.isEnabled = true
              //  moreBtn.isEnabled = true
                self.inputToolbar.isHidden = false
                removeFooterView()
            }
        }
    }
    
    
    //Remove FooterView
    func removeFooterView(){
        self.inputToolbar.isHidden = false
        footerView?.removeFromSuperview()
        footerView = nil
    }
    
    func showBlockedView(message: String){
        if footerView == nil {
            footerView = UIView.init(frame: CGRect.init(x: 0, y: UIScreen.main.bounds.size.height - 70, width: UIScreen.main.bounds.size.width, height: 70))
            footerView?.backgroundColor = Colors.chatFooterView
            let lbl = UILabel.init(frame: CGRect.init(x: 10, y: 0, width:UIScreen.main.bounds.size.width - 10 , height: 70))
            lbl.text = message
            lbl.numberOfLines = 2
            lbl.adjustsFontSizeToFitWidth = true;
            lbl.minimumScaleFactor=0.5;
            lbl.textAlignment = .center
            footerView?.addSubview(lbl)
            self.inputToolbar.isHidden = true
            self.view.addSubview(footerView!)
        }
    }
    
}


// MARK: - Collection View Delegate
extension ChatViewController
{
    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return super.collectionView(collectionView, canPerformAction: action, forItemAt: indexPath, withSender: sender)
    }
    
    //MARK: Number of items in view
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.messages.count > 0) {
         collectionView.backgroundView = nil
        }
        return self.messages.count
    }
    
    //MARK: Data on cell
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageDataForItemAt indexPath: IndexPath) -> JSQMessageData {
        
        return self.messages[indexPath.row]
        
        
    }
    
    //MARK: After deletion of message at index path
    override func collectionView(_ collectionView: JSQMessagesCollectionView, didDeleteMessageAt indexPath: IndexPath) {
        // self.toDeleteMessageWith(indexPath:indexPath)
    }
    
    //MARK: To configure outgong or incomming bubble
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageBubbleImageDataForItemAt indexPath: IndexPath) -> JSQMessageBubbleImageDataSource? {
        let message:Message = messages[indexPath.item]
        return (self.senderId() == message.senderId) ? outgoingBubble : incomingBubble
    }
    
    //MARK: For Avatar Images
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, avatarImageDataForItemAt indexPath: IndexPath) -> JSQMessageAvatarImageDataSource? {
        return nil
    }
    
    //MARK: To show name above message
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        
        // Displaying names above messages
        //Mark: Removing Sender Display Name
        /**
         *  Example on showing or removing senderDisplayName based on user settings.
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         */
        
        if isGroup == false {return nil}
        let msg = self.messages[indexPath.item]
        if msg.isSelfMessage {
            return nil
        }
        if indexPath.item - 1 > 0 {
            let prevMsg = self.messages[indexPath.item - 1]
            if prevMsg.senderId == msg.senderId {
                return nil
            }
        }
        return nil
    }
    
    //MARK: Setting up collection view cell
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let jsqcell = (super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell)
        jsqcell.backgroundColor = Color.clear
        jsqcell.accessoryButton?.isHidden = true
        //jsqcell.accessoryButton?.setImage(#imageLiteral(resourceName: "forward_icon"), for: .normal)
        jsqcell.snMessageDelegate = self
        let msg = self.messages[indexPath.item]
        if let messageMediaType:MessageTypes = msg.messageType {
            switch messageMediaType {
            case .image:
                if msg.isSelfMessage {
                    var cell : ImageSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "ImageSentCollectionViewCell", for: indexPath) as! ImageSentCollectionViewCell
                    
                    //Creating View Modal from Image Message
                    let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                    if self.isGroup == true{
                        imageMsgVMobj.isGroup = self.isGroup
                        imageMsgVMobj.groupMembers = self.groupMembers
                        imageMsgVMobj.gpImage  = getProfilePic()
                    }
                    imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                    cell.isReplying = self.isReplying
                    cell.replyMsg = self.replyingMessage
                    cell.imageMsgType = messageMediaType
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell.chatDocID = self.chatDocID
                    cell.msgVMObj = imageMsgVMobj
                    cell.panGestureRecognizer?.delegate = self
                    //Uploading image by using Image View Modal. And checking that image is not uploaded already.
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! ImageSentCollectionViewCell
                    cell.imageTappedDelegate = self
                    
                    cell.backgrundViewOutlet?.layer.borderColor = UIColor.lightGray.cgColor
                    cell.backgrundViewOutlet?.layer.borderWidth = 1.0
                    cell.backgrundViewOutlet?.layer.cornerRadius = 5.0
                    
                    return cell
                } else {
                    var cell : ImageReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "ImageReceivedCollectionViewCell", for: indexPath) as! ImageReceivedCollectionViewCell
                    let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                    imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                    cell.chatDocID = self.chatDocID
                    cell.msgVMObj = imageMsgVMobj
                    cell.imageMsgType = messageMediaType
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! ImageReceivedCollectionViewCell
                    cell.imageTappedDelegate = self
                    cell.panGestureRecognizer?.delegate = self
                    
                    cell.backGroundViewOutlet?.layer.borderColor = Colors.AppBaseColor.cgColor
                    
                    cell.backGroundViewOutlet?.layer.borderWidth = 1.0
                    cell.backGroundViewOutlet?.layer.cornerRadius = 5.0
                    
                    return cell
                }
                
            case .location:
                if msg.isSelfMessage {
                    var cell : SentLocationCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "SentLocationCell", for: indexPath) as! SentLocationCell
                    cell.backgroundColor = UIColor.clear
                    cell.msgObj = msg
                    cell.locationDelegate = self
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! SentLocationCell
                    cell.panGestureRecognizer?.delegate = self
                    cell.backGroundViewOutlet?.layer.borderColor = UIColor.lightGray.cgColor
                    cell.backGroundViewOutlet?.layer.borderWidth = 1.0
                    cell.backGroundViewOutlet?.layer.cornerRadius = 5.0

                    return cell
                } else {
                    //2 case for !isSelf and Offer = 2, is Accepted
                    var cell : ReceivedLocationCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "ReceivedLocationCell", for: indexPath) as! ReceivedLocationCell
                    cell.backgroundColor = UIColor.clear
                    cell.msgObj = msg
                    cell.locationDelegate = self
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! ReceivedLocationCell
                    cell.panGestureRecognizer?.delegate = self
                    cell.backGroundViewOutlet?.layer.borderColor = Colors.AppBaseColor.cgColor
                    cell.backGroundViewOutlet?.layer.borderWidth = 1.0
                    cell.backGroundViewOutlet?.layer.cornerRadius = 5.0

                    return cell
                }
                
            case .contact:
                if msg.isSelfMessage {
                    var cell : SentContactMessageCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "SentContactMessageCollectionViewCell", for: indexPath) as! SentContactMessageCollectionViewCell
                    let contactMVMObj = ContactMessageViewModal(withMessage: msg)
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell.msgObj = contactMVMObj
                    cell.contactMessageDelegates = self
                    cell.msgObj.presentControllerDelegate = self
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! SentContactMessageCollectionViewCell
                    cell.panGestureRecognizer?.delegate = self
                    return cell
                } else {
                    var cell : ReceivedContactCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "ReceivedContactCollectionViewCell", for: indexPath) as! ReceivedContactCollectionViewCell
                    let contactMVMObj = ContactMessageViewModal(withMessage: msg)
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell.msgObj = contactMVMObj
                    cell.contactMessageDelegates = self
                    cell.msgObj.presentControllerDelegate = self
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! ReceivedContactCollectionViewCell
                    cell.panGestureRecognizer?.delegate = self
                    return cell
                }
                
            case .doodle:
                if msg.isSelfMessage {
                    var cell : ImageSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "ImageSentCollectionViewCell", for: indexPath) as! ImageSentCollectionViewCell
                    
                    //Creating View Modal from Image Message
                    let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                    
                    if self.isGroup == true{
                        imageMsgVMobj.isGroup = self.isGroup
                        imageMsgVMobj.groupMembers = self.groupMembers
                        imageMsgVMobj.gpImage  = getProfilePic()
                    }
                    
                    imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                    cell.isReplying = self.isReplying
                    cell.replyMsg = self.replyingMessage
                    cell.chatDocID = self.chatDocID
                    cell.imageMsgType = messageMediaType
                    cell.msgVMObj = imageMsgVMobj
                    cell.imageTappedDelegate = self
                    //Uploading image by using Image View Modal. And checking that image is not uploaded already.
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! ImageSentCollectionViewCell
                    cell.panGestureRecognizer?.delegate = self
                    return cell
                } else {
                    var cell : ImageReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "ImageReceivedCollectionViewCell", for: indexPath) as! ImageReceivedCollectionViewCell
                    let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                    imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                    cell.chatDocID = self.chatDocID
                    cell.msgVMObj = imageMsgVMobj
                    cell.imageMsgType = messageMediaType
                    cell.imageTappedDelegate = self
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! ImageReceivedCollectionViewCell
                    cell.panGestureRecognizer?.delegate = self
                    return cell
                }
                
            case .video:
                if msg.isSelfMessage {
                    var cell : VideoSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "VideoSentCollectionViewCell", for: indexPath) as! VideoSentCollectionViewCell
                    
                    //Creating View Modal from Video Message
                    let videoMsgVMobj = VideoMessageViewModal(withMessage: msg)
                    if self.isGroup == true{
                        videoMsgVMobj.isGroup = self.isGroup
                        videoMsgVMobj.groupMembers = self.groupMembers
                        videoMsgVMobj.gpImage  = getProfilePic()
                    }
                    cell.isReplying = self.isReplying
                    cell.replyMsg = self.replyingMessage
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell.chatDocID = self.chatDocID
                    cell.msgVMObj = videoMsgVMobj
                    //Uploading video by using Video View Modal. And checking that video is not uploaded already.
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! VideoSentCollectionViewCell
                    cell.videoTappedDelegate = self
                    cell.panGestureRecognizer?.delegate = self
                    
                    cell.backGroundViewOutlet?.layer.borderColor = UIColor.lightGray.cgColor
                    cell.backGroundViewOutlet?.layer.borderWidth = 1.0
                    cell.backGroundViewOutlet?.layer.cornerRadius = 5.0
                    
                    return cell
                } else {
                    var cell : VideoReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "VideoReceivedCollectionViewCell", for: indexPath) as! VideoReceivedCollectionViewCell
                    let videoMsgVMobj = VideoMessageViewModal(withMessage: msg)
                    cell.chatDocID = self.chatDocID
                    cell.msgVMObj = videoMsgVMobj
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! VideoReceivedCollectionViewCell
                    cell.videoTappedDelegate = self
                    cell.panGestureRecognizer?.delegate = self
                    cell.backGroundViewOutlet?.layer.borderColor = Colors.AppBaseColor.cgColor
                    cell.backGroundViewOutlet?.layer.borderWidth = 1.0
                    cell.backGroundViewOutlet?.layer.cornerRadius = 5.0
                    return cell
                }
                
            case .sticker:
                if msg.isSelfMessage {
                    var cell : StickerSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "StickerSentCollectionViewCell", for: indexPath) as! StickerSentCollectionViewCell
                    
                    //Creating View Modal from Image Message
                    let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                    imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                    cell.isReplying = self.isReplying
                    cell.replyMsg = self.replyingMessage
                    cell.chatDocID = self.chatDocID
                    cell.msgVMObj = imageMsgVMobj
                    
                    //Uploading image by using Image View Modal. And checking that image is not uploaded already.
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! StickerSentCollectionViewCell
                    cell.panGestureRecognizer?.delegate = self
                    return cell
                } else {
                    var cell : StickerReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "StickerReceivedCollectionViewCell", for: indexPath) as! StickerReceivedCollectionViewCell
                    let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                    imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                    cell.chatDocID = self.chatDocID
                    cell.msgVMObj = imageMsgVMobj
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! StickerReceivedCollectionViewCell
                    cell.panGestureRecognizer?.delegate = self
                    return cell
                }
                
            case .gif:
                if msg.isSelfMessage {
                    var cell : ImageSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "ImageSentCollectionViewCell", for: indexPath) as! ImageSentCollectionViewCell
                    
                    //Creating View Modal from Image Message
                    let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                    if self.isGroup == true{
                        imageMsgVMobj.isGroup = self.isGroup
                        imageMsgVMobj.groupMembers = self.groupMembers
                        imageMsgVMobj.gpImage  = getProfilePic()
                    }
                    imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                    cell.isReplying = self.isReplying
                    cell.replyMsg = self.replyingMessage
                    cell.chatDocID = self.chatDocID
                    cell.msgVMObj = imageMsgVMobj
                    cell.imageMsgType = messageMediaType
                    cell.imageTappedDelegate = self
                    //Uploading image by using Image View Modal. And checking that image is not uploaded already.
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! ImageSentCollectionViewCell
                    cell.panGestureRecognizer?.delegate = self
                    
                    cell.backgrundViewOutlet?.layer.borderColor = UIColor.lightGray.cgColor
                    cell.backgrundViewOutlet?.layer.borderWidth = 1.0
                    cell.backgrundViewOutlet?.layer.cornerRadius = 5.0
                    
                    return cell
                } else {
                    var cell : ImageReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "ImageReceivedCollectionViewCell", for: indexPath) as! ImageReceivedCollectionViewCell
                    let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                    imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                    cell.chatDocID = self.chatDocID
                    cell.msgVMObj = imageMsgVMobj
                    cell.imageTappedDelegate = self
                    cell.imageMsgType = messageMediaType
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! ImageReceivedCollectionViewCell
                    cell.panGestureRecognizer?.delegate = self
                    
                    cell.backGroundViewOutlet?.layer.borderColor = Colors.AppBaseColor.cgColor
                    cell.backGroundViewOutlet?.layer.borderWidth = 1.0
                    cell.backGroundViewOutlet?.layer.cornerRadius = 5.0
                    return cell
                }
                
            case .document:
                if msg.isSelfMessage {
                    var cell : DocumentSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "DocumentSentCollectionViewCell", for: indexPath) as! DocumentSentCollectionViewCell
                    let docMVMObj = DocumentMessageViewModal(withMessage: msg)
                    if self.isGroup == true{
                        docMVMObj.isGroup = self.isGroup
                        docMVMObj.groupMembers = self.groupMembers
                        docMVMObj.gpImage  = getProfilePic()
                    }
                    docMVMObj.isReplying = self.isReplying
                    docMVMObj.replyMsg = self.replyingMessage
                    cell.documentMVMObj = docMVMObj
                    docMVMObj.chatDocID = self.chatDocID
                    cell.documentMessageDelegate = self
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! DocumentSentCollectionViewCell
                    cell.panGestureRecognizer?.delegate = self
                    cell.backGroundViewOutlet?.layer.borderColor = UIColor.lightGray.cgColor
                    cell.backGroundViewOutlet?.layer.borderWidth = 1.0
                    cell.backGroundViewOutlet?.layer.cornerRadius = 5.0
                    return cell
                } else {
                    //2 case for !isSelf and Offer = 2, is Accepted
                    var cell : DocumentReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "DocumentReceivedCollectionViewCell", for: indexPath) as! DocumentReceivedCollectionViewCell
                    let docMVMObj = DocumentMessageViewModal(withMessage: msg)
                    docMVMObj.isReplying = self.isReplying
                    docMVMObj.replyMsg = self.replyingMessage
                    cell.documentMVMObj = docMVMObj
                    docMVMObj.chatDocID = self.chatDocID
                    cell.documentMessageDelegate = self
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! DocumentReceivedCollectionViewCell
                    cell.panGestureRecognizer?.delegate = self
                    cell.backGroundViewOutlet?.layer.borderColor = Colors.AppBaseColor.cgColor
                    cell.backGroundViewOutlet?.layer.borderWidth = 1.0
                    cell.backGroundViewOutlet?.layer.cornerRadius = 5.0
                    return cell
                }
                
            case .audio:
                //Creating View Modal from Image Message
                if msg.isSelfMessage {
                    var cell : AudioSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "AudioSentCollectionViewCell", for: indexPath) as! AudioSentCollectionViewCell
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    
                    cell.msgObj = msg
                    cell.audioPlayerDelegate = self
                    let audioMVMObj = AudioMessageViewModal(withMessage: msg)
                    if self.isGroup == true{
                        audioMVMObj.isGroup = self.isGroup
                        audioMVMObj.groupMembers = self.groupMembers
                        audioMVMObj.gpImage  = getProfilePic()
                    }
                    audioMVMObj.isReplying = self.isReplying
                    audioMVMObj.replyMsg = self.replyingMessage
                    audioMVMObj.chatDocID = self.chatDocID
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! AudioSentCollectionViewCell
                    cell.panGestureRecognizer?.delegate = self
                    return cell
                } else {
                    var cell : AudioReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "AudioReceivedCollectionViewCell", for: indexPath) as! AudioReceivedCollectionViewCell
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell.audioPlayerDelegate = self
                    cell.msgObj = msg
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! AudioReceivedCollectionViewCell
                    cell.panGestureRecognizer?.delegate = self
                    return cell
                }
                
            case .text:
                
                if  msg.gpMessageType != "" {
                    let cell: GpMessageTypeCell = super.collectionView?.dequeueReusableCell(withReuseIdentifier: "GpMessageTypeCell", for: indexPath) as! GpMessageTypeCell
                    
                    //Tag For secret Chat *******
                    if chatViewModelObj?.secretID != "" && chatViewModelObj != nil{
                        
//                        if let messge = msg.messagePayload {
//                            let dtime = Helper.timeInsecArr().index(of: msg.dTime) ?? 0
//                            cell.gpMessageTypeText.text = "\(Helper.getNameFormDatabase(num: messge)) set the destruct timer to  \(Helper.defaultTimerArr()[dtime])"
//
//                            cell.backgroundColor = UIColor.clear
//                            return cell
//                        }
                    }
                    ///*******
                    
                    
                    
                    //Tag For Group Chat ****
                    
                    switch msg.gpMessageType! {
                    case "0" :
                        //Creat Group
                        if let message = msg.messagePayload{
//                            let arr =  message.components(separatedBy: ",")
//                            let num = Helper.getNameFormDatabase(num: arr[0])
//                            cell.gpMessageTypeText.text =  "\(num) created group \(arr[1])"
                        }
                    case "1":
                        
                        if let message = msg.messagePayload{
//                            let arr =  message.components(separatedBy: ",")
//                            let num1 = Helper.getNameFormDatabase(num: arr[0])
//                            let num2 = Helper.getNameFormDatabase(num: arr[1])
//                            cell.gpMessageTypeText.text =  "\(num1) added \(num2)"
                        }
                    case "2":
                        if let message = msg.messagePayload{
//                            let arr =  message.components(separatedBy: ",")
//                            let num1 = Helper.getNameFormDatabase(num: arr[0])
//                            let num2 = Helper.getNameFormDatabase(num: arr[1])
//                            cell.gpMessageTypeText.text =  "\(num1) removed \(num2)"
                        }
                        
                    case "4" :
                        
                        if let message = msg.messagePayload{
//                            let arr =  message.components(separatedBy: ",")
//                            let num = Helper.getNameFormDatabase(num: arr[0])
//                            cell.gpMessageTypeText.text =  "\(num) changed the subject to \(arr[1])"
                        }
                    case "5":
                        
                        if let message = msg.messagePayload{
//                            let num = Helper.getNameFormDatabase(num: message)
//                            cell.gpMessageTypeText.text =  "\(num) changed this group's icon"
                        }
                        
                    case "6":
                        
                        if let message = msg.messagePayload{
//                            let num = Helper.getNameFormDatabase(num: message)
//                            cell.gpMessageTypeText.text =  "\(num) left"
                        }
                        
                    default:
                        print("dd")
                    }
                    
                    ///*******
                    cell.backgroundColor = UIColor.clear
                    return cell
                }
                else {
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    jsqcell.timeLabelOutlet?.attributedText = messageVMObj.setDateTime()
                    jsqcell.currentStatusOutlet?.attributedText = messageVMObj.setReadStatus()
                    jsqcell.panGestureRecognizer?.delegate = self
                    jsqcell.textView?.textColor = UIColor.black
                    
                    if(messageVMObj.message.isSelfMessage) {
                        jsqcell.textView?.layer.borderColor = UIColor.lightGray.cgColor
                    } else {
                        jsqcell.textView?.layer.borderColor = Colors.AppBaseColor.cgColor
                    }
                    jsqcell.textView?.backgroundColor = collectionView.backgroundColor
                    jsqcell.textView?.layer.borderWidth = 1.0
                    jsqcell.textView?.layer.cornerRadius = 5.0
                    return jsqcell
                }
                
            case .replied:
                if let repliedMsg = msg.repliedMessage {
                    if let messageMediaType =  repliedMsg.replyMessageType {
                        switch messageMediaType {
                        case .text:
                            if msg.isSelfMessage {
                                var cell : SentTextCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "SentTextCollectionViewCell", for: indexPath) as! SentTextCollectionViewCell
                                cell.msgObj = msg
                                cell.repliedButtonPressedDelegate = self
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! SentTextCollectionViewCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            } else {
                                var cell : ReceivedTextCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "ReceivedTextCollectionViewCell", for: indexPath) as! ReceivedTextCollectionViewCell
                                cell.msgObj = msg
                                cell.repliedButtonPressedDelegate = self
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! ReceivedTextCollectionViewCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            }
                            
                        case .image:
                            if msg.isSelfMessage {
                                var cell : RepliedImageSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedImageSentCollectionViewCell", for: indexPath) as! RepliedImageSentCollectionViewCell
                                
                                //Creating View Modal from Image Message
                                let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                                imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                                cell.isReplying = self.isReplying
                                cell.replyMsg = self.replyingMessage
                                
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell.chatDocID = self.chatDocID
                                cell.msgVMObj = imageMsgVMobj
                                cell.imageMsgType = messageMediaType
                                cell.repliedButtonPressedDelegate = self
                                //Uploading image by using Image View Modal. And checking that image is not uploaded already.
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedImageSentCollectionViewCell
                                cell.imageTappedDelegate = self
                                cell.panGestureRecognizer?.delegate = self
                                
                                 cell.backgrundViewOutlet?.layer.borderColor = UIColor.lightGray.cgColor
                                
                                cell.backgrundViewOutlet?.layer.borderWidth = 1.0
                                cell.backgrundViewOutlet?.layer.cornerRadius = 5.0
                                
                                return cell
                            } else {
                                var cell : RepliedImageReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedImageReceivedCollectionViewCell", for: indexPath) as! RepliedImageReceivedCollectionViewCell
                                let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                                imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                                cell.chatDocID = self.chatDocID
                                cell.msgVMObj = imageMsgVMobj
                                cell.imageMsgType = messageMediaType
                                cell.repliedButtonPressedDelegate = self
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedImageReceivedCollectionViewCell
                                cell.imageTappedDelegate = self
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            }
                            
                        case .location:
                            if msg.isSelfMessage {
                                var cell : RepliedSentLocationCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedSentLocationCell", for: indexPath) as! RepliedSentLocationCell
                                cell.backgroundColor = UIColor.clear
                                cell.msgObj = msg
                                cell.locationDelegate = self
                                cell.repliedButtonPressedDelegate = self
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedSentLocationCell
                                cell.panGestureRecognizer?.delegate = self
                                
                                return cell
                            } else {
                                //2 case for !isSelf and Offer = 2, is Accepted
                                var cell : RepliedReceivedLocationCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedReceivedLocationCell", for: indexPath) as! RepliedReceivedLocationCell
                                cell.backgroundColor = UIColor.clear
                                cell.msgObj = msg
                                cell.locationDelegate = self
                                cell.repliedButtonPressedDelegate = self
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedReceivedLocationCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            }
                            
                        case .contact:
                            if msg.isSelfMessage {
                                var cell : RepliedSentContactMessageCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedSentContactMessageCollectionViewCell", for: indexPath) as! RepliedSentContactMessageCollectionViewCell
                                let contactMVMObj = ContactMessageViewModal(withMessage: msg)
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell.msgObj = contactMVMObj
                                cell.contactMessageDelegates = self
                                cell.repliedButtonPressedDelegate = self
                                cell.msgObj.presentControllerDelegate = self
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedSentContactMessageCollectionViewCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            } else {
                                var cell : RepliedReceivedContactCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedReceivedContactCollectionViewCell", for: indexPath) as! RepliedReceivedContactCollectionViewCell
                                let contactMVMObj = ContactMessageViewModal(withMessage: msg)
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell.msgObj = contactMVMObj
                                cell.contactMessageDelegates = self
                                cell.repliedButtonPressedDelegate = self
                                cell.msgObj.presentControllerDelegate = self
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedReceivedContactCollectionViewCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            }
                            
                        case .doodle:
                            if msg.isSelfMessage {
                                var cell : RepliedImageSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedImageSentCollectionViewCell", for: indexPath) as! RepliedImageSentCollectionViewCell
                                
                                //Creating View Modal from Image Message
                                let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                                imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                                cell.isReplying = self.isReplying
                                cell.replyMsg = self.replyingMessage
                                cell.chatDocID = self.chatDocID
                                cell.msgVMObj = imageMsgVMobj
                                cell.imageMsgType = messageMediaType
                                cell.imageTappedDelegate = self
                                cell.repliedButtonPressedDelegate = self
                                //Uploading image by using Image View Modal. And checking that image is not uploaded already.
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedImageSentCollectionViewCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            } else {
                                var cell : RepliedImageReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedImageReceivedCollectionViewCell", for: indexPath) as! RepliedImageReceivedCollectionViewCell
                                let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                                imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                                cell.chatDocID = self.chatDocID
                                cell.msgVMObj = imageMsgVMobj
                                cell.imageTappedDelegate = self
                                cell.imageMsgType = messageMediaType
                                cell.repliedButtonPressedDelegate = self
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedImageReceivedCollectionViewCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            }
                            
                        case .video:
                            if msg.isSelfMessage {
                                var cell : RepliedVideoSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedVideoSentCollectionViewCell", for: indexPath) as! RepliedVideoSentCollectionViewCell
                                
                                //Creating View Modal from Video Message
                                let videoMsgVMobj = VideoMessageViewModal(withMessage: msg)
                                cell.isReplying = self.isReplying
                                cell.replyMsg = self.replyingMessage
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell.chatDocID = self.chatDocID
                                cell.msgVMObj = videoMsgVMobj
                                cell.repliedButtonPressedDelegate = self
                                //Uploading video by using Video View Modal. And checking that video is not uploaded already.
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedVideoSentCollectionViewCell
                                cell.videoTappedDelegate = self
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            } else {
                                var cell : RepliedVideoReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedVideoReceivedCollectionViewCell", for: indexPath) as! RepliedVideoReceivedCollectionViewCell
                                let videoMsgVMobj = VideoMessageViewModal(withMessage: msg)
                                cell.chatDocID = self.chatDocID
                                cell.msgVMObj = videoMsgVMobj
                                cell.repliedButtonPressedDelegate = self
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedVideoReceivedCollectionViewCell
                                cell.videoTappedDelegate = self
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            }
                            
                        case .sticker:
                            if msg.isSelfMessage {
                                var cell : RepliedStickerSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedStickerSentCollectionViewCell", for: indexPath) as! RepliedStickerSentCollectionViewCell
                                
                                //Creating View Modal from Image Message
                                let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                                imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                                cell.isReplying = self.isReplying
                                cell.replyMsg = self.replyingMessage
                                cell.chatDocID = self.chatDocID
                                cell.msgVMObj = imageMsgVMobj
                                cell.repliedButtonPressedDelegate = self
                                //Uploading image by using Image View Modal. And checking that image is not uploaded already.
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedStickerSentCollectionViewCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            } else {
                                var cell : RepliedStickerReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedStickerReceivedCollectionViewCell", for: indexPath) as! RepliedStickerReceivedCollectionViewCell
                                let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                                imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                                cell.chatDocID = self.chatDocID
                                cell.msgVMObj = imageMsgVMobj
                                cell.repliedButtonPressedDelegate = self
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedStickerReceivedCollectionViewCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            }
                            
                        case .gif:
                            if msg.isSelfMessage {
                                var cell : RepliedImageSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedImageSentCollectionViewCell", for: indexPath) as! RepliedImageSentCollectionViewCell
                                
                                //Creating View Modal from Image Message
                                let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                                imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                                cell.isReplying = self.isReplying
                                cell.replyMsg = self.replyingMessage
                                cell.chatDocID = self.chatDocID
                                cell.msgVMObj = imageMsgVMobj
                                cell.imageTappedDelegate = self
                                cell.imageMsgType = messageMediaType
                                cell.repliedButtonPressedDelegate = self
                                //Uploading image by using Image View Modal. And checking that image is not uploaded already.
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedImageSentCollectionViewCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            } else {
                                var cell : RepliedImageReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedImageReceivedCollectionViewCell", for: indexPath) as! RepliedImageReceivedCollectionViewCell
                                let imageMsgVMobj = ImageMessageViewModal(withMessage: msg)
                                imageMsgVMobj.msgType = "\(messageMediaType.rawValue)"
                                cell.chatDocID = self.chatDocID
                                cell.msgVMObj = imageMsgVMobj
                                cell.imageMsgType = messageMediaType
                                cell.imageTappedDelegate = self
                                cell.repliedButtonPressedDelegate = self
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedImageReceivedCollectionViewCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            }
                            
                        case .document:
                            if msg.isSelfMessage {
                                var cell : RepliedDocumentSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedDocumentSentCollectionViewCell", for: indexPath) as! RepliedDocumentSentCollectionViewCell
                                let docMVMObj = DocumentMessageViewModal(withMessage: msg)
                                docMVMObj.isReplying = self.isReplying
                                docMVMObj.replyMsg = self.replyingMessage
                                cell.documentMVMObj = docMVMObj
                                docMVMObj.chatDocID = self.chatDocID
                                cell.documentMessageDelegate = self
                                cell.repliedButtonPressedDelegate = self
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedDocumentSentCollectionViewCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            } else {
                                //2 case for !isSelf and Offer = 2, is Accepted
                                var cell : RepliedDocumentReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedDocumentReceivedCollectionViewCell", for: indexPath) as! RepliedDocumentReceivedCollectionViewCell
                                let docMVMObj = DocumentMessageViewModal(withMessage: msg)
                                cell.documentMVMObj = docMVMObj
                                docMVMObj.chatDocID = self.chatDocID
                                cell.documentMessageDelegate = self
                                cell.repliedButtonPressedDelegate = self
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedDocumentReceivedCollectionViewCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            }
                            
                        case .audio:
                            //Creating View Modal from Image Message
                            if msg.isSelfMessage {
                                var cell : RepliedAudioSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedAudioSentCollectionViewCell", for: indexPath) as! RepliedAudioSentCollectionViewCell
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell.msgObj = msg
                                cell.audioPlayerDelegate = self
                                cell.repliedButtonPressedDelegate = self
                                let audioMVMObj = AudioMessageViewModal(withMessage: msg)
                                audioMVMObj.isReplying = self.isReplying
                                audioMVMObj.replyMsg = self.replyingMessage
                                audioMVMObj.chatDocID = self.chatDocID
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedAudioSentCollectionViewCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            } else {
                                var cell : RepliedAudioReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "RepliedAudioReceivedCollectionViewCell", for: indexPath) as! RepliedAudioReceivedCollectionViewCell
                                let messageVMObj = MessageViewModal(withMessage: msg)
                                cell.audioPlayerDelegate = self
                                cell.repliedButtonPressedDelegate = self
                                cell.msgObj = msg
                                cell = messageVMObj.addLastDateAndTime(toCell: cell) as! RepliedAudioReceivedCollectionViewCell
                                cell.panGestureRecognizer?.delegate = self
                                return cell
                            }
                        case .replied:
                            jsqcell.panGestureRecognizer?.delegate = self
                            return jsqcell
                            
                        case .deleted:
                            break
                        }
                    }
                }
                jsqcell.panGestureRecognizer?.delegate = self
                return jsqcell
                
            case .deleted:
                if msg.isSelfMessage {
                    var cell : DeletedSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "DeletedSentCollectionViewCell", for: indexPath) as! DeletedSentCollectionViewCell
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! DeletedSentCollectionViewCell
                    cell.panGestureRecognizer?.delegate = self
                    return cell
                } else {
                    var cell : DeletedReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "DeletedReceivedCollectionViewCell", for: indexPath) as! DeletedReceivedCollectionViewCell
                    let messageVMObj = MessageViewModal(withMessage: msg)
                    cell = messageVMObj.addLastDateAndTime(toCell: cell) as! DeletedReceivedCollectionViewCell
                    cell.panGestureRecognizer?.delegate = self
                    return cell
                }
            }
        }
        jsqcell.panGestureRecognizer?.delegate = self
        return jsqcell
    }
    
    //MARK: To show timestamp above message
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        let message = self.messages[indexPath.item]
        let messageVMObj = MessageViewModal(withMessage: message)
        if(indexPath.item == 0) {
            return messageVMObj.setDay()
        } else {
            let currentMessage = self.messages[indexPath.item]
            let previousMessage = self.messages[indexPath.item - 1]
            if DateHelper().compareDates(currentDate: currentMessage.date, previousDate: previousMessage.date) {
                return nil
            } else {
                return messageVMObj.setDay()
            }
        }
    }
    
    
    
    
    //MARK: Image Messages Methods
    func getimageMessageObj(withimage image : UIImage, isSelf : Bool, thumbnailData: String?, withMediaType type: String) -> Message? {
        guard let msgData = self.getImageMessageParams(withImage: image, isSelf: isSelf, imageURL: nil, withMediaType: type, isReplying: self.isReplying, replyingMsgObj: self.replyingMessage) else { return nil }
        //create image cache URL here.
        var timeStamp: String = ""
        if let ts = msgData["timestamp"] as? String {
            timeStamp = ts
        } else if let ts = msgData["timestamp"] as? String {
            timeStamp = ts
        } else if let ts = msgData["timestamp"] as? Int64 {
            timeStamp = "\(ts)"
        }
        
        //Storing image into cache with the name of the image.
        let imageName = "hola"+timeStamp+".jpg"
        if !ImageCache.default.imageCachedType(forKey: imageName).cached {
            ImageCache.default.store(image, forKey: imageName)
        }
        
        var mediaURL = imageName
        if let mURL = msgData["mediaURL"] as? String {
            mediaURL = mURL
        }
        
        var gpMessageType = ""
        if let gpMessageTyp = msgData["gpMessageType"] as? String{
            gpMessageType = gpMessageTyp
        }
        
        let message = Message(forData: msgData, withDocID: chatDocID!, andMessageobj: msgData, isSelfMessage: isSelf, mediaStates: .notUploaded, mediaURL: mediaURL, thumbnailData: thumbnailData, secretID: nil, receiverIdentifier: "", messageData: nil, isReplied: self.isReplying,gpMessageType:gpMessageType)
        return message
    }
    
    func getImageMessageParams(withImage image : UIImage, isSelf : Bool, imageURL : String?, withMediaType type: String, isReplying: Bool, replyingMsgObj: Message?) -> [String : Any]? {
        let timeStamp : String = "\(UInt64(floor(Date().timeIntervalSince1970 * 1000)))"
        var params = [String :Any]()
        let userDocVMObject = UsersDocumentViewModel(couchbase: couchbaseObj)
        guard let userData = userDocVMObject.getUserData() else { return nil }
        guard let imageData = self.createThumbnail(forImage: image) else { return nil }
        let imgData: NSData = NSData(data: UIImageJPEGRepresentation((image), 1)!)
        let imageSize: Int = imgData.length
        if isSelf {
            params["from"] = Helper.getMQTTID()
            params["to"] = self.receiverID! as Any
        } else {
            params["from"] = self.receiverID! as Any
            params["to"] = Helper.getMQTTID()
        }
        if let url = imageURL {
            params["payload"] = url as Any
        }
        params["toDocId"] = self.chatDocID! as Any
        params["timestamp"] = timeStamp as Any
        params["id"] = timeStamp as Any
        params["type"] = type as Any
        params["thumbnail"] = imageData
        params["dataSize"] = imageSize as Any
        params["userImage"] = userData["userImageUrl"]! as Any
        params["name"] = userData["userName"]! as Any
        params["receiverIdentifier"] = userData["userName"]! as Any
        
        if isReplying == true, let replyMsg = replyingMsgObj {
            if let pPload = replyMsg.messagePayload, let pFrom = replyMsg.messageFromID, let prIdentifier = replyMsg.receiverIdentifier, let msgId = replyMsg.timeStamp, let previousType = replyMsg.messageType {
                params["previousPayload"] = pPload as Any
                params["previousFrom"] = pFrom as Any
                params["replyType"] = type as Any
                params["type"] = "10" as Any
                params["previousReceiverIdentifier"] = prIdentifier as Any
                params["previousId"] = msgId as Any
                params["previousType"] = "\(previousType.hashValue)" as Any
                if previousType == .replied {
                    if let pType = replyMsg.repliedMessage?.replyMessageType {
                        params["previousType"] = "\(pType.hashValue)" as Any
                    }
                }
                if previousType == .image || previousType == .doodle || previousType == .video {
                    if let tData = replyMsg.thumbnailData {
                        params["previousPayload"] = tData
                    }
                } else if previousType == .location {
                    params["previousPayload"] = "Location"
                }
                else if previousType == .replied {
                    if let repliedMsg = self.replyingMessage?.repliedMessage {
                        if repliedMsg.replyMessageType == .image || repliedMsg.replyMessageType == .doodle || repliedMsg.replyMessageType == .video {
                            if let tData = replyMsg.thumbnailData {
                                params["previousPayload"] = tData
                            }
                        } else if repliedMsg.replyMessageType == .location {
                            params["previousPayload"] = "Location"
                        }
                    }
                }
            }
        }
        return params
    }
    
    //MARK: Height of cell to show timestamp
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellTopLabelAt indexPath: IndexPath) -> CGFloat {
        
        //set height for group Msg Tag
        if isGroup {
            let msg = self.messages[indexPath.item]
            if  msg.gpMessageType != "" {
                return 0.0
            }
        }
        
        if(indexPath.item == 0){
            return 20.0
        } else {
            let currentMessage = self.messages[indexPath.item]
            let previousMessage = self.messages[indexPath.item - 1]
            if DateHelper().compareDates(currentDate: currentMessage.date, previousDate: previousMessage.date) {
                return 0.0
            } else {
                return 20.0
            }
        }
        
        
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForMessageBubbleTopLabelAt indexPath: IndexPath) -> CGFloat {
        
        //set height for group Msg Tag
        if isGroup {
            let msg = self.messages[indexPath.item]
            if  msg.gpMessageType != "" {
                return 0.0
            }
        }
        
        if isGroup == false { return 0.0}
        let msg = self.messages[indexPath.item]
        if indexPath.item - 1 > 0 {
            let prevMsg = self.messages[indexPath.item - 1]
            if prevMsg.senderId == msg.senderId {
                return 0.0
            }
        }
        return 20.0
    }
    
    //MARK: Responding to collection view tap events
    override func collectionView(_ collectionView: JSQMessagesCollectionView, didTapMessageBubbleAt indexPath: IndexPath) {
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
        let message = messages[indexPath.item]
        if message.isMediaMessage {
            if let mediaItem = message.media as? JSQVideoMediaItem {
                //                guard let mediaURL = mediaItem.fileURL else { return }
                //                let player = AVPlayer(url: mediaURL)
                //                let playerViewController = AVPlayerViewController()
                //                playerViewController.player = player
                //                self.present(playerViewController, animated: true, completion: nil)
            }
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellBottomLabelAt indexPath: IndexPath) -> CGFloat {
        return 2.0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, didTapCellAt indexPath: IndexPath, touchLocation: CGPoint) {
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
    }
    
    override func didPressSend(_ button: UIButton, withMessageText text: String, senderId: String, senderDisplayName: String, date: Date) {
        if (self.inputToolbar.contentView?.textView?.text.count == 0 ) {
            
            //send Voice here
        } else {
            checkForNotMatchedUser(messageText: text)
        }
    }
    
    func showCoinsPopupForChat(){
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let syncView = spendSomeCoinPopUp(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        syncView.title = StringConstants.spendSomeCoins()
        syncView.loadPopUpView()
        syncView.spendSomeCoinDelegate = self
        window.addSubview(syncView)
    }
    
    func checkForNotMatchedUser(messageText:String) {
        
        if favoriteObj?.isUserBlocked == 1 {
            unblockToSendMessage()
            print("blocked \(favoriteObj?.firstName ?? "")")
        
            return
        }
        
        if(favoriteObj?.isMatchedUser == 1) {
            //send text here
            sendMessage()
        } else {
            //show popup here.
            if Helper.isUserHasMinimumCoins(minValue: coinsFor.sendUnMatchMessage) {
                self.coinBtnPressed(sender: UIButton())
            } else {
                if(favoriteObj?.chatInitiatedBy == 0) {
                    self.coinBtnPressed(sender: UIButton())
                } else {
                    Helper.showAlertWithMessage(withTitle:StringConstants.Oops(), message:StringConstants.noCoinsForMessage(), onViewController:self)
                }
            }
        }
    }
    
    
    
    
    func sendMessage() {
        self.updateMessageUIAfterSendingMessage(withText:(self.inputToolbar.contentView?.textView?.text)!, andType: "0", withData: nil)
        if self.chatDocID != nil {
            UserDefaults.standard.setValue("", forKey:self.chatDocID)
        }
    }
    
    
    func sendMessageThroughAPI(requestParms:[String:Any]) {
        
//        Helper.showProgressIndicator(withMessage:StringConstants.sendingMsg)
        
        API.requestPOSTURL(serviceName: APINAMES.sendMsgWithOutMatch,
                           withStaticAccessToken:false,
                           params: requestParms,
                           success: { (response) in
                            print("response:")
                            
                            if let statuscode = response["code"] as? Int {
                                if statuscode == 401 {
                                    Helper.changeRootVcInVaildToken()
                                }
                            }else {
                               
                                if let data = response.dictionaryObject {
                                    if let coinsDetails = data["coinWallet"] as? [String:Any] {
                                        if let numberOfCoins = coinsDetails["Coin"] as? Int {
                                            UserDefaults.standard.set(numberOfCoins, forKey: "coinBalance")
                                            UserDefaults.standard.synchronize()
                                            self.updateCoinBalance()
                                        }
                                    }
                                }
                                
                            }
                            
                           
                            
                         //   Helper.hideProgressIndicator()
        },
                           failure: { (Error) in
                            self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: Error.localizedDescription)
                            //Helper.hideProgressIndicator()
        })
    }
    
    /// For updating the chat doc inside the contact.
    func updateChatDocIDIntoContactDB() {
        //        if isSecretChat {
        self.chatDocID = self.getChatDocID()
        if let userID = favoriteObj?.userId {
//            favoriteViewModel.updateContactDoc(withUserID: userID, andChatDocId: self.chatDocID)
        } else if let userID = self.chatViewModelObj?.userID {
//            favoriteViewModel.updateContactDoc(withUserID: userID, andChatDocId: self.chatDocID)
        }
        //        }
    }
    
    func updateMessageUIAfterSendingMessage(withText text: String, andType type: String, withData messageData: [String : Any]?) {
        
        showUnmatchCoinsPrompt(show:false)
        
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        self.updateChatDocIDIntoContactDB()
        guard var message = chatsDocVMObject.makeMessageForSendingBetweenServers(withText: text, andType: type, isReplying: self.isReplying, replyingMsgObj: self.replyingMessage, senderID: self.senderId(), receiverId: self.receiverID, chatDocId: self.chatDocID) else { return }
        
        var timer = 0
        
        if chatViewModelObj?.secretID != "" && chatViewModelObj != nil{
            message["secretId"] = chatViewModelObj?.secretID
            //var timer = 0
            if  let time = UserDefaults.standard.object(forKey:(chatViewModelObj?.secretID)!) as? Int{
                timer = time
            }
            message["dTime"] = timer
            mqttChatManager.sendMessage(toChannel: "\(self.receiverID!)", withMessage: message, withQOS: .atLeastOnce)
        }else {
            if(self.favoriteObj?.isMatchedUser == 1){
                mqttChatManager.sendMessage(toChannel: "\(ChatAppConstants.MQTT.messagesTopicName)\(self.receiverID!)", withMessage: message, withQOS: .atLeastOnce)
            } else {
                //chat started by this user. so deducting coins from this user.
                if(favoriteObj?.chatInitiatedBy == 1) {
                    AudioHelper.sharedInstance.playCoinsSound()
                    for i in 1..<7 {
                        if(self.inputToolbar.superview != nil) {
                            self.view.applyCoinsAnimation(indexAt:i
                                , fromView:self.inputToolbar.superview!, coins:" 2 0 0 ")
                        }
                    }
                    self.sendMessageThroughAPI(requestParms:message)
                } else {
                    
                    //if two unmatched users started the conversation then if this user is not started the conversation then message in MQTT. 
                    mqttChatManager.sendMessage(toChannel: "\(ChatAppConstants.MQTT.messagesTopicName)\(self.receiverID!)", withMessage: message, withQOS: .atLeastOnce)
                }
            }
        }
        
        guard let MsgObjForDB = chatsDocVMObject.getMessageObject(fromData: message , withStatus: type, isSelf: true, fileSize: 0, documentData: nil, isReplying: self.isReplying, replyingMsgObj: self.replyingMessage) else { return }
        var msgObj = MsgObjForDB
        guard let dateString = DateHelper().getDateString(fromDate: Date()) else { return }
        msgObj["sentDate"] = dateString as Any
        guard let chatDocID = self.chatDocID else { return }
        if let chatDta = couchbaseObj.getData(fromDocID: chatDocID) {
            chatsDocVMObject.updateChatData(withData: chatDta, msgObject : msgObj as Any, inDocID  : chatDocID, isCreatingChat: false)
        }
        self.chatsDocVMObject.updateChatDoc(withMsgObj: msgObj, toDocID: chatDocID)
        let msgObject = Message(forData: msgObj, withDocID: chatDocID, andMessageobj: message, isSelfMessage: true, mediaStates: .notApplicable, mediaURL: nil, thumbnailData: nil, secretID: nil, receiverIdentifier: "", messageData: messageData, isReplied: self.isReplying,gpMessageType: "" ,dTime: timer)
        messages.append(msgObject)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.finishSendingMessage(animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.removeReplyView()
            }
        }
    }
    
    
    //MARK: - didPressAccessoryButton
    override func didPressAccessoryButton(_ sender: UIButton) {
        self.inputToolbar.contentView!.textView!.resignFirstResponder()
        
    }
    
    
    func openAttachementSheet() {
        let sheet = UIAlertController(title: nil , message: nil, preferredStyle: .actionSheet)
        let photoAction = UIAlertAction(title: StringConstants.camera(), style: .default) { (action : UIAlertAction) in
            self.openLivecamera()
        }
        
        let photolibAction = UIAlertAction(title: StringConstants.camerandVideo(), style: .default) { (action : UIAlertAction) in
            
            let imagePicker = ImagePickerViewModel(self)
            imagePicker.openIphoneLibrery(complition: { imgArr in
                let story = UIStoryboard.init(name: "Chat", bundle: nil)
                let preview =  story.instantiateViewController(withIdentifier: "ImagePickerPreview") as? ImagePickepreviewController
                preview?.imageArray = imgArr
                preview?.delegate = self
                preview?.isComingFromCamera = false
                imagePicker.pickerView?.present(preview!, animated: true, completion: nil)
            })
        }
        
        let giphyAction = UIAlertAction.init(title: StringConstants.giphy(), style: .default) { action  in
            self.performSegue(withIdentifier: SegueIds.chatTogifsegue, sender: true)
        }
        
        let stickerAction = UIAlertAction.init(title: StringConstants.sticker(), style: .default) { action  in
            self.performSegue(withIdentifier: SegueIds.chatTogifsegue, sender: false)
        }
        
        let doodleAction = UIAlertAction.init(title: StringConstants.doodle(), style: .default) { action  in
            self.doodleViewModel = DoodleViewModel.init(self)
            self.doodleViewModel?.OpenDoodleView()
        }
        
//        let document = UIAlertAction.init(title: StringConstants.document(), style: .default) { action in
//            self.documentViewModel = DocumentViewModel.init(self)
//            self.documentViewModel?.delegate = self
//            self.documentViewModel?.openDocumentView()
//        }
        
        let locationAction = UIAlertAction(title: StringConstants.location(), style: .default) { (action : UIAlertAction) in
            self.locationViewModel = LocationPickerViewModel.init(self)
            self.locationViewModel?.delegate = self
            self.locationViewModel?.openLocationPicker()
        }
        
        let contactAction = UIAlertAction(title: StringConstants.contact(), style: .default) { (action : UIAlertAction) in
            self.contactModel = ContactchatViewModel(self)
            self.contactModel?.delegate = self
            self.contactModel?.openContectView()
        }
        
        let cancelAction = UIAlertAction(title: StringConstants.cancel(), style: .cancel, handler: nil)
        sheet.addAction(photoAction)
        sheet.addAction(photolibAction)
        sheet.addAction(cancelAction)
        sheet.addAction(locationAction)
        //sheet.addAction(contactAction)
        sheet.addAction(giphyAction)
        sheet.addAction(doodleAction)
        sheet.addAction(stickerAction)
       // sheet.addAction(document)
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    /// Open cameraView
    func openLivecamera() {
        if(!UIImagePickerController.isSourceTypeAvailable(.camera)){
            let alert = UIAlertController.init(title: StringConstants.message(), message: StringConstants.noCamera(), preferredStyle: .alert)
            let action = UIAlertAction.init(title: StringConstants.ok(), style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        else {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.mediaTypes = ["public.image","public.movie"]
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
}

extension ChatViewController {
    @objc func receivedMessage(notification: NSNotification) {
        let userInfo = notification.userInfo as! [String: Any]
        
        if userInfo["isDelete"] != nil {
            if (userInfo["isDelete"] as? Bool == true) && (userInfo["docID"] as? String ==  self.chatDocID) , let megID = userInfo["messID"] as? String{
                self.deletePerticularMessageFromChat(messageID: megID)
            }
            
            return
        }
        
        let status = userInfo["status"] as! String
        guard let content = userInfo["message"] as? [String : Any] else { return }
        
        switch status {
        case "1","2", "3" :
            
            self.chatsDocVMObject.updateStatusForChatMessages(withStatus: status, chatDocID: self.getChatDocID())
            self.updateChatStatus()
        default:
            self.updateCurrentChatMessages(withmessage: content)
        }
        
        self.updateChatDocIDIntoContactDB()
    }
    
    @objc func receivedLastSeenStatus(notification: NSNotification) {
        let userInfo = notification.userInfo as! [String: Any]
        guard let message = userInfo["message"] as? [String : Any] else { return }
        if ((message["userId"] as? String) == self.receiverID) {
            if ((message["status"] as? Int) == 1) { // online
                self.currentChatStatusLabelOutlet.text = "Online"
            } else if ((message["status"] as? Int) == 0) { //offline
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    var tStamp = ""
                    if let timeStamp = message["timestamp"] as? String {
                        tStamp = timeStamp
                    } else if let timeStamp = message["timestamp"] as? Int {
                        tStamp = "\(timeStamp)"
                    }
                    guard let date = DateHelper().getDate(forLastSeenTimeString: tStamp) else { return }
                    let dateStr = DateHelper().lastSeenTime(date: date)
                    if dateStr.range(of:"Yesterday") != nil {
                        let timeStampTime = "\(DateHelper().lastSeenTime(date: date))"
                        self.currentChatStatusLabelOutlet.text = timeStampTime
                    } else {
                        let timeStampTime = StringConstants.lastSeen() + " \(DateHelper().lastSeenTime(date: date))"
                        self.currentChatStatusLabelOutlet.text = timeStampTime
                    }
                })
            } else {
                self.currentChatStatusLabelOutlet.text = StringConstants.Offline()
            }
        }
    }
    
    @objc func receivedTypingStatus(notification: NSNotification) {
        let userInfo = notification.userInfo as! [String: Any]
        guard let message = userInfo["message"] as? [String : Any] else { return }
        if ((message["from"] as? String) == self.receiverID) {
            self.currentChatStatusLabelOutlet.text = StringConstants.typing()
            if !isTypingVisible {
                self.isTypingVisible = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.isTypingVisible = false
                    self.currentChatStatusLabelOutlet.text = StringConstants.online()
                })
            }
        }
    }
    
    func didReceive(withMessage message: Any, inTopic topic: String) {
        guard let msgServerObj = message as? [String:Any] else { return }
        if let fromID = msgServerObj["from"] as? String {
            if fromID == selfID! {
            } else {
                self.messages = chatsDocVMObject.getMessagesFromChatDoc(withChatDocID: self.chatDocID)
                self.finishReceivingMessage()
            }
        }
    }
}

extension ChatViewController {
    override func textViewDidChange(_ textView: UITextView) {
      self.inputToolbar.contentView?.rightBarButtonItem?.setImage(#imageLiteral(resourceName: "Chatsend"), for: .normal)
        super.textViewDidChange(textView)
        if self.chatDocID != nil {
            UserDefaults.standard.setValue(textView.text, forKey:self.chatDocID)
        }
        self.sendTypingStatus()
        
        if(favoriteObj?.isMatchedUser == 0) {
            if(favoriteObj?.chatInitiatedBy == 1) {
                if(textView.text.count > 0) {
                    showUnmatchCoinsPrompt(show:true)
                } else {
                    showUnmatchCoinsPrompt(show:false)
                }
            }
        }
    }
    
    
    override func textViewDidEndEditing(_ textView: UITextView) {
        showUnmatchCoinsPrompt(show: false)
    }
    
    override func textViewDidBeginEditing(_ textView: UITextView) {
        self.inputToolbar.contentView?.rightBarButtonItem?.setImage(#imageLiteral(resourceName: "Chatsend"), for: .normal)
        if(favoriteObj?.isMatchedUser == 0) {
            if(textView.text.count > 0 ){
                showUnmatchCoinsPrompt(show:true)
            }
            updateFrameOfUnMatchCoinsPrompt()
        }
    }
    
    func updateFrameOfUnMatchCoinsPrompt() {
        let window = UIApplication.shared.keyWindow!
        if let sampleView = window.viewWithTag(23000) {
             sampleView.frame = CGRect(x:0, y:window.frame.size.height - (self.inputToolbar.superview?.frame.origin.y)! - 64, width:self.view.frame.size.width, height:100)
        }
    }
    
    func showUnmatchCoinsPrompt(show:Bool) {
        if show {
            let window = UIApplication.shared.keyWindow!
            if(window.viewWithTag(23000) == nil) {
                let sampleView = UIView(frame: CGRect(x:self.view.frame.size.width - 240, y:window.frame.size.height - (self.inputToolbar.superview?.frame.height)! - 60, width:220, height:60))
                sampleView.backgroundColor = UIColor.clear
                
                let messagebutton = UIButton(frame: CGRect(x:0, y: 0, width:0, height:60))
                messagebutton.setTitle(StringConstants.coinsForMsg(), for: .normal)
                messagebutton.setTitleColor(UIColor.white, for: .normal)
                messagebutton.setBackgroundImage(#imageLiteral(resourceName: "chatcoinpopup"), for: .normal)
                messagebutton.titleLabel?.font = UIFont (name: CircularAir.Bold, size:15)
                messagebutton.titleLabel?.textAlignment = .center
                messagebutton.isUserInteractionEnabled = false
                messagebutton.center = sampleView.center
                messagebutton.tag = 23001
                sampleView.tag = 23000
                window.bringSubview(toFront:sampleView)
                UIView.animate(withDuration:0, animations:{
                    window.addSubview(sampleView)
                }, completion:{ (finshed) in
                    if(finshed) {
                        UIView.animate(withDuration: 0.2, animations: {
                            sampleView.addSubview(messagebutton)
                            messagebutton.frame = CGRect(x:0, y: 0, width:220, height:60)
                            var originalFrame = messagebutton.titleLabel?.frame
                            originalFrame?.origin.y = 8
                            messagebutton.titleLabel?.frame = originalFrame!
                            sampleView.layoutIfNeeded()
                        })
                    }
                })
            } else {
                //updating the frame.
                let sampleView = window.viewWithTag(23000)
                 sampleView?.frame =  CGRect(x:self.view.frame.size.width - 240, y:window.frame.size.height - (self.inputToolbar.superview?.frame.height)! - 60, width:220, height:60)
                if let messagebutton = sampleView?.viewWithTag(23001) as? UIButton {
                    messagebutton.frame = CGRect(x:0, y: 0, width:220, height:60)
                    var originalFrame = messagebutton.titleLabel?.frame
                    originalFrame?.origin.y = 8
                    messagebutton.titleLabel?.frame = originalFrame!
                }
                sampleView?.layoutIfNeeded()
            }
        } else {
            let window = UIApplication.shared.keyWindow!
            if let fetchView = window.viewWithTag(23000) {
                fetchView.removeFromSuperview()
            }
        }
    }
    
}

extension ChatViewController {
    func updateCurrentChatMessages(withmessage message: [String : Any]) {
        self.chatsDocVMObject.updateMessageStatus(withMessageObj: message)
        guard let chatdocId = self.chatDocID else {return}
        self.messages = self.chatsDocVMObject.getMessagesFromChatDoc(withChatDocID: chatdocId)
        self.finishReceivingMessage()
        let state = UIApplication.shared.applicationState
        if state == .background {
            return
        }
        self.sendReadAcknowledgment(withMessage: message)
    }
    
    func sendReadAcknowledgment(withMessage message : [String : Any]) {
        
        guard let params = self.chatsDocVMObject.getMessageObjectForUpdatingStatus(withData: message, andStatus: "3", toDocID: self.chatDocID) as? [String:Any] else { return }
        mqttChatManager.sendAcknowledgment(toChannel: "\(MQTTTopic.acknowledgementTopicName)\(self.receiverID!)", withMessage: params, withQOS: .atMostOnce)
        
        //Start Timer if secret Chat start Timerrr for read message
        if message["secretId"] != nil && message["dTime"] != nil {
            if ((message["secretId"]  as? String) != "" )  && ((message["dTime"] as? Int) !=  0 ) && chatViewModelObj != nil  && (message["gpMessageType"] as? String == "" || message["gpMessageType"] == nil){
                print("Hello Frdssss Chai pee lo im  in sendReadAcknowledgment \(message)")
                var dTimee = 0
                if let dTime = message["dTime"] as? Int {
                    dTimee = dTime
                }
                guard let messageID = message["id"] as? String  else {return}
                self.chatsDocVMObject.secretChatTimer(docID:self.chatDocID , messageID: messageID, dTime:dTimee)
            }
        }
    }
    
    func updateChatForReadMessage(toDocID docID : String,isControllerAppearing : Bool) {
        if var chatData = couchbaseObj.getData(fromDocID: docID) {
            if ((chatData["hasNewMessage"] as? Bool) == true) {
                // here we are sending the received acknowledgment for last message.
                if isControllerAppearing {
                    self.fetchLastMessageFromSenderAndSendAcknowledgment(withChatData: chatData)
                }
            }
            chatData["hasNewMessage"] = false
            chatData["newMessageCount"] = "0"
            couchbaseObj.updateData(data: chatData, toDocID: docID)
        }
    }
    
    func fetchLastMessageFromSenderAndSendAcknowledgment(withChatData chatData: [String:Any]) {
        let msgArray = chatData["messageArray"] as! [[String:Any]]
        for (_, messageData) in msgArray.enumerated() {
            if ((messageData["timestamp"] as? String) == (chatData["lastMessageDate"] as? String)) {
                self.sendReadAcknowledgment(withMessage: messageData)
            } else if ((messageData["id"] as? String) == (chatData["lastMessageDate"] as? String)) {
                self.sendReadAcknowledgment(withMessage: messageData)
            } else if let tStamp = messageData["timestamp"] as? Int, let lMsgDate = chatData["lastMessageDate"] as? String {
                if tStamp == Int(lMsgDate) {
                    self.sendReadAcknowledgment(withMessage: messageData)
                }
            }
        }
    }
    
    func updateChatStatus() {
        self.messages = self.chatsDocVMObject.getMessagesFromChatDoc(withChatDocID: self.chatDocID)
        self.finishReceivingMessage(animated: false)
    }
}

extension Sequence where Iterator.Element: Hashable {
    func uniq() -> [Iterator.Element] {
        var seen = Set<Iterator.Element>()
        return filter { seen.update(with: $0) == nil }
    }
}

func == (lhs: Message, rhs: Message) -> Bool {
    guard let lhsTimeStamp = lhs.uniquemessageId, let rhsTimeStamp = rhs.uniquemessageId else { return false }
    return (lhsTimeStamp == rhsTimeStamp)
}


//MARK:-  Group Methods

extension ChatViewController {
    
    //Group releted Api call
    func getGroupInfoFromServer(_ groupID: String?) {
//        guard let groupid = groupID else { return }
//        Helper.showProgressIndicator(withMessage: "Loading...")
//        let strURL = AppConstants.getAllmembersofGroup + "\(groupid)"
//        guard let keyDict = Locksmith.loadDataForUserAccount(userAccount: AppConstants.keyChainAccount) else {return}
//        guard  let token = keyDict["token"] as? String  else {return}
//        let headerParams = ["authorization":AppConstants.authorization,"token":token]
//
//        let apiCall = RxAlmofireClass()
//        apiCall.newtworkRequestAPIcall(serviceName: strURL, requestType: .get, parameters: nil,headerParams:headerParams, responseType: AppConstants.resposeType.getAllgpMembers.rawValue)
//        apiCall.subject_response
//            .subscribe(onNext: {dict in
//                 Helper.hideProgressIndicator()
//                guard let responseKey = dict[AppConstants.resposeTypeKey] as? String else {return}
//                if responseKey == AppConstants.resposeType.getAllgpMembers.rawValue {
//
//                    guard let responseDict = dict["data"] as? [String:Any] else {return}
//                    guard let ownNum = UserDefaults.standard.object(forKey: AppConstants.UserDefaults.userNumber) as? String else {return}
//
//                    let memArr = responseDict["members"] as? [[String:Any]]
//                    var gpmemArr = [[String:Any]]()
//                    var sortedmemArr = [[String:Any]] ()
//                    for i:[String:Any] in memArr! {
//
//                        var dict = [String:Any]()
//                        dict["memberId"] = i["userId"] as? String ?? ""
//                        dict["memberIdentifier"] = i["userIdentifier"] as? String ?? ""
//                        dict["memberImage"] = i["profilePic"] as? String ?? ""
//                        dict["memberStatus"] = i["socialStatus"] as? String ?? ""
//                        dict["memberIsAdmin"] = i["isAdmin"] as? Bool
//
//                        if i["userIdentifier"] as? String == ownNum{
//                            sortedmemArr.append(dict)
//                        }else {
//                            gpmemArr.append(dict)
//                        }
//                    }
//
//                    sortedmemArr.append(contentsOf: gpmemArr)
//
//                    let storedict = ["groupMembersArray": sortedmemArr ,
//                                     "createdByMemberId": responseDict["createdByMemberId"] as? String ?? "" ,
//                                     "createdByMemberIdentifier": responseDict["createdByMemberIdentifier"] as? String ?? "" ,
//                                     "createdAt": String(describing: responseDict["createdAt"]!) ,
//                                     "isActive":true as Bool
//                        ] as [String:Any]
//
//
//                    let groupDocID = Couchbase.sharedInstance.createDocument(withProperties: ["groupMembersDocId":storedict] as [String : Any])
//
//                    //Add groupId and groupDocID in GroupChatsDocument
//                    self.updateGroupChatsDocument(groupID: groupid, groupDocId: groupDocID!)
//
//                    let mainDoc = self.couchbaseObj.getDocumentObject(fromDocID:groupDocID!)
//                    let gpData = mainDoc?.properties!
//                    let gpInfoDict = gpData!["groupMembersDocId"] as! [String:Any]
//                    self.groupInfo = gpInfoDict
//                    self.groupDocumentID = groupDocID!
//                    self.isActiveGroup = gpInfoDict["isActive"] as! Bool
//                }
//            }, onError: {error in
//                 Helper.hideProgressIndicator()
//            })
    }
    
    
    //Update groupId and groupDocId in GroupChatsDocument
    func updateGroupChatsDocument(groupID: String ,groupDocId:String){
        let documentID = UserDefaults.standard.object(forKey: "GroupChatsDocument")
        if  let document = Couchbase.sharedInstance.getDocumentObject(fromDocID: documentID as! String){
            let maindict = document.properties
            var dict = maindict!["GroupChatsDocument"] as! [String:Any]
            dict[groupID] = groupDocId
            do{
                try document.update { (newRev) -> Bool in
                    newRev["GroupChatsDocument"] = dict
                    return true
                }
            }
            catch let error {
                DDLogDebug("cdkvndkv\(error)")
            }
        } else {
            DDLogDebug("failed to get documet from provided DocID")
            return}
    }
    
    
    
    
    //Get groupDocument ID from Group DataBase
    func getgpDocIdfromgroupDatabase(groupID:String){
        let documentID = UserDefaults.standard.object(forKey: "GroupChatsDocument")
        if  let document = self.couchbaseObj.getDocumentObject(fromDocID: documentID as! String){
            let maindict = document.properties
            var dict = maindict!["GroupChatsDocument"] as! [String:Any]
            if dict[groupID] != nil {
                print("docuemtID is there \(String(describing: dict[groupID]))")
                let mainDoc = self.couchbaseObj.getDocumentObject(fromDocID:dict[groupID] as! String)
                let gpData = mainDoc?.properties!
                let gpInfoDict = gpData!["groupMembersDocId"] as! [String:Any]
                groupInfo = gpInfoDict
                groupDocumentID = dict[groupID] as? String
                isActiveGroup = gpInfoDict["isActive"] as! Bool
                showGroupMembersOnNavigation(memArr: groupInfo!["groupMembersArray"] as? [[String : Any]])
                
            }else {
                // if you dont found groupDocument with groupId then get it from API
                self.getGroupInfoFromServer(groupID)
            }
        }
    }
    
    
    //Show Group Members On Navigation
    func showGroupMembersOnNavigation(memArr:[[String:Any]]?){
        
//        guard let  groupMemArr  = memArr else { return }
//        self.groupMembers = groupMemArr
//        for  mem in groupMemArr {
//            guard let num =  mem["memberIdentifier"] as? String  else {return}
//            if self.currentChatStatusLabelOutlet.text?.count == 0 {
//                self.currentChatStatusLabelOutlet.text  = Helper.getNameFormDatabase(num: num)
//            }else {
//                self.currentChatStatusLabelOutlet.text  =  self.currentChatStatusLabelOutlet.text! + "," + " " + Helper.getNameFormDatabase(num: num) }
//        }
    }
    
    
    //Update groupINfo RealTime
    @objc func updateGroupInfo(notification:Notification){
        let mainDoc = self.couchbaseObj.getDocumentObject(fromDocID:self.groupDocumentID!)
        let gpData = mainDoc?.properties!
        let gpInfoDict = gpData!["groupMembersDocId"] as! [String:Any]
        groupInfo = gpInfoDict
        if let groupInfoArr  = groupInfo?["groupMembersArray"] as? [[String:Any]] {
            self.currentChatStatusLabelOutlet.text = ""
            showGroupMembersOnNavigation(memArr: groupInfoArr)
        }
        if let isActive = groupInfo?["isActive"] as? Bool{
            isActiveGroup  = isActive
            if isActive == true {self.removeFooterView()}
            else {self.addFooterView()}
        }
    }
}




//MARK: - Secret Chat Methodss


extension ChatViewController {
    
    func updateStatusForSecretChat(withStatus status : String, chatDocID : String?) {
        guard let chatDocID = chatDocID else { return }
        guard let chatData = Couchbase.sharedInstance.getData(fromDocID: chatDocID) else { return }
        var chatDta = chatData
        var msgArray = chatDta["messageArray"] as! [[String:Any]]
        
        for (idx, dic) in msgArray.enumerated() {
            if ((dic["deliveryStatus"] as? String) != "3") && ((dic["isSelf"] as? Bool) == true) {
                var msgData = dic
                msgData["deliveryStatus"] = status
                msgArray[idx] = msgData
                
                
                //add secret Chat Timerrrrrr
                if ((dic["secretId"]  as? String) != "" ) && (status == "3") && ((dic["dTime"] as? Int) !=  0 ) {
                    
                    if let dTime = dic["dTime"] as? Int , let timeStamp = dic["timestamp"] as? String {
                        
                        let addTime:DispatchTimeInterval = .seconds(dTime)
                        let dispatchQue = DispatchQueue(label: timeStamp)
                        dispatchQue.asyncAfter(deadline: DispatchTime.now() + addTime , execute: {
                            print("helloooo frds chai pee lo \(addTime) message Time = \(dispatchQue.label)")
                            DispatchQueue.main.async {
                                //self.deletePerticularMessage(messageID: dispatchQue.label)
                            }
                            
                            
                        })
                        
                    }
                    
                }
                
            }
        }
        chatDta["messageArray"] = msgArray
        Couchbase.sharedInstance.updateData(data: chatDta, toDocID: chatDocID)
    }
    
    
    
    
    func deletePerticularMessageFromChat(messageID : String){
        _ = self.messages.filter({
            
            if  $0.messageId == messageID {
                let index =  self.messages.index(of: $0)
                print("im in Chat Controller delete this   index \(String(describing: index))")
                self.messages.remove(at: index!)
                self.collectionView?.reloadData()
            }
            return true
        })
    }

    func openAlert() {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        if  let receiverName = getReceiverName() {
            
            var alertTitle = String(format:StringConstants.viewProfile(),receiverName)
            
            alert.addAction(UIAlertAction(title: alertTitle, style: .default, handler: {(action:UIAlertAction!) in
                self.openReceiverProfile()
            }))
            
            if (self.favoriteObj?.isMatchedUser == 1) {
                alertTitle = String(format:StringConstants.unMatchProfile(),receiverName)
                
                alert.addAction(UIAlertAction(title: alertTitle, style: .default, handler: {(action:UIAlertAction!) in
                    self.showConfirmUnMatchAlert()
                }))
            }
            
            alertTitle = String(format:StringConstants.reportProfile(),receiverName)
            
            
            alert.addAction(UIAlertAction(title: alertTitle, style: .default, handler: {(action:UIAlertAction!) in
                
                if(self.matchesVm.reportReasonsArray.count == 0) {
                    self.requestForReportUserReasons(userClicked: true)
                } else {
                    self.showReportReasons()
                }
            }))
            
            if self.favoriteObj?.isUserBlocked == 1 {
                alertTitle = StringConstants.unBlock() + " " + receiverName
            }else if self.favoriteObj?.isUserBlocked == 0 {
                alertTitle = String(format:StringConstants.blockProfile(),receiverName)
            }
            
            alert.addAction(UIAlertAction(title: alertTitle, style: .default, handler: {(action:UIAlertAction!) in
                self.showConfirmBlockAlert()
            }))
            
            
            alert.addAction(UIAlertAction(title: StringConstants.actionSheetCancelText(), style: .cancel, handler: {(_) in }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    
    func requestToUnBlock(){
        let apiCall = MatchMakerAPICalls()
        var userId = ""
        if let userid = self.favoriteObj?.userId {
            userId = userid
        }
        let params = [likeProfileParams.targetUserId: userId]
        apiCall.requestToUnblock(requestData: params)
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        
        apiCall.requestToUnblock(requestData: params)
        apiCall.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                 NotificationCenter.default.post(name: NSNotification.Name(rawValue:"unmatchedUser"), object: nil)
                self.favoriteObj?.isUserBlocked = 0
                self.dateButtonOutlet.isEnabled = true
                self.attachBtn.isEnabled = true
                //self.moreBtn.isEnabled = true
                self.removeFooterView()
            }, onError: {error in
                Helper.hideProgressIndicator()
                
                self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: error.localizedDescription)
                Helper.hideProgressIndicator()
            })
            .disposed(by:disposebag)
        
    }
    
    
    /// to fetch list of all the report reasons.
    ///
    /// - Parameter selectedUser: selected profile to report.
    func showReportReasons() {
        
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = DeactivateAccountReasonsView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.isViewForReason = true
        videoView.loadDeactivateView()
        videoView.reportReasons = self.matchesVm.reportReasonsArray
        videoView.deactDelegate = self
        window.addSubview(videoView)
    }
    
    
    func showConfirmUnMatchAlert() {
        let alert = UIAlertController(title: nil, message: StringConstants.confirmUnmatch(), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: StringConstants.actionSheetCancelText(), style: .cancel, handler: {(_) in }))
        
        alert.addAction(UIAlertAction(title: StringConstants.unamtch(), style: .default, handler: {(action:UIAlertAction!) in
            self.unmatchUser()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /// method for unmatch user.
    ///
    /// - Parameter userDetails: contains profile details.
    func unmatchUser() {
        
        let params = [likeProfileParams.targetUserId:getReceiverID()!]
        
        Helper.showProgressIndicator(withMessage:StringConstants.unMatch())
        
        API.requestPOSTURL(serviceName: APINAMES.UnMatchProfile,
                           withStaticAccessToken:false,
                           params: params,
                           success: { (response) in
                            Helper.hideProgressIndicator()
                            if (response.null != nil){
                                return
                            }
                            
                            let statuscode = response.dictionaryObject!["code"] as! Int
                            switch (statuscode) {
                            case 200:
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue:"unmatchedUser"), object: nil)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                                    Helper.hideProgressIndicator()
                                    self.moveToBack()
                                    Helper.deleteUserChat(userDeatils:self.getReceiverID()!)
                                })
                        case 401:
                              Helper.changeRootVcInVaildToken()
                                break
                            default:
                                Helper.hideProgressIndicator()
                                self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: response.dictionaryObject![LoginResponse.errorMessage] as! String)
                            }
                            
        },
                           failure: { (Error) in
                            Helper.hideProgressIndicator()
                            self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: Error.localizedDescription)
                            Helper.hideProgressIndicator()
        })
    }
    
    /// method for requesting second look profiles.
    func requestForReportUserReasons(userClicked:Bool) {
        matchesVm.requestToGetReportReasons(userClicked:userClicked)
        matchesVm.unMatchSubjectResponse
            .subscribe(onNext: {response in
                if (userClicked) {
                    //show reasons popup
                    if(self.matchesVm.reportReasonsArray.count > 0) {
                        self.showReportReasons()
                    }
                }
            }, onError:{ error in
                
            })
            
            .disposed(by:disposebag)
    }
    
    /// method for blocking user
    ///
    /// - Parameter userProfile: selected user profile to block.
    func requestForBlockUser() {
        let params = [likeProfileParams.targetUserId:getReceiverID()!]
        Helper.showProgressIndicator(withMessage:StringConstants.blockUser())
        
        
        API.requestPatchURL(serviceName: APINAMES.blockProfile,
                            withStaticAccessToken:false,
                            params: params,
                            success: { (response) in
                                Helper.hideProgressIndicator()
                                if (response.null != nil){
                                    return
                                }
                                
                                let statuscode = response.dictionaryObject!["code"] as! Int
                                switch (statuscode) {
                                case 200:
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue:"unmatchedUser"), object: nil)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                                        Helper.hideProgressIndicator()
                                        self.moveToBack()
                                    })
                                    self.dateButtonOutlet.isEnabled = false
                                    self.attachBtn.isEnabled = false
                                  //  self.moreBtn.isEnabled = false
                                    self.favoriteObj?.isUserBlocked = 1 //temp
                                case 401:
                                            Helper.changeRootVcInVaildToken()
                                default:
                                    self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: response.dictionaryObject![LoginResponse.errorMessage] as! String)
                                }
                                
        },
                            failure: { (Error) in
                                
                                Helper.hideProgressIndicator()
                                
                                self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: Error.localizedDescription)
                                Helper.hideProgressIndicator()
        })
    }
    
    func moveToBack() {
        Helper.hidePushNotifications(hidePush: false)
        Helper.updateChatOpenStatus(isChatOpen:false)
        self.navigationController?.popViewController(animated: true)
    }
    
    func showConfirmBlockAlert() {
         var title = ""
         var msg = ""
        if self.favoriteObj?.isUserBlocked == 1 {
            title = StringConstants.unBlock()
            msg = StringConstants.confirmUnBlock()
        }else{
            
            title = StringConstants.block()
            msg = StringConstants.confirmBlock()
        }
        
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: StringConstants.actionSheetCancelText(), style: .cancel, handler: {(_) in }))
        
        alert.addAction(UIAlertAction(title: title, style: .default, handler: {(action:UIAlertAction!) in
            if self.favoriteObj?.isUserBlocked == 1 {
                self.showUnmatchCoinsPrompt(show:false)
               self.requestToUnBlock()
            }else{
                self.requestForBlockUser()
            }
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func unblockToSendMessage(){
        
        let title = "Unblock"
        var name = ""
        
        showUnmatchCoinsPrompt(show:false)
        
        if let receiverName = getReceiverName() {
            name = receiverName
        }
        let msg = "Unblock " + name + " to send message"
        
        
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: StringConstants.actionSheetCancelText(), style: .cancel, handler: {(_) in }))
        
        alert.addAction(UIAlertAction(title: title, style: .default, handler: {(action:UIAlertAction!) in
                self.requestToUnBlock()
           
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

class CustomTitleView: UIView {
    override var intrinsicContentSize: CGSize {
        return UILayoutFittingExpandedSize
    }
}

extension ChatViewController: deactivateReasonsDelegate {
    func userSelectedDoneButton(selectedReason: String) {
        print(selectedReason)
    }
    
    func userSelectedReasonForReport(selectedReason: ReportReason) {
        self.inputToolbar.isHidden = false
        self.reportUserForReason(reason: selectedReason.nameOfTheReason, userId:getReceiverID()!)
    }
    
    
    /// report user button action
    ///
    /// - Parameter userDetails: contains the profile details.
    func reportedUser() {
        Helper.showAlertWithMessage(withTitle: StringConstants.reportThanks(), message:StringConstants.reportFeedBack(), onViewController: self)
    }
    
    /// reporting user.
    ///
    /// - Parameters:
    ///   - reason: reson for reporting
    ///   - userId: user id to report
    func reportUserForReason(reason:String,userId:String) {
        let params = [likeProfileParams.targetUserId:userId,
                      likeProfileParams.message:"reporting",
                      likeProfileParams.reason:reason]
        
        
        
        API.requestPOSTURL(serviceName: APINAMES.reportUser,
                           withStaticAccessToken:false,
                           params: params,
                           success: { (response) in
                            Helper.hideProgressIndicator()
                            if (response.null != nil){
                                return
                            }
                            
                            let statuscode = response.dictionaryObject!["code"] as! Int
                            switch (statuscode) {
                            case 200:
                                self.reportedUser()
                            case 401:
                                        Helper.changeRootVcInVaildToken()
                                  
                                break
                            default:
                                self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: response.dictionaryObject![LoginResponse.errorMessage] as! String)
                            }
                            
        },
                           failure: { (Error) in
                            
                            Helper.hideProgressIndicator()
                            
                            self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: Error.localizedDescription)
                            Helper.hideProgressIndicator()
        })
    }
}

extension ChatViewController:spendSomeCoinPopUpDelegate {
    func coinBtnPressed(sender: UIButton) {
        let title = sender.title(for: .normal)
        if title == StringConstants.BuyCoins() {
            let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "addCoin") as! AddCoinsVC
            userBasics.navigationController?.setNavigationBarHidden(true, animated: false)
            self.navigationController?.pushViewController(userBasics, animated: true)
            return
        }
        
        
        sendMessage()
    }
}
