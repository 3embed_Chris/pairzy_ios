//
//  LocationPickerViewModel.swift
//  MQTT Chat Module
//
//  Created by Imma Web Pvt Ltd on 16/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import GooglePlacePicker
import GooglePlaces
import GoogleMaps



protocol locationPickDelegate: class {
    
    func didSendselectedlocation(address name:String ,location latLog:[String:Any])
    
}

class LocationPickerViewModel: NSObject {
    
    var chatObj : ChatViewController?
    weak var delegate:locationPickDelegate?
    
    
    init(_ chatOb : ChatViewController) {
        chatObj = chatOb
    }
    
    
    func openLocationPicker(){
        
        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != .denied {
    
        GMSServices.provideAPIKey(AppConstant.googleMapKey)
        GMSPlacesClient.provideAPIKey(AppConstant.googleMapKey)
        let confi = GMSPlacePickerConfig.init(viewport: nil)
        let placePicker = GMSPlacePickerViewController.init(config: confi)
        placePicker.delegate = self
        self.chatObj?.present(placePicker, animated: true, completion: nil)
        }else {
            let alert = UIAlertController.init(title: StringConstants.locationDesabled(), message: StringConstants.youNeedToEnable(), preferredStyle: .alert)
            let alertAction = UIAlertAction.init(title: StringConstants.ok(), style: .cancel, handler: nil)
            alert.addAction(alertAction)
            chatObj?.present(alert, animated: true, completion: nil)
        }
    }
}

extension LocationPickerViewModel: GMSPlacePickerViewControllerDelegate{
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        chatObj?.dismiss(animated: true, completion: nil)
        let dict:[String:Any] = ["lat":"\(place.coordinate.latitude)",
                                 "log":"\(place.coordinate.longitude)"]
        delegate?.didSendselectedlocation(address: place.name, location: dict)
    }
    
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didFailWithError error: Error) {
        print("error")
    }
    
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        chatObj?.dismiss(animated: true, completion: nil)
    }
    
    
}
