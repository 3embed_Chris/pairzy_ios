//
//  DocumentViewerViewController.swift
//  MQTT Chat Module
//
//  Created by Sachin Nautiyal on 05/03/2018.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import EVURLCache


class DocumentViewerViewController: UIViewController {
    
    @IBOutlet weak var webViewProgressIndicatorOutlet: UIActivityIndicatorView!
    @IBOutlet weak var webViewOutlet: UIWebView!
    var docMVMObj : DocumentMessageViewModal!
    var isComingfromSetting = false
    var webURL:String?
    var titleName:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isComingfromSetting == false {
            if let fileName = docMVMObj.getFileName() {
                self.title = fileName
            }
            if let fileURL = docMVMObj.getFileURL() {
                self.setupDoc(withURL: fileURL)
            }
        }else {
             self.setupDoc(withURL: webURL!)
            self.title = titleName
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    func setupDoc(withURL docURL: String) {
        if let url = URL(string: String(docURL)) {
            self.webViewProgressIndicatorOutlet.startAnimating()
            webViewOutlet.loadRequest(URLRequest(url: url))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openShareScreen(withData dataString: String) {
        let activityViewController = UIActivityViewController(activityItems: [dataString as NSString], applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func shareButtonAction(_ sender: UIBarButtonItem) {
        if let fileURL = docMVMObj.getFileURL() {
            self.openShareScreen(withData: fileURL)
        }
    }
}

extension DocumentViewerViewController : UIWebViewDelegate {
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let redirectURL = EVURLCache.shouldRedirect(request: request) {
            let r = URLRequest(url: redirectURL)
            webView.loadRequest(r)
            return false
        }
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.webViewProgressIndicatorOutlet.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.webViewProgressIndicatorOutlet.stopAnimating()
    }
}
