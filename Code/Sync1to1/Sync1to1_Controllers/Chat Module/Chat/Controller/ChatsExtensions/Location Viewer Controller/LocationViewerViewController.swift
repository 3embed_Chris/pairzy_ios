//
//  LocationViewerViewController.swift
//  MQTT Chat Module
//
//  Created by Sachin Nautiyal on 15/02/2018.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import MapKit

class LocationViewerViewController: UIViewController {
    
    struct Constants {
        static let annotationViewIdentifier = "mapAnnotation"
    }
    
    struct MapsConstant {
        static let googleMaps = StringConstants.openInGoogleMap()
        static let maps = StringConstants.openInMaps()
    }
    
    @IBOutlet weak var mapViewOutlet: MKMapView!
    @IBOutlet weak var segmentControlOutlet: UISegmentedControl!
    var currentLatLong : String!
    var zoomed = false
    var isFromDate = false
    var locationManager: CLLocationManager!
    var location : CLLocationCoordinate2D!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         if isFromDate {
            if (CLLocationManager.locationServicesEnabled())
            {
                locationManager = CLLocationManager()
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.requestAlwaysAuthorization()
                locationManager.startUpdatingLocation()
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    private func drawLatLong(fromString latLongString : String) {
        if let latStr = latLongString.slice(from: "(", to: ","), let longStr = latLongString.slice(from: ",", to: ")") {
            
            if let latDecimal = Double(latStr), let log = Double(longStr) {
                self.location = CLLocationCoordinate2D(latitude: latDecimal, longitude: log)
                if let address = self.getLocationName(fromString: latLongString) {
                    self.title = address
                    self.createGeoLocationFromAddress(address, locationCoordinates: location, mapView: self.mapViewOutlet)
                }
                let coordinateRegion = MKCoordinateRegionMakeWithDistance(location, 1000.0 * 1.0, 1000.0 * 1.0)
                self.mapViewOutlet.setRegion(coordinateRegion, animated: true)
            }
        }
    }
    
    func showDirection(){
        let location = Helper.getUserLocation()
        
        guard let latStr = currentLatLong.slice(from: "(", to: ",") else { return }
        guard let longStr = currentLatLong.slice(from: ",", to: ")") else { return }
        
        guard let currentLat = location["lat"] as? Double else { return }
        guard let currentLong =  location["long"] as? Double else { return }
        
        let request = MKDirectionsRequest()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: currentLat, longitude: currentLong), addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Double(latStr) ?? 0.0, longitude: Double(longStr) ?? 0.0), addressDictionary: nil))
        request.requestsAlternateRoutes = false
        request.transportType = .walking
        
        let directions = MKDirections(request: request)
        
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            for route in unwrappedResponse.routes {
                self.mapViewOutlet.add(route.polyline)
                self.mapViewOutlet.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
            }
        }
    }
    
    private func getLocationName(fromString latLongString : String) -> String? {
        if let locationName = latLongString.slice(from: "@@", to: "@@") {
            return locationName
        }
        return nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.drawLatLong(fromString: currentLatLong)

        if isFromDate {
            showDirection()
        }

    }
    
    @IBAction func segmentControlAction(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0: // For Standerd view
            self.mapViewOutlet.mapType = .standard
            
        case 1: // For Hybrid view
            self.mapViewOutlet.mapType = .hybrid
            
        case 2: // For Satelite view
            self.mapViewOutlet.mapType = .satellite
            
        default:
            break
        }
    }
    
    @IBAction func searchButtonAction(_ sender: UIBarButtonItem) { // Zooming to current location
        let regionRadius: CLLocationDistance = 1000.0
        if zoomed {
            let coordinateRegion = MKCoordinateRegionMakeWithDistance(location, regionRadius * 1.0, regionRadius * 1.0)
            self.mapViewOutlet.setRegion(coordinateRegion, animated: true)
            zoomed = false
        } else {
            let coordinateRegion = MKCoordinateRegionMakeWithDistance(location, regionRadius * 2.0, regionRadius * 2.0)
            self.mapViewOutlet.setRegion(coordinateRegion, animated: true)
            zoomed = true
        }
    }
    
    @IBAction func openMapAction(_ sender: UIBarButtonItem) {
        // Open and show coordinate
        var optionsArray = [MapsConstant.maps]
        if (UIApplication.shared.canOpenURL(URL(string:"https://maps.google.com")!)) {
            optionsArray.append(MapsConstant.googleMaps)
        }
        self.addPopUpOptions(withOptionsArray: optionsArray)
    }
    
    func openMaps() {
        let url = "http://maps.apple.com/maps?saddr=\(location.latitude),\(location.longitude)"
        UIApplication.shared.open(URL(string : url)!, options: [:], completionHandler: nil)
    }
    
    func openGoogleMaps() {
        UIApplication.shared.open(URL(string:
            "https://maps.google.com/?q=@\(location.latitude),\(location.longitude)")!, options: [:], completionHandler: nil)
    }
    
    private func addPopUpOptions(withOptionsArray optionsArray : [String] ) {
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for optionValue in optionsArray {
            let action = UIAlertAction(title: optionValue, style: .default, handler: { (action) in
                if action.title == MapsConstant.maps {
                    self.openMaps()
                    controller.dismiss(animated: true, completion: nil)
                } else if action.title == MapsConstant.googleMaps {
                    self.openGoogleMaps()
                    controller.dismiss(animated: true, completion: nil)
                } else {
                    // have to handle for Other Options here.
                }
            })
            controller.addAction(action)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:nil)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
}

extension LocationViewerViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView : MKAnnotationView!
        if let annotationview = mapViewOutlet.dequeueReusableAnnotationView(withIdentifier: Constants.annotationViewIdentifier) {
            annotationView = annotationview
        } else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: Constants.annotationViewIdentifier)
        }
        annotationView.annotation = annotation
        annotationView.image = #imageLiteral(resourceName: "map_pin_icon")
        return annotationView
    }
    
    
    func createGeoLocationFromAddress(_ address: String, locationCoordinates : CLLocationCoordinate2D,  mapView: MKMapView) {
        // Instantiate annotation
        let annotation = MKPointAnnotation()
        // Annotation coordinate
        annotation.coordinate = locationCoordinates
        annotation.title = address
        mapView.addAnnotation(annotation)
        mapView.showsPointsOfInterest = true
    }
    
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blue
            polylineRenderer.fillColor = UIColor.red
            polylineRenderer.lineWidth = 2
            return polylineRenderer
    
        }
}

extension LocationViewerViewController : CLLocationManagerDelegate {

    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let location = locations.last as! CLLocation
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))

        self.mapViewOutlet.setRegion(region, animated: true)
    }
}

