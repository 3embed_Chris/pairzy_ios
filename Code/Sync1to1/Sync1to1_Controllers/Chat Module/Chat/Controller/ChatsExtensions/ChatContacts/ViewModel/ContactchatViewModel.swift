//
//  ContactchatViewModel.swift
//  MQTT Chat Module
//
//  Created by Imma Web Pvt Ltd on 14/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import ContactsUI
import Contacts

protocol shareContactDelegate: class {
    
    func selectedContact(_ contact:CNContact)
    
}

class ContactchatViewModel:NSObject,CNContactPickerDelegate{

    var chatObj:ChatViewController?
   weak var delegate:shareContactDelegate?
    
     init(_ chatObject:ChatViewController) {
        chatObj = chatObject
    }
    
    func openContectView(){
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        chatObj?.present(contactPicker, animated: true, completion: nil)
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        chatObj?.dismiss(animated: true, completion: nil)
    }
   
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact){
        chatObj?.dismiss(animated: true, completion: nil)
        delegate?.selectedContact(contact)
    }
    
}
