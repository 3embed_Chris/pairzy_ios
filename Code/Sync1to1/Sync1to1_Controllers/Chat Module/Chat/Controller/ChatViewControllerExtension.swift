//
//  ChatViewControllerExtension.swift
//  MQTT Chat Module
//
//  Created by Sachin Nautiyal on 11/02/2018.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import TLPhotoPicker
import UIKit
import AVFoundation
import ContactsUI
import Contacts
import JSQMessagesViewController
import Photos
import MessageUI
import SafariServices
import MobileCoreServices

//MARK:- Image Cell Tapped Delegate
extension ChatViewController : ImageCellTappedDelegate {
    func imageCellTapped(withImageMVModal imageMVModalObect: ImageMessageViewModal) {
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
        self.openMediaController(withMsgObj: imageMVModalObect.message)
    }
}

//MARK:- Video Cell Tapped Delegate
extension ChatViewController : VideoCellTappedDelegate {
    func videoCellTapped(withVideoMVModal videoMVModalObect: VideoMessageViewModal) {
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
        self.openMediaController(withMsgObj: videoMVModalObect.message)
    }
}

// MARK: - Media Controller with Media
extension ChatViewController {
    /// Used for opening media controller with current media
    func openMediaController(withMsgObj msgObj : Message) {
        let showMediaViewCntroller = ShowMediaViewController(nibName: "ShowMediaViewController", bundle: nil)
        guard let msgs = self.chatsDocVMObject.getMediaMessages(withChatDocID: self.chatDocID) else { return }
        showMediaViewCntroller.messages = msgs
        showMediaViewCntroller.selectedMessageUrl = msgObj.getVideoFileName()
        self.present(showMediaViewCntroller, animated: false, completion: nil)
    }
}

// MARK: - Location Cell Delegate
extension ChatViewController : LocationCellDelegate {
    func tapOnLocationDelegate(messageObj: Message) {
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
        self.goToShowLocationViewController(message: messageObj)
    }
    
    func goToShowLocationViewController(message: Message) {
        if let messageType = message.messageType {
            if messageType == MessageTypes.location {
                if (message.messagePayload != nil) {
                    self.performSegue(withIdentifier: SegueIds.chatVCToshowLocation, sender: message.messagePayload)
                }
            }
        }
    }
}

//MARK:- Document Delegate
extension ChatViewController:documentDelegate {
    
    func didPickDocument(docUrl: URL) {
        self.sendDocMessage(withDocLocalURL: docUrl as NSURL, andMediaTypes: "9")
    }
    
    func mimeTypeForPath(path: String) -> String {
        let url = NSURL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
    
    func sendDocMessage(withDocLocalURL docURL: NSURL, andMediaTypes type: String) {
        if self.chatDocID != nil {
            UserDefaults.standard.setValue("", forKey:self.chatDocID)
        }
        // Creating image message with thumbnail Image.
        self.chatDocID = self.getChatDocID()
        let docData = NSData(contentsOf: docURL as URL)
        guard let docSize = docData?.length else { return }
        var docDict = [String : Any]()
        let fileTYpe = self.mimeTypeForPath(path: docURL.absoluteString!)
        docDict["fileName"]
            = docURL.lastPathComponent
        docDict["mimeType"] = fileTYpe
        docDict["extension"] = docURL.pathExtension
        guard let docMsgObj = self.getMediaMessageObj(withMediaURL: docURL, isSelf: true, withMediaType: type, withMessageData: docDict) else { return }
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        self.updateChatDocIDIntoContactDB()
        guard let msgObj = chatsDocVMObject.makeMessageForSendingBetweenServers(withData: nil, withMediaSize: Int(docSize), andMediaURL: docURL.absoluteString!, withtimeStamp: docMsgObj.timeStamp, andType: type, documentData: docDict, isReplying: self.isReplying, replyingMsgObj: self.replyingMessage, senderID: self.senderId(), receiverId: self.receiverID, chatDocId: self.chatDocID) else { return }
        
        self.messages.append(docMsgObj)
        if let chatDta = couchbaseObj.getData(fromDocID: chatDocID) {
            chatsDocVMObject.updateChatData(withData: chatDta, msgObject : docMsgObj as Any, inDocID  : chatDocID, isCreatingChat: false)
        }
        guard let MsgObjForDB = chatsDocVMObject.getMessageObject(fromData: msgObj, withStatus: "0", isSelf: true, fileSize: Double(docSize), documentData: docDict, isReplying: self.isReplying, replyingMsgObj: self.replyingMessage) else { return }
        self.chatsDocVMObject.updateChatDoc(withMsgObj: MsgObjForDB, toDocID: chatDocID)
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.finishSendingMessage(animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.removeReplyView()
            }
        }
    }
}

// MARK: - Media Message creation
extension ChatViewController {
    
    func getMediaMessageObj(withMediaURL mediaURL : NSURL, isSelf : Bool, withMediaType type: String, withMessageData messageData: [String : Any]?) -> Message? {
        guard let msgData = self.getMediaMessageParams(withMediaURL:  mediaURL, isSelf: isSelf, mediaURL: nil, withMediaType: type, isReplying: self.isReplying, replyingMsgObj: self.replyingMessage) else { return nil }
        let message = Message(forData: msgData, withDocID: self.chatDocID, andMessageobj: msgData, isSelfMessage: isSelf, mediaStates : .notUploaded, mediaURL : mediaURL.absoluteString!, thumbnailData: nil, secretID: nil,receiverIdentifier : "", messageData : messageData, isReplied: self.isReplying,gpMessageType: "")
        return message
    }
    
    func getMediaMessageParams(withMediaURL resourceURL : NSURL, isSelf : Bool, mediaURL : String?, withMediaType type: String, isReplying: Bool, replyingMsgObj: Message?) -> [String : Any]? {
        let timeStamp : String = "\(UInt64(floor(Date().timeIntervalSince1970 * 1000)))"
        var params = [String :Any]()
        let userDocVMObject = UsersDocumentViewModel(couchbase: couchbaseObj)
        guard let userData = userDocVMObject.getUserData() else { return nil }
        guard let mediaSize = NSData(contentsOf: resourceURL as URL)?.length else { return nil }
        if isSelf {
            params["from"] = Helper.getMQTTID()
            params["to"] = self.receiverID! as Any
        } else {
            params["from"] = self.receiverID! as Any
            params["to"] = Helper.getMQTTID()
        }
        if let url = mediaURL {
            params["payload"] = url as Any
            let encodedUrl = url.replace(target: "\n", withString: "")
            if let decodedUrl = encodedUrl.fromBase64() {
                params["mediaURL"] = decodedUrl as Any
            }
        }
        params["toDocId"] = self.chatDocID! as Any
        params["timestamp"] = timeStamp as Any
        params["id"] = timeStamp as Any
        params["type"] = type as Any
        params["dataSize"] = mediaSize as Any
        params["userImage"] = userData["userImageUrl"]! as Any
        params["name"] = userData["userName"]! as Any
        params["receiverIdentifier"] = userData["userName"]! as Any
        
        if isReplying == true, let replyMsg = replyingMsgObj {
            if let pPload = replyMsg.messagePayload, let pFrom = replyMsg.messageFromID, let prIdentifier = replyMsg.receiverIdentifier, let msgId = replyMsg.timeStamp, let previousType = replyMsg.messageType {
                params["previousPayload"] = pPload as Any
                params["previousFrom"] = pFrom as Any
                params["replyType"] = type as Any
                params["type"] = "10" as Any
                params["previousReceiverIdentifier"] = prIdentifier as Any
                params["previousId"] = msgId as Any
                params["previousType"] = "\(previousType.hashValue)" as Any
                if previousType == .replied {
                    if let pType = replyMsg.repliedMessage?.replyMessageType {
                        params["previousType"] = "\(pType.hashValue)" as Any
                    }
                }
                if previousType == .image || previousType == .doodle || previousType == .video {
                    if let tData = replyMsg.thumbnailData {
                        params["previousPayload"] = tData
                    }
                } else if previousType == .location {
                    params["previousPayload"] = "Location"
                }
                else if previousType == .replied {
                    if let repliedMsg = self.replyingMessage?.repliedMessage {
                        if repliedMsg.replyMessageType == .image || repliedMsg.replyMessageType == .doodle || repliedMsg.replyMessageType == .video {
                            if let tData = replyMsg.thumbnailData {
                                params["previousPayload"] = tData
                            }
                        } else if repliedMsg.replyMessageType == .location {
                            params["previousPayload"] = "Location"
                        }
                    }
                }
            }
        }
        return params
    }
}

//MARK:- VoiceRecorder delegate
//extension ChatViewController : SKRecordViewDelegate {
//    func SKRecordViewDidCancelRecord(_ sender: SKRecordView, button: UIView) {
//        sender.state = .none
//        sender.setupRecordButton(UIImage.init(named:"voiceRecorderImg")!)
//        recordingView.audioRecorder?.stop()
//        self.inputToolbar.contentView?.alpha = 1.0
//        recordingView.recordButton.imageView?.stopAnimating()
//    }
//
//    func SKRecordViewDidSelectRecord(_ sender: SKRecordView, button: UIView) {
//        if let player = self.currentPlayingAudioPlayer {
//            player.stop(clearQueue: true)
//        }
//        let cameraMediaType = AVMediaType.audio
//        do {
//            let audioSession = AVAudioSession.sharedInstance()
//             try? audioSession.setCategory(AVAudioSessionCategoryAmbient, with: AVAudioSessionCategoryOptions.mixWithOthers)
//          // try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
//        } catch let error {
//            print(error.localizedDescription)
//        }
//        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
//        switch cameraAuthorizationStatus {
//        case .denied:
//            self.showdefaultAlert(title: StringConstants.Oops(), message: AppConstant.audioPermissionMsg)
//            return
//        case .authorized: break
//        case .restricted: break
//        case .notDetermined:
//            AVCaptureDevice.requestAccess(for:AVMediaType.audio, completionHandler: { isGranted in
//                if !isGranted {
//                    OperationQueue.main.addOperation({
//                        self.showdefaultAlert(title: StringConstants.Oops(), message: AppConstant.audioPermissionMsg)
//                    })
//                    return
//                }
//            })
//        }
//        sender.state = .recording
//        sender.setupRecordButton(UIImage(named: "rec1")!)
//        sender.setupRecorder()
//        sender.audioRecorder?.record()
//        recordingView.recordButton.imageView?.startAnimating()
//        self.inputToolbar.contentView?.alpha = 0.0
//    }
//
//    func SKRecordViewDidStopRecord(_ sender: SKRecordView, button: UIView) {
//        self.inputToolbar.contentView?.alpha = 1.0
//        recordingView.audioRecorder?.stop()
//        sender.state = .none
//        sender.setupRecordButton(UIImage(named: "voiceRecorderImg")!)
//        //Get url from here
//        self.sendAudioMessage(withAudioLocalURL: recordingView.getFileURL() as NSURL, andMediaTypes: "5")
//    }
//}


// MARK:-  LocationPicker delegate
extension ChatViewController:locationPickDelegate {
    func didSendselectedlocation(address name: String, location latLog: [String : Any]) {
        if let lat = latLog["lat"] as? String, let log = latLog["log"] as? String {
            let msgStr = "(\(lat),\(log))@@\(name)@@ "
            sendMessageObjForLocation(withAddress : msgStr)
            if self.chatDocID != nil {UserDefaults.standard.setValue("", forKey:self.chatDocID)}
        }
    }
    
    private func sendMessageObjForLocation(withAddress address: String) {
        if self.chatDocID != nil {UserDefaults.standard.setValue("", forKey:self.chatDocID)}
        self.updateMessageUIAfterSendingMessage(withText: address, andType: "3", withData: nil)
    }
}

//MARK:- ShareConactPicker delegate
extension ChatViewController:shareContactDelegate,ShowContactScreenDelegate {
    func didSendCliked() {
        
    }
    
    
    func selectedContact(_ contact: CNContact) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let navigation = story.instantiateViewController(withIdentifier: "ShowContactScreenTableView") as? UINavigationController
        let shareView =   navigation?.topViewController as! ShowContactScreenTableViewController
        var numbers = [String]() , types = [String]()
        
        for kk in contact.phoneNumbers {
            let num = Helper.removeSpecialCharsFromString(text: kk.value.stringValue)
            let  type = kk.label!
            if num.count > 0 {
                numbers.append(num)
                types.append(CNLabeledValue<NSString>.localizedString(forLabel: type))
            }
        }
        shareView.name = contact.givenName + contact.familyName
        shareView.typeLable = types
        shareView.mobileNumber = numbers
        shareView.delegate = self
        contactobj = contact
        self.present(navigation!, animated: true, completion: nil)
    }
}

//MARK:- ImagePicker delegate
extension ChatViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let mediaType = info["UIImagePickerControllerMediaType"] as? String else { return }
        if(mediaType == "public.image") {
            guard let imageToSend = info[UIImagePickerControllerOriginalImage] as? UIImage else { return }
            let name  = arc4random_uniform(900000) + 100000;
            let timeStamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))
            AFWrapper.updloadPhoto(withPhoto: imageToSend, andName: "\(name)\(timeStamp)", success: {_ in }, failure: {_ in })
            self.dismiss(animated: true, completion: nil)
            
            let story = UIStoryboard.init(name: "Chat", bundle: nil)
            let preview =  story.instantiateViewController(withIdentifier: "ImagePickerPreview") as? ImagePickepreviewController
            preview?.imageArray = [imageToSend]
            preview?.delegate = self
            preview?.isComingFromCamera = true
            self.present(preview!, animated: true, completion: nil)
        }
        else {
            guard  let videoURL = info["UIImagePickerControllerMediaURL"] as? NSURL else { return }
            self.sendVideoMessage(withVideoLocalURL: videoURL, andMediaTypes: "2")
            if self.chatDocID != nil {UserDefaults.standard.setValue("", forKey:self.chatDocID)}
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension ChatViewController:ImagepreviewDelegate {
    func didCancelCliked() {
        print("cancel")
    }
    
    private func uploadMedia(withMedia media: Any) {
        
        if let mediaObj = media as? TLPHAsset {
            if mediaObj.type == .photo || mediaObj.type == .livePhoto {
                if let currentImage = mediaObj.fullResolutionImage {
                    self.addImageMessage(withImage: currentImage, andMediaType: "1")
                }
            } else if mediaObj.type == .video {
                self.getVideoPath(withPhAsset: mediaObj.phAsset!, completion: { (videoLocalURL) in
                    let path = NSTemporaryDirectory() + UUID().uuidString + ".mov"
                    let outputURL = URL.init(fileURLWithPath: path)
                    self.compressVideo(inputURL: videoLocalURL! as NSURL, outputURL: outputURL as NSURL, handler: { (handler) in
                        if handler.status == AVAssetExportSessionStatus.completed {
                            self.sendVideoMessage(withVideoLocalURL: outputURL as NSURL, andMediaTypes: "2")
                        } else if handler.status == AVAssetExportSessionStatus.failed {
                            print("Error on compression")
                        }
                    })
                })
            }
            
        } else if let imageObj = media as? UIImage {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.addImageMessage(withImage: imageObj, andMediaType: "1")
            }
        }
    }
    
    func didSendCliked(medias: [Any]) {
        if self.chatDocID != nil {UserDefaults.standard.setValue("", forKey:self.chatDocID)}
        for (index, media) in medias.enumerated() {
            if index == 0 {
                self.uploadMedia(withMedia : media)
            }
            else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    self.uploadMedia(withMedia : media)
                }
            }
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    private func compressVideo(inputURL: NSURL, outputURL: NSURL, handler: @escaping (AVAssetExportSession)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL as URL, options: nil)
        let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality)
        exportSession?.outputURL = outputURL as URL
        exportSession?.outputFileType = kUTTypeQuickTimeMovie as AVFileType
        exportSession?.shouldOptimizeForNetworkUse = true
        exportSession?.exportAsynchronously { () -> Void in
            handler(exportSession!)
        }
    }
    
    private func getVideoPath(withPhAsset mPhasset : PHAsset, completion : @escaping ((URL?) -> Void)) {
        let options: PHVideoRequestOptions = PHVideoRequestOptions()
        options.version = .original
        PHImageManager.default().requestAVAsset(forVideo: mPhasset, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
            if let urlAsset = asset as? AVURLAsset {
                let localVideoUrl = urlAsset.url
                completion(localVideoUrl)
            } else {
                completion(nil)
            }
        })
    }
    
    // Used for getting thumbnail image from encoded string
    func createImageMessage(withThumbnail thumbnail: Any) -> UIImage? {
        if let thumbnailImage = thumbnail as? UIImage {
            return thumbnailImage
        }
        else if let thumbnailData = thumbnail as? String {
            let image = Image.convertBase64ToImage(base64String: thumbnailData)
            return image
        }
        return nil
    }
    
    func createThumbnail(forImage image : UIImage) -> String? {
        // Define thumbnail size
        let size = CGSize(width: 70, height: 70)
        // Define rect for thumbnail
        let scale = max(size.width/image.size.width, size.height/image.size.height)
        let width = image.size.width * scale
        let height = image.size.height * scale
        let x = (size.width - width) / CGFloat(2)
        let y = (size.height - height) / CGFloat(2)
        let thumbnailRect = CGRect(x: x, y: y, width: width, height: height)
        
        // Generate thumbnail from image
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        image.draw(in: thumbnailRect)
        guard let thumbnail = UIGraphicsGetImageFromCurrentImageContext() else { return  nil }
        UIGraphicsEndImageContext()
        let imageData = Image.convertImageToBase64(image: thumbnail)
        return imageData
    }
    
    func addImageMessage(withImage tImage: UIImage, andMediaType type: String) {
        // Creating image message with thumbnail Image.
        self.chatDocID = self.getChatDocID()
        
        let img = self.createImageMessage(withThumbnail: tImage)
        guard let imageData = self.createThumbnail(forImage: tImage) else { return }
        let imgData: NSData = NSData(data: UIImageJPEGRepresentation((tImage), 0.5)!)
        let imageSize = imgData.length
        guard let imageMsgObj = self.getimageMessageObj(withimage: img!, isSelf: true, thumbnailData: imageData, withMediaType: type) else { return }
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        self.updateChatDocIDIntoContactDB()
        guard let msgObj = chatsDocVMObject.makeMessageForSendingBetweenServers(withData: imageData, withMediaSize: imageSize, andMediaURL: "", withtimeStamp: imageMsgObj.timeStamp, andType : type, documentData : nil, isReplying: self.isReplying, replyingMsgObj: self.replyingMessage, senderID: self.senderId(), receiverId: self.receiverID, chatDocId: self.chatDocID) else { return }
        
        self.messages.append(imageMsgObj)
        if let chatDta = couchbaseObj.getData(fromDocID: chatDocID) {
            chatsDocVMObject.updateChatData(withData: chatDta, msgObject : imageMsgObj as Any, inDocID  : chatDocID, isCreatingChat: false)
        }
        //Store message to DB.
        guard let MsgObjForDB = chatsDocVMObject.getMessageObject(fromData: msgObj, withStatus: "0", isSelf: true, fileSize: Double(imageSize), documentData: nil, isReplying: self.isReplying, replyingMsgObj: self.replyingMessage) else { return }
        self.chatsDocVMObject.updateChatDoc(withMsgObj: MsgObjForDB, toDocID: chatDocID)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.finishSendingMessage(animated: true)
            //self.scrollToBottom(animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.removeReplyView()
            }
        }
    }
}

// MARK: - Video Player Extension

extension ChatViewController {
    
    func thumbnail(sourceURL:NSURL) -> UIImage {
        let asset = AVAsset(url: sourceURL as URL)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        let time = CMTime(seconds: 1, preferredTimescale: 1)
        do {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            let image = UIImage(cgImage: imageRef)
            let imgData = NSData(data: UIImageJPEGRepresentation((image), 0.2)!)
            let compressedImage = UIImage(data: imgData as Data)
            print(imgData.length/1024)
            return compressedImage!
        } catch {
            return #imageLiteral(resourceName: "play")
        }
    }
}

extension ChatViewController {
    
    func sendVideoMessage(withVideoLocalURL videoURL: NSURL, andMediaTypes type: String) {
        if self.chatDocID != nil {UserDefaults.standard.setValue("", forKey:self.chatDocID)}
        // Creating image message with thumbnail Image.
        DispatchQueue.main.async {
            guard let docID = self.getChatDocID() else { return }
            self.chatDocID = docID
            DispatchQueue.global(qos: .default).async {
                let thumbNailImage = self.thumbnail(sourceURL: videoURL)
                guard let thumbnailData = self.createThumbnail(forImage: thumbNailImage) else { return }
                guard let videoSize = self.getVideoSize(withURL: videoURL) else { return }
                guard let videoMsgObj = self.getVideoMessageObj(withVideoURL: videoURL, isSelf: true, thumbnailData: thumbnailData, withMediaType: type) else { return }
                let chatsDocVMObject = ChatsDocumentViewModel(couchbase: self.couchbaseObj)
                self.updateChatDocIDIntoContactDB()
                guard let msgObj = chatsDocVMObject.makeMessageForSendingBetweenServers(withData: thumbnailData, withMediaSize: Int(videoSize), andMediaURL: "", withtimeStamp: videoMsgObj.timeStamp, andType: type, documentData : nil, isReplying: self.isReplying, replyingMsgObj: self.replyingMessage, senderID: self.senderId(), receiverId: self.receiverID, chatDocId: self.chatDocID) else { return }
                
                self.messages.append(videoMsgObj)
                if let chatDta = self.couchbaseObj.getData(fromDocID: self.chatDocID) {
                    chatsDocVMObject.updateChatData(withData: chatDta, msgObject : videoMsgObj as Any, inDocID  : self.chatDocID, isCreatingChat: false)
                }
                
                guard let MsgObjForDB = chatsDocVMObject.getMessageObject(fromData: msgObj, withStatus: "0", isSelf: true, fileSize: Double(videoSize), documentData: nil, isReplying: self.isReplying, replyingMsgObj: self.replyingMessage) else { return }
                self.chatsDocVMObject.updateChatDoc(withMsgObj: MsgObjForDB, toDocID: self.chatDocID)
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self.finishSendingMessage(animated: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        self.removeReplyView()
                    }
                }
            }
        }
    }
    
    func getVideoSize(withURL resourceURL: NSURL) -> UInt64? {
        let asset = AVURLAsset(url: resourceURL as URL)
        print(asset.fileSize ?? 0)
        return UInt64(asset.fileSize!)
    }
    
    func getVideoMessageObj(withVideoURL videoURL : NSURL, isSelf : Bool, thumbnailData: String?, withMediaType type: String) -> Message? {
        guard let msgData = self.getVideoMessageParams(withVideoURL: videoURL, isSelf: isSelf, videoURL: nil, withMediaType: type, isReplying: self.isReplying, replyingMsgObj: self.replyingMessage) else { return nil }
        
        //        creating image cache URL here.
        var timeStamp: String = ""
        if let ts = msgData["timestamp"] as? String {
            timeStamp = ts
        } else if let ts = msgData["timestamp"] as? Int64 {
            timeStamp = "\(ts)"
        }
        
        //1. Store Video To Cache here.
        //Storing image into cache with the name of the image.
        let videoName = "hola"+timeStamp+".mp4"
        var mediaURL = videoName
        do {
            let videoData = try Data(contentsOf: videoURL as URL)
            let videoURL = MediaDownloader().SaveMedia(withURL: videoName, andData: videoData)
            if let url = videoURL?.absoluteString {
                mediaURL = url
            }
        } catch let error {
            print(error)
        }
        
        //        2. After that link the media url to the message db.
        if let mURL = msgData["mediaURL"] as? String {
            mediaURL = mURL
        }
        
        var gpMessageType = ""
        if let gpMessageTyp = msgData["gpMessageType"] as? String{
            gpMessageType = gpMessageTyp
        }
        
        let message = Message(forData: msgData, withDocID: self.chatDocID!, andMessageobj: msgData, isSelfMessage: isSelf, mediaStates: .notUploaded, mediaURL: mediaURL, thumbnailData: thumbnailData, secretID: nil, receiverIdentifier: "", messageData: nil, isReplied: self.isReplying ,gpMessageType :gpMessageType)
        return message
    }
    
    func getVideoMessageParams(withVideoURL resourceURL : NSURL, isSelf : Bool, videoURL : String?, withMediaType type: String, isReplying: Bool, replyingMsgObj: Message?) -> [String : Any]? {
        let timeStamp : String = "\(UInt64(floor(Date().timeIntervalSince1970 * 1000)))"
        var params = [String :Any]()
        let userDocVMObject = UsersDocumentViewModel(couchbase: couchbaseObj)
        guard let userData = userDocVMObject.getUserData() else { return nil }
        let tImage = self.thumbnail(sourceURL: resourceURL)
        guard let imageData = self.createThumbnail(forImage: tImage) else { return nil }
        guard let VideoSize = self.getVideoSize(withURL: resourceURL) else { return nil }
        if isSelf {
            params["from"] = Helper.getMQTTID()
            params["to"] = self.receiverID! as Any
        } else {
            params["from"] = self.receiverID! as Any
            params["to"] = Helper.getMQTTID()
        }
        if let url = videoURL {
            params["payload"] = url as Any
        }
        params["toDocId"] = self.chatDocID! as Any
        params["timestamp"] = timeStamp as Any
        params["id"] = timeStamp as Any
        params["type"] = type as Any
        params["thumbnail"] = imageData
        params["dataSize"] = VideoSize as Any
        params["userImage"] = userData["userImageUrl"]! as Any
        params["name"] = userData["userName"]! as Any
        params["receiverIdentifier"] = self.receiverID! as Any       //userData["userName"]! as Any
        
        if isReplying == true, let replyMsg = replyingMsgObj {
            if let pPload = replyMsg.messagePayload, let pFrom = replyMsg.messageFromID, let prIdentifier = replyMsg.receiverIdentifier, let msgId = replyMsg.timeStamp, let previousType = replyMsg.messageType {
                params["previousPayload"] = pPload as Any
                params["previousFrom"] = pFrom as Any
                params["replyType"] = type as Any
                params["type"] = "10" as Any
                params["previousReceiverIdentifier"] = prIdentifier as Any
                params["previousId"] = msgId as Any
                params["previousType"] = "\(previousType.hashValue)" as Any
                if previousType == .replied {
                    if let pType = replyMsg.repliedMessage?.replyMessageType {
                        params["previousType"] = "\(pType.hashValue)" as Any
                    }
                }
                if previousType == .image || previousType == .doodle || previousType == .video {
                    if let tData = replyMsg.thumbnailData {
                        params["previousPayload"] = tData
                    }
                } else if previousType == .location {
                    params["previousPayload"] = "Location"
                }
                else if previousType == .replied {
                    if let repliedMsg = self.replyingMessage?.repliedMessage {
                        if repliedMsg.replyMessageType == .image || repliedMsg.replyMessageType == .doodle || repliedMsg.replyMessageType == .video {
                            if let tData = replyMsg.thumbnailData {
                                params["previousPayload"] = tData
                            }
                        } else if repliedMsg.replyMessageType == .location {
                            params["previousPayload"] = "Location"
                        }
                    }
                }
            }
        }
        return params
    }
}

extension ChatViewController {
    func sendAudioMessage(withAudioLocalURL audioURL: NSURL, andMediaTypes type: String) {
        if self.chatDocID != nil {UserDefaults.standard.setValue("", forKey:self.chatDocID)}
        // Creating image message with thumbnail Image.
        let audioData = NSData(contentsOf: audioURL as URL)
        guard let audioSize = audioData?.length else { return }        
        guard let audioMsgObj = self.getMediaMessageObj(withMediaURL: audioURL, isSelf: true, withMediaType: type, withMessageData: nil) else { return }
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        self.updateChatDocIDIntoContactDB()
        guard let msgObj = chatsDocVMObject.makeMessageForSendingBetweenServers(withData: nil, withMediaSize: Int(audioSize), andMediaURL: audioURL.absoluteString!, withtimeStamp: audioMsgObj.timeStamp, andType: type, documentData : nil, isReplying: self.isReplying, replyingMsgObj: self.replyingMessage, senderID: self.senderId(), receiverId: self.receiverID, chatDocId: self.chatDocID) else { return }
        
        if let chatDta = couchbaseObj.getData(fromDocID: chatDocID) {
            chatsDocVMObject.updateChatData(withData: chatDta, msgObject : audioMsgObj as Any, inDocID  : chatDocID, isCreatingChat: false)
        }
        
        self.messages.append(audioMsgObj)
        
        guard let MsgObjForDB = chatsDocVMObject.getMessageObject(fromData: msgObj, withStatus: "0", isSelf: true, fileSize: Double(audioSize), documentData: nil, isReplying: self.isReplying, replyingMsgObj: self.replyingMessage) else { return }
        
        self.chatsDocVMObject.updateChatDoc(withMsgObj: MsgObjForDB, toDocID: chatDocID)
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.finishSendingMessage(animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.removeReplyView()
            }
        }
    }
}

//MARK:- Doodle delegate
extension ChatViewController : doodleDelegate {
    func didDoodleSendCliked(_ image: UIImage) {
        if self.chatDocID != nil {UserDefaults.standard.setValue("", forKey:self.chatDocID)}
        self.addImageMessage(withImage: image, andMediaType: "7")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.scrollToBottom(animated: true)
            
        }
    }
}

//MARK:- giphyDelegate  delegate
extension ChatViewController:giphyDelegate {
    func didSendStickerCliked(_ url: URL) {
        if self.chatDocID != nil {UserDefaults.standard.setValue("", forKey:self.chatDocID)}
        self.updateMessageUIAfterSendingMessage(withText: url.absoluteString, andType: "6", withData: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.finishSendingMessage(animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.removeReplyView()
            }
        }
    }
    
    func didSendGiphyCliked(_ url: URL) {
        if self.chatDocID != nil {UserDefaults.standard.setValue("", forKey:self.chatDocID)}
        self.updateMessageUIAfterSendingMessage(withText: url.absoluteString, andType: "8", withData: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.finishSendingMessage(animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.removeReplyView()
            }
        }
    }
}


// MARK: - Message View controller delegate
extension ChatViewController : MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sendTextMessage(withPhNumbers phNumbers : [String]) {
        let messageVC = MFMessageComposeViewController()
        if (messageVC != nil) {
            messageVC.body = StringConstants.messanger()
            messageVC.recipients = phNumbers
            
            messageVC.messageComposeDelegate = self
            self.present(messageVC, animated: true, completion: nil)
        }
    }
}

// MARK: - Contact Message Action Delegate
extension ChatViewController : ContactMessageActionDelegate {
    
    func openContactDetail(withMsgVMObj contactVMObj : ContactMessageViewModal) {
        self.performSegue(withIdentifier: SegueIds.chatVCTotoContactDetails, sender: contactVMObj)
    }
    
    func sendMessageToUser(withUserIds userIDs: [String]) {
        for (index, userId) in userIDs.enumerated() {
            if index == 0 {
                self.popToChatListController(withuserID : userId)
            }
        }
    }
    
    func popToChatListController(withuserID userID: String) {
//        if let controllers = self.navigationController?.childViewControllers, let navController = self.navigationController {
//            for controller in controllers {
//                if let chatListController = controller as? ChatsListViewController {
//                    navController.popToViewController(controller, animated: false)
//                    chatListController.performSegue(withIdentifier: AppConstants.Segue.chatController, sender: userID)
//                }
//            }
//        }
    }
    
    func inviteToAppAction(withPhNumbers phNumbers: [String]) {
        self.sendTextMessage(withPhNumbers: phNumbers)
    }
}

// MARK: - Contact present controller Delegate
extension ChatViewController : PresentControllerDelegate {
    func presentController(withController controller: UIViewController) {
        self.present(controller, animated: true, completion: nil)
    }
    
    func dismissController() {
        self.dismiss(animated: true, completion: nil)
    }
}


// MARK: - Document present controller delegate
extension ChatViewController : DocumentMessageDelegate {
    func openDocumentDelegate(withDocumentVMObj docMVMObj: DocumentMessageViewModal) {
        self.performSegue(withIdentifier: SegueIds.chatVCTodocumentViewer, sender: docMVMObj)
    }
}


// MARK: - Audio Player Cell Delegate
extension ChatViewController : AudioPlayerDelegate {
    func playing(withInstance audioPlayerInstance: AudioPlayerManager) {
        if let player = self.currentPlayingAudioPlayer {
            player.stop(clearQueue: true)
        }
        self.currentPlayingAudioPlayer = audioPlayerInstance
    }
}

// MARK: - Reply View Methods
extension ChatViewController {
    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        let msgObj = self.messages[indexPath.row]
        if action.description == "sn_handlePanGesture:" {
            self.collectionView?.reloadItems(at: [indexPath])
            self.addViewOnTopOfKeyboard(withMsgObj: msgObj)
        } else if action.description == "sn_reloadCell:" {
            self.collectionView?.reloadItems(at: [indexPath])
        } else if action.description == "delete:" {
            var deleteOptions = [StringConstants.deleteForMe()]
            if msgObj.isSelfMessage {
                if msgObj.messageType != .deleted {
                    deleteOptions = [StringConstants.deleteForEveryone(), StringConstants.deleteForMe()]
                }
            }
            self.opencontrollerForOptions(withOptions: deleteOptions, withIndexPath: indexPath)
        }
    }
}

// MARK: - Message Delete Methods
extension ChatViewController {
    private func deleteMessageLocally(withIndex indexPath: IndexPath) {
        if let cllectionView = collectionView {
            self.deleteMessage(winthIndex: indexPath)
            self.messages.remove(at: indexPath.row)
            cllectionView.deleteItems(at: [indexPath])
            cllectionView.collectionViewLayout.invalidateLayout()
        }
    }
    
    private func deleteMessage(winthIndex indexPath: IndexPath) {
        let msgObj = self.messages[indexPath.row]
        self.chatViewModelObj?.deleteMessage(withMessageObj: msgObj)
    }
    
    private func deleteMessageEveryone(withIndexPath indexPath  : IndexPath) {
        let msgObj = self.messages[indexPath.row]
        if let msg = self.chatViewModelObj?.updateMessageToDeletedState(withMsgObj: msgObj) {
            self.messages[indexPath.row] = msg
            self.sendReceiver(deletedMsg: msg)
        }
        collectionView?.performBatchUpdates({ () -> Void in
            let ctx = JSQMessagesCollectionViewFlowLayoutInvalidationContext()
            ctx.invalidateFlowLayoutDelegateMetrics = true
            self.collectionView?.collectionViewLayout.invalidateLayout(with: ctx)
        }) { (_: Bool) -> Void in
        }
        self.collectionView?.reloadItems(at: [indexPath])
    }
    
    private func sendReceiver(deletedMsg : Message) {
        var params = [String : Any]()
        let timeStamp : String = "\(UInt64(floor(Date().timeIntervalSince1970 * 1000)))"
        let userDocVMObject = UsersDocumentViewModel(couchbase: couchbaseObj)
        guard let userData = userDocVMObject.getUserData() else { return }
        
        params["from"] = self.senderId() as Any
        params["to"] = self.receiverID! as Any
        params["toDocId"] = self.chatDocID! as Any
        params["timestamp"] = deletedMsg.timeStamp! as Any
        params["id"] = deletedMsg.timeStamp! as Any
        params["userImage"] = userData["userImageUrl"]! as Any
        params["name"] = userData["userName"]! as Any
        params["receiverIdentifier"] = userData["userName"]! as Any
        params["removedAt"] = timeStamp
        params["payload"] = "This message has been removed".toBase64()
        params["type"] = "11"
        
        mqttChatManager.sendMessage(toChannel: "\(self.receiverID!)", withMessage: params, withQOS: .atLeastOnce)
    }
    
    fileprivate func opencontrollerForOptions(withOptions options: [String], withIndexPath indexPath: IndexPath ) {
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for optionValue in options {
            let action = UIAlertAction(title: optionValue, style: .destructive, handler: { (action) in
                if action.title == StringConstants.deleteForEveryone() {
                    self.deleteMessageEveryone(withIndexPath : indexPath)
                    controller.dismiss(animated: true, completion: nil)
                    
                } else if action.title == StringConstants.deleteForMe() {
                    self.deleteMessageLocally(withIndex: indexPath)
                    controller.dismiss(animated: true, completion: nil)
                }
            })
            controller.addAction(action)
        }
        let cancelAction = UIAlertAction(title: StringConstants.cancel(), style: .cancel, handler:nil)
        controller.addAction(cancelAction)
        self.presentController(withController: controller)
    }
}

extension ChatViewController : ReplyViewDismissDelegate {
    
    func replyViewClosedButtonSelected(_ replyView: UIView) {
        removeReplyView()
    }
    
    func replyMessageSelected(withMessageId msgId: String) {
        if let filteredMsg = self.messages.first(where: {
            $0.timeStamp == msgId || $0.messageId == msgId
        }) {
            if let index = self.messages.index(of: filteredMsg) {
                let indexPath = IndexPath(item: index, section: 0)
                self.collectionView?.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
            }
        }
    }
    
    func addViewOnTopOfKeyboard(withMsgObj msgObj : Message) {
        removeReplyView()
        if let replyView = Bundle.main.loadNibNamed("ReplyView", owner: self, options: nil)?.first as? ReplyView {
            self.isReplying = true
            replyView.replyViewDismissDelegate = self
            replyView.frame = self.changedFrame
            replyView.isReplyingView = true
            replyView.selectedMessage = msgObj
            self.replyingMessage = msgObj
            self.view.addSubview(replyView)
            self.view.bringSubview(toFront: replyView)
            self.scrollViewForReplyView()
        }
    }
    
    private func scrollViewForReplyView(){
        if var offset = self.collectionView?.contentOffset {
            if isReplying {
                offset.y += 60
                self.collectionView?.setContentOffset(offset, animated: true)
            } else {
                offset.y -= 60
                self.collectionView?.setContentOffset(offset, animated: true)
            }
        }
    }
    
    func updateReplyViewFrame() {
        if let replyView = self.view.viewWithTag(100) {
            if self.changedFrame.origin.y == self.view.frame.size.height {
                self.changedFrame.origin.y -= 100
            }
            replyView.frame = self.changedFrame
        }
    }
    
    func removeReplyView() {
        if let replyView = self.view.viewWithTag(100) {
            replyView.removeFromSuperview()
            self.isReplying = false
            self.replyingMessage = nil
            self.scrollViewForReplyView()
        }
    }
}

extension ChatViewController : UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let pGesture = gestureRecognizer as? UIPanGestureRecognizer {
            let velocity = pGesture.velocity(in: pGesture.view)
            if velocity.x>0 {
                return true
            }
        }
        return false
    }
}

extension UICollectionView {
    var centerPoint : CGPoint {
        get {
            return CGPoint(x: self.center.x + self.contentOffset.x, y: self.center.y + self.contentOffset.y);
        }
    }
}

extension ChatViewController : SNMessageCollectionViewCellDelegate {
    
    func messageAccesoryTapped(_ cell: JSQMessagesCollectionViewCell) {
        self.tappedOnForward(cell)
    }
    
    private func tappedOnForward(_ cell: JSQMessagesCollectionViewCell) {
        if let indexPath = collectionView?.indexPathForItem(at: cell.center) {
            let msg = self.messages[indexPath.row]
            self.addpartiCliked(withMsgObj : msg)
        }
    }
    
    private func addpartiCliked(withMsgObj msgObj : Message){
//        let favDatabase:[Contacts] =  Helper.getFavoriteDataFromDatabase1()
//        let story = UIStoryboard.init(name:"Main", bundle: nil)
//        let controller = story.instantiateViewController(withIdentifier: "SelectGroupMemNav") as! UINavigationController
//        let selectGpmem = controller.topViewController as! SelectGroupMemTableViewController
//        selectGpmem.allFavoriteList =  favDatabase
//        selectGpmem.isGroupMemberPicker = false
//        selectGpmem.message = msgObj
//        self.present(controller, animated: true, completion: nil)
    }
}



//MARK: - All Seceret Chat methods

extension ChatViewController{
    
    //ActionSheet
    func seceretChatTimerSheet(){
        
        let actionsheet = UIAlertController.init(title: "\n\n\n\n\n\n\n\n\n", message: nil, preferredStyle: .actionSheet)
        let margin:CGFloat = 10.0
        let rect = CGRect(x: margin, y: margin, width: actionsheet.view.bounds.size.width - margin * 4.0, height: 170)
        
        let cancelAction2 = UIAlertAction.init(title: StringConstants.done(), style: .default) { (action) in
            
            //selected Timer Time in Sec
            UserDefaults.standard.set(Helper.timeInsecArr()[self.selectedTimerIndex], forKey: (self.chatViewModelObj?.secretID)!)
            
            let chatsDocVMObject = ChatsDocumentViewModel(couchbase: self.couchbaseObj)
            self.updateChatDocIDIntoContactDB()
            guard var message = chatsDocVMObject.makeMessageForSendingBetweenServers(withText: "secret msg Tag", andType: "0", isReplying: self.isReplying, replyingMsgObj: self.replyingMessage, senderID: self.senderId(), receiverId: self.receiverID, chatDocId: self.chatDocID) else { return }
            
            if self.chatViewModelObj?.secretID != "" && self.chatViewModelObj != nil{
                message["secretId"] = self.chatViewModelObj?.secretID
                message["dTime"] = Helper.timeInsecArr()[self.selectedTimerIndex]
                message["gpMessageType"] = "7"
                message["payload"] = ""
                self.mqttChatManager.sendMessage(toChannel: "\(self.receiverID!)", withMessage: message, withQOS: .atLeastOnce)
            }
               message["payload"] = "\(message["receiverIdentifier"] as! String)".toBase64()
            
            
            guard let MsgObjForDB = chatsDocVMObject.getMessageObject(fromData: message , withStatus: "0", isSelf: true, fileSize: 0, documentData: nil, isReplying: self.isReplying, replyingMsgObj: self.replyingMessage) else { return }
            var msgObj = MsgObjForDB
            guard let dateString = DateHelper().getDateString(fromDate: Date()) else { return }
            msgObj["sentDate"] = dateString as Any
            msgObj["gpMessageType"] = "7"
            guard let chatDocID = self.chatDocID else { return }
            if let chatDta = self.couchbaseObj.getData(fromDocID: chatDocID) {
                chatsDocVMObject.updateChatData(withData: chatDta, msgObject : msgObj as Any, inDocID  : chatDocID, isCreatingChat: false)
            }
            
            self.chatsDocVMObject.updateChatDoc(withMsgObj: msgObj, toDocID: chatDocID)
            let msgObject = Message(forData: msgObj, withDocID: chatDocID, andMessageobj: message, isSelfMessage: true, mediaStates: .notApplicable, mediaURL: nil, thumbnailData: nil, secretID: self.chatViewModelObj?.secretID, receiverIdentifier: "", messageData: nil, isReplied: self.isReplying,gpMessageType: "7" ,dTime: Helper.timeInsecArr()[self.selectedTimerIndex])
            self.messages.append(msgObject)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.finishSendingMessage(animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    self.removeReplyView()
                }
            }
            actionsheet.dismiss(animated: true, completion: nil)
        }
        
        
        let cancelAction1 = UIAlertAction.init(title: StringConstants.cancel(), style: .cancel) { (action) in }
        let view =  UIView(frame: rect)
        actionsheet.view.addSubview(view)
        
        let picker = UIPickerView.init(frame: CGRect.init(x: 0, y: 0, width: rect.width, height: rect.height))
        picker.delegate  = self
        picker.dataSource  = self
        view.addSubview(picker)
        actionsheet.addAction(cancelAction2)
        actionsheet.addAction(cancelAction1)
        self.presentController(withController: actionsheet)
    }
    
    
}

extension ChatViewController:UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Helper.defaultTimerArr().count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return Helper.defaultTimerArr()[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
         selectedTimerIndex = row
    }
    
}


extension ChatViewController: DatePopUpDelegate {
    func VoiceCallDateBtnPressed() {
        selecteDateType = .audioDate
        gotoRescheduleVC(flag: true, dateType: .audioDate)
    }
    
    
    func canceled() {
        self.inputToolbar.isHidden = false
    }
    
    func VideoCallDateBtnPressed() {
        selecteDateType = .videoDate
        gotoRescheduleVC(flag: true, dateType: .videoDate)
       // AudioHelper.sharedInstance.playCoinsSound()
        
      //  let currentDate = Date()
//        let currentTimeStamp = currentDate.millisecondsSince1970
//
//        let requestParms = [likeProfileParams.targetUserId:self.receiverID!,
//                            likeProfileParams.syncDate:currentTimeStamp,
//                            likeProfileParams.dateType:"2"
//            ] as [String : Any]
//
//        Helper.showProgressIndicator(withMessage:StringConstants.creatingDate)
//
//        API.requestPOSTURL(serviceName: APINAMES.setSyncDate,
//                           withStaticAccessToken:false,
//                           params: requestParms,
//                           success: { (response) in
//                            Helper.hideProgressIndicator()
//                            if (response.null != nil){
//                                return
//                            }
//
//                            let statuscode = response.dictionaryObject!["code"] as! Int
//                            switch (statuscode) {
//                            case 200:
//                                let when = DispatchTime.now() + 0.5
//                                DispatchQueue.main.asyncAfter(deadline: when){
//                                    self.startVideoCall()
//                                    self.navigationController?.dismiss(animated: false, completion: nil)
//                                }
//                            default:
//                                self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: response.dictionaryObject![LoginResponse.errorMessage] as! String)
//                            }
//
//        },
//                           failure: { (Error) in
//                            self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: Error.localizedDescription)
//                            Helper.hideProgressIndicator()
//        })
    }
    
    func physicalDateBtnPressed() {
        selecteDateType = .inPersonDate
        gotoRescheduleVC(flag: false, dateType:.inPersonDate)
    }
    
    
    func gotoRescheduleVC(flag:Bool,dateType:DateType){
        
        let matchedProfile = MatchedProfile(matchedUserDetails:[:])
        matchedProfile.userId = self.getReceiverID()!
        matchedProfile.firstLikedByMe = false
        matchedProfile.name = getReceiverName()!
        matchedProfile.profilePic = getProfilePic()!
        
        
        let vc = UIStoryboard(name:"DatumTabBarControllers", bundle: nil).instantiateViewController(withIdentifier: "planDate") as! ReScheduleVC
        vc.createDateVM.isForCallDate = flag
        vc.createDateVM.matchedProfile = matchedProfile
        vc.createDateVM.isFromMatch = true
        vc.createDateVM.dateFromChat = true
        vc.createDateVM.dateType = dateType
        let preferenceNav: UINavigationController =  UINavigationController(rootViewController: vc)
        preferenceNav.isNavigationBarHidden = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
