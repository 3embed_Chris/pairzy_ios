
//  ChatsListViewController.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 22/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import CocoaLumberjack
import Crashlytics


class ChatsListViewController: UIViewController {
  
    @IBOutlet weak var defaultScreenView: UIView!
    @IBOutlet weak var chatlbl: UILabel!
    @IBOutlet weak var chatIcon: UIImageView!
    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var refreshButtonOutlet: UIBarButtonItem!
    
    
    var chatListViewModel: ChatListViewModel!
    let selfID = Helper.getMQTTID()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var name = NSNotification.Name(rawValue: "ChatUpdatedNotification")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatsListViewController.updateChatList), name: name, object: nil)
        
        name = NSNotification.Name(rawValue: "notificationTapped")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatsListViewController.notificationTapped(notification:)), name: name, object: nil)

        //get chatList from api call
        if let userID = selfID {
            MQTTChatManager.sharedInstance.subscribeGetChatTopic(withUserID: userID)
            if (self.chatListViewModel != nil) {
                let count = self.chatListViewModel.unreadChatsCounts()
                if count==0 {
                    Helper.showProgressIndicator(withMessage: StringConstants.fetchingChat())
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                        Helper.hideProgressIndicator()
                    }
                    ChatAPI().getChats(withPageNo:"0", withCompletion: { response in
                        print(response)
                        Helper.hideProgressIndicator()
                    })
                }
            }
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.showdefaultScreen()
        self.tableViewOutlet.reloadData()
//        if let tabbar = self.tabBarController as? TabBarController {
//            tabbar.enablePushNotification()
//        }
        self.checkNewGroupCreated()
    }
    
    @objc func updateChatList() {
        let couchbaseObj = Couchbase.sharedInstance
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        guard let chats = chatsDocVMObject.getChats() else { return }
        self.chatListViewModel = ChatListViewModel(withChatObjects: chats)
        self.chatListViewModel.authCheckForNotification()
        self.updateChatListCount()
        showdefaultScreen()
        self.tableViewOutlet.reloadData()
    }
    
    
    func updateChatListCount() {
        let count = self.chatListViewModel.unreadChatsCounts()
        if count == 0 {
            self.tabBarController?.tabBar.items?[4].badgeValue = nil
        } else {
            self.tabBarController?.tabBar.items?[4].badgeValue = "\(count)"
        }
    }
    
    @objc func notificationTapped(notification: NSNotification) {
        let userInfo = notification.userInfo as! [String: Any]
        guard let chatObj = userInfo["chatObj"] as? [String : Any] else { return }
        guard let senderID = chatObj["from"] as? String else { return }  ///from  senderID
        let individualCVMObj = IndividualChatViewModel(couchbase: Couchbase())
        if let toDocID = individualCVMObj.fetchIndividualChatDoc(withRecieverID: senderID, andSecretID: "") {
           
            let chatsDocVMObj = ChatsDocumentViewModel(couchbase: Couchbase())
            if let chat = chatsDocVMObj.getChatObj(fromChatDocID: toDocID) {
                if let chatDocID = UserDefaults.standard.object(forKey: ChatAppConstants.UserDefaults.isUserOnchatscreen) as? String {
                    if chatDocID == toDocID {
                        return}
                }
                self.performSegue(withIdentifier: SegueIds.chatSegueIdentifier, sender: chat)
            }
        }
    }
    
    func getChatInitially() {
        //get chatList from api call
        if let userID = selfID {
            MQTTChatManager.sharedInstance.subscribeGetChatTopic(withUserID: userID)
            if (self.chatListViewModel != nil) {
                let count = self.chatListViewModel.getNumberOfRows()
                if count==0 {
                    ChatAPI().getChats(withPageNo:"0", withCompletion: { response in
                        print(response)
                    })
                }
            }
        }
    }
    
    func  showdefaultScreen() {
        let count = self.chatListViewModel.getNumberOfRows()
        if count == 0 {
            self.defaultScreenView.frame.size.height = self.view.frame.size.height
            self.chatlbl.isHidden = false
            self.chatIcon.isHidden = false
        } else {
            self.chatlbl.isHidden = true
            self.chatIcon.isHidden = true
            self.defaultScreenView.frame.size.height = 0
        }
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func refreshButtonAction(_ sender: Any) {
        
        let isNetwork = (UIApplication.shared.delegate as! AppDelegate).isNetworkThere
        
        if isNetwork == true{
            ChatAPI().getChats(withPageNo:"0", withCompletion: { response in
                print(response)
            })
            refreshButtonOutlet.isEnabled = false
            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when ){
                self.refreshButtonOutlet.isEnabled = true
            }
        }else{
            Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: StringConstants.checkInternet())
        }
    }
    
    
    @IBAction func newChatCliked(_ sender: Any) {
        DDLogDebug("new chat cliked")
        self.tabBarController?.selectedIndex = 2
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateChatList()
        self.getChatInitially()
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.tableViewOutlet.setEditing(editing, animated: animated)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIds.chatSegueIdentifier {
            let controller = segue.destination as! ChatViewController
            if let chatObj = sender as? Chat {
                let chatVMObject = ChatViewModel(withChatData: chatObj)
                controller.chatViewModelObj = chatVMObject
            } else if let userID = sender as? String {
//                if let favObj = FavoriteViewModel.sharedInstance.getContactObject(forUserID: userID) {
//                    controller.favoriteObj = favObj
//                }
            }
        }
    }
    
    
    //MARK: - Other Methodes
    func checkNewGroupCreated(){
        if let isCreatGp = UserDefaults.standard.object(forKey: "isCreatingNewGroupPress") as? Bool {
            if isCreatGp == true{
                self.tableViewOutlet.selectRow(at: IndexPath.init(row: 0, section: 0), animated: false, scrollPosition: .none)
                self.tableView(self.tableViewOutlet, didSelectRowAt: IndexPath.init(row: 0, section: 0))
                UserDefaults.standard.set(false, forKey: "isCreatingNewGroupPress")
            }
        }
    }
    
}

extension ChatsListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.chatListViewModel.setUpTableViewCell(indexPath: indexPath, tableView: tableView)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatListViewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatObj = self.chatListViewModel.chats[indexPath.row]
        self.performSegue(withIdentifier: SegueIds.chatSegueIdentifier, sender: chatObj)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            //1 delete locally
            tableView.beginUpdates()
            Helper.showProgressIndicator(withMessage: StringConstants.deleting())
            self.chatListViewModel.deleteRow(fromIndexPath: indexPath, success: { response in
                self.chatListViewModel.deleteChat(fromIndexPath: indexPath)
                tableView.deleteRows(at: [indexPath], with:.none)
                tableView.endUpdates()
                self.showdefaultScreen()
                tableView.reloadData()
                Helper.hideProgressIndicator()
            }, failure: { error in
                Helper.hideProgressIndicator()
                self.chatListViewModel.deleteChat(fromIndexPath: indexPath)
                tableView.deleteRows(at: [indexPath], with:.none)
                tableView.endUpdates()
            })
        }
    }
}
