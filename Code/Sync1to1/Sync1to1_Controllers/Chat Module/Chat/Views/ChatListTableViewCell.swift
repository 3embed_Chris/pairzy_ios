//
//  ChatListTableViewCell.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 22/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

class ChatListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var coinsIcon: UIImageView!
    @IBOutlet weak var superLikeIcon: UIImageView!
    @IBOutlet weak var userImageOutlet: UIImageView!
    @IBOutlet weak var userNameOutlet: UILabel!
    @IBOutlet weak var lastMessageOutlet: UILabel!
    @IBOutlet weak var lastimeMessageOutlet: UILabel!
    @IBOutlet weak var unreadMsgCountLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var onlineStatus: UIImageView!
    @IBOutlet weak var newMsgCountBgView: UIView!
    @IBOutlet weak var newMessageIcon: UIImageView!
    var lastMessageText = ""
    var longGesture:UILongPressGestureRecognizer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        bgView.layer.cornerRadius = bgView.frame.size.height/2
        userImageOutlet.layer.cornerRadius = userImageOutlet.frame.size.height/2
        newMsgCountBgView.layer.cornerRadius = newMsgCountBgView.frame.size.height/2
        Helper.setShadow(sender: userImageOutlet)
        //MARK:- Buttons Action
        
        
    }
    
    var profileVmObj:Profile! {
        
        didSet {
//            if let profileDetails = profileVmObj.userId as? String {
            if let imageURL = profileVmObj.profilePicture as? String {
                if imageURL == "defaultUrl" {
                    self.userImageOutlet.image = UIImage()
                } else {
                    self.userImageOutlet.kf.setImage(with: URL(string: imageURL), placeholder:#imageLiteral(resourceName: "02m"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
                }
            }
            
            
            if profileVmObj.isMatchedUser == 1 {
                self.coinsIcon.isHidden = true
                userImageOutlet.layer.borderWidth = 2.0
                userImageOutlet.layer.borderColor = UIColor.white.cgColor
            } else {
                self.coinsIcon.isHidden = false
                userImageOutlet.layer.borderWidth = 2.0
                userImageOutlet.layer.borderColor = Helper.getUIColor(color: "FFD500").cgColor
            }
            
            if profileVmObj.onlineStatus == 1 {
              onlineStatus.isHidden = false
            }else {
              onlineStatus.isHidden = true
            }
            
            if profileVmObj.isMatchedBySuperLike == 1{
                self.superLikeIcon.isHidden = false
            } else {
                self.superLikeIcon.isHidden = true
            }
            
            lastMessageText = profileVmObj.lastMessage.fromBase64() ?? ""
            self.lastMessageOutlet.text = lastMessageText
            
            if let messageType:MessageTypes = MessageTypes(rawValue: Int(profileVmObj.messageType) ?? 0){
                self.setLastMessageInLabel(messageType: messageType, isSelfMsg: profileVmObj.isSelfMsg)
            }
            self.lastimeMessageOutlet.text = ""
            self.userNameOutlet.text = profileVmObj.firstName
            updateUnseenMessageDetails()

            DispatchQueue.main.async {
                self.layoutIfNeeded()
            }
        }
    }
    
    var chatsDocVMObject : ChatsDocumentViewModel! {
        return ChatsDocumentViewModel(couchbase: Couchbase.sharedInstance)
    }
    
    
    
    func updateUnseenMessageDetails() {
        let individualChatDocVMObject = IndividualChatViewModel(couchbase: Couchbase.sharedInstance)
        guard let chatDocID = individualChatDocVMObject.getChatDocID(withreceiverID: profileVmObj.userId, andSecretID: "", withContactObj: profileVmObj, messageData: nil, destructionTime: nil, isCreatingChat: false) else {
            return
        }
        if let chatObj = chatsDocVMObject.getChatObj(fromChatDocID: chatDocID) {
            self.chatVMObj = ChatViewModel(withChatData: chatObj)
        }
    }
    
    
    
    var chatVMObj : ChatViewModel! {
        didSet {
            if let docID = chatVMObj.docID {
                //self.lastMessageOutlet.text = chatVMObj.newMessage
//                self.lastMessageOutlet.text = lastMessageText
                self.unreadMsgCountLabel.text = chatVMObj.newMessageCount
                
                //self.lastMessageOutlet.text = chatVMObj.newMessageDateInString
                if !chatVMObj.hasNewMessage {
                    self.newMsgCountBgView.isHidden = true
                    self.lastMessageOutlet.textColor = UIColor.black
                } else {
                    self.newMsgCountBgView.isHidden = false
                    self.lastMessageOutlet.textColor = UIColor(red: 0/255, green: 90/255, blue: 255/255, alpha: 1)
                }
                self.getLastMessage(withDocID: docID, chatVMObject: chatVMObj)

            }
            DispatchQueue.main.async {
                self.layoutIfNeeded()
            }
        }
    }
    
    func getLastMessage(withDocID docID : String, chatVMObject  : ChatViewModel) {
        var message = ""
        let attribute = [kCTForegroundColorAttributeName : UIColor.lightGray]
        guard let messageObj = chatVMObject.getlastMessage(fromChatDocID: docID) else {
           // self.lastMessageOutlet.text = ""
            return }
        guard let deliveryStatus = messageObj.messageStatus else { return }
        if let msg = messageObj.messagePayload {
            message = msg
        }
        let payload = message.replace(target: "\n", withString: "")
        if let lastMsg = payload.fromBase64() {
            if lastMsg.count==0 {
                var str = NSMutableAttributedString()
                if self.chatVMObj.secretID.count>0 { // For showing last message text
                    if self.chatVMObj.isSelfChat {
                        str = NSMutableAttributedString(string: StringConstants.secretChat(), attributes: attribute as [NSAttributedStringKey : Any])
                    } else {
                        str = NSMutableAttributedString(string: StringConstants.youAddedToChat(), attributes: attribute as [NSAttributedStringKey : Any])
                    }
                    self.lastMessageOutlet.attributedText = str
                }
            } else {
                if messageObj.isSelfMessage {
                    guard let str = chatVMObject.getAtributeString(withMessageStatus: deliveryStatus) else { return }
                    str.append(NSAttributedString(string: lastMsg, attributes: nil))
                    self.lastMessageOutlet.attributedText = str
                } else {
                    let str = NSAttributedString(string: lastMsg, attributes: nil)
                    self.lastMessageOutlet.attributedText = str
                }
            }
        }
        
        self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: payload, attributes: attribute as [NSAttributedStringKey : Any])
        self.lastimeMessageOutlet.text = self.chatVMObj.newMessageTime
        if messageObj.isMediaMessage {
            self.setLastMediaMessage(withMessage : messageObj, isSelfMsg: messageObj.isSelfMessage)
        }
    }
    
    
    
    
    
    func setLastMediaMessage(withMessage messageObj : Message, isSelfMsg : Bool) {
//        let attribute = [kCTForegroundColorAttributeName : UIColor.lightGray]
        if let messageMediaType:MessageTypes = messageObj.messageType {
            self.setLastMessageInLabel(messageType: messageMediaType, isSelfMsg: isSelfMsg)
//            switch messageMediaType {
//
//            case .text:
//                break;
//
//            case .image:
//                if isSelfMsg == false {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youGotImage(), attributes: attribute as [NSAttributedStringKey : Any])
//                } else {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youSentImage(), attributes: attribute as [NSAttributedStringKey : Any])
//                }
//            case .video:
//                if isSelfMsg == false {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youGotVideo(), attributes: attribute as [NSAttributedStringKey : Any])
//                } else {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youSentVideo(), attributes: attribute as [NSAttributedStringKey : Any])
//                }
//                break;
//
//            case .location:
//                if isSelfMsg == false {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youGotLoc(), attributes: attribute as [NSAttributedStringKey : Any])
//                } else {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentLoc(), attributes: attribute as [NSAttributedStringKey : Any])
//                }
//            case .contact:
//                if isSelfMsg == false {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotContact(), attributes: attribute as [NSAttributedStringKey : Any])
//                } else {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentContact(), attributes: attribute as [NSAttributedStringKey : Any])
//                }
//                break;
//
//            case .audio:
//                if isSelfMsg == false {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotAudio(), attributes: attribute as [NSAttributedStringKey : Any])
//                } else {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentAudio(), attributes: attribute as [NSAttributedStringKey : Any])
//                }
//                break;
//
//            case .sticker:
//                if isSelfMsg == false {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotSticker(), attributes: attribute as [NSAttributedStringKey : Any])
//                } else {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentSticker(), attributes: attribute as [NSAttributedStringKey : Any] )
//                }
//                break;
//
//            case .doodle:
//                if isSelfMsg == false {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotDoodle(), attributes: attribute as [NSAttributedStringKey : Any])
//                } else {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentDoodle(), attributes: attribute as [NSAttributedStringKey : Any])
//                }
//                break;
//
//            case .gif:
//                if isSelfMsg == false {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotGif(), attributes: attribute as [NSAttributedStringKey : Any] )
//                } else {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentGif(), attributes: attribute as [NSAttributedStringKey : Any])
//                }
//                break;
//
//            case .document:
//                if isSelfMsg == false {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotDoc(), attributes: attribute as [NSAttributedStringKey : Any])
//                } else {
//                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentDoc(), attributes: attribute as [NSAttributedStringKey : Any])
//                }
//                break;
//
//            case .replied:
////                if let repliedMsgObj = messageObj.repliedMessage {
////                    if let messageMediaType:MessageTypes = repliedMsgObj.replyMessageType {
////                        switch messageMediaType {
////
////                        case .text:
////                            break;
////
////                        case .image:
////                            if isSelfMsg == false {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youGotImage(), attributes: attribute as [NSAttributedStringKey : Any])
////                            } else {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youSentImage(), attributes: attribute as [NSAttributedStringKey : Any])
////                            }
////                        case .video:
////                            if isSelfMsg == false {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youGotVideo(), attributes: attribute as [NSAttributedStringKey : Any])
////                            } else {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youSentVideo(), attributes: attribute as [NSAttributedStringKey : Any])
////                            }
////                            break;
////
////                        case .location:
////                            if isSelfMsg == false {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youGotLoc(), attributes: attribute as [NSAttributedStringKey : Any])
////                            } else {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentLoc(), attributes: attribute as [NSAttributedStringKey : Any])
////                            }
////                        case .contact:
////                            if isSelfMsg == false {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotContact(), attributes: attribute as [NSAttributedStringKey : Any])
////                            } else {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentContact(), attributes: attribute as [NSAttributedStringKey : Any])
////                            }
////                            break;
////
////                        case .audio:
////                            if isSelfMsg == false {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotAudio(), attributes: attribute as [NSAttributedStringKey : Any])
////                            } else {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentAudio(), attributes: attribute as [NSAttributedStringKey : Any])
////                            }
////                            break;
////
////                        case .sticker:
////                            if isSelfMsg == false {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotSticker(), attributes: attribute as [NSAttributedStringKey : Any])
////                            } else {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentSticker(), attributes: attribute as [NSAttributedStringKey : Any])
////                            }
////                            break;
////
////                        case .doodle:
////                            if isSelfMsg == false {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotDoodle(), attributes: attribute as [NSAttributedStringKey : Any])
////                            } else {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentDoodle(), attributes: attribute as [NSAttributedStringKey : Any])
////                            }
////                            break;
////
////                        case .gif:
////                            if isSelfMsg == false {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotGif(), attributes: attribute as [NSAttributedStringKey : Any])
////                            } else {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentGif(), attributes: attribute as [NSAttributedStringKey : Any])
////                            }
////                            break;
////
////                        case .document:
////                            if isSelfMsg == false {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotDoc(), attributes: attribute as [NSAttributedStringKey : Any])
////                            } else {
////                                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentDoc(), attributes: attribute as [NSAttributedStringKey : Any])
////                            }
////                            break;
////
////                        case .replied:
////
////                            break;
////                        case .deleted:
////                            self.lastMessageOutlet.attributedText = getDeleteString()
////                        }
////                    }
////                }
//                /////////
//                break;
//            case .deleted:
////                self.deleteiconOutlet.isHidden = false
//                self.lastMessageOutlet.attributedText = getDeleteString()
//            }
        }
    }
    
    
    func setLastMessageInLabel(messageType: MessageTypes, isSelfMsg: Bool){
        let attribute = [kCTForegroundColorAttributeName : UIColor.lightGray]
        switch messageType {
            
        case .text:
            break;
            
        case .image:
            if isSelfMsg == false {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youGotImage(), attributes: attribute as [NSAttributedStringKey : Any])
            } else {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youSentImage(), attributes: attribute as [NSAttributedStringKey : Any])
            }
        case .video:
            if isSelfMsg == false {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youGotVideo(), attributes: attribute as [NSAttributedStringKey : Any])
            } else {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youSentVideo(), attributes: attribute as [NSAttributedStringKey : Any])
            }
            break;
            
        case .location:
            if isSelfMsg == false {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.youGotLoc(), attributes: attribute as [NSAttributedStringKey : Any])
            } else {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentLoc(), attributes: attribute as [NSAttributedStringKey : Any])
            }
        case .contact:
            if isSelfMsg == false {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotContact(), attributes: attribute as [NSAttributedStringKey : Any])
            } else {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentContact(), attributes: attribute as [NSAttributedStringKey : Any])
            }
            break;
            
        case .audio:
            if isSelfMsg == false {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotAudio(), attributes: attribute as [NSAttributedStringKey : Any])
            } else {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentAudio(), attributes: attribute as [NSAttributedStringKey : Any])
            }
            break;
            
        case .sticker:
            if isSelfMsg == false {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotSticker(), attributes: attribute as [NSAttributedStringKey : Any])
            } else {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentSticker(), attributes: attribute as [NSAttributedStringKey : Any] )
            }
            break;
            
        case .doodle:
            if isSelfMsg == false {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotDoodle(), attributes: attribute as [NSAttributedStringKey : Any])
            } else {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentDoodle(), attributes: attribute as [NSAttributedStringKey : Any])
            }
            break;
            
        case .gif:
            if isSelfMsg == false {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotGif(), attributes: attribute as [NSAttributedStringKey : Any] )
            } else {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentGif(), attributes: attribute as [NSAttributedStringKey : Any])
            }
            break;
            
        case .document:
            if isSelfMsg == false {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.gotDoc(), attributes: attribute as [NSAttributedStringKey : Any])
            } else {
                self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: StringConstants.sentDoc(), attributes: attribute as [NSAttributedStringKey : Any])
            }
            break;
            
        case .replied:
           
            break;
        case .deleted:
            //                self.deleteiconOutlet.isHidden = false
            self.lastMessageOutlet.attributedText = getDeleteString()
        }
    }
    
    func getDeleteString() -> NSMutableAttributedString {
        let attribute = [kCTFontAttributeName : UIFont.italicSystemFont(ofSize: 13)]
        let attributedString = NSMutableAttributedString(string:StringConstants.thisMsgDeleted(), attributes: attribute as [NSAttributedStringKey : Any])
        return attributedString
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}


extension String{
    
    static func addImageToString(text: String ,image: UIImage) -> NSAttributedString{
        
        let attributedString = NSMutableAttributedString(string: text)
        let textAttach =  NSTextAttachment()
        textAttach.image = image
        textAttach.bounds = CGRect.init(x: 0, y: -3, width: 20, height: 20)
        let attrStringWithImage = NSAttributedString(attachment: textAttach)
        attributedString.replaceCharacters(in: NSMakeRange(0,0), with: attrStringWithImage)
        return  attributedString
    }
    
    
}
