//
//  SentLocationCell.swift
//  Yelo
//
//  Created by Sachin Nautiyal on 13/11/2017.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import JSQMessagesViewController

// Used for the outlets of the SentLocationCell
class RepliedSentLocationCell: JSQMessagesCollectionViewCell {
    
    var locationDelegate : LocationCellDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.delegate = self
    }
    @IBOutlet weak var locationImageOutlet: UIImageView!
    @IBOutlet weak var replyViewOutlet: ReplyView!
    
    /// model object of message. see didSet for more details.
    var msgObj  : Message!{
        didSet {
            self.setValues(withMsgObj : msgObj)
            replyViewOutlet.selectedMessage = msgObj
        }
    }
    
    
    var repliedButtonPressedDelegate : ReplyViewDismissDelegate? {
        didSet {
            replyViewOutlet.replyViewDismissDelegate = repliedButtonPressedDelegate
        }
    }
        
    /// used fo setting values for location inside the cell.
    ///
    /// - Parameter msgObj: Message Object.
    func setValues(withMsgObj msgObj : Message) {
        var msgStr = ""
        if let msg = msgObj.messagePayload?.fromBase64() {
            msgStr = msg
        } else {
            msgStr = msgObj.messagePayload!
        }
        self.locationImageOutlet.kf.indicatorType = .activity
        self.locationImageOutlet.kf.indicator?.startAnimatingView()
        DispatchQueue.global().async {
            if let latStr = msgStr.slice(from: "(", to: ","), let longStr = msgStr.slice(from: ",", to: ")") {
                let str = "http://maps.googleapis.com/maps/api/staticmap?markers=color:red|\(latStr),\(longStr)&zoom=15&size=180x220&sensor=true&key=\(AppConstant.googleMapKey)"
                
                var queryCharSet = NSCharacterSet.urlQueryAllowed
                queryCharSet.remove(charactersIn: "+&")
                
             if let utfStr =  str.addingPercentEncoding(withAllowedCharacters: queryCharSet) {
              // if let utfStr = str.addingPercentEscapes(using: String.Encoding.utf8) {
                    if let mapUrl = URL(string: utfStr) {
                        DispatchQueue.main.async {
                            self.locationImageOutlet.kf.setImage(with: mapUrl, placeholder: nil, options: [.backgroundDecode], progressBlock: nil, completionHandler: nil)
                            self.locationImageOutlet.kf.indicator?.stopAnimatingView()
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func tapOnSentLocationCell(_ sender: Any) {
        self.locationDelegate?.tapOnLocationDelegate(messageObj: msgObj)
    }
    
}

extension RepliedSentLocationCell : JSQMessagesCollectionViewCellDelegate {
    func messagesCollectionViewCellDidTapAvatar(_ cell: JSQMessagesCollectionViewCell) {
    }
    
    func messagesCollectionViewCellDidTapMessageBubble(_ cell: JSQMessagesCollectionViewCell) {
        
    }
    
    func messagesCollectionViewCellDidTap(_ cell: JSQMessagesCollectionViewCell, atPosition position: CGPoint) {
        
    }
    
    func messagesCollectionViewCell(_ cell: JSQMessagesCollectionViewCell, didPerformAction action: Selector, withSender sender: Any) {
        if let collectionView = self.superview as? UICollectionView {
            if let indexPath = collectionView.indexPathForItem(at: self.center) {
                collectionView.delegate?.collectionView!(collectionView, performAction: action, forItemAt: indexPath, withSender: nil)
            }
        }
    }
    
    func messagesCollectionViewCellDidTapAccessoryButton(_ cell: JSQMessagesCollectionViewCell) {
        print("***********Tapped Accessaory***********")

    }
}

/// SentLocationMediaItem inherites the JSQMessageMediaData properties
class RepliedSentLocationMediaItem : NSObject, JSQMessageMediaData {
    
    /// Used for the instance of SentLocationCell
    let locationCell = RepliedSentLocationCell(frame:CGRect(x: 0, y: 0, width: 180, height: 260))
    
    func mediaView() -> UIView? {
        return locationCell
    }
    
    func mediaPlaceholderView() -> UIView {
        return locationCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 180, height: 260)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}

