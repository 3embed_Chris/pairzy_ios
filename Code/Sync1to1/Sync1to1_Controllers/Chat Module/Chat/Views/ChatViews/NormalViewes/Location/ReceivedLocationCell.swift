//
//  ReceivedLocationCell.swift
//  Yelo
//
//  Created by Sachin Nautiyal on 13/11/2017.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import Kingfisher

protocol LocationCellDelegate: class {
    func tapOnLocationDelegate(messageObj: Message)
}


/// class for the outlets of the ReceivedLocationCell.
class ReceivedLocationCell: JSQMessagesCollectionViewCell {
    
    @IBOutlet weak var locationImageOutlet: UIImageView!
    weak var locationDelegate : LocationCellDelegate? = nil
    @IBOutlet weak var backGroundViewOutlet: UIView!
    /// model object of message. see didSet for more details.
    var msgObj  : Message!{
        didSet {
            self.setValues(withMsgObj : msgObj)
        }
    }
    
    /// used fo setting values for location inside the cell.
    ///
    /// - Parameter msgObj: Message Object. backgroundDecode
    func setValues(withMsgObj msgObj : Message) {
        var msgStr = ""
        if let msg = msgObj.messagePayload?.fromBase64() {
            msgStr = msg
        } else {
            msgStr = msgObj.messagePayload!
        }
        self.locationImageOutlet.kf.indicatorType = .activity
        self.locationImageOutlet.kf.indicator?.startAnimatingView()
        DispatchQueue.global().async {
            if let latStr = msgStr.slice(from: "(", to: ","), let longStr = msgStr.slice(from: ",", to: ")") {
                let str = "http://maps.googleapis.com/maps/api/staticmap?markers=color:red|\(latStr),\(longStr)&zoom=15&size=180x220&sensor=true&key=\(AppConstant.googleMapKey)"
               // if let utfStr = str.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                
                var queryCharSet = NSCharacterSet.urlFragmentAllowed
                if let utfStr = str.addingPercentEncoding(withAllowedCharacters: queryCharSet) {
                    if let mapUrl = URL(string: utfStr) {
                        DispatchQueue.main.async {
                            self.locationImageOutlet.kf.setImage(with: mapUrl, placeholder: nil, options: [.backgroundDecode], progressBlock: nil, completionHandler: nil)
                            self.locationImageOutlet.kf.indicator?.stopAnimatingView()
                        }
                    }
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.delegate = self
    }
    
    
    @IBAction func tapOnReceivedLocationCell(_ sender: Any) {
        self.locationDelegate?.tapOnLocationDelegate(messageObj: msgObj)
    }
    
    @IBAction func forwardButtonAction(_ sender: UIButton) {
    }
    
}

extension ReceivedLocationCell : JSQMessagesCollectionViewCellDelegate {
    func messagesCollectionViewCellDidTapAvatar(_ cell: JSQMessagesCollectionViewCell) {
    }
    
    func messagesCollectionViewCellDidTapMessageBubble(_ cell: JSQMessagesCollectionViewCell) {
        
    }
    
    func messagesCollectionViewCellDidTap(_ cell: JSQMessagesCollectionViewCell, atPosition position: CGPoint) {
        
    }
    
    func messagesCollectionViewCell(_ cell: JSQMessagesCollectionViewCell, didPerformAction action: Selector, withSender sender: Any) {
        if let collectionView = self.superview as? UICollectionView {
            if let indexPath = collectionView.indexPathForItem(at: self.center) {
                collectionView.delegate?.collectionView!(collectionView, performAction: action, forItemAt: indexPath, withSender: nil)
            }
        }
    }
    
    func messagesCollectionViewCellDidTapAccessoryButton(_ cell: JSQMessagesCollectionViewCell) {
        
    }
}


/// ReceivedLocationMediaItem inherites the JSQMessageMediaData properties
class ReceivedLocationMediaItem : NSObject, JSQMessageMediaData {
    
    /// Used for the instance of ReceivedLocationCell
    let locationCell = ReceivedLocationCell(frame:CGRect(x: 0, y: 0, width: 180, height: 220))
    
    func mediaView() -> UIView? {
        return locationCell
    }
    
    func mediaPlaceholderView() -> UIView {
        return locationCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 180, height: 220)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}
