//
//  DocumentReceivedCollectionViewCell.swift
//  MQTT Chat Module
//
//  Created by Sachin Nautiyal on 28/02/2018.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import EVURLCache


class DocumentReceivedCollectionViewCell: JSQMessagesCollectionViewCell {
    
    @IBOutlet weak var backGroundViewOutlet: UIView!
    @IBOutlet weak var fileTypeLabelOutlet: UILabel!
    @IBOutlet weak var webViewOutlet: UIWebView! {
        didSet {
            self.webViewOutlet.delegate = self
            self.progressIndicatorOutlet.hidesWhenStopped = true
        }
    }
    
    @IBOutlet weak var progressIndicatorOutlet: UIActivityIndicatorView!
    @IBOutlet weak var fileTypeImageOutlet: UIImageView!
    @IBOutlet weak var fileTitleLabelOutlet: UILabel!
    
    var documentMessageDelegate : DocumentMessageDelegate? = nil
    var chatDocID : String!
    
    var documentMVMObj : DocumentMessageViewModal! {
        didSet {
            self.setupDoc(withDocMVMObj: documentMVMObj)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.delegate = self
    }
    
    func setupDoc(withDocMVMObj docMVMObj: DocumentMessageViewModal) {
        if let fileURL = docMVMObj.getFileURL() {
            self.setupDoc(withURL: fileURL)
        }
        
        if let fileName = docMVMObj.getFileName() {
            self.fileTitleLabelOutlet.text = fileName
        }
        
        if let fileExtension = docMVMObj.getDocTypeText() {
            self.fileTypeLabelOutlet.text = fileExtension
        }
        self.fileTypeImageOutlet.image = docMVMObj.getDocTypeImage()
    }
    
    @IBAction func openWebViewAction(_ sender: UIButton) {
        if (documentMessageDelegate != nil) {
            self.documentMessageDelegate?.openDocumentDelegate(withDocumentVMObj : documentMVMObj)
        }
    }
    
    @IBAction func forwardButtonAction(_ sender: UIButton) {
    }
    
    func setupDoc(withURL docURL: String) {
        if let url = URL(string: docURL) {
            self.progressIndicatorOutlet.startAnimating()
            webViewOutlet.loadRequest(URLRequest(url: url))
        }
    }
}

extension DocumentReceivedCollectionViewCell : JSQMessagesCollectionViewCellDelegate {
    func messagesCollectionViewCellDidTapAvatar(_ cell: JSQMessagesCollectionViewCell) {
    }
    
    func messagesCollectionViewCellDidTapMessageBubble(_ cell: JSQMessagesCollectionViewCell) {
        
    }
    
    func messagesCollectionViewCellDidTap(_ cell: JSQMessagesCollectionViewCell, atPosition position: CGPoint) {
        
    }
    
    func messagesCollectionViewCell(_ cell: JSQMessagesCollectionViewCell, didPerformAction action: Selector, withSender sender: Any) {
        if let collectionView = self.superview as? UICollectionView {
            if let indexPath = collectionView.indexPathForItem(at: self.center) {
                collectionView.delegate?.collectionView!(collectionView, performAction: action, forItemAt: indexPath, withSender: nil)
            }
        }
    }
    
    func messagesCollectionViewCellDidTapAccessoryButton(_ cell: JSQMessagesCollectionViewCell) {
        
    }
}

extension DocumentReceivedCollectionViewCell : UIWebViewDelegate {
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let redirectURL = EVURLCache.shouldRedirect(request: request) {
            let r = URLRequest(url: redirectURL)
            webView.loadRequest(r)
            return false
        }
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.progressIndicatorOutlet.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.progressIndicatorOutlet.stopAnimating()
    }
}

/// ReceivedDocumentMediaItem inherites the JSQMessageMediaData properties
class ReceivedDocumentMediaItem : NSObject, JSQMessageMediaData {
    
    /// Used for the instance of ReceivedDocCell
    let docCell = DocumentReceivedCollectionViewCell(frame:CGRect(x: 0, y: 0, width: 250, height: 200))
    
    func mediaView() -> UIView? {
        return docCell
    }
    
    func mediaPlaceholderView() -> UIView {
        return docCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 250, height: 200)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}
