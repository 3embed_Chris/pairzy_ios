//
//  AudioSentCollectionViewCell.swift
//  MQTT Chat Module
//
//  Created by Sachin Nautiyal on 26/02/2018.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import JSQMessagesViewController

protocol AudioPlayerDelegate: class{
    func playing(withInstance : AudioPlayerManager)
}

class AudioSentCollectionViewCell: JSQMessagesCollectionViewCell {
    
    @IBOutlet weak var playButtonOutlet: UIButton!
    @IBOutlet weak var audioProgressOutlet: UIProgressView!
    
    var audioPlayer = AudioPlayerManager()
    var chatDocID : String!
    var isGroup: Bool = false
    var groupMembers:[[String:Any]]?
    var gpImage:String?
    var msgObj : Message! {
        didSet {
            self.audioPlayer.setMaximumVolume()
        }
    }
    weak var audioPlayerDelegate : AudioPlayerDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.delegate = self
    }
    
    func startPlaying(withURL audioURL : String) {
        self.audioPlayer.play(urlString : audioURL)
        print("play audio URL \(audioURL)")
        
       
        self.audioPlayer.addPlayStateChangeCallback(self, callback: { [weak self] (track: AudioTrack?) in
            self?.updateButtonStates()
            self?.updatePlaybackTime(track)
            
        })
        self.audioPlayer.addPlaybackTimeChangeCallback(self, callback: { [weak self] (track: AudioTrack?) in
            DispatchQueue.main.async {
                self?.updatePlaybackTime(track)
            }
        })
       
         self.audioPlayerDelegate?.playing(withInstance: self.audioPlayer)
    }
    
    
    
    func playAudioInitially(withAudioURL audURL : String) {
        let playerPlaying = audioPlayer.isPlaying(url: URL(string: audURL)!)
        if playerPlaying {
            audioPlayer.togglePlayPause()
        } else {
            self.startPlaying(withURL: audURL)
        }
    }
    
    private func updateButtonStates() {
        print("is playing  \(audioPlayer.isPlaying())")
        self.playButtonOutlet?.isSelected = audioPlayer.isPlaying()
        //self.playButtonOutlet.isSelected = false
    }
    
    private func updatePlaybackTime(_ track: AudioTrack?) {
        self.audioProgressOutlet.progress = track?.currentProgress() ?? 0
    }
    
    @IBAction func playButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if let audioURL = msgObj.messagePayload {
            if audioURL.count>0 {
                self.playAudioInitially(withAudioURL : audioURL)
            } else if let audioURL = msgObj.mediaURL {
                self.playAudioInitially(withAudioURL : audioURL)
            }
        }
    }
    
    @IBAction func forwardButtonAction(_ sender: UIButton) {
    }
}

extension AudioSentCollectionViewCell : JSQMessagesCollectionViewCellDelegate {
    func messagesCollectionViewCellDidTapAvatar(_ cell: JSQMessagesCollectionViewCell) {
    }
    
    func messagesCollectionViewCellDidTapMessageBubble(_ cell: JSQMessagesCollectionViewCell) {
        
    }
    
    func messagesCollectionViewCellDidTap(_ cell: JSQMessagesCollectionViewCell, atPosition position: CGPoint) {
        
    }
    
    func messagesCollectionViewCell(_ cell: JSQMessagesCollectionViewCell, didPerformAction action: Selector, withSender sender: Any) {
        if let collectionView = self.superview as? UICollectionView {
            if let indexPath = collectionView.indexPathForItem(at: self.center) {
                collectionView.delegate?.collectionView!(collectionView, performAction: action, forItemAt: indexPath, withSender: nil)
            }
        }
    }
    
    func messagesCollectionViewCellDidTapAccessoryButton(_ cell: JSQMessagesCollectionViewCell) {
        print("***********Tapped Accessaory***********")

    }
}

/// SentAudioMediaItem inherites the JSQMessageMediaData properties
class SentAudioMediaItem : NSObject, JSQMessageMediaData {
    
    /// Used for the instance of SentLocationCell
    let audioCell = AudioSentCollectionViewCell(frame:CGRect(x: 0, y: 0, width: 200, height: 50))
    
    func mediaView() -> UIView? {
        return audioCell
    }
    
    func mediaPlaceholderView() -> UIView {
        return audioCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 200, height: 50)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}
