//
//  ChatViewController.swift
//  MQTT Chat Module
//
//  Created by Dinesh Guptha Bavirisetti on 10/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import Firebase
import Kingfisher
import AVFoundation
import RxSwift
import AVKit
class ChatViewController: JSQMessagesViewController,UIGestureRecognizerDelegate {
    
    struct Constants {
        static let messagePageSize = "20"
        static let toCounterOfferSegue = "toCounterOfferSegue"
        static let toLocationPicker = "toLocationPicker"
        static let toPaypalViewcontroller = "toPaypalViewcontroller"
        static let paypalWebViewSegue = "paypalWebViewSegue"
        static let imageControllerSegue = ""
    }
    
    @IBOutlet weak var userImageOutlet: UIImageView!
    @IBOutlet weak var TitleLabelOutlet : UILabel!
    @IBOutlet weak var currentChatStatusLabelOutlet : UILabel!
    let couchbaseObj = Couchbase.sharedInstance
    var refresher : UIRefreshControl!
    var chatsDocVMObject : ChatsDocumentViewModel!
    var matchObjec:Profile?
    
    var updateConversationStarted = false
    var isMatchedUser = false
    var updateInUnMatchedUsers =  true
    
    let matchesVm = MatchMakerVM()
    let disposeBag = DisposeBag()
    
    var animateOnlyOnce = true
    var camerabtn:UIButton?
    
    var selfID : String? {
        return self.getUserID()
    }
    
    let incomingBubble = JSQMessagesBubbleImageFactory(bubble: UIImage.jsq_bubbleCompactTailless(), capInsets: .zero, layoutDirection: .leftToRight).incomingMessagesBubbleImage(with: UIColor.lightGray)
    let outgoingBubble = JSQMessagesBubbleImageFactory(bubble: UIImage.jsq_bubbleCompactTailless(), capInsets: .zero, layoutDirection: .leftToRight).outgoingMessagesBubbleImage(with: UIColor(red: 245/255, green: 51/255, blue: 111/255, alpha: 1.0))
    
    
    var chatViewModelObj : ChatViewModel?
    var chatDocID : String?
    var messages = [ChatMessage]()
    var recieverID :String!
    var userName : String!
    var userImage:String!
    var isTypingVisible = false
    
    fileprivate let mqttChatManager = MQTTChatManager.sharedInstance
    
    //MARK: View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.addRefresher()
        
        self.navigationController?.navigationBar.shadowImage = nil
        _ = Timer.scheduledTimer(timeInterval:1.0, target: self, selector: #selector(updateCountDownTime), userInfo: nil, repeats: true)
        
        super.collectionView?.register(UINib(nibName: "VideoReceivedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VideoReceivedCollectionViewCell")
        
        super.collectionView?.register(UINib(nibName: "VideoSentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VideoSentCollectionViewCell")
        
        self.chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        var name = NSNotification.Name(rawValue: "MessageNotification" + selfID!)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.receivedMessage(notification:)), name: name, object: nil)
        
        
        name = NSNotification.Name(rawValue: "LastSeen")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.receivedLastSeenStatus(notification:)), name: name, object: nil)
        
        name = NSNotification.Name(rawValue: "Typing")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.receivedTypingStatus(notification:)), name: name, object: nil)
        
        name = NSNotification.Name(rawValue: "PullToRefresh")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.stopRefresher(notification:)), name: name, object: nil)
        
        name = NSNotification.Name(rawValue: "UpdateProductDetailsNotification")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.recievedPostDetailsUpdate(notification:)), name: name, object: nil)
        
        name = NSNotification.Name(rawValue: "reloadCollectinViewNotification")
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.reloadCollectionView(notification:)), name: name, object: nil)
        
        self.requestForReportUserReasons(userClicked: false)
        
       // self.inputToolbar.isHidden = true
    }
    
    
    func findIsThisUserIsMatched() {
        var allMatchesList = ""
        let matchesDocumentVm = MatchesDocumentViewModel(couchbase: Couchbase())
        
        let matchesDocIdKey = "userMatchData/"
        let matchDocID = UserDefaults.standard.value(forKey: matchesDocIdKey)
        
        if let mDocID = matchDocID as? String {
            let matchesData = matchesDocumentVm.getMatchData(forDocId:mDocID)
            
            if let profileResponse = matchesData!["matchesArray"] as? [AnyObject] {
                for (index, _) in profileResponse.enumerated() {
                    let profileDetails = profileResponse[index] as! [String:Any]
                    if let userid = profileDetails["opponentId"] as? String {
                        allMatchesList = allMatchesList.appending(userid).appending(",")
                    } else  if let userid = profileDetails["_id"] as? String {
                        allMatchesList = allMatchesList.appending(userid).appending(",")
                    }
                }
            }
            
            if let reciever = getRecieverID() {
                if allMatchesList.contains(reciever){
                    isMatchedUser = true
                } else {
                    isMatchedUser = false
                }
            }
        }
    }
    
    
    func reloadCollectionView(notification: NSNotification) {
        self.collectionView?.reloadData()
    }
    
   
    @IBAction func backButtonAction(_ sender: Any) {
        moveToBack()
    }
    
    
    func moveToBack() {
        Helper.hidePushNotifications(hidePush: false)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    
    func updateCountDownTime()
    {
        let indexPathsArray = super.collectionView?.indexPathsForVisibleItems
        for indexPath in indexPathsArray! {
            if let cell = super.collectionView?.cellForItem(at: indexPath) as? VideoSentCollectionViewCell {
                let  remainingDate:Date = messages[indexPath.row].messageExpireDate!
                let diffrenceDate = remainingDate.offsetFrom(date: Date())
                //cell.videoTimerLabel?.text =  diffrenceDate + " Left"
            } else  if let cell = super.collectionView?.cellForItem(at: indexPath) as? VideoReceivedCollectionViewCell {
                let  remainingDate:Date = messages[indexPath.row].messageExpireDate!
                let diffrenceDate = remainingDate.offsetFrom(date: Date())
               // cell.videoTimerLabel?.text =  diffrenceDate + " Left"
            }
        }
    }
    
    
    func addRefresher() {
        self.refresher = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor(red: 10/255, green: 180/255, blue: 157/255, alpha: 1.0)
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.collectionView!.addSubview(refresher)
    }
    
    @IBAction func openProifleButtonAction(_ sender: Any) {
        openReceiverProfile()
    }
    
    
    func openReceiverProfile() {
        if let profileId  = recieverID {
            let profileVc = Helper.getSBWithName(name:"TabBarControllers").instantiateViewController(withIdentifier:StoryBoardIdentifier.profileVc) as! ProfileDetailVC
            profileVc.profileDeatilVM.dataAvailable = false
            profileVc.profileDeatilVM.showProfileByID = false
            profileVc.profileDeatilVM.profileDetails.userId = profileId
            self.navigationController?.present(profileVc, animated: true, completion:nil)
        }
    }
    
    
    
    func getReceiverName() -> String? {
        
        if  let favoriteObj = matchObjec {
            return  favoriteObj.firstName
        } else if let chatViewModelObj = chatViewModelObj {
            return chatViewModelObj.name
        }
        return nil
    }
    
    
    /// After getting the product details it will update the product, with all the related data.
    func updateProductDetails() {
        
        self.recieverID = self.getRecieverID()
        
        if let username = self.getReceiverName() {
            self.userName = username
        }
        
        if let uName =  self.chatViewModelObj?.name {
            self.userName = uName
        }
        
        
        if let profilePIc = self.getProfilePic() {
            self.userImage = profilePIc
            let url = URL(string : profilePIc)
            self.userImageOutlet.kf.setImage(with: url, placeholder:#imageLiteral(resourceName: "02m"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
        }
        
        self.TitleLabelOutlet.text =  self.userName

        if let chatDocID = self.getChatDocID() {
            self.chatDocID = chatDocID
            if self.chatsDocVMObject.chatDocID.count == 0, let receiverId = self.getRecieverID() {
              if let chatObj = self.chatsDocVMObject.getChatObject(forChatDocID: chatDocID, chatUserID: receiverId) {
                self.chatViewModelObj = ChatViewModel(withChatData: chatObj)
                }
            }
        }
        
        self.setUpCollectionView()
    }
    
    
     
    
    
    
    func getVideoSize(withURL resourceURL: NSURL) -> UInt64? {
        let asset = AVURLAsset(url: resourceURL as URL)
        print(asset.fileSize ?? 0)
        return UInt64(asset.fileSize!)
    }
    
    func thumbnail(sourceURL:NSURL) -> UIImage {
        let asset = AVAsset(url: sourceURL as URL)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        let time = CMTime(seconds: 1, preferredTimescale: 1)
        do {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            let image = UIImage(cgImage: imageRef)
            let imgData = NSData(data: UIImageJPEGRepresentation((image), 0.2)!)
            let compressedImage = UIImage(data: imgData as Data)
            print(imgData.length/1024)
            return compressedImage!
        } catch {
            return #imageLiteral(resourceName: "play")
        }
    }
    
    func loadData() {
        var timeStamp = Int64(floor(Date().timeIntervalSince1970 * 1000))
        if let ts = self.messages.first?.uniquemessageId {
            timeStamp = ts
        }
        
        guard let mqttId = Helper.getMQTTID() else { return }
        if mqttId != "mqttId" {
            mqttChatManager.subscribeToGetMessageTopic(withUserID: mqttId)
            self.fetchMessages(withTimeStamp: "\(timeStamp)")
        }
    }
    
    func stopRefresher(notification: NSNotification) {
        self.refresher.endRefreshing()
        let userInfo = notification.userInfo as! [String: Any]
        if ((userInfo["chatId"] as? String) == (self.chatViewModelObj?.chatID)) {
            self.setUpCollectionView()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate func getUserID() -> String? {
        let userData = Database.shared.fetchResults().first
        guard let mqttID = userData?.userId  else { return nil }
        if mqttID == "mqttId" {
            return nil
        }
        return mqttID
    }
    
    fileprivate func fetchMessages(withTimeStamp timeStamp: String) {
        self.chatViewModelObj?.getMessages(withTimeStamp: timeStamp, andPageSize: Constants.messagePageSize)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setUpCollectionView()
        guard let chatDocID = self.chatDocID else { return }
        self.updateChatForReadMessage(toDocID: chatDocID, isControllerAppearing: true)
        self.scrollToBottom(animated: false)
        findIsThisUserIsMatched()
        
        
        if let receiverImageUrl = getProfilePic() {
            if animateOnlyOnce {
                animateOnlyOnce = false
                
                //start animation.
                if(self.messages.count <= 0) {
                    
                    let fullView = UIView(frame:(self.collectionView?.bounds)!)
                    self.collectionView?.backgroundView = fullView
                    
                    let animateView = UIView(frame: CGRect(x: 20, y:((self.collectionView?.frame.size.height)!/2)-125, width:(self.collectionView?.frame.size.width)! - 40, height:250))
                    fullView.addSubview(animateView)
                    
                    let profileImage = UIImageView(frame:CGRect(x: animateView.frame.size.width/2-75, y: 50, width:150, height:150))
                    profileImage.layer.cornerRadius = 75
                    profileImage.clipsToBounds = true
                    profileImage.layoutIfNeeded()
                    
                    if isMatchedUser {
                        let titleTextLabel = UILabel(frame:CGRect(x:20, y:0, width: animateView.frame.size.width-40, height:30))
                        titleTextLabel.textAlignment = NSTextAlignment.center
                        titleTextLabel.font = UIFont(name: "CircularAirPro-Bold",
                                                     size: 24.0)
                        titleTextLabel.numberOfLines = 1
                        titleTextLabel.adjustsFontSizeToFitWidth=true
                        titleTextLabel.minimumScaleFactor=0.5;
                        titleTextLabel.text = "You Matched with ".appending(getReceiverName()!)
                        animateView.addSubview(titleTextLabel)
                        
                        let messageLabel = UILabel(frame:CGRect(x:20, y:200, width: animateView.frame.size.width-40, height:60))
                        messageLabel.font = UIFont(name: "CircularAirPro-Book",
                                                   size: 20.0)
                        messageLabel.numberOfLines = 0
                        titleTextLabel.adjustsFontSizeToFitWidth=true
                        messageLabel.minimumScaleFactor=0.5;
                        messageLabel.text = "Tell them why you swiped right"
                        messageLabel.textAlignment = NSTextAlignment.center
                        animateView.addSubview(messageLabel)
                    }
                    
                    let url = URL(string : receiverImageUrl)
                    profileImage.kf.setImage(with: url, placeholder:#imageLiteral(resourceName: "02m"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
                    animateView.addSubview(profileImage)
                    
                    animateView.transform = CGAffineTransform(scaleX: 0, y:0)
                    UIView.animate(withDuration:1.0, delay: 0.0, usingSpringWithDamping: 0.5,
                                   initialSpringVelocity: 0.5, options: [], animations:
                        {
                            animateView.transform = CGAffineTransform(scaleX: 1.0, y:1.0)
                    }, completion:nil)
                }
            }
        }
        
        
        //mqttChatManager.subscribeTypingChannel(withUserID: self.recieverID)
        //mqttChatManager.subscibeToLastSeenChannel(withUserID: self.recieverID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
        super.viewWillAppear(animated)
        updateProductDetails()
        findIsThisUserIsMatched()
        Helper.hidePushNotifications(hidePush: true)
        self.addCameraBtn()
        customiseBottomBar()
    }
    
    func customiseBottomBar() {
        
        //hide left bar
        self.inputToolbar.isHidden = false
        self.inputToolbar.contentView?.leftBarButtonItem?.isHidden = true
        self.inputToolbar.contentView?.leftBarButtonItemWidth = 0
        
        self.inputToolbar.contentView?.textView?.backgroundColor = UIColor.clear
        self.inputToolbar.contentView?.textView?.layer.cornerRadius = 5
        self.inputToolbar.contentView?.textView?.layer.borderWidth = 1
        self.inputToolbar.contentView?.textView?.layer.borderColor = UIColor.lightGray.cgColor
        self.inputToolbar.contentView?.textView?.placeHolder = Message.chatTextPlaceHolder
        self.inputToolbar.contentView?.backgroundColor = UIColor.white
       // self.inputToolbar.contentView?.rightBarButtonContainerView?.frame.size.height = 30
    
    
       self.collectionView?.backgroundColor = Helper.getUIColor(color: "F5F6F9")
    }
    
    func showPopup(){
        self.inputToolbar.isHidden = true
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let syncView = dateCallPopUpView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        syncView.loadPopUpView()
        syncView.datePopUpDelegate = self
        window.addSubview(syncView)
    }
    
    @IBAction func callButtonAction(_ sender: Any) {
        //check coins availabel or not.  For creating date user need 100 coins.
        if let getBal = UserDefaults.standard.object(forKey: "coinBalance") as? Int {
            if getBal >= 100 {
                //user has coins allow user to create date.
                showPopup()
            } else {
                // has lessthan 100 coins.
                Helper.showAlertWithMessage(withTitle:ActionSheetButtonNames.oops, message:ActionSheetButtonNames.noCoinsForDate, onViewController:self)
            }
        } else {
            // no coins available.
            Helper.showAlertWithMessage(withTitle:ActionSheetButtonNames.oops, message:ActionSheetButtonNames.noCoinsForDate, onViewController:self)
        }
    }
    
    @IBAction func attachmentButtonAction(_ sender: Any) {
       
        showAttachement()
    }
    
    
    @IBAction func moreButtonAction(_ sender: Any) {
        openAlert()
    }
    
    func sendTypingStatus() {
        if let selfID = selfID {
            mqttChatManager.sendTyping(toUser: selfID)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let chatDocID = self.chatDocID else { return }
        self.updateChatForReadMessage(toDocID: chatDocID, isControllerAppearing: false)
        
         self.inputToolbar.isHidden = true
        
        //        mqttChatManager.unsubscribeTypingChannel(withUserID: self.recieverID)
        //        mqttChatManager.unsubscibeToLastSeenChannel(withUserID: self.recieverID)
    }
    
    override func senderId() -> String {
        guard let selfID = selfID else { return ""}
        return selfID
    }
    
    func getProfilePic() -> String? {
        if  let matchObject = matchObjec {
            return  matchObject.profilePicture
        }else if let chatViewModelObj = chatViewModelObj{
            if chatViewModelObj.chat.image == ""{
                return " "
            }
            return  chatViewModelObj.chat.image
        }
        return nil
    }
    
    func getChatDocID() -> String? {
        if let chatViewModelObj = chatViewModelObj {
            return chatViewModelObj.docID
        }
        else if  let matchObject = matchObjec {
            let individualChatDocVMObject = IndividualChatViewModel(couchbase: couchbaseObj)
            guard let chatDocID = individualChatDocVMObject.getChatDocID(withRecieverID: matchObject.userId, andSecretID: "", withMatchObject: matchObject, messageData: nil) else {
                print("error in creating chatdoc \(self)")
                return nil
            }
            return chatDocID
        } else if let receiverID = self.getRecieverID(){
            let individualChatDocVMObject = IndividualChatViewModel(couchbase: couchbaseObj)
            guard let chatDocID = individualChatDocVMObject.getChatDocID(withRecieverID: receiverID, andSecretID: "", withMatchObject: nil, messageData: nil) else {
                print("error in creating chatdoc \(self)")
                return nil
            }
            return chatDocID
        }
        return nil
    }
    
    /// For fetching the receiver ID. Depends upon the chatViewModelObj or productDetails Object whichever is available.
    ///
    /// - Returns: recevier ID.
    func getRecieverID() -> String? {
        if let recieverID = chatViewModelObj?.recieverID {
            return recieverID
        } else if let recieverID = matchObjec?.userId {
            return recieverID
        }
        return nil
    }
    
    func setUpCollectionView() {
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: self.couchbaseObj)
        self.collectionView?.collectionViewLayout.incomingAvatarViewSize = CGSize(width:0.0, height:0.0)
        self.collectionView?.collectionViewLayout.outgoingAvatarViewSize = CGSize(width:0.0, height:0.0)
        self.collectionView?.collectionViewLayout.springinessEnabled = false
        guard let chatDocID = self.chatDocID else { return }
        let unsortedMessages = chatsDocVMObject.getMessagesFromChatDoc(withChatDocID: chatDocID)
        let sortedMessages = unsortedMessages.uniq()
        self.messages = sortedMessages.sorted {
            guard let uniqueID1 = $0.uniquemessageId, let uniqueID2 = $1.uniquemessageId else { return false }
            return uniqueID1 < uniqueID2
        }
        if self.messages.count == 0 {
            self.loadData()
        }
        self.collectionView?.reloadData()
    }
    
    override func senderDisplayName() -> String {
        return (UIDevice.current.identifierForVendor?.uuidString)!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}

extension ChatViewController {
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = (super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell)
        let msg = self.messages[indexPath.item]
        
        if msg.isMediaMessage {
            if let messageMediaType:MessageTypes = msg.messageType {
                switch messageMediaType {
                    
                case .video:
                    if msg.isSelfMessage {
                        var cell : VideoSentCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "VideoSentCollectionViewCell", for: indexPath) as! VideoSentCollectionViewCell
                        
                        //Creating View Modal from Video Message
                        
                        
                        
                        let videoMsgVMobj = VideoMessageViewModal(withMessage: msg)
                        let messageVMObj = MessageViewModal(withMessage: msg)
                        cell.chatDocID = self.chatDocID
                        cell.msgVMObj = videoMsgVMobj
                        //Uploading video by using Video View Modal. And checking that video is not uploaded already.
                        cell = messageVMObj.addLastDateAndTime(toCell: cell) as! VideoSentCollectionViewCell
                        cell.videoTappedDelegate = self
                        return cell
                    } else {
                        var cell : VideoReceivedCollectionViewCell =  super.collectionView?.dequeueReusableCell(withReuseIdentifier: "VideoReceivedCollectionViewCell", for: indexPath) as! VideoReceivedCollectionViewCell
                        let videoMsgVMobj = VideoMessageViewModal(withMessage: msg)
                        cell.chatDocID = self.chatDocID
                        cell.msgVMObj = videoMsgVMobj
                        let messageVMObj = MessageViewModal(withMessage: msg)
                        cell = messageVMObj.addLastDateAndTime(toCell: cell) as! VideoReceivedCollectionViewCell
                        cell.videoTappedDelegate = self
                        return cell
                    }
                    
                default :
                    return cell
                }
            }
        }
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellBottomLabelAt indexPath: IndexPath) -> CGFloat {
        let msg = self.messages[indexPath.item]
        if msg.isSelfMessage {
            return 15
        } else {
            return 15
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellBottomLabelAt indexPath: IndexPath) -> NSAttributedString? {
        let msg = self.messages[indexPath.item]
        guard let timestamp = msg.timeStamp else { return NSAttributedString() }
        let dayStr = DateHelper().getDateString(fromTimeStamp: timestamp)
        let attribute = [NSForegroundColorAttributeName : UIColor.red]  ////change label colour hereeee
        let str = NSAttributedString(string: dayStr, attributes: attribute)
        return str
    }
    
    //MARK: Number of items in view
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.messages.count > 0) {
            self.collectionView?.backgroundView = nil
        }
        return self.messages.count
    }
    
    
    
    
    //MARK: Data on cell
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageDataForItemAt indexPath: IndexPath) -> JSQMessageData {
        let data = self.messages[indexPath.row]
        return data
    }
    
    
    
    //MARK: After deletion of message at index path
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, didDeleteMessageAt indexPath: IndexPath) {
        
    }
    
    //MARK: To configure outgong or incomming bubble
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageBubbleImageDataForItemAt indexPath: IndexPath) -> JSQMessageBubbleImageDataSource? {
        let message:ChatMessage = messages[indexPath.item]
        return (self.senderId() == message.senderId) ? outgoingBubble : incomingBubble
    }
    
    //MARK: For Avatar Images
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, avatarImageDataForItemAt indexPath: IndexPath) -> JSQMessageAvatarImageDataSource? {
        return nil
    }
    
    //MARK: To show name above message
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        
        // Displaying names above messages
        //Mark: Removing Sender Display Name
        /**
         *  Example on showing or removing senderDisplayName based on user settings.
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         */
        return nil
    }
    
    //MARK: To show timestamp above message
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        
        /**
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         *  The other label text delegate methods should follow a similar pattern.
         *
         *  Show a timestamp for every 3rd message
         */
        
        if(indexPath.item == 0){
            let message = self.messages[indexPath.item]
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
        }
        else{
            
            let currentMessage = self.messages[indexPath.item]
            let previousMessage = self.messages[indexPath.item - 1]
            
            if compareDates(currentDate: currentMessage.date, previousDate: previousMessage.date){
                return nil
            }else{
                let message = self.messages[indexPath.item]
                return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
            }
        }
    }
    
    /// Used to get message object array as respect to the passed data.
    ///
    /// - Parameter messageData: data for the current message.
    /// - Returns: Array of Object with containg all the data of messages.
    func getMessageObject(fromData data: [String:Any], withStatus status: String, isSelf : Bool,fileSize : Double, documentData : [String : Any]?) -> [String:Any]? {
        guard let messageType = data["type"] as? String, let payload = data["payload"], let from = data["from"], let timeStamp = data["timestamp"], let id = data["id"], let to = data["to"] else { return nil }
        var params = [String : Any]()
        if let docData = documentData {
            params = docData
        }
        params["message"] = payload as Any
        params["messageType"] = messageType as Any
        params["isSelf"] = isSelf as Any
        params["from"] = from as Any
        params["to"] = to as Any
        params["timestamp"] = timeStamp as Any
        params["deliveryStatus"] = status as Any
        params["id"] = id as Any
        params["mediaState"] = 3 as Any
        params["dataSize"] = fileSize as Any
        params["thumbnailPath"] = "" as Any
        params["thumbnail"] = data["thumbnail"]
        return params
    }
    
    func compareDates(currentDate:Date, previousDate:Date)->Bool{
        let calendar = Calendar.current
        let currentYear = calendar.component(.year, from: currentDate )
        let currentMonth = calendar.component(.month, from: currentDate)
        let currentDay = calendar.component(.day, from: currentDate)
        
        let previousYear = calendar.component(.year, from: previousDate)
        let previousMonth = calendar.component(.month, from: previousDate)
        let previousDay = calendar.component(.day, from: previousDate)
        
        if currentYear == previousYear && currentMonth == previousMonth && currentDay == previousDay{
            return true
        }
        else{
            return false
        }
    }
    
    //MARK: Height of cell to show timestamp
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellTopLabelAt indexPath: IndexPath) -> CGFloat {
        
        /**
         *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
         */
        
        /**
         *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
         *  The other label height delegate methods should follow similarly
         *
         *  Show a timestamp for every 3rd message
         */
        
        if(indexPath.item == 0){
            return 25
        }
        else{
            let currentMessage = self.messages[indexPath.item]
            let previousMessage = self.messages[indexPath.item - 1]
            
            if compareDates(currentDate: currentMessage.date, previousDate: previousMessage.date){
                return 0.0
            }else{
                return 25
            }
        }
    }
    
    //MARK:To show sender name
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForMessageBubbleTopLabelAt indexPath: IndexPath) -> CGFloat {
        
        /**
         *  Example on showing or removing senderDisplayName based on user settings.
         *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
         */
        
        /**
         *  iOS7-style sender name labels
         */
        let currentMessage = self.messages[indexPath.item]
        
        if currentMessage.senderId == self.senderId() {
            return 0.0
        }
        
        if indexPath.item - 1 > 0 {
            let previousMessage = self.messages[indexPath.item - 1]
            if previousMessage.senderId == currentMessage.senderId {
                return 0.0
            }
        }
        return 25
    }
    
    //MARK: Responding to collection view tap events
    override func collectionView(_ collectionView: JSQMessagesCollectionView, didTapMessageBubbleAt indexPath: IndexPath) {
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, didTapCellAt indexPath: IndexPath, touchLocation: CGPoint) {
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
    }
    
    override func didPressSend(_ button: UIButton, withMessageText text: String, senderId: String, senderDisplayName: String, date: Date) {
        if(isMatchedUser) {
           sendMessage()
        } else {
            if Helper.isUserHasMinimumCoins(minValue: coinsFor.sendUnMatchMessage) {
                if Helper.isShowCoinsDeductPopup(popUpFor: .forChat) {
                    showCoinsPopupForChat()
                }
                else {
                    self.coinBtnPressed(sender: button)
                }
            } else {
                Helper.showAlertWithMessage(withTitle:ActionSheetButtonNames.oops, message:ActionSheetButtonNames.noCoinsForMessage, onViewController:self)
            }
        }
    }
    
    func sendMessage() {
        self.updateMessageUIAfterSendingMessage(withText:(self.inputToolbar.contentView?.textView?.text)!)
        DispatchQueue.main.async {
            self.finishSendingMessage()
        }
        
        if updateConversationStarted {
            if let hasReceiverID = self.getRecieverID(){
                matchesVm.updateChatStartedWithUser(userId:hasReceiverID )
            }
        }
        updateConversationStarted =  false
    }
    
        func showCoinsPopupForChat(){
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let syncView = spendSomeCoinPopUp(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        syncView.title = "Spend some coins for sending  message"
        syncView.loadPopUpView()
        syncView.spendSomeCoinDelegate = self
        window.addSubview(syncView)
    }
    
    func updateMessageUIAfterSendingMessage(withText text: String) {
        self.updateChatDocIDIntoMatchDB()
        let userDocVMObject = UsersDocumentViewModel(couchbase: couchbaseObj)
        guard let message = makeMessageForSendingBetweenServers(withText: text, andType: "0") else { return }
        
        if(isMatchedUser) {
             mqttChatManager.sendMessage(toChannel: "\(ChatAppConstants.MQTT.messagesTopicName)\(self.recieverID!)", withMessage: message, withQOS: .atLeastOnce)
        } else {
             self.sendMessageThroughAPI(requestParms:message)
        }
       
        guard let MsgObjForDB = self.getMessageObject(fromData: message, withStatus: "0", isSelf: true, fileSize: 0, documentData: nil) else { return }
        var msgObj = MsgObjForDB
        guard let userData = userDocVMObject.getUserData() else { return }
        msgObj["name"] = userData["userName"] as Any
        guard let dateString = DateHelper().getDateString(fromDate: Date()) else { return }
        msgObj["sentDate"] = dateString as Any
        guard let chatDocID = self.chatDocID else { return }
        self.chatsDocVMObject.updateChatDoc(withMsgObj: msgObj, toDocID: chatDocID)
        
        let messgeObj = ChatMessage(forData: msgObj, withDocID: chatDocID, andMessageobj: message, isSelfMessage: true, mediaStates: .notApplicable, mediaURL:nil, thumbnailData: nil, secretID: "", messageData:msgObj)
        messages.append(messgeObj)
    }
    
    @IBAction  func openVideoRecordAction(_ sender: Any) {
        openVideoController()
        
        //        let localUrl = URL(string:"file:///private/var/mobile/Containers/Data/Application/CE6C06E0-5373-4994-A967-CEB4F6B982A3/tmp/A967EA18-355D-4055-AA9B-3B69264F3D52.mov")
        //
        //        let remoteUtl = "http://res.cloudinary.com/deu1yq6y6/video/upload/q_30/v1520349153/20180306084230PM.mov"
        //
        //
        //        sendVideoMessage(withVideoLocalURL:localUrl as! NSURL , andMediaTypes:"2")
    }
    
    func openVideoController() {
        let videoVc = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "videoRecordVc") as! VideoRecorderViewController
        videoVc.videoUploadForEdit = self
        let newNavVc = UINavigationController.init(rootViewController: videoVc)
        newNavVc.isNavigationBarHidden = true
        videoVc.videoForChat = true
        self.present(newNavVc, animated: true, completion: nil)
    }
    
    
    
    
    
    func makeMessageForSendingBetweenServers(withText text : String, andType type: String) -> [String:Any]? {
        let chatDocumentID = self.getChatDocID()
        self.chatDocID = chatDocumentID
        self.chatViewModelObj?.chat.docID = chatDocumentID
        let userDocVMObject = UsersDocumentViewModel(couchbase: couchbaseObj)
        guard let userData = userDocVMObject.getUserData() else { return nil }
        guard let chatDocID = chatDocumentID, let receiverID = self.recieverID else { return nil }
        let timeStamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))
        var params = [String :Any]()
        params["receiverIdentifier"] = "" as Any
        params["from"] = self.senderId() as Any
        params["to"] = receiverID as Any
        params["payload"] = text.toBase64() as Any
        params["toDocId"] = chatDocID as Any
        params["timestamp"] = "\(timeStamp)" as Any
        params["id"] = "\(timeStamp)" as Any
        params["type"] = type as Any
        params["userImage"] = userData["userImageUrl"]! as Any
        params["name"] = userData["userName"]! as Any
        params["receiverIdentifier"] = userData["userName"]! as Any
        return params
    }
    
    
    /// Create a Media message for transfer it between clients.
    ///
    /// - Parameters:
    ///   - mediaData: thumbnail media data
    ///   - dataSize: Media size
    ///   - mediaUrl: Media URL after uploading it remotely
    ///   - timestamp: timestamp of the message.
    /// - Returns: Dictionary of the message.
    func makeMessageForSendingBetweenServers(withData mediaData : String?, withMediaSize dataSize:Int, andMediaURL mediaUrl: String, withtimeStamp timestamp : String?, andType type : String, documentData : [String : Any]?) -> [String:Any]? {
        let userDocVMObject = UsersDocumentViewModel(couchbase: couchbaseObj)
        guard let userData = userDocVMObject.getUserData(), let timeStamp = timestamp else { return nil }
        guard let dateString = DateHelper().getDateString(fromDate: Date()) else { return nil }
        var params = [String :Any]()
        if let documentData = documentData {
            params = documentData
        }
        params["from"] = self.senderId() as Any
        params["to"] = self.recieverID as Any
        params["payload"] = mediaUrl.toBase64() as Any
        params["toDocId"] = chatDocID as Any
        params["timestamp"] = timeStamp as Any
        params["id"] = timeStamp as Any
        params["type"] = type as Any
        params["dataSize"] = dataSize as Any
        params["userImage"] = userData["userImageUrl"]! as Any
        params["name"] = userData["userName"]! as Any
        params["receiverIdentifier"] = userData["userName"]! as Any
        params["sentDate"] = dateString as Any
        if let thumbnailData = mediaData {
            params["thumbnail"] = thumbnailData as Any
        }
        return params
    }
    
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, header headerView: JSQMessagesLoadEarlierHeaderView, didTapLoadEarlierMessagesButton sender: UIButton) {
    }
    
    override func didPressAccessoryButton(_ sender: UIButton) {
        
        self.inputToolbar.contentView!.textView!.resignFirstResponder()
        
        let sheet = UIAlertController(title: "Media messages", message: nil, preferredStyle: .actionSheet)
        
        let photoAction = UIAlertAction(title: "Photo Library", style: .default) { (action : UIAlertAction) in
            /**
             *  Opening gallery to select image
             */
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.mediaTypes = ["public.image"]
            self.present(imagePicker, animated: true, completion: nil)
            
        }
        
        let locationAction = UIAlertAction(title: "Send Location", style: .default) { (action : UIAlertAction) in
            /**
             *  Add fake location
             */
            let locationItem = self.buildLocationItem()
            
            self.addMedia(media: locationItem)
            // self.performSegue(withIdentifier: Constants.toLocationPicker, sender: nil)
        }
        
        
        //        let audioAction = UIAlertAction(title: "Send Audio", style: .default) { (action : UIAlertAction) in
        //            /**
        //             *  Add fake audio
        //             */
        //            let audioItem = self.buildAudioItem()
        //
        //            self.addMedia(media: audioItem)
        //        }
        //
        let cameraActrion = UIAlertAction(title: "Open camera", style: .default) {(action: UIAlertAction) in
            if(!UIImagePickerController.isSourceTypeAvailable(.camera)){
                
                let alert = UIAlertController.init(title: "Message", message: "Camera is not available in your device", preferredStyle: .alert)
                let action = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        sheet.addAction(photoAction)
        //        sheet.addAction(videoAction)
        sheet.addAction(cancelAction)
        sheet.addAction(cameraActrion)
        //        sheet.addAction(audioAction)
        sheet.addAction(locationAction)
        
        self.present(sheet, animated: true, completion: nil)
    }
}

extension ChatViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let mediaType = info["UIImagePickerControllerMediaType"] as? String else { return }
        
        if(mediaType == "public.image"){
            guard let imageToSend = info[UIImagePickerControllerOriginalImage] as? UIImage else { return }
            let photoItem = JSQPhotoMediaItem(image: imageToSend)
            self.addMedia(media: photoItem)
            let name  = arc4random_uniform(900000) + 100000;
            let timeStamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))
            AFWrapper.updloadPhoto(withPhoto: imageToSend, andName: "\(name)\(timeStamp)", success: { (response) in
                let fileName = "\(name)\(timeStamp).jpeg"
                let url = "http://45.55.223.151:8009/YeloChat/profilePics/\(fileName)"
                self.sendImageMessage(withImage: imageToSend, andUploadedUrl: url)
            }, failure: { (error) in
            })
            DispatchQueue.main.async {
                self.finishSendingMessage()
            }
            self.dismiss(animated: true, completion: nil)
        }
            
        else{
            guard  let videoURL = info["UIImagePickerControllerMediaURL"] as? NSURL else { return }
            let videoItem = self.buildVideoItem(videoURL: videoURL)
            self.addMedia(media: videoItem)
            let strURL = ChatAppConstants.constantURL + ChatAppConstants.uploadPhoto
            AFWrapper.updateVideoByUsingMultipart(serviceName: strURL, videoURL: videoURL, success: { (response) in
            }, failure: { (error) in
            })
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func sendImageMessage(withImage image: UIImage, andUploadedUrl imageURL : String) {
        
    }
    
    
    
    func createThumbnail(forImage image : UIImage) -> String?{
        // Define thumbnail size
        let size = CGSize(width: 70, height: 70)
        
        // Define rect for thumbnail
        let scale = max(size.width/image.size.width, size.height/image.size.height)
        let width = image.size.width * scale
        let height = image.size.height * scale
        let x = (size.width - width) / CGFloat(2)
        let y = (size.height - height) / CGFloat(2)
        let thumbnailRect = CGRect(x: x, y: y, width: width, height: height)
        
        // Generate thumbnail from image
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        image.draw(in: thumbnailRect)
        guard let thumbnail = UIGraphicsGetImageFromCurrentImageContext() else { return  nil }
        UIGraphicsEndImageContext()
        let imageData = ImageExtension.convertImageToBase64(image: thumbnail)
        return imageData
    }
    
    func buildVideoItem(videoURL : NSURL) -> JSQVideoMediaItem {
        let videoItem = JSQVideoMediaItem(fileURL: videoURL as URL, isReadyToPlay: true, thumbnailImage: thumbnail(sourceURL: videoURL))
        return videoItem
    }
    
    //    func thumbnail(sourceURL:NSURL) -> UIImage {
    //        let asset = AVAsset(url: sourceURL as URL)
    //        let imageGenerator = AVAssetImageGenerator(asset: asset)
    //        let time = CMTime(seconds: 5, preferredTimescale: 1)
    //
    //        do {
    //            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
    //            return UIImage(cgImage: imageRef)
    //        } catch {
    //            return UIImage(named: "play")!
    //        }
    //    }
    
    func getUserDetails() -> [String:Any] {
        let params = [String : Any]()
        return params
    }
    
    func addMedia(media:JSQMediaItem) {
        let messageId = NSDate.timeIntervalSinceReferenceDate
        
        //       let message = ChatMessage(withSenderID: self.senderId(), andRecieverID: "123", withPayload: "", messageDocId: "234", timeStamp: "3244", messageType: MessageTypes.image, messangerName: "sachin", messageSentDate: Date(), messageId:"\(messageId)", media :media, isMediaAvailable: true, messageStatus: "2", isSelfMessage: true)
        //
        //        self.messages.append(message)
        //        DispatchQueue.main.async {
        //            self.finishSendingMessage(animated: true)
        //        }
    }
    
    func buildLocationItem() -> JSQLocationMediaItem {
        let ferryBuildingInSF = CLLocation(latitude: 37.795313, longitude: -122.393757)
        
        let locationItem = JSQLocationMediaItem()
        locationItem.setLocation(ferryBuildingInSF) {
            self.collectionView!.reloadData()
        }
        return locationItem
    }
    
    func buildAudioItem() -> JSQAudioMediaItem {
        let sample = Bundle.main.path(forResource: "jsq_messages_sample", ofType: "m4a")
        let audioData = NSData(contentsOf: NSURL.fileURL(withPath: sample!))
        let audioItem = JSQAudioMediaItem(data: audioData! as Data)
        return audioItem
    }
}

extension ChatViewController {
    
    func receivedMessage(notification: NSNotification) {
        let userInfo = notification.userInfo as! [String: Any]
        let status = userInfo["status"] as! String
        
        switch status {
        case "1","2", "3" :
            self.chatsDocVMObject.updateStatusForChatMessages(withStatus: status, chatDocID: self.getChatDocID())
            self.updateChatStatus()
        default:
            guard let content = userInfo["message"] as? [String : Any] else { return }
            self.updateCurrentChatMessages(withmessage: content)
        }
    }
    
    func receivedLastSeenStatus(notification: NSNotification) {
        let userInfo = notification.userInfo as! [String: Any]
        guard let message = userInfo["message"] as? [String : Any] else { return }
        if ((message["userId"] as? String) == self.recieverID) {
            if ((message["status"] as? Int) == 1) { // online
                self.currentChatStatusLabelOutlet.text = "Online"
            } else if ((message["status"] as? Int) == 0), let timeStamp = message["timestamp"] as? String { //offline
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                    guard let date = DateHelper().getDate(forLastSeenTimeString: timeStamp) else { return }
                    let timeStampTime = "last seen \(DateHelper().lastSeenTime(date: date))"
                    self.currentChatStatusLabelOutlet.text = timeStampTime
                })
            }
        }
    }
    
    func recievedPostDetailsUpdate(notification: NSNotification) {
        let chatsDocVMObj = ChatsDocumentViewModel(couchbase: self.couchbaseObj)
        if let chatObject = chatsDocVMObj.getChatObject(forChatDocID: self.chatViewModelObj?.docID, chatUserID: self.chatViewModelObj?.userID) {
            self.chatViewModelObj = ChatViewModel(withChatData: chatObject)
        }
    }
    
    
    
    
    func receivedTypingStatus(notification: NSNotification) {
        let userInfo = notification.userInfo as! [String: Any]
        guard let message = userInfo["message"] as? [String : Any] else { return }
        if ((message["from"] as? String) == self.recieverID) {
            self.currentChatStatusLabelOutlet.text = "Typing..."
            if !isTypingVisible {
                self.isTypingVisible = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.isTypingVisible = false
                    self.currentChatStatusLabelOutlet.text = "Online"
                })
            }
        }
    }
    
    func didRecieve(withMessage message: Any, inTopic topic: String) {
        guard let msgServerObj = message as? [String:Any] else { return }
        if let fromID = msgServerObj["from"] as? String {
            if fromID == selfID! {
            } else {
                guard let chatDocID = self.chatDocID else { return }
                self.messages = chatsDocVMObject.getMessagesFromChatDoc(withChatDocID: chatDocID)
                self.finishReceivingMessage()
            }
        }
    }
}

extension ChatViewController {
    
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        if textView.text.count > 0 {
            camerabtn?.isHidden = true
        } else {
            camerabtn?.isHidden = false
        }
        self.sendTypingStatus()
    }
}

extension ChatViewController {
    func updateCurrentChatMessages(withmessage message: [String : Any]) {
        let state = UIApplication.shared.applicationState
        if state == .background {
            return
        }
        self.chatsDocVMObject.updateMessageStatus(withMessageObj: message)
        
        guard let chatDocID = self.chatDocID else { return }
        self.messages = self.chatsDocVMObject.getMessagesFromChatDoc(withChatDocID: chatDocID)
        self.finishReceivingMessage()
        self.sendReadAcknowledgment(withMessage: message)
    }
    
    func sendReadAcknowledgment(withMessage message : [String : Any]) {
        guard var params = self.chatsDocVMObject.getMessageObjectForUpdatingStatus(withData: message, andStatus: "3") as? [String:Any] else { return }
        let timestamp = Int(Date().timeIntervalSince1970)
        params["userRecivedTime"] = timestamp
        mqttChatManager.sendAcknowledgment(toChannel: "\(ChatAppConstants.MQTT.acknowledgementTopicName)\(self.recieverID!)", withMessage: params, withQOS: .atMostOnce)
    }
    
    func updateChatDocIDIntoMatchDB() {
        //        if let userID = matchObjec?.userId {
        //            favoriteViewModel.updateContactDoc(withUserID: userID, andChatDocId: self.chatDocID)
        //        } else if let userID = self.chatViewModelObj?.userID {
        //            favoriteViewModel.updateContactDoc(withUserID: userID, andChatDocId: self.chatDocID)
        //        }
    }
    
    func openVideoForUrl(videoUrl:String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if let videoURL = URL(string: videoUrl) {
                let player = AVPlayer(url: videoURL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    if #available(iOS 10.0, *) {
                        playerViewController.player!.playImmediately(atRate: 1.0)
                    } else {
                        playerViewController.player!.play()
                    }
                }
            }
        }
    }
    
    func updateChatForReadMessage(toDocID docID : String,isControllerAppearing : Bool) {
        var chatData = couchbaseObj.getData(fromDocID: docID)!
        if ((chatData["hasNewMessage"] as? Bool) == true){
            // here we are sending the recieved acknowledgment for last message.
            if isControllerAppearing{
                self.fetchLastMessageFromSenderAndSendAcknowledgment(withChatData: chatData)
            }
        }
        chatData["hasNewMessage"] = false
        chatData["newMessageCount"] = 0
        couchbaseObj.updateData(data: chatData, toDocID: docID)
    }
    
    
    func fetchLastMessageFromSenderAndSendAcknowledgment(withChatData chatData: [String:Any]) {
        let msgArray = chatData["messageArray"] as! [[String:Any]]
        for (_, messageData) in msgArray.enumerated() {
            if ((messageData["timestamp"] as? String) == (chatData["lastMessageDate"] as? String)){
                self.sendReadAcknowledgment(withMessage: messageData)
            }
        }
    }
    
    func updateChatStatus() {
        guard let chatDocID = self.chatDocID else {  return }
        self.messages = self.chatsDocVMObject.getMessagesFromChatDoc(withChatDocID: chatDocID)
        self.finishReceivingMessage()
    }
}

func == (lhs: ChatMessage, rhs: ChatMessage) -> Bool {
    guard let lhsTimeStamp = lhs.uniquemessageId, let rhsTimeStamp = rhs.uniquemessageId else { return false }
    return (lhsTimeStamp == rhsTimeStamp)
}

extension Sequence where Iterator.Element: Hashable {
    func uniq() -> [Iterator.Element] {
        var seen = Set<Iterator.Element>()
        return filter { seen.update(with: $0) == nil }
    }
}

extension ChatViewController:videoUploadForEditProifleDelegate {
    func videoUploadedSuccessfully(uploadedVideoUrl: (String, URL)) {
        
        print("recorded Vidoe Remote Url:%@",uploadedVideoUrl.0)
        print("recorded Vidoe Local Url:%@",uploadedVideoUrl.1)
        
        sendVideoMessage(withVideoLocalURL: uploadedVideoUrl.1 as NSURL, andMediaTypes: "2",remoteUrl:uploadedVideoUrl.0)
    }
    
    func sendVideoMessage(withVideoLocalURL videoURL: NSURL, andMediaTypes type: String,remoteUrl:String) {
        
        // Creating image message with thumbnail Image.
        guard let thumbNailImage = self.chatViewModelObj?.thumbnail(sourceURL: videoURL) else { return }
        guard let thumbnailData = self.chatViewModelObj?.createThumbnail(forImage: thumbNailImage) else { return }
        guard let videoSize = self.chatViewModelObj?.getVideoSize(withURL: videoURL) else { return }
        
        guard let videoMsgObj = self.chatViewModelObj?.getVideoMessageObj(withVideoURL: videoURL, isSelf: true, thumbnailData: thumbnailData, withMediaType: type,remoteUrl:remoteUrl) else { return }
        
        guard let msgObj = self.makeMessageForSendingBetweenServers(withData: thumbnailData, withMediaSize: Int(videoSize), andMediaURL: remoteUrl, withtimeStamp: videoMsgObj.timeStamp, andType: type, documentData : nil) else { return }
        
        let videoMsgVMobj = VideoMessageViewModal(withMessage: videoMsgObj)
        videoMsgVMobj.createVideoMessageObject(withVideo: remoteUrl)
        
        self.messages.append(videoMsgObj)
        //        //Store message to DB.
        
        guard let MsgObjForDB = self.getMessageObject(fromData: msgObj, withStatus: "0", isSelf: true, fileSize: Double(videoSize), documentData: nil) else { return }
        self.chatsDocVMObject.updateChatDoc(withMsgObj: MsgObjForDB, toDocID: chatDocID!)
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.finishSendingMessage(animated: true)
        }
    }
    
    func videoUploadFailed() {
        print("failed to upload")
    }
}

extension ChatViewController:VideoCellTappedDelegate {
    //MARK:- Video Cell Tapped Delegate
    func videoCellTapped(withVideoMVModal videoMVModalObect: VideoMessageViewModal) {
        self.openVideoForUrl(videoUrl: videoMVModalObect.message.messagePayload!)
    }
}

extension ChatViewController:spendSomeCoinPopUpDelegate {
   
    func coinBtnPressed(sender: UIButton) {
        
        let title = sender.title(for: .normal)
        if title == StringConstants.BuyCoins {
            let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "addCoin") as! AddCoinsVC
            userBasics.navigationController?.setNavigationBarHidden(true, animated: false)
            self.navigationController?.pushViewController(userBasics, animated: true)
            return
        }
        
        AudioHelper.sharedInstance.playCoinsSound()
        sendMessage()
        if updateInUnMatchedUsers {
            updateUnmatchesChatUsers()
        }
    }
    
    
    
    
    func updateUnmatchesChatUsers() {
        if var existingUnMatchUsers = UserDefaults.standard.string(forKey:SyUserdefaultKeys.unmatchedChatUsers){
            if var receiverId = getRecieverID() {
                if !existingUnMatchUsers.contains(receiverId) {
                    receiverId.append(",")
                    existingUnMatchUsers.append(receiverId)
                    UserDefaults.standard.set(existingUnMatchUsers, forKey:SyUserdefaultKeys.unmatchedChatUsers)
                    UserDefaults.standard.synchronize()
                }
            }
        } else {
            if let receiverId = getRecieverID() {
                UserDefaults.standard.set(receiverId, forKey:SyUserdefaultKeys.unmatchedChatUsers)
                UserDefaults.standard.synchronize()
            }
        }
    }
}

extension ChatViewController: DatePopUpDelegate {
    func VoiceCallDateBtnPressed() {
        
    }
    
   
    func canceled() {
        self.inputToolbar.isHidden = false
    }
    
    func sendMessageThroughAPI(requestParms:[String:Any]) {
        
        Helper.showProgressIndicator(withMessage:progressIndicatorMessage.sendingMsg)
        
        API.requestPOSTURL(serviceName: APINAMES.sendMsgWithOutMatch,
                           withStaticAccessToken:false,
                           params: requestParms,
                           success: { (response) in
                            print("response:")
                            Helper.hideProgressIndicator()
        },
                           failure: { (Error) in
                            self.view.showErrorMessage(titleMsg: Message.Error, andMessage: Error.localizedDescription)
                            Helper.hideProgressIndicator()
        })
    }
    
    
    func VideoCallDateBtnPressed() {
        
        AudioHelper.sharedInstance.playCoinsSound()
        
        let currentDate = Date()
        let currentTimeStamp = currentDate.millisecondsSince1970
        
        let requestParms = [likeProfileParams.targetUserId:self.getRecieverID()!,
                            likeProfileParams.syncDate:currentTimeStamp,
                            likeProfileParams.dateType:"2"
            ] as [String : Any]
        
        Helper.showProgressIndicator(withMessage:progressIndicatorMessage.creatingDate)
        
        API.requestPOSTURL(serviceName: APINAMES.setSyncDate,
                           withStaticAccessToken:false,
                           params: requestParms,
                           success: { (response) in
                            Helper.hideProgressIndicator()
                            if (response.null != nil){
                                return
                            }
                            
                            let statuscode = response.dictionaryObject!["code"] as! Int
                            switch (statuscode) {
                            case 200:
                                let when = DispatchTime.now() + 0.5
                                DispatchQueue.main.asyncAfter(deadline: when){
                                    self.startVideoCall(profileID:self.getRecieverID()!,dateIdForCall:response.dictionaryObject!["date_id"] as! String)
//                                    self.removeObserer()
                                    self.navigationController?.dismiss(animated: false, completion: nil)
                                }
                            default:
                                self.view.showErrorMessage(titleMsg: Message.Error, andMessage: response.dictionaryObject![LoginResponse.errorMessage] as! String)
                            }
                            
        },
                           failure: { (Error) in
                            self.view.showErrorMessage(titleMsg: Message.Error, andMessage: Error.localizedDescription)
                            Helper.hideProgressIndicator()
        })
    }
    
    func physicalDateBtnPressed() {
        gotoRescheduleVC()
    }
    
    
    func gotoRescheduleVC(){
        
        let matchedProfile = MatchedProfile(matchedUserDetails:[:])
        matchedProfile.userId = getRecieverID()!
        matchedProfile.firstLikedByMe = false
        matchedProfile.name = getReceiverName()!
        matchedProfile.profilePic = getProfilePic()!
        
        
        let vc = UIStoryboard(name:"TabBarControllers", bundle: nil).instantiateViewController(withIdentifier: "planDate") as! ReScheduleVC
        vc.isForCallDate = false
        vc.matchedProfile = matchedProfile
        vc.isFromMatch = true
        vc.dateFromChat = true
        let preferenceNav: UINavigationController =  UINavigationController(rootViewController: vc)
        preferenceNav.isNavigationBarHidden = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- SYNC DATE ALERT DELAGATES.

extension ChatViewController {
    
    
    /// method for intiate video call.
    ///
    /// - Parameter data: data contains matched profile details.
    func startVideoCall(profileID:String,dateIdForCall:String) {
        
        guard let ownID = Helper.getMQTTID() else { return }
        MQTTCallManager.sendcallAvilibilityStatus(status: 0, topic: MQTTTopic.callsAvailability + ownID)
        UserDefaults.standard.set(true, forKey: "iscallBtnCliked")
        
        let userID = profileID
        let registerNum = ""
        let dict = ["callerId": userID,
                    "callType" : CallTypes.videoCall ,
                    "registerNum": registerNum,
                    "callId": randomString(length: 100),
                    "callerIdentifier": ownID,
                    "dateId":dateIdForCall] as [String:Any]
        
        UserDefaults.standard.set(dict, forKey: "storeIndexPath")
        MQTT.sharedInstance.subscribeTopic(withTopicName: MQTTTopic.callsAvailability + userID , withDelivering: .atLeastOnce)
        
        //for VideoCall
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = IncomingVideocallView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.tag =  17
        videoView.setCallId()
        videoView.otherCallerId = userID
        videoView.calling_userName.text = String(format:"%@", self.getReceiverName()!)
        videoView.userImageView.kf.setImage(with: URL(string: (self.getProfilePic()!)), placeholder: UIImage(), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
        })
        
        videoView.addCameraView()
        window.addSubview(videoView)
    }
    
}


extension ChatViewController {
    /// method for unmatch user.
    ///
    /// - Parameter userDetails: contains profile details.
    func unmatchUser() {
        
        let params = [likeProfileParams.targetUserId:getRecieverID()!]

        Helper.showProgressIndicator(withMessage:progressIndicatorMessage.unMatch)

        API.requestPOSTURL(serviceName: APINAMES.UnMatchProfile,
                           withStaticAccessToken:false,
                           params: params,
                           success: { (response) in
                            Helper.hideProgressIndicator()
                            if (response.null != nil){
                                return
                            }

                            let statuscode = response.dictionaryObject!["code"] as! Int
                            switch (statuscode) {
                            case 200:
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue:"unmatchedUser"), object: nil)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                                    Helper.hideProgressIndicator()
                                    self.moveToBack()
                                })
                            default:
                                Helper.hideProgressIndicator()
                                self.view.showErrorMessage(titleMsg: Message.Error, andMessage: response.dictionaryObject![LoginResponse.errorMessage] as! String)
                            }

        },
                           failure: { (Error) in

                            Helper.hideProgressIndicator()

                            self.view.showErrorMessage(titleMsg: Message.Error, andMessage: Error.localizedDescription)
                            Helper.hideProgressIndicator()
        })
    }
    
    
    
    /// method for blocking user
    ///
    /// - Parameter userProfile: selected user profile to block.
    func requestForBlockUser() {
        let params = [likeProfileParams.targetUserId:getRecieverID()!]
        Helper.showProgressIndicator(withMessage:progressIndicatorMessage.blockUser)
        
        
        API.requestPatchURL(serviceName: APINAMES.blockProfile,
                            withStaticAccessToken:false,
                            params: params,
                            success: { (response) in
                                Helper.hideProgressIndicator()
                                if (response.null != nil){
                                    return
                                }
                                
                                let statuscode = response.dictionaryObject!["code"] as! Int
                                switch (statuscode) {
                                case 200:
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue:"unmatchedUser"), object: nil)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                                        Helper.hideProgressIndicator()
                                        self.moveToBack()
                                    })
                                default:
                                    self.view.showErrorMessage(titleMsg: Message.Error, andMessage: response.dictionaryObject![LoginResponse.errorMessage] as! String)
                                }
                                
        },
                            failure: { (Error) in
                                
                                Helper.hideProgressIndicator()
                                
                                self.view.showErrorMessage(titleMsg: Message.Error, andMessage: Error.localizedDescription)
                                Helper.hideProgressIndicator()
        })
    }
    
    func openAlert() {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        if  let receiverName = getReceiverName() {
        
        var alertTitle = String(format:"View %@'s profile",receiverName)
        
        alert.addAction(UIAlertAction(title: alertTitle, style: .default, handler: {(action:UIAlertAction!) in
            self.openReceiverProfile()
        }))
            
        if isMatchedUser {
            alertTitle = String(format:"Unmatch %@",receiverName)
            
            alert.addAction(UIAlertAction(title: alertTitle, style: .default, handler: {(action:UIAlertAction!) in
                self.showConfirmUnMatchAlert()
            }))
        }
            
        alertTitle = String(format:"Report %@",receiverName)
        
        
        alert.addAction(UIAlertAction(title: alertTitle, style: .default, handler: {(action:UIAlertAction!) in
            
            if(self.matchesVm.reportReasonsArray.count == 0) {
                self.requestForReportUserReasons(userClicked: true)
            } else {
                self.showReportReasons()
            }
        }))
        
        alertTitle = String(format:"Block %@",receiverName)
        
        alert.addAction(UIAlertAction(title: alertTitle, style: .default, handler: {(action:UIAlertAction!) in
            self.showConfirmBlockAlert()
        }))
        
        
        alert.addAction(UIAlertAction(title: ActionSheetButtonNames.actionSheetCancelText, style: .cancel, handler: {(_) in }))
        
        self.present(alert, animated: true, completion: nil)
        
        }
    }
    
    
    func showConfirmUnMatchAlert() {
        let alert = UIAlertController(title: nil, message: ActionSheetButtonNames.confirmUnmatch, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: ActionSheetButtonNames.actionSheetCancelText, style: .cancel, handler: {(_) in }))
        
        alert.addAction(UIAlertAction(title: ActionSheetButtonNames.unamtch, style: .default, handler: {(action:UIAlertAction!) in
             self.unmatchUser()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showConfirmBlockAlert() {
         let alert = UIAlertController(title: nil, message: ActionSheetButtonNames.confirmBlock, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: ActionSheetButtonNames.actionSheetCancelText, style: .cancel, handler: {(_) in }))
        
        alert.addAction(UIAlertAction(title: ActionSheetButtonNames.yesBlock, style: .default, handler: {(action:UIAlertAction!) in
            self.requestForBlockUser()
        }))
        
         self.present(alert, animated: true, completion: nil)
    }
    
    
    /// method for requesting second look profiles.
    func requestForReportUserReasons(userClicked:Bool) {
        matchesVm.requestToGetReportReasons(userClicked:userClicked)
        matchesVm.unMatchSubjectResponse
            .subscribe(onNext: {response in
                if (userClicked) {
                    //show reasons popup
                    if(self.matchesVm.reportReasonsArray.count > 0) {
                        self.showReportReasons()
                    }
                }
            }, onError:{ error in
                
            })
            
            .disposed(by:disposeBag)
    }
    

    /// report user button action
    ///
    /// - Parameter userDetails: contains the profile details.
    func reportedUser() {
        Helper.showAlertWithMessage(withTitle: ActionSheetButtonNames.reportThanks, message:ActionSheetButtonNames.reportFeedBack , onViewController: self)
    }
    
    
    /// to fetch list of all the report reasons.
    ///
    /// - Parameter selectedUser: selected profile to report.
    func showReportReasons() {
        
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = DeactivateAccountReasonsView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.isViewForReason = true
        videoView.loadDeactivateView()
        videoView.reportReasons = self.matchesVm.reportReasonsArray
        videoView.deactDelegate = self
        window.addSubview(videoView)
    }
    
    
    /// reporting user.
    ///
    /// - Parameters:
    ///   - reason: reson for reporting
    ///   - userId: user id to report
    func reportUserForReason(reason:String,userId:String) {
        let params = [likeProfileParams.targetUserId:userId,
                      likeProfileParams.message:"reporting",
                      likeProfileParams.reason:reason]
        
        
        
        API.requestPOSTURL(serviceName: APINAMES.reportUser,
                           withStaticAccessToken:false,
                           params: params,
                           success: { (response) in
                            Helper.hideProgressIndicator()
                            if (response.null != nil){
                                return
                            }
                            
                            let statuscode = response.dictionaryObject!["code"] as! Int
                            switch (statuscode) {
                            case 200:
                                self.reportedUser()
                            default:
                                self.view.showErrorMessage(titleMsg: Message.Error, andMessage: response.dictionaryObject![LoginResponse.errorMessage] as! String)
                            }
                            
        },
                           failure: { (Error) in
                            
                            Helper.hideProgressIndicator()
                            
                            self.view.showErrorMessage(titleMsg: Message.Error, andMessage: Error.localizedDescription)
                            Helper.hideProgressIndicator()
        })
    }
}

extension ChatViewController: deactivateReasonsDelegate {
    func userSelectedDoneButton(selectedReason: String) {
        print(selectedReason)
    }
    
    func userSelectedReasonForReport(selectedReason: ReportReason) {
        self.inputToolbar.isHidden = false
        self.reportUserForReason(reason: selectedReason.nameOfTheReason, userId:getRecieverID()!)
    }
}


extension ChatViewController {
    func showAttachement() {
        self.inputToolbar.contentView!.textView!.resignFirstResponder()
        let sheet = UIAlertController(title: nil , message: nil, preferredStyle: .actionSheet)
        let photoAction = UIAlertAction(title: chatActionSheet.camera, style: .default) { (action : UIAlertAction) in
            // self.openLivecamera()
        }
        
        let photolibAction = UIAlertAction(title: chatActionSheet.camerandVideo, style: .default) { (action : UIAlertAction) in
            
            //            let imagePicker = ImagePickerViewModel(self)
            //            imagePicker.openIphoneLibrery(complition: { imgArr in
            //                let story = UIStoryboard.init(name: "Main", bundle: nil)
            //                let preview =  story.instantiateViewController(withIdentifier: "ImagePickerPreview") as? ImagePickepreviewController
            //                preview?.imageArray = imgArr
            //                preview?.delegate = self
            //                preview?.isComingFromCamera = false
            //                imagePicker.pickerView?.present(preview!, animated: true, completion: nil)
            //            })
        }
        
        let giphyAction = UIAlertAction.init(title: chatActionSheet.giphy, style: .default) { action  in
            // self.performSegue(withIdentifier: "chatTogifsegue", sender: true)
        }
        
        let stickerAction = UIAlertAction.init(title: chatActionSheet.sticker, style: .default) { action  in
            //self.performSegue(withIdentifier: "chatTogifsegue", sender: false)
        }
        
        let doodleAction = UIAlertAction.init(title: chatActionSheet.doodle, style: .default) { action  in
            //            self.doodleViewModel = DoodleViewModel.init(self)
            //            self.doodleViewModel?.OpenDoodleView()
        }
        
        let document = UIAlertAction.init(title: chatActionSheet.document , style: .default) { action in
            //            self.documentViewModel = DocumentViewModel.init(self)
            //            self.documentViewModel?.delegate = self
            //            self.documentViewModel?.openDocumentView()
        }
        
        let locationAction = UIAlertAction(title: chatActionSheet.location, style: .default) { (action : UIAlertAction) in
            //            self.locationViewModel = LocationPickerViewModel.init(self)
            //            self.locationViewModel?.delegate = self
            //            self.locationViewModel?.openLocationPicker()
        }
        
        let contactAction = UIAlertAction(title: chatActionSheet.contact, style: .default) { (action : UIAlertAction) in
            //            self.contactModel = ContactchatViewModel(self)
            //            self.contactModel?.delegate = self
            //            self.contactModel?.openContectView()
        }
        
        let cancelAction = UIAlertAction(title: ActionSheetButtonNames.actionSheetCancelText, style: .cancel, handler: nil)
        sheet.addAction(photoAction)
        sheet.addAction(photolibAction)
        sheet.addAction(cancelAction)
        sheet.addAction(locationAction)
        sheet.addAction(contactAction)
        sheet.addAction(giphyAction)
        sheet.addAction(doodleAction)
        sheet.addAction(stickerAction)
        sheet.addAction(document)
        
        self.present(sheet, animated: true, completion: nil)
    }
}


extension ChatViewController {
    //ADD CameraButton
    func addCameraBtn() {
        if camerabtn == nil {
            if self.view.frame.size.width == 320 {
                camerabtn = UIButton.init(frame:CGRect.init(x: (self.inputToolbar.contentView?.textView?.frame.size.width)! + (self.inputToolbar.contentView?.textView?.frame.origin.x)! - 70 , y: 0, width: 35, height: 35))
            }
            else if (self.view.frame.size.width == 414){
                camerabtn = UIButton.init(frame:CGRect.init(x: (self.inputToolbar.contentView?.textView?.frame.size.width)! + (self.inputToolbar.contentView?.textView?.frame.origin.x)! + 45 , y: -2, width: 35, height: 35))
            }
            else {
                camerabtn = UIButton.init(frame:CGRect.init(x: (self.inputToolbar.contentView?.textView?.frame.size.width)! + (self.inputToolbar.contentView?.textView?.frame.origin.x)! - 10 , y: -2, width: 35, height: 35))
            }
            camerabtn?.setImage(#imageLiteral(resourceName: "cameraOff"), for: .normal)
            self.inputToolbar.contentView?.textView?.addSubview(camerabtn!)
        }
    }
}
