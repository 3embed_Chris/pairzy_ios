//
//  ContactInfoTableViewController.swift
//  MQTT Chat Module
//
//  Created by Imma Web Pvt Ltd on 29/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import ParallaxHeader
import Kingfisher
import Locksmith

class ContactInfoTableViewController: UITableViewController {
    weak var headerImageView: UIView?
    var headImgView:UIImageView?
    var othercellArr = [StringConstants.clearChat(),StringConstants.blockContact(),StringConstants.reportSpam()]
    var newContactArr : [String]?
    
    var userName: String?
    var userImage: String?
    var userRegisterNum : String?
    var userID : String?
    var userStatus: String?
    var chatDocID: String?
    var favoriteObj : Profile?
    var chatVMObj : ChatViewModel?
    var isBlockUser =  false
    var chatsDocVMObject : ChatsDocumentViewModel?
    var isComingFromHistory = false
    var clearChatBlock:((Bool) -> ())?
    var blockUser:((Bool) -> ())?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if userName == userRegisterNum {
            newContactArr = [StringConstants.createNewContact(),StringConstants.contactDetails()]
        }else {
            newContactArr = [StringConstants.contactDetails()] }
        
        setupParallaxHeader()
        
        if  isComingFromHistory {
            isBlockUser = getIsUserBlock()
        }else {
            userStatus = getStatusFromDB(num: userRegisterNum)
        }
        
        self.imageRemoveForBlockUser()
    }
    
    
    private func getStatusFromDB(num: String?) -> String{
        return AppConstant.defaultStatus
    }
    
    fileprivate func imageRemoveForBlockUser(){
        
        if isBlockUser {
            self.headImgView?.image = #imageLiteral(resourceName: "defaultImage")
            self.headerImageView = self.headImgView
        }else {
            self.headImgView?.kf.setImage(with: URL(string: userImage!), placeholder: #imageLiteral(resourceName: "defaultImage"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
            })
            self.headerImageView = self.headImgView
            
        }
    }
    
    private func getIsUserBlock() -> Bool {
        if let chatViewModelObj = chatVMObj {
            return chatViewModelObj.isUserBlock
        }
        return false
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    private func setupParallaxHeader() {

        headImgView = UIImageView()
        headImgView?.image = #imageLiteral(resourceName: "defaultImage")
        headImgView?.kf.setImage(with: URL(string: userImage!), placeholder: #imageLiteral(resourceName: "arrow-point-to-right"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
        })
        
        headImgView?.contentMode = .scaleAspectFill
        headerImageView = headImgView
        tableView.parallaxHeader.view = headImgView!
        tableView.parallaxHeader.height = 400
        tableView.parallaxHeader.minimumHeight = 0
        tableView.parallaxHeader.mode = .fill
        tableView.parallaxHeader.parallaxHeaderDidScrollHandler = { parallaxHeader in
            
            //print("hello \(parallaxHeader)")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ////Clear Chat
    func openClearChatActionsheet(){
//        let actionSheet = Helper.initActionSheet(title: nil, message: "Delete messages")
//        let action1 = UIAlertAction.init(title: "Delete all messages", style: .destructive, handler: {action in
//
//             Helper.showProgressIndicator(withMessage:"")
//
//            //call API for delete All Messages
//            guard let keyDict = Locksmith.loadDataForUserAccount(userAccount: AppConstant.keyChainAccount) else {return}
//            guard  let token = keyDict["token"] as? String  else {return}
//            let headerParams = ["authorization":AuthorizationHeader.getAuthHeader(staticToken:false),"token":token]
//            let url = AppConstant.getMessages + "/" + "true/\(String(describing: self.userID!))"
//            let apiCall = RxAlmofireClass()
//            apiCall.newtworkRequestAPIcall(serviceName: url, requestType: .delete, parameters:nil,headerParams:headerParams, responseType: AppConstants.resposeType.deleteMessage.rawValue)
//            apiCall.subject_response
//                .subscribe(onNext: {dict in
//
//                    guard let responseKey = dict[AppConstants.resposeTypeKey] as? String else {return}
//                    if responseKey == AppConstants.resposeType.deleteMessage.rawValue{
//                        let chatDocId = self.getChatDocumentID()
//                        self.chatsDocVMObject?.clearMessagesInChatDoc(toDocID: chatDocId)
//                        self.clearChatBlock?(true)
//
//
//                         //go back after delete messages
//                        DispatchQueue.main.async {
//                             Helper.hidePI()
//                            if self.isComingFromHistory == false {
//                                self.navigationController?.popViewController(animated: true)
//                            }
//                        }
//
//
//                    }
//                }, onError: {error in
//                    DispatchQueue.main.async {
//                       Helper.hidePI()
//                    }
//                })
//        })
//        actionSheet.addAction(action1)
//        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    //Report Spam
    func openReportSpamActionsheet(){
//        let actionSheet = Helper.initActionSheet(title: nil, message: "Report spam this contact?")
//        let action1 = UIAlertAction.init(title: "Report Spam", style: .destructive, handler: {action in
//        
//            Helper.showProgressIndicator(withMessage: "Loading...")
//            //call API for delete All Messages
//            guard let keyDict = Locksmith.loadDataForUserAccount(userAccount: AppConstant.keyChainAccount) else {return}
//            guard  let token = keyDict["token"] as? String  else {return}
//            let headerParams = ["authorization":AuthorizationHeader.getAuthHeader(staticToken:false),"token":token]
//            let url = AppConstants.reportSpamUserAPI
//            let apiCall = RxAlmofireClass()
//            guard let oppUserID = self.userID else {return}
//            apiCall.newtworkRequestAPIcall(serviceName: url, requestType: .post, parameters:["targetUserId":oppUserID],headerParams:headerParams, responseType: AppConstants.resposeType.reportUser.rawValue)
//            apiCall.subject_response
//                .subscribe(onNext: {dict in
//                    guard let responseKey = dict[AppConstants.resposeTypeKey] as? String else {return}
//                    if responseKey == AppConstants.resposeType.reportUser.rawValue{
//                        DispatchQueue.main.async {
//                             Helper.hidePI()
//                             Helper.showAlertViewOnViewController("Success", message:"We have received your request. We will take appropriate action.", controller: self)
//                        }
//                    }
//                }, onError: {error in
//                     Helper.hidePI()
//                })
//        })
//        actionSheet.addAction(action1)
//        self.present(actionSheet, animated: true, completion: nil)
    }
    
    //block User
    func openBlockActionsheet(){
        
        let actionSheet = Helper.initActionSheet(title: nil, message: StringConstants.blockedContactWill())
        
        let isBlockstr =  isBlockUser  ? StringConstants.unBlock() : StringConstants.block()
        let action1 = UIAlertAction.init(title: isBlockstr, style: .destructive, handler: {action in
            self.blockUserApicall(reciverID: self.userID, strBlock:   self.isBlockUser  ? StringConstants.unBlock() : StringConstants.block())
        })
        actionSheet.addAction(action1)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
}



//  ApiOnServer.BLOCK_USER + "/" + receiverUid + "/" + str

extension ContactInfoTableViewController {
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0{
            return 70
        }else if indexPath.section == 2{
            return 50
        }  else{
            return 45
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 2
        }else if section == 3{
            return othercellArr.count
        }else if section == 1{
            return newContactArr!.count
        }else {
            return 2
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "contactInfocell", for: indexPath) as! UserNameTableViewCell
            if indexPath.row == 0{
                cell.userContactNumber.isHidden = false
                cell.userName.isHidden = false
                cell.userName.isHidden = false
                cell.chatIconBtn.isHidden = false
                cell.callIconBtn.isHidden = false
                cell.videoIconBtn.isHidden = false
                cell.statusLbl.isHidden = true
                cell.setUpcontactInfo(regiNum: userRegisterNum!, userName: userName!, infoObj: self, userID: userID!, profilePic: userImage!, isBlock: isBlockUser)
            }else {
                cell.userContactNumber.isHidden = true
                cell.userName.isHidden = false
                cell.chatIconBtn.isHidden = true
                cell.callIconBtn.isHidden = true
                cell.videoIconBtn.isHidden = true
                cell.statusLbl.isHidden = false
                cell.userName.text = StringConstants.status()
                cell.statusLbl.text = userStatus
            }
            
            return cell}
        else if indexPath.section == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "newContactcell", for: indexPath)
            cell.textLabel?.text = newContactArr![indexPath.row] as String
            cell.accessoryType = .disclosureIndicator
            
            return cell
        } else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "mediaCell", for: indexPath) as! MediaCellTableViewCell
            if let favObj = favoriteObj {
                cell.setUpmediaCell(index: indexPath, withChatDocId: favObj.userId)
            } else if let chatVmObj = self.chatVMObj {
                cell.setUpmediaCell(index: indexPath, withChatDocId: chatVmObj.docID)
            }
            cell.setUpmediaCell(index: indexPath, withChatDocId: nil)
            cell.switchh.addTarget(self, action: #selector(muteNotificationAction(_:)), for: .valueChanged)
           
            if indexPath.row == 1 {
                cell.switchh.isHidden = false
                cell.accessoryType = .none
                cell.media_count.isHidden = true
                if (UserDefaults.standard.object(forKey:"\(self.userID!)+isMute") as? Bool) != nil{
                    cell.switchh.setOn(true, animated: true)
                }else {
                    cell.switchh.setOn(false, animated: true)
                }
            }else {
                cell.media_count.isHidden = false
                 cell.switchh.isHidden = true
                cell.accessoryType =  .disclosureIndicator
            }
            return cell
            
        }else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "otherCell", for: indexPath)
            cell.textLabel?.text = othercellArr[indexPath.row] as String
            if isBlockUser {
                if indexPath.row == 1 {
                    cell.textLabel?.text = StringConstants.unblockContact()
                    
                }
            }
            return cell
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            if indexPath.row == 0 {
                self.performSegue(withIdentifier: SegueIds.contactToInfoTomedia, sender: self)
            }
        }else if indexPath.section == 3 {
            if indexPath.row == 0 {
                self.openClearChatActionsheet()
            }else if indexPath.row == 1 {
                openBlockActionsheet()
            }else {
                openReportSpamActionsheet()
            }
        }else if indexPath.section == 1 {
            if newContactArr!.count == 2 {
                
            }
            let main = UIStoryboard.init(name: "Main", bundle: nil)
            if  let contctVC =  main.instantiateViewController(withIdentifier: "ContactDetailsTableViewController") as? ContactDetailsTableViewController{
                contctVC.userNUmber = userRegisterNum
                contctVC.userphType = "mobile"
                contctVC.userName = userName?.count == 0 ? userRegisterNum : userName
                self.navigationController?.pushViewController(contctVC, animated: true)
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIds.infoToChatView{
            let controller = segue.destination as! ChatViewController
            controller.favoriteObj = favoriteObj
        }
        else if segue.identifier == SegueIds.contactToInfoTomedia {
            if let controller = segue.destination as? MediaCollectionViewController {
                if let favObj = favoriteObj {
                    controller.chatdocID = favObj.userId
                } else if let chatVmObj = self.chatVMObj {
                    controller.chatdocID = chatVmObj.docID
                }
            }
        }
    }
}


//MARK - API calling Methods


extension ContactInfoTableViewController{
    
    
    func getChatDocumentID() -> String{
        
        var chatDocId = ""
        if let favObj = self.favoriteObj {
            chatDocId = favObj.userId
        } else if let chatVmObj = self.chatVMObj {
            chatDocId = chatVmObj.docID!
        }
        return chatDocId
    }
    
    
    //Block user
    func blockUserApicall(reciverID: String? ,strBlock : String){
        
//        let chatDocId = getChatDocumentID()
//        if chatDocId.count == 0 {return}
//        guard let reciverid = reciverID else {return}
//        Helper.showProgressIndicator(withMessage: "Loading...")
//        let strURL = AppConstant.blockPersonAPI + "/" + reciverid + "/" + strBlock
//        guard let keyDict = Locksmith.loadDataForUserAccount(userAccount: AppConstants.keyChainAccount) else {return}
//        guard  let token = keyDict["token"] as? String  else {return}
//        let headerParams = ["authorization":AppConstants.authorization,"token":token]
//
//        let apiCall = RxAlmofireClass()
//        apiCall.newtworkRequestAPIcall(serviceName: strURL, requestType: .post, parameters:nil,headerParams:headerParams, responseType: AppConstants.resposeType.blockUser.rawValue)
//        apiCall.subject_response
//            .subscribe(onNext: {dict in
//                Helper.hidePI()
//                guard let responseKey = dict[AppConstants.resposeTypeKey] as? String else {return}
//                if responseKey == AppConstants.resposeType.blockUser.rawValue {
//                    self.blockUser?(!self.isBlockUser)
//                    self.isBlockUser = !self.isBlockUser
//                    self.chatsDocVMObject?.updateUserBlockStatus(toDocID: chatDocId, isBlock:self.isBlockUser )
//                    self.imageRemoveForBlockUser()
//                    self.sendBlockOnmqtt(reciveID: reciverid)
//                    self.tableView.reloadData()
//                }
//            }, onError: {error in
//
//                Helper.hidePI()
//            })
    }
    
    
    @objc func muteNotificationAction(_ ischage:UISwitch){
        if ischage.isOn{
            muteNotificationAPI(isOn: true)
        }else{
            muteNotificationAPI(isOn: false)
        }
    }
    
    
    func muteNotificationAPI(isOn:Bool){
//        Helper.showProgressIndicator(withMessage:"Loading...")
//        guard let keyDict = Locksmith.loadDataForUserAccount(userAccount: AppConstant.keyChainAccount) else {return}
//        guard  let token = keyDict["token"] as? String  else {return}
//        let headerParams = ["authorization":AuthorizationHeader.getAuthHeader(staticToken:false),"token":token] as [String : Any]
//        let url = AppConstant.muteNotificationAPI
//        let apiCall = RxAlmofireClass()
//        guard let oppUserID = self.userID else {return}
//        apiCall.newtworkRequestAPIcall(serviceName: url, requestType: .post, parameters:["targetUserId":oppUserID,"isMuteNotification":isOn],headerParams:headerParams, responseType: AppConstants.resposeType.muteNotification.rawValue)
//        apiCall.subject_response
//            .subscribe(onNext: {dict in
//                Helper.hidePI()
//                guard let responseKey = dict[AppConstants.resposeTypeKey] as? String else {return}
//                if responseKey == AppConstants.resposeType.muteNotification.rawValue{
//                    UserDefaults.standard.set(isOn, forKey:"\(self.userID!)+isMute")
//                    if isOn == false{
//                        UserDefaults.standard.removeObject(forKey:"\(self.userID!)+isMute")
//                    }
//                }
//            }, onError: {error in
//                Helper.hidePI()
//            })
    }
    
    func  sendBlockOnmqtt(reciveID: String){
        
//        guard let userID = Helper.getMQTTID() else { return }
//        let mqttDict:[String:Any] = ["blocked":self.isBlockUser,
//                                     "initiatorId":userID,
//                                     "type": 6  as Any]
//
//        //print("send On MQTT block msg \(mqttDict)")
//        let groupChannel = "\(AppConstants.MQTT.userUpdates)\(reciveID)"
//
//        do {
//            let jsonData = try JSONSerialization.data(withJSONObject:mqttDict , options: .prettyPrinted)
//            MQTT.sharedInstance.publishData(wthData: jsonData, onTopic: groupChannel, retain: false, withDelivering:  .atLeastOnce)
//        }catch  {
//            print("\(error.localizedDescription)")
//        }
    }
    
    
}
