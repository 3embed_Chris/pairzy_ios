//
//  MediaCollectionViewController.swift
//  MQTT Chat Module
//
//  Created by Imma Web Pvt Ltd on 09/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

private let reuseIdentifier = "Cell"

class MediaCollectionViewController:UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var segment: UISegmentedControl!
    var selectImageCollectionView : UICollectionView?
    var chatdocID : String?
    var sections = Dictionary<String, Array<Message>>()
    var sortedSections = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.delegate = self
        self.collectionView?.dataSource = self
        let collectionviewLyout:UICollectionViewFlowLayout = self.collectionViewLayout as! UICollectionViewFlowLayout
        collectionviewLyout.sectionInset = UIEdgeInsets.init(top: 5, left: 10, bottom: 5, right: 10)
        getMediaMsgs(withChatDocID: chatdocID)
    }
    
    @IBAction func segmentCliked(_ sender: Any) {
        if segment.selectedSegmentIndex == 0 {
            collectionView?.reloadData()
        } else {
            collectionView?.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    private func getMediaMsgs(withChatDocID chatDocID: String?) {
        guard let msgs = self.getAllMediaMessages(withChatDocID : chatDocID) else { return }
        self.addDataIntoArray(withMessages: msgs)
    }
    
    private func getAllMediaMessages(withChatDocID chatDocID: String?) -> [Message]? {
        guard let chatDocID = chatDocID else { return nil }
        let chatDocVMObj = ChatsDocumentViewModel(couchbase: Couchbase.sharedInstance)
        return chatDocVMObj.getMediaMessages(withChatDocID: chatDocID)
    }
    
    func addDataIntoArray(withMessages messages: [Message]) {
        for msg in messages {
            if let dateObj = msg.messageSentDate {
                let dateStr = dateObj.getMonthFormatted()
                if self.sections.index(forKey: dateStr) != nil {
                    if var msgs = self.sections[dateStr] {
                        msgs.append(msg)
                        self.sections[dateStr] = msgs
                    }
                } else {
                    self.sections[dateStr] = [msg]
                }
            }
        }
        //we are storing our sections in dictionary, so we need to sort it
        self.sortedSections = self.sections.keys.sorted(by:<)
        self.collectionView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sections.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return sections[sortedSections[section]]!.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let msgSection = sections[sortedSections[indexPath.section]]
        let msgItem = msgSection![indexPath.row]
        if msgItem.messageType == MessageTypes.video { // Message type is Video
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaVideoCollectionViewCell", for: indexPath) as! MediaVideoCollectionViewCell
            cell.msgObject = msgItem
            return cell
        } else if msgItem.messageType == MessageTypes.replied {
            if let repliedMsg = msgItem.repliedMessage {
                if repliedMsg.replyMessageType == MessageTypes.video { // Message type is Video
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaVideoCollectionViewCell", for: indexPath) as! MediaVideoCollectionViewCell
                    cell.msgObject = msgItem
                    return cell
                } else {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mediaImageS", for: indexPath) as! MediaImageCollectionViewCell
                    cell.msgObject = msgItem
                    if repliedMsg.replyMessageType == MessageTypes.gif { // Message type is gif
                        cell.gifIconOutlet.isHidden = false
                    } else {
                        cell.gifIconOutlet.isHidden = true
                    }
                    return cell
                }
            }
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mediaImageS", for: indexPath) as! MediaImageCollectionViewCell
            cell.msgObject = msgItem
            if msgItem.messageType == MessageTypes.gif { // Message type is gif
                cell.gifIconOutlet.isHidden = false
            } else {
                cell.gifIconOutlet.isHidden = true
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    @objc func  collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.view.frame.size.width == 375 { return CGSize(width: self.view.frame.size.width/4 - 9, height: 80)  } else {
            return CGSize(width: self.view.frame.size.width/4 - 9, height: 80)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let showMediaViewCntroller = ShowMediaViewController(nibName: "ShowMediaViewController", bundle: nil)
        guard let msgs = self.getAllMediaMessages(withChatDocID: chatdocID) else { return }
        showMediaViewCntroller.messages = msgs
        let msgSection = sections[sortedSections[indexPath.section]]
        let msgItem = msgSection![indexPath.row]
        showMediaViewCntroller.selectedMessageUrl = msgItem.getVideoFileName()
        self.present(showMediaViewCntroller, animated: false, completion: nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "timeHeaderView", for: indexPath) as! MediaHeaderCollectionReusableView
        view.timeLbl.text = sortedSections[indexPath.section]
        return view
    }
}

extension Date {
    func getMonthFormatted() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        return dateFormatter.string(from: self)
    }
}
