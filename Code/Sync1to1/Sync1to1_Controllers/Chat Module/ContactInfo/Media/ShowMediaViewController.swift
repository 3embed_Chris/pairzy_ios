//
//  showMediaView.swift
//  MQTT Chat Module
//
//  Created by Imma Web Pvt Ltd on 12/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import AVKit

protocol ShowVideoPlayerDelegate: class {
    func presentVideoPlayer(withURL : URL)
}

class ShowMediaViewController : UIViewController {
    
    let  MyCollectionViewCellId = "ShowMediaCollectionViewCell"
    
    @IBOutlet weak var showMediaCollectionView: UICollectionView!
    
    @IBOutlet weak var playButtonOtuelt: UIButton!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    
    var isTapped = false
    var messages : [Message]!
    var selectedMessageUrl : String?
  weak var showVideoPlayerDelegate : ShowVideoPlayerDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCell()
        self.showMediaCollectionView.delegate = self
        self.showMediaCollectionView.dataSource = self
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapCliked(tapGesture:)))
        self.view.addGestureRecognizer(tapGesture)
        self.showMediaCollectionView.isHidden = true
        self.scrollToSelectedIndex()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.editPlayButton()
    }
    
    func scrollToSelectedIndex() {
        if let msgUrl = selectedMessageUrl {
            if let rowIndex = self.getCurrentIndex(withSelectedPath: msgUrl, withMediaMsgs: messages) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    self.showMediaCollectionView.isHidden = false
                    self.showMediaCollectionView.scrollToItem(at: IndexPath(item: rowIndex, section: 0), at: .centeredHorizontally, animated: false)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        self.playVideo()
                    }
                }
            }
        }
    }
    
    private func getCurrentIndex(withSelectedPath selectedPath : String, withMediaMsgs mediaMsgs: [Message]) -> Int? {
        let selectedMsg = mediaMsgs.filter({
            if let str = $0.getVideoFileName() {
                return str == selectedPath
            }
            return false
        })
        if selectedMsg.count>0 {
            if let rowIndex = mediaMsgs.index(of: selectedMsg[0]) {
                return rowIndex
            }
        }
        return nil
    }
    
    func registerCell() {
        self.showMediaCollectionView.register(UINib(nibName: "ShowMediaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShowMediaCollectionViewCell")
    }
    
    @IBAction func backCliked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func playButtonAction(_ sender: UIButton) {
        self.playVideo()
    }
    
    private func playVideo() {
        if let visibleCell = self.showMediaCollectionView.visibleCells.first as? ShowMediaCollectionViewCell {
            self.playButtonOtuelt.isSelected = true
            if let urlStr = visibleCell.videoURL {
                let mediaURL = URL(fileURLWithPath: urlStr)
                self.playVideo(withURL: mediaURL)
            }
        }
    }
    
    func playVideo(withURL url : URL) {
        let player = AVPlayer(url: url)
        self.playButtonOtuelt.isSelected = true
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.present(playerController, animated: false, completion: {
            player.play()
            self.playButtonOtuelt.isSelected = false
        })
    }
}

private extension ShowMediaViewController {
    
    @objc func swipeCliked() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tapCliked(tapGesture : UITapGestureRecognizer){
        if isTapped == false {
            isTapped = true
            navView.isHidden = true
            bottomView.isHidden = true
        } else {
            isTapped = false
            navView.isHidden = false
            bottomView.isHidden = false
        }
    }
}

extension ShowMediaViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.editPlayButton()
    }
    
    fileprivate func editPlayButton() {
        self.playButtonOtuelt.isHidden = true
        if let visibleCell = self.showMediaCollectionView.visibleCells.first as? ShowMediaCollectionViewCell {
            self.playButtonOtuelt.isHidden = visibleCell.playBtnCellOutlet.isHidden
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.editPlayButton()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return messages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MyCollectionViewCellId, for: indexPath) as! ShowMediaCollectionViewCell
            cell.msgObject = messages[indexPath.row]
            cell.playButtonDelegate = self
        return cell
    }
    
    @objc func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
    }
}

extension ShowMediaViewController : playButtonActionDelegate {
    func playButtonPressed(withURLString urlStr: String) {
        let mediaURL = URL(fileURLWithPath: urlStr)
        self.playVideo(withURL: mediaURL)
    }
}

