//
//  ContactDetailsTableViewCell.swift
//  MQTT Chat Module
//
//  Created by Imma Web Pvt Ltd on 23/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class ContactDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var mobilelbl: UILabel!
    @IBOutlet weak var userName: UILabel!
    var perentobj : ContactDetailsTableViewController?
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    
    func showData(mobileType:String, userNum: String, perentobj: ContactDetailsTableViewController ){
        
        self.mobilelbl.text = mobileType
        self.userName.text = userNum
        self.perentobj = perentobj
    }
    
    
    @IBAction func chatButtonCliked(_ sender: Any) {
        
        var isChatVCFound = false
        let arr = self.perentobj?.navigationController?.viewControllers
        for i:UIViewController in arr! {
            if i.isKind(of: ChatViewController.self)   {
                isChatVCFound = true
                self.perentobj?.navigationController?.popToViewController( i, animated: true)
            }
        }
        
     if isChatVCFound == false{self.perentobj?.navigationController?.popToRootViewController(animated: true)}
        
    }
    
    
    @IBAction func videoCallCliked(_ sender: Any) {
        
        
        
    }
    

    @IBAction func audioCallCliked(_ sender: Any) {
        
        
        
        
    }
    
    
}
