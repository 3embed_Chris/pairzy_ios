//
//  DateHelper.swift
//  MQTT Chat Module
//
//  Created by Dinesh Guptha Bavirisetti on 11/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

class DateHelper  : NSObject{
    func getCurrentDate(forDate dateString: String) -> Date? {
        let dateFormat = DateFormatter()
        dateFormat.timeZone = TimeZone(abbreviation: "GMT")
        dateFormat.dateFormat = "dd-MM-yyyy"
        let thisDate = dateFormat.date(from:dateString)
        return thisDate
    }
    
    
 
    
    func compareDates(currentDate:Date, previousDate:Date)->Bool{
        let calendar = Calendar.current
        let currentYear = calendar.component(.year, from: currentDate )
        let currentMonth = calendar.component(.month, from: currentDate)
        let currentDay = calendar.component(.day, from: currentDate)
        let previousYear = calendar.component(.year, from: previousDate)
        let previousMonth = calendar.component(.month, from: previousDate)
        let previousDay = calendar.component(.day, from: previousDate)
        if currentYear == previousYear && currentMonth == previousMonth && currentDay == previousDay{
            return true
        }
        else{
            return false
        }
    }
    
    func getDateString(fromDate date: Date?) -> String?{
        guard let date = date else { return nil }
        let dateFormat = DateFormatter()
        dateFormat.timeZone = TimeZone(abbreviation: "GMT")
        dateFormat.dateFormat = "yyyy-MM-dd"
        let thisDate = dateFormat.string(from: date)
        return thisDate
    }
    
    
    func getDateOfb(fromDate date: Date?) -> String?{
        guard let date = date else { return nil }
       
        let FormatDate = DateFormatter()
        FormatDate.dateStyle = .medium
        FormatDate.timeStyle = .none
        FormatDate.dateFormat = "MM/dd/yyyy"
        let thisDate = FormatDate.string(from: date)
      // dateFormat.string(from: date)
        return thisDate
    }
    
    func getDate(forLastSeenTimeString timeString : String) -> Date? {
        let dateFormat = DateFormatter()
        dateFormat.timeZone = TimeZone(abbreviation: "GMT")
        dateFormat.dateFormat = "yyyyMMddHHmmssSSS z"
        let thisDate = dateFormat.date(from:timeString)
        return thisDate
    }
    
    func sendTimeStamp(fromDate date : Date) -> String? {
        let dateFormat = DateFormatter()
        dateFormat.timeZone = TimeZone(abbreviation: "GMT")
        dateFormat.dateFormat = "yyyyMMddHHmmssSSS z"
        let thisDate = dateFormat.string(from: date)
        return thisDate
    }
    
    func getDateString(fromTimeStamp timeStamp: String) -> String {
        let timeStampInt = UInt64(UInt64(timeStamp)!/1000)
        let msgDate = Date(timeIntervalSince1970: TimeInterval(timeStampInt))
        let dateStr = self.lastMessageTime(date: msgDate)
        return dateStr
    }
    
    func getDateObj(fromTimeStamp timeStamp: String) -> Date {
        let timeStampInt = UInt64(UInt64(timeStamp)!/1000)
        let msgDate = Date(timeIntervalSince1970: TimeInterval(timeStampInt))
        return msgDate
    }
    
    func lastMessageTime(date: Date)->String{
        let dateFormatter = DateFormatter()
        let today = NSCalendar.current.isDateInToday(date)
        dateFormatter.dateFormat = "hh:mm a"
        if(today) {
            return dateFormatter.string(from: date)
            //return "today"
        }
        else if(NSCalendar.current.isDateInYesterday(date)){
            return "yesterday"
        }
        else{
            dateFormatter.dateStyle = .short
            let dateString = dateFormatter.string(from: date)
            return dateString
        }
    }
    
    func lastSeenTime(date : Date) -> String {
        let dateFormatter = DateFormatter()
        let today = NSCalendar.current.isDateInToday(date)
        dateFormatter.dateFormat = "hh:mm a"
        if(today){
            
            return dateFormatter.string(from: date)
        }
        else if(NSCalendar.current.isDateInYesterday(date)){
            return "yesterday \(dateFormatter.string(from: date))"
        }
        else{
            
            dateFormatter.dateStyle = .short
            let dateString = dateFormatter.string(from: date)
            return dateString
        }
    }
    
    func lastMessageInHours(date : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        return dateFormatter.string(from: date)
    }
    
    func relativePast(for date : Date) -> String {
        
        let units = Set<Calendar.Component>([.year, .month, .day, .hour, .minute, .second, .weekOfYear])
        let components = Calendar.current.dateComponents(units, from: date, to: Date())
        
        if components.year! > 0 {
            return "\(components.year!) " + (components.year! > 1 ? "years ago" : "year ago")
            
        } else if components.month! > 0 {
            return "\(components.month!) " + (components.month! > 1 ? "months ago" : "month ago")
            
        } else if components.weekOfYear! > 0 {
            return "\(components.weekOfYear!) " + (components.weekOfYear! > 1 ? "weeks ago" : "week ago")
            
        } else if (components.day! > 0) {
            return (components.day! > 1 ? "\(components.day!) days ago" : "Yesterday")
            
        } else if components.hour! > 0 {
            return "\(components.hour!) " + (components.hour! > 1 ? "hours ago" : "hour ago")
            
        } else if components.minute! > 0 {
            return "\(components.minute!) " + (components.minute! > 1 ? "minutes ago" : "minute ago")
            
        } else {
            return "\(components.second!) " + (components.second! > 1 ? "seconds ago" : "second ago")
        }
    }
    
    
    
    
    func getCurrentDate()-> Date {
        var now = Date()
        var nowComponents = DateComponents()
        let calendar = Calendar.current
        nowComponents.year = Calendar.current.component(.year, from: now)
        nowComponents.month = Calendar.current.component(.month, from: now)
        nowComponents.day = Calendar.current.component(.day, from: now)
        nowComponents.hour = Calendar.current.component(.hour, from: now)
        nowComponents.minute = Calendar.current.component(.minute, from: now)
        nowComponents.second = Calendar.current.component(.second, from: now)
        nowComponents.timeZone = NSTimeZone.local
        now = calendar.date(from: nowComponents)!
        return now as Date
    }
    
}
