//
//  ChatAppConstants.swift
//  MQTT Chat Module
//
//  Created by Dinesh Guptha Bavirisetti on 19/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class ChatAppConstants  : NSObject {
    
    //8009
    static let constantURL =  "http://34.209.115.119:8008/"   //https://uploadfile.findamatch.online/" //"http://45.55.223.151:5010/"
    static let uploadMultimediaURL = "http://34.209.115.119:8008/" //"https://uploadfile.findamatch.online/" //"http://45.55.223.151:8009/"
    static let uploadVideos =   "http://34.209.115.119:8008/" //"https://uploadfile.findamatch.online/"  //"http://45.55.223.151:8009/"
    static let uploadPhoto = "upload" //post
    static let uploadProfilePic = "upload"     //"YeloChat/profilePics"
    static let getChats = "Chats" //get
    static let getMessages = "Messages" //get
    static let uploadedVideoExtension = "mov"
    static let mGoogleLegaceyServerKey = "AIzaSyA6kBuA_QYRnuZYhZC8keYhg6VYzfAnxsI"
    static let mGoogleAPIKeyFCM        = "key=AIzaSyA6kBuA_QYRnuZYhZC8keYhg6VYzfAnxsI"
    
    struct CouchbaseConstant {
        static let dbName = "mqttchatdb"
    }
    
    struct  UserDefaults {
        static let indexDocID = "indexDocID"
        static let userName = "userName"
        static let userImage = "userImage"
        static let sessionToken = "userDetail"
        static let pushToken = "pushToken"
        static let mqttId = "mqttId"
        static let rechabilityNotificationKey = "rechabilityNotification"
        static let isUserOnchatscreen = "userIsnoChatscreen"
    }
    
    struct indexDocumentConstants {
        static let userIDArray = "userIDArray"
        static let userDocIDArray = "userDocIDArray"
        static let chatDocument = "chatDocument"
    }
    
    struct MQTT {
        static let messagesTopicName = "Message/"
        static let acknowledgementTopicName = "Acknowledgement/"
        static let onlineStatus = "OnlineStatus"
        static let typing = "Typ/"
        static let getChats = "GetChats/"
        static let getMessages = "GetMessages/"
        static let getMatches = "match/user/"
        static let getnewLikes = "newLikes/"
        static let boostupdate = "boost/"
        static let getProfiles = "searchResult/"
        static let unMatchProfile = "unMatch/"
        static let bannedUser     = "bannedUser/"
        static let campaign     = "campaign/"
        static let campaignAll     = "campaign/All"
        
    }
}
