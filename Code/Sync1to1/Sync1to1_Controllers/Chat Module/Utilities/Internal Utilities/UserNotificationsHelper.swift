//
//  UserNotificationsHelper.swift
//  MQTT Chat Module
//
//  Created by Dinesh Guptha Bavirisetti on 29/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UserNotifications

class UserNotificationsHelper: NSObject, UNUserNotificationCenterDelegate {
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Play sound and show alert to the user
        completionHandler([.alert,.sound])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // Determine the user action
        if #available(iOS 10.0, *) {
            switch response.actionIdentifier {
            case UNNotificationDismissActionIdentifier: break
            case UNNotificationDefaultActionIdentifier: break
            case "Snooze": break
            case "Delete":break
            default: break
            }
        } else {
            // Fallback on earlier versions
        }
        completionHandler()
    }
}
