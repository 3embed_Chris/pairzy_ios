//
//  topLabelCollectionCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 23/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class TopLabelCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var bottomIndicatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Helper.setShadow(sender: bottomIndicatorView)
        bottomIndicatorView.backgroundColor = Colors.AppBaseColor
    }
    
    func updateCell(title: String,view: UIView){
        labelTitle.text = title
        
        if bottomIndicatorView.tag == view.tag {
            bottomIndicatorView.isHidden = false
            labelTitle.textColor = Colors.PrimaryText
            bottomIndicatorView.backgroundColor = Colors.AppBaseColor
        }else {
            labelTitle.textColor = Helper.getUIColor(color: "#B2B2B2")
            bottomIndicatorView.backgroundColor = UIColor.white
            bottomIndicatorView.isHidden = true
        }
   bottomIndicatorView.layer.shadowOpacity = 0.7
        
    }
    
    
}
