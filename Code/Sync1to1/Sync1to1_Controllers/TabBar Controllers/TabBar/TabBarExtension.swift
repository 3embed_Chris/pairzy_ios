//
//  TabBarExtension.swift
//  Datum
//
//  Created by apple on 06/05/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

extension UITabBar {
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        guard let window = UIApplication.shared.keyWindow else {
            return super.sizeThatFits(size)
        }
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = window.safeAreaInsets.bottom + 50
        return sizeThatFits
    }
}
