//
//  TabBarController.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 07/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController,UITabBarControllerDelegate {
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        // Sets the background color of the selected UITabBarItem (using and plain colored UIImage with the width = 1/5 of the tabBar (if you have 5 items) and the height of the tabBar)
   //    UITabBar.appearance().selectionIndicatorImage = UIImage().makeImageWithColorAndSize(color: UIColor.lightGray .withAlphaComponent(0.2), size: CGSize(width:tabBar.frame.width/5, height:tabBar.frame.height))
        
        self.tabBar.unselectedItemTintColor = Colors.tabBarUnselected
        // Do any additional setup after loading the view.
        guard let items = tabBar.items else { return }
        
        items[0].title = StringConstants.makeMatch()
        items[1].title = StringConstants.discover()
        items[2].title = StringConstants.dates()
        items[3].title = StringConstants.prospects()
        items[4].title = StringConstants.chats()
    }
    
    func enablePushNotification() {
        UserDefaults.standard.removeObject(forKey: "currentUserId")
        UserDefaults.standard.synchronize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.tabBar.invalidateIntrinsicContentSize()
    }
    
     func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool{
//        if viewController == tabBarController.viewControllers?[3] {
//            if !Helper.isSubscribedForPlans() {
//                openInAppPurchaseView()
//               return false
//              //  return true
//            }
//        }        
        return true
    }
    
    
    func openInAppPurchaseView() {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = InAppPuchasePopupView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.loadInAppPurchaseView()
        window.addSubview(videoView)
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        tabBar.tintColor =  Colors.AppBaseColor
    }
  
}

extension UIImage {
    func makeImageWithColorAndSize(color: UIColor, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
func fixedOrientation() -> UIImage {
            
            if imageOrientation == UIImageOrientation.up {
                return self
            }
            
            var transform: CGAffineTransform = CGAffineTransform.identity
            
            switch imageOrientation {
            case .down, .downMirrored:
                transform = transform.translatedBy(x: size.width, y: size.height)
                transform = transform.rotated(by: CGFloat.pi)
                break
            case .left, .leftMirrored:
                transform = transform.translatedBy(x: size.width, y: 0)
                transform = transform.rotated(by: CGFloat.pi / 2.0)
                break
            case .right, .rightMirrored:
                transform = transform.translatedBy(x: 0, y: size.height)
                transform = transform.rotated(by: CGFloat.pi / -2.0)
                break
            case .up, .upMirrored:
                break
            }
            switch imageOrientation {
            case .upMirrored, .downMirrored:
                transform.translatedBy(x: size.width, y: 0)
                transform.scaledBy(x: -1, y: 1)
                break
            case .leftMirrored, .rightMirrored:
                transform.translatedBy(x: size.height, y: 0)
                transform.scaledBy(x: -1, y: 1)
            case .up, .down, .left, .right:
                break
            }
            
            let ctx: CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0, space: self.cgImage!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
            
            ctx.concatenate(transform)
            
            switch imageOrientation {
            case .left, .leftMirrored, .right, .rightMirrored:
                ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
            default:
                ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
                break
            }
            
            return UIImage(cgImage: ctx.makeImage()!)
    }
}


