//
//  MatchMakerVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxAlamofire

class MatchMakerVM: NSObject {

    let disposeBag = DisposeBag()
    
    
    /// matchMakersubject_responser is subject response object for matches and instantnt matches and second look profiles.
    let matchMakersubject_responser = PublishSubject<Bool>()
    
    /// unMatchSubjectResponse is subject response object for unmatch API.
    let unMatchSubjectResponse = PublishSubject<Bool>()
    
    var isRequestForMacthes = false
    
    /// profilesArrayData this array contains array of proifles.
    var profilesWithOutChat = [Profile]()
    var profilesWithChat = [Profile]()
    
    
//    var dummyTableviewForMatches = UITableView()

    
    var reportReasonsArray = [ReportReason]()
    
    //MARK: - GET MATCHES API CALL
    
    
    /// method for requesting matches.
    func requestForMatches() {
        
        isRequestForMacthes = true
        
        
        let apiCall = MatchMakerAPICalls()
        apiCall.requestForGetMatches()
        
        apiCall.subject_response
            .subscribe(onNext: {response in
                self.handleMatchesResponse(response: response)
            }, onError: {error in
                self.matchMakersubject_responser.onError(error)
            })
            .disposed(by:disposeBag)
    }
    
    
    func checkMatchesDataFromDB(matchesCollectionView:UICollectionView,chatTableView:UITableView) {
        if self.checkMatchesDocument() {
            
            let matchesDocumentVm = MatchesDocumentViewModel(couchbase: Couchbase())
            
            let matchesDocIdKey = "userMatchData/"
            let matchDocID = UserDefaults.standard.value(forKey: matchesDocIdKey)
            
            let matchesData = matchesDocumentVm.getMatchData(forDocId: matchDocID! as! String)
            var notChatProfiles = [Profile]()
            var chatProfiles = [Profile]()
            
            if (matchesData != nil) {
                if let profileResponse = matchesData!["matchesArray"] as? [AnyObject] {
                    for (index, _) in profileResponse.enumerated() {
                        
                        var profileDetails = profileResponse[index] as! [String:Any]
                        if let lastMessage = profileDetails[profileData.chatStarted] as? String {
                            if(lastMessage == "3embed test") {
                                //add in non- conversation list.
                                notChatProfiles.append(Profile.fetchprofileDataObj(userDataDictionary:profileDetails))
                            } else {
                                //add in conversation list.
                                chatProfiles.append(Profile.fetchprofileDataObj(userDataDictionary:profileDetails))
                            }
                        }
                    }
                }
                
                self.profilesWithOutChat = notChatProfiles
                self.profilesWithChat = chatProfiles
                
                
                matchesCollectionView.reloadData()
                chatTableView.reloadData()
            }
        }
    }
    
    
    func updateChatStartedWithUser(userId:String) {
        if self.checkMatchesDocument() {
            let matchesDocumentVm = MatchesDocumentViewModel(couchbase: Couchbase())
            
            let matchesDocIdKey = "userMatchData/"
            let matchDocID = UserDefaults.standard.value(forKey: matchesDocIdKey)
            
            let matchesData = matchesDocumentVm.getMatchData(forDocId: matchDocID! as! String)
            
            if (matchesData != nil) {
                if var profileResponse = matchesData!["matchesArray"] as? [AnyObject] {
                    for (index, _) in profileResponse.enumerated() {
                        var profileDetails = profileResponse[index] as! [String:Any]
                        if let matchID = profileDetails["opponentId"] as? String{
                            if userId == matchID {
                                profileDetails[profileData.isCoversationStarted] = 1
                                profileResponse[index] = profileDetails as AnyObject
                                
                                //updating local match data
                                guard  let docId = UserDefaults.standard.object(forKey:"userMatchData/") as? String else {return}
                                matchesDocumentVm.updateMatchDocument(withMatchDocumentId: docId , newData: profileResponse)
                                
                                return
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    //MARK: - Fetch and update Matches Data From Database.
    
    
    /// method to check document availble or not.
    ///
    /// - Returns: true if doc avaialble otherwise false.
    func checkMatchesDocument() -> Bool {
        let matchesDocId = "userMatchData/"
        if Helper.isKeyPresentInUserDefaults(key: matchesDocId) {
           return true
        }
        return false
    }
    
    
    /// method for adding data to database
    ///
    /// - Parameter response: data for adding to database.
    func addDataToDataBase(response:ResponseModel) {
        let matchesDocumentVm = MatchesDocumentViewModel(couchbase: Couchbase())
        if checkMatchesDocument() {
            if let profileResponse = response.response["data"] as? [AnyObject] {
                guard  let docId = UserDefaults.standard.object(forKey:"userMatchData/") as? String else {return}
                matchesDocumentVm.updateMatchDocument(withMatchDocumentId: docId , newData: profileResponse)
            } else {
                guard  let docId = UserDefaults.standard.object(forKey:"userMatchData/") as? String else {return}
                matchesDocumentVm.removeDataFromDocument(withMatchDocumentId: docId)
                UserDefaults.standard.removeObject(forKey: "userMatchData/")
                UserDefaults.standard.synchronize()
            }
        } else {
            if let profileResponse = response.response["data"] as? [AnyObject] {
                let matchDocID = matchesDocumentVm.createMatchesDocument(withMatchesData:profileResponse)
                matchesDocumentVm.saveMatchesDocId(matchDocId: matchDocID!)
            }
        }
    }
    
    
    /// method for update existing data
    func updateMatchesDataToDataBase() {
        
    }
    
    
    //MARK: - GET INSTANT MATCHES API CALL
    
    
    /// method for requesting instant matches.
    func requestForInstantMatches() {
        
        let apiCall = MatchMakerAPICalls()
        apiCall.requestForInstantMatches()
        
        apiCall.subject_response
            .subscribe(onNext: {response in
                self.handleMatchesResponse(response: response)
            }, onError: {error in
                self.matchMakersubject_responser.onError(error)
            })
            .disposed(by:disposeBag)
    }
    
    
    //MARK: - GET SECOND LOOK  PROFILE API CALL

    
    /// method forrequesting second chance profiles.
    func requestForSecondLook() {
        
        let apiCall = MatchMakerAPICalls()
        apiCall.requestForSecondLookProfiles()
        
        apiCall.subject_response
            .subscribe(onNext: {response in
                self.handleMatchesResponse(response: response)
            }, onError: {error in
                self.matchMakersubject_responser.onError(error)
            })
            .disposed(by:disposeBag)
    }
    
    
    /// method forrequesting second chance profiles.
    func requestToGetReportReasons(userClicked:Bool) {
        
        let apiCall = MatchMakerAPICalls()
        apiCall.requestToGetReportReasons()
        
        if(userClicked) {
            Helper.showProgressIndicator(withMessage:StringConstants.getReportReasons())
        }
        
        apiCall.subject_response
            .subscribe(onNext: {response in
                self.handleReportReasonsResponse(response: response)
            }, onError: {error in
                self.unMatchSubjectResponse.onError(error)
            })
            .disposed(by:disposeBag)
    }
    
    
    //MARK: - API RESPONSE HANDLER - GET MATCHES, GET INSTANT MATCHES , GET SECOND LOOK PROFILES
    
    
    
    /// method for handling matches api response.
    ///
    /// - Parameter response: contains api response and status code.
    func handleReportReasonsResponse(response:ResponseModel) {
        
        switch (response.statusCode) {
        case 200:
            
            // 200 is for success response (contains profiles)
            // 412 is for when there is no profiles.
            var reportReasonData = [ReportReason]()
            if let profileResponse = response.response["data"] as? [AnyObject] {
                for (index, _) in profileResponse.enumerated() {
                    if let reason = profileResponse[index] as? String {
                        reportReasonData.append(ReportReason(reportDettails:reason))

                    }
                }
            }
            
            self.reportReasonsArray = reportReasonData
            
            //reportReasonsArray
            self.unMatchSubjectResponse.onNext(true)
            
        default: break
            
        }
        
        Helper.hideProgressIndicator()
    }
    
    
    /// method for handling matches api response.
    ///
    /// - Parameter response: contains api response and status code.
    func handleMatchesResponse(response:ResponseModel) {
        
        switch (response.statusCode) {
        case 200,412:
            
            // 200 is for success response (contains profiles)
            // 412 is for when there is no profiles.
            var notChatProfiles = [Profile]()
            var chatProfiles = [Profile]()
            
            if let profileResponse = response.response["data"] as? [AnyObject] {
                for (index, _) in profileResponse.enumerated() {
                    var profileDetails = profileResponse[index] as! [String:Any]
                    if let lastMessage = profileDetails[profileData.chatStarted] as? String {
                        if(lastMessage == "3embed test") {
                            //add in non- conversation list.
                            notChatProfiles.append(Profile.fetchprofileDataObj(userDataDictionary:profileDetails))
                        } else {
                            //add in conversation list.
                        chatProfiles.append(Profile.fetchprofileDataObj(userDataDictionary:profileDetails))
                        }
                    }
                }
                
                self.profilesWithOutChat = notChatProfiles
                self.profilesWithChat = chatProfiles
                self.matchMakersubject_responser.onNext(true)
                
                if(isRequestForMacthes) {
                    self.addDataToDataBase(response:response)
                }
            }
        default: break
        }
        
        self.matchMakersubject_responser.onNext(true)
        Helper.hideProgressIndicator()
    }
    
    
    //MARK: - UNMATCH API CALL AND RESPONSE HANDLER
    
    
    /// method for requesting unmatch profile.
    ///
    /// - Parameter params: unmatch profile details.
    func unMatchProfile(params:[String:Any]) {
        let apiCall = MatchMakerAPICalls()
        Helper.showProgressIndicator(withMessage:StringConstants.unMatch())
        let requestData = RequestModel().dislikeProfileRequestDetails(Details:params)
        apiCall.requestForUnMatchUser(requestData:requestData)
        apiCall.subject_response
            .subscribe(onNext: {response in
                self.handleUnMatcheResponse(response:response)
            }, onError: {error in
                Helper.hideProgressIndicator()
                self.unMatchSubjectResponse.onError(error)
            })
            
            .disposed(by:disposeBag)
    }
    
    /// method for requesting report profile.
    ///
    /// - Parameter params: reoort profile details.
    func reportProfile(params:[String:Any]) {
        let apiCall = MatchMakerAPICalls()
        Helper.showProgressIndicator(withMessage:StringConstants.reportingUser())
        let requestData = RequestModel().dislikeProfileRequestDetails(Details:params)
        apiCall.requestForReportUser(requestData:requestData)
        apiCall.subject_response
            .subscribe(onNext: {response in
                self.handleUnMatcheResponse(response:response)
            }, onError: {error in
                Helper.hideProgressIndicator()
                self.unMatchSubjectResponse.onError(error)
            })
            
            .disposed(by:disposeBag)
    }
    
    /// method for requesting block profile.
    ///
    /// - Parameter params: block profile details.
    func blockProfile(params:[String:Any]) {
        let apiCall = MatchMakerAPICalls()
        Helper.showProgressIndicator(withMessage:StringConstants.blockUser())
        let requestData = RequestModel().dislikeProfileRequestDetails(Details:params)
        apiCall.requestForBlockUser(requestData:requestData)
        apiCall.subject_response
            .subscribe(onNext: {response in
                self.handleUnMatcheResponse(response:response)
            }, onError: {error in
                Helper.hideProgressIndicator()
                self.unMatchSubjectResponse.onError(error)
            })
            
            .disposed(by:disposeBag)
    }
    
    /// method for handling unmatch profile api.
    ///
    /// - Parameter response: contains response and status code.
    func handleUnMatcheResponse(response:ResponseModel) {
        switch (response.statusCode) {
        case 200:
            self.unMatchSubjectResponse.onNext(true)
        default: break
        }
        Helper.hideProgressIndicator()
    }
}

