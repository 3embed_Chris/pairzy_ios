//
//  MessageTableViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 16/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageOutlet: UIImageView!
    @IBOutlet weak var userNameOutlet: UILabel!
    @IBOutlet weak var messageTimeLabelOutlet: UILabel!
    
    var userData : Messages!{
        didSet{
            if let userName = userData.userName, let userImage = userData.userImage, let messageTime = userData.messageTime{
                self.userImageOutlet.image = userImage
                self.userNameOutlet.text = userName
                self.messageTimeLabelOutlet.text = Utility().relativePast(for: messageTime)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
