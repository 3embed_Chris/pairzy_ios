//
//  MessagesViewController.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 16/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController {
    
    struct Constants {
        static let cellIdentifierForUnreadMessage = "MessageWithUnreadmessageTableViewCell"
        static let cellIdentifierForMessages = "MessageTableViewCell"
    }
    
    @IBOutlet weak var tableViewOutlet: UITableView!
    var users = Messages.getAllUsers()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MessagesViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:MessageTableViewCell
        if indexPath.row == 0{
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifierForUnreadMessage, for: indexPath) as! MessageTableViewCell
        }else{
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifierForMessages, for: indexPath) as! MessageTableViewCell
        }
        cell.userData = self.users[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
