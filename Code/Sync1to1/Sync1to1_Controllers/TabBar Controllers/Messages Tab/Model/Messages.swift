//
//  Messages.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 16/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

class Messages : NSObject {
    var userImage : UIImage?
    var userName : String?
    var messageTime : Date?
    
    init(userImage : UIImage, userName : String,messageTime:Date) {
        self.userName = userName
        self.userImage = userImage
        self.messageTime = messageTime
    }
    
    class func getAllUsers() -> [Messages]{
        let messages = UsersData().userDetails
        return messages
    }
}

class UsersData{
    var userDetails:[Messages] = {
        
        let lola = Messages(userImage: #imageLiteral(resourceName: "account"), userName: "Anthony", messageTime: Calendar.current.date(byAdding: .minute, value: -1, to: Date())!)
        
        let eula = Messages(userImage: #imageLiteral(resourceName: "account"), userName: "Joe", messageTime: Calendar.current.date(byAdding: .hour, value: -1, to: Date())!)
        
        let frank = Messages(userImage: #imageLiteral(resourceName: "account"), userName: "Hallie", messageTime: Calendar.current.date(byAdding: .day, value: -1, to: Date())!)
        
        let minnie = Messages(userImage: #imageLiteral(resourceName: "account"), userName: "Mitchell", messageTime: Calendar.current.date(byAdding: .month, value: -1, to: Date())!)
        
        return [lola,eula,frank,minnie]
    }()
}
