  //
  //  SyncDatesViewController.Swift
  //  Sync1to1
  //
  //  Created by Dinesh Guptha Bavirisetti on 14/06/17.
  //  Copyright © 2017 Rahul Sharma. All rights reserved.
  //
  
  import UIKit
  import RxSwift
  import EventKit
  import CocoaLumberjack
  import Contacts
  import Kingfisher
  
  class SyncDatesViewController: UIViewController {
    
    @IBOutlet weak var containerViewOutlet: UIView!
    @IBOutlet weak var nowButtonOutlet: UIButton!
    @IBOutlet weak var futureButtonOutlet: UIButton!
    @IBOutlet weak var pastButtonOutlet: UIButton!
    @IBOutlet weak var nowTableView: UITableView!
    @IBOutlet weak var futureTableView: UITableView!
    @IBOutlet weak var pastTableView: UITableView!
    @IBOutlet weak var moableViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var movableView: UIView!
    @IBOutlet weak var topBadgeView: UIView!
    @IBOutlet weak var pendingCount: UILabel!
    @IBOutlet weak var topTitlesView: UIView!
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var balanceBtn: UIButton!
    @IBOutlet weak var noContentView: UIView!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyScreenTitle: UILabel!
    @IBOutlet weak var emptyScreenMsg: UILabel!
    @IBOutlet weak var upcomingEmptyTitle: UILabel!
    @IBOutlet weak var upcomingEmptyMsg: UILabel!
    @IBOutlet weak var pastEmptyTitle: UILabel!
    @IBOutlet weak var pastEmptyMsg: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    let syncDateVm = SyncDateeRequestVM()
    var currentLabel = 0
    var isConfirmBtnTapped = false
    var selectedIndex = 0
    
    
    var tableView = UITableView()
    
    
    
    struct  Constants {
        static let nowWithImageCellIdentifier = "NowWithImageTableViewCell"
        static let nowMissedCellIdentifier = "NowMissedTableViewCell"
        static let futurePendingDateCellIdentifier = "FuturePedingDatesTableViewCell"
        static let futureScheduleCellIdentifier = "FutureScheduledTableViewCell"
        static let pastNoDateCellIdentifier = "PastNoDateTableViewCell"
        static let pastDateWithRatingCellIdentifier = "PastDateWithRatingsTableViewCell"
        static let headerViewCellIdentifier = "HeaderViewTableViewCell"
        static let openCalenderPickerSegue = "openCalenderPicker"
    }
    
    var lightBlueColor: UIColor!{
        var color = UIColor.blue
        if #available(iOS 10.0, *) {
            color = UIColor(displayP3Red: 0.0, green: 0.659, blue: 1.0, alpha: 1.0)
        }
        return color
    }
    
    var pendingRefreshControl: UIRefreshControl = UIRefreshControl()
    var pastRefreshControl: UIRefreshControl = UIRefreshControl()
    var upcomingRefreshControl: UIRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nowButtonOutlet.isSelected = true
        addObservers()
        topBadgeView.layer.cornerRadius = topBadgeView.frame.size.height/2
        Helper.setShadow(sender: movableView)
        Helper.setShadow(sender: topBadgeView)
        
        nowButtonOutlet.setTitle(StringConstants.pending(), for: .normal)
        futureButtonOutlet.setTitle(StringConstants.upcoming(), for: .normal)
        pastButtonOutlet.setTitle(StringConstants.pastDates(), for: .normal)
        pastButtonOutlet.titleLabel?.numberOfLines = 2
        
        syncDateVm.pendingTableView = self.nowTableView
        syncDateVm.pastTableView = self.pastTableView
        syncDateVm.upcomingTableView = self.futureTableView
        
        emptyScreenTitle.text = StringConstants.pendingDateMsg()
        emptyScreenMsg.text = StringConstants.pleaseSchedule()
        
        upcomingEmptyTitle.text = StringConstants.upcomingDateMsg()
        upcomingEmptyMsg.text = StringConstants.pleaseSchedule()
        
        pastEmptyTitle.text = StringConstants.pastDateMsg()
        pastEmptyMsg.text = StringConstants.pleaseSchedule()
        
        pendingRefreshControl.tintColor = Colors.AppBaseColor
        pendingRefreshControl.addTarget(self, action: #selector(SyncDatesViewController.refresh), for: UIControlEvents.valueChanged)
        
        pastRefreshControl.tintColor = Colors.AppBaseColor
        pastRefreshControl.addTarget(self, action: #selector(SyncDatesViewController.refresh), for: UIControlEvents.valueChanged)
        
        upcomingRefreshControl.tintColor = Colors.AppBaseColor
        upcomingRefreshControl.addTarget(self, action: #selector(SyncDatesViewController.refresh), for: UIControlEvents.valueChanged)
        
        nowTableView.addSubview(pendingRefreshControl)
        pastTableView.addSubview(pastRefreshControl)
        futureTableView.addSubview(upcomingRefreshControl)
        
        balanceBtn.layer.borderColor = Colors.coinBalanceBtnBorder.cgColor
        balanceBtn.layer.borderWidth = 1
        balanceBtn.layer.cornerRadius = 5
        balanceBtn.layer.cornerRadius = balanceBtn.frame.size.height/2
        movableView.backgroundColor = Colors.AppBaseColor
        topBadgeView.backgroundColor = Colors.AppBaseColor
        receivedAPIResponse()
    }
    
    
   @objc func refresh(senderRC: UIRefreshControl) {
        syncDateVm.index = 0
        syncDateVm.datesOffset = 0
        syncDateVm.datesLimit = 20
        getData()
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            senderRC.endRefreshing()
        }
    }
    
    
    func getData(){
        switch currentLabel {
        case 2:
            self.requestForPast()
            break
        default:
            self.requestForNowProfiles()
            break
        }
    }
    
    
    @IBAction func viewProfileButtonAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: SegueIds.DatesVCToProfileVC, sender: nil)
        
    }
    
    func openProfileByID(data: SyncDate) {
        let profileVc = self.storyboard?.instantiateViewController(withIdentifier:StoryBoardIdentifier.profileVc) as! ProfileDetailVC
        //        profileVc.profileUpdateDelegate = self as! profileInterestUpdated
        profileVc.profileDeatilVM.dataAvailable = false
        // profileVc.profileDeatilVM.showProfileByID = true
        profileVc.profileDeatilVM.profileDetails.userId = data.profileId
        profileVc.profileDeatilVM.profileDetails.gender = data.gender
        self.navigationController?.present(profileVc, animated: true, completion:nil)
    }
    
    @IBAction func openProfileActionForPendingDate(_ sender: UIButton) {
        openProfileByID(data: self.syncDateVm.pendingProfiles[selectedIndex])
        
    }
    @IBAction func openProfileActionForUpcomingDate(_ sender: UIButton) {
        openProfileByID(data: self.syncDateVm.upcomingProfiles[selectedIndex])
        
    }
    @objc func moveToDatesTab(notification: NSNotification) {
        if let typeOfDate = notification.object as? Int {
            if typeOfDate == 2 {
                
                self.selectePendingTab()
            } else if typeOfDate == 1 {
                self.selecteFutureTab()
            }
        }
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(showFutureTab), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForSyncDatePushTapped), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateDatesData), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForRefreshFutureTab), object: nil)
        //adding observer to refresh match data when any new match oocurs
        NotificationCenter.default.addObserver(self, selector: #selector(requestForNowProfiles), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForUnmatchProfile), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadFastTableView), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForRating), object: nil)
        
        let nameOfObserVer = NSNotification.Name(rawValue: "openDateTab")
        NotificationCenter.default.addObserver(self, selector: #selector(moveToDatesTab(notification:)), name: nameOfObserVer, object: nil)
        NotificationCenter.default.setObserver(self, selector: #selector(updateCoinBalance), name: NSNotification.Name(rawValue: "updateCoinsBal"), object: nil)
    }
    @objc func updateDatesData(notification:NSNotification){
        if(notification.userInfo == nil) {
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when){
                 self.requestForNowProfiles()
            }
        }
        requestForNowProfiles()
    }
    
    @objc func showFutureTab () {
        self.mainScrollView.contentOffset.x = self.view.frame.size.width
    }
    
   @objc func reloadFastTableView(){
        self.showAlertForRating(title: StringConstants.ThanksForRating())
        syncDateVm.getPastProfiles()
        pastTableView.reloadData()
    }
    
    func reloadTableview() {
        if(self.syncDateVm.upcomingProfiles.count > 0) {
            self.futureTableView.reloadData()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        syncDateVm.index = 0
        syncDateVm.datesOffset = 0
        syncDateVm.datesLimit = 20
        self.requestForNowProfiles()
        self.requestForPast()
        setProfileImage(profileImage: profileImageView)
        setUpProfileImageUI(profileImage: profileImageView)
        if self.syncDateVm.pendingProfiles.count == 0 {
            self.topBadgeView.isHidden = true
        }else {
            self.topBadgeView.isHidden = false
            pendingCount.text = String(self.syncDateVm.pendingProfiles.count)
        }
        if (self.syncDateVm.pendingProfiles.count) == 0 {
            self.tabBarController?.tabBar.items?[2].badgeValue = nil
        } else {
            self.tabBarController?.tabBar.items?[2].badgeValue = "\(self.syncDateVm.pendingProfiles.count)"
        }
        
        UserDefaults.standard.setValue(self.syncDateVm.pendingProfiles.count, forKey:"pendingDateCount")
        UserDefaults.standard.synchronize()
        updateCoinBalance()
        self.navigationController?.navigationBar.isHidden = false

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func callButtonAction(_ sender: Any) {
        let button  = sender as! UIButton
//        self.perform(#selector(self.enableCallButton(_:)), with: self, afterDelay: 0.2)
        button.isEnabled = false
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            button.isEnabled = true
        }
        let selectedIndex = button.tag - 3000
        let selectedProfile = self.syncDateVm.upcomingProfiles[selectedIndex]        
        if self.syncDateVm.upcomingProfiles[selectedIndex].isLiveDate == 1{
            
            if selectedProfile.dateType == 1 {
                //video Date
                self.startVideoCall(data: selectedProfile)
                self.removeObserer()
                self.navigationController?.dismiss(animated: false, completion: nil)
                
            }else if selectedProfile.dateType == 3 {
                // audio date
                self.startAudioCall(profile: selectedProfile)
                
            }else {
                
                //phisical Date
                let msgStr = "(\(selectedProfile.latitude),\(selectedProfile.longitude))@@\(selectedProfile.placeName)@@ "
                self.performSegue(withIdentifier: "dateToLocationViewer", sender: msgStr)
                
            }
            //  cell.callButton.isEnabled = true
        }else{
            
            if selectedProfile.dateType == 1 {
                let msg = StringConstants.pleaseWaitForVideo()
                Helper.showAlertWithMessage(withTitle: msg, message: "", onViewController: self)
            }else if selectedProfile.dateType == 3 {
                let msg = StringConstants.plzWaitForAudio()
                Helper.showAlertWithMessage(withTitle: msg, message: "", onViewController: self)
            }else {
                
//                let msg = StringConstants.plzWaitForTime()
//                Helper.showAlertWithMessage(withTitle: msg, message: "", onViewController: self)
                let msgStr = "(\(selectedProfile.latitude),\(selectedProfile.longitude))@@\(selectedProfile.placeName)@@ "
                self.performSegue(withIdentifier: "dateToLocationViewer", sender: msgStr)
                
            }
            
        }
    }
    
    
    @objc func enableCallButton(_ callBtn: UIButton){
//        callBtn.isEnabled = true
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "dateToLocationViewer"{
            if let controller = segue.destination as? LocationViewerViewController, let latLongStr = sender as? String {
                controller.currentLatLong = latLongStr
                controller.isFromDate = true
            }
        }
    }
    
    
    @IBAction func nowButtonAction(_ sender: Any)
    {
        selectePendingTab()
    }
    
    func selectePendingTab(){
        currentLabel = 0
        if !nowButtonOutlet.isSelected
        {
            self.setNowButtonSelected()
        }
        self.requestForNowProfiles()
    }
    func setUpProfileImageUI(profileImage:UIImageView){
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        profileImage.layer.borderWidth = 1
        profileImage.layer.borderColor = UIColor.white.cgColor
    }
    
    func setProfileImage(profileImage:UIImageView){
        let userData = Database.shared.fetchResults()
        if let userDetails = userData.first{
            if let urlString:String = userDetails.profilePictureURL {
                if urlString.count>0 {
                    let url = URL(string: urlString)
                    var imageName = "man_default"
                    if userDetails.gender == "Male" {
                        imageName = "man_default"
                    }else {
                        imageName = "woman_default"
                    }
                    self.profileImageView.kf.setImage(with:url, placeholder:UIImage.init(named:"02m"))
                }else{
                    profileImage.image = #imageLiteral(resourceName: "Splash")
                }
            }
        }
    }
    
    @IBAction func futureDateButtonAction(_ sender: Any)
    {
        selecteFutureTab()
    }
    
    func selecteFutureTab() {
        currentLabel = 1
        if !futureButtonOutlet.isSelected{
            self.setfutureButtonSelected()
        }
        
        self.requestForNowProfiles()
    }
    
    @IBAction func pastButtonAction(_ sender: Any)
    {
        currentLabel = 2
        if !pastButtonOutlet.isSelected{
            self.setpastButtonSelected()
        }
        self.requestForPast()
    }
    
    @IBAction func confirmBtnAction(_ sender: Any) {
        let btn = sender as! UIButton
        self.showAlertToConform(status: 1, tag: btn.tag)
        
        
    }
    @IBAction func reScheduleBtnAction(_ sender: Any) {
        let btn = sender as! UIButton
        self.showAlertToConform(status: 3, tag: btn.tag)
        
        
        
    }
    @IBAction func cancelBtnAction(_ sender: Any) {
        let btn = sender as! UIButton
        self.showAlertToConform(status: 2, tag: btn.tag)
        
    }
    
    
    
    @IBAction func ratingBtnAction(_ sender: UIButton) {
        let vc = UIStoryboard(name:"DatumTabBarControllers", bundle: nil).instantiateViewController(withIdentifier: "ratingVC") as! RatingWithMessageViewController
        vc.ratingProfile = self.syncDateVm.pastProfiles[sender.tag]
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    @IBAction func balanceBtnAction(_ sender: Any) {
        //checking coins plans details from  appstore available or not.
        if(Helper.getCoinPLansFromAppStore().count > 0) {
            let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "addCoin") as! AddCoinsVC
            self.navigationController?.pushViewController(userBasics, animated: true)
        }
    }
    
   @objc func updateCoinBalance(){
        var balance = " 0"
        if let getBal = UserDefaults.standard.object(forKey: "coinBalance") as? Int {
            if getBal > 1000 {
                let floatVal = Float(Float(getBal)/1000.0)
                let inter = floor(floatVal)
                let decimal = floatVal.truncatingRemainder(dividingBy: 1)
                if decimal == 0.0{
                    balance = "\(Int(inter))k"
                }else {
                    balance = "\(Float(Float(getBal)/1000.0))k"
                }
            } else {
                balance = " " + String(getBal)
            }
            balanceBtn.setTitle(balance, for: .normal)
        } else {
            balanceBtn.setTitle(" 0", for: .normal)
        }
    }
    
    func showAlertForRating(title:String){
        
        // ThanksForRating
        let alert = UIAlertController(title: title, message:nil, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: StringConstants.ok(), style: .cancel, handler: {(action:UIAlertAction!) in
            
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setNowButtonSelected()  //pending
    {
        self.updateScrollViewFrame(newOffsetPoint: 0)
    }
    
    func setfutureButtonSelected() //upcoming
    {
        self.updateScrollViewFrame(newOffsetPoint: self.view.frame.size.width)
    }
    
    func setpastButtonSelected() //past
    {
        self.updateScrollViewFrame(newOffsetPoint: self.view.frame.size.width*2)
    }
    
    
    /// method for updating button state based on condition.
    func updateButtonState() {
        
        if self.mainScrollView.contentOffset.x == 0 {
            nowButtonOutlet.isSelected = true
            futureButtonOutlet.isSelected = false
            pastButtonOutlet.isSelected = false
            
            ///requesting for get matches api ,
            //            if(self.matchesVm.profilesArrayData.count == 0) {
            //                self.requestForMatches()
            //            }
            
        } else if (self.mainScrollView.contentOffset.x == self.view.frame.size.width){
            nowButtonOutlet.isSelected = false
            futureButtonOutlet.isSelected = true
            pastButtonOutlet.isSelected = false
            
            ///requesting for get instant matches api,
            //            if(self.instantMatchesVm.profilesArrayData.count == 0) {
            //                self.requestForInstantMatches()
            //            }
            
        } else if(self.mainScrollView.contentOffset.x == self.view.frame.size.width*2){
            nowButtonOutlet.isSelected = false
            futureButtonOutlet.isSelected = false
            pastButtonOutlet.isSelected = true
            
            ///requesting for get second look profiles api.
            //            if(self.secondLookVm.profilesArrayData.count == 0) {
            //                self.requestForSecondLook()
            //            }
        }
    }
    
    @objc func requestForNowProfiles()  {
        syncDateVm.getNowPrfiles()
    }
    
    
    
    func requestForPast()  {
        syncDateVm.getPastProfiles()
    }
    
    
    
  }
  
  
  
  //MARK: - ScrollView Delegates
  
  extension SyncDatesViewController : UIScrollViewDelegate {
    
    /// method will trigger when user scrolling the scroll view.
    ///
    /// - Parameter scrollView: current scrollview.
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainScrollView {
            let scrollContentOffset = self.mainScrollView.contentOffset
            self.moableViewLeadingConstraint.constant = scrollContentOffset.x/3
            self.updateButtonState()
            
            
        }
        let movedOffset: CGFloat = scrollView.contentOffset.y
        if scrollView == self.mainScrollView {
            self.view.endEditing(true)
            let pageWidth: CGFloat = 350          //171 - 64
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            
            if self.navigationController != nil {
                
                if ratio > 0 {
                    
                    // Helper.addShadowToNavigationBar(controller: self.navigationController!)
                    Helper.setShadow(sender: topTitlesView)
                }else {
                    
                    // Helper.removeShadowToNavigationBar(controller: self.navigationController!)
                    Helper.removeShadow(sender: topTitlesView)
                }
                
            }
        }
        
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        currentLabel = Int(pageNumber)
        
        if scrollView == self.mainScrollView {
            
            switch currentLabel {
            case 0:
                nowTableView.addSubview(self.pendingRefreshControl)
                self.requestForNowProfiles()
                break
            case 1:
                
                futureTableView.addSubview(self.upcomingRefreshControl)
                self.requestForNowProfiles()
                break
            case 2:
                
                pastTableView.addSubview(self.pastRefreshControl)
                self.requestForPast()
                break
            default:
                break
                
            }
            
        }
    }
    
    /// method for updting frame.
    ///
    /// - Parameter newOffsetPoint: point for new frame.
    func updateScrollViewFrame(newOffsetPoint:CGFloat) {
        var scrollContentOffset = self.mainScrollView.contentOffset
        scrollContentOffset.x = newOffsetPoint
        self.mainScrollView.setContentOffset(scrollContentOffset, animated: true)
    }
  }
  
  
  extension SyncDatesViewController:UITableViewDataSource,UITableViewDelegate {
    
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        switch tableView.tag {
        case 0:
            //for pending profiles
            
            if(self.syncDateVm.pendingProfiles.count > 0) {
                self.nowTableView.backgroundView = nil
                tableView.backgroundColor = UIColor.white
            }else {
                tableView.backgroundColor = UIColor.clear
            }
            return self.syncDateVm.pendingProfiles.count
        case 1:
            
            if(self.syncDateVm.upcomingProfiles.count > 0) {
                self.futureTableView.backgroundView = nil
                tableView.backgroundColor = UIColor.white
            }else {
                tableView.backgroundColor = UIColor.clear
            }
            return self.syncDateVm.upcomingProfiles.count
            //for upcomingProfiles
            
            
        default:
            if(self.syncDateVm.pastProfiles.count > 0) {
                self.pastTableView.backgroundView = nil
                tableView.backgroundColor = UIColor.white
            }else {
                tableView.backgroundColor = UIColor.clear
            }
            return self.syncDateVm.pastProfiles.count
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        switch tableView.tag {
        case 0:
            if indexPath.row + 1 == self.syncDateVm.pendingProfiles.count {
                if self.syncDateVm.pendingProfiles.count >= 20 {
                    self.syncDateVm.index = syncDateVm.index + 1
                    self.syncDateVm.datesOffset = syncDateVm.index * 20
                    self.syncDateVm.datesLimit = (syncDateVm.index+1) * 20
                    self.getData()
                }
                
            }
            if indexPath.row  == 0 {
                self.syncDateVm.index = self.syncDateVm.pendingProfiles.count/20
                self.syncDateVm.datesOffset = 0
                self.syncDateVm.datesLimit = 20
                
            }
            break
        case 1:
            if indexPath.row + 1 == self.syncDateVm.upcomingProfiles.count {
                if self.syncDateVm.upcomingProfiles.count >= 20 {
                    self.syncDateVm.index = syncDateVm.index + 1
                    self.syncDateVm.datesOffset = syncDateVm.index * 20
                    self.syncDateVm.datesLimit = (syncDateVm.index+1) * 20
                    self.getData()
                }
                
            }
            if indexPath.row  == 0 {
                self.syncDateVm.index = self.syncDateVm.upcomingProfiles.count/20
                self.syncDateVm.datesOffset = 0
                self.syncDateVm.datesLimit = 20
                
            }
            break
        default:
            if indexPath.row + 1 == self.syncDateVm.pastProfiles.count {
                if self.syncDateVm.pastProfiles.count >= 20 {
                    self.syncDateVm.index = syncDateVm.index + 1
                    self.syncDateVm.datesOffset = syncDateVm.index * 20
                    self.syncDateVm.datesLimit = (syncDateVm.index+1) * 20
                    self.getData()
                }
                
            }
            if indexPath.row  == 0 {
                self.syncDateVm.index = self.syncDateVm.pastProfiles.count/20
                self.syncDateVm.datesOffset = 0
                self.syncDateVm.datesLimit = 20
                
            }
            break
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if (tableView == self.nowTableView) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.headerViewCellIdentifier) as! HeaderViewTableViewCell
            
            cell.cancelDateBtn.tag = indexPath.row
            cell.confirmBtn.tag = indexPath.row
            cell.rescheduleBtn.tag =  indexPath.row
            
            cell.initialsetup(flag: true)
            //cell.confirmBtnWidth.constant = (UIScreen.main.bounds.size.width/3 - 20)
            
            cell.updatePendingCell(data: self.syncDateVm.pendingProfiles[indexPath.row], isPendingCall: true)
            cell.confirmBtn.layer.cornerRadius = cell.confirmBtn.frame.size.height/2
            
            return cell
            
        } else if (tableView == self.futureTableView) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingCell") as! HeaderViewTableViewCell
            cell.initialsetup(flag: false)
            cell.updatePendingCell(data: self.syncDateVm.upcomingProfiles[indexPath.row], isPendingCall: false)
            cell.callButton.tag = 3000 + indexPath.row
            cell.callBtnView.layer.cornerRadius = 5
          //  cell.updateVideoCallBtn(data:self.syncDateVm.upcomingProfiles[indexPath.row])
            if self.syncDateVm.upcomingProfiles[indexPath.row].isLiveDate == 1{
                cell.callBtnGradient.setDiagonalGradient()
            }else {
                cell.callBtnGradient.disabledColor()
            }
            if self.syncDateVm.upcomingProfiles[indexPath.row].dateType == 1{
                cell.callButton.setTitle(StringConstants.videoCall(), for: .normal)
                cell.callButton.setImage(#imageLiteral(resourceName: "video"), for: .normal)
            }else if self.syncDateVm.upcomingProfiles[indexPath.row].dateType == 3{
                cell.callButton.setTitle(StringConstants.voiceCall(), for: .normal)
                cell.callButton.setImage(#imageLiteral(resourceName: "call"), for: .normal)
            }else {
                
                //  cell.callBtnGradient.setDiagonalGradient()
                cell.callButton.isEnabled = true
                cell.callButton.setImage(#imageLiteral(resourceName: "navigation"), for: .normal)
                cell.callButton.setTitle(StringConstants.getDirection(), for: .normal)
            }
            cell.rescheduleBtn.layer.cornerRadius =  cell.rescheduleBtn.frame.size.height/2
            return cell
            
        } else {
            var cell:PastNoDateTableViewCell!
            //  let profileDetails = self.syncDateVm.pastProfiles[indexPath.row]
            //            if(profileDetails.ratings != 100) {
            //                 cell = tableView.dequeueReusableCell(withIdentifier: Constants.pastDateWithRatingCellIdentifier, for: indexPath) as! PastNoDateTableViewCell
            //            } else {
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.pastNoDateCellIdentifier, for: indexPath) as! PastNoDateTableViewCell
            cell.ratingBtn.tag = indexPath.row
            cell.displayProfileDetails(profileDet: self.syncDateVm.pastProfiles[indexPath.row])
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == pastTableView {
             openProfileByID(data: self.syncDateVm.pastProfiles[indexPath.row])
        }
//        if tableView == nowTableView {
//            openProfileByID(data: self.syncDateVm.pendingProfiles[indexPath.row])
//
//        }else if tableView == futureTableView {
//
//
//            openProfileByID(data: self.syncDateVm.upcomingProfiles[indexPath.row])
//
//        }else {
//
//         openProfileByID(data: self.syncDateVm.pastProfiles[indexPath.row])
//
//        }
        selectedIndex = indexPath.row
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        switch tableView.tag {
            
        case 0:
            return 600  //tableView.frame.size.height + 60
            
        case 1:
            return 600  //tableView.frame.size.height + 60
        case 2:
            return 80
            
        default:
            return 0
            
        }
        
        
    }
    
    /// default screen when there is no profiles found
    ///
    /// - Parameters:
    ///   - tableView: is the tableview for displaying default view.
    ///   - title: title for default view.
    ///   - message: message for default view.
    ///   - imageName: image for default view.
    func viewForNoProfiles(tableView:UITableView,title:String,message:String,imageName:String) {
        emptyImage.image = UIImage.init(named: "prospectsEmpty") //prospectsEmpty
        //        emptyScreenTitle.text = title
        //        emptyScreenMsg.text = message
        tableView.backgroundColor = UIColor.clear
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.syncDateVm.pendingProfiles.count == 0 {
            self.topBadgeView.isHidden = true
        }else {
            self.topBadgeView.isHidden = false
            pendingCount.text = String(self.syncDateVm.pendingProfiles.count)
        }
        if (self.syncDateVm.pendingProfiles.count) == 0 {
            self.tabBarController?.tabBar.items?[2].badgeValue = nil
        } else {
            self.tabBarController?.tabBar.items?[2].badgeValue = "\(self.syncDateVm.pendingProfiles.count)"
        }
        
        UserDefaults.standard.setValue(self.syncDateVm.pendingProfiles.count, forKey:"pendingDateCount")
        UserDefaults.standard.synchronize()
        switch tableView.tag {
        case 2:
            //for past tableView.
            return 1
        default:
            return 1
        }
    }
    
    func rescheduleBtnTapped(){
        performSegue(withIdentifier: String(describing: ReScheduleVC.self), sender: self)
        
    }
    
  }
  
  extension SyncDatesViewController:FuturePendingCellDeleagate {
    
    func confirmButtonPressed(selectedButton: UIButton) {
        self.requestForSetDateStatus(status: 1, tag: selectedButton.tag)
    }
    
    func rejectButtonPressed(selectedButton: UIButton) {
        self.requestForSetDateStatus(status: 2, tag: selectedButton.tag)
    }
    
    func rescheduleButtonPressed(selectedButton: UIButton) {
        self.requestForSetDateStatus(status: 3, tag: selectedButton.tag)
    }
    //     accepted : 1 / denied : 2 / reschedule : 3
    
    
    func showAlertToConform(status:Int,tag:Int) {
        
        var alertmsg = ""
        
        switch status {
        case 1:
            let profile = self.syncDateVm.pendingProfiles[tag]
            
            alertmsg = String(format: StringConstants.areYouSureToConfirm(),profile.requestedFor,profile.nameOfTheProfile)
                //"Are you sure want to Confirm " + profile.requestedFor + " with " + profile.nameOfTheProfile
            
        case 2:
            if futureButtonOutlet.isSelected {
                let profile = self.syncDateVm.upcomingProfiles[tag]
                alertmsg = String(format: StringConstants.areYouSureToCancel(),profile.requestedFor,profile.nameOfTheProfile)
                    //"Are you sure want to cancel " +  profile.requestedFor + " with " + profile.nameOfTheProfile
            }else {
                
                let profile = self.syncDateVm.pendingProfiles[tag]
                
                
                if profile.isInitiatedByMe == 0 {
                    alertmsg = String(format: StringConstants.areYouSureToReject(),profile.requestedFor,profile.nameOfTheProfile)
                      //  "Are you sure want to reject " +  profile.requestedFor  + " with " + profile.nameOfTheProfile
                }else {
                    alertmsg = String(format: StringConstants.areYouSureToCancel(),profile.requestedFor,profile.nameOfTheProfile)
                        //"Are you sure want to cancel " +  profile.requestedFor  + " with " + profile.nameOfTheProfile
                }
                
            }
            
            break
        case 3:
            if futureButtonOutlet.isSelected {
                let profile = self.syncDateVm.upcomingProfiles[tag]
                alertmsg = String(format: StringConstants.areYouSureToReschedule(),profile.requestedFor,profile.nameOfTheProfile)
                    //"Are you sure want to reschedule " +  profile.requestedFor + " with " + profile.nameOfTheProfile
            }else {
                let profile = self.syncDateVm.pendingProfiles[tag]
                alertmsg = String(format: StringConstants.areYouSureToReschedule(),profile.requestedFor,profile.nameOfTheProfile)
                    //"Are you sure want to reschedule " +  profile.requestedFor + " with " + profile.nameOfTheProfile
            }
            
            
            break
            
        default:
            break
        }
        
        
        let alert:UIAlertController=UIAlertController(title: alertmsg, message: nil , preferredStyle: UIAlertControllerStyle.alert)
        
        let cameraAction = UIAlertAction(title: StringConstants.yes(), style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            switch status {
            case 1:
                self.requestForSetDateStatus(status: 1, tag: tag) //Confirm
                break
            case 2:
                self.requestForSetDateStatus(status: 2, tag: tag) //Cancel or Reject
                break
            case 3:
                self.requestForSetDateStatus(status: 3, tag: tag) // ReSchedule
                break
            default:
                break
            }
        }
        
        let cancelAction = UIAlertAction(title: StringConstants.cancel(), style: UIAlertActionStyle.default)
        {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func requestForSetDateStatus(status:Int,tag:Int) {
        
        if(status == 3) {
            
            var selectedDateForReschedule:SyncDate = SyncDate(profileDetails: [:])
            if futureButtonOutlet.isSelected {
                selectedDateForReschedule = self.syncDateVm.upcomingProfiles[tag]
            }else {
                
                selectedDateForReschedule = self.syncDateVm.pendingProfiles[tag]
            }
            
            
            let selectedTime = selectedDateForReschedule.dateTime
            let sheduleddate:Date = Date(timeIntervalSince1970: Double(selectedTime))
            let vc = UIStoryboard(name:"DatumTabBarControllers", bundle: nil).instantiateViewController(withIdentifier: "planDate") as! ReScheduleVC
            
            vc.createDateVM.previousSelectedDate = sheduleddate
            
            
            vc.createDateVM.resheduleProfile = selectedDateForReschedule
            vc.createDateVM.dateType = DateType(rawValue: selectedDateForReschedule.dateType)!
            if selectedDateForReschedule.dateType == 1 || selectedDateForReschedule.dateType == 3 {
                vc.createDateVM.isForCallDate = true
            }else {
                vc.createDateVM.isForCallDate = false
            }
            let preferenceNav: UINavigationController =  UINavigationController(rootViewController: vc)
            preferenceNav.isNavigationBarHidden = true
            
            self.present(preferenceNav, animated: true, completion: nil)
            
            
        } else {
            
            if status == 2 {
                if futureButtonOutlet.isSelected {
                    Helper.showProgressIndicator(withMessage: StringConstants.Loading())
                    if self.syncDateVm.upcomingProfiles.count != 0 {
                         syncDateVm.setDateStatus(profile: self.syncDateVm.upcomingProfiles[tag], dateStatus:status)
                    }
                   
                }else {
                    Helper.showProgressIndicator(withMessage: StringConstants.Loading())
                    if self.syncDateVm.pendingProfiles.count != 0 {
                        syncDateVm.setDateStatus(profile: self.syncDateVm.pendingProfiles[tag], dateStatus:status)

                    }
                }
            }else {
                Helper.showProgressIndicator(withMessage: StringConstants.Loading())
                syncDateVm.setDateStatus(profile: self.syncDateVm.pendingProfiles[tag], dateStatus:status)
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            let name = NSNotification.Name(rawValue: "updateActionsCountForAds")
            NotificationCenter.default.post(name: name, object: self, userInfo: nil)
        }
    }
    
    
  }
  
  
  extension Date {
    
    func offsetFrom(date: Date) -> String {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: self);
        
        let seconds = "\(difference.second ?? 0)s"
        let minutes = "\(difference.minute ?? 0)m" + " " + seconds
        let hours = "\(difference.hour ?? 0)h" + " " + minutes
        let days = "\(difference.day ?? 0)d" + " " + hours
        
        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
        if let second = difference.second, second > 0 { return seconds }
        return ""
    }
    
    
    func isDateIsPast(date: Date) -> Bool {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: self)
  
        if let day = difference.day, day          > 0 { return false }
        if let hour = difference.hour, hour       > 0 { return false }
        if let minute = difference.minute, minute > 0 { return false }
        if let second = difference.second, second > 0 { return false }
        return true
    }
  }
  
  extension SyncDatesViewController {
    
    func receivedAPIResponse(){
        
        self.syncDateVm.syncDateSubjectResponse.subscribe(onNext: { success in
            
            switch success.0 {
                
            case .currentDates:
                
                if success.1 {
                    
                    self.nowTableView.backgroundView = nil
                    if(self.syncDateVm.pendingProfiles.count == 0  || self.syncDateVm.upcomingProfiles.count == 0) {
                        self.viewForNoProfiles(tableView: self.nowTableView, title:StringConstants.Future_Pending_Message(), message:StringConstants.Past_Message() , imageName:"prospectsEmpty")
                        
                        self.viewForNoProfiles(tableView: self.futureTableView, title:StringConstants.Upcoming_Title(), message:StringConstants.Past_Message() , imageName:"prospectsEmpty")
                    }
                    
                    
                }else {
                    Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: self.syncDateVm.errorMsg, onViewController: self)
                }
                
                self.futureTableView.reloadData()
                self.nowTableView.reloadData()
                break
            case .pastDates:
                
                if success.1 {
                    
                }else {
                    Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: self.syncDateVm.errorMsg, onViewController: self)
                }
                
                self.pastTableView.backgroundView = nil
                if(self.syncDateVm.pastProfiles.count == 0) {
                    self.viewForNoProfiles(tableView: self.pastTableView, title:StringConstants.Past_Title(), message:StringConstants.Past_Message() , imageName:"prospectsEmpty")
                }
                self.pastTableView.reloadData()
                
                break
            case .dateResponse:
                
                if success.1 {
                    self.requestForNowProfiles()
                }else {
                    Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: self.syncDateVm.errorMsg, onViewController: self)
                }
                
                break
            }
            
        }).addDisposableTo(self.syncDateVm.disposeBag)
        
    }
    
    
    /// method for intiate video call.
    ///
    /// - Parameter data: contains profile details.
    func startVideoCall(data:SyncDate) {
        
        guard let ownID = Helper.getMQTTID() else { return }
        MQTTCallManager.sendcallAvilibilityStatus(status: 0, topic: MQTTTopic.callsAvailability + ownID)
        UserDefaults.standard.set(true, forKey: "iscallBtnCliked")
        
        let userID = data.profileId
        let registerNum = data.mobileNumber
        //data.mobileNumber
        let dict = ["callerId": userID,
                    "callType" : CallTypes.videoCall ,
                    "registerNum": registerNum,
                    "callId": randomString(length: 100),
                    "callerIdentifier": ownID,
                    "dateId":data.dateId] as [String:Any]
        
        UserDefaults.standard.set(dict, forKey: "storeIndexPath")
        MQTT.sharedInstance.subscribeTopic(withTopicName: MQTTTopic.callsAvailability + userID , withDelivering: .atLeastOnce)
        
        //for VideoCall
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = IncomingVideocallView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.tag =  17
        videoView.setCallId()
        videoView.otherCallerId = userID
        videoView.calling_userName.text = String(format:"%@",(data.nameOfTheProfile))
        
        let url = URL(string:data.profilePicutre)
        videoView.userImageView.kf.setImage(with:url, placeholder:UIImage.init(named:"02m"))
        videoView.addCameraView()
        window.addSubview(videoView)
    }
    
    func removeObserer() {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func startAudioCall(profile:SyncDate){
        if Helper.checkCallGoingOn() == true{
            return
        }
        guard let ownID = Helper.getMQTTID() else { return }
        MQTTCallManager.sendcallAvilibilityStatus(status: 0, topic: MQTTTopic.callsAvailability + ownID)
        UserDefaults.standard.set(true, forKey: "iscallBtnCliked")
        
        let userID = profile.profileId
        let registerNum = profile.profileId
        
        let dict = ["callerId": userID,
                    "callType" : CallTypes.audioCall,
                    "registerNum": registerNum,
                    "callId": randomString(length: 100),
                    "callerIdentifier": "",
                    "dateId":profile.dateId] as [String:Any]
        
        UserDefaults.standard.set(dict, forKey: "storeIndexPath")
        DDLogDebug("subscribe channel = \(MQTTTopic.callsAvailability + userID)")
        MQTT.sharedInstance.subscribeTopic(withTopicName: MQTTTopic.callsAvailability + userID , withDelivering: .atLeastOnce)
        let window = UIApplication.shared.keyWindow!
        let audioView = AudioCallView(frame: CGRect(x:0, y:0, width: window.frame.width, height: window.frame.height))
        audioView.tag = 15
        audioView.userNameLbl.text = String(format:"%@",profile.nameOfTheProfile)
        
        audioView.callerID = userID
        audioView.playSound("calling", loop: 40)
        audioView.setMessageData(messageData: dict)
        if let profileIm = profile.profilePicutre as? String{
            if profileIm.count == 0 { audioView.backImageView.backgroundColor = .black } else {
                audioView.backImageView.backgroundColor = .clear
            }
        }
        
        
        
        if let profilepic = profile.profilePicutre as? String {
            audioView.backImageView.kf.setImage(with: URL(string: profilepic), placeholder: #imageLiteral(resourceName: "02m"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
            })
            
            audioView.userImageView.kf.setImage(with: URL(string:profilepic), placeholder: #imageLiteral(resourceName: "02m"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
            })
            
        } else {
            audioView.userImageView.image = #imageLiteral(resourceName: "02m")
            audioView.backImageView.image = #imageLiteral(resourceName: "02m")
        }
        window.addSubview(audioView);
        window.bringSubview(toFront: audioView)
    }
    
    
  }
  
  
  
  
  
