//
//  SyncDate.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 15/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

class SyncDate : NSObject {
    
    var dateId:String = ""
    var isInitiatedByMe:Int = 0
    var profileId:String = ""
    var nameOfTheProfile : String = ""
    var profilePicutre :String = ""
    var creationTime:Double = 0.0
    var dateTime : Double = 0.0
    var enableDateTime: Double = 0.0
    var placeName: String = ""
    var requestedFor:String = ""
    var dateType:Int = 0
    var latitude:Double = 0.0
    var longitude:Double = 0.0
    var onlineStatus:Int = 0
    var dateStatus:String = ""
    var mobileNumber:String = ""
    var isNoDate : Bool = false
    var ratings : Int = 100
    var gender:String = ""
    var isLiveDate:Int = 0
    
    init(profileDetails:[String:Any]) {
        
        if let profilePic = profileDetails[SyncProfile.profilepic] as? String {
            self.profilePicutre = profilePic
        }
        
        if let name = profileDetails[SyncProfile.name] as? String {
            self.nameOfTheProfile = name
        }
        
        if let profileId = profileDetails[SyncProfile.profileID] as? String {
            self.profileId = profileId
        }
        
        if let initatedBy = profileDetails[SyncProfile.InitiatedBy] as? Int {
            self.isInitiatedByMe = initatedBy
        }
        if let requestedFor = profileDetails[SyncProfile.requestedFor] as? String {
            self.requestedFor = requestedFor
        }
        
        if let id = profileDetails[SyncProfile.dateId] as? String {
            self.dateId = id
        }
        
        if let islive = profileDetails[SyncProfile.isLiveDate] as? Int {
            self.isLiveDate = islive
        }
        
        if let syncDate = profileDetails[SyncProfile.dateTime] as? Double {
            self.dateTime = syncDate
            //self.dateTime = syncDate/1000
            self.enableDateTime = ((dateTime)-(60*1000))
            
        }
        
        if let missedCall = profileDetails[SyncProfile.creationTime] as? Double {
            self.creationTime = missedCall/1000
        }
        
        if let place = profileDetails[SyncProfile.PlaceName] as? String {
            self.placeName = place
        }
        
        if let dateType = profileDetails[SyncProfile.dateType] as? Int {
            self.dateType = dateType
        }
        
        if let lat = profileDetails[SyncProfile.latitude] as? Double {
            self.latitude = lat
        }
        
        
        
        
        
        if let longitude = profileDetails[SyncProfile.longitude] as? Double {
            self.longitude = longitude
        }
        
        if let onlineStatus = profileDetails[profileData.onlineStatus] as? Int {
            self.onlineStatus = onlineStatus
        }
        if let rating = profileDetails[SyncProfile.rating] as? Int {
            self.ratings = rating
        }
        
        if let dateStatus = profileDetails[SyncProfile.opponentResponse] as? String {
            self.dateStatus = dateStatus
        }
        
        if let gender = profileDetails[profileData.Gender] as? String{
            self.gender = gender
        }
     
    }
    
}
