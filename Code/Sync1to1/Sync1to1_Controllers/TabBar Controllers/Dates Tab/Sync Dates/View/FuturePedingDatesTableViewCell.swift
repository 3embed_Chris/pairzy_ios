//
//  FuturePedingDatesTableViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 15/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

protocol FuturePendingCellDeleagate: class {
    func rescheduleButtonPressed(selectedButton:UIButton)
    func confirmButtonPressed(selectedButton:UIButton)
    func rejectButtonPressed(selectedButton:UIButton)

}

class FuturePedingDatesTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageOutlet: UIImageView!
    @IBOutlet weak var userNameOutlet: UILabel!
    
    @IBOutlet weak var timeForDateLabelOutlet: UILabel!
    @IBOutlet weak var dateForDateLabelOutlet: UILabel!
    
    @IBOutlet weak var confirmButtonOutlet: UIButton!
    
    @IBOutlet weak var rejectButtonOutlet: UIButton!
    
    @IBOutlet weak var resheduleButtonOutlet: UIButton!
    
    
    
    weak var futurePendingCellDelegate : FuturePendingCellDeleagate? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func displayProfileDetails(profileDet:SyncDate) {
        self.userNameOutlet.text = profileDet.nameOfTheProfile
        
        let url = URL(string: profileDet.profilePicutre)
        self.userImageOutlet.kf.setImage(with:url, placeholder:UIImage.init(named:"02m"))
        
        
        let  date:Date = Date(timeIntervalSince1970: Double(profileDet.dateTime))
        self.timeForDateLabelOutlet.text = CalenderUtility.getCurrentTime(fromDate: date)
        self.dateForDateLabelOutlet.text = CalenderUtility.getCurrentMonthAndDayWithYear(fromDate: date)
        
        self.updateActivityButtonsStatus(initatedBy: profileDet.isInitiatedByMe)
        
        print(date)
    }
    
    
    func updateActivityButtonsStatus(initatedBy:Int) {
        if(initatedBy == 1) {
            self.confirmButtonOutlet.isEnabled = false
//            self.resheduleButtonOutlet.isEnabled = false
//            self.rejectButtonOutlet.isEnabled = false
        } else {
            self.confirmButtonOutlet.isEnabled = true
            self.resheduleButtonOutlet.isEnabled = true
            self.rejectButtonOutlet.isEnabled = true
        }
    }
    
    func updateTimeRemainingLabel(timeStamp:Double) {
        
    }
    
    
    func addTagForButton(tagForButton:Int) {
        self.confirmButtonOutlet.tag = tagForButton
        self.rejectButtonOutlet.tag = tagForButton
        self.resheduleButtonOutlet.tag = tagForButton
    }
    
    @IBAction func rejectButtonAction(_ sender: Any){
        if (self.futurePendingCellDelegate != nil){
            self.futurePendingCellDelegate?.rejectButtonPressed(selectedButton : sender as! UIButton)
        }
    }
    
    @IBAction func rescheduleButtonAction(_ sender: Any){
        if (self.futurePendingCellDelegate != nil){
            self.futurePendingCellDelegate?.rescheduleButtonPressed(selectedButton : sender as! UIButton)
        }
    }

    @IBAction func confirmButtonAction(_ sender: Any){
        if (self.futurePendingCellDelegate != nil){
            self.futurePendingCellDelegate?.confirmButtonPressed(selectedButton : sender as! UIButton)
        }
    }
}
