//
//  PastNoDateTableViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 15/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class PastNoDateTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var userImageOutlet: UIImageView!
    @IBOutlet weak var userNameOutlet: UILabel!
    @IBOutlet weak var dateForDateLabelOutlet: UILabel!
    @IBOutlet weak var timeForDateLabelOutlet: UILabel!
    @IBOutlet weak var ratingOutlet: FloatRatingView!
    @IBOutlet weak var ratingTextOutlet: UILabel!
    @IBOutlet weak var ratingBackgroundColorOutlet: UIView!
    
    @IBOutlet weak var rateView: UIView!
    @IBOutlet weak var ratingBtn: UIButton!
    
    var currentRating : Float?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userImageOutlet.layer.cornerRadius = userImageOutlet.frame.size.height/2
        //ratingOutlet.delegate = self
    }
    
    
   
    func displayProfileDetails(profileDet:SyncDate) {
        
       
        
        self.userNameOutlet.text = profileDet.nameOfTheProfile
        ratingOutlet.minRating = 0
        let url = URL(string: profileDet.profilePicutre)
        self.userImageOutlet.kf.setImage(with:url, placeholder:UIImage.init(named:"02m"))
        
//        let dateTimeAccepted = Helper.getTimeStamp(timeStamp: Double(profileDet.dateTime) * 1000)
//        self.timeForDateLabelOutlet.text = dateTimeAccepted
        
        let currentDate = Date()
        let  date:Date = Date(timeIntervalSince1970: Double(profileDet.dateTime))
        if date == currentDate{
            self.timeForDateLabelOutlet.text = CalenderUtility.getCurrentTime(fromDate: date)
        }
        else{
           self.timeForDateLabelOutlet.text = CalenderUtility.getDateAndTimeWithoutAt(fromDate: date)
        }
        
        self.dateForDateLabelOutlet.text = profileDet.requestedFor
        
        if profileDet.dateStatus == "accepted" {
            if(profileDet.ratings != 100) {
                self.ratingOutlet.rating = Float(profileDet.ratings)
                
                self.rateView.isHidden = true
            }else {
                
                self.rateView.isHidden = false
            }
            
            self.ratingBtn.isHidden = false
            self.ratingOutlet.isHidden = false
        }else {
            self.rateView.isHidden = true
            self.ratingBtn.isHidden = true
            self.ratingOutlet.isHidden = true
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    // "accepted"
}

//extension PastNoDateTableViewCell : FloatRatingViewDelegate
//{
//    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
//        switch rating {
//        case 1:
//            self.ratingOutlet.fullImage = #imageLiteral(resourceName: "red_star")
//        case 2 :
//            self.ratingOutlet.fullImage = #imageLiteral(resourceName: "red_star")
//        case 3 :
//            self.ratingOutlet.fullImage = #imageLiteral(resourceName: "red_star")
//        case 4 :
//            self.ratingOutlet.fullImage = #imageLiteral(resourceName: "red_star")
//        case 5 :
//            self.ratingOutlet.fullImage = #imageLiteral(resourceName: "red_star")
//        default: break
//        }
//    }
//}

