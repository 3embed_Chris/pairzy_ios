//
//  NowWithImageTableViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 15/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class NowWithImageTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageOutlet: UIImageView!
    @IBOutlet weak var userNameOutlet: UILabel!
    
    @IBOutlet weak var videoCallButtonOutlet: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func displayProfileDetails(profileDet:SyncDate) {
        self.userNameOutlet.text = profileDet.nameOfTheProfile
        
        let url = URL(string: profileDet.profilePicutre)
        self.userImageOutlet.kf.setImage(with:url, placeholder:UIImage.init(named:"02m"))
        let  date:Date = Date(timeIntervalSince1970: Double(profileDet.dateTime))
        print(date)
    }
}
