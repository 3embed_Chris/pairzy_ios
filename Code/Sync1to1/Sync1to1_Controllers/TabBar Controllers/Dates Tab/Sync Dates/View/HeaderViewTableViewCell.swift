//
//  HeaderViewTableViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 15/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class HeaderViewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var backgroundViewOutlet: UIView!
    @IBOutlet weak var titleForHeaderLabelOutlet: UILabel!
    @IBOutlet weak var imageForNoProfiles: UIImageView!
    @IBOutlet weak var titleForNoProfilesLabel: UILabel!
    @IBOutlet weak var messageLabelForNoProfiles: UILabel!
    
    @IBOutlet weak var profileIcon: UIImageView!
    @IBOutlet weak var rescheduleBtn: UIButton!
    @IBOutlet weak var cancelDateBtn: UIButton! // or reject
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateRequestLabel: UILabel!
    @IBOutlet weak var roundIndicatorView: UIImageView!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var confirmBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var cofirmBtnTrailing: NSLayoutConstraint!
    @IBOutlet weak var locationHeight: NSLayoutConstraint!
    
    @IBOutlet weak var callButton: UIButton!
    
    @IBOutlet weak var callBtnView: UIView!
    @IBOutlet weak var callBtnGradient: UIGradientImageView!
    @IBOutlet weak var callBtnViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var reScheduleBtnView: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    
    /// changes the buttons corner radius , color and
    ///
    /// - Parameter flag: true --for pending cell updates , false - for upcoming cell updates
    func initialsetup(flag: Bool){
        self.profileIcon.layer.cornerRadius = profileIcon.frame.size.height/2
        self.cancelDateBtn.layer.cornerRadius = self.cancelDateBtn.frame.size.height/2
        if flag {
            self.confirmBtn.layer.cornerRadius = self.confirmBtn.frame.size.height/2
            self.rescheduleBtn.layer.cornerRadius = 4
            self.rescheduleBtn.layer.borderWidth = 1
            self.rescheduleBtn.layer.borderColor = Helper.getUIColor(color: "#484848").cgColor
            
        }else {
            self.rescheduleBtn.layer.cornerRadius = 4
            
        }
        
    }
    
    
    /// up dates pending and upcoming cells
    ///
    /// - Parameters:
    ///   - data: SyncDate model type
    ///   - flag: true --> indicates pending cell ,false --> indicates upcoming cell
    func updatePendingCell(data: SyncDate, isPendingCall: Bool){
        
        let newUrl = URL(string: data.profilePicutre)
    
        var imageNam = "man_default"
        if data.gender == "Female" {
            imageNam = "woman_default"
        }
        profileIcon.kf.setImage(with:newUrl, placeholder:UIImage.init(named: imageNam))
        
        if data.isInitiatedByMe == 0 {
            
            if data.dateType == 1 {
                dateRequestLabel.text = String(format: StringConstants.dateReceivedFrom(),data.requestedFor,data.nameOfTheProfile)
                   // data.requestedFor + "request received from " + data.nameOfTheProfile
                locationHeight.constant = 0
            }else if data.dateType == 3 {
                dateRequestLabel.text = String(format: StringConstants.dateReceivedFrom(),data.requestedFor,data.nameOfTheProfile) //data.requestedFor + "request received from " + data.nameOfTheProfile
                locationHeight.constant = 0
            }else {
                dateRequestLabel.text = String(format: StringConstants.dateReceivedFrom(),data.requestedFor,data.nameOfTheProfile) //data.requestedFor + "request received from " + data.nameOfTheProfile
                locationHeight.constant = 30
            }
            if isPendingCall {
                // for pending dates
                cofirmBtnTrailing.constant = 10
                confirmBtnWidth.constant = 42
                
                //self.cancelDateBtn.setTitle("Reject", for: .normal)
            }else {
                // for upcoming dates
                if data.dateType == 2{
                    //   callButton.isHidden = true
                }
                else {
                    //  callButton.isHidden = false
                }
                //self.cancelDateBtn.setTitle("Cancel", for: .normal)
            }
            
        }
        else {
            
            if data.dateType == 1 {
                dateRequestLabel.text = String(format: StringConstants.dateRequestSentTo(), data.requestedFor,data.nameOfTheProfile) //data.requestedFor + "request sent to " + data.nameOfTheProfile
                locationHeight.constant = 0
            }else if data.dateType == 3 {
                dateRequestLabel.text = String(format: StringConstants.dateRequestSentTo(), data.requestedFor,data.nameOfTheProfile)
                locationHeight.constant = 0
            }else {
                dateRequestLabel.text = String(format: StringConstants.dateRequestSentTo(), data.requestedFor,data.nameOfTheProfile)
                locationHeight.constant = 30
            }
            //self.cancelDateBtn.setTitle("Cancel", for: .normal)
            
            if isPendingCall {
                //pending dates
                confirmBtnWidth.constant = 0
                cofirmBtnTrailing.constant = 0
            }else {
                
                if data.dateType == 2{
                    //   callButton.isHidden = true
                    
                }
                else {
                    // callButton.isHidden = false
                }
            }
        }
        
        if data.onlineStatus == 1{
            roundIndicatorView.isHidden = false
            roundIndicatorView.image = #imageLiteral(resourceName: "Ellipse 95")
        }else {
            roundIndicatorView.isHidden = true
            roundIndicatorView.image = #imageLiteral(resourceName: "Ellipse 96")
        }
        
        titleForHeaderLabelOutlet.text = data.nameOfTheProfile
        titleForNoProfilesLabel.text = data.placeName
        let urlPic = URL(string: data.profilePicutre)
        var imageName = "man_default1"
        if data.gender == "Female" {
            imageName = "woman_default1"
        }
        imageForNoProfiles.kf.setImage(with:urlPic, placeholder:UIImage.init(named: imageName))
        
        let  date:Date = Date(timeIntervalSince1970: Double(data.dateTime))
        messageLabelForNoProfiles.text = CalenderUtility.getDateAndTime(fromDate: date)
        
        
        let  createdTime:Date = Date(timeIntervalSince1970: Double(data.creationTime))
        timeLabel.text = CalenderUtility.getCurrentTime(fromDate: createdTime)
        
    }
    
    func updateVideoCallBtn(data: SyncDate){
        let dateIsAt = data.enableDateTime
        let currentDate = Date().timeIntervalSince1970
        let newDate = Date(timeIntervalSince1970: Double(data.dateTime)).addingTimeInterval(3600)
        if currentDate >= dateIsAt{
            callButton.isEnabled = true
        }else {
            callButton.isEnabled = false
        }
        if currentDate >= newDate.timeIntervalSince1970 {
            callButton.isEnabled = false
        }
    }
    
//    //for enabling and disabling video call button      //not Using
//    func updateVideoCallBtn(data: SyncDate, flag: Bool){
//        let dateIsAt = data.dateTime - (60 * 60 * 1000)
//        let currentDate = Date().timeIntervalSince1970
//        let newDate = Date(timeIntervalSince1970: Double(data.dateTime)).addingTimeInterval(3600)
//        if currentDate >= dateIsAt{
//            callButton.isEnabled = true
//        }else {
//            callButton.isEnabled = false
//        }
//        if currentDate >= newDate.timeIntervalSince1970 {
//            callButton.isEnabled = false
//        }
//    }
//
 
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
