//
//  SyncDateeRequestVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 13/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxAlamofire

class SyncDateeRequestVM: NSObject {
    
    enum ResponseType:Int{
        case currentDates = 0
        case pastDates = 1
        case dateResponse = 2
    }
    
    
    let disposeBag = DisposeBag()
    
    let syncDateSubjectResponse = PublishSubject<(ResponseType,Bool)>()
    
    var pendingProfiles = [SyncDate]()
    var upcomingProfiles = [SyncDate]()
    var pastProfiles = [SyncDate]()

    var index = 0
    var datesOffset = 0
    var datesLimit = 20
    
    
    var pendingTableView:UITableView? = nil
    var upcomingTableView:UITableView? = nil
    var pastTableView:UITableView? = nil
    
    var errorMsg = ""
    
    
    //MARK: - sync date  API CALL AND RESPONSE HANDLER
    
    
    
    func getNowPrfiles() {
        let apiCall = SyncAPICalls()
        apiCall.requestToGetNowProfiles(offset: datesOffset,limit: datesLimit)
        apiCall.subject_response
            .subscribe(onNext: {response in
                
                Helper.hideProgressIndicator()
                self.handleSyncNowProfilesResponse(response: response)
                
            }, onError: {error in
                self.errorMsg = error.localizedDescription
                self.syncDateSubjectResponse.onNext((.currentDates,false))
                Helper.hideProgressIndicator()
            })
            
            .disposed(by:disposeBag)
    }
    
    
    /// method for handling matches api response.
    ///
    /// - Parameter response: contains api response and status code.
    func handleSyncNowProfilesResponse(response:ResponseModel) {
        
        switch (response.statusCode) {
        case 200:
            
            // 200 is for success response (contains profiles)
            // 412 is for when there is no profiles.
            var pendingDatesProfileArray = [SyncDate]()
            var upcomingDatesProfileArray = [SyncDate]()
            
            var allPendingDates = [AnyObject]()
            var allUpcomingDates = [AnyObject]()
            
            if let data = response.response[ApiResponseKeys.data] as? [String:Any] {
                if let profileResponse = data[ApiResponseKeys.pendingDates] as? [AnyObject] {
                    allPendingDates = profileResponse
                    for (index, _) in profileResponse.enumerated() {
                        let profileDet = profileResponse[index] as! [String:Any]
                        pendingDatesProfileArray.append(SyncDate.init(profileDetails:profileDet))
                    }
                }
                
            }
            
            if let data = response.response[ApiResponseKeys.data] as? [String:Any] {
                if let profileResponse = data[ApiResponseKeys.upcomingDates] as? [AnyObject] {
                    allUpcomingDates = profileResponse
                    for (index, _) in profileResponse.enumerated() {
                        let profileDet = profileResponse[index] as! [String:Any]
                        upcomingDatesProfileArray.append(SyncDate.init(profileDetails:profileDet))
                        CalenderHelper().createCalenderEvent(profile: upcomingDatesProfileArray[index])
                        LNHELPER().craateNotificationForDateReminder(dateDetails: upcomingDatesProfileArray[index])
                    }
                }
                
            }
            
            
            UserDefaults.standard.setValue(allUpcomingDates, forKey: "upcomingDatesDetails")
            UserDefaults.standard.setValue(allPendingDates, forKey: "pendingDateDatesDetails")
            UserDefaults.standard.synchronize()
            
            self.pendingProfiles = pendingDatesProfileArray
            self.upcomingProfiles = upcomingDatesProfileArray
            self.upcomingTableView?.reloadData()
            self.pendingTableView?.reloadData()
        
            self.syncDateSubjectResponse.onNext((.currentDates,true))
            
        default:
            self.errorMsg = response.response["message"] as! String
            self.syncDateSubjectResponse.onNext((.currentDates,false))
            break
        }
        
        Helper.hideProgressIndicator()
    }
    
    
    func getPastProfiles() {
        let apiCall = SyncAPICalls()
        apiCall.requestForPastProfiles(offset:datesOffset,limit: datesLimit)
        apiCall.subject_response
            .subscribe(onNext: {response in
                self.handleSyncPastProfilesResponse(response: response)
                Helper.hideProgressIndicator()
            }, onError: {error in
                self.errorMsg = error.localizedDescription
                Helper.hideProgressIndicator()
                self.syncDateSubjectResponse.onNext((.pastDates,false))
            })
            
            .disposed(by:disposeBag)
    }
    
    
    
    
    
    func handleSyncPastProfilesResponse(response:ResponseModel) {
        
        switch (response.statusCode) {
        case 200:
            
            // 200 is for success response (contains profiles)
            // 412 is for when there is no profiles.
            var syncProfile = [SyncDate]()
            
            if let profileResponse = response.response["data"] as? [AnyObject] {
                for (index, _) in profileResponse.enumerated() {
                    let profileDet = profileResponse[index] as! [String:Any]
                    syncProfile.append(SyncDate.init(profileDetails:profileDet))
                }
            }
            
            self.pastProfiles = syncProfile
            self.pastTableView?.reloadData()
            self.syncDateSubjectResponse.onNext((.pastDates,true))
        default:
             self.errorMsg = response.response["message"] as! String
             self.syncDateSubjectResponse.onNext((.pastDates,false))
            break
        }
        
        Helper.hideProgressIndicator()
    }
    

    
    /// method for requesting unmatch profile.
    ///
    /// - Parameter params: set sync api details.
    func setDateStatus(profile:SyncDate,dateStatus:Int) {
        let apiCall = SyncAPICalls()
        let dateTime  = profile.dateTime
        //sending current time stamp in milliseconds
        let epochValueForSelectedDate = Date().timeIntervalSince1970*1000
        
        
        let requestParms = [likeProfileParams.status:dateStatus,
                            likeProfileParams.dateId:profile.dateId,
                            likeProfileParams.syncDate:dateTime,
                            likeProfileParams.latitude: Double(profile.latitude),
                            likeProfileParams.longitude: Double(profile.longitude),
                            likeProfileParams.placeName: profile.placeName,
                            likeProfileParams.dateType: profile.dateType
            ] as [String : Any]
        
        let requestData = RequestModel().dislikeProfileRequestDetails(Details:requestParms)
        
        apiCall.requestToSetDateStatus(requestData:requestData)
        apiCall.subject_response
            .subscribe(onNext: {response in
                
                Helper.hideProgressIndicator()
                if response.statusCode == 200 {
                    self.syncDateSubjectResponse.onNext((.dateResponse,true))
                    
                }else {
                    
                    self.errorMsg = response.response["message"] as! String
                    self.syncDateSubjectResponse.onNext((.dateResponse,false))
                }
                
            }, onError: {error in
                self.errorMsg = error.localizedDescription
                self.syncDateSubjectResponse.onNext((.dateResponse,false))
                Helper.hideProgressIndicator()
            })
            
            .disposed(by:disposeBag)
    }
    
    
    
    
//***************************************************************************************************************************??
    func requestForEndDate(dateID:String) {
        //postCompletedate
        Helper.showProgressIndicator(withMessage: "Ending Date...")
        
        let params = ["date_id":dateID]
        let apiCall = SyncAPICalls()
        let requestData = RequestModel().dislikeProfileRequestDetails(Details:params)
        apiCall.requestForEndDate(requestData: requestData)
        apiCall.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                if response.statusCode == 200 {
                    //self.syncDateSubjectResponse.onNext((.dateResponse,true))
                  Helper.showProgressIndicator(withMessage:"Date Completed successfully.")
                    let notificationName = NSNotification.Name(rawValue:NSNotificationNames.notificationForRefreshFutureTab)
                    NotificationCenter.default.post(name: notificationName, object: self, userInfo:nil)
                }
            }, onError: {error in
                Helper.hideProgressIndicator()
            })
            
            .disposed(by:disposeBag)
    }
    
    func rateForVideoCall(params:[String:Any]) {
        let apiCall = SyncAPICalls()
        let requestData = RequestModel().dislikeProfileRequestDetails(Details:params)
        apiCall.rateForVideoCall(requestData: requestData)
        apiCall.subject_response
            .subscribe(onNext: {response in
                
            }, onError: {error in
                self.errorMsg = error.localizedDescription
            })
            
            .disposed(by:disposeBag)
    }
    
}
