//
//  DatePickerViewController.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 16/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxAlamofire

class DatePickerViewController: UIViewController {
    
    @IBOutlet weak var currentDateLabelOutlet: UILabel!
    @IBOutlet weak var dateCollectionViewOutlet: DatesCollectionView!
    @IBOutlet weak var datePickerOutlet: UIDatePicker!
    var previousSelectedDate = Date()
    var newSelectedDate = Date()
    let disposeBag = DisposeBag()
    var resheduleProfile = SyncDate(profileDetails:[:])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.datePickerOutlet.minimumDate = Date()
        self.dateCollectionViewOutlet.dateViewSelectedDelegate = self
        self.datePickerOutlet.date = previousSelectedDate
        self.setTime(fromDate: previousSelectedDate, andTime: previousSelectedDate)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        // Do any additional setup after loading the view.
    }
    
    func setTime(fromDate date: Date, andTime time:Date){
        newSelectedDate = date
        let currentDate = CalenderUtility.getCurrentMonthAndDayWithYear(fromDate: newSelectedDate)
        let currentTime = CalenderUtility.getCurrentTime(fromDate: newSelectedDate)
        self.currentDateLabelOutlet.text = "\(currentDate) | \(currentTime)"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func doneButtonAction(_ sender: Any)
    {
       setNewDate()
    }
    
    @IBAction func cancelButtonAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.dateCollectionViewOutlet.firstTimeScroll()
    }
    
    //MARK: - sync date  API CALL AND RESPONSE HANDLER
    
    
    /// method for requesting unmatch profile.
    ///
    /// - Parameter params: set sync api details.
    func setNewDate() {
        let apiCall = SyncAPICalls()
        Helper.showProgressIndicator(withMessage:StringConstants.setResheduleSyncDate())
        
        let selectedDateAndTime:Date = CalenderUtility.combineTimeAndDate(forDate:self.newSelectedDate,timedate:self.datePickerOutlet.date)
        
        //sending time stamp in milliseconds
        let epochValueForSelectedDate = (selectedDateAndTime.timeIntervalSince1970*1000)
        
        self.setDateStatus(profile:resheduleProfile , dateStatus:3, newDate:epochValueForSelectedDate)
        
        
        let requestParms = [likeProfileParams.targetUserId:resheduleProfile.profileId,
                            likeProfileParams.isSchedule:1,
                            likeProfileParams.syncDate:epochValueForSelectedDate,
                            likeProfileParams.dateType:"1"
            ] as [String : Any]

        let requestData = RequestModel().dislikeProfileRequestDetails(Details:requestParms)

        apiCall.requestToSetSyncDate(requestData:requestData)
        apiCall.subject_response
            .subscribe(onNext: {response in
                self.handleResheduleAPIResponse(response: response)
                Helper.hideProgressIndicator()
            }, onError: {error in
                Helper.hideProgressIndicator()
            })

            .disposed(by:disposeBag)
    }
    
    
    func setDateStatus(profile:SyncDate,dateStatus:Int,newDate:Double) {
        let apiCall = SyncAPICalls()
        Helper.showProgressIndicator(withMessage:StringConstants.setSyncDate())
        
        
        let requestParms = [likeProfileParams.status:dateStatus,
                            likeProfileParams.dateId:profile.dateId,
                            likeProfileParams.syncDate:newDate,
            ] as [String : Any]
        
        let requestData = RequestModel().dislikeProfileRequestDetails(Details:requestParms)
        
        apiCall.requestToSetDateStatus(requestData:requestData)
        apiCall.subject_response
            .subscribe(onNext: {response in
                
                self.handleResheduleAPIResponse(response: response)

                //self.syncDateSubjectResponse.onNext(true)
                Helper.hideProgressIndicator()
            }, onError: {error in
               // self.syncDateSubjectResponse.onNext(false)
                Helper.hideProgressIndicator()
            })
            
            .disposed(by:disposeBag)
    }
    
    func handleResheduleAPIResponse(response:ResponseModel) {
        switch (response.statusCode) {
        case 200:
          self.dismiss(animated: true, completion: nil)
        default: break
        }
        Helper.hideProgressIndicator()
    }
    
    
    @IBAction func dateChangedInPickerAction(_ sender: Any)
    {
        self.currentDateLabelOutlet.text = CalenderUtility.getCurrentMonthAndDayWithYear(fromDate: self.datePickerOutlet.date)
        let currentDate = CalenderUtility.getCurrentMonthAndDayWithYear(fromDate: self.datePickerOutlet.date)
        let currentTime = CalenderUtility.getCurrentTime(fromDate: self.datePickerOutlet.date)
        self.currentDateLabelOutlet.text = "\(currentDate) | \(currentTime)"
    }
}

extension DatePickerViewController : DateViewSelectedDelegate
{
    func dateSelected(withDate date: Date?)
    {
        if let date = date{
            self.setTime(fromDate: date, andTime: self.datePickerOutlet.date)
            self.datePickerOutlet.setDate(date, animated: true)
        }
    }
}
