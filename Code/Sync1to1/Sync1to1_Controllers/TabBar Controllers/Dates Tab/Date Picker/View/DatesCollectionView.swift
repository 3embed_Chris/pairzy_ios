//
//  DatesCollectionView.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 16/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

@objc protocol DateViewSelectedDelegate: class {
    func dateSelected(withDate date:Date?)
}

class DatesCollectionView: UIView
{
    struct Constant {
        static let cellIdentifier = "DateCollectionViewCell"
    }
    
    var currentDate = Date()
    var dateDataForCalender = [DateDataViewModal]()
    var previousIndex:IndexPath!
    weak var dateViewSelectedDelegate:DateViewSelectedDelegate?
    
    @IBOutlet weak var previousButtonOutlet: UIButton!
    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBOutlet weak var collectionViewOutlet: UICollectionView!
    @IBOutlet weak var currentMonthOutlet: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.currentMonthOutlet.text = CalenderUtility.getCurrentMonthWithYear(fromDate: currentDate)
        self.getAllDates(forDate: self.currentDate)
        self.previousButtonOutlet.isEnabled = false
        self.collectionViewOutlet.reloadData()
    }
    
    @IBAction func previousMonthButtonAction(_ sender: Any)
    {
        let previousDate = Calendar.current.date(byAdding: .month, value: -1, to: currentDate)!
        currentDate = Calendar.current.date(byAdding: .month, value: -1, to: currentDate)!
        self.currentMonthOutlet.text = CalenderUtility.getCurrentMonthWithYear(fromDate: currentDate)
        self.getAllDates(forDate: self.currentDate)
        
        if previousDate<Date()
        {
            DispatchQueue.main.async{
                self.getTakeViewToCurrentDate(toDate: Date())
            }
            self.previousButtonOutlet.isEnabled = false
        }else{
            self.collectionViewOutlet.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: true)
        }
        self.collectionViewOutlet.reloadData()
    }
    
    @IBAction func nextMonthButtonAction(_ sender: Any)
    {
        currentDate = Calendar.current.date(byAdding: .month, value: +1, to: currentDate)!
        self.currentMonthOutlet.text = CalenderUtility.getCurrentMonthWithYear(fromDate: currentDate)
        self.getAllDates(forDate: self.currentDate)
        self.previousButtonOutlet.isEnabled = true
        self.collectionViewOutlet.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: true)
        self.collectionViewOutlet.reloadData()
    }
    
    func getAllDates(forDate date:Date)
    {
        let dateObj = DateData(date: date)
        let getDates = DateDataViewModal(withDateData: dateObj)
        self.dateDataForCalender = getDates.getAllDates()
        self.collectionViewOutlet.reloadData()
    }
}

extension DatesCollectionView: UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dateDataForCalender.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.cellIdentifier, for: indexPath) as! DateCollectionViewCell
        cell.dateDataViewModalObj = dateDataForCalender[indexPath.row]
        
        if (previousIndex != nil)
        {
            if self.previousIndex == indexPath
            {
                cell.dateLabelOutlet.textColor = UIColor.white
                cell.dayLabelOutlet.textColor = UIColor.white
            }
        }
        return cell
    }
}

extension DatesCollectionView : UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool
    {
        let currentYear = Calendar.current.component(.year, from: self.currentDate)
        let currentMonth = Calendar.current.component(.month, from: self.currentDate)
        if let currentDateObj = CalenderUtility.getCurrentDate(forDate: Int(indexPath.row+1), inMonth: currentMonth, andYear: currentYear){
            if currentDateObj<CalenderUtility.getDateWithoutTime(fromDate: Date())
            {
                return false
            }
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let previousindex = self.previousIndex
        self.previousIndex = indexPath
        self.collectionViewOutlet.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.collectionViewOutlet.reloadItems(at: [self.previousIndex,previousindex!])
    }
}

extension DatesCollectionView : UIScrollViewDelegate
{
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
        self.scrollViewDidEndScrolling(scrollView)
    }
    
    /// Used when the scrolling is stopped at that time this method will get called
    ///
    /// - Parameter scrollView: current scrolling view
    
    func scrollViewDidEndScrolling(_ scrollView: UIScrollView)
    {
        var index = self.findCenterIndex()
        if (self.dateViewSelectedDelegate != nil)
        {
            let currentYear = Calendar.current.component(.year, from: self.currentDate)
            let currentMonth = Calendar.current.component(.month, from: self.currentDate)
            let currentDateObj = CalenderUtility.getCurrentDate(forDate: Int(index.row+1), inMonth: currentMonth, andYear: currentYear)
            if Date()<currentDateObj! {
                let previosindex = self.previousIndex
                self.previousIndex = index
                if previosindex == index {
                    self.collectionViewOutlet.reloadItems(at: [index])
                }
                else {
                    if (previosindex == nil) {
                        self.collectionViewOutlet.reloadItems(at: [index])
                    } else {
                        self.collectionViewOutlet.reloadItems(at: [index,previosindex!])
                    }
                }
                self.dateViewSelectedDelegate?.dateSelected(withDate: currentDateObj)
            }
            else{
                self.getTakeViewToCurrentDate(toDate: Date())
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        self.scrollViewDidEndScrolling(scrollView)
    }
    
    func firstTimeScroll()
    {
        DispatchQueue.main.async {
            self.layoutIfNeeded()
        }
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            self.getTakeViewToCurrentDate(toDate: Calendar.current.date(byAdding: .day, value: +1, to: Date())!)
        })
    }
    
    func getTakeViewToCurrentDate(toDate date :Date)
    {
        self.dateViewSelectedDelegate?.dateSelected(withDate: date)
        let dateNum = CalenderUtility.getCurrentDateNumber(fromDate: date)
        let index = IndexPath(item: dateNum-1, section: 0)
        self.previousIndex = index
        self.collectionViewOutlet.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
        self.collectionViewOutlet.reloadItems(at: [self.previousIndex])
    }
    
    private func findCenterIndex() -> IndexPath
    {
        let center = self.convert(self.collectionViewOutlet.center, to: self.collectionViewOutlet)
        if let index = collectionViewOutlet!.indexPathForItem(at: center){
            return index
        }
        return IndexPath(item: 0, section: 0)
    }
}
