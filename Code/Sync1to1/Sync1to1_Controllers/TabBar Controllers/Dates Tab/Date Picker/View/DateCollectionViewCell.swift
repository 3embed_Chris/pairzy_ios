//
//  DateCollectionViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 16/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class DateCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var dateLabelOutlet: UILabel!
    @IBOutlet weak var dayLabelOutlet: UILabel!
    var dateDataViewModalObj : DateDataViewModal? {
        didSet{
            if let dateObj = dateDataViewModalObj{
                if let date = dateObj.currentDate, let day = dateObj.day{
                    self.dateLabelOutlet.text = "\(date)"
                    self.dayLabelOutlet.text = "\(day)"
                }
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.dateLabelOutlet.textColor = UIColor.black
        self.dayLabelOutlet.textColor = UIColor.black
        if let dateObj = dateDataViewModalObj{
            if let date = dateObj.currentDate, let day = dateObj.day{
                self.dateLabelOutlet.text = "\(date)"
                self.dayLabelOutlet.text = "\(day)"
            }
        }
    }
}
