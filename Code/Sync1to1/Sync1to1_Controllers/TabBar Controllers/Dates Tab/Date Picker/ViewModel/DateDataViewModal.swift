//
//  DateDataViewModal.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 19/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

import UIKit

class DateDataViewModal : NSObject
{
    let dateDataObj : DateData
    
    var day : String?
    var currentDate : Int?
    var month : Int?
    var year : Int?
    var date : Date?

    init(withDateData dateData: DateData)
    {
        self.dateDataObj  = dateData
        
        self.day = dateData.day
        self.currentDate = dateData.currentDate
        self.month = dateData.month
        self.year = dateData.year
        self.date = dateData.date
    }
    
    func getAllDates() -> [DateDataViewModal]
    {
        var dateArray = [DateDataViewModal]()
        let month = CalenderUtility.getCurrentMonth(fromDate: self.dateDataObj.date!)
        let year = CalenderUtility.getCurrentYear(fromDate: self.dateDataObj.date!)
        let numberOfMonths = CalenderUtility.getNumberOfDays(inMonth: month, andYear: year)
        for date in 1...numberOfMonths
        {
            let dateObject = self.getCurrentDate(forDate: date, inMonth: month, inYear: year)
            let dateDataObj = DateData(date: dateObject!)
            let dateDataVMObj = DateDataViewModal(withDateData: dateDataObj)
            dateArray.append(dateDataVMObj)
        }
        return dateArray
    }
    
    func getCurrentDate(forDate date : Int,inMonth month:Int, inYear year:Int) -> Date?
    {
        let date = CalenderUtility.getCurrentDate(forDate: date, inMonth: month, andYear: year)
        return date
    }
    
}
