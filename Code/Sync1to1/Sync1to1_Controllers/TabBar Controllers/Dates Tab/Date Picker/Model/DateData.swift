//
//  DateData.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 19/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

class DateData : NSObject {
    var day : String?
    var currentDate : Int?
    var month : Int?
    var year : Int?
    var date : Date?
    
    
    init(date:Date) {
        let year = CalenderUtility.getCurrentYear(fromDate: date)
        let month = CalenderUtility.getCurrentMonth(fromDate: date)
        let dateNumber = CalenderUtility.getCurrentDateNumber(fromDate: date)
        let day = CalenderUtility.getCurrentDay(fromDate: date)
        
        self.day = day
        self.currentDate = dateNumber
        self.month = month
        self.year = year
        self.date = date
    }
}
