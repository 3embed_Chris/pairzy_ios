//
//  ChooseLocationModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 15/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class ChooseLocationModel: NSObject {
    
    var name = ""
    var id           = ""
    var state = ""
    var city  = ""
    var crossStreet = ""
    var longitude:Double = 0.0
    var latitude:Double = 0.0
    var formattedAddress = [String]()
    var address = ""
    var distance:Int = 0
    var country = ""
    var cc = ""
    var fullAddress = ""
    
    
    init(address:[String:Any]) {
        
        if let id = address["id"] as? String {
            self.id = id
        }
        if let name = address["name"] as? String {
            self.name = name
        }
        if let address = address["address"] as? String {
            self.address = address
        }
        if let lang = address["lng"] as? Double {
            self.longitude = lang
        }
        if let lat = address["lat"] as? Double{
            self.latitude = lat
            
        }
        if let country = address["country"] as? String {
            self.country = country
        }
        
        if let state = address["state"] as? String {
            self.state = state
        }
        if let city = address["city"] as? String {
            self.city = city
        }
        
        if let location = address["location"] as? [String:Any] {
            
            if let state = location["state"] as? String {
                self.state = state
            }
            if let city = location["city"] as? String {
                self.city = city
            }
            if let crossStreet = location["crossStreet"] as? String {
                self.crossStreet = crossStreet
            }
            if let address = location["address"] as? String {
                self.address = address
            }
            if let distance = location["distance"] as? Int {
                self.distance = distance/1000
            }
            if let cc = location["cc"] as? String {
                self.cc = cc
            }
            if let country = location["country"] as? String {
                self.country = country
            }
            if let lang = location["lng"] as? Double {
                self.longitude = lang
            }
            if let lat = location["lat"] as? Double{
                self.latitude = lat
                
            }
            
            if let fullAddr = location["formattedAddress"] as? [String]{
                for each in fullAddr {
                    self.fullAddress = fullAddress + each + ","
                }
            }
            
        }
        
    
    }
    
    func updateFromMap(data: Location){
        
        self.name = data.fullText
        self.id           = data.placeId
        self.state = data.state
        self.city  = data.city
        self.crossStreet = data.street
        self.longitude = data.longitude
        self.latitude  = data.latitude
        self.formattedAddress = [String]()
        self.address =  data.fullText
        //     self.distance = data.distance
        self.country = data.country
        self.cc = ""
        
    }
    
}
