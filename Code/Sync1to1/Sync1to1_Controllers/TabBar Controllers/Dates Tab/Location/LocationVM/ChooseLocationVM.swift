//
//  ChooseLocationVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 14/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import RxSwift
import RxAlamofire

class ChooseLocationVM: NSObject {
    
    let disposeBag = DisposeBag()
    let chooseLocationVM = PublishSubject<Bool>()
    
   
    
    let client_id = FourSquare.clientId
    let client_secret = FourSquare.clientSecret
    
  //  var locations = [ChooseLocationModel]()
    var locations = [Location]()
 
    var tableView:UITableView? = nil
    
    var isForPassPort = false
    var isFromEdit = false
    var isFromMatch = false
    var locationManager:LocationManager?    = nil
    var dataAddress:[String : Any]          = [:]
    var addressList                         = [Location]()
    var recentPassPort  = [Location]()
    var othersavedLoc = [Location]()
    
    func getNearByLocations(query: String){
        
        
        let locDetails:[String:Any?] = Helper.getUserLocation()
        var latitude = 0.0
        var longitude = 0.0
        

        
        if Platform.isSimulator {
            print("Running on Simulator")
            latitude = 13.028694
            longitude = 77.589564
        } else {
            let hasValue:String = locDetails["hasLocation"] as! String
            
            if Helper.getIsFeatureExist(){
                if(hasValue == "1") {
                    let featureLoc:Location =  Helper.getFeatureLocation()
                    latitude = featureLoc.latitude
                    longitude = featureLoc.longitude
                }
            }else {
                
                if(hasValue == "1") {
                    if let lat = locDetails["lat"] as? Double {
                        latitude = lat
                    }
                    if let long = locDetails["long"] as? Double {
                        longitude = long
                    }
                }
            }
        }
        
 
        var querry = "checkin"
        
        var strURL = "https://api.foursquare.com/v2/venues/explore?ll=\(latitude),\(longitude)&v=20160607&intent=\(querry)&limit=40&radius=10000&client_id=\(client_id)&client_secret=\(client_secret)&offset=0"
        
        
        if query.count != 0 || isForPassPort || isFromEdit {
            querry = query                                  //trending
           strURL = "https://api.foursquare.com/v2/venues/search?ll=\(latitude),\(longitude)&v=20160607&intent=\(querry)&limit=15&radius=5000&client_id=\(client_id)&client_secret=\(client_secret)&offset=0"
          //  strURL = "https://api.foursquare.com/v2/venues/trending"
        }
        
        let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
       
        
        RxAlamofire.requestJSON(.get,strURL,parameters:[:],headers:headers)
            
            .debug()
            
            .subscribe(onNext: {(HeaderResponse, bodyResponse) in
                Helper.hideProgressIndicator()
              //  var locModel = [ChooseLocationModel]()
                var locModel = [Location]()
                var newLocations = [Location]()
                if let dict = bodyResponse as? [String: AnyObject] {
                    if let respDict: [String: Any] = JSON(dict).dictionaryObject {
                        let response = ResponseModel.init(statusCode: HeaderResponse.statusCode, response:respDict)
                        if let requestResp = response.response["response"] as? [String:Any] {
                            if let groups = requestResp["groups"] as? [[String:Any]] {
                                for eachCat in groups {
                                    if let items = eachCat["items"] as? [[String:Any]] {
                                        for eachItem in items {
                                            
                                            if let venue =  eachItem["venue"] as? [String:Any] {

                                             //   locModel.append(ChooseLocationModel.init(address: venue))
                                                locModel.append(Location.init(data: venue))
                                            }
                                        }
                                    }
                                }
                            }
                            if let catgories = requestResp["venues"] as? [[String:Any]] {
                                for venue in catgories {
                                 //locModel.append(ChooseLocationModel.init(address: venue))
                                    locModel.append(Location.init(data: venue))
                                }
                            }
                    
                        }
                    }
                }
                
                self.locations = locModel
               
               self.tableView?.reloadData()
            }, onError: { (error) in
                 Helper.hideProgressIndicator()
                //self.subject_response.onError(error)
            })
            
            .disposed(by: disposeBag)
      
        
    }
    
    
    func parseForQueryResponse(data: [String:Any]){
        
        
        
    }
    
}


//let strURLTo = SERVICE.BASE_URL + apiName          //+ apiextension + "/" + limit
//let headers = AuthorizationHeader.getAuthHeader(staticToken: false)
//
//var urlComponents = URLComponents(string: strURLTo)
//urlComponents?.queryItems = [URLQueryItem(name: "offset", value: String(0.0)),URLQueryItem(name: "limit", value: String(20.0))]
//let strURL  = urlComponents?.url
