//
//  MapVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

protocol MapVCDelegate: class {
    func selectedAddress(location: Location)
}

class MapVC: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var currentLocation: UIButton!
    @IBOutlet weak var mapMarker: UIImageView!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var chooseLocation: UILabel!
    
    var addressList = [Location]()
    var zoomLevelofMap:Float = 16
    var flagEdit = false
    var flagMapChange = Bool()
    let passportLocationManager = CLLocationManager()
    var locationManager:LocationManager? = nil
    weak var mapVCDelegate:MapVCDelegate? = nil
    var addressSelected = Location(data: [:])
    
    var isForPassport = false
    var isForMatch = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.locationManager?.delegate = self
        initialSetup()
        checkLocationEnabled()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        myLocationAction(UIButton())
        self.searchTableView.isHidden = true
        if isForPassport {
        zoomLevelofMap = 16
        }else{
        zoomLevelofMap = 16
        }
   
    }
    
    func checkLocationEnabled(){
       // self.passportLocationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                let alert = UIAlertController(title: StringConstants.locationDesabled(), message: StringConstants.youNeedToEnable(), preferredStyle: .alert)
                
//                // Add "OK" Button to alert, pressing it will bring you to the settings app
//                alert.addAction(UIAlertAction(title: StringConstants.ok(), style: .default, handler: { action in
//                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
//                }))
                alert.addAction(UIAlertAction(title: StringConstants.ok(), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                // self.present(alert, animated: true)
                print(false,"No access")
                
                
            case .authorizedAlways, .authorizedWhenInUse:
               break
            }
            
        }
    }
//        guard let lat = self.mapView.myLocation?.coordinate.latitude,
//            let lng = self.mapView.myLocation?.coordinate.longitude else { return }
//
//        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 16)
//        self.mapView.animate(to: camera)
//
//
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func confirmButtonAction(_ sender: Any) {
        
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:NSNotificationNames.notificationForLocationUpdate), object: addressSelected)
        
        if isForPassport {
            for vc in (self.navigationController?.viewControllers)! {
                if vc.isKind(of: PassportLocationVC.self) {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
            
        }else if isForMatch {
            for vc in (self.navigationController?.viewControllers)! {
                if vc.isKind(of: ReScheduleVC.self) {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }else {
            self.navigationController?.popToRootViewController(animated: true)
        }
        
    }
    
    
    @IBAction func myLocationAction(_ sender: UIButton) {
        
        let locDetails:[String:Any?] = Helper.getUserLocation()
        
        let hasValue:String = locDetails["hasLocation"] as! String
        var latitude = 0.0
        var longitude = 0.0
        
        
        if Platform.isSimulator {
            print("Running on Simulator")
            latitude = 13.028694
            longitude = 77.589564
        }
        
        if(hasValue == "1") {
            if let lat = locDetails["lat"] as? Double {
                latitude = lat
            }
            if let long = locDetails["long"] as? Double {
                longitude = long
            }
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude) ,longitude: CLLocationDegrees(longitude) , zoom: zoomLevelofMap)
        self.mapView.animate(to: camera)
        
        
    }
    
    
    @IBAction func seachAction(_ sender: Any) {
        flagEdit = true
        let selectLocVc = Helper.getSBWithName(name:"DatumTabBarControllers").instantiateViewController(withIdentifier:"selectLocation") as! SelectLocationVC
        selectLocVc.delegate = self
        self.navigationController?.present(selectLocVc, animated: true, completion:nil)
        
    }
    
    
    
}
extension MapVC {
    
    func initialSetup(){
        
        myLocationAction(UIButton())
        self.addressTF.addTarget(self, action: #selector(MapVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        chooseLocation.text = StringConstants.chooseLocation()
        flagMapChange = false
        locationManager = LocationManager.shared
        locationManager?.delegate = self
        locationManager?.start()
        self.mapView?.isMyLocationEnabled = true
        saveButton.setTitle(StringConstants.confirmAddress(), for: .normal)
        self.mapMarker.bringSubview(toFront: self.view)
    }
    
    
}
//MARK:- LocationManager Delegate
extension MapVC : LocationManagerDelegate {
    func didFailToUpdateLocation() {
        
    }
    // func didUpdateLocation(location: Location, search: Bool) {
    func didUpdateLocation(location: Location, search:Bool,update:Bool) {
        let locationin = CLLocation(latitude: CLLocationDegrees(location.latitude), longitude: CLLocationDegrees(location.longitude))
        self.mapView.animate(to: GMSCameraPosition(target: locationin.coordinate, zoom: zoomLevelofMap, bearing: 0, viewingAngle: 0))
        var address = location.fullText
        if isForPassport {
            
            if flagEdit {
                if location.locationDescription.count == 0 {
                    address = location.city + ", " + location.state + ", " + location.country
                }else {
                    address = addressSelected.locationDescription
                }
           // flagEdit = false
            }else {
               
                if addressSelected.locationDescription.count == 0 {
                    address = location.city + ", " + location.state + ", " + location.country
                }else {
                    address = addressSelected.locationDescription
                }
            }
        }else {
            if location.mainText.sorted().count > 0 {
                address = location.mainText
            }
            addressSelected = location
        }
        
       
        addressSelected = location
        
        self.addressLabel.text = address.count > location.fullText.count  ? address:location.fullText
     
        // self.addressTF.text = address as String
        //self.addressLabel.text = address as String
        self.view.endEditing(true)
        flagMapChange = true
    }
    
    func didChangeAuthorization(authorized: Bool) {
        self.mapView.settings.myLocationButton = true
    }
    
    func didUpdateSearch(locations: [Location]) {
        addressList = locations
      //  searchTableView.reloadData()
        
    }
}

extension MapVC : GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        zoomLevelofMap = mapView.camera.zoom
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        zoomLevelofMap = mapView.camera.zoom
        flagMapChange = true
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if flagMapChange {
            var address = addressSelected.fullText
            if isForPassport {
                if addressSelected.locationDescription.count == 0 {
                    address = addressSelected.city + "," + addressSelected.state + "," + addressSelected.country
                    
                }else {
                    address = addressSelected.locationDescription
                }
            }else {
                if addressSelected.mainText.sorted().count > 0 {
                    address = addressSelected.locationDescription
                }
            }
            // self.addressTF.text = address as String
    
            self.addressLabel.text = address.count > addressSelected.fullText.count  ? address:addressSelected.fullText
            // self.addressTF.text = address as String
            //self.addressLabel.text = address as String
            let latitude: CLLocationDegrees = position.target.latitude
            let longitude: CLLocationDegrees = position.target.longitude
            
            
            if flagEdit == false {
                if (addressSelected.latitude != latitude) || (addressSelected.longitude != longitude) {
                    addressSelected.locationDescription = ""
                    addressSelected.mainText = ""
                    addressSelected.placeId = ""
                    let location: CLLocation = CLLocation(latitude: latitude, longitude: longitude)
                    locationManager?.updateLocationData(location: location, placeIn: addressSelected,flag: false)
                }
               //
            }else {
                flagEdit = false
            }
        }
        zoomLevelofMap = mapView.camera.zoom
    }
}

extension MapVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == addressTF {
            searchTableView.isHidden = false
            textField.text = ""
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == addressTF {
            searchTableView.isHidden = true
        }
    }
    @objc func textFieldDidChange(_ textField : UITextField) {
        if textField == addressTF{
            locationManager?.search(searchText: textField.text!)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}



// MARK: - Tableview Delegate
extension MapVC : UITableViewDelegate, UITableViewDataSource {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.isHidden = true
        if addressList.count > 0 {
            tableView.isHidden = false
        }
        return addressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Location", for: indexPath) as! LocationTableViewCell
        let data = addressList[indexPath.row]
        cell.headLabel.text = data.locationDescription
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let data = addressList[indexPath.row]
        
        if data.reference.sorted().count != 0 {
            flagMapChange = false
            locationManager?.getAddressFromPlace(place: data)
        }
    }
}
extension MapVC: UIScrollViewDelegate
{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == searchTableView {
            self.view.endEditing(true)
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
    }
    
}
extension MapVC: SelectLocationVCDelegate{
    
    func didSelectAddress(address: Location) {
        if address.reference.sorted().count != 0 {
            flagMapChange = false
            self.locationManager?.delegate = self
            flagEdit = true
            self.locationManager?.getAddressFromPlace(place: address)
        }
        
    }
    
    
}




