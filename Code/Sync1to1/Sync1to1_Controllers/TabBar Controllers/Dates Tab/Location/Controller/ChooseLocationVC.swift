//
//  ChooseLocationVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 01/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

protocol ChooseLocationVCDelegate: class {
    func didSelectAddress(address: Location)
}

class ChooseLocationVC: UIViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var searchBackView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var topBackBtn: UIButton!
    @IBOutlet weak var screenSubTitle: UILabel!
    
    @IBOutlet weak var searchMoreLabel: UILabel!
    weak var delegate:ChooseLocationVCDelegate? = nil
    let locationVM = ChooseLocationVM()
    
    var didSelectRow = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.placeholder = StringConstants.search()
        titleLabel.text = StringConstants.choosePlace()
        screenSubTitle.text = StringConstants.choosePlace()
        searchMoreLabel.text = StringConstants.searchMore()
        let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as! UITextField
        textFieldInsideSearchBar.font = UIFont(name: CircularAir.Bold, size: 14)
        //   Fonts.setPrimaryRegular(textFieldInsideSearchBar)
        searchBar.barTintColor = UIColor.white
        searchBar.backgroundColor = UIColor.white
        searchBar.layer.borderColor = Colors.ScreenBackground.cgColor
        // Do any additional setup after loading the view.
        locationVM.locationManager = LocationManager.shared
        locationVM.locationManager?.delegate = self
        locationVM.locationManager?.start()
        screenSubTitle.textColor = Colors.AppBaseColor
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        locationVM.getNearByLocations(query: "")
        locationVM.tableView = mainTableView
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.titleLabel.textColor = UIColor.clear
        headerView.backgroundColor = UIColor.clear
        headerView.isHidden = true
        locationVM.othersavedLoc = Helper.getOtherLocations()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addCustomLocationAction(_ sender: Any) {
        
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "LocationSelectVC") as! MapVC
        userBasics.isForPassport = self.locationVM.isForPassPort
        userBasics.isForMatch = true
        self.navigationController?.pushViewController(userBasics, animated: true)
        
    }
    
    
}

extension ChooseLocationVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if locationVM.isFromEdit || locationVM.isForPassPort{
            if locationVM.addressList.count == 0 {
                return locationVM.othersavedLoc.count
            }
            return locationVM.addressList.count
            
        }else {
            
            return locationVM.locations.count
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseLoactionCell", for: indexPath) as! ChooseLoactionCell
        if locationVM.isFromEdit || locationVM.isForPassPort{
            cell.ratingDistView.isHidden = true
            if locationVM.addressList.count == 0 {
                cell.updateAddressCell(data: locationVM.othersavedLoc[indexPath.row])
                
            }else {
                cell.updateAddressCell(data: locationVM.addressList[indexPath.row])
            }
            
        }else {
            
            cell.ratingDistView.isHidden = false
            cell.updateCell(data: locationVM.locations[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedIndex = indexPath.row
        tableView.deselectRow(at: indexPath, animated: true)
        let btn = UIButton()
        btn.tag = indexPath.row
        didSelectRow = true
        var tempLocation:Location? = nil
        
        if locationVM.isFromEdit || locationVM.isForPassPort{
            if locationVM.addressList.count == 0 {
                tempLocation = locationVM.othersavedLoc[indexPath.row]
            }else {
                tempLocation = locationVM.addressList[indexPath.row]
            }
            
            if tempLocation?.latitude == 0.0 && tempLocation?.latitude == 0.0 {
                locationVM.locationManager?.getAddressFromPlace(place: tempLocation!)
                return
            }
        }else {
            tempLocation = locationVM.locations[indexPath.row]
            self.navigationController?.popViewController(animated: true)
            if self.delegate != nil {
                self.delegate?.didSelectAddress(address: tempLocation!)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return 48
    //    }
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //
    //        let cell = tableView.dequeueReusableCell(withIdentifier: "chooseLocHeader") as! SectionHeaderCell
    //
    //
    //        switch section {
    //        case 0:
    //            cell.updateHeader(title: "CURRENT LOCATION")
    //        // cell.separator.isHidden = false
    //        case 1:
    //            cell.updateHeader(title: "RECENT PASSPORT LOCATIONS")
    //        //cell.separator.isHidden = false
    //        default:
    //            break
    //        }
    //
    //
    //        return cell
    //
    //    }
    
    
    
    
}

extension ChooseLocationVC: UISearchBarDelegate {
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //  filterContent(searchBar.text!)
        searchBar.resignFirstResponder()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if self.locationVM.isFromMatch {
            filterContent(searchBar.text!)
        }else{
            
            if searchBar.text!.sorted().count > 0 {
                mainTableView.reloadData()
            }
            
            locationVM.locationManager?.search(searchText: searchBar.text!)
        }
        
        
    }
    
    
    /// Filter Content
    ///
    /// - Parameter searchText: Search String
    fileprivate func filterContent(_ searchText: String) {
        locationVM.getNearByLocations(query: searchText)
        
    }
    
    
}

// MARK: - UIScrollViewDelegate
extension ChooseLocationVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let movedOffset: CGFloat = scrollView.contentOffset.y
        if scrollView == mainTableView {
            self.view.endEditing(true)
            let pageWidth: CGFloat = 88      //171 - 64
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            print(movedOffset)
            
            if ratio > 0 {
                
                if movedOffset >= 20 {
                    self.titleLabel.textColor = Colors.PrimaryText.withAlphaComponent(ratio)
                    headerView.backgroundColor = Helper.getUIColor(color: "#ffffff")//.withAlphaComponent(ratio)
                    Helper.setShadow(sender: headerView)
                    if  ratio <= 1 && ratio >= 0{
                        self.titleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle)*ratio)
                    }
                    if movedOffset > 88 {
                        self.titleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle))
                    }
                    headerView.isHidden = false
                    topBackBtn.isHidden = false
                }
                
            }else {
                
                self.titleLabel.textColor = UIColor.clear
                headerView.backgroundColor = UIColor.clear
                Helper.removeShadow(sender: headerView)
                topBackBtn.isHidden = true
                headerView.isHidden = true
            }
            
            
            
        }
    }
    
}
//MARK:- LocationManager Delegate
extension ChooseLocationVC: LocationManagerDelegate {
    func didFailToUpdateLocation() {
        
    }
    // func didUpdateLocation(location: Location, search: Bool) {
    func didUpdateLocation(location: Location, search:Bool,update:Bool) {
        if didSelectRow {
            if self.delegate != nil {
                self.navigationController?.popViewController(animated: true)
                self.delegate?.didSelectAddress(address: location)
            }
        }
    }
    func didChangeAuthorization(authorized: Bool) {
        
    }
    
    func didUpdateSearch(locations: [Location]) {
        locationVM.addressList = locations
        mainTableView.reloadData()
        
    }
}
