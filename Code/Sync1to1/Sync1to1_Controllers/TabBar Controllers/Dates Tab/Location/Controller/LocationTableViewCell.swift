//
//  LocationTableViewCell.swift
//  UFly
//
//  Created by 3Embed on 08/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {    //this cell is re-using in two Controllers PassportLocationVC and addAddressVC
    
    @IBOutlet weak var headLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
 
    /// update the adress to each Address cell
    ///
    /// - Parameter data: data it is of type Location object
    func setValue(data: Location) {
        
        headLabel.text = data.locationDescription
        if data.locationDescription.sorted().count == 0 {
            headLabel.text = data.fullText
        }
       
    }
    
    /// update saved Location Secion
    ///
    /// - Parameter data: Location Model Object
    func setSavedAddress(data: Location){
       
        headLabel.text = data.locationDescription
        if data.locationDescription.sorted().count == 0 {
            headLabel.text = data.fullText
        }
    }
    
    /// setCurrent Location
    func setCurrentLocation(){
        //let locationStored = Utility.currentLatAndLong()//Utility.getAddress()
        
        
        
        headLabel.text = StringConstants.locationStored()//.Name
        }
    
    
}
