//
//  CreateDateVM.swift
//  Datum
//
//  Created by 3 Embed on 17/08/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class CreateDateVM: NSObject {
    
    enum ResponseType:Int {
        case dateCreated = 0
        case dateResponseType = 1
        case faild = 3
    }
    
    
    let disposeBag = DisposeBag()
    let CreateDateVM_resp = PublishSubject<ResponseType>()
    var errorMsg = ""
    
    
    
    var isForCallDate = false
    var isFromMatch = false
    var dateType:DateType = .none
    
    
    var matchedProfile:MatchedProfile! = nil
    var requestFromMatchMaker:Bool = false
    
    
    var previousSelectedDate = Date()
    var newSelectedDate:Date? = nil
    
    var resheduleProfile = SyncDate(profileDetails:[:])
    
    var selectedAdreess:Location? = Location(data: [:])
    var newSelectedAddress:Location? = nil
    
    let userData = Database.shared.fetchResults()

    var dateFromChat =  false
    
    
    
    //DateResponse API For  Confirm ,ReSchedule , Reject
    func setDateStatus(profile:SyncDate,dateStatus:Int,newDate:Double) {
        let apiCall = SyncAPICalls()
        Helper.showProgressIndicator(withMessage:StringConstants.setSyncDate())
        
        
        let requestParms = [likeProfileParams.status:dateStatus,
                            likeProfileParams.dateId:profile.dateId,
                            likeProfileParams.syncDate: newDate,
                            likeProfileParams.dateType: profile.dateType,
                            likeProfileParams.latitude: selectedAdreess?.latitude ?? 0.0,
                            likeProfileParams.longitude: selectedAdreess?.longitude ?? 0.0,
                            likeProfileParams.placeName: selectedAdreess?.name ?? ""
            ] as [String : Any]
        
        let requestData = RequestModel().dislikeProfileRequestDetails(Details:requestParms)
        
        apiCall.requestToSetDateStatus(requestData:requestData)
        apiCall.subject_response
            .subscribe(onNext: {response in
                
                self.handleResheduleAPIResponse(response: response)
                
                //self.syncDateSubjectResponse.onNext(true)
                Helper.hideProgressIndicator()
            }, onError: {error in
                // self.syncDateSubjectResponse.onNext(false)
                Helper.hideProgressIndicator()
            })
            
            .disposed(by:disposeBag)
    }
    
    
    
    
    //Date API
    // Creates New Date
    func createNewDate() {
        
        //sending time stamp in milliseconds
        let epochValueForSelectedDate = (newSelectedDate?.timeIntervalSince1970)!*1000
        
        //dateType : 1 -- videoDate , 2 -- inPersonDate , 3  -- audioDate
     
        let params = [likeProfileParams.targetUserId: matchedProfile.userId,
                      likeProfileParams.syncDate:epochValueForSelectedDate,
                      likeProfileParams.dateType: "\(dateType.rawValue)",
                      likeProfileParams.latitude: selectedAdreess?.latitude ?? 0.0,
                      likeProfileParams.longitude: selectedAdreess?.longitude ?? 0.0,
                      likeProfileParams.placeName: selectedAdreess?.name ?? ""] as [String : Any]
        
        
        let apiCall = SyncAPICalls()
        Helper.showProgressIndicator(withMessage:StringConstants.setSyncDate())
        let requestData = RequestModel().dislikeProfileRequestDetails(Details:params)
        apiCall.requestToSetSyncDate(requestData:requestData)
        apiCall.subject_response
            .subscribe(onNext: {response in
                self.handleCreateDateResponse(response: response)
              
            }, onError: {error in
                self.CreateDateVM_resp.onNext(.faild)
                Helper.hideProgressIndicator()
            })
            
            .disposed(by:disposeBag)
    }
    
    
    
    func handleCreateDateResponse(response:ResponseModel){
        
        switch response.statusCode {
        case 200:
            self.CreateDateVM_resp.onNext(.dateCreated)
            break
            
        default:
            errorMsg = response.response["message"] as! String
            self.CreateDateVM_resp.onNext(.faild)
            break
        }
        
    }
    
    func handleResheduleAPIResponse(response:ResponseModel) {
        switch (response.statusCode) {
        case 200:
            if let coinWallet = response.response["coinWallet"] as? [String:Any] {
                if let coinbal = coinWallet["Coin"] as? Int {
                    UserDefaults.standard.set(coinbal, forKey: "coinBalance")
                }
            }
            self.CreateDateVM_resp.onNext(.dateResponseType)
            
        default:
            errorMsg = response.response["message"] as! String
            self.CreateDateVM_resp.onNext(.faild)
            break
        }
        Helper.hideProgressIndicator()
    }
    
    
    
}
