//
//  SyncDateRequestViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 26/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class SyncDateRequestViewController: UIViewController {
    
    @IBOutlet weak var bottomConstraintForDatePickerOutlet: NSLayoutConstraint!
    @IBOutlet weak var heartImageOutlet: HeartBeatingImageView!
    @IBOutlet weak var userPicOutlet: UIImageView!
    @IBOutlet weak var matchPicOutlet: UIImageView!
    @IBOutlet weak var dateCollectionViewOutlet: DatesCollectionView!
    @IBOutlet weak var selectedTimeOutlet: UILabel!
    @IBOutlet weak var datePickerCurrentTimeOutlet: UILabel!
    @IBOutlet weak var currentDateAndTimeOutlet: UILabel!
    @IBOutlet weak var datePickerOutlet: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bottomConstraintForDatePickerOutlet.constant = -265
        self.view.layoutIfNeeded()
        self.datePickerOutlet.minimumDate = Date()
        self.dateCollectionViewOutlet.dateViewSelectedDelegate = self
        self.setTime(fromDate: self.datePickerOutlet.date, andTime: self.datePickerOutlet.date)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setTime(fromDate date: Date, andTime time:Date){
        let currentDate = CalenderUtility.getCurrentMonthAndDayWithYear(fromDate: date)
        let currentTime = CalenderUtility.getCurrentTime(fromDate: time)
        self.currentDateAndTimeOutlet.text = "\(currentDate) | \(currentTime)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.heartImageOutlet.startHeartBeating()
        self.dateCollectionViewOutlet.firstTimeScroll()
    }
    
    @IBAction func selectTimeButtonAction(_ sender: Any)
    {
        self.bottomConstraintForDatePickerOutlet.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        self.currentDateAndTimeOutlet.text = CalenderUtility.getCurrentMonthAndDayWithYear(fromDate: self.datePickerOutlet.date)
        let currentDate = CalenderUtility.getCurrentMonthAndDayWithYear(fromDate: self.datePickerOutlet.date)
        let currentTime = CalenderUtility.getCurrentTime(fromDate: self.datePickerOutlet.date)
        self.currentDateAndTimeOutlet.text = "\(currentDate) | \(currentTime)"
        self.datePickerCurrentTimeOutlet.text = currentTime
    }
    @IBAction func backButtonAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneButtonAction(_ sender: Any)
    {
//        guard let controller = self.tabBarController?.selectedIndex = 2 else {
//        }
    }
    
    @IBAction func cancelButtonAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func datePickerSelectButtonAction(_ sender: Any)
    {
        self.bottomConstraintForDatePickerOutlet.constant = -265
        let currentTime = CalenderUtility.getCurrentTime(fromDate: self.datePickerOutlet.date)
        self.selectedTimeOutlet.text = currentTime
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func datePickerCloseButtonAction(_ sender: Any)
    {
        self.bottomConstraintForDatePickerOutlet.constant = -265
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func datePickerDateChanged(_ sender: Any)
    {
        self.currentDateAndTimeOutlet.text = CalenderUtility.getCurrentMonthAndDayWithYear(fromDate: self.datePickerOutlet.date)
        let currentDate = CalenderUtility.getCurrentMonthAndDayWithYear(fromDate: self.datePickerOutlet.date)
        let currentTime = CalenderUtility.getCurrentTime(fromDate: self.datePickerOutlet.date)
        self.currentDateAndTimeOutlet.text = "\(currentDate) | \(currentTime)"
        self.datePickerCurrentTimeOutlet.text = currentTime
        self.selectedTimeOutlet.text = currentTime
    }
}

extension SyncDateRequestViewController : DateViewSelectedDelegate
{
    func dateSelected(withDate date: Date?)
    {
        self.setTime(fromDate: date!, andTime: self.datePickerOutlet.date)
    }
}
