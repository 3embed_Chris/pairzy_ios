//
//  RatingWithMessageViewController.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 09/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RatingWithMessageViewController: UIViewController {
    
    enum RatingsValue {
        case oneStar
        case twoStar
        case ThreeStar
    }
   
    @IBOutlet weak var subViewForRate: UIView!
    @IBOutlet weak var ratingViewOutlet: FloatRatingView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateType: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var doneBtn: BorderedButton!
    
    var ratingProfile:SyncDate = SyncDate(profileDetails: [:])
    
    let disposeBag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        doneBtn.setTitle(" " + StringConstants.done() + " ", for: .normal)
         // Do any additional setup after loading the view.
        self.ratingViewOutlet.delegate = self
        self.ratingViewOutlet.rating = 5
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
        doneBtn.backgroundColor = Colors.AppBaseColor
       
    }
    
    
    func removeViewWithAnimation() {
        var frameForAnimation = self.subViewForRate.frame
        frameForAnimation.origin.y = 0
        self.subViewForRate.frame = frameForAnimation
        self.subViewForRate.layoutIfNeeded()
        
        
        UIView.animate(withDuration:0.5, delay: 0.0, usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.5, options: [], animations:
            {
                frameForAnimation.origin.y = 700
                self.subViewForRate.frame = frameForAnimation
        }, completion:{ (finished) in
            if(finished) {
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    func animateTheView() {
        
        var frameForAnimation = self.subViewForRate.frame
        frameForAnimation.origin.y = -900
        self.subViewForRate.frame = frameForAnimation
        self.subViewForRate.layoutIfNeeded()
        
        UIView.animate(withDuration:0.3, delay:0.0, usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 0.7, options: [], animations:
            {
                frameForAnimation.origin.y = 0
                self.subViewForRate.frame = frameForAnimation
        }, completion:{ (finished) in
            if(finished) {
                
            }
        })
        
    }

    override func viewWillAppear(_ animated: Bool) {
        let url = URL(string: self.ratingProfile.profilePicutre)
        profileImage.kf.setImage(with:url, placeholder:UIImage.init(named:"02m"))
        
        nameLabel.text = ratingProfile.nameOfTheProfile
        dateType.text = ratingProfile.requestedFor
        animateTheView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonAction(_ sender: Any)
    {
        removeViewWithAnimation()
    }
    
    @IBAction func doneButtonAction(_ sender: Any)
    {
        
        let doneButton = sender as! UIButton
        
        UIButton.animate(withDuration: 0.1,
                         animations: {
                            doneButton.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.1, animations: {
                                doneButton.transform = CGAffineTransform.identity
                            })
        })
        
        if  Int(self.ratingViewOutlet.rating) > 0 {
            Helper.showProgressIndicator(withMessage: StringConstants.Loading())
            self.requestForApiToContact(rateValue: Int(self.ratingViewOutlet.rating), message: "")
        }else {
            showAlertForRating(title: StringConstants.RateTheDate(), isRated: false)
        }
        
        
        
    }
    
    @IBAction func tapGestureAction(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    
    func requestForApiToContact(rateValue:Int,message:String) {
        
        
        let requestDic = [ServiceInfo.rateValue:rateValue,
                          profileData.dateId: ratingProfile.dateId] as [String : Any]
        
        let requestData = RequestModel().verificationCodeRequestDetails(verifyCodeDetails:requestDic)
        
        let apiCall = Authentication()
        
        apiCall.requestForIRateApi(details: requestData)
        
        apiCall.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                let errNum:API.ErrorCode? = API.ErrorCode(rawValue: response.statusCode)!
                if errNum == .Success {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue:NSNotificationNames.notificationForRating), object: self)
                    self.removeViewWithAnimation()
                }else {
                    // show the alert for error and remove from superview
                    
                }
                
            }, onError: {error in
                Helper.hideProgressIndicator()
            })
            .disposed(by:disposeBag)
    }
    
    func showAlertForRating(title:String ,isRated: Bool){
        
        // ThanksForRating
        let alert = UIAlertController(title: title, message:nil, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title:StringConstants.ok(), style: .cancel, handler: {(action:UIAlertAction!) in
            if isRated {
                self.removeViewWithAnimation()
            }
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension RatingWithMessageViewController : FloatRatingViewDelegate
{
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        switch rating {
        case 1..<6:
            self.ratingViewOutlet.fullImage = #imageLiteral(resourceName: "red_star")
        default: break
        }
    }
}
