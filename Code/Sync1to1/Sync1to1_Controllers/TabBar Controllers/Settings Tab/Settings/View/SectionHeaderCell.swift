//
//  SectionHeaderCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 20/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class SectionHeaderCell: UITableViewCell {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var separator: UIView! // used in profile detail for About
    @IBOutlet weak var noActiveChats: UILabel!
    @IBOutlet weak var activeChatLabel: UILabel!
    
    @IBOutlet weak var viewAllButtonOutlet: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    /// updates section header label
    ///
    /// - Parameter title: is of type String title for section header
    func updateHeader(title: String){
        self.headingLabel.text = title
        self.headingLabel.font = UIFont (name: CircularAir.Bold, size: 20)
   }
    
    func updateAbout(data:Profile){
        
        self.headingLabel.font = UIFont (name: CircularAir.Book, size: 16)
        self.headingLabel.textColor = Colors.PrimaryText
        headingLabel.text = data.about
    }
    
    
}
