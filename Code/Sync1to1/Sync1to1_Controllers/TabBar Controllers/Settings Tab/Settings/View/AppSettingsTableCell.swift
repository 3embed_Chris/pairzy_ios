//
//  AppSettingsTableCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 20/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class AppSettingsTableCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
        
    /// updates thr view
    ///
    /// - Parameter title: is of type string title of cell
    func updateView(title: String){
        self.titleLabel.text = title
        
    }
    
    func updateLanguage(){
        let language = Helper.getSelectedLanguage()
        titleLabel.text = language.name
    }
    
}
