//
//  LogoutCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 20/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class LogoutCell: UITableViewCell {
    
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var logoutBtnView: UIView!
    @IBOutlet weak var deleteAccountBtn: UIButton!
    @IBOutlet weak var deleteAccountView: UIView!
    @IBOutlet weak var appLogo: UIImageView!
    @IBOutlet weak var versionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    
    /// initial setup
    func initialSetup(){
        logoutBtnView.layer.cornerRadius = 5
        logoutBtnView.layer.borderWidth = 1
        logoutBtnView.layer.borderColor = Colors.SeparatorDark.cgColor
        
        deleteAccountView.layer.cornerRadius = 5
        deleteAccountView.layer.borderWidth = 1
        deleteAccountView.layer.borderColor = Colors.SeparatorDark.cgColor
        
        var appVersion = "1.0"
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            appVersion = version
        }
        logoutBtn.setTitle(StringConstants.logout(), for: .normal)
        deleteAccountBtn.setTitle(StringConstants.deleteAccount(), for: .normal)
        versionLabel.text = StringConstants.version() + " " + appVersion
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
