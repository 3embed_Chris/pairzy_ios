//
//  SettingsViewController.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 08/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MessageUI
import RxSwift
import RxAlamofire

class SettingsViewController: UITableViewController,preferncesUpdated,UIGestureRecognizerDelegate {
    
    struct Constant {
        
        static let tableHeaderViewCutaway : CGFloat = 0.0
        static let contactUsOptions :[String] = [StringConstants.reportIssue(),StringConstants.makeSuggetion(),StringConstants.askQuestion()]
        static let cancelButtonTitle : String  = StringConstants.cancel()
        static let ratingControllerModallySegue = "ratingControllerModallySegue"
        static let editProfileSegue = "editProfileSegue"
    }
   //var tableHeaderViewHeight : CGFloat = 180.0
    var tableHeaderViewHeight : CGFloat = 220.0

    let settingsVM = SettingsVM()
    
    @IBOutlet weak var headerViewOutlet: HeaderView!
    @IBOutlet weak var myPreferencesLabel: UILabel!
    @IBOutlet weak var inviteFriendsLabel: UILabel!
    @IBOutlet weak var appSettings: UILabel!
    @IBOutlet weak var needHelpLabel: UILabel!
    @IBOutlet weak var datumPlusLabel: UILabel!
    
    var headerMaskLayer : CAShapeLayer!
     let disposeBag = DisposeBag()
    var selectedIssueForContact = 0
    
    
      //MARK:- VIEW DELEGATES.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myPreferencesLabel.text = StringConstants.myPreferences()
        inviteFriendsLabel.text = StringConstants.inviteFriends()
        appSettings.text = StringConstants.appSettings()
        needHelpLabel.text = StringConstants.needHelp()
       // datumPlusLabel.text = StringConstants.datumPlus()
         
        //self.tableHeaderViewHeight = 130                   //self.view.frame.size.height/2.7
        self.tableHeaderViewHeight = 180
        tableView.rowHeight = 75.0
        self.headerViewOutlet.layoutIfNeeded()
        self.setupHeaderView()
       // Helper.removeNavigationSeparator(controller: self.navigationController! ,image: true)
        self.addNotifications()
        self.addGestureForView()
        settingsVM.getPreferences()
    }
    
    func addGestureForView() {
        let gestureForView = UITapGestureRecognizer(target: self, action:#selector(openEditProfile))
        gestureForView.delegate = self
        self.headerViewOutlet.addGestureRecognizer(gestureForView)
    }
    
    
    @objc func openEditProfile() {
        let myProfile = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: StoryBoardIdentifier.myProfile) as! MyProfileVC
        let newNav = UINavigationController(rootViewController: myProfile)
        newNav.navigationBar.isHidden = true
        self.present(newNav, animated:true, completion:nil)
    }
    
    
    /// adding observers for events.
    func addNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(refreshProfileImage), name:NSNotification.Name(rawValue:NSNotificationNames.notificationToUpdateProfileImage), object: nil)
    }
    
    @objc func refreshProfileImage(notification: NSNotification) {
        headerViewOutlet.getUserimage()
    }
    
    
    /// method for tableview header
    ///contains image view and name of the profile.
    func setupHeaderView()
    {
        headerViewOutlet = tableView.tableHeaderView as! HeaderView
        headerViewOutlet.getUserimage()
        tableView.tableHeaderView = nil
        tableView.addSubview(headerViewOutlet)
        
        tableView.contentInset = UIEdgeInsets(top: tableHeaderViewHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -tableHeaderViewHeight + 64)
        
        // cut away the header view
        headerMaskLayer = CAShapeLayer()
        headerMaskLayer.fillColor = UIColor.black.cgColor
        headerViewOutlet.layer.mask = headerMaskLayer
        
        let effectiveHeight = tableHeaderViewHeight - Constant.tableHeaderViewCutaway/2
        tableView.contentInset = UIEdgeInsets(top: effectiveHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -effectiveHeight)
        
        headerViewOutlet.backgroundImageView.layer.cornerRadius = headerViewOutlet.backgroundImageView.frame.size.height/2
        Helper.setShadow(sender: headerViewOutlet.backgroundImageView)
        
        updateHeaderView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = true
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
       // self.navigationController?.navigationBar.isHidden = false
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateHeaderView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateHeaderView()
    }
    
      //MARK:- BUTTON  ACTIONS.
    
    
    
    
    func openActivityController() {
        
        let shareAll = [self.deepLinkForshareUrl()] as [Any]
        
        let activityViewController = UIActivityViewController(activityItems:shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    

    
    
    func deepLinkForshareUrl() -> String {
        let message = StringConstants.plzCheckoutApp()
        if  let userDeepLink = UserDefaults.standard.value(forKey: SyUserdefaultKeys.userDeepLink) as? String {
            return message.appending(userDeepLink)
        }
        return message
    }
    
    /// view profile button avtion.
    ///
    /// - Parameter sender: view profile button.
    @IBAction func viewProfileButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        //self.navigationController?.popViewController(animated: false)

    }
    /// open match pref button action.
    ///
    /// - Parameter sender: open profile button.
    @IBAction func openMatchPreferenceButtonAction(_ sender: Any) {
        let vc = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "MatchPrefrencesVc") as! MatchPreferencesVC
        vc.delegate = self
        let preferenceNav: UINavigationController = UINavigationController(rootViewController: vc)
        preferenceNav.isNavigationBarHidden =  true
        self.present(preferenceNav, animated: true, completion: nil)
      
    }
    
    
    @IBAction func inviteFriendsButtonaCTION(_ sender: Any) {
        openActivityController()
    }
    
    
    
    /// contacts button action.
    ///
    /// - Parameter sender: contacts button.
    @IBAction func contactUsButtonAction(_ sender: Any) {
        performSegue(withIdentifier: String(describing: AppSettingsVC.self), sender: self)
    }
    
    
    @IBAction func needHelpAction(_ sender: Any) {
        self.openContactUsPopUp()
    }

    
    func openContactUSXIB(resonForContact:String) {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let contactView = ContactUSView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        contactView.selectedReasonForContact = resonForContact
        contactView.contactDelegate = self
        contactView.loadContactUsView()
        window.addSubview(contactView)
    }
    
    
    func animateTheView() {
        
        
        
    }
    
    
    /// Datum Plus
    ///
    /// - Parameter sender: rating button
    @IBAction func ratingButtonAction(_ sender: Any)
    {
        showActivePlanPopUp()
//        if(Helper.isSubscribedForPlans()) {
//            showActivePlanPopUp()
//        } else {
//            showPlansPopUp()
//        }
    }
    
    func showActivePlanPopUp() {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let purchaseView = ActivePurchasePlanView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        purchaseView.loadActivePurchaseView()
        window.addSubview(purchaseView)
    }
    
    
    
    func showPlansPopUp(){
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let purchaseView = InAppPuchasePopupView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        // purchaseView.purchasePlan = self.getDatumPlusVM.purchasePlan
        purchaseView.loadInAppPurchaseView()
        window.addSubview(purchaseView)
    }
    
    //MARK: CLASS LOCAL METHODS
    
    
    /// method trigger when preferences are updated.
    func prefrenceUpdateCompleted() {
        self.tabBarController?.selectedIndex = 0
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "preferencesUpdated"), object: self, userInfo:nil)
    }
    
    
    /// method for update header with image.
    func updateHeaderView()
    {
        let effectiveheight = tableHeaderViewHeight - Constant.tableHeaderViewCutaway/2
        var headerRect = CGRect(x: 0, y: -effectiveheight, width: tableView.bounds.width, height: tableHeaderViewHeight)
        
        if tableView.contentOffset.y < -effectiveheight{
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y + Constant.tableHeaderViewCutaway/2
        }
        headerViewOutlet.frame = headerRect
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: headerRect.width, y: 0))
        path.addLine(to: CGPoint(x: headerRect.width, y: headerRect.height))
        path.addLine(to: CGPoint(x: 0, y: headerRect.height - Constant.tableHeaderViewCutaway))
        
        headerMaskLayer?.path = path.cgPath
    }
    

    //MARK:- SCROLL VIEW DELEGATES.
    
    
    /// trigger when scrollview frame changes
    ///
    /// - Parameter scrollView: tableview header.
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.updateHeaderView()
    }
    
  
    ///open mail composer.
    func presentMailController(messageSub:String){
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.setToRecipients(["appscrip@gmail.com"])
            mail.setSubject(messageSub)
            mail.mailComposeDelegate = self
            present(mail, animated: true)
        } else {
            Helper.showAlertWithMessageOnwindow(withTitle:StringConstants.error() , message: StringConstants.mailNotAv())
        }
    }
    
    
    /// METHOD TO OPEN CONTACT
    func openContactUsPopUp(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for i in 0 ..< Constant.contactUsOptions.count {
            alert.addAction(UIAlertAction(title: Constant.contactUsOptions[i], style: .default, handler: {(action:UIAlertAction!) in
                self.openContactUSXIB(resonForContact: Constant.contactUsOptions[i])
                self.selectedIssueForContact = i
                //self.presentMailController(messageSub:Constant.contactUsOptions[i])
            }))
        }
        
        alert.addAction(UIAlertAction(title: Constant.cancelButtonTitle, style: .cancel, handler: {(action:UIAlertAction!) in
            //action for cancel button.
        }))
        self.present(alert, animated: true, completion: nil)
    }
}


extension SettingsViewController : MFMailComposeViewControllerDelegate
{
    ///MFMailComposeViewController
    ///
    /// - Parameters:
    ///   - controller: MFMailComposeViewController
    ///   - result: dismissing MFMailComposeViewController
    ///   - error: contains error details.
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

}

extension SettingsViewController: contactusDelegate  {
    
    func doneButtonSelected(name:String,emailAddress:String,issue:String) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1.0, execute: {
            Helper.showAlertWithMessage(withTitle: StringConstants.thanksForContacting(), message: StringConstants.weWillReach(), onViewController: self)
            self.requestForApiToContact(name: name, emailAddress:emailAddress, issue:issue)
        })
    }
    
    func requestForApiToContact(name:String,emailAddress:String,issue:String) {
        
        let requestDic = [
                          ServiceInfo.issue:issue]
//        ServiceInfo.email:emailAddress,
//        ServiceInfo.name:name,
        
        let requestData = RequestModel().verificationCodeRequestDetails(verifyCodeDetails:requestDic)
        
        let apiCall = Authentication()
        
        apiCall.requestForIssueConatact(type:selectedIssueForContact, details:requestData)
        
        apiCall.subject_response
            .subscribe(onNext: {response in
                
            }, onError: {error in
                
            })
            .disposed(by:disposeBag)
    }
    
   
}

