//
//  AppSettingsVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 20/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class AppSettingsVC: UIViewController {
    
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var appSettingSubtitle: UILabel!
    @IBOutlet weak var topBackBtn: UIButton!
    
    let communityArray = [StringConstants.communityGuidlines(),StringConstants.safetyTips()]
    let communityLink = [SERVICE.CommunityGuideLines,SERVICE.SafetyTips]
    let legalArray = [StringConstants.privacyPolicy(),StringConstants.termsOfService(),StringConstants.licenses()]
    let legalLink = [SERVICE.PrivacyPolicy,SERVICE.TermAndCondition,SERVICE.Licenses]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        appSettingSubtitle.text = StringConstants.appSettings()
        titleLabel.text = StringConstants.appSettings()
        Helper.addDefaultNavigationGesture(VC:self)
        appSettingSubtitle.textColor = Colors.AppBaseColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.titleLabel.textColor = UIColor.clear
        headerView.backgroundColor = UIColor.clear
        topBackBtn.isHidden = true
        headerView.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       // self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func gotoLanguageSelect(){
        let vc = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "selectLanguage") as! AppLanguageVC
        self.present(vc, animated: true, completion: nil)
    }
}


// MARK: - UITableViewDelegate,UITableViewDataSource
extension AppSettingsVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return communityArray.count
        case 1:
            return legalArray.count
        case 2:
            return 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.section {
        case 0,1,2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AppSettingsTableCell", for: indexPath) as! AppSettingsTableCell
            
            if indexPath.section == 0 {
                cell.updateView(title: communityArray[indexPath.row])
            }else if indexPath.section == 1{
                cell.updateView(title: legalArray[indexPath.row])
            }else {
                cell.updateLanguage()
            }
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LogoutCell", for: indexPath) as! LogoutCell
            cell.logoutBtn.addTarget(self, action: #selector(logoutBtnAction), for: .touchUpInside)
            cell.deleteAccountBtn.addTarget(self, action: #selector(deleteAccountBtnAction), for: .touchUpInside)
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.section {
        case 0:
            gotoWebVC(title:communityArray[indexPath.row] ,link: communityLink[indexPath.row] )
            break
        case 1:
            gotoWebVC(title: legalArray[indexPath.row],link: legalLink[indexPath.row])
            break
        case 2:
            gotoLanguageSelect()
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0,1,2:
            return 60
            
        default:
            return 286
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SectionHeaderCell") as! SectionHeaderCell
        
        switch section {
        case 0:
            cell.updateHeader(title: StringConstants.community())
            cell.separator.isHidden = false
        case 1:
            cell.updateHeader(title: StringConstants.legal())
            cell.separator.isHidden = false
        case 2:
            cell.updateHeader(title: StringConstants.selectLanguage())
            cell.separator.isHidden = false
            break
        default:
            cell.updateHeader(title: "")
            cell.separator.isHidden = true
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 3
        {
            return 0
        }
        return 60
    }
}


// MARK: - Methods
extension AppSettingsVC {
    
    
    /// method to create object for web view controller
    ///
    /// - Parameters:
    ///   - title: title of web view screen
    ///   - link: url in string type
    func gotoWebVC(title:String,link: String){
        
        let vc = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "webVC") as! WebViewController
        vc.navTitle = title
        vc.link = link
        let preferenceNav: UINavigationController = UINavigationController(rootViewController: vc)
        self.present(preferenceNav, animated: true, completion: nil)
        
    }
    
    /// logout button action
    @objc func logoutBtnAction(){
        changeRootVc(deleteAccount: false)
    }
    
    /// delete account button action
    @objc func deleteAccountBtnAction(){
        changeRootVc(deleteAccount: true)
    }
    
    
    /// calls after logout changes the root view controller
    /// deletes user data and disconnects mqtt and remove all notifications
    func changeRootVc(deleteAccount:Bool) {
        var title = ""
        var actionHeader = ""   //     Helper.getAttributedText(text: "Log Out", subText: "Log Out", color: Colors.Red)
        
        
        if deleteAccount {
            title = StringConstants.areYouSureDelete()
            actionHeader = StringConstants.deleteAccount()  // Helper.getAttributedText(text: "Delete account", subText: "Delete account", color: Colors.Red)
        } else {
            title = StringConstants.areYouSureLogout()
            actionHeader = StringConstants.logout()     //Helper.getAttributedText(text: "Log Out", subText: "Log Out", color: Colors.Red)
        }
        
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        
        
        
        let deleteCancelAction: UIAlertAction = UIAlertAction(title: actionHeader,
                                                              style: .default ,handler:{ action -> Void in
                                                                
                                                                if deleteAccount {
                                                                    self.deleteAccount()
                                                                } else {
                                                                    self.moveToRootVc()
                                                                }
                                                                
        })
      
        deleteCancelAction.setValue(UIColor.red, forKey: "titleTextColor")  
        alert.addAction(deleteCancelAction)
        alert.addAction(UIAlertAction(title: StringConstants.cancel(), style: .cancel, handler: {(action:UIAlertAction!) in
            //action for cancel button.
        }))
        self.present(alert, animated: true, completion: nil)
    
    }
    
    func deleteAccount() {
        
        Helper.showProgressIndicator(withMessage: StringConstants.DeletingAccount())
        
        API.requestDELETEURL(serviceName: APINAMES.deleteAccount,
                             withStaticAccessToken:false,
                             params: [:],
                             success: { (response) in
                                Helper.hideProgressIndicator()
                                if (response.null != nil){
                                    return
                                }
                                
                                let statuscode = response.dictionaryObject!["code"] as! Int
                                switch (statuscode) {
                                case 200:
                                    self.moveToRootVc()
                                case 401:
                                    Helper.changeRootVcInVaildToken()
                                    break
                                default:
                                    self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: response.dictionaryObject![LoginResponse.errorMessage] as! String)
                                }
                                
        },
                             failure: { (Error) in
                                Helper.hideProgressIndicator()
                                self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: Error.localizedDescription)
        })
    }
    
    func moveToRootVc() {
        CalenderHelper().deleteEvents()
        //changing user online status.
        let mqttManagerInstance = MQTTChatManager.sharedInstance
        mqttManagerInstance.sendOnlineStatus(withOfflineStatus: true)
        
        //unsubscribing For Push notification topic.
        let appDelegateObj = UIApplication.shared.delegate! as! AppDelegate
        appDelegateObj.unsubscribeForPushNotificatiionTopic()
        
        UserLoginDataHelper.removeUserToken()
        UserLoginDataHelper.removeUserDefaultsData()
        Database.shared.deleteUserData()
        
        removeDataFromCouch()
        MQTT.sharedInstance.disconnectMQTTConnection()
        LNHELPER().removeAllSheduledNotifications()
        Couchbase.sharedInstance.deleteDatabase()
        let notificationName = NSNotification.Name(rawValue:NSNotificationNames.removeAllObserversForController)
        NotificationCenter.default.post(name: notificationName, object: self, userInfo:nil)
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        self.view.window?.rootViewController = storyBoard.instantiateViewController(withIdentifier: "navController")
    }
    
    //removing matches data from database.
    func removeDataFromCouch() {
        
        let matchesDocumentVm = MatchesDocumentViewModel(couchbase: Couchbase())
        guard  let docId = UserDefaults.standard.object(forKey:"userMatchData/") as? String else {return}
        matchesDocumentVm.removeDataFromDocument(withMatchDocumentId: docId)
        UserDefaults.standard.removeObject(forKey: "userMatchData/")
        UserDefaults.standard.synchronize()
    }
    
}

// MARK: - UIScrollViewDelegate
extension AppSettingsVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let movedOffset: CGFloat = scrollView.contentOffset.y
        if scrollView == mainTableView {
            self.view.endEditing(true)
            let pageWidth: CGFloat = 88      //171 - 64
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            
            if ratio > 0 {
                if movedOffset >= 20 {
                    self.titleLabel.textColor = Colors.PrimaryText.withAlphaComponent(ratio)
                    headerView.backgroundColor = UIColor.white//.withAlphaComponent(ratio)
                    Helper.setShadow(sender: headerView)
                    if  ratio <= 1 && ratio >= 0{
                        self.titleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle)*ratio)
                    }
                    if movedOffset > 88 {
                        self.titleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle))
                    }
                    headerView.isHidden = false
                    topBackBtn.isHidden = false
                }
                
            }else {
                
                self.titleLabel.textColor = UIColor.clear
                headerView.backgroundColor = UIColor.clear
                Helper.removeShadow(sender: headerView)
                topBackBtn.isHidden = true
                headerView.isHidden = true
            }
        }
    }
    
}

