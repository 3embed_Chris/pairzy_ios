//
//  EditProfilePhotoCollectionViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 03/07/17.8
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class EditProfilePhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userImageOutlet: UIImageView!
    @IBOutlet weak var unSelectedImageOutlet: UIImageView!
    @IBOutlet weak var backGroungView: UIView!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var fullScreenBtn: UIButton!
    @IBOutlet weak var setAsProfilePic: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
   
    func updateCell(){
        Helper.setShadow(sender: backGroungView)
        self.userImageOutlet.layer.cornerRadius = 4
        self.userImageOutlet.layer.borderWidth = 1
        self.userImageOutlet.layer.borderColor =  Colors.BorderColor.cgColor
    }
    
    
}
