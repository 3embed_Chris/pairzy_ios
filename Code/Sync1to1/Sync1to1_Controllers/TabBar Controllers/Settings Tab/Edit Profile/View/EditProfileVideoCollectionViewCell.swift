//
//  EditProfileVideoCollectionViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 03/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class EditProfileVideoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var videoTickImageOutlet: UIImageView!
    @IBOutlet weak var userVideoOutlet: UIImageView!
    @IBOutlet weak var imageBackgroundView: UIView!
    @IBOutlet weak var fullscreenBtn: UIButton!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var setAsProfileVideo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    /// sets the border color and corner radius
    func addBorderLayers() {
        Helper.setShadow(sender: imageBackgroundView)
        userVideoOutlet.layer.cornerRadius = 4
        userVideoOutlet.layer.borderWidth = 1
        userVideoOutlet.layer.borderColor =  Colors.BorderColor.cgColor
        userVideoOutlet.clipsToBounds = true
   
    }
    
}
