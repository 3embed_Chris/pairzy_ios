//
//  ImageDisplayCollectionViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 26/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class ImageDisplayCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var fullImageView: UIImageView!
}
