//
//  EdiitProfileViewController.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 02/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import AVKit
import RxSwift




class EdiitProfileViewController: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate,UICollectionViewDelegateFlowLayout{
    
    
    struct Constants {
        static let editProfilePhotoWithSelectedProfileCell = "editProfilePhotoWithSelectedProfileCell"
        
        static let editProfilePhotoWithUnselectedProfileCell = "editProfilePhotoWithUnselectedProfileCell"
        
        static let editProfilePhotoWithoutProfileCell = "editProfilePhotoWithoutProfileCell"
        
        static let editProfileVideoWithSelectedVideoCell = "EditProfileVideoWithSelectedVideoCell"
        
        static let editProfileVideoWithUnselectedVideoCell = "EditProfileVideoWithUnselectedVideoCell"
        
        static let editProfileWithoutVideoCell = "EditProfileWithoutVideoCell"
       
    }
    
    let disposeBag = DisposeBag()
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var profilePicCollectionViewOutlet: UICollectionView!
    @IBOutlet weak var profileVideoCollectionViewOutlet: UICollectionView!
    @IBOutlet weak var videoViewOutlet: UIView!
    @IBOutlet weak var videoViewHeigth: NSLayoutConstraint!
    @IBOutlet weak var photoViewHeight: NSLayoutConstraint!
    @IBOutlet weak var photoViewOutlet: UIView!
    @IBOutlet weak var fullImageDisplayView: UIView!
    @IBOutlet weak var disPlayCollectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var topBackBtn: UIButton!
    @IBOutlet weak var profileLabel: UILabel!
    @IBOutlet weak var editLabel: UILabel!
    @IBOutlet weak var videoLabel: UILabel!
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var addVideoButton: UIButton!
    @IBOutlet weak var addAPhoto: UIButton!
    @IBOutlet weak var profilePicCollectionViewHeightConstraint: NSLayoutConstraint!
    
    
    var sourceType = 3     // 0 for photoLibrary , 1 for Camera
    var cameraDeviceMode = 3  // 0 for front Camera ,1 for back Camera
    
    var maximumNumberOfImages = 6
    
    var selectedIndexForPhoto : IndexPath = IndexPath(item: 0, section: 0)
    var selectedIndexForVideo : IndexPath = IndexPath(item: 0, section: 0)
    var profileVideo = ""
    var userVideosArray : [Any]? = []
    var userPhotoArray :[Any]? = []
    
    var uploadingMainView = UIView()
    var uploadingProgress = UIProgressView()
    var uploadingProgressLabel = UILabel()
    var cameraCapturedImage:UIImage?
    var type = 10
    var imagePicker = UIImagePickerController()
    
    //MARK:- VIEW DELEGATES
    override func viewDidLoad() {
        super.viewDidLoad()
        addAPhoto.setTitle(StringConstants.addAPhoto(), for: .normal)
        profileLabel.textColor = Colors.AppBaseColor
        addVideoButton.setTitle(StringConstants.addAVideo(), for: .normal)
        videoLabel.text = StringConstants.video()
        titleLabel.text = StringConstants.editProfile()
        self.title = StringConstants.editProfile()
        editLabel.text = StringConstants.edit()
        profileLabel.text = StringConstants.profile()
        photoLabel.text = StringConstants.photoLowerCase()
        self.getUserData()
        self.profileVideoCollectionViewOutlet.dataSource = self
        self.profileVideoCollectionViewOutlet.delegate = self
        self.profilePicCollectionViewOutlet.dataSource = self
        self.profilePicCollectionViewOutlet.delegate = self
        // Add the actions
        imagePicker.delegate = self
        var nameOfObserVer = NSNotification.Name(rawValue: "PostCameraCapturedImage")
        NotificationCenter.default.addObserver(self, selector: #selector(postCameraCapturedImage), name: nameOfObserVer, object: nil)
        
        nameOfObserVer = NSNotification.Name(rawValue: "PostCapturedVideo")
        NotificationCenter.default.addObserver(self, selector: #selector(postCapturedVideo), name: nameOfObserVer, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        self.titleLabel.textColor = UIColor.clear
        self.headerView.backgroundColor = UIColor.clear
        self.headerView.isHidden = true
        self.topBackBtn.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openFullImageViewAction(_ sender: Any) {
        let btn = sender as! UIButton
        let imageVc:ImageVideoPreViewController = self.storyboard?.instantiateViewController(withIdentifier: "imageVideoPreViewVc") as! ImageVideoPreViewController
        imageVc.userPhotoArray = self.userPhotoArray
        imageVc.isPreviewForImage = true
        imageVc.selectedIndex = btn.tag
        self.present(imageVc, animated: true, completion:nil)
    }
    
    
    @IBAction func openFullVideoButtonAction(_ sender: Any) {
        let btn = sender as! UIButton
        let imageVc:ImageVideoPreViewController = self.storyboard?.instantiateViewController(withIdentifier: "imageVideoPreViewVc") as! ImageVideoPreViewController
        imageVc.userVideoArray = self.userVideosArray
        imageVc.isPreviewForImage = false
        imageVc.selectedIndex = btn.tag
        self.present(imageVc, animated: true, completion:nil)
    }
    
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- BUTTON ACTIONS.
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.gobackToOrigin()
    }
    
    
    
    @IBAction func doneButtonAction(_ sender: Any) {
        let doneButton = sender as! UIButton
        doneButton.addZoomEffect()
        self.checkMediaToUpdate()
    }
    
    
    @IBAction func addVideoAction(_ sender: Any) {
        presentMediaPicker(forVideo: true)
    }
    
    
    //MARK: CLASS LOCAL METHODS.
    
    //METHOD TO FETCH DATA FROM DATABASE.
    func getUserData() {
        
        let userData = Database.shared.fetchResults()
        if !userData.isEmpty
        {
            if let userDetails = userData.first
            {
                
                if  let profilePic = userDetails.profilePictureURL {
                    if(profilePic.count > 5) {
                        let imageDetails = [
                            "LocalData": 0,
                            "imageDataOrUrl":profilePic
                            ] as [String : Any]
                        
                        self.userPhotoArray?.append(imageDetails)
                    }
                }
                
                if  let profileVideo = userDetails.profileVideoUrl {
                    if(profileVideo.count > 5) {
                        let videoDetails = [
                            "LocalData": 0,
                            "imageDataOrUrl":profileVideo
                            ] as [String : Any]
                        self.userVideosArray?.append(videoDetails)
                    }
                }
                
                if let otherVideos = userDetails.otherVideos as? [String] {
                    for (index, element) in otherVideos.enumerated() {
                        print("Item \(index): \(element)")
                        if(otherVideos[index] == userDetails.profileVideoUrl) {
                            
                        } else {
                            let imageDetails = [
                                "LocalData": 0,
                                "imageDataOrUrl":otherVideos[index]
                                ] as [String : Any]
                            //  self.userVideosArray?.append(imageDetails as Any as AnyObject)
                        }
                    }
                }
                
                if let otherPhotos = userDetails.otherPics as? [String] {
                    
                    for (index, element) in otherPhotos.enumerated() {
                        print("Item \(index): \(element)")
                        if(otherPhotos[index] == userDetails.profilePictureURL) {
                            
                        } else {
                            let imageDetails = [
                                "LocalData": 0,
                                "imageDataOrUrl":otherPhotos[index]
                                ] as [String : Any]
                            
                            self.userPhotoArray?.append(imageDetails as Any as AnyObject)
                        }
                    }
                }
            }
        }
    }
    
    
    /// METHOD FOR DISMISSING EDIT PROFILE VC.
    func gobackToOrigin() {
        guard ((self.navigationController?.popViewController(animated: true)) != nil) else {
            self.dismiss(animated: true, completion: nil)
            return
        }
    }
    
    
    /// METHOD TO CHECK ANY MEDIA TO AVAILABLE FOR UPLOAD.
    func checkMediaToUpdate() {
        if(self.userPhotoArray?.count == 0 ) {
            Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: StringConstants.addOnePhoto(), onViewController: self)
        } else {
            self.checkAnyImagesToUpload()
        }
    }
    
    
    @IBAction func removeImageOrVideo(_ sender: Any) {
        let button = sender as! UIButton
        if button.tag == 2 {
            // remove video button action
            showAlertToRemoveFile(forVideo: true,tag: button.tag)
        } else {
            showAlertToRemoveFile(forVideo: false,tag: button.tag)
        }
    }
    
    
    func showAlertToRemoveFile(forVideo:Bool,tag: Int) {
        let alertTitle :String
        
        if forVideo {
            alertTitle =  StringConstants.RemoveVideo()
        } else {
            alertTitle =  StringConstants.RemoveImage()
        }
        
        
        let alert:UIAlertController=UIAlertController(title:alertTitle, message: nil, preferredStyle: UIAlertControllerStyle.alert)
        
        let cameraAction = UIAlertAction(title: StringConstants.yes(), style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            if(forVideo) {
                self.userVideosArray?.remove(at:0)
                self.profileVideoCollectionViewOutlet.reloadData()
            } else {
                let index = tag - 2000
                self.userPhotoArray?.remove(at: index)
                if self.userPhotoArray?.count == 1 {
                    self.selectedIndexForPhoto = IndexPath(item: 0, section: 0)
                }
                self.profilePicCollectionViewOutlet.reloadData()
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.1, execute: {
                    self.profilePicCollectionViewHeightConstraint.constant = self.profilePicCollectionViewOutlet.contentSize.height
                })
            }
        }
        
        let cancelAction = UIAlertAction(title: StringConstants.cancel(), style: UIAlertActionStyle.default)
        {
            UIAlertAction in
        }
        
        
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    /// METHOD FOR REQUESTING UPDATE MEDIA.
    func requestupdateMedia() {
        
        uploadingMainView.isHidden = true
        
        Helper.showProgressIndicator(withMessage: StringConstants.updateMediaMessage())
        
        let dbObj = Database.shared
        var profilePictureUrl:String = ""
        var profileVideoUrl:String = ""
        
        
        var otheProfilePics:[String] = []
        var otherProfileVideos:[String] = []
        
        if let count = self.userVideosArray?.count {
            if(count>0) {
                if let imageDic = self.userVideosArray![selectedIndexForVideo.row] as? [String:Any] {
                    if (imageDic["LocalData"] as! Int == 0) {
                        profileVideoUrl = imageDic["imageDataOrUrl"] as! String
                    }
                }
            }
        }
        
        if let imageDic = self.userPhotoArray![selectedIndexForPhoto.row] as? [String:Any] {
            if (imageDic["LocalData"] as! Int == 0) {
                profilePictureUrl = imageDic["imageDataOrUrl"] as! String
                userPhotoArray?.remove(at: selectedIndexForPhoto.row)
            }
        }
        
        var selectedProfilePics = userPhotoArray
        var selectedProfileVideos = userVideosArray
        
        
        for (index, _) in (selectedProfilePics?.enumerated())! {
            let imageDic = selectedProfilePics![index] as! [String:Any]
            let otherPictureUrl = imageDic["imageDataOrUrl"] as! String
            otheProfilePics.append(otherPictureUrl)
        }
        
        
        for (index, _) in (selectedProfileVideos?.enumerated())! {
            let imageDic = selectedProfileVideos![index] as! [String:Any]
            let otherVideoUrl = imageDic["imageDataOrUrl"] as! String
            otherProfileVideos.append(otherVideoUrl)
        }
        
        
        let locDetails:[String:Any?] = Helper.getUserLocation()
        var latitude = 0.0
        var longitude = 0.0
        
        
        if Platform.isSimulator {
            print("Running on Simulator")
            latitude = 13.028694
            longitude = 77.589564
        } else {
            let hasValue:String = locDetails["hasLocation"] as! String
            if Helper.getIsFeatureExist(){
                if(hasValue == "1") {
                    let featureLoc:Location =  Helper.getFeatureLocation()
                    latitude = featureLoc.latitude
                    longitude = featureLoc.longitude
                }
            }else {
                if(hasValue == "1") {
                    if let lat = locDetails["lat"] as? Double {
                        latitude = lat
                    }
                    if let long = locDetails["long"] as? Double {
                         longitude = long
                    }
                }
            }
        }

        if dbObj.userDataObj.profilePictureURL != profilePictureUrl {
            type = 1
        }
        
        let jpgURL = URL(string: profileVideoUrl)?
            .deletingPathExtension()
            .appendingPathExtension("jpg")
        let url = jpgURL?.absoluteString
        let params =
            [ ServiceInfo.UserImageLink          : profilePictureUrl ,
              ServiceInfo.UservideoLink        :  profileVideoUrl,            //profileVideoUrl,
                ServiceInfo.profileVideoThumbnail       :url ?? "",
                ServiceInfo.otherProfilePic       : otheProfilePics,
                ServiceInfo.latitude            :latitude,
                ServiceInfo.longitude           :longitude,
                ServiceInfo.type: type]  as [String:Any]
        
       
        
        dbObj.userDataObj.profilePictureURL = profilePictureUrl
        
        
        dbObj.userDataObj.otherPics = otheProfilePics as NSObject
        dbObj.userDataObj.profileVideoUrl = profileVideoUrl
        // dbObj.userDataObj.otherVideos = otherProfileVideos as NSObject
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        
        
        API.requestPUTURL(serviceName:APINAMES.profile, withStaticAccessToken:false, params:params, success: { (response) in
            
            let statuscode = response.dictionaryObject!["code"] as! Int
            
            switch  statuscode{
            case 200:
                
                dbObj.userDataObj.profilePictureURL = profilePictureUrl
                dbObj.userDataObj.otherPics = otheProfilePics as NSObject
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: NSNotificationNames.notificationToUpdateProfileImage), object: nil)
                self.gobackToOrigin()
                
                break;
            case 401:
                Helper.changeRootVcInVaildToken()
                break
            default:
                Helper.showAlertMessageViewFromTopNavBar(showOnView: self.view, message:response.dictionaryObject![LoginResponse.errorMessage] as! String)
            }
            
            Helper.hideProgressIndicator()
            
            
        }, failure:{ (error) in
            Helper.hideProgressIndicator()
            Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
        })
        
    
    }
    
    
    /// METHOD FOR BUTTON OPENING CAMERA
    ///
    /// - Parameter forVideo: TRUE IS  OPENING CAMERA FOR VIDEO.
    func
        openCamera(forVideo:Bool)
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            
//            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
//            self.imagePicker.cameraDevice = .front
//
//            if forVideo {
//                imagePicker.mediaTypes = ["public.movie"]
//            } else {
//                imagePicker.mediaTypes = ["public.image"]
//            }
//
//            self .present(imagePicker, animated: true, completion: nil)
//
//        }
            
            let picker = YPImagePicker()
            if forVideo{
                picker.showsVideosVC = true
                picker.showsCameraVC = false
            }else{
                picker.showsVideosVC = false
                picker.showsCameraVC = true
            }
            DispatchQueue.main.async {
                self.present(picker, animated: true, completion: nil)
           }
        }
        else{
            
            Helper.showAlertWithMessage(withTitle:StringConstants.warning() , message: StringConstants.dontHaveCamera(), onViewController:self)
        }
        
    }
    
    
    @objc func postCameraCapturedImage(_ notification: NSNotification){
        
        let userInfo = notification.userInfo as! [String: Any]
        if let cameraImage = userInfo["image"] {
            let filename = getDocumentsDirectory().appendingPathComponent(Helper.getCurrentTimeStamp())
            if let data = UIImagePNGRepresentation(cameraImage as! UIImage) {
                try? data.write(to: filename)
            }
            
            let imageDetails = [
                "LocalData": 1,
                "imageDataOrUrl":filename.path,
                "image": cameraImage,
                "ifFront": cameraDeviceMode
                ] as [String : Any]
            
            self.userPhotoArray?.append(imageDetails as Any as AnyObject)
            self.profilePicCollectionViewOutlet.reloadData()
        
        }
    }
    
    @objc func postCapturedVideo(_ notification: NSNotification){
        
        let filename = getDocumentsDirectory().appendingPathComponent(Helper.getCurrentTimeStamp())
        let userInfo = notification.userInfo as! [String: Any]
        if let videoPath = userInfo["video"] as? URL {
            //if let videoURL = info[UIImagePickerControllerMediaURL] as? NSURL {
            if var image = thumbnail(sourceURL: videoPath as NSURL) as? UIImage{
                image = fixOrientation(img: image)
                if let data = UIImagePNGRepresentation(image) {
                    try? data.write(to: filename)
                }
            }
            
            let imageDetails = [
                "LocalData": 1,
                "imageDataOrUrl":filename.path,
                "videoFile": videoPath
                ] as [String : Any]
            
            
            
            self.userVideosArray?.append(imageDetails as Any as AnyObject)
            self.profileVideoCollectionViewOutlet.reloadData()
        }
    }
    
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
    
    /// METHOD FOR OPENING GALLERY
    ///
    /// - Parameter forVideo:TRUE IS  OPENING CAMERA FOR VIDEO.
    func openGallary(forVideo:Bool)
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.title = StringConstants.video()
        
        if forVideo {
            imagePicker.mediaTypes = ["public.movie"]
        } else {
            imagePicker.mediaTypes = ["public.image"]
        }
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: IMAGE PICKER.
    
    
    /// METHO FOR UIIMAGE PICKER.
    ///
    /// - Parameters:
    ///   - navigationController: current navigation controller.
    ///   - viewController: uiimagepicker
    ///   - animated: true f need animation.
    func navigationController(_ navigationController: UINavigationController,willShow viewController: UIViewController, animated: Bool) {
        
        if imagePicker.mediaTypes == ["public.image"] {
            viewController.navigationItem.title = StringConstants.photo()
        } else {
            viewController.navigationItem.title = StringConstants.video()
        }
        
    }
    
    
    
    /// method for opening media picker.
    ///
    /// - Parameter forVideo:TRUE IS  OPENING picker FOR VIDEO.
    func presentMediaPicker(forVideo:Bool) {
        
        let alertTitle :String
        let mediaTitle:String
        
        if forVideo {
            alertTitle = StringConstants.chooseVideo()
            mediaTitle = StringConstants.camera()
            
        } else {
            alertTitle =  StringConstants.chooseImage()
            mediaTitle = StringConstants.camera()
        }
        
        
        let alert:UIAlertController=UIAlertController(title:alertTitle, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        
        let cameraAction = UIAlertAction(title: mediaTitle, style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera(forVideo: forVideo)
        }
        
        let gallaryAction = UIAlertAction(title: StringConstants.uploadToGallery(), style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary(forVideo: forVideo)
        }
        
        let cancelAction = UIAlertAction(title: StringConstants.cancel(), style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //fetching local documentry.
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    /// method for image picker finsh
    ///
    /// - Parameters:
    ///   - picker: image picker
    ///   - info: contains selected image /video detials.
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let filename = getDocumentsDirectory().appendingPathComponent(Helper.getCurrentTimeStamp())
        
        if var selImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if(selImage.size.width > 499){
                selImage = selImage.resizeImage(image: selImage, targetSize: CGSize(width: 500, height: 500))
            }
            selImage = selImage.compressTo(0.5)!
            selImage = fixOrientation(img: selImage)
            if picker.sourceType.rawValue == 1 { //
                if picker.cameraDevice == .front {
                    selImage = UIImage(cgImage: selImage.cgImage!, scale: selImage.scale, orientation: .upMirrored)
                }
            }
            
            if let data = UIImagePNGRepresentation(selImage) {
                try? data.write(to: filename)
            }
            
            let imageDetails = [
                "LocalData": 1,
                "imageDataOrUrl":filename.path,
                "image": selImage,
                "ifFront": cameraDeviceMode
                ] as [String : Any]
            
            self.userPhotoArray?.append(imageDetails as Any as AnyObject)
            self.profilePicCollectionViewOutlet.reloadData()
       
        }else {
            
            let filename = getDocumentsDirectory().appendingPathComponent(Helper.getCurrentTimeStamp())
            if let videoURL = info[UIImagePickerControllerMediaURL] as? NSURL {
                if var image = thumbnail(sourceURL: videoURL) as? UIImage{
                    image = fixOrientation(img: image)
                    if let data = UIImagePNGRepresentation(image) {
                        try? data.write(to: filename)
                    }
                }
            }
            
            let imageDetails = [
                "LocalData": 1,
                "imageDataOrUrl":filename.path,
                "videoFile":info["UIImagePickerControllerMediaURL"] ?? ""
                ] as [String : Any]
            
            
            
            self.userVideosArray?.append(imageDetails as Any as AnyObject)
            self.profileVideoCollectionViewOutlet.reloadData()
            
        }
        
        
        
        if imagePicker.mediaTypes == ["public.image"] {
            
        } else {
            
            
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    func thumbnail(sourceURL:NSURL) -> UIImage {
        let asset = AVAsset(url: sourceURL as URL)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        let time = CMTime(seconds: 1, preferredTimescale: 1)
        do {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            
            let image = UIImage(cgImage: imageRef)
            let imgData = NSData(data: UIImageJPEGRepresentation((image), 0.2)!)
            let compressedImage = UIImage(data: imgData as Data)
            print(imgData.length/1024)
            return compressedImage!
        } catch {
            return #imageLiteral(resourceName: "play")
        }
    }
    
    
    /// removing data from dircory.
    ///
    /// - Parameter removeDataAtURL:url to remove data.
    func removeDatafromDocument(removeDataAtURL:URL) {
        let fileManager = FileManager.default
        do {
            try fileManager.removeItem(atPath: removeDataAtURL.path)
        } catch let error as NSError {
            print(error.debugDescription)
        }
    }
    
}



//MARK: COLLECTION VIEW DELEGATE AND DATA SOURCE.

extension EdiitProfileViewController : UICollectionViewDelegate {
    
    
    /// METHOD TO ALLOCATE SIZE FOR COLLECTION VIEW CELL SIZZE.
    ///
    /// - Parameters:
    ///   - collectionView: profilePicCollectionViewOutlet
    ///   - collectionViewLayout:
    ///   - indexPath: current ndexpath
    /// - Returns: size of the cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.profilePicCollectionViewOutlet {
            return CGSize(width: self.view.frame.size.width/3 - 30, height: CGFloat(150.00))
        }else {
            return CGSize(width: self.view.frame.size.width/3 - 30, height: self.view.frame.size.width/3 - 30)
        }
        
    }
    
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        var size = CGSize.zero
//        let cellWidth = (collectionView.frame.size.width - 20)/3
//        let cellHight = (cellWidth)
//        size.height =  cellHight
//        size.width  =  cellWidth
//        return size
//
    
//    // MARK: - UICollectionViewDelegateFlowLayout
//    let edgeGaps:UIEdgeInsets = UIEdgeInsets.init(top: 5, left: 10, bottom: 5, right: 10)
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        let cellWidth = ((self.momentCollectionView.bounds.width - 15) - edgeGaps.left - edgeGaps.right) / 3
//        let cellHeight = cellWidth
//        return CGSize(width: cellWidth, height: cellHeight)
//    }
//
    
    /// collection view delegate ..triggers when user select any cell.
    ///
    /// - Parameters:
    ///   - collectionView:profilePicCollectionViewOutlet
    ///   - indexPath: current indexpath
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let totalRows = collectionView.numberOfItems(inSection: 0)
        
        if collectionView == self.profilePicCollectionViewOutlet {
            if totalRows-1 == indexPath.row {
                if totalRows == maximumNumberOfImages{
                    Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: StringConstants.plzDeleteOneImage(), onViewController: self)
                    return
                }
                presentMediaPicker(forVideo: false)
                //     setProfilePic()
                
                return
            }
            self.selectedIndexForPhoto = indexPath
            collectionView.reloadData()
        } else {
            
            if totalRows-1 == indexPath.row {
                
                if totalRows == 3 {
                    Helper.showAlertWithMessage(withTitle:StringConstants.error(), message: StringConstants.plzDeleteOneVideo(), onViewController: self)
                    return
                }
                presentMediaPicker(forVideo: true)
                //   gotoVideoRecord()
                
                return
            }
            self.selectedIndexForVideo = indexPath
            collectionView.reloadData()
        }
    }
    
    
}

extension EdiitProfileViewController : UICollectionViewDataSource {
    
    
    
    /// method for cell data display.
    ///
    /// - Parameters:
    ///   - collectionView: profilePicCollectionViewOutlet
    ///   - indexPath: current indexpath
    /// - Returns: cell after adding detials.
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let totalRows = collectionView.numberOfItems(inSection: 0)
        switch collectionView {
        case profilePicCollectionViewOutlet:
            switch indexPath.row {
            case totalRows-1:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.editProfilePhotoWithoutProfileCell, for: indexPath) as! EditProfilePhotoCollectionViewCell
                
                // cell.updateCell()
                return cell
                
            case selectedIndexForPhoto.row:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.editProfilePhotoWithSelectedProfileCell, for: indexPath) as! EditProfilePhotoCollectionViewCell
                cell.fullScreenBtn.tag = indexPath.row
                cell.updateCell()
                 cell.setAsProfilePic.text = StringConstants.setAsProfilePicture()
                UIView.animate(withDuration: 0.1, animations: {
                    cell.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                })
                if let imageDic = self.userPhotoArray![indexPath.row] as? [String:Any] {
                    
                    if (imageDic["LocalData"] as! Int == 0){
                        let url = URL(string: (imageDic["imageDataOrUrl"] as? String)!)
                        cell.userImageOutlet.kf.setImage(with:url)
                    } else {
                        
                        cell.userImageOutlet.image = imageDic["image"] as! UIImage            //UIImage(contentsOfFile: imageDic["imageDataOrUrl"] as! String)
                    }
                }
                
                return cell
                
            default :
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.editProfilePhotoWithUnselectedProfileCell, for: indexPath) as! EditProfilePhotoCollectionViewCell
                
                cell.updateCell()
                cell.fullScreenBtn.tag = indexPath.row
                cell.removeBtn.tag = 2000 + indexPath.item
                cell.setAsProfilePic.text = StringConstants.setAsProfilePicture()
                if let imageDic = self.userPhotoArray![indexPath.row] as? [String:Any] {
                    if (imageDic["LocalData"] as! Int == 0) {
                        let url = URL(string: (imageDic["imageDataOrUrl"] as? String)!)
                        cell.userImageOutlet.kf.setImage(with:url)
                    } else {
                        
                        cell.userImageOutlet.image = imageDic["image"] as! UIImage /// self.changeImageFrame(with: UIImage(contentsOfFile: imageDic["imageDataOrUrl"] as! String)!, scaledTo: CGSize(width: cell.userImageOutlet.bounds.size.width, height: cell.userImageOutlet.bounds.size.height))
                    }
                }
                
                return cell
            }
            
        case profileVideoCollectionViewOutlet:
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.editProfileVideoWithUnselectedVideoCell, for: indexPath) as! EditProfileVideoCollectionViewCell
            
            if let imageDic = self.userVideosArray![indexPath.row] as? [String:Any] {
                if (imageDic["LocalData"] as! Int == 0) {
                    let videoUrl = imageDic["imageDataOrUrl"] as? String
                    var url = URL(string: (imageDic["imageDataOrUrl"] as? String)!)
                    
                    if(videoUrl? .contains("res.cloudinary.com"))! {
                        let modifedTumbNailVideo = videoUrl
                        url = URL(string:modifedTumbNailVideo!)?
                            .deletingPathExtension()
                            .appendingPathExtension("jpg")        //modifedTumbNailVideo?.replace(target: ".mov", withString: ".jpg")
                      // let url = URL(string: modifedTumbNailVideo!)

                    }
                    cell.userVideoOutlet.kf.setImage(with:url)
                    
                } else {
                    cell.userVideoOutlet.image = UIImage(contentsOfFile: imageDic["imageDataOrUrl"] as! String)
                }
            }
            cell.addBorderLayers()
            cell.fullscreenBtn.tag = indexPath.row
            return cell
            
            
        default :
            return UICollectionViewCell()
        }
    }
    
    
    /// method for alloting number of rows to the section.
    ///
    /// - Parameters:
    ///   - collectionView:
    ///   - section: CURRENT SECTION
    /// - Returns: RETURNS NUMBER OF ROWS IN THE SECTION.
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
            
        case profilePicCollectionViewOutlet:
            if let count = self.userPhotoArray?.count {
                if count == 0 {
                    
                    return 1
                } else if(count > maximumNumberOfImages-1) {
                    
                    // for maximum number ofimages.
                    return maximumNumberOfImages
                } else {
                    
                    return count+1
                }
            }
            else {
                return 1
            }
            
        case profileVideoCollectionViewOutlet:
            if let count = self.userVideosArray?.count {
                if count == 0 {
                    videoViewHeigth.constant = 150
                    profileVideoCollectionViewOutlet.isHidden = true
                    return 0
                }
                else {
                    profileVideoCollectionViewOutlet.isHidden = false
                    videoViewHeigth.constant = 200
                    return 1
                }
            }
            else {
                return 1
            }
            
        default :
            return 0
        }
    }
    
    
    /// METHOD FOR CHECKING image uploads.
    func checkAnyImagesToUpload() {
        
        if (userPhotoArray?.count == 0) && (userVideosArray?.count == 0){
            //no images available
        } else {
            for (index,_) in (userPhotoArray?.enumerated())! {
                if let imageDic = self.userPhotoArray![index] as? [String:Any] {
                    if (imageDic["LocalData"] as! Int == 1) {
                        Helper.showProgressIndicator(withMessage: StringConstants.Uploading())
                        let tempImage = imageDic["image"] as! UIImage              //UIImage(contentsOfFile: imageDic["imageDataOrUrl"] as! String)
                        self.uploadImageToCloudinary(image:tempImage,atIndex:index)
                        break
                    }
                    
                    if(index + 1 == self.userPhotoArray?.count) {
                        
                        if (self.userVideosArray?.count)! > 0 {
                            if let videoDict = self.userVideosArray![0]  as? [String:Any] {
                                if (videoDict["LocalData"] as! Int == 1) {
                                    Helper.showProgressIndicator(withMessage: StringConstants.Uploading())
                                    if let tempUrl = videoDict["videoFile"]  as? URL {
                                        
                                        self.uploadVideoToCloudinary(video: tempUrl)
                                    }
                                }else {
                                    self.requestupdateMedia()
                                }
                            }
                        }else {
                            self.requestupdateMedia()
                        }
                        
                    }
                }
            }
            
            
            
            //  self.uploadVideoToCloudinary(video: videoURL as URL)
        }
    }
    
    //method to upload data to cloudinary.
    func uploadImageToCloudinary(image: UIImage, atIndex:Int ) {
        let lang = Helper.getCludinaryDetails()
        let cloudinary = CLDCloudinary(configuration: CLDConfiguration(cloudName:lang["cloudName"] as! String, secure: true))
        
        let params = CLDUploadRequestParams()
        params.setResourceType(CLDUrlResourceType.image)
        let data = UIImageJPEGRepresentation(image, 1.0)
        let timestamp = Helper.getCurrentTimeStamp()
        params.setPublicId("\(timestamp)")
        cloudinary.createUploader().upload(data: data!, uploadPreset: lang["uploadPreset"] as! String, params: params, progress:{ (progress) in
            
        }, completionHandler: { (result, error) in
            Helper.hideProgressIndicator()
            if error != nil {
                self.uploadingMainView.isHidden = true
                
            } else {
                
                if let result = result {
                    let imageDetails = [
                        "LocalData": 0,
                        "imageDataOrUrl":result.url!
                        ] as [String : Any]
                    self.type = 3
                    self.userPhotoArray![atIndex] = imageDetails
                    self.checkAnyImagesToUpload()
                }
                
            }
        })
    }
    
    
    
    func uploadVideoToCloudinary(video: URL) {  //deu1yq6y6
        let lang = Helper.getCludinaryDetails()
        let cloudinary = CLDCloudinary(configuration: CLDConfiguration(cloudName:lang["cloudName"] as! String, secure: true))
        //self.uploadingProgress.setProgress(uploadedProgress, animated: true)
        let params = CLDUploadRequestParams()
        params.setResourceType(CLDUrlResourceType.video)
        let timestamp = Helper.getCurrentTimeStamp()
        params.setPublicId("\(timestamp)")
        Helper.showProgressIndicator(withMessage: StringConstants.Uploading())
        do {
            cloudinary.createUploader().upload(url: video, uploadPreset: lang["uploadPreset"] as! String, params: params, progress: { (progress) in
                
                
            }, completionHandler: { (result, error) in
                Helper.hideProgressIndicator()
                DispatchQueue.main.async {
                    if error != nil {
                        print("failed to upload")
                    } else {
                        if let result = result{
                            var uploadedVideoUrl = result.resultJson["url"] as? String
                            uploadedVideoUrl = uploadedVideoUrl?.replace(target: "video/upload", withString: "video/upload/q_60")
                            
                            
                            let imageDetails = [
                                "LocalData": 0,
                                "imageDataOrUrl":result.url!
                                ] as [String : Any]
                            self.userVideosArray?.removeAll()
                            self.userVideosArray?.append(imageDetails)
                            self.type = 2
                            self.requestupdateMedia()
                        }
                    }
                }
            })
        }
    }
    
    
    
    func changeImageFrame(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.5)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? UIImage()
    }
}


// MARK: - UIScrollViewDelegate
extension EdiitProfileViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let movedOffset: CGFloat = scrollView.contentOffset.y
        if scrollView == mainScrollView {
            self.view.endEditing(true)
            let pageWidth: CGFloat = 160      //171 - 64
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            
            if ratio > 0 {
                if movedOffset >= 20 {
                    self.titleLabel.textColor = Colors.PrimaryText.withAlphaComponent(ratio)
                    headerView.backgroundColor = Colors.SecondBaseColor//.withAlphaComponent(ratio)
                    Helper.setShadow(sender: headerView)
                    if  ratio <= 1 && ratio >= 0{
                        self.titleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle)*ratio)
                    }
                    if movedOffset > 160 {
                        self.titleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle))
                    }
                    headerView.isHidden = false
                    topBackBtn.isHidden = false
                }
                
            }else {
                self.titleLabel.textColor = UIColor.clear
                headerView.backgroundColor = UIColor.clear
                Helper.removeShadow(sender: headerView)
                topBackBtn.isHidden = true
                headerView.isHidden = true
            }
        }
    }
    
    
    
    
   
    
}



