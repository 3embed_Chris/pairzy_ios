//
//  CameraPreviewController.swift
//  Datum
//
//  Created by Rahul Sharma on 30/07/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

protocol CameraPreviewControllerDelegate: class{
    func dismissYPImagePicker()
}


class CameraPreviewController: UIViewController {

   weak var delegate: CameraPreviewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePreview.image = capturedImage
    
        // Do any additional setup after loading the view.
        
    }
   
    @IBOutlet weak var imagePreview: UIImageView!
    var capturedImage:UIImage?
    var isFromCameraPreview = false
   
    @IBAction func doneButtonAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popToRootViewController(animated: false)
        delegate?.dismissYPImagePicker()
        let notificationName = NSNotification.Name(rawValue: "PostCameraCapturedImage")
        NotificationCenter.default.post(name: notificationName, object: self, userInfo: ["image": capturedImage as Any])
        
        let imageNotificationName = NSNotification.Name(rawValue: "SignUpProfileImage")
        NotificationCenter.default.post(name: imageNotificationName, object: self, userInfo: ["image": capturedImage as Any])
    }
    
    @IBAction func backButtonAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
