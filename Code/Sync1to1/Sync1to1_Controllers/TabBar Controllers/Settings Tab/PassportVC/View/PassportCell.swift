//
//  PassportCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 28/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class PassportCell: UITableViewCell {

    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var placeAddress: UILabel!
    @IBOutlet weak var selectionTickButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var locationIcon: UIImageView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func updateCell(data:Location){
        self.placeName.text = data.name
        self.placeAddress.text = data.fullText
    }
    
    
    func updateCellBtns(tag: Int){
        if self.selectionTickButton.tag == tag {
            self.selectionTickButton.isSelected = true
            removeButton.isHidden = false
        }
        else{
            self.selectionTickButton.isSelected = false
          removeButton.isHidden = true
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
