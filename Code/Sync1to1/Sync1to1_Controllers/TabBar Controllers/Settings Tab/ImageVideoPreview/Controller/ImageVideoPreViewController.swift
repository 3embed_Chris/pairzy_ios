//
//  ImageVideoPreViewController.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 15/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import  VGPlayer

class ImageVideoPreViewController: UIViewController {
    @IBOutlet weak var collectionViewOutlet: UICollectionView!
    
    var userPhotoArray :[Any]? = []
    var userVideoArray:[Any]? = []
    
    var isPreviewForImage:Bool = false
    var player : VGPlayer?
    
    var selectedIndex = 0
    
    var selectedCellForPlayingVideo = 0
    var previousCellForPalyingVideo = -1
    
    @IBOutlet weak var imageCountLabel: UILabel!
    
  
    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //adding down gesture to view for dismissing view controller.
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        
        
        if(self.isPreviewForImage) {
            let totalNumberOfImages = String(describing: self.userPhotoArray?.count ?? 0)
            self.imageCountLabel.text = "\(selectedIndex+1)/\(totalNumberOfImages)"
        } else {
            let totalNumberOfImages = String(describing: self.userVideoArray?.count ?? 0)
            self.imageCountLabel.text = "\(selectedIndex+1)/\(totalNumberOfImages)"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scrollToCell()
    }
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.down:
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollToCell()
    }
    
    func scrollToCell(){
        let screenSize = NSInteger(self.view.frame.size.width)*selectedIndex
        self.collectionViewOutlet.setContentOffset(CGPoint(x:screenSize,y:0), animated:true)
    }
    
}


extension ImageVideoPreViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(isPreviewForImage) {
           return self.userPhotoArray!.count
        } else {
            return self.userVideoArray!.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let imageCell:imagePreviewCollectionViewCell =  collectionView.dequeueReusableCell(withReuseIdentifier:"imageCell", for: indexPath) as! imagePreviewCollectionViewCell
        
        if(self.isPreviewForImage) {
            imageCell.videoPlayButton.isHidden = true
            if let imageDic = self.userPhotoArray![indexPath.row] as? [String:Any] {
                if (imageDic["LocalData"] as! Int == 0) {
                    let url = URL(string: (imageDic["imageDataOrUrl"] as? String)!)
                    imageCell.imageViewOutlet.kf.setImage(with:url)
                } else {
                    imageCell.imageViewOutlet.image = UIImage(contentsOfFile: imageDic["imageDataOrUrl"] as! String)
                }
            } else if let imageUrl = self.userPhotoArray![indexPath.row] as? String {
                let url = URL(string:imageUrl)
                imageCell.imageViewOutlet.kf.setImage(with:url)
            }
        } else {
            imageCell.videoPlayButton.isHidden = false
            
            if(indexPath.row == selectedCellForPlayingVideo) {
                if(self.player?.state == .playing) {
                    imageCell.videoPlayButton.isSelected = true
                } else {
                     imageCell.videoPlayButton.isSelected = false
                }
            } else {
                 imageCell.videoPlayButton.isSelected = false
            }
            imageCell.videoPlayButton.tag = indexPath.row
            
            if let imageDic = self.userVideoArray![indexPath.row] as? [String:Any] {
                if (imageDic["LocalData"] as! Int == 0) {
                    let videoUrl = imageDic["imageDataOrUrl"] as? String
                    var url = URL(string: (imageDic["imageDataOrUrl"] as? String)!)
                    self.setUpVideoPlayer(videoUrl:url!,viewForVideo:imageCell.viewForVideo)
                    imageCell.viewForVideo.bringSubview(toFront: imageCell.imageViewOutlet)
                    imageCell.viewForVideo.bringSubview(toFront: imageCell.videoPlayButton)
                    if(videoUrl? .contains("res.cloudinary.com"))! {
                        var modifedTumbNailVideo = videoUrl
                        modifedTumbNailVideo = modifedTumbNailVideo?.replace(target: ".mov", withString: ".jpeg")
                        url = URL(string: modifedTumbNailVideo!)
                    }
                    imageCell.imageViewOutlet.kf.setImage(with:url)
                } else{
                    let videoUrl = imageDic["videoFile"] as? URL
                 self.setUpVideoPlayer(videoUrl:videoUrl!,viewForVideo:imageCell.viewForVideo)
                    imageCell.viewForVideo.bringSubview(toFront: imageCell.imageViewOutlet)
                    imageCell.viewForVideo.bringSubview(toFront: imageCell.videoPlayButton)
                    imageCell.imageViewOutlet.image = UIImage(contentsOfFile: imageDic["imageDataOrUrl"] as! String)
                }
            }
        }
        return imageCell
    }
    
    
    func setUpVideoPlayer(videoUrl:URL,viewForVideo:UIView)  {
        player = VGPlayer(URL: videoUrl)
        player?.delegate = self
        viewForVideo.addSubview((player?.displayView)!)
        player?.backgroundMode = .proceed
        player?.displayView.delegate = self
        player?.displayView.snp.makeConstraints { [weak viewForVideo] (make) in
            guard let strongSelf = viewForVideo else { return }
            make.edges.equalTo(strongSelf)
        }
    }
    
    
    func setupVideoForLocalData(videoUrl:URL,viewForVideo:UIView) {
        player = VGPlayer(URL: videoUrl)
        player?.delegate = self
        viewForVideo.addSubview((player?.displayView)!)
        player?.backgroundMode = .proceed
        player?.displayView.delegate = self
        player?.displayView.snp.makeConstraints { [weak viewForVideo] (make) in
            guard let strongSelf = viewForVideo else { return }
            make.edges.equalTo(strongSelf)
        }
    }
    
    func getViewForCell(indexRow:Int) -> imagePreviewCollectionViewCell {
        let indexPath = IndexPath(row: indexRow, section:0)
        let imageCell:imagePreviewCollectionViewCell =  self.collectionViewOutlet.cellForItem(at: indexPath) as! imagePreviewCollectionViewCell
        return imageCell
    }
    
    func showVideoForUrl() {
        player?.play()
    }
    
    @IBAction func videoPlayButtonAction(_ sender: Any) {
        let selectedButton = sender as! UIButton
        
        previousCellForPalyingVideo = selectedCellForPlayingVideo
        selectedCellForPlayingVideo = selectedButton.tag
        
        if(selectedCellForPlayingVideo == previousCellForPalyingVideo) {
            if(self.player?.state == .playFinished) {
                self.player?.displayView.replayButton.sendActions(for: .touchUpInside)
            } else {
                self.player?.displayView.playButtion.sendActions(for: .touchUpInside)
            }
            return
        }
        
        let imageCell = self.getViewForCell(indexRow: selectedCellForPlayingVideo)
        if let imageDic = self.userVideoArray![selectedCellForPlayingVideo] as? [String:Any] {
            if (imageDic["LocalData"] as! Int == 0) {
                let videoUrl = imageDic["imageDataOrUrl"] as? String
                var url = URL(string: (imageDic["imageDataOrUrl"] as? String)!)
                self.setUpVideoPlayer(videoUrl:url!,viewForVideo:imageCell.viewForVideo)
                imageCell.viewForVideo.bringSubview(toFront: imageCell.imageViewOutlet)
                imageCell.viewForVideo.bringSubview(toFront: imageCell.videoPlayButton)
                if(videoUrl? .contains("res.cloudinary.com"))! {
                    var modifedTumbNailVideo = videoUrl
                    modifedTumbNailVideo = modifedTumbNailVideo?.replace(target: ".mov", withString: ".jpeg")
                    url = URL(string: modifedTumbNailVideo!)
                }
                imageCell.imageViewOutlet.kf.setImage(with:url)
            }
        }
        
        if(self.player?.state == .playFinished) {
            self.player?.displayView.replayButton.sendActions(for: .touchUpInside)
        } else {
            self.player?.displayView.playButtion.sendActions(for: .touchUpInside)
        }
        
       
    }

    
    /// METHOD TO ALLOCATE SIZE FOR COLLECTION VIEW CELL SIZZE.
    ///
    /// - Parameters:
    ///   - collectionView: collectionViewOutlet
    ///   - collectionViewLayout:
    ///   - indexPath: current ndexpath
    /// - Returns: size of the cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        player?.pause()
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var currentIndex:Int = Int(self.collectionViewOutlet.contentOffset.x / self.collectionViewOutlet.frame.size.width)
        currentIndex = currentIndex+1
        if(self.isPreviewForImage) {
            let totalNumberOfImages = String(describing: self.userPhotoArray?.count ?? 0)
            self.imageCountLabel.text = "\(currentIndex)/\(totalNumberOfImages)"
        } else {
            let totalNumberOfImages = String(describing: self.userVideoArray?.count ?? 0)
            self.imageCountLabel.text = "\(currentIndex)/\(totalNumberOfImages)"
        }
    }
    
    
}

extension ImageVideoPreViewController: VGPlayerDelegate {
    func vgPlayer(_ player: VGPlayer, playerFailed error: VGPlayerError) {
        print(error)
    }
    
    func vgPlayer(_ player: VGPlayer, stateDidChange state: VGPlayerState) {
        print("player State ",state)
        let selectedVideoCell  = self.getViewForCell(indexRow: selectedCellForPlayingVideo)
        if (player.state == .playing) {
            selectedVideoCell.imageViewOutlet.isHidden = true
            selectedVideoCell.videoPlayButton.isSelected = true
        } else {
            selectedVideoCell.imageViewOutlet.isHidden = false
            selectedVideoCell.videoPlayButton.isSelected = false
        }
    }
    
    func vgPlayer(_ player: VGPlayer, bufferStateDidChange state: VGPlayerBufferstate) {
        print("buffer State", state)
    }
}

extension ImageVideoPreViewController: VGPlayerViewDelegate {
    func vgPlayerView(_ playerView: VGPlayerView, willFullscreen fullscreen: Bool) {
        
    }
    
    func vgPlayerView(didTappedClose playerView: VGPlayerView) {
        if playerView.isFullScreen {
            playerView.exitFullscreen()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func rvgPlayerView(didDisplayControl playerView: VGPlayerView) {
        UIApplication.shared.setStatusBarHidden(!playerView.isDisplayControl, with: .fade)
     }
}
