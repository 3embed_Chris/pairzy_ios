//
//  imagePreviewCollectionViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 15/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class imagePreviewCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewForVideo: UIView!
    
    @IBOutlet weak var videoPlayButton: UIButton!
    @IBOutlet weak var imageViewOutlet: UIImageView!
}
