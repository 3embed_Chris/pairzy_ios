//
//  LetsSyncViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 24/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class LetsSyncViewController: UIViewController {
    
    struct Constants {
        static let letsSyncRequestSegue = "letsSyncRequestSegue"
        static let openLetsSyncPopUpSegue = "openLetsSyncPopUpSegue"
    }
    @IBOutlet weak var matchTextLabelOutlet: UILabel!
    @IBOutlet weak var syncPersonImageViewOutlet: UIImageView!
    @IBOutlet weak var yourPicImageViewOutlet: UIImageView!
    @IBOutlet weak var heartIconOutlet: HeartBeatingImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.heartIconOutlet.startHeartBeating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startConversationButtonAction(_ sender: Any)
    {
        
    }
    
    @IBAction func syncDateButtonAction(_ sender: Any)
    {
        self.performSegue(withIdentifier: Constants.openLetsSyncPopUpSegue, sender: nil)
    }
    
    @IBAction func viewMoreProfileButtonAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.openLetsSyncPopUpSegue {
            guard let controller = segue.destination as? LetsSyncAlertPopUpViewController else { return }
            
            controller.letsSyncAlertButtonPressedDelegate = self
        }
    }
}

extension LetsSyncViewController : LetsSyncAlertButtonPressedDelegate {
    
    func nowButtonPressed() {
        
    }
    
    func LaterButtonPressed() {
        self.performSegue(withIdentifier: Constants.letsSyncRequestSegue, sender: nil)
    }
}
