//
//  DatupPlusCollection.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 30/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit


struct DatumPlusObj {
    var featureTitle = ""
    var featureDescrption = ""
    var featureImage = ""
}

class DatupPlusCollection {
    
    
    static var screens: [DatumPlusObj] {
        
        var screen1 = DatumPlusObj()
        screen1.featureTitle = StringConstants.unlimitedLikes()
        screen1.featureDescrption = StringConstants.swipeRight()
        screen1.featureImage = "DPLikes"
        
        var screen2 = DatumPlusObj()
        screen2.featureTitle = StringConstants.oneFreeBoost()
        screen2.featureDescrption = StringConstants.skipTheQue()
        screen2.featureImage = "boost"
        
        var screen3 = DatumPlusObj()
        screen3.featureTitle = StringConstants.chooseWhoSee()
        screen3.featureDescrption = StringConstants.onlyBeShown()
        screen3.featureImage = "who_sees_you"
        
        var screen4 = DatumPlusObj()
        screen4.featureTitle = StringConstants.swipeArround()
        screen4.featureDescrption = StringConstants.passportToAny()
        screen4.featureImage = "swipe_around_the_world"
        
        var screen5 = DatumPlusObj()
        screen5.featureTitle = StringConstants.controlYourProf()
        screen5.featureDescrption = StringConstants.limitWhatOthersSee()
        screen5.featureImage = "control_your_profile"
        
        var screen6 = DatumPlusObj()
        screen6.featureTitle = StringConstants.fiveFreeSuperLike()
        screen6.featureDescrption = StringConstants.threeTimesMoreLikely()
        screen6.featureImage = "super_Likes"
        
        var screen7 = DatumPlusObj()
        screen7.featureTitle = StringConstants.unlimitedRewind()
        screen7.featureDescrption = StringConstants.goBackToSwipe()
        screen7.featureImage = "unlimited_rewinds"
        
        var screen8 = DatumPlusObj()
        screen8.featureTitle = StringConstants.turnOffAdvert()
        screen8.featureDescrption = StringConstants.haveFunSwiping()
        screen8.featureImage = "Turn_off_adverts"
        
        
        return [screen1,screen2,screen3,screen4,screen5,screen6,screen7,screen8]
        
    }
    
    
    static var features: [DatumPlusObj] {
        
        var featureText = StringConstants.unlockedUnlimitedLike()
        
        let feature1 = DatumPlusObj(featureTitle:featureText, featureDescrption:"", featureImage: "DPLikes")
        featureText = StringConstants.unlockedSuperLike()
        let feature2 = DatumPlusObj(featureTitle:featureText, featureDescrption:"", featureImage: "super_Likes")
        featureText = StringConstants.unlockedRewinds()
        let feature3 = DatumPlusObj(featureTitle:featureText, featureDescrption:"", featureImage: "unlimited_rewinds")
        
        featureText = StringConstants.unlockedPassport()
        let feature4 = DatumPlusObj(featureTitle:featureText, featureDescrption:"", featureImage: "swipe_around_the_world")
 
        return [feature1,feature2,feature3,feature4]
        
    }
}
