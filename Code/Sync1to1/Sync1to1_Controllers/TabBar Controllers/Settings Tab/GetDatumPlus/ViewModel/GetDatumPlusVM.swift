//
//  GetDatumPlusVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 01/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift


class GetDatumPlusVM: NSObject {
    
    enum respType:Int{
        case getPurchasePlan = 0
        case getCoinPlan = 1
        case subscription = 2
        case faild = 3
    }
    
    let disposeBag = DisposeBag()
    let getDatumPlusVM_resp = PublishSubject<respType>()
    var coinPlans = [CoinsPlanModel]()
    var purchasePlan = [PurchasePlanModel]()
    var subscribedObj = SubscriptionModel(data: [:])
    var message  = ""
    var view = UIView()
    let apiCalls = CoinsAPICalls()
    /// get Purchase plans
    
    
    func getPlans(){
    apiCalls.requestForGetPlans()
        
        apiCalls.subject_response
            .subscribe(onNext: {response in
                self.handleAPIResponse(response: response,type: .getPurchasePlan)
            }, onError: {error in
                self.getDatumPlusVM_resp.onError(error)
                Helper.hideProgressIndicator()
            })
            .disposed(by:disposeBag)
    }
    
    func getCoinConfig(){
        
        let apiCalls = CoinsAPICalls()
        apiCalls.getCoinConfig()
        
        apiCalls.subject_response
            .subscribe(onNext: {response in
            Helper.saveCoinConfig(data: response.response)
            }, onError: {error in
                
                self.getDatumPlusVM_resp.onError(error)
                Helper.hideProgressIndicator()
            })
            .disposed(by:disposeBag)
    }
    

    /// get coinPlans
    func getCoinPlans(){
        //
        apiCalls.requestForGetCoinPlans()
        
        apiCalls.subject_response
            .subscribe(onNext: {response in
                //  Helper.hideProgressIndicator()
                self.handleAPIResponse(response: response, type: .getCoinPlan)
            }, onError: {error in
                //  Helper.hideProgressIndicator()
                self.getDatumPlusVM_resp.onError(error)
                
            })
            .disposed(by:disposeBag)
    }
    
    
    
    
    /// subscribes selected plan
    ///
    /// - Parameter selectedPlan: selected plane is of type PurchasePlanModel
    func subscribePlan(){
        var params:[String:Any] = [:]
        
        
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        if let purchasedProDetails = UserDefaults.standard.object(forKey: "purchaseDetails") as? [String:Any] {
            let date:Date = Date()
            var planId = "5b0e879734874c0500481530"
            self.purchasePlan = Helper.getPlans()
            for eachPlan in purchasePlan {
                if eachPlan.actualId == purchasedProDetails["productId"] as! String {
                    planId = eachPlan.planId
                }
            }
            params = [ServiceInfo.PlanId: planId,
                          ServiceInfo.PaymentGatewayTxnId: purchasedProDetails["transactionId"] as! String,
                          ServiceInfo.PaymentGateway: "AppStore",
                          ServiceInfo.UserPurchaseTime: date.millisecondsSince1970] as [String : Any]
        }
        
      //  let apiCall = CoinsAPICalls()
        apiCalls.requestToSubscribe(data: params)
        apiCalls.subject_response
            .subscribe(onNext: {response in
                self.handleAPIResponse(response: response, type: .subscription)
                
            }, onError: {error in
                Helper.hideProgressIndicator()
                self.getDatumPlusVM_resp.onError(error)
                
            })
            .disposed(by:disposeBag)
    }
    
    
    /// method for handling matches api response.
    ///
    /// - Parameter response: contains api response and status code.
    func handleAPIResponse(response:ResponseModel,type:respType) {
         Helper.hideProgressIndicator()
        switch (response.statusCode) {
        case 200:
            
            switch type {
            case .getCoinPlan:
                var coinPlan = [CoinsPlanModel]()
                
                if let data = response.response[ApiResponseKeys.data] as? [Any] {
                    for each in data {
                        coinPlan.append(CoinsPlanModel.init(data:each as! [String : Any]))
                    }
                }
                self.coinPlans = coinPlan
                self.getDatumPlusVM_resp.onNext(type)
                break
            case .getPurchasePlan:
                var purchasPlan = [PurchasePlanModel]()
                if let data = response.response[ApiResponseKeys.data] as? [Any] {
                    Helper.savePlans(data: data)
                    for each in data {
                        purchasPlan.append(PurchasePlanModel.init(data:each as! [String : Any]))
                    }
                }
                self.purchasePlan = purchasPlan
                self.getDatumPlusVM_resp.onNext(type)
                break
            case .subscription:
                
                    Helper.hideProgressIndicator()
                    
                if let data = response.response[ApiResponseKeys.data] as? [String:Any] {
                    if let subData = data[ApiResponseKeys.Subscription] as? [String:Any] {
                        self.subscribedObj = SubscriptionModel.init(data: subData)
                    }
                    
                }
                Helper.saveIsUserPurchased(flag: true)
                message = response.response[ApiResponseKeys.message] as! String
                self.getDatumPlusVM_resp.onNext(.subscription)
                break
            case .faild:
                break
            }
        default:
            self.getDatumPlusVM_resp.onNext(.faild)
            Helper.showAlertWithMessageOnwindow(withTitle:"", message: response.response[ApiResponseKeys.message] as! String)
            Helper.hideProgressIndicator()
            break
        }
        
    }
    
}
