//
//  AmountCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 30/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class AmountCell: UITableViewCell {

    @IBOutlet weak var coins: UILabel!
  
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var amountView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func updateCell(data: CoinsPlanModel ,plansFormApps: InAppPurchaseModel){
        amountView.layer.borderWidth = 1.5
        amountView.layer.borderColor = Colors.AppBaseColor.cgColor
        
        if data.coins == 0 {
         coins.text = StringConstants.nullCoins()
            
        }else {
            coins.text = data.planName                          //String(data.coins)+" coins"
        }
        
        // amountLabel.text = currency+String(data.cost)  // from API
         amountLabel.text = plansFormApps.currencySymbol + " " + String(plansFormApps.price)   // from AppStore
        amountLabel.textColor = Colors.AppBaseColor
    }
    
    
    func updateCellForFreeCoins() {
        amountView.layer.borderWidth = 1.5
        amountView.layer.borderColor = Colors.AppBaseColor.cgColor
        coins.text = String(format: StringConstants.viewAdToReciveCoins(), 100)
        amountLabel.text = StringConstants.viewad()
    }
    
}
