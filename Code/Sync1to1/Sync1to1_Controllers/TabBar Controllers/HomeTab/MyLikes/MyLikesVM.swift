//
//  MyLikesVM.swift
//  Datum
//
//  Created by 3 Embed on 06/09/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MyLikesVM: NSObject {
    
    let disposeBag = DisposeBag()
    let myLikesVM_resp = PublishSubject<Bool>()
    
    var profilesArray = [Profile]()
    

        /// method for requesting profiles.
        func requestForProfiles(offset:Int , limit:Int) {
            
            let apiCall = ProfileAPICalls()
            apiCall.requestForGetLikesByBoost()
            
            apiCall.subject_response
                .subscribe(onNext: {response in
                    
                    Helper.hideProgressIndicator()
                     self.handleAPIResponse(response: response)
                  
                }, onError: {error in
                    Helper.hideProgressIndicator()
                    
                })
                .disposed(by:disposeBag)
        }

    
    
    
    func handleAPIResponse(response: ResponseModel){

        switch response.statusCode {
        case 200:
            break
        default:
            break
        }
        
        var profileModelData = [Profile]()
        if let profileResponse = response.response["data"] as? [AnyObject] {
            for (index, _) in profileResponse.enumerated() {
                let profileDetails = profileResponse[index] as! [String:Any]
                profileModelData.append(Profile.fetchprofileDataObj(userDataDictionary:profileDetails))
            }
        }
        self.profilesArray = profileModelData
     self.myLikesVM_resp.onNext(true)
    }
    
    
}
