//
//  MyLikesVC.swift
//  Datum
//
//  Created by 3 Embed on 06/09/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class MyLikesVC: UIViewController {
    
    @IBOutlet weak var profilesCollectionView: UICollectionView!
    @IBOutlet weak var boostBtn: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var noContentView: UIView!
    @IBOutlet weak var emptyScreenTitle: UILabel!
    @IBOutlet weak var emptySubTitle: UILabel!
    @IBOutlet weak var defaultIcon: UIImageView!

    let myLikesVM = MyLikesVM()
    let profileVm = ProfileViewModel()
    
    var indicator:UIActivityIndicatorView!
    var refreshControl: UIRefreshControl!
    
    var popUpFor:CoinsPopupFor = .forAll
    var coinConfig = Helper.getCoinConfig()

    override func viewDidLoad() {
        super.viewDidLoad()
        didGetAPIResponse()
        initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func boostBtnAction(_ sender: Any) {
        
        
        self.popUpFor = .forBoost
        if(Helper.isUserBoostActive()) {
            openTimerPopUp()
            return
        }
        
        if Helper.isUserHasMinimumCoins(minValue: coinsFor.boost) {
            
            let buttoTitle = String(format:StringConstants.iamOkToSpend(),coinConfig.coinsForBoost)
            let title =  String(format:StringConstants.youNeedToSpend(),coinConfig.coinsForBoost)
            showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coins"), mainTitle: title, subTitle: "", buttnTitle: buttoTitle, dontShowHide: true)
            
            
        }  else {
            let title = StringConstants.oopsYouDontHave()
            let subTitle = ""
            
            showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coinEmpty"), mainTitle: title, subTitle: subTitle, buttnTitle: StringConstants.BuyCoins(), dontShowHide: true)
        }
        
        
    }
    
    func openTimerPopUp() {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = TimerPopUpView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.loadTimerPopupView()
        window.addSubview(videoView)
    }
    
    @IBAction func disLikeBtnAction(_ sender: Any) {
        let btn = sender as! UIButton
        profileVm.disLikeProfile(selectedProfile: self.myLikesVM.profilesArray[btn.tag])
        removeProfileFromGrid(removeIndex: btn.tag)
        
    }
    
    
    @IBAction func likeBtnAction(_ sender: Any) {
        let btn = sender as! UIButton
        profileVm.likeProfile(selectedProfile: self.myLikesVM.profilesArray[btn.tag])
        removeProfileFromGrid(removeIndex: btn.tag)
        
    }
    
    @IBAction func superLikeBtnAction(_ sender: Any) {
        let btn = sender as! UIButton
        
         superLikeButtonTapped(selectedButton:btn)
       
        
    }
    
    
    func didGetAPIResponse(){
        self.myLikesVM.myLikesVM_resp.subscribe(onNext: { success in
            self.profilesCollectionView.reloadData()
            
        })
    }
    
    func initialSetup(){
        // Do any additional setup after loading the view.
        myLikesVM.requestForProfiles(offset:0 , limit: 100)
        
        defaultIcon.image = #imageLiteral(resourceName: "likes_me")
        emptyScreenTitle.text = StringConstants.likesMe()
        emptySubTitle.text = StringConstants.likesMeMsg()
        titleLabel.text = StringConstants.likes()
        titleLabel.textColor = Colors.AppBaseColor
        
        //indicator
        
        indicator = UIActivityIndicatorView()
        indicator = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
      
        indicator.color = Colors.SeparatorDark
        indicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        indicator.center = profilesCollectionView.center
        indicator.startAnimating()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ProspectCollectionCell.refresh), for: UIControlEvents.valueChanged)
        profilesCollectionView.refreshControl = self.refreshControl
        refreshControl.tintColor = Colors.AppBaseColor
        profilesCollectionView.backgroundColor = Colors.ScreenBackground
        
    }
    
    @objc func refresh() {
        self.indicator.stopAnimating()
        self.indicator.removeFromSuperview()
        myLikesVM.requestForProfiles(offset:0 , limit: 100)
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            self.refreshControl.endRefreshing()
        }
    }
    
    
    func removeProfileFromGrid(removeIndex:Int) {
        self.myLikesVM.profilesArray.remove(at: removeIndex)
        let indexpa = IndexPath(row: removeIndex, section: 0)
        self.profilesCollectionView.performBatchUpdates({
            self.profilesCollectionView.deleteItems(at: [indexpa])
        }) { (finished) in
            self.profilesCollectionView.reloadItems(at: self.profilesCollectionView.indexPathsForVisibleItems)
        }
    }

    
    func showCoinsPopupForChat(icon: UIImage, mainTitle: String, subTitle: String, buttnTitle: String, dontShowHide: Bool){
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let coinsView = spendSomeCoinPopUp(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        coinsView.loadPopUpView()
        coinsView.coinsButton.titleLabel?.font = UIFont(name: CircularAir.Bold , size: 16)
        coinsView.titleForCoinsSpend.font = UIFont(name: CircularAir.Bold , size: CGFloat(fontSize.coinsPopUpTitle))
        coinsView.updatePopup(icon: icon, mainTitle: mainTitle, subTitle: subTitle, buttnTitle: buttnTitle, dontShowHide: dontShowHide)
        coinsView.spendSomeCoinDelegate = self
        window.addSubview(coinsView)
    }
    
    
    func superLikeButtonTapped(selectedButton:UIButton) {
        self.popUpFor = .forSuperLike
        if Helper.isUserHasMinimumCoins(minValue: coinsFor.SuperLike) {
            if Helper.isShowCoinsDeductPopup(popUpFor: .forSuperLike) {
                coinConfig = Helper.getCoinConfig()
                let prifile = self.myLikesVM.profilesArray[selectedButton.tag]
                var temp = ""
                if prifile.gender == StringConstants.male() {
                    temp = StringConstants.him()
                }else{
                    temp = StringConstants.her()
                } //spendCoinsToLet
                let mainTitle = String(format:StringConstants.spendCoinsToLet(),coinConfig.coinsForSuperLike,prifile.firstName,temp)
                    
                //"Spend \(coinConfig.coinsForSuperLike) coins to let " + prifile.firstName + " know you really like " + temp + " by super liking"
                let buttoTitle = String(format:StringConstants.iamOkToSpend(),coinConfig.coinsForSuperLike)
                //"I am OK to spend \(coinConfig.coinsForSuperLike) coins"
                
                showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coins"), mainTitle: mainTitle, subTitle: "", buttnTitle: buttoTitle, dontShowHide: true)
            }
            else {
                self.coinBtnPressed(sender: selectedButton)
            }
        } else {
            showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coinEmpty"), mainTitle: StringConstants.CoinsGotOver(), subTitle: StringConstants.NoEnoughCoins() + StringConstants.superLike(), buttnTitle: StringConstants.BuyCoins(), dontShowHide: true)
        }
        
    }
    
    
    
}
// MARK: - UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension MyLikesVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.myLikesVM.profilesArray.count == 0 {
            self.indicator.startAnimating()
            profilesCollectionView.backgroundColor = UIColor.clear
        }
        else {
            profilesCollectionView.backgroundColor = Colors.ScreenBackground
            self.indicator.stopAnimating()
            self.indicator.removeFromSuperview()
        }
        return self.myLikesVM.profilesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let homeCell:HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "myLikes", for: indexPath) as! HomeCollectionCell
        
        let profile = self.myLikesVM.profilesArray[indexPath.row]
        
        if profile.gender == StringConstants.male() {
            homeCell.imageName = "man_default1"
        } else {
            homeCell.imageName = "woman_default1"
        }
        
        homeCell.updateMyLikes(profile: profile)
        homeCell.updateTagForButtons(tag:indexPath.item)
        return homeCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size = CGSize.zero
        let cellWidth = (UIScreen.main.bounds.width - 30)/2
        let cellHight = (collectionView.frame.size.height - 40)/2
        size.height =  cellHight
        size.width  =  cellWidth
        return size
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //performSegue(withIdentifier: String(describing:ProfileDetailVC.self), sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
}

extension MyLikesVC:spendSomeCoinPopUpDelegate {
    
    
    /// popUp delegate method
    ///
    /// - Parameter sender: UIbutton
    func coinBtnPressed(sender:UIButton) {
        let title = sender.title(for: .normal)
        
        if title == StringConstants.BuyCoins() {
           // gotoCoinBalanceScreen()
         //   return
        }
        
        switch self.popUpFor {
            
        case .forSuperLike:
            self.profileVm.superLikeProfile(selectedProfile: self.myLikesVM.profilesArray[sender.tag])
            self.removeProfileFromGrid(removeIndex: sender.tag)
            break
        case .forDate:
            
            break
        case .forChat:
        
            break
        case .forBoost:
            self.profileVm.boostProfile()
            break
        default:
            break
        }
}
}
