//
//  HomeCollectionCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 05/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import VGPlayer

class HomeCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var cellBackView: UIView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var videoBtn: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var viewForVideo: UIView!
    @IBOutlet weak var onlineStatusIcon: UIImageView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var boostButton: UIButton!
    @IBOutlet weak var superLikeBtn: UIButton!
    
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var superLikeIcon: UIImageView!
    @IBOutlet weak var superLikeWidth: NSLayoutConstraint!
    var localplayer : VGPlayer?
    
    var imageName = "man_default1"
  
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellBackView.layer.cornerRadius = 3
        Helper.setShadow(sender: cellBackView)
     
    }
    
    func updateTagForButtons(tag:Int) {
        self.likeBtn.tag = tag
        self.closeBtn.tag = tag
        self.superLikeBtn.tag = tag
       // self.boostButton.tag = tag
        self.chatButton.tag = tag
    }
    
    
    func updateCell(profile: Profile){
        
        nameLabel.text = profile.firstName
        
        if(profile.isHiddenUserAge == 1) {
            self.nameLabel.text = profile.firstName
            
        } else {
            if profile.userAge.count != 0 {
                self.nameLabel.text = profile.firstName
                self.nameLabel.text = profile.firstName.appending(", ").appending(profile.userAge)
            }
        }
        
        viewForVideo.isHidden = true
        profilePic.isHidden = true
        if profile.hasProfileVideo == 1 {
            updateProfileVideo(data:  profile.profileVideo,thumbnail:profile.profileVideoThumbnail)
            viewForVideo.isHidden = true
            profilePic.isHidden = false
        } else {
            let url = URL(string: profile.profilePicture)
            profilePic.kf.setImage(with:url, placeholder:UIImage.init(named: imageName))
            videoBtn.isHidden = true
            viewForVideo.isHidden = true
            profilePic.isHidden = false
            localplayer?.pause()
        }
        
        if profile.onlineStatus == 1 {
            self.onlineStatusIcon.image = #imageLiteral(resourceName: "Ellipse 95")
            onlineStatusIcon.isHidden = false
        }else {
            onlineStatusIcon.isHidden = true
            self.onlineStatusIcon.image = #imageLiteral(resourceName: "Ellipse 96")
        }
        
        if profile.superLiked == 0 {
            superLikeIcon.isHidden = true
            superLikeWidth.constant = 0
            
        }else {
            superLikeIcon.isHidden = false
            superLikeWidth.constant = 20
        }
        superLikeIcon.startHeartBeat()
    }
    
    func showVideoForUrl(videoUrl:String) {
        if(videoUrl.count > 5) {
            
            let url = URL(string:videoUrl)
            
            if(localplayer != nil) {
                if(localplayer?.contentURL?.absoluteString==videoUrl) {
                    //localplayer?.reloadPlayer()
                    localplayer?.play()
                    localplayer?.player?.isMuted = true
                    return
                } else {
                    localplayer?.replaceVideo(url!)
                    localplayer?.play()
                    localplayer?.player?.isMuted = true
                    return
                }
            }
            
            if url != nil {
                localplayer = VGPlayer(URL: url!)
            }
            
            localplayer?.delegate = self
            viewForVideo.addSubview((localplayer?.displayView)!)
            localplayer?.displayView.bringSubview(toFront:(localplayer?.displayView.playButtion)!)
            viewForVideo.bringSubview(toFront: videoBtn)
            localplayer?.backgroundMode = .suspend
            localplayer?.gravityMode = .resizeAspectFill
            localplayer?.displayView.backgroundColor = UIColor.black
            localplayer?.displayView.delegate = self
            localplayer?.displayView.snp.makeConstraints { [weak viewForVideo] (make) in
                guard let strongSelf = viewForVideo else { return }
                make.edges.equalTo(strongSelf)
            }
            localplayer?.player?.isMuted = true
            localplayer?.play()
        }
    }
    
    func updateProfileVideo(data: String,thumbnail:String){
        
        
        var newUrl = URL(string: thumbnail)
        
        
        
        let modifiedLink = data.replace(target:"video/upload/", withString:"video/upload/q_20,w_200,h_200/")
        
        let jpgURL = URL(string:modifiedLink)?
            .deletingPathExtension()
            .appendingPathExtension("gif")
        newUrl = jpgURL
        
        profilePic.isHidden = false
        profilePic.kf.setImage(with:newUrl, placeholder:UIImage.init(named: imageName))
    
        videoBtn.isHidden = false
    }
    
    func updateMyLikes(profile: Profile){
        nameLabel.text = profile.firstName
        
        if(profile.isHiddenUserAge == 1) {
            self.nameLabel.text = profile.firstName
            
        } else {
            if profile.userAge.count != 0 {
                self.nameLabel.text = profile.firstName
                self.nameLabel.text = profile.firstName.appending(", ").appending(profile.userAge)
            }
        }
        
        viewForVideo.isHidden = true
        profilePic.isHidden = true
        if profile.hasProfileVideo == 1 {
            updateProfileVideo(data:  profile.profileVideo,thumbnail:profile.profileVideoThumbnail)
            viewForVideo.isHidden = true
            profilePic.isHidden = false
         
        } else {
            let url = URL(string: profile.profilePicture)
            profilePic.kf.setImage(with:url, placeholder:UIImage.init(named: imageName))
          
            viewForVideo.isHidden = true
            profilePic.isHidden = false
            localplayer?.pause()
        }
        
        if profile.onlineStatus == 1 {
            self.onlineStatusIcon.image = #imageLiteral(resourceName: "Ellipse 95")
            onlineStatusIcon.isHidden = false
        } else {
            onlineStatusIcon.isHidden = true
            self.onlineStatusIcon.image = #imageLiteral(resourceName: "Ellipse 96")
        }
    }
    
    
}

extension HomeCollectionCell: VGPlayerDelegate {
    func vgPlayer(_ player: VGPlayer, playerFailed error: VGPlayerError) {
        print(error)
    }
    
    func vgPlayer(_ player: VGPlayer, stateDidChange state: VGPlayerState) {
        print("player State ",state)
        if (player.state == .playFinished) {
            //profilePic.isHidden = true
            localplayer?.seekTime(0)
            localplayer?.play()
            localplayer?.player?.isMuted = true
        } else {
            //profilePic.isHidden = false
        }
    }
    
    func vgPlayer(_ player: VGPlayer, bufferStateDidChange state: VGPlayerBufferstate) {
        print("buffer State", state)
    }
    
}

extension HomeCollectionCell: VGPlayerViewDelegate {
    func vgPlayerView(_ playerView: VGPlayerView, willFullscreen fullscreen: Bool) {
        print(fullscreen)
    }
    
    func vgPlayerView(didTappedClose playerView: VGPlayerView) {
        if playerView.isFullScreen {
            playerView.exitFullscreen()
        } else {
            //self.navigationController?.popViewController(animated: true)
        }
    }
    
    func vgPlayerView(didDisplayControl playerView: VGPlayerView) {

    }
}
