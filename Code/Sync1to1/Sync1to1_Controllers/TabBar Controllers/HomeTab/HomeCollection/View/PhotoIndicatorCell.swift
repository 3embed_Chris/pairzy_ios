//
//  PhotoIndicatorCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 10/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class PhotoIndicatorCell: UICollectionViewCell {
    
 @IBOutlet weak var indicatorView: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        indicatorView.layer.cornerRadius = 3
    }
    
    func updateCell(view: UIView){
        
        if indicatorView.tag == view.tag {
          //  view?.backgroundColor = UIColor(white: 1, alpha: 0.5)
            indicatorView.backgroundColor = UIColor.white
        }else {
            indicatorView.backgroundColor = UIColor.brown
        }
    }
}
