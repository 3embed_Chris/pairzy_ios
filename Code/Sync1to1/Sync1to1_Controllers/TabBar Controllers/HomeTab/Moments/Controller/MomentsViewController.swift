//
//  MomentsViewController.swift
//  Datum
//
//  Created by Rahul Sharma on 07/06/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

class MomentsViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var momentCollectionView: UICollectionView!
    
    var momentMedia = [MomentsModel]()
    var index:Int?
    var profileData: Profile?
    var isFromMyProfile = true
    
    // MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    
    /// Popping back to previous Controller
    ///
    /// - Parameter sender: Back Buton
    @IBAction func backButtonAction(_ sender: UIBarButtonItem) {
        if isFromMyProfile{
            self.navigationController?.popViewController(animated: true)
        }else{
             self.dismiss(animated: false, completion: nil)
        }
        
        
    }
    
    
    // MARK: - UICollectionViewDelegate,UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return momentMedia.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "MomentsDetailCVCell", for: indexPath) as! MomentsDetailCVCell
        
        cell.updateMomentsMedia(mediaUrl: (momentMedia[indexPath.item].url[0]))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let momentDetailVC = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.MomentsDetailViewController) as! MomentsDetailViewController
        momentDetailVC.selectedIndex = indexPath.item
        momentDetailVC.selectedUserMoments = momentMedia
        
        
//        let navigationVC: UINavigationController =  UINavigationController(rootViewController: momentDetailVC)
//        navigationVC.isNavigationBarHidden = false
        
        if isFromMyProfile{
            momentDetailVC.isFromMyProfile = true
            self.navigationController?.pushViewController(momentDetailVC, animated: true)
        }else{
            momentDetailVC.isFromMyProfile = false
            self.present(momentDetailVC, animated: false, completion: nil)
        }
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    let edgeGaps:UIEdgeInsets = UIEdgeInsets.init(top: 5, left: 10, bottom: 5, right: 10)
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = ((self.momentCollectionView.bounds.width - 15) - edgeGaps.left - edgeGaps.right) / 3
        let cellHeight = cellWidth
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    
    
    
}




