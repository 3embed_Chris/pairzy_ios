//
//  MomentsDetailViewController.swift
//  Datum
//
//  Created by Rahul Sharma on 11/06/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import VGPlayer
import RxSwift

class MomentsDetailViewController: UIViewController {

    //MARK:- Variables and Declarations
    var selectedIndex:Int?
    let createPostVM = CreatePostViewModel() //
    
    var disposeBag = DisposeBag()
    var selectedUserMoments = [MomentsModel]()
    var selectedProfile:Profile?
    var currentIndex: Int = -1
    var localIndex: Int = 0
    var visibleIP: IndexPath?
    var aboutToBecomeInvisibleCell: Int = -1
    var playingIndex: Int = -1
   // var player : VGPlayer?
    var player:IJKFFMoviePlayerController!
    var isFromMyProfile = false
    private var scrollFirstTime = true
    var currentCell: MatchUserPostTVCell?

    let userData = Database.shared.fetchResults()
    
    weak var playingCell:PostImageCVCell?
 

// MARK: - Outlets
    @IBOutlet weak var matchesPostTableView: UITableView!
    @IBOutlet weak var profileButtonOutlet: UIButton!
    
    /// Back To previous Controller
    ///
    /// - Parameter sender: backButton
    @IBAction func backButtonAction(_ sender: UIBarButtonItem) {
        if isFromMyProfile{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: false, completion: nil)
        }
        
    }
    
    
    
    
    /// On Clicking OF profile Image Going to the ProfileDetailVc
    ///
    /// - Parameter sender: Profile Image
    @IBAction func profileButtonAction(_ sender: UIButton) {
        let btn = sender
        var tempProfile = Profile.fetchprofileDataObj(userDataDictionary:[:])
        tempProfile.userId = self.selectedUserMoments[btn.tag].userId
        self.createPostVM.selectedIndex = btn.tag
        let profileVc = Helper.getSBWithName(name:"DatumTabBarControllers").instantiateViewController(withIdentifier:StoryBoardIdentifier.profileVc) as! ProfileDetailVC
        profileVc.profileDeatilVM.dataAvailable = false
        profileVc.profileDeatilVM.showProfileByID = false
        profileVc.profileFromChat = true
        profileVc.profileDeatilVM.profileDetails.userId = tempProfile.userId
        self.present(profileVc, animated: true, completion:nil)
        
    }
    
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.navigationBar.isHidden = true
        self.matchesPostTableView.reloadData()
        didGetResponse()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        if playingCell?.localPlayer != nil {
            playingCell!.localPlayer!.pause()
            playingCell!.localPlayer!.shutdown()
            playingCell!.localPlayer!.view.removeFromSuperview()
            playingCell!.localPlayer = nil
            playingCell!.playingIndex = -1
        }
    }
    
    
    /// Comment Button- Showing All Comments on Post
    ///Like Button -  Showing the List Of Likers
    /// - Parameters:
    ///   - segue: MomentDetailsToCommentVC,MomentDetailstoLikersVC
    ///   - sender: Comment Button,Like Button
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == SegueIds.MomentDetailsToCommentVC{
//            let commentVC = segue.destination as! CommentsViewController
//            let data = self.selectedUserMoments[selectedIndex!]
//            self.currentIndex =  selectedIndex!
//            commentVC.postId = data.postId
//            commentVC.userId = data.userId
//            commentVC.delegate = self
//        }
//        if segue.identifier == SegueIds.MomentDetailstoLikersVC{
//            let likersVc = segue.destination as! LikersViewController
//            let data = self.selectedUserMoments[selectedIndex!]
//            likersVc.postId = data.postId
//        }
//
//    }
    
    
    /// Deleting the Particular users Post
    ///
    /// - Parameter sender: Delete Button
    @IBAction func deletePostAction(_ sender: UIButton) {
        let btn = sender
        
        self.createPostVM.postId = self.selectedUserMoments[btn.tag].postId
        self.localIndex = btn.tag
        let alert:UIAlertController=UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let deletePost = UIAlertAction(title: StringConstants.DeletePost(), style: UIAlertActionStyle.default, handler: {(Bool) in
            self.showConfirmDeleteAlert()
        })
        let cancelAction = UIAlertAction(title: StringConstants.cancel(), style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        deletePost.setValue(UIColor.red, forKey: "titleTextColor")
        alert.addAction(deletePost)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    /// Show PopUp Before Deleting the Post
    func showConfirmDeleteAlert() {
        
        let  msgForAlert = StringConstants.confirmDelete()
        let buttonText = StringConstants.delete()
        
        let alert = UIAlertController(title: nil, message: msgForAlert, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: StringConstants.actionSheetCancelText(), style: .cancel, handler: {(_) in }))
        alert.addAction(UIAlertAction(title: buttonText, style: .default, handler: {(action:UIAlertAction!) in
            //API Call for DELETE Post
            self.createPostVM.deleteUserPost()
            self.selectedUserMoments.remove(at: self.localIndex)
            self.matchesPostTableView.reloadData()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    /// Reloading the Table View After Deleting Post
    func didGetResponse(){
        if !createPostVM.createPostVM_resp.hasObservers {
            createPostVM.createPostVM_resp.subscribe(onNext: { success in
                switch success.0 {
                case CreatePostViewModel.responseType.deleteUserPost:
                    if success.1 {
                        //Reload TableView After Getting response
                        
                        self.matchesPostTableView.reloadData()
                    }
                default:
                    break
                }
            }).disposed(by: disposeBag)
        }
    }
    
    
    /// To prepare and play video when cell is visiable
    ///
    /// - Parameters:
    ///   - cell: visible cell
    ///   - indexPath: index path of visiable cell
    func playVideoOnTheCell(cell : MatchUserPostTVCell, indexPath : IndexPath){
        
        if currentIndex == indexPath.row{
            return
        }
        currentIndex = indexPath.row
        let data = self.selectedUserMoments[indexPath.row]
        let collectionView = cell.postImageCollectionView
        guard let collectionViewCell = collectionView?.cellForItem(at: IndexPath(row: 0, section: 0)) as? PostImageCVCell else{return}
        if data.url[0].suffix(3) == "jpg" || data.url[0].suffix(3) == "png" || data.url[0].suffix(3) == "jpeg" || data.url[0].contains("facebook") {
            if collectionViewCell.localPlayer != nil{
                collectionViewCell.localPlayer.shutdown()
                collectionViewCell.localPlayer.view.removeFromSuperview()
                collectionViewCell.localPlayer = nil
            }
        }else{
            self.playingCell = collectionViewCell
            //collectionViewCell.showVideoForUrl(videoUrl: data.url[0], videoView: collectionViewCell.viewForVideo)
            let videoUrl = URL(string: data.url[0])
            collectionViewCell.play(videoUrl, cell: cell)
            //self.player.prepareToPlay()
            self.currentCell = cell
            collectionViewCell.soundVideoImage.image = #imageLiteral(resourceName: "Sound Off")
            collectionViewCell.localPlayer?.playbackVolume = 0.0
        }
        
    }
    
    
    /// To perform any operation when cell start moving from full frame
    ///
    /// - Parameters:
    ///   - cell: cell which is moving
    ///   - indexPath: index of that cell
    func stopPlayBack(cell : MatchUserPostTVCell, indexPath : IndexPath){
        let collectionView = cell.postImageCollectionView
        guard let collectionViewCell = collectionView?.cellForItem(at: IndexPath(row: 0, section: 0)) as? PostImageCVCell else{return}
        if self.playingIndex == indexPath.row{
            collectionViewCell.localPlayer!.pause()
            collectionViewCell.localPlayer!.shutdown()
            collectionViewCell.localPlayer!.view.removeFromSuperview()
            collectionViewCell.localPlayer = nil
            collectionViewCell.playingIndex = -1
            self.playingIndex = -1
        }
    }
    
}

// MARK: - UITableViewDelegate,UITableViewDataSource

extension MomentsDetailViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  
        return self.selectedUserMoments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MatchUserPostTVCell.self)) as? MatchUserPostTVCell else{fatalError()}
        let data = self.selectedUserMoments[indexPath.item]
        cell.momentPostModel = data
        cell.chatButtonOutlet.isHidden = true
        cell.deleteOptionBtnOutlet.tag = indexPath.row
        if self.isFromMyProfile{
            cell.deleteOptionBtnOutlet.isHidden =  false
        }else{
            cell.deleteOptionBtnOutlet.isHidden =  true
        }
        cell.profileButtonOutlet.tag = indexPath.row
        cell.updateUIForPageControl()
        cell.setMatchUserPostData(userMomentModel: data)
        cell.index = indexPath.row
        cell.delegate = self
        return cell
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if (self.selectedUserMoments.count > 0){
            tableView.estimatedRowHeight = 44.0 // standard tableViewCell height
            tableView.rowHeight = UITableViewAutomaticDimension
            return UITableViewAutomaticDimension
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if scrollFirstTime{
            scrollFirstTime = false
            DispatchQueue.main.async {
                let index = IndexPath(row: self.selectedIndex!, section: 0)
                self.matchesPostTableView.scrollToRow(at: index, at: .bottom, animated: false)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.item
    }
    
   
}

// MARK: - Conforming MatchUserPostTVCellDelegate
extension MomentsDetailViewController: MatchUserPostTVCellDelegate{
    
    func getLikersOnTap(index: Int) {
        guard let likersVc = self.storyboard?.instantiateViewController(withIdentifier: "LikersViewController") as? LikersViewController else{return}
      //  let data = self.createPostVM.momentDetailsArray[index]
        let data = self.selectedUserMoments[index]
        likersVc.postId = data.postId
        self.navigationController?.pushViewController(likersVc, animated: true)
    }
     func likeButtonTap(index: Int, isSelected: Bool, momentModel: MomentsModel) {
        self.createPostVM.likeAndUnlikePostService(index: index, isSelected: isSelected, momentModel: momentModel)
    }
    
    func commentButtonTap(index: Int) {
        guard let commentVC = self.storyboard?.instantiateViewController(withIdentifier: "CommentsViewController") as? CommentsViewController else{return}
        let data = self.selectedUserMoments[index]
      //  let data = self.createPostVM.momentDetailsArray[index]
        self.currentIndex =  index
        commentVC.postId = data.postId
        commentVC.userId = data.userId
        commentVC.delegate = self
        self.navigationController?.pushViewController(commentVC, animated: true)
    }
    
}

//// MARK: - Conforming MatchUserPostTVCellDelegate Methods
//extension MomentsDetailViewController: MatchUserPostTVCellDelegate{
//
//    func getLikersOnTap(index: Int) {
//        self.performSegue(withIdentifier: SegueIds.MomentDetailstoLikersVC, sender: self)
//    }
//
//
//    func likeButtonTap(index: Int, isSelected: Bool, momentModel: MomentsModel) {
//        self.createPostVM.likeAndUnlikePostService(index: index, isSelected: isSelected, momentModel: momentModel)
//    }
//
//
//    func commentButtonTap(index: Int) {
//        self.performSegue(withIdentifier: SegueIds.MomentDetailsToCommentVC, sender: self)
//    }
//
//}



    // MARK: - UIScrollViewDelegate
  extension MomentsDetailViewController :UIScrollViewDelegate {
    
     func scrollViewDidScroll(_ scrollView: UIScrollView) {
            if scrollView == self.matchesPostTableView{
                
                //Stop scrolling in table view reachs it's top
                //            if scrollView.contentOffset.y <= 0 {
                //                scrollView.contentOffset = CGPoint.zero
                //            }
                
                guard let indexPaths = self.matchesPostTableView.indexPathsForVisibleRows else {return}
                var cells = [MatchUserPostTVCell]()
                
                for ip in indexPaths{
                    if let videoCell = self.matchesPostTableView.cellForRow(at: ip) as? MatchUserPostTVCell{
                        cells.append(videoCell)
                    }
                }
                let cellCount = cells.count
                if cellCount == 0 {return}
                if cellCount == 1{
                    if visibleIP != indexPaths[0]{
                        visibleIP = indexPaths[0]
                    }
                    if let videoCell = cells.last{
                        DispatchQueue.main.async {
                                self.playVideoOnTheCell(cell: videoCell, indexPath: indexPaths.last!)                        }
                    }
                }
                for i in 0..<cellCount{
                    let cellRect = self.matchesPostTableView.rectForRow(at:indexPaths[i])
                    let intersect = cellRect.intersection(self.matchesPostTableView.bounds)
                    //                curerntHeight is the height of the cell that
                    //                is visible
                    let currentHeight = intersect.height
                    let cellHeight = cells[i].frame.size.height
                    //                0.95 here denotes how much you want the cell to display
                    //                for it to mark itself as visible,
                    //                .95 denotes 95 percent,
                    //                you can change the values accordingly
                    if currentHeight > (cellHeight * 0.95){
                        if visibleIP != indexPaths[i]{
                            visibleIP = indexPaths[i]
                            DispatchQueue.main.async {
                                self.playVideoOnTheCell(cell: cells[i], indexPath: indexPaths[i])
                            }
                       }
                    }
                    else{
                        if aboutToBecomeInvisibleCell != indexPaths[i].row{
                            aboutToBecomeInvisibleCell = (indexPaths[i].row)
                            self.stopPlayBack(cell: cells[i], indexPath: indexPaths[i])
                        }
                    }
                }
            }
        }
    }




// MARK: - Confirming CommentsViewControllerDelegate Methods for getting Comment Count And Increasing it Locally After calling API
extension MomentsDetailViewController: CommentsViewControllerDelegate{
    
    func commentCount(count: Int) {
        let data = selectedUserMoments[currentIndex]
        data.commentCount = data.commentCount + count
        selectedUserMoments[currentIndex] = data
        let indexPath = IndexPath(row: currentIndex, section: 0)
        self.matchesPostTableView.reloadRows(at: [indexPath], with: .automatic)
        
    }
}
