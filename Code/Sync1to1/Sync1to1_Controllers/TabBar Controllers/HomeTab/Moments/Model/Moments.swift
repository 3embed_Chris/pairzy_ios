//
//  Moments.swift
//  Datum
//
//  Created by Rahul Sharma on 11/06/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

class MomentsModel: NSObject {
    var liked = false
    var type = ""
    var typeFlag = 2
    var postedOn = 0.00
    var date = ""
    var momentDescription = ""
    var status = 0
    var profilePic = ""
    var url = [String]()
    var likeCount = 0
    var commentCount = 0
    var postId = ""
    var userId = ""
    var userName = ""
    var distance = 0.00
    var likersName = ""
    var isPairSuccess = 0
    
    init(momentsData: [String : Any]){
        if let liked = momentsData["liked"] as? Bool{
            self.liked = liked
        }
        if let type = momentsData["type"] as? String{
            self.type = type
        }
        if let typeFlag = momentsData["typeFlag"] as? Int{
            self.typeFlag = typeFlag
        }
        if let postedOn = momentsData["postedOn"] as? Double{
            self.postedOn = postedOn
        }
        if let description = momentsData["description"] as? String{
            self.momentDescription = description
        }
        if let status = momentsData["status"] as? Int{
            self.status = status
        }
        if let profilePic = momentsData["profilePic"] as? String{
            self.profilePic = profilePic
        }
        if let likeCount = momentsData["likeCount"] as? Int{
            self.likeCount = likeCount
        }
        
        if let postId = momentsData["postId"] as? String{
            self.postId = postId
        }
        if let commentCount = momentsData["commentCount"] as? Int{
            self.commentCount = commentCount
        }
        if let userId = momentsData["userId"] as? String{
            self.userId = userId
        }
        if let userName = momentsData["userName"] as? String{
            self.userName = userName
        }
        if let likersName = momentsData["likersName"] as? String{
            self.likersName = likersName
        }
        if let distance = momentsData["distance"] as? Double{
            self.distance = distance
        }
        if let url:[String] = momentsData["url"] as? [String] {
            self.url = url
        }
        if let isPairSuccess = momentsData["isPairSuccess"] as? Int{
            self.isPairSuccess = isPairSuccess
        }
        

    }
    
    
    
}













