//
//  MomentsDetailCVCell.swift
//  Datum
//
//  Created by Rahul Sharma on 07/06/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import VGPlayer

class MomentsDetailCVCell: UICollectionViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var momentMediaView: UIImageView!
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var viewForVideo: UIView!
    
    //Local variables and declarations
    var localplayer : VGPlayer?
    var profileModel:Profile?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        momentMediaView.layer.cornerRadius = 4
        momentMediaView.layer.borderWidth = 1
        momentMediaView.clipsToBounds = true
        momentMediaView.layer.borderColor = Colors.BorderColor.cgColor
        viewForVideo.layer.cornerRadius = 4
        viewForVideo.layer.borderWidth = 1
        videoImageView.clipsToBounds = true
        viewForVideo.layer.borderColor = Colors.BorderColor.cgColor
    }
    
    
    /// Updates Moments Media
    ///
    /// - Parameter imageUrl/VideoUrl: image/Video url is of string
    func updateMomentsMedia(mediaUrl: String){
        
        if mediaUrl.suffix(3) == "jpg" || mediaUrl.suffix(3) == "png" || mediaUrl.suffix(3) == "jpeg" || mediaUrl.contains("facebook") {
            let newUrl = URL(string: mediaUrl)
            videoImageView.isHidden = true
            viewForVideo.isHidden = true
            momentMediaView.isHidden = false
            momentMediaView.kf.setImage(with:newUrl, placeholder:UIImage())
            
        }else{
            // let newUrl = URL(string: mediaUrl)
            self.videoImageView.isHidden = false
            viewForVideo.isHidden = true
            momentMediaView.isHidden = false
            //viewForVideo.kf.setImage(with:newUrl, placeholder:UIImage())
            let thumbNailUrl = mediaUrl.replace(target:".mp4", withString:".jpg")
            updateProfileVideo(data: mediaUrl, thumbnail:thumbNailUrl )
        }
        
    }
    
    
    func showVideoForUrl(videoUrl:String) {
        if(videoUrl.count > 5) {
            
            let url = URL(string:videoUrl)
            
            if(localplayer != nil) {
                if(localplayer?.contentURL?.absoluteString==videoUrl) {
                    //localplayer?.reloadPlayer()
                    localplayer?.play()
                    localplayer?.player?.isMuted = true
                    return
                } else {
                    localplayer?.replaceVideo(url!)
                    localplayer?.play()
                    localplayer?.player?.isMuted = true
                    return
                }
            }
            
            if url != nil {
                localplayer = VGPlayer(URL: url!)
            }
            
            localplayer?.delegate = self
            viewForVideo.addSubview((localplayer?.displayView)!)
            localplayer?.displayView.bringSubview(toFront:(localplayer?.displayView.playButtion)!)
            viewForVideo.bringSubview(toFront: videoImageView)
            localplayer?.backgroundMode = .suspend
            localplayer?.gravityMode = .resizeAspectFill
            localplayer?.displayView.backgroundColor = UIColor.black
            localplayer?.displayView.delegate = self
            localplayer?.displayView.snp.makeConstraints { [weak viewForVideo] (make) in
                guard let strongSelf = viewForVideo else { return }
                make.edges.equalTo(strongSelf)
            }
            localplayer?.player?.isMuted = true
            localplayer?.play()
        }
    }
    
    func updateProfileVideo(data: String,thumbnail:String){
        
        
        var newUrl = URL(string: thumbnail)
        
        let modifiedLink = data.replace(target:"video/upload/", withString:"video/upload/q_20,w_200,h_200/")
        
        let jpgURL = URL(string:modifiedLink)?
            .deletingPathExtension()
            .appendingPathExtension("gif")
        newUrl = jpgURL
        
        momentMediaView.isHidden = false
        momentMediaView.kf.setImage(with:newUrl, placeholder:UIImage())
        videoImageView.isHidden = false
        
    }
    
}

// MARK: - Implementing VGPlayerDelegate For playing Video
extension MomentsDetailCVCell: VGPlayerDelegate {
    func vgPlayer(_ player: VGPlayer, playerFailed error: VGPlayerError) {
        print(error)
    }
    
    func vgPlayer(_ player: VGPlayer, stateDidChange state: VGPlayerState) {
        print("player State ",state)
        if (player.state == .playFinished) {
            //profilePic.isHidden = true
            localplayer?.seekTime(0)
            localplayer?.play()
            localplayer?.player?.isMuted = true
        } else {
            //profilePic.isHidden = false
        }
    }
    
    func vgPlayer(_ player: VGPlayer, bufferStateDidChange state: VGPlayerBufferstate) {
        print("buffer State", state)
    }
    
}

extension MomentsDetailCVCell: VGPlayerViewDelegate {
    func vgPlayerView(_ playerView: VGPlayerView, willFullscreen fullscreen: Bool) {
        print(fullscreen)
    }
    
    func vgPlayerView(didTappedClose playerView: VGPlayerView) {
        if playerView.isFullScreen {
            playerView.exitFullscreen()
        } else {
            //self.navigationController?.popViewController(animated: true)
        }
    }
    
    func vgPlayerView(didDisplayControl playerView: VGPlayerView) {
        
    }
}
