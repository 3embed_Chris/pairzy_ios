//
//  ProfileDeatilVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 11/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ProfileDeatilVM: NSObject {
    
    //Variable and Constants
    let disposeBag = DisposeBag()
    let profileDeatilVM_resp = PublishSubject<ResponseType>()
    var firstImage = 0
    var reportReasonsArray = [ReportReason]()
    var profileView = UIView()
    var showProfileByID = false
    var errorMsg = ""
    var dataAvailable = true
    var profileDetails = Profile.fetchprofileDataObj(userDataDictionary:[:])
    var isBlocked = 0      ///use this and comment unnecessary calling API getProfile by ID in block and unblock API response
    enum ResponseType: Int{
        case getReporReasons = 0
        case blockUser = 1
        case errorMsg = 2
        case unblock = 3
        case getProfileById = 4
    }
    
    
    //  method forrequesting second chance profiles.
    func requestToGetReportReasons(userClicked:Bool) {
        
        let apiCall = MatchMakerAPICalls()
        apiCall.requestToGetReportReasons()
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        if(userClicked) {
            Helper.showProgressIndicator(withMessage:StringConstants.getReportReasons())
        }
        
        apiCall.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                self.handleReportReasonsResponse(response: response)
                
            }, onError: {error in
                self.profileDeatilVM_resp.onError(error)
            })
            .disposed(by:disposeBag)
    }
    
    
    func requestToUnBlock(userProfile: Profile){
        let apiCall = MatchMakerAPICalls()
        let params = [likeProfileParams.targetUserId: userProfile.userId]
        apiCall.requestToUnblock(requestData: params)
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        
        apiCall.requestToUnblock(requestData: params)
        apiCall.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                 self.getProfileDetailsByID()
             self.profileDeatilVM_resp.onNext(.unblock)
                
            }, onError: {error in
                self.profileDeatilVM_resp.onError(error)
            })
            .disposed(by:disposeBag)
        
    }
    
    
    //MARK: - API RESPONSE HANDLER - GET MATCHES, GET INSTANT MATCHES , GET SECOND LOOK PROFILES
    
    /// method for handling matches api response.
    ///
    /// - Parameter response: contains api response and status code.
    func handleReportReasonsResponse(response:ResponseModel) {
        
        switch (response.statusCode) {
        case 200:
            
            // 200 is for success response (contains profiles)
            // 412 is for when there is no profiles.
            var reportReasonData = [ReportReason]()
            if let profileResponse = response.response["data"] as? [AnyObject] {
                for (index, _) in profileResponse.enumerated() {
                    if let reason = profileResponse[index] as? String {
                        reportReasonData.append(ReportReason(reportDettails:reason))
                    }
                }
            }
            
            self.reportReasonsArray = reportReasonData
            
            //reportReasonsArray
            self.profileDeatilVM_resp.onNext(.getReporReasons)
            
        default: break
            
        }
        
        Helper.hideProgressIndicator()
    }
    
    
    
    /// method for blocking user
    ///
    /// - Parameter userProfile: selected user profile to block.
    func requestForBlockUser(userProfile:Profile) {
        let params = [likeProfileParams.targetUserId:userProfile.userId]
        Helper.showProgressIndicator(withMessage:StringConstants.blockUser())
        
        
        API.requestPatchURL(serviceName: APINAMES.blockProfile,
                            withStaticAccessToken:false,
                            params: params,
                            success: { (response) in
                                Helper.hideProgressIndicator()
                                if (response.null != nil){
                                    return
                                }
                                
                                let statuscode = response.dictionaryObject!["code"] as! Int
                                switch (statuscode) {
                                case 200:
                                     self.getProfileDetailsByID()
                                    self.profileDeatilVM_resp.onNext(.blockUser)
                                    
                                    break
                                case 401:
                                    
                                    Helper.changeRootVcInVaildToken()
                                    break
                                default:
                                    if let errorMsg = response.dictionaryObject![LoginResponse.errorMessage]  as? String {
                                        self.errorMsg = errorMsg
                                    }
                                    self.profileDeatilVM_resp.onNext(.errorMsg)
                                    break
                                    
                                }
                                
        },
                            failure: { (Error) in
                                
                                Helper.hideProgressIndicator()
                                
                                self.profileView.showErrorMessage(titleMsg: StringConstants.error(), andMessage: Error.localizedDescription)
                                
        })
        
        
    }
    
    
     /// API for getProfile by userID
    func getProfileDetailsByID() {
        let apiName = String(format:"%@%@",APINAMES.getProfileDetails,profileDetails.userId)
        if showProfileByID {
            Helper.showProgressIndicator(withMessage: "")
        }
        
        API.requestGETURL(serviceName: apiName,
                          withStaticAccessToken:false,
                          success: { (response) in
                            print(response)
                            Helper.hideProgressIndicator()
                            if (response.null != nil){
                                return
                            }
                            
                            let statuscode = response.dictionaryObject!["code"] as! Int
                            switch (statuscode) {
                            case 200:
                                
                                self.dataAvailable = true
                                
                               
                                if let respDetails = response.dictionaryObject!["data"] as? [String:Any] {
                                    
                                   
                                    self.profileDetails = Profile.fetchprofileDataObj(userDataDictionary:respDetails)
                                    var allNewPref = self.profileDetails.myPreferences
                                    
                                    for (prefIndex,pref) in self.profileDetails.myPreferences.enumerated() {
                                        var singlePref = [PreferenceSubModel]()
                                        for(_,subPref) in pref.arrayOfSubPreference.enumerated() {
                                            if(subPref.selectedValues.count > 0) {
                                                singlePref.append(subPref)
                                            }
                                        }
                                        allNewPref[prefIndex].arrayOfSubPreference = singlePref
                                    }
                                    self.profileDetails.myPreferences = allNewPref
                                    self.isBlocked = self.profileDetails.isUserBlocked
                                }
                                self.profileDeatilVM_resp.onNext(.getProfileById)
                            case 401:
                                        Helper.changeRootVcInVaildToken()
                                break
                            default:
                                self.profileView.showErrorMessage(titleMsg: StringConstants.error(), andMessage: response.dictionaryObject![LoginResponse.errorMessage] as! String)
                            }
                            
        },
                          failure: { (Error) in
                            
                            Helper.hideProgressIndicator()
                            
                          self.profileView.showErrorMessage(titleMsg: StringConstants.error(), andMessage: Error.localizedDescription)
                            Helper.hideProgressIndicator()
        })
    }
    
}


