//
//  ProfileInfoCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 11/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class ProfileInfoCell: UITableViewCell {
    
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var workLabel: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var onlineStatusIcon: UIImageView!
    @IBOutlet weak var workIconHeght: NSLayoutConstraint!
    
    @IBOutlet weak var chatButtonOutlet: UIButton!
    @IBOutlet weak var hightOfAgeDist: NSLayoutConstraint!
    @IBOutlet weak var workIcon: UIButton!
    @IBOutlet weak var superLikeIcon: UIImageView!
    @IBOutlet weak var superLikeWidth: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        workIconHeght.constant = 30
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    func updateCell(profileDetails: Profile){
       
        if(profileDetails.isHiddenUserAge == 1) {
            profileName.text = profileDetails.firstName
        } else {
            profileName.text = profileDetails.firstName.appending(", ").appending(profileDetails.userAge)
        }
        
        if profileDetails.isHiddenDistance == 1{
            distance.text = ""
            hightOfAgeDist.constant = 0
        }else {
            if(profileDetails.distance == StringConstants.lessThan()) {
                 distance.text = profileDetails.distance.appending(" " + StringConstants.aKmAway())
            } else {
                 distance.text = profileDetails.distance.appending(" " + StringConstants.kmsAway())
            }
            hightOfAgeDist.constant = 30
        }
        
        workIconHeght.constant = 30
        if profileDetails.work.count != 0 {
            self.workLabel.text = profileDetails.work
            workIcon.setImage(#imageLiteral(resourceName: "work"), for: .normal)
        }else if profileDetails.education.count != 0 {
            self.workLabel.text = profileDetails.education
             workIcon.setImage(#imageLiteral(resourceName: "Education"), for: .normal)
        }else if profileDetails.job.count != 0  {
            workIcon.setImage(#imageLiteral(resourceName: "work"), for: .normal)
            self.workLabel.text = profileDetails.job
        }
        else {
                        workIconHeght.constant = 0
                        self.workLabel.text = ""
        }
        if profileDetails.onlineStatus == 1 {
            self.onlineStatusIcon.image = #imageLiteral(resourceName: "Ellipse 95")
            self.onlineStatusIcon.isHidden = false
        }else {
             self.onlineStatusIcon.isHidden = true
            self.onlineStatusIcon.image = #imageLiteral(resourceName: "Ellipse 96")
           
        }
        
        
        if profileDetails.superLiked == 0 {
            superLikeIcon.isHidden = true
            superLikeWidth.constant = 0
            
        }else {
            superLikeIcon.isHidden = false
            superLikeWidth.constant = 20
        }
    }
    
}
