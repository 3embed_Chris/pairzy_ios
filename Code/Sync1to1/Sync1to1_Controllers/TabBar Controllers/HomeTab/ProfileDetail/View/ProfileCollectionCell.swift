//
//  ProfileCollectionCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 11/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit


protocol  profileCellDelegate: class{
    func tappedOnInstaMedia(atIndex:Int,mediaArray:[String])
}


class ProfileCollectionCell: UITableViewCell {
    
    @IBOutlet weak var collectionTitle: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var connectBtn: UIButton!
    @IBOutlet weak var collectionViewHight: NSLayoutConstraint!
    @IBOutlet weak var connectBtnHeight: NSLayoutConstraint!
    
    
   weak var profileDelegate:profileCellDelegate? = nil
    var instaProfile:InstagramProfile? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // collectionViewHight.constant = 300
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    /// updates user instagram posted images
    ///
    /// - Parameter userInstaMedia: is of type instagramm profile
    func updateInstaMedia(userInstaMedia: InstagramProfile){
        self.instaProfile = userInstaMedia
        let numberofPages = (instaProfile?.userMedia.count)!/6
        if numberofPages > 1 {
            pageControl.numberOfPages = (instaProfile?.userMedia.count)!/6
        }else {
            pageControl.numberOfPages = 0
        }
        
      //Share your Instagram photos, too
        if (instaProfile?.userMedia.count)! > 1 {
            self.collectionTitle.text = String(format:StringConstants.instagramPhotos(),(instaProfile?.userMedia.count)!)
            //"\((instaProfile?.userMedia.count)!) Instagram Photos"
        }else {
            self.collectionTitle.text = String(format:StringConstants.instagramPhoto(),(instaProfile?.userMedia.count)!)
            //"\((instaProfile?.userMedia.count)!) Instagram Photo"
        }
        
        self.imageCollectionView.reloadData()
    }
    
    
    
}


// MARK: - UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension ProfileCollectionCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if instaProfile != nil {
            return 1
        }else {
            return  0
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if instaProfile != nil {
            return (instaProfile?.userMedia.count)!
        }else {
            return 0
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileImagesCell", for: indexPath) as! ProfileImagesCell
        cell.updateInstaMedia(imageUrl: (instaProfile?.userMedia[indexPath.item])!)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        profileDelegate?.tappedOnInstaMedia(atIndex: indexPath.row, mediaArray:(instaProfile?.userMedia)!)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize.zero
        let cellWidth = (collectionView.frame.size.width - 20)/3
        let cellHight = (cellWidth)
        size.height =  cellHight
        size.width  =  cellWidth
        return size
        
    }
    
}

// MARK: - UIScrollViewDelegate
extension ProfileCollectionCell: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.pageControl.currentPage = Int(pageNumber)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.pageControl.currentPage = Int(pageNumber)
    }
    
    
    /// method for updting frame.
    ///
    /// - Parameter newOffsetPoint: point for new frame.
    func updateScrollViewFrame(newOffsetPoint:CGFloat) {
        var scrollContentOffset = self.imageCollectionView.contentOffset
        scrollContentOffset.x = newOffsetPoint
        self.imageCollectionView.setContentOffset(scrollContentOffset, animated: true)
    }
}


