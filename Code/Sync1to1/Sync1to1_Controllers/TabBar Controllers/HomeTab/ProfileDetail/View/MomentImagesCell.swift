//
//  MomentImagesCell.swift
//  Datum
//
//  Created by Rahul Sharma on 06/06/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

class MomentImagesCell: UICollectionViewCell {
    
    
    
    @IBOutlet weak var momentsPicture: UIImageView!
    
    
    @IBOutlet weak var videoImageView: UIImageView!
    
   
    let profileDeatilVM = ProfileDeatilVM()
    //var localplayer : VGPlayer?
    var imageName = "man_default1"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    /// Updates Moments Media
    ///
    /// - Parameter imageUrl/VideoUrl: image/Video url is of string
    func updateMomentsMedia(mediaUrl:String){
        if mediaUrl.suffix(3) == "jpg" || mediaUrl.contains("facebook"){
            let newUrl = URL(string: mediaUrl)
            self.videoImageView.isHidden = true
            momentsPicture.kf.setImage(with:newUrl, placeholder:UIImage())
            momentsPicture.layer.cornerRadius = 4
            momentsPicture.layer.borderWidth = 1
            momentsPicture.layer.borderColor = Colors.BorderColor.cgColor
        }else{
           // let newUrl = URL(string: mediaUrl)
            //momentsPicture.kf.setImage(with:newUrl, placeholder:UIImage())
            
            //GIf set to momentPicture
            //let thumbNailUrl = mediaUrl.replace(target:".mp4", withString:".jpg")
            //updateProfileVideo(data: mediaUrl, thumbnail:thumbNailUrl )
            
            let endIndex = mediaUrl.index(mediaUrl.endIndex, offsetBy: -3)
            var truncated = mediaUrl.substring(to: endIndex)
            truncated = truncated + "png"
            self.momentsPicture.setImageOn(imageUrl: truncated, defaultImage: #imageLiteral(resourceName: "defaultPicture"))
            videoImageView.isHidden = false
            momentsPicture.layer.cornerRadius = 4
            momentsPicture.layer.borderWidth = 1
            momentsPicture.layer.borderColor = Colors.BorderColor.cgColor
            
        }
       
    }
    
    func updateProfileVideo(data: String,thumbnail:String){
        
        
        var newUrl = URL(string: thumbnail)
        
        let modifiedLink = data.replace(target:"video/upload/", withString:"video/upload/q_20,w_200,h_200/")
        
        let jpgURL = URL(string:modifiedLink)?
            .deletingPathExtension()
            .appendingPathExtension("gif")
        newUrl = jpgURL
        videoImageView.isHidden = false
        momentsPicture.kf.setImage(with:newUrl, placeholder:UIImage())
        videoImageView.isHidden = false
        
    }
    
}
