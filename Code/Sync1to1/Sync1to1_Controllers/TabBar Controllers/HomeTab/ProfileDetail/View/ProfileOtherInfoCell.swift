//
//  ProfileOtherInfoCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 11/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class ProfileOtherInfoCell: UITableViewCell {

    @IBOutlet weak var optionLabel: UILabel!
    @IBOutlet weak var selectedLabel: UILabel!
    @IBOutlet weak var labelTopConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       // labelTopConstraint.constant = 12
    }

    func updateProfileDisc(){
        labelTopConstraint.constant = 25
        optionLabel.text = ""
        selectedLabel.text = ""
    }
    
    
    func updatePrefernces(option: String,selectedOp: String){
        labelTopConstraint.constant = 12
        optionLabel.text = option
        selectedLabel.text = selectedOp
    }
    
    func updateVitals(option: String,selectedOp: String){
        labelTopConstraint.constant = 12
        optionLabel.text = option
        selectedLabel.text = selectedOp
    }
    
    func updateValues(option: String,selectedOp: String) {
        optionLabel.text = option
        selectedLabel.text = selectedOp
    }
    
    func updateVertues(option: String,selectedOp: String){
        labelTopConstraint.constant = 12
        optionLabel.text = option
        selectedLabel.text = selectedOp
    }
    
    func updateVices(option: String,selectedOp: String){
        labelTopConstraint.constant = 12
        optionLabel.text = option
        selectedLabel.text = selectedOp
        
    }
    
    
    func updateLocation(location: Location){
        if location.secondaryText.count != 0 {
            selectedLabel.text = location.secondaryText
            
        }
        else if location.state.count != 0 || location.country.count != 0 {
            selectedLabel.text = location.city + ", " + location.state + ", " + location.country
        }
        else {
            selectedLabel.text = location.name
        }
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
