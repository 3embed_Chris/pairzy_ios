//
//  MomentsViewCell.swift
//  Datum
//
//  Created by Rahul Sharma on 27/05/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

class MomentsViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var momentCollectionView: UICollectionView!
    
    
    var profileDetalVC = ProfileDetailVC()
    var momentImages  = [String]()
    let profileDeatilVM = ProfileDeatilVM()
    var profileMoments:[MomentsModel]?
    var profileModel: Profile?
    var commonVC: UIViewController?
    var isFromMyProfile = true
    var index:Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    //Passing View Controller object MyProfileVc
    func update(_ vc: UIViewController){
        self.commonVC = vc
    }

}

// MARK: - UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension MomentsViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if profileMoments != nil {
            return 1
        }else {
            return  0
        }    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if profileMoments != nil {
            if (profileMoments!.count) < 6 {
                return (profileMoments!.count)
            }else{
                return 6
            }
        }else {
            return 0
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "MomentImagesCell", for: indexPath) as! MomentImagesCell
        cell.updateMomentsMedia(mediaUrl: (profileMoments![indexPath.item].url[0]))
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard  = UIStoryboard(name: "DatumTabBarControllers", bundle: nil)
        let momentDetailVC = storyBoard.instantiateViewController(withIdentifier: StoryBoardIdentifier.MomentsDetailViewController) as! MomentsDetailViewController
        self.index = indexPath.item
        momentDetailVC.selectedIndex = self.index
        momentDetailVC.selectedUserMoments = self.profileMoments!
        momentDetailVC.selectedProfile = self.profileModel
        if isFromMyProfile {
        momentDetailVC.isFromMyProfile = true
        commonVC!.navigationController?.pushViewController(momentDetailVC, animated: false)
        }else{
        momentDetailVC.isFromMyProfile = false
        commonVC!.performSegue(withIdentifier: SegueIds.ProfileDetailToMomentDetailVC, sender: nil)
        }
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize.zero
        let cellWidth = (collectionView.frame.size.width - 20)/3
        let cellHight = (cellWidth)
        size.height =  cellHight
        size.width  =  cellWidth
        return size
      
    }
    
}
