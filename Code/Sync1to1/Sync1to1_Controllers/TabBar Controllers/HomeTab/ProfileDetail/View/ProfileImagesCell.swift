//
//  ProfileImagesCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 11/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import AudioToolbox
import VGPlayer
import AVKit

class ProfileImagesCell: UICollectionViewCell {    //reused for header collection and instagram media
    
    @IBOutlet weak var profilePic: UIImageView!   // for both
    @IBOutlet weak var videoView: UIView!         // for profile detail header
    @IBOutlet weak var playBtn: UIButton!          //
    
    @IBOutlet weak var gradientImage: UIGradientImageView!
    
    let profileDeatilVM = ProfileDeatilVM()
    var localplayer : VGPlayer?
    var imageName = "man_default1"
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    /// Updates Instagram Media
    ///
    /// - Parameter imageUrl: image url is of string
    func updateInstaMedia(imageUrl:String){
        let newUrl = URL(string: imageUrl)
        profilePic.kf.setImage(with:newUrl, placeholder:UIImage())
        profilePic.layer.cornerRadius = 4
        profilePic.layer.borderWidth = 1
        profilePic.layer.borderColor = Colors.BorderColor.cgColor
        
    }
    
    
    func updateProfileHeader(data:String){
      //  gradientImage.setTopGradient()
        playBtn.isHidden = true
        let url = URL(string: data)
        profilePic.kf.setImage(with:url, placeholder:UIImage.init(named:imageName))
        
    }
    
    func updateProfileVideo(data: String,thumbnail:String){
      //  gradientImage.setTopGradient()
       // gradientImage.setup()
        var newUrl = URL(string: thumbnail)
        if thumbnail.count == 0 {
            
            let jpgURL = URL(string:data)?
                .deletingPathExtension()
                .appendingPathExtension("jpg")
            
            newUrl = jpgURL
        }
        
        profilePic.isHidden = false
        
        profilePic.kf.setImage(with:newUrl, placeholder:UIImage.init(named: imageName))
        
        showVideoForUrl(videoUrl:data)
        playBtn.isHidden = false
    }
    
    
    func showVideoForUrl(videoUrl:String) {
        if(videoUrl.count > 5) {
            
            let url = URL(string:videoUrl)
            if url != nil {
                localplayer = VGPlayer(URL: url!)
            }
            
            localplayer?.delegate = self
            
            
            videoView.addSubview((localplayer?.displayView)!)
            localplayer?.displayView.bringSubview(toFront:(localplayer?.displayView.playButtion)!)
            // localplayer?.displayView.clipsToBounds = true
          //  videoView.bringSubview(toFront: playBtn)
            localplayer?.backgroundMode = .proceed
            localplayer?.displayView.backgroundColor = UIColor.black
            localplayer?.displayView.delegate = self
            localplayer?.displayView.snp.makeConstraints { [weak videoView] (make) in
                guard let strongSelf = videoView else { return }
                make.edges.equalTo(strongSelf)
            }
            
            
        }
    }
    
    
    @IBAction func playBtnAction(_ sender: Any) {
        
        if(self.localplayer?.state == .playFinished) {
            self.localplayer?.displayView.replayButton.sendActions(for: .touchUpInside)
        } else {
            self.localplayer?.displayView.playButtion.sendActions(for: .touchUpInside)
        }
        
    }
}

extension ProfileImagesCell: VGPlayerDelegate {
    func vgPlayer(_ player: VGPlayer, playerFailed error: VGPlayerError) {
        print(error)
    }
    
    func vgPlayer(_ player: VGPlayer, stateDidChange state: VGPlayerState) {
        print("player State ",state)
        if (player.state == .playing) {
            self.playBtn.isSelected = true
            profilePic.isHidden = true
        } else {
            self.playBtn.isSelected = false
            profilePic.isHidden = false
        }
    }
    
    func vgPlayer(_ player: VGPlayer, bufferStateDidChange state: VGPlayerBufferstate) {
        print("buffer State", state)
    }
    
}

extension ProfileImagesCell: VGPlayerViewDelegate {
    func vgPlayerView(_ playerView: VGPlayerView, willFullscreen fullscreen: Bool) {
        print(fullscreen)
    }
    
    func vgPlayerView(didTappedClose playerView: VGPlayerView) {
        if playerView.isFullScreen {
            playerView.exitFullscreen()
        } else {
            //self.navigationController?.popViewController(animated: true)
        }
    }
    
    func vgPlayerView(didDisplayControl playerView: VGPlayerView) {
        //        UIApplication.shared.setStatusBarHidden(!playerView.isDisplayControl, with: .fade)
    }
}
