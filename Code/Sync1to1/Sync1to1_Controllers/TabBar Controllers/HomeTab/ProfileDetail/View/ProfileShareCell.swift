//
//  ProfileShareCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 11/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class ProfileShareCell: UITableViewCell {

    @IBOutlet weak var reportLabel: UILabel!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var warningBtn: UIButton!
    @IBOutlet weak var recommendLabel: UILabel!
    
    @IBOutlet weak var separator: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func updateCell(title: String){
      let capsTitle = title.capitalized
        
    warningBtn.setTitle(capsTitle, for: .normal)
    }
    
}
