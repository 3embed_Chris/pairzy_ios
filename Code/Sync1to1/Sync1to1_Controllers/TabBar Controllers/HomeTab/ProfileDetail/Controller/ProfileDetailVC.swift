//
//  ProfileDetailVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 11/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import AudioToolbox
import VGPlayer
import AVKit


protocol profileInterestUpdated: class {
    func profileUpdatedWithStatus(status:profileActivityStatus)
    func openChatForProfile(profile:Profile)
}


class ProfileDetailVC: UIViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var photoIndicatorCollection: UICollectionView!
    @IBOutlet weak var scrollViewForImages: UIScrollView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var playTimingLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var topBackBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleHeader: UIView!
    @IBOutlet weak var bottomBtnsView: UIView!
    @IBOutlet weak var buttonsLeading: NSLayoutConstraint!
    @IBOutlet weak var navigationChatBtn: UIButton!
    @IBOutlet weak var unlikBtnView: UIView!
    @IBOutlet weak var unlikeBtn: UIButton!
    @IBOutlet weak var bottomViewHt: NSLayoutConstraint!
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var chatBtn: UIButton!
 
    var popUpFor:CoinsPopupFor = .forAll
    weak var profileUpdateDelegate:profileInterestUpdated? = nil
    var headerMaskLayer : CAShapeLayer!
    var localplayer : VGPlayer?
    var userDeepLinkUrl = ""
    var profileFromChat = false
    var instagramProfile:InstagramProfile? = nil
    let profileDeatilVM = ProfileDeatilVM()
    let instagramVM = InstagramVM()
    let tempProfile:Profile? = nil
    let userProfile:Profile? = nil
    var tableHeaderViewHeight : CGFloat = 400.0
    var staticRows = 4
    var isSelf: Bool = false
    var userMoments:[MomentsModel]? = nil
    var momentViewCell = UITableViewCell()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UITableViewHeaderFooterView.appearance().backgroundColor = UIColor.white
        mainTableView.estimatedRowHeight = 50
        // Do any additional setup after loading the view.
        header.frame = CGRect(x: 0, y: 0, width: mainTableView.frame.size.width, height: tableHeaderViewHeight)
        self.requestForReportUserReasons(userClicked: false,selectedUser: profileDeatilVM.profileDetails)
        self.playBtn.isHidden = true
        hideActivityButtons(hide: true)
        if profileFromChat {
            self.chatBtn.isHidden =  true
        }else {
            self.chatBtn.isHidden =  false
        }
        
        self.bottomBtnsView.isHidden = true
        
        profileDeatilVM.profileView = self.view
        didGetResponse()
        didGetInstaResp()
        
        if(!profileDeatilVM.showProfileByID) {
            profileDeatilVM.getProfileDetailsByID()
            mainTableView.reloadData()
            titleLabel.text = profileDeatilVM.profileDetails.firstName
        } else {
           self.headerView.isHidden = true
        }
        self.header.bringSubview(toFront: chatBtn)
        
        setupHeaderView()
        updateHeaderConstraints()
        updateScrollView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        titleLabel.text = profileDeatilVM.profileDetails.firstName
        titleHeader.backgroundColor = UIColor.clear
        self.titleLabel.textColor = UIColor.clear
        topBackBtn.isHidden = false
        titleHeader.isHidden = false
        backBtn.isHidden = true
        profileDeatilVM.profileDetails = self.profileDeatilVM.profileDetails
        navigationChatBtn.isHidden = true
        scrollToCell()
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopVideoPlay()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.header.translatesAutoresizingMaskIntoConstraints = true
    }
    
    
    func updateScrollView(){
        removeSubviews()
        var imageName = ""
        if self.profileDeatilVM.profileDetails.gender == "Male" {
            imageName = "man_default1"
        }else {
            imageName = "woman_default1"
        }
        
        for i in 0..<self.profileDeatilVM.profileDetails.allMedia.count {
            //  let window = UIApplication.shared.keyWindow!
            let x = self.view.frame.size.width * CGFloat(i)
            if i == 0 && profileDeatilVM.profileDetails.hasProfileVideo == 1{
                let videoView1 = UIView()
                
                videoView1.frame = CGRect(x: x, y: 0, width: self.view.frame.size.width, height: 400)
               videoView1.contentMode = .scaleAspectFill
                videoView1.clipsToBounds = true
                scrollViewForImages.contentSize.width = self.view.frame.size.width * CGFloat(i + 1)
                scrollViewForImages.addSubview(videoView1)
                showVideoForUrl(videoUrl: self.profileDeatilVM.profileDetails.allMedia[i],videoView: videoView1)
            }else {
                let imageView = UIImageView()
                imageView.frame = CGRect(x: x, y: 0, width: self.view.frame.size.width, height: 400)
                imageView.contentMode = .scaleAspectFill
                imageView.clipsToBounds = true
                
                let url = URL(string: self.profileDeatilVM.profileDetails.allMedia[i])
                imageView.kf.setImage(with:url, placeholder:UIImage.init(named:imageName))
                
                scrollViewForImages.contentSize.width = self.view.frame.size.width * CGFloat(i + 1)
                imageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(tapped(_ :)))
                imageView.addGestureRecognizer(tap)
                scrollViewForImages.addSubview(imageView)
            }
        }
    }
    
    /// removes subview
    func removeSubviews(){
        let subViews = self.scrollViewForImages.subviews
        for subview in subViews{
            
            subview.removeFromSuperview()
        }
        
    }
    
    func showVideoForUrl(videoUrl:String,videoView: UIView) {
        if(videoUrl.count > 5) {
            
            let url = URL(string:videoUrl)
            if url != nil {
                localplayer = VGPlayer(URL: url!)
            }
            
            localplayer?.delegate = self
            localplayer?.displayView.tag = 2018
            videoView.addSubview((localplayer?.displayView)!)
            localplayer?.displayView.bringSubview(toFront:(localplayer?.displayView.playButtion)!)
            localplayer?.backgroundMode = .proceed
            localplayer?.displayView.backgroundColor = UIColor.black
            localplayer?.displayView.delegate = self
            localplayer?.displayView.snp.makeConstraints { [weak videoView] (make) in
                guard let strongSelf = videoView else { return }
                make.edges.equalTo(strongSelf)
            }
            
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapped(_ :)))
            localplayer?.displayView.addGestureRecognizer(tap)
            localplayer?.displayView.isUserInteractionEnabled = true
        }
    }
    
    // method for tableview header
    ///contains image view and name of the profile.
    func setupHeaderView()
    {
        self.header.translatesAutoresizingMaskIntoConstraints = true
        header = mainTableView.tableHeaderView
        mainTableView.tableHeaderView = nil
        mainTableView.addSubview(header)
        
        mainTableView.contentInset = UIEdgeInsets(top: tableHeaderViewHeight, left: 0, bottom: 0, right: 0)
        mainTableView.contentOffset = CGPoint(x: 0, y: -tableHeaderViewHeight)
        
        // cut away the header view
        headerMaskLayer = CAShapeLayer()
        headerMaskLayer.fillColor = UIColor.black.cgColor
        header.layer.mask = headerMaskLayer
        
        let effectiveHeight = tableHeaderViewHeight
        mainTableView.contentInset = UIEdgeInsets(top: effectiveHeight, left: 0, bottom: 0, right: 0)
        mainTableView.contentOffset = CGPoint(x: 0, y: -effectiveHeight)
        updateHeaderView()
    }
    
    
    
    /// method for update header with image.
    func updateHeaderView()
    {
        let effectiveheight = tableHeaderViewHeight
        var headerRect = CGRect(x: 0, y: -effectiveheight, width: mainTableView.bounds.width, height: tableHeaderViewHeight)
        
        if mainTableView.contentOffset.y < -effectiveheight{
            headerRect.origin.y = mainTableView.contentOffset.y
            headerRect.size.height = -mainTableView.contentOffset.y
        }
        
        header.frame = headerRect
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: headerRect.width, y: 0))
        path.addLine(to: CGPoint(x: headerRect.width, y: headerRect.height))
        path.addLine(to: CGPoint(x: 0, y: headerRect.height - 0.0))
        headerMaskLayer?.path = path.cgPath
        
        self.header.bringSubview(toFront: chatBtn)
        scrollViewForImages.layoutIfNeeded()
        scrollViewForImages.updateConstraints()
        updateImagesHeight()
        
    }
    
    
    func updateImagesHeight() {
        
        for subView in scrollViewForImages.subviews {
            
            //updating the frame for images.
            if let imageView = subView as? UIImageView {
                var oldFrame = imageView.frame
                oldFrame.size.height = scrollViewForImages.frame.size.height
                imageView.frame = oldFrame
                scrollViewForImages.layoutIfNeeded()
            }
            else {
                
                //updating the frame for video
                let videoSuperView = subView
                if let disPlayView = videoSuperView.viewWithTag(2018) {
                    var oldFrame = videoSuperView.frame
                    oldFrame.size.height = scrollViewForImages.frame.size.height
                    videoSuperView.frame = oldFrame
                    
                    localplayer?.displayView.frame = oldFrame
                    localplayer?.displayView.layoutIfNeeded()
                    scrollViewForImages.layoutIfNeeded()
                }
            }
        }
    }
    
    func updateHeaderConstraints(){
        
        self.header.translatesAutoresizingMaskIntoConstraints = false
        
        mainTableView.addConstraint(NSLayoutConstraint(item: header, attribute: .top, relatedBy: .equal, toItem: mainTableView, attribute: .top, multiplier: 1, constant: -tableHeaderViewHeight))
        
        mainTableView.addConstraint(NSLayoutConstraint(item: header, attribute: .leading, relatedBy: .equal, toItem: mainTableView, attribute: .leading, multiplier: 1, constant: 0))
        
        mainTableView.addConstraint(NSLayoutConstraint(item: header, attribute: .height, relatedBy: .equal, toItem: mainTableView.tableHeaderView, attribute: .height, multiplier: 1, constant: tableHeaderViewHeight))
        
        mainTableView.addConstraint(NSLayoutConstraint(item: header, attribute: .width, relatedBy: .equal, toItem: mainTableView, attribute: .width, multiplier: 1, constant: 0))
        
    }
    
    
    func stopVideoPlay() {
        if((localplayer) != nil) {
            localplayer?.pause()
        }
    }
    
    func hideActivityButtons(hide:Bool) {
        //SELF.LIKE.ISHIDDEN = hide
    }
    
    
    /// method for requesting second look profiles.
    func requestForReportUserReasons(userClicked:Bool,selectedUser:Profile) {
        Helper.showProgressIndicator(withMessage: "")
        profileDeatilVM.requestToGetReportReasons(userClicked:userClicked)
        self.profileDeatilVM.profileDeatilVM_resp
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                
                if (userClicked) {
                    //show reasons popup
                    if(self.profileDeatilVM.reportReasonsArray.count > 0) {
                        self.showReportReasons(selectedUser:self.profileDeatilVM.profileDetails)
                    }
                }
            }, onError:{ error in
                
            })
            
            .disposed(by:profileDeatilVM.disposeBag)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Button Actions
    
    @IBAction func chatAction(_ sender: Any) {
        dismiss(animated: true, completion: {
            DispatchQueue.main.async {
                if(self.profileUpdateDelegate != nil) {
                    self.profileUpdateDelegate?.openChatForProfile(profile: self.profileDeatilVM.profileDetails)
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name("openChatFromOtherControllers"), object: self.profileDeatilVM.profileDetails, userInfo:nil)
                }
            }
        })
        
    }
    
    
    @IBAction func optionsAction(_ sender: Any) {
        
        let alert:UIAlertController=UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let recomondToFrnd = UIAlertAction(title: StringConstants.Recomend(), style: UIAlertActionStyle.default, handler: {(Bool) in
            self.shareUserProfile()
            
        })
        
        let reportToProfile = UIAlertAction(title: StringConstants.report() + " \(profileDeatilVM.profileDetails.firstName)", style: UIAlertActionStyle.default, handler: {(Bool) in
            self.reportAction(UIButton())
        })
        
        var title = StringConstants.Blocked() + profileDeatilVM.profileDetails.firstName
        
        if self.profileDeatilVM.profileDetails.isUserBlocked == 1 {
            title = StringConstants.unBlock() + " " + profileDeatilVM.profileDetails.firstName
        }else if self.profileDeatilVM.profileDetails.isUserBlocked == 0 {
            title = StringConstants.block() + " " + profileDeatilVM.profileDetails.firstName
        }
        
        let blockXyz = UIAlertAction(title: title, style: UIAlertActionStyle.default, handler: {(Bool) in
            self.showConfirmBlockAlert()
        })
        
        let cancelAction = UIAlertAction(title: StringConstants.cancel(), style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        blockXyz.setValue(UIColor.red, forKey: "titleTextColor")
        alert.addAction(recomondToFrnd)
        alert.addAction(reportToProfile)
//        if self.profileDeatilVM.profileDetails.isMatchedUser == 1{
//            alert.addAction(blockXyz)
//        }
        alert.addAction(blockXyz)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showConfirmBlockAlert() {
        
        var isForBlock = true
        var  msgForAlert = StringConstants.confirmBlock()
        var buttonText = StringConstants.block()
        
        if self.profileDeatilVM.profileDetails.isUserBlocked == 1 {
            isForBlock = false
            msgForAlert = StringConstants.confirmUnBlock()
            buttonText  = StringConstants.unBlock()
        }
        
        let alert = UIAlertController(title: nil, message: msgForAlert, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: StringConstants.actionSheetCancelText(), style: .cancel, handler: {(_) in }))
        alert.addAction(UIAlertAction(title: buttonText, style: .default, handler: {(action:UIAlertAction!) in
            if isForBlock {
                self.profileDeatilVM.requestToUnBlock(userProfile: self.profileDeatilVM.profileDetails)
            } else {
                self.profileDeatilVM.requestForBlockUser(userProfile: self.profileDeatilVM.profileDetails)
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func boostBtnAction(_ sender: Any) {
        //openInAppPurchaseView()
    }
    
    @IBAction func backAction(_ sender: Any) {
        stopVideoPlay()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func playBtnAction(_ sender: UIButton) {
        
        if(self.localplayer?.state == .playFinished) {
            self.localplayer?.displayView.replayButton.sendActions(for: .touchUpInside)
        } else {
            self.localplayer?.displayView.playButtion.sendActions(for: .touchUpInside)
        }
    }
    
    @IBAction func superLikeButtonAction(_ sender: Any) {
        if(profileUpdateDelegate != nil) {
            profileUpdateDelegate?.profileUpdatedWithStatus(status: .superLiked)
        } else {
            requestSuperLikeApi()
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func likeButtonAction(_ sender: Any) {
        if(profileUpdateDelegate != nil) {
            profileUpdateDelegate?.profileUpdatedWithStatus(status: .liked)
        } else {
            requestLikeApi()
        }
        dismiss(animated: true, completion: nil)
//        if(Helper.isUserOutOfLikes()) {
//            openInAppPurchaseViewForOutOfLikes()
//        } else {
//            if(profileUpdateDelegate != nil) {
//                profileUpdateDelegate?.profileUpdatedWithStatus(status: .liked)
//            } else {
//                requestLikeApi()
//            }
//            dismiss(animated: true, completion: nil)
//        }
    }
    
    @IBAction func unLikeButtonAction(_ sender: Any) {
        if(profileUpdateDelegate != nil) {
            profileUpdateDelegate?.profileUpdatedWithStatus(status: .unLiked)
        } else {
            requestUnlikeApi()
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func recomendToFreindAction(_ sender: Any) {
        shareUserProfile()
    }
    
    
    
    @IBAction func viewAllMomentsAction(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: SegueIds.profileDetailToMomentVC, sender: self)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIds.profileDetailToMomentVC{
        let momentVC = segue.destination as! MomentsViewController
            momentVC.momentMedia = self.userMoments!
            momentVC.isFromMyProfile = false
        }
        if segue.identifier == SegueIds.ProfileDetailToMomentDetailVC{
            let momentDetailVC = segue.destination as! MomentsDetailViewController
            momentDetailVC.selectedUserMoments = self.userMoments!
            if let momentCell = momentViewCell as? MomentsViewCell{
                momentDetailVC.selectedIndex = momentCell.index
            }
        }


    }
    
    
    
//
//
//   // MomentVC
//    func gotoMomentVC(){
//        let momentVC = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.MomentsViewController) as! MomentsViewController
//        momentVC.momentMedia = self.profileDeatilVM.profileDetails.moments
//        self.navigationController?.pushViewController(momentVC, animated: true)
//    }
  
    func shareUserProfile() {
        if(userDeepLinkUrl != "") {
            self.openControllerForSocialShare(linkToShare: self.userDeepLinkUrl)
            
            return
        }
        openActivityController(profileDetails: profileDeatilVM.profileDetails)
    }
    
    @IBAction func reportAction(_ sender: Any) {
        
        if(self.profileDeatilVM.reportReasonsArray.count == 0) {
            self.requestForReportUserReasons(userClicked: true,selectedUser: profileDeatilVM.profileDetails)
        } else {
            self.showReportReasons(selectedUser: profileDeatilVM.profileDetails)
        }
        
    }
    
    @IBAction func connectButtonAction(_ sender: Any) {
        
        let instaLoginVc = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "instaVC") as! InstagramLoginVC
        if self.instagramProfile != nil {
            
        } else {
            let navigationController = UINavigationController(rootViewController: instaLoginVc)
            self.navigationController?.present(navigationController, animated:true, completion:nil)
        }
        
    }
    
    
    func openInAppPurchaseView() {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = InAppPuchasePopupView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.loadInAppPurchaseView()
        window.addSubview(videoView)
    }
    
    
    func openInAppPurchaseViewForOutOfLikes() {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = TimerPopUpView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.popupFor = timerPopType.outOfLikes
        videoView.loadTimerPopupView()
        window.addSubview(videoView)
    }
    
    
    /// to fetch list of all the report reasons.
    ///
    /// - Parameter selectedUser: selected profile to report.
    func showReportReasons(selectedUser:Profile) {
        
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = DeactivateAccountReasonsView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.isViewForReason = true
        videoView.loadDeactivateView()
        videoView.reportReasons = self.profileDeatilVM.reportReasonsArray
        videoView.deactDelegate = self
        window.addSubview(videoView)
    }
    
    
    func scrollToCell(){
        let window = UIApplication.shared.keyWindow!
        let x = window.frame.size.width * CGFloat(profileDeatilVM.profileDetails.currentPhotoIndex)
        scrollViewForImages.contentOffset.x = x
        if profileDeatilVM.profileDetails.hasProfileVideo == 1 {
            stopVideoPlay()
            if x == 0 {
                self.playBtn.isHidden = false
            }else {
                self.playBtn.isHidden = true
            }
        }else {
            self.playBtn.isHidden = true
        }
    }
    
}

// MARK: - methods
extension ProfileDetailVC {
    
    /// method to open activity controller.
    ///
    /// - Parameter profileDetails: p
    func openActivityController(profileDetails:Profile) {
        
        //checking deepLink from database.
        //if not available creating new deep link to share.
        Helper.showProgressIndicator(withMessage: "")
        if(profileDetails.userdeepLink.count > 5) {
            self.openControllerForSocialShare(linkToShare: profileDetails.userdeepLink)
        } else {
            // image to share
            //            self.openControllerForSocialShare(linkToShare: profileDetails.userdeepLink)
            Helper.createDynamicLinkForProfile(profileDetails: profileDetails, completion: { (deepLink,isLinkCreated) in
                Helper.hideProgressIndicator()
                if !isLinkCreated {
                    return
                }
                self.userDeepLinkUrl = deepLink
                self.openControllerForSocialShare(linkToShare: self.userDeepLinkUrl)
            })
        }
    }
    
    
    func openControllerForSocialShare(linkToShare:String) {
        
        
        var linkWithmessage = StringConstants.iFoundAGirl()
        linkWithmessage = linkWithmessage.appending(linkToShare)
        
        // set up activity view controller
        let imageToShare = [linkWithmessage] as [Any]
        
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    func requestLikeApi() {
        let profileVm = ProfileViewModel()
        profileVm.likeProfile(selectedProfile: self.profileDeatilVM.profileDetails)
    }
    
    func requestUnlikeApi() {
        let profileVm = ProfileViewModel()
        profileVm.disLikeProfile(selectedProfile: self.profileDeatilVM.profileDetails)
    }
    
    func requestSuperLikeApi() {
        let profileVm = ProfileViewModel()
        profileVm.superLikeProfile(selectedProfile: self.profileDeatilVM.profileDetails)
    }
    // report user button action
    ///
    /// - Parameter userDetails: contains the profile details.
    func reportedUser() {
        Helper.showAlertWithMessage(withTitle: StringConstants.ThanksForReporting(), message: StringConstants.YourFeedBack(), onViewController: self)
    }
    
    
    
    @objc func tapped(_ sender: UIGestureRecognizer){
        print("tapped")
        let locPoint = sender.location(in:sender.view)
        
        if(locPoint.x > self.view.frame.size.width/2) {
            // show next image
            if(self.profileDeatilVM.profileDetails.allMedia.count > 1) {
                if(self.profileDeatilVM.profileDetails.currentPhotoIndex+1 != self.profileDeatilVM.profileDetails.allMedia.count) {
                    self.profileDeatilVM.profileDetails.currentPhotoIndex = self.profileDeatilVM.profileDetails.currentPhotoIndex+1
                    // let url = URL(string: self.profileDeatilVM.profileDetails.allMedia[profileDeatilVM.profileDetails.currentPhotoIndex])
                    self.scrollToCell()
                    //   profilePic.kf.setImage(with:url, placeholder:UIImage.init(named:"02m"))
                    profileDeatilVM.firstImage = profileDeatilVM.firstImage + 1
                    photoIndicatorCollection.reloadData()
                } else {
                    scrollViewForImages.shakeView()
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                }
            } else {
                scrollViewForImages.shakeView()
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            }
        } else {
            // show previous image
            if(self.profileDeatilVM.profileDetails.allMedia.count != 0) {
                if(self.profileDeatilVM.profileDetails.currentPhotoIndex != 0) {
                    
                    self.profileDeatilVM.profileDetails.currentPhotoIndex = self.profileDeatilVM.profileDetails.currentPhotoIndex-1
                    
                    profileDeatilVM.firstImage = profileDeatilVM.firstImage - 1
                    self.scrollToCell()
                    photoIndicatorCollection.reloadData()
                } else {
                    scrollViewForImages.shakeView()
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                }
            } else {
                scrollViewForImages.shakeView()
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            }
        }
    }
    
    
}

extension ProfileDetailVC {
    
    
    
    /// reporting user.
    ///
    /// - Parameters:
    ///   - reason: reson for reporting
    ///   - userId: user id to report
    func reportUserForReason(reason:String,userId:String) {
        let params = [likeProfileParams.targetUserId:userId,
                      likeProfileParams.message:"reporting",
                      likeProfileParams.reason:reason]
        
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        
        API.requestPOSTURL(serviceName: APINAMES.reportUser,
                           withStaticAccessToken:false,
                           params: params,
                           success: { (response) in
                            Helper.hideProgressIndicator()
                            if (response.null != nil){
                                return
                            }
                            
                            let statuscode = response.dictionaryObject!["code"] as! Int
                            switch (statuscode) {
                            case 200:
                                self.reportedUser()
                            case 401:
                                Helper.changeRootVcInVaildToken()
                            default:
                                self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: response.dictionaryObject![LoginResponse.errorMessage] as! String)
                            }
                            
        },
                           failure: { (Error) in
                            
                            Helper.hideProgressIndicator()
                            
                            self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: Error.localizedDescription)
                            Helper.hideProgressIndicator()
        })
        
    }
    
    
}


extension ProfileDetailVC :UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       
        if scrollView == scrollViewForImages {
            if profileDeatilVM.profileDetails.hasProfileVideo == 1 {
    
                if scrollView.contentOffset.x <= 180{
                    self.playBtn.isHidden = false
                }else {
                    self.playBtn.isHidden = true
                }
            }else {
                self.playBtn.isHidden = true
                stopVideoPlay()
            }
            
        }else {
            if scrollView == mainTableView {
                let movedOffset: CGFloat = scrollView.contentOffset.y
                let pageWidth: CGFloat = -50          //171 - 64
                let ratio: CGFloat = (movedOffset) / (pageWidth)
                if movedOffset >= -50 {
                    //if movedOffset >= 20 {
                    titleHeader.isHidden = false
                    self.titleLabel.textColor = Colors.PrimaryText       //.withAlphaComponent(ratio)
                    topBackBtn.isHidden = false
                    titleHeader.backgroundColor = Colors.SecondBaseColor
                    if  ratio <= 1 && ratio >= 0{
                        self.titleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle))           //*ratio)
                    }
                    if movedOffset > pageWidth{
                        self.titleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle))
                    }
                    Helper.setShadow(sender: titleHeader)
                    // BackButtonImage white back
                    topBackBtn.tintColor = Colors.AppBaseColor
                    self.bottomBtnsView.alpha = 0.8
                    UIView.animate(withDuration: 0.3, animations: {
                        if self.profileFromChat {
                            self.navigationChatBtn.isHidden = true
                        }else {
                            self.navigationChatBtn.isHidden = false
                        }
                        
                    })
                    self.view.updateConstraints()
                    self.view.layoutIfNeeded()
                    // }
                }else {
                    
                    self.titleLabel.textColor = UIColor.clear
                    titleHeader.backgroundColor = UIColor.clear
                    topBackBtn.isHidden = false
                    
                    Helper.removeShadow(sender: titleHeader)
                    topBackBtn.tintColor = UIColor.white
                    UIView.animate(withDuration: 0.3, animations: {
                        self.navigationChatBtn.isHidden = true
                    })
                    self.view.updateConstraints()
                    self.view.layoutIfNeeded()
                }
                updateHeaderView()
                
            }
        }
    }
    
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.profileDeatilVM.profileDetails.currentPhotoIndex  = Int(pageNumber)
        photoIndicatorCollection.reloadData()
        if scrollView == scrollViewForImages {
            if profileDeatilVM.profileDetails.hasProfileVideo == 1 {
                if scrollView.contentOffset.x == 0 {
                    self.playBtn.isHidden = false
                }else {
                    self.playBtn.isHidden = true
                }
            }else {
                self.playBtn.isHidden = true
            }
        }
        
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        self.bottomBtnsView.alpha = 1
    }
    
    
    
}

extension ProfileDetailVC: deactivateReasonsDelegate {
    
    func userSelectedDoneButton(selectedReason: String) {
        print(selectedReason)
    }
    
    func userSelectedReasonForReport(selectedReason: ReportReason) {
        self.reportUserForReason(reason: selectedReason.nameOfTheReason, userId: profileDeatilVM.profileDetails.userId)
    }
    
}

extension ProfileDetailVC {
    
    
    func didGetResponse(){
        
        self.profileDeatilVM.profileDeatilVM_resp.subscribe(onNext:{ success in
            
            
            switch success {
            case ProfileDeatilVM.ResponseType.getReporReasons:
                break
            case ProfileDeatilVM.ResponseType.blockUser:
                self.userBlocked(userDetails: self.profileDeatilVM.profileDetails, type: StringConstants.Blocked())
                
                break
            case ProfileDeatilVM.ResponseType.errorMsg:
                self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: self.profileDeatilVM.errorMsg)
                break
            case .unblock:
                self.userBlocked(userDetails: self.profileDeatilVM.profileDetails, type: StringConstants.UnBlocked())
                
                break
            case .getProfileById:
               // if self.profileDeatilVM.profileDetails.instaGramProfileId.count != 0 { // &&
                   // if self.instagramVM.isUserHasAuthToken().0 {
                        self.instagramVM.otherUserId = self.profileDeatilVM.profileDetails.instaGramProfileId
                        self.instagramVM.instaProfile.updateTokenId(instaId: self.profileDeatilVM.profileDetails.instaGramProfileId, instaAuth: self.profileDeatilVM.profileDetails.instaGramToken)
                        //   self.instagramVM.getOtherUserMedia()
                        self.instagramVM.getUserMedia()
                  //  }
                    
               // }
                self.userMoments = self.profileDeatilVM.profileDetails.moments
                self.titleLabel.text = self.profileDeatilVM.profileDetails.firstName
                self.updateBottomBtnsView()
                self.headerView.isHidden = false
                self.mainTableView.reloadData()
                self.updateScrollView()
                self.updateImagesHeight()
                break
            }
            
        }).disposed(by: self.profileDeatilVM.disposeBag)
    }
    
    
    func updateBottomBtnsView(){
        
        if self.profileDeatilVM.profileDetails.liked == 1 || self.profileDeatilVM.profileDetails.superLiked == 1 ||  self.profileDeatilVM.profileDetails.isMatchedUser == 1{
            self.bottomBtnsView.isHidden = true
        }else {
            UIView.animate(withDuration: 0.3){
                self.bottomBtnsView.isHidden = false
                self.view.updateConstraints()
                self.view.layoutIfNeeded()
            }
        }
        if self.profileDeatilVM.profileDetails.unliked == 1 {
            buttonsLeading.constant = -(bottomBtnsView.frame.size.width/6.5)
            unlikeBtn.isHidden = true
        }else {
            buttonsLeading.constant = 0
            unlikeBtn.isHidden = false
        }
    }
    
    
    
    func didGetInstaResp(){
        self.instagramVM.instagramVM_resp.subscribe(onNext:{ success in
            //            if success.0 == InstagramVM.ResponseType.getUserOtherMedia {     //unComment When fix the bug
            //                self.instagramProfile = success.1
            //                self.mainTableView.reloadData()
            //            }
            
            
            
            if success.0 == InstagramVM.ResponseType.getUserMedia {
                self.instagramProfile = success.1
                self.mainTableView.reloadData()
            }
            
        }).disposed(by: self.instagramVM.disposeBag)
        
    }
    
    
    /// block user  button action.
    ///
    /// - Parameter userDetails: contains profile details.
    func userBlocked(userDetails:Profile,type: String)  {
        Helper.showAlertWithMessage(withTitle: StringConstants.message(), message: String(format: type + " %@",userDetails.firstName), onViewController: self)
        // self.dismiss(animated: true, completion: nil)
    }
}



// MARK: - UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension ProfileDetailVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.profileDeatilVM.profileDetails.allMedia.count == 0 {
            return 1
        }else {
            return self.profileDeatilVM.profileDetails.allMedia.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let Cell:PhotoIndicatorCell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIds.PhotoIndicatorCell, for: indexPath) as! PhotoIndicatorCell
        Cell.indicatorView.tag = indexPath.row
        let viewIn = UIView()
        viewIn.tag = profileDeatilVM.firstImage
        
        if(indexPath.row <= self.profileDeatilVM.profileDetails.currentPhotoIndex) {
            Cell.indicatorView.backgroundColor = UIColor.white
        } else {
            Cell.indicatorView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        }
        
        return Cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size = CGSize.zero
        
        
        let cellSize = Int(collectionView.frame.size.width) - (((profileDeatilVM.profileDetails.allMedia.count)-1)*5)
        var cellWidth = collectionView.frame.size.width
        if profileDeatilVM.profileDetails.allMedia.count != 0 {
            cellWidth = CGFloat((cellSize)/(profileDeatilVM.profileDetails.allMedia.count))
        }
        size.height =  5
        size.width  =  CGFloat(cellWidth)
        
        return size
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
}

extension ProfileDetailVC: VGPlayerDelegate {
    func vgPlayer(_ player: VGPlayer, playerFailed error: VGPlayerError) {
        print(error)
    }
    
    func vgPlayer(_ player: VGPlayer, stateDidChange state: VGPlayerState) {
        print("player State ",state)
        if (player.state == .playing) {
            self.playBtn.isSelected = true
            self.playBtn.setImage(UIImage(), for: .normal)
        } else {
            self.playBtn.isSelected = false
            self.playBtn.setImage(#imageLiteral(resourceName: "PlayButtonImage"), for: .normal)
        }
    }
    
    func vgPlayer(_ player: VGPlayer, bufferStateDidChange state: VGPlayerBufferstate) {
        print("buffer State", state)
    }
    
}

extension ProfileDetailVC: VGPlayerViewDelegate {
    func vgPlayerView(_ playerView: VGPlayerView, willFullscreen fullscreen: Bool) {
        print(fullscreen)
    }
    
    func vgPlayerView(didTappedClose playerView: VGPlayerView) {
        if playerView.isFullScreen {
            playerView.exitFullscreen()
        } else {
            //self.navigationController?.popViewController(animated: true)
        }
    }
    
    func vgPlayerView(didDisplayControl playerView: VGPlayerView) {
        //        UIApplication.shared.setStatusBarHidden(!playerView.isDisplayControl, with: .fade)
    }
}

extension ProfileDetailVC: profileCellDelegate {
    func tappedOnInstaMedia(atIndex: Int, mediaArray: [String]) {
        let imageVc:ImageVideoPreViewController = self.storyboard?.instantiateViewController(withIdentifier: "imageVideoPreViewVc") as! ImageVideoPreViewController
        imageVc.userPhotoArray = mediaArray
        imageVc.isPreviewForImage = true
        imageVc.selectedIndex = atIndex
        self.present(imageVc, animated: true, completion:nil)
    }
}

