//
//  ProfileDetailTVE.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 11/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit


// MARK: - UITableViewDelegate
extension ProfileDetailVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)
     
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        case 1:
            //            if (profileDeatilVM.profileDetails.moments.count) > 0 {
            //                return 40
            //            }
            return 0
        case 2:
            if self.instagramProfile != nil && (instagramProfile?.userMedia.count)! > 0{
                return 40
            }
            return 0
        default:
            if tableView.numberOfSections - 1  == section {
                return 100
            }
            if self.profileDeatilVM.profileDetails.myPreferences.count != 0 {
                if self.profileDeatilVM.profileDetails.myPreferences[section-3].arrayOfSubPreference.count == 0 || (tableView.numberOfSections - 2 == section){
                    return 0
                }
                return 40
            }
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 1{
            if indexPath.row == 0{
                if (profileDeatilVM.profileDetails.moments.count) == 0 {
                    return 0
                }
                return 60
            }else{
                if (profileDeatilVM.profileDetails.moments.count) == 0 {
                    return 0
                }else{
                    if (profileDeatilVM.profileDetails.moments.count) >= 4{
                      return  ((self.view.frame.size.width/3) * 2 )
                    }else{
                       return (self.view.frame.size.width/3)
                    }

                }
                return UITableViewAutomaticDimension
            }
        }
        if indexPath.section == 2 {
            if self.instagramProfile != nil && (instagramProfile?.userMedia.count)! > 0 {
                if (instagramProfile?.userMedia.count)! >= 6 {
                    return ((self.view.frame.size.width/3)*2 + 25)
                }else
                {
                    return (self.view.frame.size.width/3 + 25)
                }
            }
            return UITableViewAutomaticDimension
        }else {
            return UITableViewAutomaticDimension
        }
//        switch indexPath.section {
//        case 0:
//            return UITableViewAutomaticDimension
//        case 1:
//            if indexPath.row == 0{
//                if (profileDeatilVM.profileDetails.moments.count) == 0 {
//                    return 0
//                }
//                return 60
//            }else{
//                if (profileDeatilVM.profileDetails.moments.count) == 0 {
//                    return 0
//                }else{
//                    if (profileDeatilVM.profileDetails.moments.count) >= 4{
//                        return  ((self.view.frame.size.width/3) * 2 )
//                    }else{
//                        return (self.view.frame.size.width/3)
//                    }
//
//                }
//                return UITableViewAutomaticDimension
//            }
//        case 2:
//            if self.instagramProfile != nil && (instagramProfile?.userMedia.count)! > 0 {
//                if (instagramProfile?.userMedia.count)! >= 6 {
//                    return ((self.view.frame.size.width/3)*2 + 25)
//                }else
//                {
//                    return (self.view.frame.size.width/3 + 25)
//                }
//            }
//            return UITableViewAutomaticDimension
////        }else {
////            return UITableViewAutomaticDimension
////        }
//
//       default:
//            if indexPath.row == 0{
//                if (profileDeatilVM.profileDetails.moments.count) == 0 {
//                    return 0
//                }else{
//                     return UITableViewAutomaticDimension
//                }
//
//
//            }
//             return UITableViewAutomaticDimension
//        }
  
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "separatorCell")
        if(tableView.numberOfSections-1 == section) {
            let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 100))
            returnedView.backgroundColor = .clear
            return returnedView
        }
        return cell
    }
}

// MARK: - UITableViewDataSource
extension ProfileDetailVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(profileDeatilVM.dataAvailable) {
            staticRows = 4
            return staticRows + self.profileDeatilVM.profileDetails.myPreferences.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       // if(self.profileDeatilVM.profileDetails.myPreferences.count > 0) {
            
            let totalNumberOfSections = tableView.numberOfSections
            switch section {
            case 0:
                if profileDeatilVM.profileDetails.about.count != 0 {
                    return 2
                }
                return 1
            case 1:
                if (profileDeatilVM.profileDetails.moments.count) == 0 {
                    return 0
                }
                return 2
            case 2:
                if self.instagramProfile != nil && (instagramProfile?.userMedia.count)! > 0 {
                    return 1
                }
                return 0
                
            case tableView.numberOfSections - 1:
               return 1
            default:
             return self.profileDeatilVM.profileDetails.myPreferences[section-3].arrayOfSubPreference.count + 1
            }
//        }else {
//            if section == 0 && profileDeatilVM.profileDetails.about.count != 0{
//                return 2
//            }else {
//                return 1
//            }
//        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIds.ProfileInfoCell, for: indexPath) as! ProfileInfoCell
                cell.updateCell(profileDetails: self.profileDeatilVM.profileDetails)
                if profileFromChat {
                    self.chatBtn.isHidden =  true
                }
                return cell
                
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "profileSecHeader") as! SectionHeaderCell
                cell.updateAbout(data: self.profileDeatilVM.profileDetails)
                cell.separator.isHidden = false
                cell.viewAllButtonOutlet.isHidden = true
                return cell
            }
            
        case 1:
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "profileSecHeader") as! SectionHeaderCell
                cell.headingLabel.text = "Moments"
                cell.headingLabel.font = UIFont (name: CircularAir.Bold, size: 20)
                cell.separator.isHidden = true
                cell.viewAllButtonOutlet.isHidden = false
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MomentsViewCell", for: indexPath) as! MomentsViewCell
                cell.profileModel = self.profileDeatilVM.profileDetails
                cell.isFromMyProfile = false
                self.momentViewCell = cell
                cell.profileMoments = self.profileDeatilVM.profileDetails.moments
                cell.update(self)
                if self.profileDeatilVM.profileDetails.moments.count >= 4 {
                    cell.collectionViewHeight.constant = (cell.momentCollectionView.frame.size.width/3) * 2
                }else{
                    cell.collectionViewHeight.constant = cell.momentCollectionView.frame.size.width/3
                    
                }
                return cell
            }
            
        case 2:
          //  if self.instagramProfile != nil && (instagramProfile?.userMedia.count)! > 0 && indexPath.section == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCollectionCell", for: indexPath) as! ProfileCollectionCell
                cell.profileDelegate = self
                if self.instagramProfile != nil {
                    cell.updateInstaMedia(userInstaMedia: self.instagramProfile!)
                    if (instagramProfile?.userMedia.count)! >= 6 {
                        cell.collectionViewHight.constant = (cell.imageCollectionView.frame.size.width/3) * 2
                    }else {
                        cell.collectionViewHight.constant = cell.imageCollectionView.frame.size.width/3
                    }
                }
                if self.profileDeatilVM.profileDetails.instaGramProfileId.count != 0 {  //other user
                    if self.instagramVM.isUserHasAuthToken().0 { // my profile
                        cell.shareLabel.text = ""
                        cell.connectBtnHeight.constant = 0
                    }else {
                        cell.connectBtnHeight.constant = 40
                        cell.shareLabel.text = StringConstants.shareYourInstaTo()
                    }
                }
                return cell
                
           // }
            
        default: //2,3,4 preferences

            if indexPath.section == tableView.numberOfSections - 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIds.ProfileShareCell, for: indexPath) as! ProfileShareCell
                let numOfsections = tableView.numberOfSections
                if numOfsections == 2 {
                    cell.separator.isHidden = true
                }else {
                    cell.separator.isHidden = false
                }
                return cell
            }
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "profileSecHeader") as! SectionHeaderCell
                cell.updateHeader(title: self.profileDeatilVM.profileDetails.myPreferences[indexPath.section-(staticRows-1)].title)
                cell.separator.isHidden = true
                cell.viewAllButtonOutlet.isHidden = true
                return cell
            }
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIds.ProfileOtherInfoCell, for: indexPath) as! ProfileOtherInfoCell
            
            
            let selectedPrefTitle = self.profileDeatilVM.profileDetails.myPreferences[indexPath.section-(staticRows-1)].arrayOfSubPreference[indexPath.row-1]
            var valueForPref = "NA"
            var setAttribute = false
            let valuesCount = self.profileDeatilVM.profileDetails.myPreferences[indexPath.section-(staticRows-1)].arrayOfSubPreference[indexPath.row-1].selectedValues.count
            
            if valuesCount>0 {
                valueForPref = self.profileDeatilVM.profileDetails.myPreferences[indexPath.section-(staticRows-1)].arrayOfSubPreference[indexPath.row-1].selectedValues[0]
            }
            if valuesCount > 1 && self.profileDeatilVM.profileDetails.myPreferences[indexPath.section-(staticRows-1)].arrayOfSubPreference[indexPath.row-1].prefTypeId != 5{
                valueForPref = valueForPref+"+\(valuesCount-1)"
                setAttribute = true
            }
            
            cell.updateVitals(option:selectedPrefTitle.prefSubTitle,selectedOp:valueForPref)
            if setAttribute {
                cell.selectedLabel.attributedText = Helper.getAttributedText(text: valueForPref, subText: "+\(valuesCount-1)", color: Colors.blueColor)
            }
            return cell
            
        }
    }
}

