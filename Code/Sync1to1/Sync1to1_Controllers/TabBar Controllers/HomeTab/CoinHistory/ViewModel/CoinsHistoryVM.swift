//
//  CoinsHistoryVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 01/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class CoinsHistoryVM: NSObject {
    
    let disposeBag = DisposeBag()
    let coinsHistoryVM_resp = PublishSubject<responceType>()
    
    //coinHistory
    var allCoins = [CoinsHistoryModel]()
    var coinsIn = [CoinsHistoryModel]()
    var coinsOut = [CoinsHistoryModel]()
    
    var coinHistory = Helper.getCoinHistory()
    
    var coinBalance = CoinBalanceModel(data: [:])
    
    
    enum responceType:Int {
        case getCoinBalance = 0
        case coinHistory = 1
    }
    
    func getCoinHistory(){
        
        let apiCalls = CoinsAPICalls()
        
        apiCalls.requestForGetCoinHistory()
        apiCalls.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                self.handleReportReasonsResponse(response: response, type: .coinHistory)
            }, onError: {error in
                self.coinsHistoryVM_resp.onError(error)
            })
            .disposed(by:disposeBag)
        
        
    }
    
    
    /// get coin balance
    func getCoinBalance(){
        
        let apiCalls = CoinsAPICalls()
        
        apiCalls.requestForGetCoinBalance()
        apiCalls.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                self.handleReportReasonsResponse(response: response, type: .getCoinBalance)
            }, onError: {error in
                self.coinsHistoryVM_resp.onError(error)
            })
            .disposed(by:disposeBag)
        
    }
    
    
    /// method for handling matches api response.
    ///
    /// - Parameter response: contains api response and status code.
    func handleReportReasonsResponse(response:ResponseModel,type: responceType) {
        
        switch (response.statusCode) {
        case 200:
            self.coinHistory.removeAll()
            if type == .coinHistory {
                
                var tempAllCoins = [CoinsHistoryModel]()
                var tempCoinIn = [CoinsHistoryModel]()
                var tempCoinOut = [CoinsHistoryModel]()
                
                if let data = response.response[ApiResponseKeys.data] as? [String:Any] {
                    Helper.saveCoinHistory(data: data)
                    
                    if let allcoins = data[ApiResponseKeys.AllCoins] as? [Any] {
                        for each in allcoins {
                            tempAllCoins.append(CoinsHistoryModel.init(data:each as! [String : Any]))
                        }
                        self.allCoins = tempAllCoins
                      self.coinHistory.append(tempAllCoins)
                    }
                    
                    if let coinsIn = data[ApiResponseKeys.CoinIn] as? [Any] {
                        for each in coinsIn {
                            tempCoinIn.append(CoinsHistoryModel.init(data:each as! [String : Any]))
                        }
                        self.coinsIn = tempCoinIn
                        self.coinHistory.append(tempCoinIn)
                    }
                    
                    if let coinsOut = data[ApiResponseKeys.CoinOut] as? [Any] {
                        for each in coinsOut {
                            tempCoinOut.append(CoinsHistoryModel.init(data:each as! [String : Any]))
                        }
                        self.coinsOut = tempCoinOut
                        self.coinHistory.append(tempCoinOut)
                    }
                }
                
                self.coinHistory.insert(coinsOut, at: 1)
                self.coinHistory.insert(coinsIn, at: 2)
                self.coinHistory.remove(at: 3)
                self.coinHistory.remove(at: 3)
                
                self.coinsHistoryVM_resp.onNext(.coinHistory)
                
            }
            else {
                //get Balance
                if let data = response.response[ApiResponseKeys.data] as? [String:Any] {
                    self.coinBalance = CoinBalanceModel.init(data: data)
                }
                
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "updateCoinsBal"), object: self, userInfo:[:])
                self.coinsHistoryVM_resp.onNext(type)
                
            }
        default: break
            
        }
        Helper.hideProgressIndicator()
    }
 
}
