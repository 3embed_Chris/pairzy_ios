//
//  CoinHistoryCollectionCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 31/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class CoinHistoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var mainTableView: UITableView!
    
    @IBOutlet weak var noContentView: UIView!
    @IBOutlet weak var emptyScreenTitle: UILabel!
    @IBOutlet weak var emptySubTitle: UILabel!
    @IBOutlet weak var defaultIcon: UIImageView!
    
    var currentPage = 0
    var coinHistory = [CoinsHistoryModel]()
    
    let coinHistoryVM = CoinsHistoryVM()
    var refreshControl: UIRefreshControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }
    
    
    /// updates tableView
    ///
    /// - Parameters:
    ///   - currentScreen: is of type Integer 0: All ,1: coin-in , 2: coin-out screen
    ///   - coinHist: is of array off coinHistoryModel
    func updateCell(currentScreen: Int,coinHist:[CoinsHistoryModel]){
        self.currentPage = currentScreen
        self.coinHistory = coinHist
        mainTableView.reloadData()
    }
    
    func initialSetup(){
        emptyScreenTitle.text = StringConstants.recentTrans()
        emptySubTitle.text = StringConstants.notDoneTransaction()

        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ProspectCollectionCell.refresh), for: UIControlEvents.valueChanged)
        mainTableView.refreshControl = self.refreshControl
        refreshControl.tintColor = Colors.AppBaseColor
    }
    
    @objc func refresh() {
        
        coinHistoryVM.getCoinHistory()
        coinHistoryVM.getCoinBalance()
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            self.refreshControl.endRefreshing()
        }
    }
    
}
extension CoinHistoryCollectionCell: UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.coinHistory.count == 0 {
            mainTableView.backgroundColor = UIColor.clear
        }
        else {
            mainTableView.backgroundColor = Colors.ScreenBackground
        }
       
        return coinHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CoinHistoryCell", for: indexPath) as! CoinHistoryCell
        
        cell.updateCell(currentScreen: self.currentPage,coinHist: self.coinHistory[indexPath.row])
        
        return cell
    }
    
}
