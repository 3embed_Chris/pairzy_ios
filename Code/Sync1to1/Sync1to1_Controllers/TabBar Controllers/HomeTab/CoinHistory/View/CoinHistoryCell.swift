//
//  CoinHistoryCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 31/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class CoinHistoryCell: UITableViewCell {
    
    @IBOutlet weak var inOutCoinIcon: UIImageView!
    @IBOutlet weak var mainTitle: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //  1 - credit, 2- debit, 3-purchase
    
    func updateCell(currentScreen:Int,coinHist: CoinsHistoryModel){
        
        
        switch currentScreen {
        //All
        case 0:
           if coinHist.txnTypeCode == 2 {
                inOutCoinIcon.image = #imageLiteral(resourceName: "coin_out")
                //mainTitle.text = "Spent \(coinHist.coinAmount) Coins on \(coinHist.trigger)"
              mainTitle.text = coinHist.trigger
            }else {
                inOutCoinIcon.image = #imageLiteral(resourceName: "coin_in")
               // mainTitle.text = "Added \(coinHist.coinAmount) Coins to wallet"
             mainTitle.text = coinHist.trigger
            }
            
            subTitle.text = coinHist.userPurchaseTime
            //amount.text = String(coinHist.coinClosingBalance)
             amount.text = String(coinHist.coinAmount)
            break
      //spent
        case 1:
           self.inOutCoinIcon.image = #imageLiteral(resourceName: "coin_out")
            //mainTitle.text = "spent \(coinHist.coinAmount) Coins on \(coinHist.trigger)"
            mainTitle.text = coinHist.trigger
            subTitle.text = coinHist.userPurchaseTime
           // amount.text = String(coinHist.coinClosingBalance)
           amount.text = String(coinHist.coinAmount)
            break
        //Bought
        default:
            inOutCoinIcon.image = #imageLiteral(resourceName: "coin_in")
           // mainTitle.text = "Added \(coinHist.coinAmount) Coins to wallet"
            mainTitle.text = coinHist.trigger
            subTitle.text = coinHist.userPurchaseTime
           // amount.text = String(coinHist.coinClosingBalance)
            amount.text = String(coinHist.coinAmount)
            break
          
        }
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
