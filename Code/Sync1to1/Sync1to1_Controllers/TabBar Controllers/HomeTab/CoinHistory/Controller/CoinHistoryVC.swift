//
//  CoinHistoryVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 31/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class CoinHistoryVC: UIViewController {
    
   
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var coinInBtn: UIButton!
    @IBOutlet weak var coinOutBtn: UIButton!
    @IBOutlet weak var balanceAmount: UILabel!
   // @IBOutlet weak var addCoinsBtn: UIButton!
    @IBOutlet weak var tabsSeparator: UIView!
    @IBOutlet weak var botomIndicatorView: UIView!
    @IBOutlet weak var movableViewLeading: NSLayoutConstraint!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleHeader: UIView!
 
    let coinHistoryVM = CoinsHistoryVM()
    var currentPage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        coinInBtn.setTitle(StringConstants.spent(), for: .normal)
        allBtn.setTitle(StringConstants.all(), for: .normal)
        coinOutBtn.setTitle(StringConstants.bought(), for: .normal)
        balanceAmount.text = StringConstants.recentTrans()
        Helper.setShadow(sender: botomIndicatorView)
        coinHistoryVM.getCoinHistory()
        balanceAmount.textColor = Colors.AppBaseColor
        didGetRespose()
        botomIndicatorView.backgroundColor = Colors.AppBaseColor
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        coinHistoryVM.getCoinHistory()
    //   coinHistoryVM.getCoinBalance()
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark: Button Actions
    
    
    @IBAction func addCoinsAction(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIds.coinHistToAddCoins, sender: self)
    }
    
    
    @IBAction func allTabAction(_ sender: Any) {
        currentPage = 0
        let screenSize = NSInteger(self.view.frame.size.width)*currentPage
        self.mainCollectionView.setContentOffset(CGPoint(x:screenSize,y:0), animated:true)
        mainCollectionView.reloadData()
        
    }
    
    @IBAction func coinInTabAction(_ sender: Any) {
        currentPage = 1
        let screenSize = NSInteger(self.view.frame.size.width)*currentPage
        self.mainCollectionView.setContentOffset(CGPoint(x:screenSize,y:0), animated:true)
        mainCollectionView.reloadData()
    }
    
    @IBAction func coinOutTabAction(_ sender: Any) {
        
        currentPage = 2
        let screenSize = NSInteger(self.view.frame.size.width)*currentPage
        self.mainCollectionView.setContentOffset(CGPoint(x:screenSize,y:0), animated:true)
        mainCollectionView.reloadData()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
     self.navigationController?.popViewController(animated: true)
    //     self.dismiss(animated: true, completion: nil)
        
    }
    
    //Mark: methods
    func updateBalance(){
       // balanceAmount.text = String(coinHistoryVM.coinBalance.coin)+" C"
        
    }
    
}


extension CoinHistoryVC {
    
    
    
}

extension CoinHistoryVC {
    
    func didGetRespose(){
        self.coinHistoryVM.coinsHistoryVM_resp.subscribe(onNext: { success in
            
            switch success {
            case CoinsHistoryVM.responceType.coinHistory:
                break
            case CoinsHistoryVM.responceType.getCoinBalance:
                self.updateBalance()
                break
            }
      self.mainCollectionView.reloadData()
        }).disposed(by: coinHistoryVM.disposeBag)
        
    }
    
}




// MARK: - UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
extension CoinHistoryVC: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CoinHistoryCollectionCell", for: indexPath) as! CoinHistoryCollectionCell
        if  coinHistoryVM.coinHistory.count != 0 {
            cell.updateCell(currentScreen: indexPath.section,coinHist: coinHistoryVM.coinHistory[indexPath.section])
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize.zero
        let cellWidth = UIScreen.main.bounds.width
        let cellHight = collectionView.frame.size.height
        size.height =  cellHight
        size.width  =  cellWidth
        return size
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

//MARK: - ScrollView Delegates
extension CoinHistoryVC :UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        if scrollView == self.mainCollectionView {
            let scrollContentOffset = self.mainCollectionView.contentOffset
            self.movableViewLeading.constant = scrollContentOffset.x/3
            //  self.updateButtonState()
        
            
            
        }
        
        
        //        let movedOffset: CGFloat = scrollView.contentOffset.y
        //        if scrollView == mainTableView {
        //            self.view.endEditing(true)
        //            let pageWidth: CGFloat = 350          //171 - 64
        //            let ratio: CGFloat = (movedOffset) / (pageWidth)
        //            if ratio > 0 {
        //                self.titleLabel.textColor = Colors.PrimaryText.withAlphaComponent(ratio)
        //
        //                titleHeader.backgroundColor = Helper.getUIColor(color: "#ffffff").withAlphaComponent(ratio)
        //                Helper.setShadow(sender: titleHeader)
        //            }else {
        //                self.titleLabel.textColor = UIColor.clear
        //                titleHeader.backgroundColor = UIColor.clear
        //                Helper.removeShadow(sender: titleHeader)
        //            }
        //        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == mainCollectionView {
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            currentPage = Int(pageNumber)
            let indexSets = IndexSet(integer: currentPage)
            mainCollectionView.reloadSections(indexSets)

        }
        
    }
    
}



