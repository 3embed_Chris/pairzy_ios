 //
//  AddCoinsVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 01/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AddCoinsVM: NSObject {
    
    let disposeBag = DisposeBag()
    let addCoinsVM_resp = PublishSubject<respType>()
    var coinPlans = Helper.getCoinPlans()
    
    var selectedCoinPlan:CoinsPlanModel? = nil
    var errorMsg = ""
    
    enum respType:Int{
        case getCoinBalance = 0
        case getCoinPlans = 1
        case postCoinPlan = 2
        case error = 3
    }
    
    
    /// get coin balance
    func getCoinBalance(){
        
        let apiCalls = CoinsAPICalls()
        
        apiCalls.requestForGetCoinBalance()
        apiCalls.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                self.handleAPIResponse(response: response, type: .getCoinBalance)
               // UserDefaults.standard.set(self.coinBalance.coin, forKey: "coinBalance")
            }, onError: {error in
                self.errorMsg = error.localizedDescription
                self.addCoinsVM_resp.onError(error)
            })
            .disposed(by:disposeBag)
    }
    
    
    
    /// get coin plans
    func getCoinPlans(){
      
        let apiCalls = CoinsAPICalls()
        apiCalls.requestForGetCoinPlans()
        
        apiCalls.subject_response
            .subscribe(onNext: {response in
                
                self.handleAPIResponse(response: response, type: .getCoinPlans)
            }, onError: {error in
                self.errorMsg = error.localizedDescription
                self.addCoinsVM_resp.onError(error)
            })
            .disposed(by:disposeBag)
        
    }
    
    
    
    /// post selected coin plan
    func postCoinPlan(){
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
       var params:[String:Any] = [:]
        guard selectedCoinPlan != nil else {
            return
        }
         let date:Date = Date()
         let dateString = CalenderUtility.getDateTimeForAddCoin(fromDate: date)
            params = [ServiceInfo.PaymentGatewayTxnId: selectedCoinPlan!.actualId,
                          ServiceInfo.Trigger: "Coin purchase",
                          ServiceInfo.PaymentType: "card",
                          ServiceInfo.PlanId: selectedCoinPlan!.coinPlanId,
                          ServiceInfo.UserPurchaseTime: dateString] as [String:Any]
      
        let apiCalls = CoinsAPICalls()
        apiCalls.requestToPostCoinPlan(data: params)
        
        apiCalls.subject_response
            .subscribe(onNext: {response in
                 Helper.hideProgressIndicator()
                self.handleAPIResponse(response: response, type: .postCoinPlan)
            }, onError: {error in
                Helper.hideProgressIndicator()
                 self.errorMsg = error.localizedDescription
                self.addCoinsVM_resp.onError(error)
            })
            .disposed(by:disposeBag)
        
    }
    
    
    
    /// method for handling matches api response.
    ///
    /// - Parameter response: contains api response and status code.
    func handleAPIResponse(response:ResponseModel,type:respType) {
        
        switch (response.statusCode) {
        case 200:
            
            switch type {
            case .getCoinPlans:
                
                var coinsPlan = [CoinsPlanModel]()
                if let coinPlans = response.response[ApiResponseKeys.data] as? [Any] {
                    Helper.saveCoinPlans(data: coinPlans)
                    for each in coinPlans {
                        coinsPlan.append(CoinsPlanModel.init(data:each as! [String : Any]))
                    }
                }
                self.coinPlans = coinsPlan
                self.addCoinsVM_resp.onNext(type)
                break
            case .getCoinBalance:
                if let data = response.response[ApiResponseKeys.data] as? [String:Any] {
                    //self.coinBalance = CoinBalanceModel.init(data: data)
                    if let coins = data[ApiResponseKeys.Coins] as? [String:Any] {
                        if let coin = coins[ApiResponseKeys.coin] as? Int {
                            Helper.saveCoinBalance(coinBalance: coin)
                        }
                    }
                }
                self.addCoinsVM_resp.onNext(type)
                break
                
            case .postCoinPlan:
                if let data = response.response[ApiResponseKeys.data] as? [String:Any] {
                    if let coinBal = data["coinBalance"] as? [String:Any] {
                        if let coin = coinBal["Coin"] as? Int {
                            Helper.saveCoinBalance(coinBalance: coin)
                        }
                    }
                    
                    // self.coinBalance = CoinBalanceModel.init(data: data)
                }
                
                self.addCoinsVM_resp.onNext(type)
                break
            case .error:
                break
            }
        default:
            self.errorMsg = response.response["message"] as! String
            Helper.hideProgressIndicator()
            self.addCoinsVM_resp.onNext(.error)
            break
        }
    }
    
}
