//
//  AddCoinsVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 31/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import KRProgressHUD
import GoogleMobileAds

class AddCoinsVC: UIViewController {
    
    @IBOutlet weak var coinBalanceLabel: UILabel!
    @IBOutlet weak var balance: UILabel!
    @IBOutlet weak var earnFreeCoinsBtn: UIButton!
    @IBOutlet weak var recentTransactBtn: UIButton!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var topBackBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleHeader: UIView!
    @IBOutlet weak var earnFreeCoinsLable: UILabel!
    @IBOutlet weak var noContentView: UIView!
    @IBOutlet weak var emptyScreenTitle: UILabel!
    @IBOutlet weak var emptySubTitle: UILabel!
    @IBOutlet weak var defaultIcon: UIImageView!
    
    let addCoinsVM = AddCoinsVM()
    let purchaseVM = AppStoreKitHelper()
    
    var refreshControl: UIRefreshControl!
    let balanceIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .white)
    let indicator = UIActivityIndicatorView.init(activityIndicatorStyle: .white)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //addCoinsVM.getCoinBalance()
        didGetResponse()
        didGetPlanResponse()
        initialSetup()
        coinBalanceLabel.text = StringConstants.coinBalance()
        Helper.addDefaultNavigationGesture(VC:self)
         NotificationCenter.default.addObserver(self, selector: #selector(updateNewBalance), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForRefreshCoins), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(updateEarnCoinButtonState), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForNewADAvailable), object: nil)
        updateEarnCoinButtonState()
        //purchaseVM.clearIncompleteTansact()
    }
    
    @objc func updateNewBalance() {
        if let bal = Helper.getCoinBalance() as? Int {
            balance.text = String(bal) + " " + StringConstants.coins()
        }
    }

 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.titleLabel.textColor = UIColor.clear
        titleHeader.backgroundColor = UIColor.clear
        
        topBackBtn.isHidden = true
        titleHeader.isHidden = true
   
        indicator.color = Colors.SeparatorDark
        indicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        indicator.center = CGPoint(x: mainTableView.bounds.size.width/2 - 20, y: mainTableView.bounds.size.height/2+60)
        indicator.startAnimating()
        mainTableView.addSubview(indicator)
        
        
        balanceIndicator.color = Colors.SeparatorDark
        balanceIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        balanceIndicator.center = balance.center
        balanceIndicator.startAnimating()
       // balance.addSubview(balanceIndicator)
        if let bal = Helper.getCoinBalance() as? Int {
            balance.text = String(bal) + " " + StringConstants.coins()
        }
        
        addCoinsVM.getCoinBalance()
        addCoinsVM.getCoinPlans()
        addObserver()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        removeObserver()
        self.purchaseVM.removeObserver()
    }
    
    @objc func updateEarnCoinButtonState() {
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.newAdAvailable() {
            self.earnFreeCoinsBtn.isEnabled = true
        } else {
            self.earnFreeCoinsBtn.isEnabled = false
        }
    }
  
    @IBAction func earnFreeCoinsButtonAction(_ sender: Any) {
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if appDel.openNewVideoAdd() {
            //ads available.
        }
    }
    
    
    @IBAction func recentTransactBtnAction(_ sender: Any) {
        performSegue(withIdentifier: SegueIds.addCoinsToCoinHist, sender: self)
    }
 
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    /// creates an viewcontroller object and performs push operation
    func gotoNextVC(){
        let controller = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "coinHistory") as! CoinHistoryVC
        controller.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    /// updates the current balance
    func updateBalance(){
        let coins = Helper.getCoinBalance()
        balance.text = String(coins) + " " + StringConstants.coins()
        
    }
    
    func updateSelectedCoins(){
        addCoinsVM.postCoinPlan()
    }
    
    
    func initialSetup(){
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ProspectCollectionCell.refresh), for: UIControlEvents.valueChanged)
        mainTableView.refreshControl = self.refreshControl
        refreshControl.tintColor = Colors.AppBaseColor
        coinBalanceLabel.textColor = Colors.AppBaseColor
       
    }
    
    @objc func refresh() {
        
        indicator.stopAnimating()
        addCoinsVM.getCoinBalance()
        addCoinsVM.getCoinPlans()
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            self.refreshControl.endRefreshing()
        }
    }
    
    
}

// MARK: - Observer for ViewModel
extension AddCoinsVC {
    
    
    /// observableFor viewModel
    func didGetResponse(){
        self.addCoinsVM.addCoinsVM_resp.subscribe(onNext:{ success in
            
            switch success {
            case AddCoinsVM.respType.getCoinBalance:
                self.balanceIndicator.stopAnimating()
                self.indicator.removeFromSuperview()
                self.updateBalance()
                break
            case AddCoinsVM.respType.getCoinPlans:
                break
            case AddCoinsVM.respType.postCoinPlan:
                self.updateBalance()
                KRProgressHUD.dismiss() {
                     self.showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coin_add_popup"), mainTitle: StringConstants.congrats(), subTitle: StringConstants.coisAddedTo(), buttnTitle: StringConstants.done(), dontShowHide: true)
                }
                self.navigationController?.popViewController(animated: true)
                break
            case .error:
                Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: self.addCoinsVM.errorMsg, onViewController: self)
                break
            }
            self.indicator.stopAnimating()
            self.indicator.removeFromSuperview()
            self.mainTableView.reloadData()
        }).disposed(by:self.addCoinsVM.disposeBag)
    }
    

    func didGetPlanResponse(){
        self.purchaseVM.purchasePlan_resp.subscribe(onNext:{ success in
            
            switch success {
                
            case AppStoreKitHelper.ResponseType.purchased:
                print("purchased response")
                break
                
            case AppStoreKitHelper.ResponseType.receivedPlans:
                print("purchased plans")
                break
                
            case AppStoreKitHelper.ResponseType.purchaseCoins:
                Helper.showProgressIndicator(withMessage: StringConstants.Loading())
                self.updateSelectedCoins()
                break
            }
            
            Helper.hideProgressIndicator()
        }).disposed(by: purchaseVM.disposeBag)
    }
    
    
    func showNewCoinsPopUp(){
        let window = UIApplication.shared.keyWindow!
        let coinsView = RewardPopup(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        coinsView.loadPopUpView()
         window.addSubview(coinsView)
    }
    
    
    func showCoinsPopupForChat(icon: UIImage, mainTitle: String, subTitle: String, buttnTitle: String, dontShowHide: Bool){
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let coinsView = spendSomeCoinPopUp(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        coinsView.loadPopUpView()
        coinsView.updatePopup(icon: icon, mainTitle: mainTitle, subTitle: subTitle, buttnTitle: buttnTitle, dontShowHide: dontShowHide)
        coinsView.spendSomeCoinDelegate = self
        window.addSubview(coinsView)
    }
    
    
    
}

extension AddCoinsVC:spendSomeCoinPopUpDelegate {
    
    func coinBtnPressed(sender:UIButton) {
    }
}

// MARK: - UITextFieldDelegate
extension AddCoinsVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}



// MARK: - UITableViewDelegate,UITableViewDataSource
extension AddCoinsVC : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if addCoinsVM.coinPlans.count == 0 {
            indicator.startAnimating()
        }else {
            indicator.stopAnimating()
            indicator.removeFromSuperview()
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if(section == 2) {
            //section for earning free coins.
            return 1
        }
        
        return addCoinsVM.coinPlans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier:"buyCoins", for: indexPath) as! MyCurrentLocationCell
            cell.updateBuyCoins()
            return cell
        } else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dollerAmountCell", for: indexPath) as! AmountCell
            cell.updateCellForFreeCoins()
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dollerAmountCell", for: indexPath) as! AmountCell
            let coinPlansFromAppStor = Helper.getCoinPLansFromAppStore()
            if coinPlansFromAppStor.count != 0{
                 cell.updateCell(data:addCoinsVM.coinPlans[indexPath.row],plansFormApps: coinPlansFromAppStor[indexPath.row])
            }else {
                cell.updateCell(data:addCoinsVM.coinPlans[indexPath.row],plansFormApps: InAppPurchaseModel(data: [:]))
            }
           
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            
         return UITableViewAutomaticDimension
            
        }
        else {
            return 66
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 1 {
            let plans = Helper.getCoinPlans()
            addCoinsVM.selectedCoinPlan = plans[indexPath.row]
            let productIDs = [AppConstant.coinsSubId750,AppConstant.coinsSubId1500,AppConstant.coinsSubId2000]
            showAlertToBuyCoins(purchaseID:productIDs[indexPath.row])
        } else if indexPath.section == 2 {
            let appDel = UIApplication.shared.delegate as! AppDelegate
            if appDel.openNewVideoAdd() {
                //ads available.
            } else {
                //no ads are availble.
               Helper.showAlertWithMessage(withTitle: StringConstants.error(), message:StringConstants.noAdsAvailable() , onViewController: self)
            }
        }
    }
    
    func openInAppPurchaseView() {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = InAppPuchasePopupView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.loadInAppPurchaseView()
        window.addSubview(videoView)
    }
    
    func showAlertToBuyCoins(purchaseID:String) {
        
        let alertmsg = String(format:StringConstants.areYouSure(),addCoinsVM.selectedCoinPlan?.planName ?? "")
            //"Are you sure want to purchase " + (addCoinsVM.selectedCoinPlan?.planName)! + " plan?"
        let alertTitle = StringConstants.coinPurchase()
        
        let alert:UIAlertController=UIAlertController(title:alertTitle, message: alertmsg, preferredStyle: UIAlertControllerStyle.alert)
        
        let buyAction = UIAlertAction(title: StringConstants.yes(), style: UIAlertActionStyle.default)
        {
            UIAlertAction in
           // self.updateSelectedCoins()
            self.purchaseVM.purchase(productID:purchaseID)
        }
        
        let cancelAction = UIAlertAction(title: StringConstants.cancel(), style: UIAlertActionStyle.default)
        {
            UIAlertAction in
        }
        
        
        alert.addAction(buyAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
}

// MARK: - UIScrollViewDelegate
extension AddCoinsVC :UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let movedOffset: CGFloat = scrollView.contentOffset.y
        if scrollView == mainTableView {
            self.view.endEditing(true)
            let pageWidth: CGFloat = 66          //171 - 64
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            if ratio > 0 {
                if movedOffset >= 20 {
                    titleHeader.isHidden = false
                    self.titleLabel.textColor = Colors.PrimaryText.withAlphaComponent(ratio)
                    topBackBtn.isHidden = false
                    titleHeader.backgroundColor = Helper.getUIColor(color: "#ffffff")//.withAlphaComponent(ratio)
                    if  ratio <= 1 && ratio >= 0{
                        self.titleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle)*ratio)
                    }
                    if movedOffset > 66 {
                        self.titleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle))
                    }
                    Helper.setShadow(sender: titleHeader)
                    
                }
            }else {
                self.titleLabel.textColor = UIColor.clear
                titleHeader.backgroundColor = UIColor.clear
                topBackBtn.isHidden = true
                titleHeader.isHidden = true
                Helper.removeShadow(sender: titleHeader)
            }
        }
    }
}


//MARK: - Admob ADS DELEGATE via NSNotification.

extension  AddCoinsVC {
    
    func recivedEventsForAds() {
        
    }
}
