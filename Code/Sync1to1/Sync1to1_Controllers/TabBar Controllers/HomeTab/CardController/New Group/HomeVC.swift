//
//  ViewController.swift
//  Koloda
//
//  Created by Eugene Andreyev on 4/23/15.
//  Copyright (c) 2015 Eugene Andreyev. All rights reserved.
//

import UIKit
import Koloda
import RxSwift
import AudioToolbox
import PulsingHalo
import AVFoundation
import StoreKit
import VGPlayer
import AVKit
import GoogleMobileAds
import Kingfisher

enum viewStyle {
    case grid
    case card
}

enum profileActivityStatus {
    case liked
    case unLiked
    case superLiked
}

class HomeVC: UIViewController,preferncesUpdated,profileInterestUpdated,cardVieDelegate{
    
    var audioPlayer: AVAudioPlayer!
    var popUpFor:CoinsPopupFor = .forAll
    
    @IBOutlet weak var kolodaObj: KolodaView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var HomeCollectionView: UICollectionView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var preferencesBtn: UIButton!
    @IBOutlet weak var dollerBtn: UIButton!
    @IBOutlet weak var bottomBtnsHight: NSLayoutConstraint!
    @IBOutlet weak var segmentImage: UIImageView!
    @IBOutlet weak var searchStatusMsg: UILabel!
    @IBOutlet weak var undoButtonOutlet: UIButton!
    @IBOutlet weak var unlikeButtonOutlet: UIButton!
    @IBOutlet weak var boostBtn: UIButton!
    @IBOutlet weak var likeButtonOutlet: UIButton!
    @IBOutlet weak var superLikeButtonOutlet: UIButton!
    @IBOutlet weak var profileImageForRotation: UIImageView!
    @IBOutlet weak var passportBtn: UIButton!
    @IBOutlet weak var pulseView: UIView!
    @IBOutlet weak var boostBtnView: UIView!
    @IBOutlet weak var superLikeBtnView: UIView!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var numberOfViewForBoost: UIButton!
    @IBOutlet weak var boostViewBgImage: UIImageView!
    @IBOutlet weak var numberOfViewForBoostForGrid: UIButton!
    @IBOutlet weak var boostViewBgImageForGrid: UIImageView!
    @IBOutlet weak var collectionBoostBtnView: UIView!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    var offset = 0
    var limit = 20
    var index = 0
    let disposeBag = DisposeBag()
    let userData = Database.shared.fetchResults()
    var coinConfig = Helper.getCoinConfig()
    let profileVm = ProfileViewModel()
    let getDatumPlusVm = GetDatumPlusVM()
    var gridRefreshControl: UIRefreshControl = UIRefreshControl()
    let cardView:CardViewClass = CardViewClass().shared
    var firstImage = 0
    /// matchMakerViewModel is the view model object for matches
    let matchMakerViewModel = MatchMakerVM()
    var profilesArray = [Profile]()
    var cardProfiles = [Profile]()
    var profileForChatSelected = Profile.fetchprofileDataObj(userDataDictionary: [:])
    
    let disposebag = DisposeBag()
    var requestIsInProcess  = false
   // static let sharedInstance = ProfileViewController()
    var animatedFirstTime = false
    var likedOrDislikedProfileCount = 0
    let maximumNumberOfProfilesForADS = 24
    var selectedViewStyle = viewStyle.card
    var products: [String: SKProduct] = [:]
    var lastIndexSelectedToOpenProfile = 0
    var recivedADDFromAdMob = true

    let inAppPurchaseVM = AppStoreKitHelper()
    
    var localplayer : VGPlayer?
    
    var profileFoSuperLikeSelected:Profile = Profile.fetchprofileDataObj(userDataDictionary: [:])
    
    var interstitial: GADInterstitial!
    var halo = PulsingHaloLayer()
    var balance = 0
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        kolodaObj.isPairKoloda = false
        interstitial = createAndLoadInterstitial()
        let appde = UIApplication.shared.delegate as! AppDelegate
        appde.checkForPendingCalls()
        appde.checkForUpdateLocation()
        getDatumPlusVm.getCoinConfig()
        didGetResponse()
        showUserProifleImage()
        kolodaObj.layer.cornerRadius = 10
        dollerBtn.layer.cornerRadius = dollerBtn.frame.size.height/2
        dollerBtn.layer.borderWidth = 1
        dollerBtn.layer.borderColor = Colors.coinBalanceBtnBorder.cgColor
        kolodaObj.dataSource = self
        kolodaObj.delegate = self
       gridRefreshControl.tintColor = Colors.AppBaseColor
        gridRefreshControl.addTarget(self, action: #selector(HomeVC.refresh), for: UIControlEvents.valueChanged)
        HomeCollectionView.addSubview(gridRefreshControl)
        self.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
        self.subscribeForEvents()
        let emptyProfile = Profile.fetchprofileDataObj(userDataDictionary: [:])
        self.requestForReportUsefrReasons(userClicked: false,selectedUser: emptyProfile)
        cardView.isUserInteractionEnabled = true
        passportBtn.setTitleColor(UIColor.white, for: .normal)
        passportBtn.setTitle(StringConstants.DiscoverySetting(), for: .normal)
        setupPulsing()
        prepareAudiPlayer()
        self.inAppPurchaseVM.fetchProducts()
        getDatumPlusVm.getPlans()
        self.getCloudinaryDetails()
    
        //getting chats list.
        let matchesVm = MatchMakerVM()
        matchesVm.requestForMatches()
        
        self.getCoinConfig()
        fetchAllDateDetails()
        updateChatListCount()
    }
    
    func fetchAllDateDetails() {
        let syncDateVm = SyncDateeRequestVM()
        syncDateVm.getNowPrfiles()
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: Admob.interstitialAdUnitId)
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }

    
    func showAds(){
        if interstitial.isReady && !Helper.isSubscribedForPlans(){
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
    }
    
    
    @objc func updateLikeDislikeProfilesCount() {
        likedOrDislikedProfileCount = likedOrDislikedProfileCount + 1
        if(likedOrDislikedProfileCount == maximumNumberOfProfilesForADS) {
            likedOrDislikedProfileCount = 0
            if(recivedADDFromAdMob) {
                showAds()
            }
            
        }
    }
    
    
 
    func addBorderForBoost() {
        let circle = boostBtn!
        
        circle.bounds = CGRect(x: 0,y: 0, width: boostBtn!.frame.size.width, height: boostBtn.frame.size.width);
        circle.frame = CGRect(x: 0,y: 0, width: boostBtn!.frame.size.width, height: boostBtn.frame.size.width);
        
        circle.layoutIfNeeded()
        
        var progressCircle = CAShapeLayer();
        
        let centerPoint = CGPoint (x: circle.bounds.width / 2, y: circle.bounds.width / 2);
        let circleRadius : CGFloat = circle.bounds.width / 2 * 0.83;
        
        let circlePath = UIBezierPath(arcCenter: centerPoint, radius: circleRadius, startAngle: CGFloat(-0.5 * M_PI), endAngle: CGFloat(1.5 * M_PI), clockwise: true    );
        progressCircle = CAShapeLayer ();
        progressCircle.path = circlePath.cgPath;
        progressCircle.strokeColor = Colors.boostColor.cgColor
        progressCircle.fillColor = UIColor.clear.cgColor;
        progressCircle.lineWidth = 3;
        progressCircle.strokeStart = 0;
        progressCircle.strokeEnd = 0;
        circle.layer.addSublayer(progressCircle);
        CATransaction.begin()
        CATransaction.setCompletionBlock({
            //self.addBorderForBoost()
        })
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.toValue = 1.0
        animation.duration = 5.0
        animation.fillMode = kCAFillModeForwards
        animation.isRemovedOnCompletion = true
        
        progressCircle.add(animation, forKey: "ani")
        self.boostBtn.superview?.addSubview(circle)
        CATransaction.commit()
    }
    
    func stopAnimationForBoost() {
        self.boostBtn!.layer.removeAllAnimations()
        self.boostBtn!.superview?.layer.removeAllAnimations()
    }
    
    
    func prepareAudiPlayer() {
//        guard let coinsSound = Bundle.main.url(forResource: "Coin-collect-sound-effect", withExtension: "mp3")else{ return}
        guard let path = Bundle.main.path(forResource: "Coin-collect-sound-effect", ofType: "mp3") else{return}
        guard let url = URL(string: path) else{return}
        do{
//            audioPlayer = try AVAudioPlayer(contentsOf: coinsSound)
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer.prepareToPlay()
        }catch let error{
            print(error.localizedDescription)
        }
    }
    
    @objc func refresh(senderRC: UIRefreshControl) {
        index = 0
        offset = 0
        limit = 20
        self.requestForProfles()
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            senderRC.endRefreshing()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupPulsing()
        kolodaObj.isPairKoloda = false
        profileVm.getCoinBalance()
        index = 0
        offset = 0
        limit = 20
        updateCoinBalance()
        setProfileImage(profileImage: profileImageView)
        setUpProfileImageUI(profileImage: profileImageView)
        self.navigationController?.navigationBar.isHidden = true
        
        if(selectedViewStyle == .grid) {
            self.updateScrollViewFrame(newOffsetPoint: self.view.frame.size.width)
        }else if(selectedViewStyle == .card) {
            startVideoPlay()
        }
    }


    
    /// method will trigger beforecontrollers  view appears.
    ///
    /// - Parameter animated: tells that view appeared or not.
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (!requestIsInProcess) {
            requestIsInProcess = true
        }
        let appde = UIApplication.shared.delegate as! AppDelegate
        appde.checkForUpdateLocation()
        self.refreshProfiles()
        self.inAppPurchaseVM.receiptValidation()
        //isViewIsAppearing = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopVideoPlay()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        halo.position = profileImageForRotation.center
        //   halo.position = view.center
    }
    
    
   @objc func updateCoinBalance(){
        var balance = " 0"
        if let getBal = UserDefaults.standard.object(forKey: "coinBalance") as? Int {
            if getBal > 1000 {
                let floatVal = Float(Float(getBal)/1000.0)
                let inter = floor(floatVal)
                let decimal = floatVal.truncatingRemainder(dividingBy: 1)
                if decimal == 0.0{
                    balance = "\(Int(inter))k"
                }else {
                    balance = "\(Float(Float(getBal)/1000.0))k"
                }
            } else {
                balance = " " + String(getBal)
            }
            dollerBtn.setTitle(balance, for: .normal)
        } else {
            dollerBtn.setTitle(" 0", for: .normal)
        }
    }
    
    func setUpProfileImageUI(profileImage:UIImageView){
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        profileImage.layer.borderWidth = 1
        profileImage.layer.borderColor = UIColor.white.cgColor
    }
    
    func setProfileImage(profileImage:UIImageView){
        if let userDetails = userData.first{
            if let urlString:String = userDetails.profilePictureURL {
                if urlString.count>0 {
                    let url = URL(string: urlString)
                    var imageName = "man_default"
                    if userDetails.gender == "Male" {
                        imageName = "man_default"
                    }else {
                        imageName = "woman_default"
                    }
                    self.profileImageView.kf.setImage(with:url, placeholder:UIImage.init(named:"02m"))
                }else{
                    profileImage.image = #imageLiteral(resourceName: "Splash")
                }
            }
        }
        
    }
    
    func setupPulsing(){
        let layer = PulsingHaloLayer()
        self.searchStatusMsg.text = StringConstants.HomeProfileSearch()
        passportBtn.isHidden = true
        halo = layer
        halo.haloLayerNumber = 3
        self.halo.radius = 180.0
        profileImageForRotation.superview?.layer.insertSublayer(halo, below: profileImageForRotation.layer)
        self.halo.backgroundColor = Colors.AppBaseColor.cgColor
        profileImageForRotation.layer.cornerRadius = profileImageForRotation.frame.size.height/2
        halo.position = profileImageForRotation.center
        halo.start()
    }
    
    func stopVideoPlay() {
        guard kolodaObj != nil else {
            return
        }
        if  kolodaObj.subviews.count > 0  {
            if let selectedCardView = kolodaObj.subviews[kolodaObj.subviews.count-1].viewWithTag(2012) as? CardViewClass {
                if((localplayer) != nil) {
                    localplayer?.pause()
                    selectedCardView.localplayer?.pause()
                }
            }
        }
    }
    
    func startVideoPlay() {
        guard kolodaObj != nil else {
            return
        }
        
        if  kolodaObj.subviews.count > 0  {
            if let selectedCardView = kolodaObj.subviews[kolodaObj.subviews.count-1].viewWithTag(2012) as? CardViewClass {
                if((localplayer) != nil) {
                    if self.cardProfiles[kolodaObj.currentCardIndex].hasProfileVideo == 1{
                        localplayer?.play()
                        selectedCardView.localplayer?.play()
                    }
                   
                }
            }
        }
        
        
    }
    
    func openSelectedProfile(openProfile:Profile) {
        let profileVc = self.storyboard?.instantiateViewController(withIdentifier:StoryBoardIdentifier.profileVc) as! ProfileDetailVC
  
        profileVc.profileDeatilVM.profileDetails = openProfile
        profileVc.profileUpdateDelegate = self
        self.navigationController?.present(profileVc, animated: true, completion:nil)
    }
    
    func openProfileByID(userID:String) {
        
        profileForChatSelected.userId = ""
        
        let profileVc = self.storyboard?.instantiateViewController(withIdentifier:StoryBoardIdentifier.profileVc) as! ProfileDetailVC
        //        profileVc.profileUpdateDelegate = self
        profileVc.profileDeatilVM.dataAvailable = false
        profileVc.profileDeatilVM.showProfileByID = false
        profileVc.profileDeatilVM.profileDetails.userId = userID
        self.navigationController?.present(profileVc, animated: true, completion:nil)
    }
    
    
    func openChatForProfile(profile:Profile) {
        self.profileForChatSelected = profile
        chatButtonTapped()
    }
    
    func profileUpdatedWithStatus(status:profileActivityStatus) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            switch status{
            case .liked:
                print("profile liked")
                
                if(self.selectedViewStyle == .card) {
                    self.likeButtonTapped(selectedButton:self.likeButtonOutlet)
                } else {
                    let fetchCell = self.HomeCollectionView.cellForItem(at: IndexPath(row: self.lastIndexSelectedToOpenProfile, section: 0)) as! HomeCollectionCell
                    fetchCell.likeBtn.sendActions(for: .touchUpInside)
                }
                
            case .unLiked:
                if(self.selectedViewStyle == .card) {
                    self.unlikeButtonTapped(selectedButton:self.unlikeButtonOutlet)
                } else {
                    let fetchCell = self.HomeCollectionView.cellForItem(at: IndexPath(row: self.lastIndexSelectedToOpenProfile, section: 0)) as! HomeCollectionCell
                    fetchCell.closeBtn.sendActions(for: .touchUpInside)
                }
                print("profile unliked")
            case .superLiked:
                if(self.selectedViewStyle == .card) {
                    self.superLikeButtonTapped(selectedButton:self.superLikeButtonOutlet)
                } else {
                    let fetchCell = self.HomeCollectionView.cellForItem(at: IndexPath(row: self.lastIndexSelectedToOpenProfile, section: 0)) as! HomeCollectionCell
                    fetchCell.superLikeBtn.sendActions(for: .touchUpInside)
                }
                print("profile super liked")
            }
        })
    }
    
    func triggerlikebuttonAction() {
        
    }
    
    @IBAction func homeViewAction(_ sender: Any) {
        index = 0
        offset = 0
        limit = 20
        homeBtnSelected()
        startVideoPlay()
    }
    
    @IBAction func groupViewAction(_ sender: Any) {
        
        index = 0
        offset = 0
        limit = 20
        groupBtnSelected()
        stopVideoPlay()
        
    }
    
    // MARK: IBActions
    
    func likeButtonTapped(selectedButton:UIButton) {
        
        updateLikeDislikeProfilesCount()
//        if(Helper.isUserOutOfLikes()) {
//            openInAppPurchaseViewForOutOfLikes()
//        } else {
            if(selectedViewStyle == .card) {
                kolodaObj?.swipe(.right)
            } else {
                self.requestLikeApi(ProfileDetails: self.profilesArray[selectedButton.tag])
                removeProfileFromCards(removeProfile:self.profilesArray[selectedButton.tag])
                removeProfileFromGrid(removeIndex:selectedButton.tag)
            }
            checkHasProfiles(callAPi: true)
        //}
    }
    
    func superLikeButtonTapped(selectedButton:UIButton) {
        
        updateLikeDislikeProfilesCount()
        self.popUpFor = .forSuperLike
        if Helper.isUserHasMinimumCoins(minValue: coinsFor.SuperLike) {
            if Helper.isShowCoinsDeductPopup(popUpFor: .forSuperLike) {
                coinConfig = Helper.getCoinConfig()
                let prifile = profilesArray[selectedButton.tag]
                var temp = ""
                if prifile.gender == "Male" {
                    temp = StringConstants.him()
                }else{
                    temp = StringConstants.her()
                }
                let mainTitle = String(format:StringConstants.spendCoinsToLet(),coinConfig.coinsForSuperLike,prifile.firstName,temp)
                    //"Spend \(coinConfig.coinsForSuperLike) coins to let " + prifile.firstName + " know you really like " + temp + " by super liking"
                let buttoTitle = String(format:StringConstants.iamOkToSpend(),coinConfig.coinsForSuperLike)
                //"I am OK to spend \(coinConfig.coinsForSuperLike) coins"
                
                showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coins"), mainTitle: mainTitle, subTitle: "", buttnTitle: buttoTitle, dontShowHide: true)
            }
            else {
                self.coinBtnPressed(sender: selectedButton)
            }
        } else {
            showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coinEmpty"), mainTitle: StringConstants.CoinsGotOver(), subTitle: StringConstants.NoEnoughCoins() + StringConstants.superLike(), buttnTitle: StringConstants.BuyCoins(), dontShowHide: true)
        }
    }
    
    func openChatForProfile(selectedButton:UIButton) {
        
    }
    
    
    
    func unlikeButtonTapped(selectedButton:UIButton) {
       // stopVideoPlay()
        if(selectedViewStyle == .card) {
            //  self.requestUnlikeApi(ProfileDetails: self.cardProfiles[kolodaObj.currentCardIndex])
            kolodaObj?.swipe(.left)
            
        } else {
            self.requestUnlikeApi(ProfileDetails: self.profilesArray[selectedButton.tag])
            removeProfileFromCards(removeProfile:self.profilesArray[selectedButton.tag])
            removeProfileFromGrid(removeIndex:selectedButton.tag)
        }
        checkHasProfiles(callAPi: true)
        //user can undo this profile. enabling undo button
      //  updateUndoButton(enable:true)
        updateLikeDislikeProfilesCount()

    }
    
    @IBAction func unlikeButtonAction(_ sender: Any) {
        let selectedButton = sender as! UIButton
        unlikeButtonTapped(selectedButton: selectedButton)
    }
 
    @IBAction func superLikeButtonAction(_ sender: Any) {
        self.popUpFor = .forSuperLike
        if Helper.isUserHasMinimumCoins(minValue: coinsFor.SuperLike) {
            if Helper.isShowCoinsDeductPopup(popUpFor: .forSuperLike) {
                coinConfig = Helper.getCoinConfig()
                let btn = sender as! UIButton
                lastIndexSelectedToOpenProfile = btn.tag
                let prifile = profilesArray[btn.tag]
                var temp = ""
                if prifile.gender == "Male" {
                    temp = StringConstants.him()
                }else{
                    temp = StringConstants.her()
                }
                
                let mainTitle = String(format:StringConstants.spendCoinsToLet(),coinConfig.coinsForSuperLike,prifile.firstName,temp)
                    //"Spend \(coinConfig.coinsForSuperLike) coins to let " + prifile.firstName + " know you really like " + temp + " by super liking"
                let buttoTitle = String(format:StringConstants.iamOkToSpend(),coinConfig.coinsForSuperLike)
                //"I am OK to spend \(coinConfig.coinsForSuperLike) coins"
                
                showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coins"), mainTitle: mainTitle, subTitle: "", buttnTitle: buttoTitle, dontShowHide: true)
            }
            else {
                self.coinBtnPressed(sender: sender as! UIButton)
            }
        } else {
            showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coinEmpty"), mainTitle: StringConstants.CoinsGotOver(), subTitle: StringConstants.NoEnoughCoins() + StringConstants.superLike(), buttnTitle: StringConstants.BuyCoins(), dontShowHide: true)
        }
    }
    
    
    @IBAction func viewProfileButtonAction(_ sender: UIButton) {
        
         self.performSegue(withIdentifier: SegueIds.HomeVCToProfileVC, sender: nil)
    }
    
    @IBAction func likeButtonAction(_ sender: Any) {
        let selectedButton = sender as! UIButton
        likeButtonTapped(selectedButton: selectedButton)
    }
    
    func removeProfileFromGrid(removeIndex:Int) {
        self.profilesArray.remove(at: removeIndex)
        let indexpa = IndexPath(row: removeIndex, section: 0)
        self.HomeCollectionView.performBatchUpdates({
            self.HomeCollectionView.deleteItems(at: [indexpa])
        }) { (finished) in
            self.HomeCollectionView.reloadItems(at: self.HomeCollectionView.indexPathsForVisibleItems)
        }
    }
    
    func removeProfileFromCards(removeProfile:Profile) {
        for (index,profile) in self.cardProfiles.enumerated() {
            if(profile.userId ==  removeProfile.userId) {
                self.cardProfiles.remove(at:index)
                kolodaObj.resetCurrentCardIndex()
                break
            }
        }
    }
    func removeProfileFromGridView(removeProfile:Profile) {
        for (index,profile) in self.profilesArray.enumerated() {
            if(profile.userId ==  removeProfile.userId) {
                self.profilesArray.remove(at:index)
                HomeCollectionView.reloadData()
                break
            }
        }
    }
    
    func requestLikeApi(ProfileDetails:Profile) {
        profileVm.likeProfile(selectedProfile: ProfileDetails)
        updateUndoButton(enable:false)
    }
    
    func requestUnlikeApi(ProfileDetails:Profile) {
        profileVm.disLikeProfile(selectedProfile: ProfileDetails)
    }
    
    func requestSuperLikeApi(ProfileDetails:Profile) {
        profileVm.superLikeProfile(selectedProfile: ProfileDetails)
    }
    
    func updateProfileStatus(status:profileActivityStatus) {
        
    }

    func gotoCoinBalanceScreen()
    {
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "addCoin") as! AddCoinsVC
        userBasics.navigationController?.setNavigationBarHidden(true, animated: false)
       // self.navigationController?.pushViewController(userBasics, animated: true)
        self.present(userBasics, animated: true, completion: nil)
}
    
   @IBAction func undoButtonAction(_ sender: Any) {
//        if(Helper.isUserOutOfRewinds()) {
//            openInAppPurchaseViewForOutOfRewinds()
//        } else {
            updateUndoButton(enable: false)
            profileVm.rewindProfile()
        //}
    }
    
    
    func openInAppPurchaseViewForOutOfRewinds() {
        let window = UIApplication.shared.keyWindow!
        let videoView = TimerPopUpView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.popupFor = timerPopType.outOfRewinds
        videoView.loadTimerPopupView()
        window.addSubview(videoView)
    }
    
    func openInAppPurchaseView() {
        let window = UIApplication.shared.keyWindow!
        let videoView = InAppPuchasePopupView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.loadInAppPurchaseView()
        window.addSubview(videoView)
    }
    
    
    func openInAppPurchaseViewForOutOfLikes() {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = TimerPopUpView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.popupFor = timerPopType.outOfLikes
        videoView.loadTimerPopupView()
        window.addSubview(videoView)
    }
    
    func openTimerPopUp() {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = TimerPopUpView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.loadTimerPopupView()
        window.addSubview(videoView)
    }
    
    @IBAction func boostButtonAction(_ sender: Any) {
        // self.addBorderForBoost()
        
        self.popUpFor = .forBoost
        if(Helper.isUserBoostActive()) {
            openTimerPopUp()
            return
        }
        
        if Helper.isUserHasMinimumCoins(minValue: coinsFor.boost) {
         
            let buttoTitle = String(format:StringConstants.iamOkToSpend(),coinConfig.coinsForBoost)
            //"I am OK to spend \(coinConfig.coinsForBoost) coins"
            let title = String(format:StringConstants.youNeedToSpend(),coinConfig.coinsForBoost)
            //"You need to spend \(coinConfig.coinsForBoost) Coins to boost your profile"
           showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coins"), mainTitle: title, subTitle: "", buttnTitle: buttoTitle, dontShowHide: true)
            
            
        }  else {
            let title = StringConstants.oopsYouDontHave()
            let subTitle = ""
          
            showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coinEmpty"), mainTitle: title, subTitle: subTitle, buttnTitle: StringConstants.BuyCoins(), dontShowHide: true)
        }
       
    }
    
    @IBAction func preferencesBtnAction(_ sender: Any) {
        
        let vc = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "MatchPrefrencesVc") as! MatchPreferencesVC
        vc.delegate = self
        let preferenceNav: UINavigationController = UINavigationController(rootViewController: vc)
        preferenceNav.navigationBar.isHidden = true
        self.present(preferenceNav, animated: true, completion: nil)
        
    }
    
    @IBAction func dollerBtnAction(_ sender: Any) {
        
        //checking coins plans details from  appstore available or not.
        if(Helper.getCoinPLansFromAppStore().count > 0) {
            let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "addCoin") as! AddCoinsVC
            userBasics.navigationController?.navigationBar.isHidden = true
            self.navigationController?.pushViewController(userBasics, animated: true)
        }
    }
    
    
    //GrigView button action
    @IBAction func chatBtnTapped(_ sender: Any) {
        let chatBtn = sender as! UIButton
        profileForChatSelected = self.profilesArray[chatBtn.tag]
        gotChatScreen()
    }
    
    func showCamping(details:[String:Any]){
        
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let camping = CampingView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        camping.loadPopUpView()
        camping.delegate = self
        camping.campaginDetails = details
        camping.updateView()
        window.addSubview(camping)
        
    }
   
    
    func downloadAllTheImagesAndVideos() {
        if(profilesArray.count > 1) {
            let profile = profilesArray[1]
            if profile.profileVideo.count > 5 {
                let dummmyImageView = UIImageView()
                if(profile.profileVideo .contains("res.cloudinary.com")) {
                    var modifedTumbNailVideo = profile.profileVideo
                    modifedTumbNailVideo = modifedTumbNailVideo.replace(target: ".mov", withString: ".jpeg")
                    let newUrl = URL(string: modifedTumbNailVideo)
                    dummmyImageView.kf.setImage(with:newUrl, placeholder:UIImage.init(named:"02m"))
                    
                } else {
                    let url = URL(string: profile.profilePicture)
                    dummmyImageView.kf.setImage(with:url, placeholder:UIImage.init(named:"02m"))
                }
            }
            
            if(profile.profilePicture.count > 5) {
                let imageUrl = URL(string: profile.profilePicture)
                let dummmyImageView = UIImageView()
                dummmyImageView.kf.setImage(with:imageUrl, placeholder:UIImage.init(named:"02m"))
            }
        }
    }
    
    func checkHasProfiles(callAPi:Bool) {
        if(self.profilesArray.count == 0) {
            self.searchStatusMsg.text = StringConstants.HomeNoProfiles()
            passportBtn.isHidden = false
            self.mainScrollView.isHidden = true
            if(callAPi) {
                self.index = self.index + 1
                self.offset = self.index * 20
                self.limit = (self.index+1) * 20
                self.requestForProfles()
            }
        } else {
            self.mainScrollView.isHidden = false
            downloadAllTheImagesAndVideos()
        }
    }
    
    func showUserProifleImage() {
        
        let userData = Database.shared.fetchResults()
        
        
        
        if !userData.isEmpty
        {
            if let userDetails = userData.first {
                if let urlString:String = userDetails.profilePictureURL {
                    let url = URL(string: urlString)
                    var imageName = "man_default"
                    if  userDetails.gender == "Male" {
                        imageName = "man_default"
                    }else {
                        imageName = "woman_default"
                    }
                    profileImageForRotation.kf.setImage(with:url, placeholder: UIImage.init(named: imageName))
                }
            }
        }
        
    }
    
    func groupBtnSelected()
    {
        UIView.animate(withDuration: 0.1){
            self.segmentImage.image = #imageLiteral(resourceName: "Group")
        }
        self.updateScrollViewFrame(newOffsetPoint: self.view.frame.size.width)
        
        selectedViewStyle = .grid
        
    }
    
    func homeBtnSelected()
    {
        UIView.animate(withDuration: 0.1){
            self.segmentImage.image = #imageLiteral(resourceName: "home")
        }
        self.updateScrollViewFrame(newOffsetPoint: 0)
        
        selectedViewStyle = .card
        
    }
    
    
    /// method for requesting get profiles.
    func refreshProfiles() {
        if (self.profilesArray.count == 0) {
            requestIsInProcess = true
            self.requestForProfles()
        }
    }
    
    ///method for requesting get profiles.
    func requestForProfles() {
        passportBtn.isHidden = true
        profileVm.requestForProfiles(offset: 0,limit: 100)
    }
    
    
    /// method for updating prefernces.
   @objc func prefrenceUpdateCompleted(){
        // new preferences are updated so removing the exist data and refreshing the request
        self.profilesArray.removeAll()
        self.cardProfiles.removeAll()
        self.kolodaObj.resetCurrentCardIndex()
        checkHasProfiles(callAPi: false)
        //        self.ProfileView.isHidden = true
        //        self.refreshProfiles()
    }
    
    
    @objc func userPassportedLocation(notification:NSNotification){
        self.profilesArray.removeAll()
        self.cardProfiles.removeAll()
        self.kolodaObj.resetCurrentCardIndex()
        self.requestForProfles()
    }
    
    @objc func removeAllObservers(notification: NSNotification) {
        NotificationCenter.default.removeObserver(self)
    }
    
    /// method for displaying new profile to existing one.
    ///
    /// - Parameter notification: contains new profile details.
   @objc func addONewProfileForMatchMaker(notification: NSNotification) {
        let selectedProfile = notification.object as! Profile
        self.profilesArray.insert(selectedProfile, at: 0)
        
        var seen = Set<String>()
        var unique = [Profile]()
        
        //removing duplicate profiles adding.
        
        for message in profilesArray {
            if !seen.contains(message.userId) {
                unique.append(message)
                seen.insert(message.userId)
            }
        }
        self.profilesArray = unique
        
        self.tabBarController?.selectedIndex = 0
    }
    
    
    /// adding different observers to profile viewcontroller.
    func subscribeForEvents() {
        NotificationCenter.default.setObserver(self, selector: #selector(matchedWithUser), name:NSNotification.Name(rawValue:NSNotificationNames.notificationForMatchUser), object: nil)
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(updateProfiles), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForGetProfiles), object: nil)
        NotificationCenter.default.setObserver(self, selector: #selector(updateCoinsAddedAdmin), name:NSNotification.Name(rawValue:NSNotificationNames.updateCoinFromAdmin), object: nil)
        
        NotificationCenter.default.setObserver(self, selector: #selector(addONewProfileForMatchMaker), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForOpenProfile), object: nil)
        
        NotificationCenter.default.setObserver(self, selector: #selector(removeAllObservers), name:NSNotification.Name(rawValue: NSNotificationNames.removeAllObserversForController), object: nil)
        
        NotificationCenter.default.setObserver(self, selector: #selector(prefrenceUpdateCompleted), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForPreferncesUpdate), object: nil)
        
        NotificationCenter.default.setObserver(self, selector: #selector(refreshProfileImage), name:NSNotification.Name(rawValue:NSNotificationNames.notificationToUpdateProfileImage), object: nil)
        
        NotificationCenter.default.setObserver(self, selector: #selector(tappedOnPushNotification), name:NSNotification.Name(rawValue: NSNotificationNames.notificationFortappedOnPushNotification), object: nil)

        NotificationCenter.default.setObserver(self, selector: #selector(newsFeedPostUpdateNotification), name:NSNotification.Name(rawValue: NSNotificationNames.updateNewsFeedDetails), object: nil)
        
        NotificationCenter.default.setObserver(self, selector: #selector(openProfileFromDynamicLik), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForDynamicLink), object: nil)
       
        NotificationCenter.default.setObserver(self, selector: #selector(showCampaginDetails), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForCampagin), object: nil)
        
        NotificationCenter.default.setObserver(self, selector: #selector(profileBannedFromAdmin), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForBannedProfile), object: nil)
        
        NotificationCenter.default.setObserver(self, selector: #selector(logoutForDelete), name:NSNotification.Name(rawValue: NSNotificationNames.deletedUserFromAdmin), object: nil)
        
        NotificationCenter.default.setObserver(self, selector: #selector(userPassportedLocation), name:NSNotification.Name(rawValue: NSNotificationNames.userPassportedLocation), object: nil)
        
        NotificationCenter.default.setObserver(self, selector: #selector(openChatForProfileFromOtherControllers), name:NSNotification.Name(rawValue:"openChatFromOtherControllers"), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(updateLikeDislikeProfilesCount), name: NSNotification.Name(rawValue:"updateActionsCountForAds"), object: nil)
        
        NotificationCenter.default.setObserver(self, selector: #selector(updateBoostViewDetails), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForBoostUpdate), object: nil)
        
        NotificationCenter.default.setObserver(self, selector: #selector(newsFeedUpdateDetails), name:NSNotification.Name(rawValue: NSNotificationNames.updateNewsFeedDetails), object: nil)
        
        var nameOfObserVer = NSNotification.Name(rawValue: "updateCoinsBal")
        NotificationCenter.default.setObserver(self, selector: #selector(updateCoinBalance), name: nameOfObserVer, object: nil)
        
    
        nameOfObserVer = NSNotification.Name(rawValue: "ChatUpdatedNotification")
        NotificationCenter.default.addObserver(self, selector: #selector(updateChatList), name: nameOfObserVer, object: nil)
        
        
    }
    
    @objc func updateCoinsAddedAdmin(){
        profileVm.getCoinBalance()
    }

    @objc func newsFeedUpdateDetails(){
        
        
        
        
        
        
    }
    
   @objc func openChatForProfileFromOtherControllers(not: Notification) {
        if let openProfile = not.object as? Profile {
            profileForChatSelected = openProfile
            if profileForChatSelected.isMatchedUser == 1 {
                //directly open chat. No need to show popup.
                openChatForProfileDetails(profile:profileForChatSelected)
            } else {
                gotChatScreen()
            }
        }
    }
    
   @objc func updateBoostViewDetails(not: Notification) {
        if let boostDetails = not.object as? [String: Any] {
            if let boostDic = boostDetails["boost"] as? [String:Any] {
                checkIsUsersBoostDetailsActive(details:boostDic)
            }
        } else {
            self.stopAnimationForBoost()
            self.boostViewBgImage.isHidden = true
            self.numberOfViewForBoost.isHidden = true
            
            self.boostViewBgImageForGrid.isHidden = true
          self.numberOfViewForBoostForGrid.isHidden = true
            
        }
    }
    
    
    func checkIsUsersBoostDetailsActive(details:[String:Any]) {
        

        if(!Helper.isUserBoostActive()){
            //if boost is not active then dot show details of views.
            self.stopAnimationForBoost()
            self.boostViewBgImage.isHidden = true
            self.numberOfViewForBoost.isHidden = true
            return
        }
        
        var totalViews = ""
        if let viewsCount = details["views"] as? String {
            totalViews = viewsCount
        }
        
        if let viewsCount = details["views"] as? Int {
            if(viewsCount>0) {
                totalViews = String(viewsCount)
            }
            
        }
        
        if(totalViews == "") {
            self.stopAnimationForBoost()
            self.boostViewBgImage.isHidden = true
            self.numberOfViewForBoost.isHidden = true
            
        self.boostViewBgImageForGrid.isHidden = true
        self.numberOfViewForBoostForGrid.isHidden = true
        } else {
            //self.addBorderForBoost()
            totalViews = totalViews + " " + StringConstants.views()
            
            
            
            self.boostViewBgImage.isHidden = false
            self.boostViewBgImageForGrid.isHidden = false
            
            
            self.numberOfViewForBoost.isHidden = false
            self.numberOfViewForBoostForGrid.isHidden = false
            self.numberOfViewForBoost.setTitle(totalViews, for:.normal)
            self.numberOfViewForBoostForGrid.setTitle(totalViews, for:.normal)
        }
    }
    
    @objc func profileBannedFromAdmin(notification:NSNotification) {
        logoutFromTheApp()
    }
   @objc func logoutForDelete(notification:NSNotification){
        Helper.changeRootVcInVaildToken()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    
    
    func logoutFromTheApp() {
        Helper.changeRootVcInVaildToken()
        Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.message(), message:StringConstants.userBannedFromAdmin())
    }
    
    @objc func showCampaginDetails(notification:NSNotification) {
        if let campaginDetails = notification.userInfo as? [String: Any] {
            if let imgUrl = campaginDetails["image"] as? String,let details = campaginDetails["aps"] as? [String: Any]{
                if let data = details["alert"] as? [String:Any] {
                    //show content here
                    if let title = data["title"] as? String {
                        print("campagin details available")
                        print("title of the campagain \(title)")
                        print("image details of the campagain \(imgUrl)")
                        let descrption = campaginDetails["message"] as? String
                        let temp = ["imageUrl":imgUrl,
                                    "title":title,
                                    "descrptin":descrption]
                        
                        showCamping(details:temp)
                    }
                }
            }
        }
    }
    
    @objc func newsFeedPostUpdateNotification(){
        self.tabBarController?.selectedIndex = 3
        let notificationName = NSNotification.Name(rawValue: "NewsFeedPostUpdateNotification")
        NotificationCenter.default.post(name: notificationName, object:nil, userInfo: nil)
    }
    
 @objc func tappedOnPushNotification(notification:NSNotification) {
        let userInfo = notification.userInfo as! [String: Any]
        if let notificationType = userInfo["type"] as? String {
            if(notificationType == "1") {
                self.tabBarController?.selectedIndex = 3
                print("both interested in each other")
            } else if(notificationType == "2") {
                if let memberProfileId = userInfo["target_id"] as? String {
                    openProfileByID(userID:memberProfileId)
                }
                print("interested in you.")
            } else if(notificationType == "41") {
                // 41 - for new chat message from user.
                if let openUsersChatWithID = userInfo["target_id"] as? String  {
                    self.openChatForMessage(receiverId:openUsersChatWithID)
                } else {
                    self.tabBarController?.selectedIndex = 3
                }
                self.tabBarController?.selectedIndex = 3
            } else if (notificationType == "7") || (notificationType == "10") || (notificationType == "9") || (notificationType == "8") || (notificationType == "20"){
                print("other")  //
                self.tabBarController?.selectedIndex = 2
            } else if(notificationType == "32"){
                self.tabBarController?.selectedIndex = 3
                let notificationName = NSNotification.Name(rawValue: "NewsFeedPostUpdateNotification")
                NotificationCenter.default.post(name: notificationName, object:nil, userInfo: nil)
            }
        }
        print(userInfo)
    }
    
    
   @objc func openProfileFromDynamicLik(notification:NSNotification) {
        let profileDetails = notification.userInfo as! [String:Any]
        if let receivedLink = profileDetails["receivedLink"] as? String {
            if(receivedLink.contains("searchResult")) {
                let fixedUrlForParsing = SERVICE.staticlinkForDeepLinking
                let sharedprofileId = receivedLink.replace(target: fixedUrlForParsing, withString: "")
                self.openProfileByID(userID:sharedprofileId)
            }
        }
    }
    
    @objc func refreshProfileImage(notification: NSNotification) {
        showUserProifleImage()
    }
    
    func openProfileButtonTapped() {
        self.openSelectedProfile(openProfile:  self.cardProfiles[kolodaObj.currentCardIndex])
        
    }
    
    func chatButtonTappedWithProfile(profile:Profile) {
        self.profileForChatSelected = profile
        chatButtonTapped()
    }
    
    
    /// delegate method of card view class , cardview chat button action
    func chatButtonTapped() {
        
        
        if profileForChatSelected.isMatchedUser == 1 {
            //directly open chat. No need to show popup.
            openChatForProfileDetails(profile:profileForChatSelected)
        } else {
            if Helper.isUserHasMinimumCoins(minValue: coinsFor.sendUnMatchMessage) {
                gotChatScreen()
            } else {
                coinConfig = Helper.getCoinConfig()
                showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coinEmpty"), mainTitle: StringConstants.CoinsGotOver(), subTitle: StringConstants.NoEnoughCoins() + StringConstants.chat(), buttnTitle: StringConstants.BuyCoins(), dontShowHide: true)
            }
        }
    }
    
    func gotChatScreen(){
        self.popUpFor = .forChat
        if Helper.isUserHasMinimumCoins(minValue: coinsFor.withOutMatchChat) {
            coinConfig = Helper.getCoinConfig()
            let buttoTitle = String(format:StringConstants.iamOkToSpend(),coinConfig.coinsForMsg)
            //"I am OK to spend \(coinConfig.coinsForMsg) coins"
            showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coins"), mainTitle: StringConstants.spendCoinsForChat(), subTitle: StringConstants.getChanceChat(), buttnTitle: buttoTitle, dontShowHide: true)
        } else {
            showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coinEmpty"), mainTitle: StringConstants.CoinsGotOver(), subTitle: StringConstants.NoEnoughCoins() + StringConstants.chat(), buttnTitle: StringConstants.BuyCoins(), dontShowHide: true)
        }
    }
    
    func showCoinsPopupForChat(icon: UIImage, mainTitle: String, subTitle: String, buttnTitle: String, dontShowHide: Bool){
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let coinsView = spendSomeCoinPopUp(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        coinsView.loadPopUpView()
        coinsView.coinsButton.titleLabel?.font = UIFont(name: CircularAir.Bold , size: 16)
        coinsView.titleForCoinsSpend.font = UIFont(name: CircularAir.Bold , size: CGFloat(fontSize.coinsPopUpTitle))
        coinsView.updatePopup(icon: icon, mainTitle: mainTitle, subTitle: subTitle, buttnTitle: buttnTitle, dontShowHide: dontShowHide)
        coinsView.spendSomeCoinDelegate = self
        window.addSubview(coinsView)
    }
    
    /// method for requesting second look profiles.
    func requestForReportUsefrReasons(userClicked:Bool,selectedUser:Profile) {
        matchMakerViewModel.requestToGetReportReasons(userClicked:userClicked)
        matchMakerViewModel.unMatchSubjectResponse
            .subscribe(onNext: {response in
                if (userClicked) {
                    //show reasons popup.
                    if(self.matchMakerViewModel.reportReasonsArray.count > 0) {
                        // self.showReportReasons(selectedUser:selectedUser)
                    }
                }
            }, onError:{ error in
                
            })
            
            .disposed(by:disposebag)
    }
    
    /// METHOD FOR OPENING MATCH SCREEN
    ///
    /// - Parameter notification: contains match details.
    @objc func matchedWithUser(notification: NSNotification)  {
        let matchedProfile = MatchedProfile.init(matchedUserDetails:notification.object as! [String : Any])
        if let syncVc = self.storyboard?.instantiateViewController(withIdentifier: "LetsSyncViewController") as? LetsSyncViewController {
            syncVc.matchedProfile = matchedProfile
            let syncNavVc: UINavigationController = UINavigationController(rootViewController: syncVc)
            syncNavVc.navigationBar.isHidden = true
            syncVc.delegate = self
            self.navigationController?.present(syncNavVc, animated: true, completion:nil)
        }
    }
    
    /// method for refreshing  profiles. -  prefernces are updated.
    ///
    /// - Parameter notification:contains new preferences details.
    func updateProfiles(profileModelData: [Profile]) {
        requestIsInProcess  = false
        if (profileModelData.count > 0) {
            if index == 0 {
                self.profilesArray.removeAll()
                self.cardProfiles.removeAll()
            }
            if profilesArray.count >= index * 20 {
                self.profilesArray =  profileModelData
                self.cardProfiles = profileModelData
            }
            else if profilesArray.count == 0 {
                self.profilesArray =  profileModelData
                self.cardProfiles = profileModelData
            }
            insertCardsForProfiles()
        }
        
        checkHasProfiles(callAPi: false)
        self.halo.shouldResume = false
    }
    
    func insertCardsForProfiles() {
        kolodaObj.resetCurrentCardIndex()
        HomeCollectionView.reloadData()
        checkHasProfiles(callAPi: false)
        updateUndoButton(enable:false)
        startVideoPlay()
    }
    
    func updateUndoButton(enable:Bool) {
        self.undoButtonOutlet.isEnabled = enable
    }
    
}

// MARK: KolodaViewDelegate

extension HomeVC: KolodaViewDelegate {
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        self.mainScrollView.isHidden = true
        self.index = index + 1
        self.offset = index * 20
        self.limit = (index+1) * 20
        self.requestForProfles()
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        startVideoPlay()
        // UIApplication.shared.openURL(URL(string: "https://yalantis.com/")!)
    }
    
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        if(direction == .right) {
//            if(Helper.isUserOutOfLikes()) {
//                openInAppPurchaseViewForOutOfLikes()
//                kolodaObj.revertAction()
//            } else {
                self.requestLikeApi(ProfileDetails: self.cardProfiles[kolodaObj.currentCardIndex-1])
                removeProfileFromGridView(removeProfile: self.cardProfiles[kolodaObj.currentCardIndex-1])
            //}
        } else if(direction == .left) {
            self.requestUnlikeApi(ProfileDetails: self.cardProfiles[kolodaObj.currentCardIndex-1])
//            updateUndoButton(enable:true)
            removeProfileFromGridView(removeProfile: self.cardProfiles[kolodaObj.currentCardIndex-1])
        } else if(direction == .up) {
            self.requestSuperLikeApi(ProfileDetails: self.cardProfiles[kolodaObj.currentCardIndex-1])
            removeProfileFromGridView(removeProfile: self.cardProfiles[kolodaObj.currentCardIndex-1])
        }
        if(kolodaObj.currentCardIndex-1>=0){
            _ =  updateCardView(selectedProfile:self.cardProfiles[kolodaObj.currentCardIndex-1])
        }
        startVideoPlay()
    }
    
    
    func koloda(_ koloda: KolodaView, didSelectCardAtPoint index: CGPoint) {
        
        //when user tapped on card moving to next/previous image/video based on user tapped on left/right side.
        //first time app login/signup showing tutorial view.
        
        let isNewUser = Helper.showTutorialView()
        let selectedCardView = koloda.subviews[koloda.subviews.count-1].viewWithTag(2012) as! CardViewClass
        if(!isNewUser) {
            //hiding the tutorialView and changing hide status to true for next time hiding the tutorialView.
           Helper.updateShowTutorialStatus()
            hideTutorialView(hide:false, cardView:selectedCardView )
           return
        } else {
            hideTutorialView(hide:true, cardView:selectedCardView)
        }
        
        // if tutorial view is notshowing user can see next/previous image/video.
        // checking the users tapped location and finding user tapped on left half or right half
        // left half - show previous message.
        // right half - show next image.
        // if next/previous image is not availble shaking the  card view and applying vibrartion effect.
        
        if(index.x > self.view.frame.size.width/2) {
            // show next image.
            let selectedCardView = koloda.subviews[koloda.subviews.count-1].viewWithTag(2012) as! CardViewClass
            
            var selectedProfile = self.cardProfiles[self.kolodaObj.currentCardIndex]
            if(selectedProfile.allMedia.count > 1) {
                if(selectedProfile.currentPhotoIndex+1 != selectedProfile.allMedia.count) {
                    selectedProfile.currentPhotoIndex = selectedProfile.currentPhotoIndex+1
                    self.cardProfiles[self.kolodaObj.currentCardIndex] = selectedProfile
                    selectedCardView.profileDetails = selectedProfile
                    selectedCardView.updateDetails()
                
                } else {
                    shakeTheCardView(cardView:selectedCardView)
                }
            } else {
                shakeTheCardView(cardView:selectedCardView)
            }
        } else {
            // show previous image.
            
            let selectedCardView = koloda.subviews[koloda.subviews.count-1].viewWithTag(2012) as! CardViewClass
            var selectedProfile = self.cardProfiles[self.kolodaObj.currentCardIndex]
            if(selectedProfile.allMedia.count != 0) {
                if(selectedProfile.currentPhotoIndex != 0) {
                    selectedProfile.currentPhotoIndex = selectedProfile.currentPhotoIndex-1
                    self.cardProfiles[self.kolodaObj.currentCardIndex] = selectedProfile
                    selectedCardView.profileDetails = selectedProfile
                    selectedCardView.updateDetails()
                } else {
                    shakeTheCardView(cardView:selectedCardView)
                }
            } else {
                shakeTheCardView(cardView:selectedCardView)
            }
        }
    }
    
    func shakeTheCardView(cardView:CardViewClass) {
        cardView.shakeView()
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
    }
    
    func hideTutorialView(hide:Bool,cardView:CardViewClass) {
        cardView.tutorialView.isHidden = hide
    }
}

// MARK: KolodaViewDataSource

extension HomeVC: KolodaViewDataSource {
    
    //delegates
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return self.cardProfiles.count
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .fast
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        return updateCardView(selectedProfile:self.cardProfiles[index])
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        
        return Bundle.main.loadNibNamed(IDENTIFIER.OverlayView, owner: self, options: nil)?[0] as? OverlayView
    }
    
    func updateCardView(selectedProfile:Profile) -> UIView{
        let cardView:CardViewClass = CardViewClass().shared
        cardView.profileDetails = selectedProfile
        cardView.updateDetails()
        cardView.cardDelegate = self
        if selectedProfile.hasProfileVideo == 1{
            cardView.videoView.isHidden = false
            self.localplayer = cardView.localplayer
            //self.localplayer?.play()
            
        }
        return cardView
    }
}



extension HomeVC:LetsSyncVCDelegate {
    
    /// lets sync vc delegates
    ///
    /// - Parameter matchedProifle: contains matched profile details.
    func openChatVcForProifle(matchedProifle: MatchedProfile) {
        Helper.updateNewMatchChatDetails(isOpenChat: true)
        var profileObj = Profile.fetchprofileDataObj(userDataDictionary: [:])
        profileObj.firstName = matchedProifle.name
        profileObj.profilePicture = matchedProifle.profilePic
        profileObj.userId = matchedProifle.userId
        profileObj.isMatchedUser = 1
        openChatForProfileDetails(profile:profileObj)
    }
}

extension HomeVC:spendSomeCoinPopUpDelegate {
    
    /// popUp delegate method
    ///
    /// - Parameter sender: UIbutton
    func coinBtnPressed(sender:UIButton) {
        let title = sender.title(for: .normal)
        if title == StringConstants.BuyCoins() {
            gotoCoinBalanceScreen()
            return
        }
        
        switch self.popUpFor {
            
        case .forSuperLike:
            UIView.animate(withDuration: 0.3, animations: {
                if(self.selectedViewStyle == .card) {
                    self.kolodaObj?.swipe(.up)
                } else {
                    self.requestSuperLikeApi(ProfileDetails: self.profilesArray[self.lastIndexSelectedToOpenProfile])
                    self.removeProfileFromCards(removeProfile:self.profilesArray[self.lastIndexSelectedToOpenProfile])
                    self.removeProfileFromGrid(removeIndex:self.lastIndexSelectedToOpenProfile)
                }
            })
            checkHasProfiles(callAPi: true)
            updateUndoButton(enable:false)
            break
        case .forDate:
            
            break
        case .forChat:
            openChatForProfileDetails(profile:profileForChatSelected)
            
            break
        case .forBoost:
            self.profileVm.boostProfile()
            break
        default:
            break
        }
    }
    
    func openChatForProfileDetails(profile:Profile) {
        Helper.updateNewMatchChatDetails(isOpenChat: true)
        self.tabBarController?.selectedIndex = 4
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when){
            let notificationName = NSNotification.Name(rawValue: "SyncChatNotification")
            NotificationCenter.default.post(name: notificationName, object: self, userInfo: ["receiverID":profile])
        }
    }
    
    
    func openChatForMessage(receiverId:String) {
        Helper.updateNewMatchChatDetails(isOpenChat: true)
        self.tabBarController?.selectedIndex = 4
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when){
            let notificationName = NSNotification.Name(rawValue: "openChatForProfileForPushNotification")
            NotificationCenter.default.post(name: notificationName, object:nil, userInfo: ["receiverID":receiverId])
        }
    }
}

extension HomeVC: CampingDelegate {
    func sendBtnPressed() {
        gotoWebVC(title: StringConstants.campaign(), link: "https://www.google.com")
    }
    
    /// method to create object for web view controller
    ///
    /// - Parameters:
    ///   - title: title of web view screen
    ///   - link: url in string type
    func gotoWebVC(title:String,link: String){
        
        let vc = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "webVC") as! WebViewController
        vc.navTitle = title
        vc.link = link
        let preferenceNav: UINavigationController = UINavigationController(rootViewController: vc)
        self.present(preferenceNav, animated: true, completion: nil)
    }
}

extension HomeVC {
    
    func didGetResponse(){
        self.profileVm.subject_response.subscribe(onNext: { success in
            
            switch success.0 {
            case ProfileViewModel.ResponseType.getProfileData:
                if success.1 {
                    self.updateProfiles(profileModelData: self.profileVm.profileModelData)
                }else {
                    Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: self.profileVm.errorMsg, onViewController: self)
                }
                
                break
                
            case ProfileViewModel.ResponseType.liked:
                if success.1 == false {
                    
                    self.kolodaObj.revertAction()
                    Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: self.profileVm.errorMsg, onViewController: self)
                }
                break
                
            case ProfileViewModel.ResponseType.unliked:
                if success.1 == false {
                    self.kolodaObj.revertAction()
                    Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: self.profileVm.errorMsg, onViewController: self)
                }
                self.updateUndoButton(enable:true)
                break
                
            case ProfileViewModel.ResponseType.rewind:
                
                if success.1 == false {
                    Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: self.profileVm.errorMsg, onViewController: self)
                }else {
                    self.kolodaObj.revertAction()
                }
                self.updateLikeDislikeProfilesCount()
                break
                
            case ProfileViewModel.ResponseType.superLiked:
                
                if success.1 {
                    self.updateCoinBalance()
                   
                    if self.selectedViewStyle == .card {
                         self.showCoinAnimation(view: self.superLikeBtnView)
                    }else{
                        self.showCoinAnimation(view: self.collectionBoostBtnView)
                    }
           
                } else {
                    self.kolodaObj.revertAction()
                    Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: self.profileVm.errorMsg, onViewController: self)
                }
                break
                
            case ProfileViewModel.ResponseType.boost:
                if success.1 == false {
                    Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: self.profileVm.errorMsg, onViewController: self)
                } else {
                    if self.selectedViewStyle == .card {
                        self.showCoinAnimation(view: self.boostBtnView)
                    } else{
                        self.showCoinAnimation(view: self.collectionBoostBtnView)
                    }
                    
                    //self.addBorderForBoost()
                }
        
                break
                
            case ProfileViewModel.ResponseType.coinBalance:
                if success.1 {
                    self.updateCoinBalance()
                     NotificationCenter.default.post(name:NSNotification.Name(rawValue: "updateCoinsBal"), object: self, userInfo:[:])
                }else {
                    Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: self.profileVm.errorMsg, onViewController: self)
                }
                
                break
            case ProfileViewModel.ResponseType.unsubscribed:
                if success.1 {
                    self.openInAppPurchaseView()
                    self.kolodaObj.revertAction()
                }
                break
            }
            
        }, onError: {error in
            print(error)
        }).disposed(by: self.profileVm.disposeBag)
    }
    
    func showCoinAnimation(view: UIView){
        for i in 1..<7 {
            self.view.applyCoinsAnimation(indexAt:i, fromView:self.buttonsView!,coins:" 1 0 0 ")
        }
    }
}

// coins config API calls.
extension HomeVC {
    func getCoinConfig(){
        
        let apiCalls = CoinsAPICalls()
        apiCalls.getCoinConfig()
        
        apiCalls.subject_response
            .subscribe(onNext: {response in
                Helper.saveCoinConfig(data: response.response)
            }, onError: {error in
            })
            .disposed(by:disposebag)
    }
    
    func getCloudinaryDetails(){
        
        let apiCall = Authentication()
        
        apiCall.getCloudinaryCredential()
        apiCall.subject_response
            .subscribe(onNext: {response in
                self.handleCloudinaryDetailsResponse(responseModel: response)
            }, onError: {error in
                Helper.hideProgressIndicator()
                Helper.showAlertMessageViewFromTopNavBar(showOnView: self.view, message: error.localizedDescription)
                
            })
            .disposed(by:disposeBag)
    }
    
    
    
    func handleCloudinaryDetailsResponse(responseModel: ResponseModel){
        
        let statuscode:API.ErrorCode = API.ErrorCode(rawValue: responseModel.statusCode)!
        
        switch statuscode {
        case .Success:
            if  let data = responseModel.response["data"] as? [String:Any] {
                Helper.saveCloudinaryDetails(data: data)
                
            }
            print("yyeyrfrfc ")
        default:
            break
        }
    }
}

extension NotificationCenter {
    func setObserver(_ observer: AnyObject, selector: Selector, name: NSNotification.Name, object: AnyObject?) {
        NotificationCenter.default.removeObserver(observer, name: name, object: object)
        NotificationCenter.default.addObserver(observer, selector: selector, name: name, object: object)
    }
}

extension HomeVC: GADInterstitialDelegate {
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
    
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
        recivedADDFromAdMob = true
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
    
}
extension HomeVC {
    
    
    @objc func updateChatList() {
        let couchbaseObj = Couchbase.sharedInstance
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        guard let chats = chatsDocVMObject.getChatsFromCouchbase() else { return }
        let sortedChats = chats.sorted {
            return $0.lastMessageDate > $1.lastMessageDate
        }
        let chatListViewModel = ChatListViewModel(withChatObjects: sortedChats)
        let count = chatListViewModel.unreadChatsCounts()
        if count == 0 {
            self.tabBarController?.tabBar.items?[4].badgeValue = nil
        } else {
            self.tabBarController?.tabBar.items?[4].badgeValue = "\(count)"
        }
    }
    
    func updateChatListCount() {
        let count = UserDefaults.standard.integer(forKey: "unreadChatCount")
        if count == 0 {
            self.tabBarController?.tabBar.items?[4].badgeValue = nil
        } else {
            self.tabBarController?.tabBar.items?[4].badgeValue = "\(count)"
        }
        
        let dateCount = UserDefaults.standard.integer(forKey: "pendingDateCount")
        
        if dateCount == 0 {
            self.tabBarController?.tabBar.items?[2].badgeValue = nil
        } else {
            self.tabBarController?.tabBar.items?[2].badgeValue = "\(dateCount)"
        }
    }
}
