//
//  CardViewClass.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 04/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import VGPlayer

protocol cardVieDelegate: class {
    func openProfileButtonTapped()
    func chatButtonTapped()
    func chatButtonTappedWithProfile(profile:Profile)
}

class CardViewClass: UIView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var onlineStatusIcon: UIImageView!
    @IBOutlet weak var indicatorCollView: UICollectionView!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var videoIconBtn: UIButton!
    @IBOutlet weak var gradientImage: UIGradientImageView!
    @IBOutlet weak var superLikeImage: UIImageView!
    @IBOutlet weak var tutorialView: UIView!
    @IBOutlet weak var lastLabel: UILabel!
    @IBOutlet weak var lastPhotoLabel: UILabel!
    @IBOutlet weak var nextLabel: UILabel!
    @IBOutlet weak var nextPhotoLabel: UILabel!
    @IBOutlet weak var openProfile: UILabel!
    @IBOutlet weak var pairLikeCountLabel: UILabel!
    @IBOutlet weak var pairUnlikeCount: UILabel!
    
    var obj:CardViewClass? = nil
    var localplayer : VGPlayer?
   weak var cardDelegate:cardVieDelegate? = nil
    var profileDetails:Profile? = nil
    var imageName = "man_default1"
        
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cardImage.layer.cornerRadius = 10
        indicatorCollView.backgroundColor = UIColor.clear
        superLikeImage.isHidden = true
        lastLabel.text = StringConstants.lastPhoto()
        lastPhotoLabel.text = StringConstants.photo()
        nextLabel.text = StringConstants.next()
        nextPhotoLabel.text = StringConstants.photo()
        openProfile.text = StringConstants.openProfile()
        if profileDetails != nil && profileDetails?.gender == "Male" {
           imageName = "man_default1"
        }else {
            imageName = "woman_default1"
        }
        
    }
    
    
    
    var shared:CardViewClass {
        
        if obj == nil {
            obj = UINib(nibName: "CardView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as? CardViewClass
            obj?.frame = UIScreen.main.bounds
        }
        return obj!
    }
    
    @IBAction func chatButtonAction(_ sender: Any) {
        self.cardDelegate?.chatButtonTappedWithProfile(profile:profileDetails!)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return (profileDetails?.allMedia.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        var size = CGSize.zero
            let cellSize = Int(collectionView.frame.size.width) - (((self.profileDetails?.allMedia.count)!-1)*5)
            let cellWidth = (cellSize)/(self.profileDetails?.allMedia.count)!
            let cellHight = collectionView.frame.size.height
            size.height =  5
            size.width  =  CGFloat(cellWidth)
    
        return size
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: "IndexIndicatorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "indicatorCell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "indicatorCell", for: indexPath) as! IndexIndicatorCollectionViewCell
        if(indexPath.section <= (profileDetails?.currentPhotoIndex)!) {
            cell.indexIndicatorView.backgroundColor = UIColor.white
        } else {
            cell.indexIndicatorView.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 5)
    }
    
    @IBAction func infoButtonAction(_ sender: Any) {
        self.cardDelegate?.openProfileButtonTapped()
    }
    
    
    @IBAction func playBtnAction(_ sender: Any) {
         videoView.isHidden = false
        if(self.localplayer?.state == .playFinished) {
            self.localplayer?.displayView.replayButton.sendActions(for: .touchUpInside)
        } else {
            self.localplayer?.displayView.playButtion.sendActions(for: .touchUpInside)
        }
        
        
    }
    
    
    
    func updateDetails() {
      //  gradientImage.setTopGradient()
        if(profileDetails?.totalPhotosAndVideos != 0) {
            
            let url = URL(string: (profileDetails?.allMedia[(profileDetails?.currentPhotoIndex)!])!)
            
            if profileDetails?.currentPhotoIndex == 0 {
                if profileDetails?.hasProfileVideo == 1 {
                    updateProfileVideo(data:(profileDetails?.allMedia[(profileDetails?.currentPhotoIndex)!])!,thumbnail: (profileDetails?.profileVideoThumbnail)!)
                }else {
                    videoView.isHidden = true
                    self.cardImage.isHidden = false
                    self.localplayer?.pause()
                    self.cardImage.kf.setImage(with:url, placeholder:UIImage.init(named:imageName))
                    videoIconBtn.isHidden = true
                }
                
            }else {
                videoView.isHidden = true
                self.cardImage.isHidden = false
                self.localplayer?.pause()
                self.cardImage.kf.setImage(with:url, placeholder:UIImage.init(named:imageName))
                videoIconBtn.isHidden = true
            }
           
        } else {
            self.cardImage.image = UIImage.init(named:"02m")
        }
        
        if(profileDetails?.isHiddenUserAge == 1) {
            self.name.text = profileDetails?.firstName
            
        } else {
            if let age = profileDetails?.userAge {
                self.name.text = profileDetails?.firstName
             self.name.text = profileDetails?.firstName.appending(", ").appending(age)
            }
        }
        
        if let yesVotesCount = profileDetails?.yesVotes {
                self.pairLikeCountLabel.isHidden = false
                self.pairLikeCountLabel.text = "Yes: " + yesVotesCount + "%"
        }
        if let noVotesCount = profileDetails?.noVotes {
                self.pairUnlikeCount.isHidden = false
                self.pairUnlikeCount.text = "No: " + noVotesCount + "%"
        }
        
        if profileDetails?.noVotes == "0" && profileDetails?.yesVotes == "0"  {
            self.pairLikeCountLabel.isHidden = true
            self.pairUnlikeCount.isHidden = true
        }else{
            self.pairUnlikeCount.isHidden = false
            self.pairLikeCountLabel.isHidden = false
        }
        
        
        if profileDetails?.work.count != 0 {
            self.placeName.text = profileDetails?.work
        }else if profileDetails?.education.count != 0 {
            self.placeName.text = profileDetails?.education
        }else{
            self.placeName.text = profileDetails?.job
        }
        
        if profileDetails?.onlineStatus == 1 {
            onlineStatusIcon.isHidden = false
            self.onlineStatusIcon.image = #imageLiteral(resourceName: "Ellipse 95")
        }else {
            onlineStatusIcon.isHidden = true
            self.onlineStatusIcon.image = #imageLiteral(resourceName: "Ellipse 96")
        }
        
        if profileDetails?.superLiked == 0 {
            superLikeImage.isHidden = true
        }else {
            superLikeImage.isHidden = false
            
            gradientImage.gradientForMatch()
        }
        
        superLikeImage.startHeartBeat()
        
        self.tag = 2012
        self.indicatorCollView.reloadData()
    }
    
    
    func updateProfileVideo(data: String,thumbnail:String){
        
        var newUrl = URL(string: thumbnail)
        if thumbnail.count == 0 {
            
            let jpgURL = URL(string:data)?
                .deletingPathExtension()
                .appendingPathExtension("jpg")
            
            newUrl = jpgURL
        }
        
        videoView.isHidden = true
        cardImage.isHidden = false
        cardImage.kf.setImage(with:newUrl, placeholder:UIImage.init(named:imageName))
        
        showVideoForUrl(videoUrl:data)
        videoIconBtn.isHidden = false
    }
    
    
    func showVideoForUrl(videoUrl:String) {
        if(videoUrl.count > 5) {
            
            let url = URL(string:videoUrl)
            if url != nil {
                localplayer = VGPlayer(URL: url!)
            }
            localplayer?.delegate = self
            localplayer?.displayView.frame.size.width = UIScreen.main.bounds.size.width
            localplayer?.displayView.frame.size.height = videoView.frame.size.height
            videoView.addSubview((localplayer?.displayView)!)
            localplayer?.displayView.bringSubview(toFront:(localplayer?.displayView.playButtion)!)
            localplayer?.displayView.clipsToBounds = true
            videoView.bringSubview(toFront: videoIconBtn)
            localplayer?.backgroundMode = .proceed
            localplayer?.displayView.backgroundColor = UIColor.black
            localplayer?.displayView.delegate = self
            localplayer?.displayView.snp.makeConstraints { [weak self] (make) in
                guard let strongSelf = self?.videoView else { return }
                make.edges.equalTo(strongSelf)
            }
           // localplayer?.play()
        }
    }
    
}

extension CardViewClass: VGPlayerDelegate {
    func vgPlayer(_ player: VGPlayer, playerFailed error: VGPlayerError) {
        print(error)
    }
    
    func vgPlayer(_ player: VGPlayer, stateDidChange state: VGPlayerState) {
        print("player State ",state)
        
        if (player.state == .playing) {
            self.videoIconBtn.isSelected = true
            videoView.isHidden = false
            cardImage.isHidden = true
        } else if(self.localplayer?.state == .playFinished) {
            
            self.localplayer?.displayView.replayButton.sendActions(for: .touchUpInside)
            
        }else {
            self.videoIconBtn.isSelected = false
            videoView.isHidden = true
            cardImage.isHidden = false
           
        }
    }
    
    func vgPlayer(_ player: VGPlayer, bufferStateDidChange state: VGPlayerBufferstate) {
        print("buffer State", state)
    }
    
}

extension CardViewClass: VGPlayerViewDelegate {
    func vgPlayerView(_ playerView: VGPlayerView, willFullscreen fullscreen: Bool) {
        
    }
    
    func vgPlayerView(didTappedClose playerView: VGPlayerView) {
        if playerView.isFullScreen {
            playerView.exitFullscreen()
        } else {
            //self.navigationController?.popViewController(animated: true)
        }
    }
    
    func vgPlayerView(didDisplayControl playerView: VGPlayerView) {
   //     UIApplication.shared.setStatusBarHidden(!playerView.isDisplayControl, with: .fade)
    }
}


