 
 //
 //  ExampleOverlayView.swift
 //  KolodaView
 //
 //  Created by Eugene Andreyev on 6/21/15.
 //  Copyright (c) 2015 CocoaPods. All rights reserved.
 //
 
 import UIKit
 import Koloda
 
 private let overlayRightImageName = "like-1"
 private let overlayLeftImageName = "nope"
 private let overlayUpmageName = "superlike"
  var isMatchPair = false
 
 class ExampleOverlayView: OverlayView {
    
    @IBOutlet weak var unlike: UIImageView!
    @IBOutlet weak var superLikeImageView: UIImageView!
    @IBOutlet lazy var overlayImageView
        : UIImageView! = {
            [unowned self] in
            
            var imageView = UIImageView(frame: self.bounds)
            self.addSubview(imageView)
            
            return imageView
            }()
    var obj:ExampleOverlayView? = nil
    
    
    
    var shared:ExampleOverlayView {
        if obj == nil {
            obj = UINib(nibName: "OverlayView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as? ExampleOverlayView
            obj?.frame = UIScreen.main.bounds
        }
        return obj!
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // overlayImageView.layer.cornerRadius = 10
    
        
    }
    //override var isForPairMatch = isMatchPair
    
    override var overlayState: SwipeResultDirection? {
        didSet {
            switch overlayState {
            case .left? :
                overlayImageView.isHidden = true
                unlike.isHidden = false
                superLikeImageView.isHidden = true
                 self.alpha = 1.0
                break
            case .right? :
               
                overlayImageView.isHidden = false
                unlike.isHidden = true
                superLikeImageView.isHidden = true
                self.alpha = 1.0
                break
       
            case .up? :
                overlayImageView.isHidden = true
                unlike.isHidden = true
                if isMatchPair {
                    superLikeImageView.isHidden = true
                }else{
                    superLikeImageView.isHidden = false
                }
                 self.alpha = 1.0
                break
  
            default:
                overlayImageView.image = nil
            }
        }
    }
    
 }
