//
//  IndexIndicatorCollectionViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 24/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class IndexIndicatorCollectionViewCell: UICollectionViewCell {
   @IBOutlet weak var indexIndicatorView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.indexIndicatorView.layer.cornerRadius = 3
    }

}
