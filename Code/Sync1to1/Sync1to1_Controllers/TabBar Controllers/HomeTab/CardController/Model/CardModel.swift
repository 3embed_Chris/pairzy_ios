//
//  CardModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 05/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit


struct CardModel {
    var name = ""
    var subtitle = ""
    var image = UIImage()
}

