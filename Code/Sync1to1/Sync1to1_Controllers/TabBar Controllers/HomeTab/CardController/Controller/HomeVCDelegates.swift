//
//  HomeVCDelegates.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 09/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit


// MARK: - UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension HomeVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.profilesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let homeCell:HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIds.HomeCollectionCell, for: indexPath) as! HomeCollectionCell
        
        let profile = self.profilesArray[indexPath.row]
        
        if profile.gender == "Male" {
            homeCell.imageName = "man_default1"
        } else {
            homeCell.imageName = "woman_default1"
        }
        
        homeCell.updateCell(profile: profile)
        homeCell.updateTagForButtons(tag:indexPath.item)
        return homeCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize.zero
        let cellWidth = (UIScreen.main.bounds.width - 30)/2
        let cellHight = (collectionView.frame.size.height - 40)/2
        size.height =  cellHight
        size.width  =  cellWidth
        return size
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //performSegue(withIdentifier: String(describing:ProfileDetailVC.self), sender: self)
        self.lastIndexSelectedToOpenProfile = indexPath.row
        self.profileForChatSelected = self.profilesArray[indexPath.row]
        self.openSelectedProfile(openProfile: self.profilesArray[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row + 1 == self.profilesArray.count {
          
            if self.profilesArray.count >= (index+1) * 20 {
                self.index = index + 1
                self.offset = index * 20
                self.limit = (index+1) * 20
                 //  self.requestForProfles()
            }
        }else {
            if indexPath.row  == 0 {
                self.index = self.profilesArray.count/20
                self.offset = 0
                self.limit = 20
                
            }
        }
    }
    
}


// MARK: - UIScrollViewDelegate
extension HomeVC: UIScrollViewDelegate {
    
    /// method for updting frame.
    ///
    /// - Parameter newOffsetPoint: point for new frame.
    func updateScrollViewFrame(newOffsetPoint:CGFloat) {
        var scrollContentOffset = self.mainScrollView.contentOffset
        scrollContentOffset.x = newOffsetPoint
        self.mainScrollView.setContentOffset(scrollContentOffset, animated: true)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
    }
    
}

