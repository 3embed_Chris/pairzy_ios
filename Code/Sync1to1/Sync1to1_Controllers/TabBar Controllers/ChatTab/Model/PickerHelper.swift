//
//  PickerHelper.swift
//  Datum
//
//  Created by Rahul Sharma on 30/05/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation



open class ImageSaveHelper {
    static func saveImageDocumentDirectory(imageToSave:UIImage , completionHandler: @escaping (_ imagePath:String) -> Void){
        let fileManager = FileManager.default
        let timeStamp = Helper.getcurrentDataAndtimeForSave()
        
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("\(timeStamp).jpg")
        let imageData = UIImageJPEGRepresentation(imageToSave, 0.5)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        completionHandler(paths)
    }
}
