//
//  CommentModel.swift
//  Datum
//
//  Created by Rahul Sharma on 01/06/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit


class CommentModel: NSObject {

    var comment: String?
    var commentId: String?
    var commenterName: String?
    var commentedOn: Double?
    var email:String?
    var dob:Int?
    var profilePic:String?
    var gender:String?
    var phoneNo:String?
    
    //var commentModelArray = [CommentModel]()
//    "_id": "5cf7c43e022e6b3e5f6857d0",
//    "comment": "Nice",
//    "commenterName": "Sofi",
//    "PhoneNo": "+917982583020",
//    "Email": "rahul@gmail.con",
//    "dob": 832703400000,
//    "profilePic": "http://res.cloudinary.com/dabyufj03/image/upload/v1559635304/Datum/20190604013141PM.jpg",
//    "gender": "Female",
//    "commentedOn": 1559741502887
    
    init(commentData: [String : Any]){
        if let comment = commentData["comment"] as? String{
            self.comment = comment
        }
        if let commentId = commentData["_id"] as? String{
            self.commentId = commentId
        }
        if let commenterName = commentData["commenterName"] as? String{
            self.commenterName = commenterName
        }
        if let pic = commentData["profilePic"] as? String{
            self.profilePic = pic
        }
        if let commentedOn = commentData["commentedOn"] as? Double{
            self.commentedOn = commentedOn
        }
        if let email = commentData["Email"] as? String{
            self.email = email
        }
        if let dob = commentData["dob"] as? Int{
            self.dob = dob
        }
        if let phoneNo = commentData["PhoneNo"] as? String{
            self.phoneNo = phoneNo
        }
//        if let commentsArray = modelData["comments"] as? [Any]{
//            for data in commentsArray{
//                if let modelData = data as? [String : Any]{
//                    let commentModel = CommentModel(commentData: modelData)
//                    self.commentModelArray.append(commentModel)
//                }
//            }
//        }
        
    }
        init(postModelData: [String : Any], userImage: String, userName: String){
            if let comment = postModelData["comment"] as? String{
                self.comment = comment
            }
            if let commentedBy = postModelData["commenterName"] as? String{
                self.commenterName = commentedBy
            }
//            if let timeStamp = postModelData["commentedOn"] as? String{
//                self.commentedOn = Double(timeStamp)
//            }
            
            if let timeStamp = postModelData["commentedOn"] as? Double{
                self.commentedOn = timeStamp
            }
            self.profilePic = userImage
            self.commenterName = userName
        }
    
}
