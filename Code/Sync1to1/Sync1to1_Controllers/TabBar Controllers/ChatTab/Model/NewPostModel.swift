//
//  NewPostModel.swift
//  Datum
//
//  Created by Rahul Sharma on 30/05/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

struct NewPostModel {
    var isEdit: Bool = false
    var postId: String?
    var caption:String = ""
    var location:String = ""
    var mediaPath:String = ""
    var cloudinaryPublicID:String = ""
    var isVideo = false
    var width:CGFloat = 100
    var height:CGFloat = 100
    var image:UIImage?
    
}
