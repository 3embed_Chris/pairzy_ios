//
//  CreatePostHelper.swift
//  Datum
//
//  Created by Rahul Sharma on 31/05/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit


class CreatePostHelper: NSObject {
    //  static let hud = JGProgressHUD(style: .dark)
    
    static func showPIOnView(view:UIView,message:String) {
        // hud.textLabel.text = message
        // hud.show(in: view)
        // hud.show(in: view, animated: true)
    }
    
    
    static func hidePi() {
        // hud.dismiss(afterDelay:0)
    }
    
    static func uploadImageToCloudinary(image:UIImage,view:UIView,message:String,onCompletion: @escaping (_ status: Bool,_ storyDetails:[String:Any]) -> Void) {
        Helper.showProgressIndicator(withMessage: message)
        CloudinaryHelper.sharedInstance.uploadImage(image: image, onCompletion: {(isUploaded,ImageUrl) in
            hidePi()
            var storyDetails:[String:Any] = [:]
            if(isUploaded) {
                let newStory = ["urlPath":ImageUrl!,"type":1,"thumbnail":ImageUrl!] as [String : Any]
                storyDetails = newStory
            }
            onCompletion(isUploaded,storyDetails)
        })
    }
    
    
    static func uploadImageToCloudinaryForTextStory(image:UIImage,view:UIView,message:String,onCompletion: @escaping (_ status: Bool,_ storyDetails:[String:Any]) -> Void) {
        Helper.showProgressIndicator(withMessage: message)
        CloudinaryHelper.sharedInstance.uploadImage(image: image, onCompletion: {(isUploaded,ImageUrl) in
            hidePi()
            var storyDetails:[String:Any] = [:]
            if(isUploaded) {
                let newStory = ["urlPath":ImageUrl!] as [String : Any]
                storyDetails = newStory
            }
            onCompletion(isUploaded,storyDetails)
        })
    }
    
    static func uploadVideoToCloudinary(videoURL:URL,view:UIView,message:String,onCompletion: @escaping (_ status: Bool,_ storyDetails:[String:Any]) -> Void) {
        Helper.showProgressIndicator(withMessage: message)
        CloudinaryHelper.sharedInstance.uploadVideo(video: videoURL, onCompletion: {(isUploaded,ImageUrl) in
            hidePi()
            var storyDetails:[String:Any] = [:]
            if(isUploaded) {
                let thumbNailUrl = ImageUrl?.replace(target:".mov", withString:".jpeg")
                let newStory = ["urlPath":ImageUrl!,"type":2,"thumbnail":thumbNailUrl ?? ""] as [String : Any]
                storyDetails = newStory
            }
            onCompletion(isUploaded,storyDetails)
        })
    }
    
}
