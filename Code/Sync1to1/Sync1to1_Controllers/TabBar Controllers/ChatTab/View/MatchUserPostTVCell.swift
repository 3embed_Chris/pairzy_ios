//
//  MatchUserPostTVCell.swift
//  Datum
//
//  Created by Rahul Sharma on 23/05/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher
import RxSwift

protocol MatchUserPostTVCellDelegate: class {
    func likeButtonTap(index: Int, isSelected: Bool, momentModel: MomentsModel)
    func commentButtonTap(index: Int)
    func getLikersOnTap(index:Int)
}

class MatchUserPostTVCell: UITableViewCell {

    @IBOutlet weak var profileButtonOutlet: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var postTimeLabel: UILabel!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var postTitleLabel: UILabel!
    @IBOutlet weak var itsaMatchView: UIImageView!
  
    @IBOutlet weak var chatButtonOutlet: UIButton!
    @IBOutlet weak var viewAllCommentsLabel: UILabel!
    @IBOutlet weak var postImageCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var deleteOptionBtnOutlet: UIButton!
    @IBOutlet weak var bioHeadingLabel: UILabel!
    @IBOutlet weak var aboutMeLabel: UILabel!
    

    let userData = Database.shared.fetchResults()
    var momentPostModel: MomentsModel?
    var index: Int = 0
    weak var delegate: MatchUserPostTVCellDelegate?
    var createPostVM = CreatePostViewModel()
    var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.userImageView.makeCornerRadius(radius: userImageView.frame.size.width / 2)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    /// To set profile model data in in cell
    ///
    /// - Parameter profileModel: profileModel
    func setMatchUserPostData(userMomentModel: MomentsModel){
        self.momentPostModel = userMomentModel
        guard let momentPost = momentPostModel else{return}
        self.userImageView.setImageOn(imageUrl: momentPost.profilePic, defaultImage: #imageLiteral(resourceName: "defaultImage"))
        self.userNameLabel.text = momentPost.userName
        self.postTimeLabel.text = Helper.getTimeStamp(timeStamp: momentPost.postedOn)
        //   let date =
        // self.postTitleLabel.text = momentPost.momentDescription
        //self.deleteOptionBtnOutlet.isHidden = false
        if !userData.isEmpty{
            if let userDetails = userData.first {
                let userId = userDetails.userId
                if momentPostModel?.userId == userId{
                    self.chatButtonOutlet.isHidden = true
                    //self.deleteOptionBtnOutlet.isHidden = false
                }else{
                    //self.deleteOptionBtnOutlet.isHidden = true
                    self.chatButtonOutlet.isHidden = false
                }
        }
        }
        if momentPost.typeFlag == 5{
            let actvityTime: Date = Date(timeIntervalSince1970: momentPost.postedOn/1000)
            let dateAndTime = CalenderUtility.getDateAndTime(fromDate: actvityTime)
            self.postTitleLabel.text = "Matched on" + " " + dateAndTime
        }else{
            self.postTitleLabel.text = momentPost.momentDescription
        }
        if momentPost.typeFlag == 5{
            self.itsaMatchView.isHidden = false
            self.itsaMatchView.image = #imageLiteral(resourceName: "Itsamatch")
        }else{
            self.itsaMatchView.isHidden = true
        }
        if momentPost.typeFlag == 4{
            self.bioHeadingLabel.isHidden = false
            self.aboutMeLabel.isHidden = false
            self.aboutMeLabel.text = momentPost.momentDescription
            self.postTitleLabel.text = ""
        }else{
            self.bioHeadingLabel.isHidden = true
            self.aboutMeLabel.isHidden = true
        }
        
        if momentPost.liked{
            self.likeImageView.image = UIImage(named: "like_on")
        }else{
            self.likeImageView.image = UIImage(named: "like_off")
        }
        self.makeAttributedStringForViewAndLike(momentPostModel: momentPost)
        self.postImageCollectionView.reloadData()
        
    }
    
    @IBAction func getLikersDetailAction(_ sender: UIButton) {
        
        delegate?.getLikersOnTap(index: self.index)
    }
 
    //MARK:- Buttons Action
    
    @IBAction func gestureAction(_ sender: Any) {
        
    }
    
    
    @IBAction func likeButtonAction(_ sender: Any) {
        
        if momentPostModel?.liked == true{
            
            momentPostModel?.liked = false
            let likeCount = momentPostModel?.likeCount
            momentPostModel?.likeCount = likeCount! - 1
            delegate?.likeButtonTap(index: self.index, isSelected: true, momentModel: self.momentPostModel!)
            self.likeImageView.singleTapLikeButtonAnimation(changeImage: #imageLiteral(resourceName: "like_off"), view: self.likeImageView)
            
        }else{
            
            momentPostModel?.liked = true
            let likeCount = momentPostModel?.likeCount
            momentPostModel?.likeCount = likeCount! + 1
            delegate?.likeButtonTap(index: self.index, isSelected: false, momentModel: momentPostModel!)
            
            self.likeImageView.singleTapLikeButtonAnimation(changeImage: #imageLiteral(resourceName: "like_on"), view: self.likeImageView)
        }
        self.makeAttributedStringForViewAndLike(momentPostModel: self.momentPostModel!)
    }
    
    @IBAction func commentButtonAction(_ sender: Any) {
        self.delegate?.commentButtonTap(index: index)
        
    }
    
    
    
    @IBAction func viewAllCommentsAction(_ sender: Any) {
        self.delegate?.commentButtonTap(index: index)
    }
    
    func updateUIForPageControl(){
        guard momentPostModel != nil else { return}
        if momentPostModel?.isPairSuccess == 1 {
            pageControl.numberOfPages = 0
            pageControl.isHidden = true
        }else{
            let numberofPages = momentPostModel?.url.count
            if numberofPages != nil{
                if numberofPages! > 1 {
                    pageControl.numberOfPages = numberofPages!
                    pageControl.isHidden = false
                }else {
                    pageControl.numberOfPages = 0
                    pageControl.isHidden = true
                }
            }
        }

    }
    
//    /// To handle double tap of cell
//    ///
//    /// - Parameter gesture: tap gesture
//    @objc func doubleTapHandle(_ gesture: UITapGestureRecognizer){
//        print("doubletapped")
//        if let data = self.momentPostModel{
//            if data.liked == false {
//                self.momentPostModel?.liked = true
//                let likeCount = self.momentPostModel?.likeCount
//                self.momentPostModel?.likeCount = likeCount! + 1
//                createPostVM.postId = (momentPostModel?.postId)!
//                self.delegate?.likeButtonTap(index: self.index, isSelected: false, momentModel: self.momentPostModel!)
//                //self.likeImageView.singleTapLikeButtonAnimation(changeImage: #imageLiteral(resourceName: "like_on"),! view: self.likeImageView)
//                self.makeAttributedStringForViewAndLike(momentPostModel: self.momentPostModel!)
//            }
//            self.bigLikeImageView.popUpDoubletapFavouritAnimation()
//            self.likeImageView.singleTapLikeButtonAnimation(changeImage: #imageLiteral(resourceName: "like_on"), view: self.likeImageView)
//        }
//    }

    func makeAttributedStringForViewAndLike(momentPostModel: MomentsModel){
        
        var font = UIFont(name: "CircularAirPro-Bold", size: 12.0)!
        var attributed = [ NSAttributedStringKey.font: font]
        let commentCount = NSMutableAttributedString(string: "\(momentPostModel.commentCount) ", attributes: attributed)
        let likecount = NSMutableAttributedString(string: "\(momentPostModel.likeCount) ", attributes: attributed)
        font = UIFont(name: "CircularAirPro-Bold", size: 12.0)!
        attributed = [ NSAttributedStringKey.font: font]
        let comment = NSMutableAttributedString(string: "Comments", attributes: attributed)
        let likes = NSMutableAttributedString(string: "likes ", attributes: attributed)
        likecount.append(likes)
        commentCount.append(comment)
        self.likesCountLabel.attributedText = likecount
        self.viewAllCommentsLabel.attributedText = commentCount
        
    }
    
    

 


}

extension MatchUserPostTVCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if momentPostModel != nil{
            return (momentPostModel?.url.count)!
        }else {
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "PostImageCVCell", for: indexPath) as! PostImageCVCell
        // cell.updateInstaMedia(imageUrl: (instaProfile?.userMedia[indexPath.item])!)
        
        cell.momentPostModel = self.momentPostModel
        if momentPostModel != nil{
            if momentPostModel!.typeFlag == 4{
                cell.bioImageView.isHidden = false
                cell.bioImageView.backgroundColor = Colors.AppBaseColor
                cell.bioImageView.backgroundColor = Colors.AppBaseColor
            }else{
                cell.bioImageView.isHidden = true
            }
        }
        cell.index = indexPath.item
        cell.delegate = self
        cell.setMediaPostData(mediaUrl: (momentPostModel?.url[indexPath.item])!)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size = CGSize.zero
        var cellWidth = self.postImageCollectionView.frame.size.width
        if let momentPost = momentPostModel {
            if momentPost.isPairSuccess == 1{
                cellWidth = self.postImageCollectionView.frame.size.width/2
            }else{
                cellWidth = self.postImageCollectionView.frame.size.width
            }
        }
        let cellHight = collectionView.frame.size.height
        size.height =  cellHight
        size.width  =  cellWidth
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        guard let videoCell = (cell as? PostImageCVCell) else { return }
        let visibleCells = collectionView.visibleCells
        let minIndex = visibleCells.startIndex
        if collectionView.visibleCells.index(of: cell) == minIndex {
            //videoCell.playerView.player?.play()
            videoCell.localPlayer?.play()
        }
        
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let videoCell = cell as? PostImageCVCell else { return }
        //        videoCell.playerView.player?.pause()
        //        videoCell.playerView.player = nil
        videoCell.localPlayer?.pause()
        videoCell.localPlayer?.shutdown()
        videoCell.localPlayer = nil
    }
}


extension MatchUserPostTVCell: PostImageCVCellDelegate{
    
    func likeAndCommentCount(likeCount: NSMutableAttributedString, commentCount: NSMutableAttributedString) {
        self.likesCountLabel.attributedText = likeCount
        self.viewAllCommentsLabel.attributedText = commentCount
    }
    
    func showLikeAnimation() {
        self.likeImageView.singleTapLikeButtonAnimation(changeImage: #imageLiteral(resourceName: "like_on"), view: self.likeImageView)
    }
    
    func likeButtonTap(index: Int, isSelected: Bool, momentModel: MomentsModel) {
        self.createPostVM.likeAndUnlikePostService(index: index, isSelected: isSelected, momentModel: momentModel)
    }


   
}


// MARK: - UIScrollViewDelegate
extension MatchUserPostTVCell: UIScrollViewDelegate {
   
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //updateScrollViewFrame(newOffsetPoint: scrollView.contentOffset.x )
    }


    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.pageControl.currentPage = Int(pageNumber)
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.pageControl.currentPage = Int(pageNumber)
    }
    
     /// method for updting frame.
    ///
    /// - Parameter newOffsetPoint: point for new frame.
    func updateScrollViewFrame(newOffsetPoint:CGFloat) {
        var scrollContentOffset = self.postImageCollectionView.contentOffset
        scrollContentOffset.x = newOffsetPoint
        self.postImageCollectionView.setContentOffset(scrollContentOffset, animated: true)
    }
    
}
