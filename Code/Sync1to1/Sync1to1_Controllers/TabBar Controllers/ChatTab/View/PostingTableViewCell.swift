//
//  PostingTableViewCell.swift
//  dub.ly
//
//  Created by Shivansh on 1/31/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

class PostingTableViewCell: UITableViewCell,UITextViewDelegate {

    @IBOutlet weak var captionTextView: UITextView!
    @IBOutlet weak var shareImageView: UIImageView!
    @IBOutlet weak var videoPlayPauseBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        captionTextView.text = StringConstants.captionPlaceholderText()
        captionTextView.textColor = UIColor.lightGray
        captionTextView.font = UIFont(name: "CircularAirPro-Book", size: 13.0)
        captionTextView.returnKeyType = .done
        captionTextView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   

}
