//
//  DatumChatTableCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 07/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class DatumChatTableCell: UITableViewCell {
    
    @IBOutlet weak var imageBackView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var messageTitle: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var messageCountView: UIView!
    @IBOutlet weak var messageCountLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    func initialSetup(){
        
        imageBackView.layer.cornerRadius = imageBackView.frame.size.height/2
        profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2
        Helper.setShadow(sender: profileImageView)
        
        
        messageCountView.layer.cornerRadius = messageCountView.frame.size.height/2
        messageCountView.backgroundColor = Colors.AppBaseColor
        Helper.setShadow(sender: messageCountView)
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
