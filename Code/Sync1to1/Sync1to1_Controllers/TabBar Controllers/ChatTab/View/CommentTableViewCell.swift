//
//  CommentTableViewCell.swift
//  MQTT Chat Module
//
//  Created by 3Embed Software Tech Pvt Ltd on 12/10/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

protocol CommentTableViewCellDelegate: class{
    func userNameGetClicked(name: String)
}

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userCommentLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    weak var delegate: CommentTableViewCellDelegate?
    var Timestamp: Double {
        return (Date().timeIntervalSince1970 * 1000)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userImageView.makeCornerRadius(radius: self.userImageView.frame.size.width / 2)
//        self.userCommentLabel.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setCellData(modelData: CommentModel){
        
        self.userImageView.setImageOn(imageUrl: modelData.profilePic, defaultImage: #imageLiteral(resourceName: "defaultImage"))
        self.makeUserNameHighlightedWithComment(modelData: modelData)
        if (modelData.commentedOn) != nil {
            self.timeLabel.text = Helper.getTimeStamp(timeStamp: modelData.commentedOn!)
        }else{
            self.timeLabel.text = Helper.getTimeStamp(timeStamp: Timestamp)
        }
        
    }
    private func makeUserNameHighlightedWithComment(modelData: CommentModel){
    
         var font = UIFont(name: "GillSans-SemiBold", size: 14.0)!
        var attributed = [ NSAttributedStringKey.font: font]
        let userName = NSMutableAttributedString(string: modelData.commenterName!, attributes: attributed)
        font = UIFont(name: "GillSans", size: 14.0)!
        attributed = [ NSAttributedStringKey.font: font]
        let comment = NSMutableAttributedString(string: modelData.comment!, attributes: attributed)
        let attrString = NSAttributedString(string: " ")
        userName.append(attrString)
        let fullcomment = userName
        fullcomment.append(comment)
        self.userCommentLabel.attributedText = fullcomment
        
    }
}
