//
//  LikersTableViewCell.swift
//  Datum
//
//  Created by Rahul Sharma on 20/06/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

class LikersTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var profileImage: UIImageView!
    
    
    @IBOutlet weak var userNamelabel: UILabel!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         self.profileImage.makeCornerRadius(radius: profileImage.frame.size.width / 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func setCellData(modelData: MomentsModel){
        self.profileImage.setImageOn(imageUrl: modelData.profilePic, defaultImage: #imageLiteral(resourceName: "defaultImage"))
        self.userNamelabel.text = modelData.likersName
    }

}
