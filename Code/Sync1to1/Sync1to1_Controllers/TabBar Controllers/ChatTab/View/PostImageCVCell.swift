//
//  PostImageCVCell.swift
//  Datum
//
//  Created by Rahul Sharma on 04/06/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher
import IJKMediaFramework

protocol PostImageCVCellDelegate: class {
    func likeButtonTap(index: Int, isSelected: Bool, momentModel: MomentsModel)
    func likeAndCommentCount(likeCount: NSMutableAttributedString, commentCount: NSMutableAttributedString)
    func showLikeAnimation()
}

class PostImageCVCell: UICollectionViewCell {
    //  MARK:- Outlet
    @IBOutlet weak var videoImageView: UIImageView!
    
    @IBOutlet weak var userPostImageView: AnimatedImageView!
    
    @IBOutlet weak var viewForVideo: UIView!
    
    @IBOutlet weak var soundVideoImage: UIImageView!
    
    @IBOutlet weak var bioImageView: UIImageView!
    
      @IBOutlet weak var bigLikeImageView: UIImageView!

    
    
    let createPostVM = CreatePostViewModel()
    var momentPostModel: MomentsModel?
    var index: Int = 0
   weak var delegate: PostImageCVCellDelegate?
    //var localplayer : VGPlayer?
    var localPlayer : IJKFFMoviePlayerController!
    var currentIndex: Int = -1
    var videoUrl: URL?
    var playingIndex: Int = -1
    

    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        //Tap gesture setup
        //Double tap gesture
        let doubleTapGR = UITapGestureRecognizer(target: self, action: #selector(PostImageCVCell.doubleTapHandle(_:)))
        doubleTapGR.numberOfTapsRequired = 2
        //        doubleTapGR.numberOfTouchesRequired = 1
        self.addGestureRecognizer(doubleTapGR)

        //Single tap gesture
        let singleTapGR = UITapGestureRecognizer(target: self, action: #selector(PostImageCVCell.singleTapHandle(_:)))
        singleTapGR.numberOfTapsRequired = 1
        //        singleTapGR.numberOfTouchesRequired = 1
        self.addGestureRecognizer(singleTapGR)
        singleTapGR.require(toFail: doubleTapGR)
 
    }
    
    open fileprivate(set) var player : IJKAVMoviePlayerController? {
        willSet{
            removePlayerNotifications()
        }
        didSet {
            addPlayerNotifications()
        }
    }
    
    func addPlayerNotifications() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    func removePlayerNotifications() {
        
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    //App enter in forground.
    @objc func applicationWillEnterForeground(_ notification: Notification)        {
        
        if self.localPlayer != nil {
            self.localPlayer.play()
        }
        
    }
    
    //App enter in forground.
    @objc func applicationDidEnterBackground(_ notification: Notification) {
        
        self.localPlayer = nil
        
    }
    
    func setMediaPostData(mediaUrl: String ){
            if mediaUrl.suffix(3) == "jpg" || mediaUrl.suffix(3) == "png" || mediaUrl.suffix(3) == "jpeg" || mediaUrl.contains("facebook") {
                videoImageView.isHidden = true
                viewForVideo.isHidden = true
                soundVideoImage.isHidden = true
                userPostImageView.isHidden = false
                // videoMuteBtnOutlet.isEnabled = false
                let newUrl = URL(string: mediaUrl)
                
                userPostImageView.setImageOn(imageUrl: mediaUrl, defaultImage: #imageLiteral(resourceName: "giphyDefaultlogo"))
                
                //userPostImageView.kf.setImage(with:newUrl, placeholder:#imageLiteral(resourceName: "giphyDefaultlogo"))
                self.userPostImageView.contentMode = UIViewContentMode.scaleAspectFill
            }else{
                soundVideoImage.isHidden = false
                videoImageView.isHidden = false
                userPostImageView.isHidden = false
                videoImageView.contentMode = .scaleAspectFill
                //     videoMuteBtnOutlet.isEnabled = false
                //            let videoUrl = URL(string: mediaUrl)
                //            let thumbNailImage = self.thumbnail(sourceURL: videoUrl!)
                //            userPostImageView.image = thumbNailImage
                
                let endIndex = mediaUrl.index(mediaUrl.endIndex, offsetBy: -3)
                var truncated = mediaUrl.substring(to: endIndex)
                truncated = truncated + "png"
                self.userPostImageView.setImageOn(imageUrl: truncated, defaultImage: #imageLiteral(resourceName: "giphyDefaultlogo"))
                
            }
        }
        
      
        
    
    
    
    //MARK:- Hndle tap gesture
    /// To handle single tap
    ///
    /// - Parameter gesture: tap gesture
//    @objc func singleTapHandle(_ gesture: UITapGestureRecognizer){
//        print("single tapped")
//        if momentPostModel?.typeFlag == 4{
//            //
//            if localplayer!.player?.isMuted == false {
//                self.localplayer!.player?.isMuted = true
//                self.soundVideoImage.image = #imageLiteral(resourceName: "Sound Off")
//
//            }else if localplayer!.player?.isMuted == true {
//                self.localplayer!.player?.isMuted = false
//                self.soundVideoImage.image = #imageLiteral(resourceName: "Sound On")
//            }
//        }
//    }

    
     func play(_ url: URL?, cell: MatchUserPostTVCell) {
        self.userPostImageView.isHidden = true
        videoImageView.isHidden = false
        viewForVideo.isHidden = false
        soundVideoImage.isHidden = false
        let options = IJKFFOptions.byDefault()
        self.localPlayer = IJKFFMoviePlayerController(contentURL: url!, with: options)
        self.localPlayer.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.localPlayer.view.frame = cell.postImageCollectionView.bounds
        self.localPlayer.scalingMode = .aspectFill
        self.localPlayer.shouldAutoplay = true
        self.autoresizesSubviews = true
        viewForVideo.addSubview((self.localPlayer.view)!)
        viewForVideo.bringSubview(toFront:localPlayer.view)
        self.localPlayer.prepareToPlay()
    }
    
//    func showVideoForUrl(videoUrl:String,videoView: UIView) {
//        self.userPostImageView.isHidden = true
//        videoImageView.isHidden = false
//        viewForVideo.isHidden = false
//        soundVideoImage.isHidden = false
//        if(videoUrl.count > 5) {
//            let url = URL(string:videoUrl)
//            if url != nil {
//                //localplayer = VGPlayer(URL: url!)
//                let options = IJKFFOptions.byDefault()
//                localPlayer = IJKFFMoviePlayerController(contentURL: url!, with: options)
//            }
//
//                //                localplayer?.delegate = self
//                //                localplayer?.displayView.tag = 2018
//            //videoView.addSubview((localplayer?.displayView)!)
//            videoView.addSubview((localPlayer.view)!)
//            videoView.bringSubview(toFront:localPlayer.view )
//            localPlayer.setPauseInBackground(true)
//            localPlayer.scalingMode = .aspectFill
////            localplayer?.displayView.snp.makeConstraints { [weak viewForVideo] (make) in
////                guard let strongSelf = viewForVideo else { return }
////                make.edges.equalTo(strongSelf)
////            }
//            localPlayer.play()
//            // let tap = UITapGestureRecognizer(target: self, action: #selector(tapped(_ :)))
//            //localplayer?.displayView.addGestureRecognizer(tap)
//            localPlayer.view.isUserInteractionEnabled =  true
//
//        }
//    }
    

    
    func updateProfileVideo(data: String,thumbnail:String){
        var newUrl = URL(string: thumbnail)
        let modifiedLink = data.replace(target:"video/upload/", withString:"video/upload/q_20,w_200,h_200/")
        let jpgURL = URL(string:modifiedLink)?
            .deletingPathExtension()
            .appendingPathExtension("gif")
        newUrl = jpgURL
        userPostImageView.isHidden = false
        userPostImageView.kf.setImage(with:newUrl, placeholder:UIImage())
        videoImageView.isHidden = false
    }
    
    
    //MARK:- Hndle tap gesture
    /// To handle single tap
    ///
    /// - Parameter gesture: tap gesture
    @objc func singleTapHandle(_ gesture: UITapGestureRecognizer){
        print("single tapped")
        guard let videoPlayer = self.localPlayer  else {return}
        if videoPlayer.playbackVolume == 1.0 {
           // videoPlayer.play.isMuted = true
            videoPlayer.playbackVolume = 0.0
            self.soundVideoImage.image = #imageLiteral(resourceName: "Sound Off")
        }else if videoPlayer.playbackVolume == 0.0 {
            videoPlayer.playbackVolume = 1.0
            self.soundVideoImage.image = #imageLiteral(resourceName: "Sound On")
        }

    }
 
    /// To handle double tap of cell
    ///
    /// - Parameter gesture: tap gesture
    @objc func doubleTapHandle(_ gesture: UITapGestureRecognizer){
        print("doubletapped")
        if let data = self.momentPostModel{
            if data.liked == false {
                self.momentPostModel?.liked = true
                let likeCount = self.momentPostModel?.likeCount
                self.momentPostModel?.likeCount = likeCount! + 1
                createPostVM.postId = (momentPostModel?.postId)!
                self.delegate?.likeButtonTap(index: self.index, isSelected: false, momentModel: self.momentPostModel!)
                //self.likeImageView.singleTapLikeButtonAnimation(changeImage: #imageLiteral(resourceName: "like_on"),! view: self.likeImageView)
                self.makeAttributedStringForViewAndLike(momentPostModel: self.momentPostModel!)
            }
            self.bigLikeImageView.popUpDoubletapFavouritAnimation()
            delegate?.showLikeAnimation()
            //self.likeImageView.singleTapLikeButtonAnimation(changeImage: #imageLiteral(resourceName: "like_on"), view: self.likeImageView)
        }
    }
    
    func makeAttributedStringForViewAndLike(momentPostModel: MomentsModel){
        var font = UIFont(name: "CircularAirPro-Bold", size: 12.0)!
        var attributed = [ NSAttributedStringKey.font: font]
        let commentCount = NSMutableAttributedString(string: "\(momentPostModel.commentCount) ", attributes: attributed)
        let likecount = NSMutableAttributedString(string: "\(momentPostModel.likeCount) ", attributes: attributed)
        font = UIFont(name: "CircularAirPro-Bold", size: 12.0)!
        attributed = [ NSAttributedStringKey.font: font]
        let comment = NSMutableAttributedString(string: "Comments", attributes: attributed)
        let likes = NSMutableAttributedString(string: "likes ", attributes: attributed)
        likecount.append(likes)
        commentCount.append(comment)
        delegate?.likeAndCommentCount(likeCount: likecount, commentCount: commentCount)

    }
    
    
    
    

//    /// To handle double tap of cell
//    ///
//    /// - Parameter gesture: tap gesture
//    @objc func doubleTapHandle(_ gesture: UITapGestureRecognizer){
//        print("doubletapped")
//        if let data = self.momentPostModel{
//            if data.liked == false{
//                self.momentPostModel?.liked = true
//                let likeCount = self.momentPostModel?.likeCount
//                self.momentPostModel?.likeCount = likeCount! + 1
//                createPostVM.postId = (momentPostModel?.postId)!
//                self.delegate?.likeButtonTap(index: self.index, isSelected: false, momentModel: self.momentPostModel!)
//                //                self.likeImageView.singleTapLikeButtonAnimation(changeImage: #imageLiteral(resourceName: "like_on"),! view: self.likeImageView)
//                self.makeAttributedStringForViewAndLike(momentPostModel: self.momentPostModel!)
//                
//            }
//            self.bigLikeImageView.popUpDoubletapFavouritAnimation()
//            self.likeImageView.singleTapLikeButtonAnimation(changeImage: #imageLiteral(resourceName: "like_on"), view: self.likeImageView)
//        }
//    }
    
    /// Creating Thumbnail For Video url
    ///
    /// - Parameter sourceURL: videoUrl
    /// - Returns: Thumbnail Image
    private func thumbnail(sourceURL:URL) -> UIImage {
        let asset = AVAsset(url: sourceURL)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        let time = CMTime(seconds: 2, preferredTimescale: 1)
        do {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            return #imageLiteral(resourceName: "play")
        }
    }
    
  


}




///// TO make imgae url from any type of URL
/////
///// - Returns: image url
//func makeThumbnailUrl() -> String{
//    let endIndex = self.index(self.endIndex, offsetBy: -3)
//    var truncated = self.substring(to: endIndex)
//    truncated = truncated + "jpg"
//    return truncated
//}


