//
//  DatumChatCollectionCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 07/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class DatumChatCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var superLikeIcon: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var imagBackView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var onlineStatusImage: UIImageView!
    @IBOutlet weak var likesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }
    
    func initialSetup(){
        profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2
        Helper.setShadow(sender: profileImageView)
    }
    
    
    func isSuperLikedMatch(superLiked:Int) {
        imagBackView.layer.cornerRadius = imagBackView.frame.size.height/2
        imagBackView.layer.borderWidth = 2
        if superLiked  == 1{
            imagBackView.layer.borderColor = UIColor(red: 0, green: 165.0/255.0, blue: 235/255.0, alpha:1.0).cgColor
            superLikeIcon.isHidden = false
        } else {
            imagBackView.layer.borderColor = UIColor.white.cgColor
            superLikeIcon.isHidden = true
        }
    }
    
    
    /// method for displaying data on cell
    ///
    /// - Parameter userData: user details for displaying on cell.
    func displayUserDetails(userData:Profile)  {
        self.profileName.text = userData.firstName
        let url = URL(string: userData.profilePicture)
        self.profileImageView.kf.setImage(with:url, placeholder:UIImage.init(named:"02m"))
        if userData.onlineStatus == 1{
            onlineStatusImage.isHidden = false
        }else {
            onlineStatusImage.isHidden = true
        }
        
        isSuperLikedMatch(superLiked:userData.isMatchedBySuperLike)
    }
}
