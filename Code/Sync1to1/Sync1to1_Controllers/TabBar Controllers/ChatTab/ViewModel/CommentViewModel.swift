//
//  CommentViewModel.swift
//  Datum
//
//  Created by Rahul Sharma on 01/06/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class CommentViewModel: NSObject {
    
//
//    var profileModelData = [Profile]()
//    if let profileResponse = response.response["data"] as? [AnyObject] {
//        for (index, _) in profileResponse.enumerated() {
//            let profileDetails = profileResponse[index] as! [String:Any]
//            profileModelData.append(Profile.fetchprofileDataObj(userDataDictionary:profileDetails))
//        }
//    }
//    self.profileModelData = profileModelData

    
    enum responseType:Int {
        case getCommnent = 0
        case postComment = 1
    }
    let disposeBag = DisposeBag()
    let commentPostVM_resp = PublishSubject<(responseType, Bool)>()
    var message = ""
    var postId = ""
    var comment = ""
    var timeStamp = ""
    var commentModelArray = [CommentModel]()

    func postingCommentOnUserPost(){
   
        // let data = self.profileDetails[index]
        let createPostAPICall = CreatePostAPICalls()
 
        let params = [ServiceInfo.postId: postId,
                      ServiceInfo.comment: comment] as [String : Any]
       
        if !createPostAPICall.subject_response.hasObservers {
            createPostAPICall.subject_response
                .subscribe(onNext: {response in
                    Helper.hideProgressIndicator()
                    self.handlePostComment(response: response)
                }, onError: {error in
                    Helper.hideProgressIndicator()
                    self.message = error.localizedDescription
                    self.commentPostVM_resp.onError(error)
                })
                .disposed(by:disposeBag)
        }
        
        createPostAPICall.requestForCommentPost(postParams: params)
    
    }
  
    
    /// handleFriendRequestResponse
    ///
    /// - Parameter response: response
    func handlePostComment(response: ResponseModel){
        guard let statusCode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode) else {
            return
        }
        if statusCode == API.ErrorCode.Success {
            let userData = Database.shared.fetchResults()
            if !userData.isEmpty
            {
                if let userDetails = userData.first {
                     let userName = userDetails.first_name
                    let pic = userDetails.profilePictureURL
                    let ticks = Helper.getTimeStamp(timeStamp: Timestamp)
                    self.timeStamp = ticks
                    let commentModel = CommentModel(postModelData: ["comment":self.comment,"commentedOn":timeStamp], userImage: pic!, userName: userName!)
                    self.commentModelArray.insert(commentModel, at: 0)
                }
            }
            
            self.commentPostVM_resp.onNext((.postComment,true))
        } else {
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            self.commentPostVM_resp.onNext((.postComment,false))
        }
    }
    
    func gettingCommentOfUserPost(userPostId : String){
        
        // let data = self.profileDetails[index]
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        let createPostAPICall = CreatePostAPICalls()
        
        if !createPostAPICall.subject_response.hasObservers {
            createPostAPICall.subject_response
                .subscribe(onNext: {response in
                    Helper.hideProgressIndicator()
                    self.handlegetAllPostedComment(response: response)
                }, onError: {error in
                    Helper.hideProgressIndicator()
                    self.message = error.localizedDescription
                    self.commentPostVM_resp.onError(error)
                })
                .disposed(by:disposeBag)
        }
        createPostAPICall.requestForGettingComment(postId: userPostId)
        
    }
        
    func handlegetAllPostedComment(response: ResponseModel){
        guard let statusCode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode) else {
            return
        }
        if statusCode == API.ErrorCode.Success {
            if let commentResponse = response.response["data"] as? [[String:Any]] {
                self.commentModelArray = commentResponse.map({ (data) -> CommentModel in
                    CommentModel(commentData: data)
                })
            }
            self.commentPostVM_resp.onNext((.getCommnent,true))
        } else {
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            self.commentPostVM_resp.onNext((.getCommnent,false))
        }
    }
    
}

extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}


var Timestamp: Double {
    return (Date().timeIntervalSince1970 * 1000)
}
