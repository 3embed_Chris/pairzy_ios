//
//  CreatePostViewModel.swift
//  Datum
//
//  Created by Rahul Sharma on 31/05/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa



class CreatePostViewModel: NSObject {
    
    
    enum responseType:Int {
        case createUserPost = 0
        case getUserPost = 1
        case likeOrUnlikeUserPost = 2
        case getLikers = 3
        case deleteUserPost = 4
        case getReportReasons = 5
        
    }
    
    //MARK: Variables And Declarations
    let disposeBag = DisposeBag()
    let createPostVM_resp = PublishSubject<(responseType, Bool)>()
    //let createPostVM_resp = PublishSubject<(responseType, Bool)>()
    var newPostModel:NewPostModel?
    var postDetailsCell:PostingTableViewCell?
   // var profileDetails = Profile.fetchprofileDataObj(userDataDictionary:[:])
     var profileDetails = [Profile]()
    var profilePostModel:Profile?
    var momentDetailsArray = [MomentsModel]()
    var reportReasonsArray = [ReportReason]()
    var mediaURl: String = ""
    var typeFlag:Int = 3
    var caption:String = ""
    var message = ""
    var postId = ""
    var type = 0
    var selectedIndex = 0
    
    // MARK: - API Calls -
    
    //API CallFor Creating user post
    func postUserPostBio(){
        
        let createPostAPICall = CreatePostAPICalls()
        let params = [ ServiceInfo.typeFlag : typeFlag as Any,
                       ServiceInfo.description : caption as Any,
                       ServiceInfo.url: mediaURl as Any
                       ] as [String:Any]
        
        
        if !createPostAPICall.subject_response.hasObservers {
            createPostAPICall.subject_response
                .subscribe(onNext: {response in
                    Helper.hideProgressIndicator()
                    self.handlePostUserPostBioResponse(response: response)
                }, onError: {error in
                    Helper.hideProgressIndicator()
                    self.message = error.localizedDescription
                    self.createPostVM_resp.onError(error)
                })
                .disposed(by:disposeBag)
        }
        DispatchQueue.global(qos: .background).async{
        createPostAPICall.requestToPostBio(postParams: params)
        }
    }

    
//   API call For getting User Post Of Matches
    func getUserPostBioOfMatches() {
     //   Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        let createPostAPICall = CreatePostAPICalls()
        if !createPostAPICall.subject_response.hasObservers {
            createPostAPICall.subject_response
                .subscribe(onNext: {response in
                    Helper.hideProgressIndicator()
                    self.handleGetUserPostBio(response: response)
                }, onError: {error in
                    Helper.hideProgressIndicator()
                    self.message = error.localizedDescription
                    self.createPostVM_resp.onError(error)
                })
                .disposed(by:disposeBag)
        }
         DispatchQueue.global(qos: .background).async{
            createPostAPICall.requestForGetPost()
        }
    }
    
    /// handleUserBioPostResponse
    ///
    /// - Parameter response: response
    func handlePostUserPostBioResponse(response: ResponseModel){
        guard let statusCode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode) else {
            return
        }
        if statusCode == API.ErrorCode.Success {
            
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
//            let name = NSNotification.Name(rawValue: "PostUpdateNotification")
//            NotificationCenter.default.post(name: name, object: self, userInfo: nil)
            self.createPostVM_resp.onNext((.createUserPost,true))
        } else {
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            self.createPostVM_resp.onNext((.createUserPost,false))
        }
    }
    
  
    /// handleGetUserBioPostResponse
    ///
    /// - Parameter response: response
    func handleGetUserPostBio(response: ResponseModel){
        guard let statusCode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode) else {
            return
        }
        if statusCode == API.ErrorCode.Success {
            
            Helper.hideProgressIndicator()
            if let momentDataResponse = response.response["data"] as? [[String:Any]] {
                self.momentDetailsArray = momentDataResponse.map({ (data) -> MomentsModel in
                    MomentsModel(momentsData: data)
                })
            }
            
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            self.createPostVM_resp.onNext((.getUserPost,true))
        } else {
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            self.createPostVM_resp.onNext((.getUserPost,false))
        }
    }
    
    /// like and unlike Post Api
    ///
    /// - Parameter : index,is Selected Bool value,MomentModel
    func likeAndUnlikePostService(index: Int, isSelected: Bool, momentModel: MomentsModel) {
       // let data = self.profileDetails[index]
        
        let createPostAPICall = CreatePostAPICalls()
        if isSelected{
            type = 2
        }else{
            type = 1
        }
       // let selectedPostId = momentDetailsArray[index].postId
        //self.momentDetailsArray[index] = momentModel
        let selectedPostId = momentModel.postId

        let params = [ServiceInfo.type: type,
                      ServiceInfo.postId: selectedPostId] as [String : Any]
        
        if !createPostAPICall.subject_response.hasObservers {
            createPostAPICall.subject_response
                .subscribe(onNext: {response in
                    Helper.hideProgressIndicator()
                    self.handleLikeAndUnlikePostResponse(response: response)
                }, onError: {error in
                    Helper.hideProgressIndicator()
                    self.message = error.localizedDescription
                    self.createPostVM_resp.onError(error)
                })
                .disposed(by:disposeBag)
        }
        
        createPostAPICall.requestForLikeAndUnlikePost(postParams: params)
    }
    
    
    /// handle like and unlike response
    ///
    /// - Parameter response: response Model
    func handleLikeAndUnlikePostResponse(response: ResponseModel){
        guard let statusCode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode) else {
            return
        }
        if statusCode == API.ErrorCode.Success {
//            if let profileResponse = response.response["data"] as? [[String:Any]] {
//                var profileModelData = [Profile]()
//                for (index, _) in profileResponse.enumerated() {
//                    let profileDetails = profileResponse[index]
//                    profileModelData.append(Profile.fetchprofileDataObj(userDataDictionary:profileDetails))
//
//                }
//                self.profileDetails = profileModelData
//            }
            
            self.createPostVM_resp.onNext((.likeOrUnlikeUserPost,true))
        } else {
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            self.createPostVM_resp.onNext((.likeOrUnlikeUserPost,false))
        }
    }
    
    /// getLikers API
    ///
    /// - Parameter response: userPostId
    func gettingLikersName(userPostId : String){
        // let data = self.profileDetails[index]
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        let createPostAPICall = CreatePostAPICalls()
        
        if !createPostAPICall.subject_response.hasObservers {
            createPostAPICall.subject_response
                .subscribe(onNext: {response in
                    Helper.hideProgressIndicator()
                    self.handleGetAllLikersName(response: response)
                }, onError: {error in
                    Helper.hideProgressIndicator()
                    self.message = error.localizedDescription
                    self.createPostVM_resp.onError(error)
                })
                .disposed(by:disposeBag)
        }
        createPostAPICall.requestForGetLikersName(postParams: userPostId)
        
    }
    
    /// handle getLikers response
    ///
    /// - Parameter response: response Model
    func handleGetAllLikersName(response: ResponseModel){
        guard let statusCode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode) else {
            return
        }
        if statusCode == API.ErrorCode.Success {
            if let momentDataResponse = response.response["data"] as? [[String:Any]] {
                self.momentDetailsArray = momentDataResponse.map({ (data) -> MomentsModel in
                    MomentsModel(momentsData: data)
                })
            }
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            self.createPostVM_resp.onNext((.getLikers,true))
        } else {
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            self.createPostVM_resp.onNext((.getLikers,false))
        }
    }
    
    
    
    /// Delete Post Api
    func deleteUserPost(){
        
        // let data = self.profileDetails[index]
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        let createPostAPICall = CreatePostAPICalls()
        let params = [ ServiceInfo.postId : postId] as [String:Any]
        if !createPostAPICall.subject_response.hasObservers {
            createPostAPICall.subject_response
                .subscribe(onNext: {response in
                    Helper.hideProgressIndicator()
                    self.handleDeletePostResponse(response: response)
                }, onError: {error in
                    Helper.hideProgressIndicator()
                    self.message = error.localizedDescription
                    self.createPostVM_resp.onError(error)
                })
                .disposed(by:disposeBag)
        }
        createPostAPICall.deleteUserPost(requestData: params)
    }
    
    
    /// DeletePostApi Response
    ///
    /// - Parameter response: response
    func handleDeletePostResponse(response: ResponseModel){
        guard let statusCode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode) else {
            return
        }
        if statusCode == API.ErrorCode.Success {
            
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            self.createPostVM_resp.onNext((.deleteUserPost,true))
        } else {
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            self.createPostVM_resp.onNext((.deleteUserPost,false))
        }
    }
    
    //  method for getting report reasons.
    func requestToGetReportPostReasons(userClicked:Bool) {
        
        let createPostAPICall = CreatePostAPICalls()
        createPostAPICall.requestToGetReportPostReasons()
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        if(userClicked) {
            Helper.showProgressIndicator(withMessage:StringConstants.getReportReasons())
        }
        
        createPostAPICall.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                self.handleReportPostReasonsResponse(response: response)
                
            }, onError: {error in
                self.createPostVM_resp.onError(error)
            })
            .disposed(by:disposeBag)
    }

    
  
    //MARK: - API RESPONSE HANDLER - GET REPORT POST Reasons
    /// method for handling reportReason api response.
    ///
    /// - Parameter response: contains api response and status code.
    func handleReportPostReasonsResponse(response:ResponseModel) {
        
        switch (response.statusCode) {
        case 200:
            
            // 200 is for success response (contains profiles)
            // 412 is for when there is no profiles.
            var reportReasonData = [ReportReason]()
            if let profileResponse = response.response["data"] as? [AnyObject] {
                for (index, _) in profileResponse.enumerated() {
                    if let reason = profileResponse[index] as? String {
                        reportReasonData.append(ReportReason(reportDettails:reason))
                    }
                }
            }
            
            self.reportReasonsArray = reportReasonData
            
            //reportReasonsArray
            self.createPostVM_resp.onNext((.getReportReasons, true))
            
        default: break
            
        }
        
        Helper.hideProgressIndicator()
    }
    
    
    
    
}


