//
//  DatumChatViewModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 07/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DatumChatViewModel: NSObject {
    
    
    let disposeBag = DisposeBag()
    let chatVM_resp = PublishSubject<Bool>()
    var coinBalance:CoinBalanceModel? = nil
    
    /// get coin balance
    func getCoinBalance(){
        
        let apiCalls = CoinsAPICalls()
        
        apiCalls.requestForGetCoinBalance()
        apiCalls.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                
                if let data = response.response[ApiResponseKeys.data] as? [String:Any] {
                    self.coinBalance = CoinBalanceModel.init(data: data)
                    Helper.saveCoinBalance(coinBalance: (self.coinBalance?.coin)!)
                }
                
                self.chatVM_resp.onNext(true)
            }, onError: {error in
                self.chatVM_resp.onError(error)
            })
            .disposed(by:disposeBag)
    }
    
    
    
}
