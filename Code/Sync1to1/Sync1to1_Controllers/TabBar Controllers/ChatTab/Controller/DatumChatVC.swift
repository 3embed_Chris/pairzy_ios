//
//  DatumChatVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 07/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher
import RxSwift
import VGPlayer
import IJKMediaFramework

class DatumChatVC: UIViewController {
    
    @IBOutlet weak var topBackgroundView: UIView!
    @IBOutlet weak var matchLabel: UILabel!
    @IBOutlet weak var topProfileCollectionView: UICollectionView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var tocollectionHight: NSLayoutConstraint!
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var balanceBtn: UIButton!
    @IBOutlet weak var matchesTableView: UITableView!
    @IBOutlet weak var matchesLabel: UILabel!
    @IBOutlet weak var matchCountLabel: UILabel!
    @IBOutlet weak var matchesButtonOutlet: UIButton!
    @IBOutlet weak var messagesLabel: UILabel!
    @IBOutlet weak var messagesButtonOutlet: UIButton!
    @IBOutlet weak var btnHighlightVieew: UIView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var leadingConstraintOfSelectedBtnView: NSLayoutConstraint!
    @IBOutlet weak var noMatchesView: UIView!
    @IBOutlet weak var createPostButton: UIButton!
    @IBOutlet weak var uploadProgressView: UIView!
    @IBOutlet weak var uploadingStatusLabel: UILabel!
    @IBOutlet weak var uploadingPercentageLabel: UILabel!
    @IBOutlet weak var newPostedImageView: UIImageView!
    @IBOutlet weak var uploadingProgressView: UIProgressView!
    @IBOutlet weak var uploadingViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var uploadPostView: UIView!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    
    //MARK:- Variables And Declarations
    var currentIndex: Int = -1
    var visibleIP: IndexPath?
    var aboutToBecomeInvisibleCell: Int = -1
    var playingIndex: Int = -1
    //var player : VGPlayer?
    var profileForMomentSelected = Profile.fetchprofileDataObj(userDataDictionary: [:])
    let createPostVM = CreatePostViewModel()
    let commentPostVM   = CommentViewModel()
    let profileDetailVM = ProfileDeatilVM()
    var disposeBag = DisposeBag()
    var chatListViewModel: ChatListViewModel!
    var receiverId:String?
    /// matchesVm is the view model object for matches
    let matchesVm = MatchMakerVM()
    var chatRefreshControl: UIRefreshControl = UIRefreshControl()
    var matchesRefreshControl: UIRefreshControl = UIRefreshControl()
    var currentLabel = 0
    let rowCount = 0
    var mediaURl: String = ""
    var typeFlag:Int = 3
    let chatVM = DatumChatViewModel()
    weak var playingCell:PostImageCVCell?
    var newPostData:NewPostModel?
    var player:IJKFFMoviePlayerController!
    var currentCell: MatchUserPostTVCell?
    let userData = Database.shared.fetchResults()


    /// Opening YPImagePicker Library for Selecting Photos And Videos
    ///
    /// - Parameter sender: CreatePost Btn
    @IBAction func createPostBtnAction(_ sender: UIButton) {
        let picker = YPImagePicker()
        DispatchQueue.main.async {
            self.present(picker, animated: true, completion: nil)
        }
 
    }
    
    
    /// Opening Messages tab
    ///
    /// - Parameter sender: MessageBtn
    @IBAction func messagesButtonAction(_ sender: UIButton) {
        
        buttonDesignOnAction(mainTableView, [sender,messagesButtonOutlet], self)
        messagesLabel.textColor = Colors.AppBaseColor
        matchesLabel.textColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
        createPostButton.isHidden = true
        self.requestForMatches()
        self.mainTableView.reloadData()
    }
    
    
    @IBAction func viewProfileButtonAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: SegueIds.ChatVCToProfileVC, sender: nil)

    }
    
    
    /// Opening NewsFeed tab
    ///
    /// - Parameter sender: MatchesBtn
    @IBAction func matchesButtonAction(_ sender: UIButton) {
        buttonDesignOnAction(matchesTableView, [sender,matchesButtonOutlet], self)
        matchesLabel.textColor = Colors.AppBaseColor
        messagesLabel.textColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
        createPostButton.isHidden = false
        self.createPostVM.getUserPostBioOfMatches()
        self.matchesTableView.reloadData()
    }
    
 

    func updateChatListCount() {
        let count = self.chatListViewModel.unreadChatsCounts()
        if count == 0 {
            self.tabBarController?.tabBar.items?[4].badgeValue = nil
        } else {
            
            self.tabBarController?.tabBar.items?[4].badgeValue = "\(count)"
            
        }
        UserDefaults.standard.setValue(count, forKey:"unreadChatCount")
        UserDefaults.standard.synchronize()
    }
    
    func setUpProfileImageUI(profileImage:UIImageView){
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        profileImage.layer.borderWidth = 1
        profileImage.layer.borderColor = UIColor.white.cgColor
    }
    
    func setProfileImage(profileImage:UIImageView){
        if let userDetails = userData.first{
            if let urlString:String = userDetails.profilePictureURL {
                if urlString.count>0 {
                    let url = URL(string: urlString)
                    var imageName = "man_default"
                    if userDetails.gender == "Male" {
                        imageName = "man_default"
                    }else {
                        imageName = "woman_default"
                    }
                    self.profileImageView.kf.setImage(with:url, placeholder:UIImage.init(named:"02m"))
                }else{
                    profileImage.image = #imageLiteral(resourceName: "Splash")
                }
            }
        }
        
    }
    
    // MARK: - Methodds
    /// Methods for Updating The UI
    func updateUI() {
        matchCountLabel.layer.cornerRadius = matchCountLabel.frame.size.width/2
        matchCountLabel.layer.masksToBounds = true
        matchCountLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        matchCountLabel.text = String(10)
        
    // matchesTableView.backgroundColor = Helper.getUIColor(color:"F8F8F8")
       updateNavigation()
        //setProfileImage()
       // setUpProfileImageUI()
    }
    /// Updating Navigation Bar
    func updateNavigation() {
        self.navigationController?.navigationBar.isHidden = false
    }
    
//    ///    Update Profile Image UI
//    func setUpProfileImageUI() {
//        matchUserImageView.layer.cornerRadius = matchUserImageView.frame.height/2
//        matchUserImageView.layer.masksToBounds = true
//        matchUserImageView.layer.borderWidth = 1
//        matchUserImageView.layer.borderColor = UIColor.white.cgColor
//    }
    
  
    /// Setting Default Image For Profile Image
    func setProfileImage() {
        if let userDetails = Database.shared.fetchResults().first {
            if let urlString:String = userDetails.profilePictureURL {
                if urlString.count>0 {
                    let url = URL(string: urlString)
                    var imageName = "man_default"
                    if userDetails.gender == "Male" {
                        imageName = "man_default"
                    }else {
                        imageName = "woman_default"
                    }
                    //matchUserImageView.kf.setImage(with: url, placeholder: UIImage.init(named: imageName), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
                    //backgroundImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "user_default_image"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
                }else{
                   // matchUserImageView.image = #imageLiteral(resourceName: "Splash")
                    //  backgroundImageView.image = #imageLiteral(resourceName: "Splash")
                }
            }
        }
    }
    
    

    
    struct  Constants {
        static let cellIdentifier = "ChatListTableViewCell"
        static let chatSegueIdentifier = "segueToChatViewController"
    }
    
    
    //    MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
        addObservers()
        self.uploadingProgressView.progress = 0
        self.uploadingPercentageLabel.text = "0%"
        self.uploadingViewHeightConstraint.constant = 0
        createPostButton.isHidden = true
        matchesLabel.text = StringConstants.newsfeed()
        messagesLabel.text = StringConstants.chatActivity()
        didGetResponse()
        matchesVm.checkMatchesDataFromDB(matchesCollectionView:self.topProfileCollectionView, chatTableView: self.mainTableView)
        requestGetUserPostApi()
        self.requestForReportPostReasons(userClicked: false)
        initialSetup()
        balanceBtn.layer.borderColor = Colors.coinBalanceBtnBorder.cgColor
        balanceBtn.layer.borderWidth = 1
        balanceBtn.layer.cornerRadius = 5
        balanceBtn.layer.cornerRadius = balanceBtn.frame.size.height/2
        self.navigationItem.title = StringConstants.chat()
        mainTableView.backgroundColor = Helper.getUIColor(color:"F8F8F8")
        topProfileCollectionView.backgroundColor = Helper.getUIColor(color:"F8F8F8")
        matchLabel.text = StringConstants.matches()
        matchesTableView.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden =  false
        chatVM.getCoinBalance()
        matchesVm.checkMatchesDataFromDB(matchesCollectionView:self.topProfileCollectionView, chatTableView: self.mainTableView)
        updateHeight()
        requestAPIS()
       // requestBioPostAPIS()
        updateChatList()
        reloadTableView()
        updateCoinBalance()
        updateUI()
        setProfileImage(profileImage: profileImageView)
        setUpProfileImageUI(profileImage: profileImageView)
        let count = self.chatListViewModel.unreadChatsCounts()
        if count > 0 {
            messagesButtonAction(UIButton())
        }
        
      //  createPostVM.getUserPostBioOfMatches()
        
    }

    
    /// Coin Balance Updation
    @objc func updateCoinBalance(){
        var balance = " 0"
        if let getBal = UserDefaults.standard.object(forKey: "coinBalance") as? Int {
            if getBal > 1000 {
                let floatVal = Float(Float(getBal)/1000.0)
                let inter = floor(floatVal)
                let decimal = floatVal.truncatingRemainder(dividingBy: 1)
                if decimal == 0.0{
                    balance = "\(Int(inter))k"
                }else {
                    balance = "\(Float(Float(getBal)/1000.0))k"
                }
            } else {
                balance = " " + String(getBal)
            }
            balanceBtn.setTitle(balance, for: .normal)
        } else {
            balanceBtn.setTitle(" 0", for: .normal)
        }
    }
    
    func addObservers() {
        var nameOfObserVer = NSNotification.Name(rawValue: "SyncChatNotification")
        NotificationCenter.default.addObserver(self, selector: #selector(getChat(notification:)), name: nameOfObserVer, object: nil)
        nameOfObserVer = NSNotification.Name(rawValue: "openChatForProfileForPushNotification")
        NotificationCenter.default.addObserver(self, selector: #selector(openChatForpushMessage(notification:)), name: nameOfObserVer, object: nil)
        
        nameOfObserVer = NSNotification.Name(rawValue: "ChatUpdatedNotification")

        NotificationCenter.default.addObserver(self, selector: #selector(updateChatList), name: nameOfObserVer, object: nil)
        nameOfObserVer = NSNotification.Name(rawValue: "unmatchedUser")
        NotificationCenter.default.addObserver(self, selector: #selector(requestAPIS), name: nameOfObserVer, object: nil)
        nameOfObserVer = NSNotification.Name(rawValue: "updateCoinsBal")
        NotificationCenter.default.addObserver(self, selector: #selector(updateCoinBalance), name: nameOfObserVer, object: nil)
//        nameOfObserVer = NSNotification.Name(rawValue: "PostUpdateNotification")
//        NotificationCenter.default.addObserver(self, selector: #selector(postUpdateNotification), name: nameOfObserVer, object: nil)
        nameOfObserVer = NSNotification.Name(rawValue: "NewsFeedPostUpdateNotification")
        NotificationCenter.default.addObserver(self, selector: #selector(newsFeedPostUpdateNotification), name: nameOfObserVer, object: nil)
        //adding observer to refresh match data when any new match occurs
        NotificationCenter.default.addObserver(self, selector: #selector(requestAPIS), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForUnmatchProfile), object: nil)
        //adding observer to refresh match data when any new match oocurs
        NotificationCenter.default.addObserver(self, selector: #selector(requestAPIS), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForMatchUser), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(uploadNewPost(_:)), name: NSNotification.Name(rawValue: NSNotificationNames.createNewPost), object: nil)
    }
    
    
    @objc func openChatForpushMessage(notification: NSNotification) {
        
        // dont open chat screen if chat already opens
        if(Helper.isChatScreenOpened()) {
            return
        }
        
        guard let userInfo = notification.userInfo as? [String : Any] else { return }
        guard let matchObjUserId = userInfo["receiverID"] as? String else { return }
        let controller = Helper.getSBWithName(name: "Chat").instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        // fetch profile object from available getchats data.
        
        
        let allProfiles = self.matchesVm.profilesWithOutChat + self.matchesVm.profilesWithChat
        for(_,value) in  allProfiles.enumerated() {
            if(value.userId == matchObjUserId) {
                controller.favoriteObj = value
                self.navigationController?.pushViewController(controller, animated:true)
                break
            }
        }
    }
    
    @objc func getChat(notification: NSNotification) {
        
        // dont open chat screen if chat already opens
        if(Helper.isChatScreenOpened()) {
            return
        }
        
        guard let userInfo = notification.userInfo as? [String : Any] else { return }
        guard let matchObj = userInfo["receiverID"] as? Profile else { return }
        let controller = Helper.getSBWithName(name: "Chat").instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        // fetch profile object from available getchats data.
        
        
        let allProfiles = self.matchesVm.profilesWithOutChat + self.matchesVm.profilesWithChat
        
        var isProfileObjFound = false
        for(_,value) in  allProfiles.enumerated() {
            if(value.userId == matchObj.userId) {
                isProfileObjFound = true
                controller.favoriteObj = value
                self.navigationController?.pushViewController(controller, animated:true)
                break
            }
        }
        if(!isProfileObjFound) {
            controller.favoriteObj = matchObj
            self.navigationController?.pushViewController(controller, animated:true)
        }
    }
    
    @objc func updateChatList() {
        let couchbaseObj = Couchbase.sharedInstance
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        guard let chats = chatsDocVMObject.getChatsFromCouchbase() else { return }
        let sortedChats = chats.sorted {
            return $0.lastMessageDate > $1.lastMessageDate
        }
        self.chatListViewModel = ChatListViewModel(withChatObjects: sortedChats)
        self.chatListViewModel.setupNotificationSettings()
        self.chatListViewModel.authCheckForNotification()
        
        self.updateChatListCount()

        reloadTableView()
    }
    
 
    @IBAction func chatButtonAction(_ sender: UIButton) {
        let btn = sender
        let controller = Helper.getSBWithName(name: "Chat").instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        var tempProfile = Profile.fetchprofileDataObj(userDataDictionary:[:])
        tempProfile.userId = self.createPostVM.momentDetailsArray[btn.tag].userId
        tempProfile.userName = self.createPostVM.momentDetailsArray[btn.tag].userName
        tempProfile.profilePicture = self.createPostVM.momentDetailsArray[btn.tag].profilePic
        controller.favoriteObj = tempProfile
        controller.favoriteObj?.isMatchedUser = 1
        self.navigationController?.pushViewController(controller, animated:true)
    }
    
   
    
    @IBAction func profileButtonAction(_ sender: UIButton) {
        
        let btn = sender
        var tempProfile = Profile.fetchprofileDataObj(userDataDictionary:[:])
        tempProfile.userId = self.createPostVM.momentDetailsArray[btn.tag].userId
        let profileVc = Helper.getSBWithName(name:"DatumTabBarControllers").instantiateViewController(withIdentifier:StoryBoardIdentifier.profileVc) as! ProfileDetailVC
        profileVc.profileDeatilVM.dataAvailable = false
        profileVc.profileDeatilVM.showProfileByID = false
        profileVc.profileFromChat = true
        profileVc.profileDeatilVM.profileDetails.userId = tempProfile.userId
        self.navigationController?.present(profileVc, animated: true, completion:nil)
    }


    @IBAction func deletePostAction(_ sender: UIButton) {
        let btn = sender
        self.createPostVM.postId = self.createPostVM.momentDetailsArray[btn.tag].postId
        let alert:UIAlertController=UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let deletePost = UIAlertAction(title: StringConstants.DeletePost(), style: UIAlertActionStyle.default, handler: {(Bool) in
            self.showConfirmDeleteAlert(index: btn.tag)
        })
        let reportPost = UIAlertAction(title: StringConstants.report(), style: UIAlertActionStyle.default, handler: {(Bool) in
            self.getReportReasons()
        })
        let cancelAction = UIAlertAction(title: StringConstants.cancel(), style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        deletePost.setValue(UIColor.red, forKey: "titleTextColor")
        reportPost.setValue(UIColor.red, forKey: "titleTextColor")
        //var tempProfile = Profile.fetchprofileDataObj(userDataDictionary:[:])
        if !userData.isEmpty{
            if let userDetails = userData.first {
                let userId = userDetails.userId
                if self.createPostVM.momentDetailsArray[btn.tag].userId != userId{
                    alert.addAction(reportPost)
                }else{
                    alert.addAction(deletePost)
                }
                
            }
        }
        
        
        
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func getReportReasons(){
        if(self.createPostVM.reportReasonsArray.count == 0) {
            self.requestForReportPostReasons(userClicked: true)
        } else {
            self.showReportReasons()
        }
    }
    
    
//    func showReportReasons() {
//
//        let alert:UIAlertController=UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
//        let reportAsSpam = UIAlertAction(title: StringConstants.reportAsSpam(), style: UIAlertActionStyle.default, handler: {(Bool) in
//
//        })
//        let reportAsInappropriate = UIAlertAction(title: StringConstants.reportAsInappropriate(), style: UIAlertActionStyle.default, handler: {(Bool) in
//
//        })
//        let cancelAction = UIAlertAction(title: StringConstants.cancel(), style: UIAlertActionStyle.cancel)
//        {
//            UIAlertAction in
//        }
//
//        alert.addAction(reportAsSpam)
//        alert.addAction(reportAsInappropriate)
//        alert.addAction(cancelAction)
//        self.present(alert, animated: true, completion: nil)
//    }
    
    
    /// method for requesting second look profiles.
    func requestForReportPostReasons(userClicked:Bool) {
        Helper.showProgressIndicator(withMessage: "")
        self.createPostVM.requestToGetReportPostReasons(userClicked:userClicked)
        self.createPostVM.createPostVM_resp
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                
                if (userClicked) {
                    //show reasons popup
                    if(self.createPostVM.reportReasonsArray.count > 0) {
                        self.showReportReasons()
                    }
                }
            }, onError:{ error in
                
            })
            
            .disposed(by:createPostVM.disposeBag)
    }
    
    
    /// to fetch list of all the report reasons.
    ///
    /// - Parameter selectedUser: selected profile to report.
    func showReportReasons() {
        
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = DeactivateAccountReasonsView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.isViewForReason = true
        videoView.loadDeactivateView()
        videoView.reportReasons = self.createPostVM.reportReasonsArray
        videoView.deactDelegate = self
        window.addSubview(videoView)
    }
    
    
    func showConfirmDeleteAlert(index: Int) {
        
        let  msgForAlert = StringConstants.confirmDelete()
        let buttonText = StringConstants.delete()
        
        let alert = UIAlertController(title: nil, message: msgForAlert, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: StringConstants.actionSheetCancelText(), style: .cancel, handler: {(_) in }))
        alert.addAction(UIAlertAction(title: buttonText, style: .default, handler: {(action:UIAlertAction!) in
            //API Call for DELETE Post
            self.currentIndex = index
            self.createPostVM.deleteUserPost()
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openMyLikesVC() {
        let profileVc = self.storyboard?.instantiateViewController(withIdentifier: "toMyLikesVC") as! MyLikesVC
        self.navigationController?.present(profileVc, animated: true, completion:nil)
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func reloadTableView() {
        self.mainTableView.reloadData()
       // self.matchesTableView.reloadData()
    }
    
  
    
    
    /// To start again player which has finished
    @objc func startAgainPlaying(){
        print("stop playing")
        guard let currentPlayer = self.player else{return}
        currentPlayer.prepareToPlay()
        currentPlayer.play()
    }
    
    /// To monitor player status changed
    @objc func playingStateChange(){
        guard let currentPlayer = self.player else{return}
        switch currentPlayer.playbackState {
        case .playing:
            print("Start playing")
            //            currentPlayer.play()
            break
        case .stopped:
            print("Stop playing")
            break
        case .interrupted:
            print("playing Interrupted")
            break
        case .paused:
            print("playing pause")
            break
        case .seekingForward, .seekingBackward:
            break
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        chatRefreshControl.didMoveToSuperview()
        matchesRefreshControl.didMoveToSuperview()
    }

    @IBAction func balanceBtnAction(_ sender: Any) {
        //checking coins plans details from  appstore available or not.
        if(Helper.getCoinPLansFromAppStore().count > 0) {
            let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "addCoin") as! AddCoinsVC
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(userBasics, animated: true)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func postUpdateNotification(){
        requestGetUserPostApi()
    }
    @objc func newsFeedPostUpdateNotification(){
        matchesTableView.isHidden = false
        matchesLabel.textColor = Colors.AppBaseColor
        messagesLabel.textColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
        leadingConstraintOfSelectedBtnView.constant = 0.0 + ((self.view.bounds.width) * 0.50)
        var frame: CGRect = self.mainScrollView.bounds
        frame.origin.x = (1 * self.view.frame.size.width)
        mainScrollView.scrollRectToVisible(frame, animated: true)
        requestGetUserPostApi()
    }
    
    
    
}

// MARK: - Methodds
extension DatumChatVC {
    @objc func uploadNewPost(_ notification: NSNotification) {
        self.tabBarController?.selectedIndex = 4
        if let newpost = notification.object as? NewPostModel {
            self.newPostData = newpost
            DispatchQueue.main.async {
                self.uploadingProgressView.progress = 0.0
                self.uploadingPercentageLabel.text = "0%"
            }
            if newpost.isVideo {
                self.uploadingStatusLabel.text = "Uploading Video ... "
                self.uploadVideoToCloudinaryandCreateUserPost(video: URL(string: newpost.mediaPath)!)
            } else {
                self.uploadingStatusLabel.text = "Uploading Image ..."
                self.uploadImageToCloudinaryandCreateUserPost(image: newpost.image!)
            }
        }
    }
    
    
    /// Uploading video to Cloudinary and after getting url calling post API for creating post
    ///
    /// - Parameter image: videourl
    func uploadVideoToCloudinaryandCreateUserPost(video: URL) {  //deu1yq6y6
        
        self.uploadingViewHeightConstraint.constant = 75
        self.newPostedImageView.image = newPostData?.image

        let lang = Helper.getCludinaryDetails()
        let cloudinary = CLDCloudinary(configuration: CLDConfiguration(cloudName: lang["cloudName"] as! String, secure: true))
        //self.uploadingProgress.setProgress(uploadedProgress, animated: true)
        let params = CLDUploadRequestParams()
        params.setResourceType(CLDUrlResourceType.video)
        let timestamp = Helper.getCurrentTimeStamp()
        params.setPublicId("\(timestamp)")
        // Helper.showProgressIndicator(withMessage: StringConstants.uploading())
        // Helper.showProgressWithoutBackg(withMessage: "")
        do {
            cloudinary.createUploader().upload(url: video, uploadPreset: lang["uploadPreset"] as! String, params: params, progress: { (progress) in
                
                DispatchQueue.main.async {
                    let uploadedProgress = Float (Float(progress.completedUnitCount) / Float(progress.totalUnitCount))
                    let progress = Int(uploadedProgress * 100)
                    self.uploadingPercentageLabel.text = "\(progress)%"
                    self.uploadingProgressView.setProgress(uploadedProgress, animated:true)
                }
                
            }, completionHandler: { (result, error) in
                Helper.hideProgressIndicator()
                DispatchQueue.main.async {
                    self.uploadingViewHeightConstraint.constant = 0
                    if error != nil {
                        print("failed to upload")
                    } else {
                        if let result = result{
                            var uploadedVideoUrl = result.resultJson["url"] as? String
                            uploadedVideoUrl = uploadedVideoUrl?.replace(target: "video/upload", withString: "video/upload/q_60")
                            self.videoUploadResponseHandler(url: uploadedVideoUrl!)
                            // Calling API for creating Post
                            self.createPostVM.postUserPostBio()
                            
                        }
                    }
                }
            })
        }
    }
    
    
    /// Uploading Image to Cloudinary and after getting url calling post API for creating post
    ///
    /// - Parameter image: UIImage
    func uploadImageToCloudinaryandCreateUserPost(image: UIImage) {
        
        self.uploadingViewHeightConstraint.constant = 75
        self.newPostedImageView.image = image
        let lang = Helper.getCludinaryDetails()
        
        let cloudinary = CLDCloudinary(configuration: CLDConfiguration(cloudName:lang["cloudName"] as! String
            , secure: true))
        
        let params = CLDUploadRequestParams()
        params.setResourceType(CLDUrlResourceType.image)
        let data = UIImageJPEGRepresentation(image, 1.0)
        let timestamp = Helper.getCurrentTimeStamp()
        params.setPublicId("\(timestamp)")
        // Helper.showProgressIndicator(withMessage: StringConstants.uploading())
        // Helper.showProgressWithoutBackg(withMessage: "")
        cloudinary.createUploader().upload(data: data!, uploadPreset: lang["uploadPreset"] as! String, params: params, progress:{ (progress) in
            DispatchQueue.main.async {
                let uploadedProgress = Float (Float(progress.completedUnitCount) / Float(progress.totalUnitCount))
                let progress = Int(uploadedProgress * 100)
                self.uploadingPercentageLabel.text = "\(progress)%"
                self.uploadingProgressView.setProgress(uploadedProgress, animated: true)
            }
        }, completionHandler: { (result, error) in
            Helper.hideProgressIndicator()
            DispatchQueue.main.async {
                self.uploadingViewHeightConstraint.constant = 0

                if error != nil {
                    
                    
                } else {
                    if let result = result{
                       // http://res.cloudinary.com/pipelineai/image/upload/w_500,h_500,c_scale/v1566373241/20190821011025PM.jpg
                        guard let uploadedVideoUrl = result.resultJson["url"] as? String else  {return}
                        let replacedUrl = uploadedVideoUrl.replacingOccurrences(of: "upload/", with: "upload/w_1000,h_1000/")
                        self.imageUploadResponseHandler(url: replacedUrl)
                        // Calling API for creating Post
                        self.createPostVM.postUserPostBio()

                    }
                }
            }
        })
    }
 
    
    func imageUploadResponseHandler(url: String){
        self.createPostVM.typeFlag = 3
        self.createPostVM.caption = ((self.newPostData?.caption)!)
        self.createPostVM.mediaURl = url
    }
    
    func videoUploadResponseHandler(url: String){
        self.createPostVM.mediaURl = url
        self.createPostVM.typeFlag = 4
        self.createPostVM.caption = ((self.newPostData?.caption)!)
        self.typeFlag = 4
    }
    
    func initialSetup(){
        chatRefreshControl.tintColor = Colors.AppBaseColor
        matchesRefreshControl.tintColor = Colors.AppBaseColor
        chatRefreshControl.addTarget(self, action: #selector(self.refreshMainTV), for: UIControlEvents.valueChanged)
        matchesRefreshControl.addTarget(self, action: #selector(self.refreshMatchesTV), for: UIControlEvents.valueChanged)
//        mainTableView.addSubview(chatRefreshControl)
//        matchesTableView.addSubview(matchesRefreshControl)
        if #available(iOS 10.0, *) {
            mainTableView.refreshControl = self.chatRefreshControl
        } else {
            mainTableView.addSubview(self.chatRefreshControl)
        }
        
        if #available(iOS 10.0, *) {
            matchesTableView.refreshControl = self.matchesRefreshControl
        } else {
            matchesTableView.addSubview(self.matchesRefreshControl)
        }

    }
    
    /// Changing Title Color of Button
    ///
    /// - Parameter sender: Tag Of Button using Index
    func buttonHighlightDesign(_ sender: [UIButton]) {
        sender[0].setTitleColor(Colors.AppBaseColor, for: .normal)
        sender[1].setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .normal)
    }
   
    fileprivate func stopPlayingCurrentVideo(){
        if let player = self.playingCell?.localPlayer{
            player.pause()
            player.shutdown()
            player.view.removeFromSuperview()
            self.player = nil
            self.playingIndex = -1
        }
    }
    
//    fileprivate func play(_ url: URL?, cell: MatchUserPostTVCell) {
//            let options = IJKFFOptions.byDefault()
//            self.player = IJKFFMoviePlayerController(contentURL: url!, with: options)
//            self.player.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//            self.player.view.frame = cell.postImageCollectionView.bounds
//            self.player.scalingMode = .aspectFill
//            self.player.shouldAutoplay = true
//            cell.autoresizesSubviews = true
//            cell.postImageCollectionView.addSubview((self.player.view)!)
//   }
    
     @objc func requestAPIS() {
        self.requestForMatches()
    }
    
    @objc func requestGetUserPostApi() {
        self.createPostVM.getUserPostBioOfMatches()
    }
    
    @objc func refreshMainTV(senderRC: UIRefreshControl) {
        self.requestForMatches()
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            senderRC.endRefreshing()
        }
    }
    
    @objc func refreshMatchesTV(senderRC: UIRefreshControl) {
        self.requestGetUserPostApi()
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            senderRC.endRefreshing()
        }
    }

    func updateHeight(){
        
        UIView.animate(withDuration: 0.3, animations: {
            
            if self.matchesVm.profilesWithOutChat.count == 0 {
               self.tocollectionHight.constant = 0
            }else {
                self.tocollectionHight.constant = 160
            }
        })
        self.view.layoutIfNeeded()
        self.view.updateConstraints()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.chatRefreshControl.endRefreshing()
        self.matchesRefreshControl.endRefreshing()
        stopPlayingCurrentVideo()
        
    }
    
    /// For Updating View For Tab
    ///
    /// - Parameters:
    ///   - table: matchesTableView && mainTableView
    ///   - sender: Array of [Button]
    ///   - selfVC: viewController
    func buttonDesignOnAction(_ table: UITableView, _ sender: [UIButton], _ selfVC: UIViewController) {
        switch table {
            
        case mainTableView:
            mainTableView.isHidden = false
            leadingConstraintOfSelectedBtnView.constant = 0.0
            var frame: CGRect = self.mainScrollView.bounds;
            frame.origin.x = (0 * self.view.frame.size.width)
            mainScrollView.scrollRectToVisible(frame, animated: true)
            buttonHighlightDesign(sender)
            
        case matchesTableView:
            matchesTableView.isHidden = false
            leadingConstraintOfSelectedBtnView.constant = 0.0 + ((self.view.bounds.width) * 0.50)
            var frame: CGRect = self.mainScrollView.bounds
            frame.origin.x = (1 * self.view.frame.size.width)
            mainScrollView.scrollRectToVisible(frame, animated: true)
            buttonHighlightDesign(sender)
        default:
            break
        }
    }
    /// method for matches.
    func requestForMatches(){
        matchesVm.requestForMatches()
        matchesVm.matchMakersubject_responser
            .subscribe(onNext: {response in
                if(self.matchesVm.profilesWithOutChat.count == 0) {
                    //  self.nomatchesFoundBackGround()
                }
                self.chatRefreshControl.endRefreshing()
                self.topProfileCollectionView.reloadData()
                self.mainTableView.reloadData()
            }, onError:{ error in
                
                
            }).disposed(by:matchesVm.disposeBag)
    }
    
    /// method for matches.
    func requestForGetMatchesPost() {
        
        createPostVM.getUserPostBioOfMatches()
        createPostVM.createPostVM_resp
            .subscribe(onNext: {response in
                if(self.createPostVM.momentDetailsArray.count == 0) {
                    //  self.nomatchesFoundBackGround()
                }
                self.matchesRefreshControl.endRefreshing()
                self.matchesTableView.reloadData()
            }, onError:{ error in
                
            })
            .disposed(by:createPostVM.disposeBag)
    }
    
    func checkForContent(){
        if self.rowCount == 0 {
            mainTableView.backgroundColor = UIColor.clear
        }
        else {
            mainTableView.backgroundColor = UIColor.white
        }
    }
    
   
    func didGetResponse(){
        
            createPostVM.createPostVM_resp.subscribe(onNext: { [weak self] success in
                guard let self  =  self else {return}
                switch success.0 {
             case CreatePostViewModel.responseType.getUserPost:
                
                if success.1 {
                    self.matchesTableView.isHidden = false
                    self.noMatchesView.isHidden = true
                    self.matchesRefreshControl.endRefreshing()
                    self.matchesTableView.reloadData()
                }else{
                    /// Showing NoFriendView if FriendList Count is zero
                    self.matchesTableView.isHidden = true
                    self.noMatchesView.isHidden = false
                    }
                case CreatePostViewModel.responseType.deleteUserPost:
                    if success.1 {
                       //Reload TableView After Getting response
                        self.createPostVM.momentDetailsArray.remove(at: self.currentIndex)
                        self.matchesTableView.reloadData()
                    }
                case CreatePostViewModel.responseType.createUserPost:
                    if success.1 {
                        //Reload TableView After Getting response
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                            self.requestGetUserPostApi()
                        })
                    }
                default:
                    break
                }
            }).disposed(by: disposeBag)
    }
    
    
    /// To prepare and play video when cell is visiable
    ///
    /// - Parameters:
    ///   - cell: visible cell
    ///   - indexPath: index path of visiable cell
    func playVideoOnTheCell(cell : MatchUserPostTVCell, indexPath : IndexPath){
        
        if currentIndex == indexPath.row{
            return
        }
        currentIndex = indexPath.row
        let data = self.createPostVM.momentDetailsArray[indexPath.row]
        let collectionView = cell.postImageCollectionView
        guard let collectionViewCell = collectionView?.cellForItem(at: IndexPath(row: 0, section: 0)) as? PostImageCVCell else{return}
        
        if data.url[0].suffix(3) == "jpg" || data.url[0].suffix(3) == "png" || data.url[0].suffix(3) == "jpeg" || data.url[0].contains("facebook") {
            if collectionViewCell.localPlayer != nil{
                collectionViewCell.localPlayer.shutdown()
                collectionViewCell.localPlayer.view.removeFromSuperview()
                collectionViewCell.localPlayer = nil
            }
          
        }else{
            self.playingCell = collectionViewCell
            let videoUrl = URL(string: data.url[0])
            collectionViewCell.play(videoUrl, cell: cell)
            self.currentCell = cell
            collectionViewCell.soundVideoImage.image = #imageLiteral(resourceName: "Sound Off")
            collectionViewCell.localPlayer?.playbackVolume = 0.0
        }
        
    }
    
    /// To perform any operation when cell start moving from full frame
    ///
    /// - Parameters:
    ///   - cell: cell which is moving
    ///   - indexPath: index of that cell
    func stopPlayBack(cell : MatchUserPostTVCell, indexPath : IndexPath){
        let collectionView = cell.postImageCollectionView
        guard let collectionViewCell = collectionView?.cellForItem(at: IndexPath(row: 0, section: 0)) as? PostImageCVCell else{return}
        if self.playingIndex == indexPath.row{
            self.stopPlayingCurrentVideo()
            self.playingIndex = -1
        }
    }
}

// MARK: - UITableViewDelegate,UITableViewDataSource
extension DatumChatVC: UITableViewDelegate,UITableViewDataSource {
  
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch tableView {
        case matchesTableView:
            if (self.createPostVM.momentDetailsArray.count > 0){
                tableView.estimatedRowHeight = 44.0 // standard tableViewCell height
                tableView.rowHeight = UITableViewAutomaticDimension
                return UITableViewAutomaticDimension
                
            }else{
                return 0
            }
        default:
            if(self.matchesVm.profilesWithChat.count > 0) {
                if indexPath.row == 0 {
                    return 30
                }
                return 80
            } else {
                return 250
            }
        }
  
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch tableView {
        case matchesTableView:
            break
        default:
            if indexPath.row != 0 {
                let controller = Helper.getSBWithName(name: "Chat").instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                controller.favoriteObj = self.matchesVm.profilesWithChat[indexPath.row-1]
                self.navigationController?.pushViewController(controller, animated:true)
            }
        }
    }
    
    func updateMatchesApi() {
        requestAPIS()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row == 0{
            return false
        }
        return true
    }
    
    func confirmationForDeleteChat(indexpath:Int) {
        let alert = UIAlertController(title:nil, message: StringConstants.areYouSureToDelete(), preferredStyle: .alert)
        let selectedProfile = self.matchesVm.profilesWithChat[indexpath]

        alert.addAction(UIAlertAction(title: StringConstants.cancel(), style: .cancel, handler: { action in
           
        }))
        
        alert.addAction(UIAlertAction(title: StringConstants.yes(), style: .default, handler: { action in
            self.requestApiForDeleteChat(selectedProfile: selectedProfile)
            Helper.deleteUserChat(userDeatils: selectedProfile.userId)
            self.view.showErrorMessage(titleMsg: StringConstants.success(), andMessage:StringConstants.conversationDeleted())
            self.matchesVm.profilesWithChat.remove(at: indexpath)
            self.mainTableView.reloadData()
            
        }))
        // Show the alert with animation
        self.present(alert, animated: true)
    }
    
    func requestApiForDeleteChat(selectedProfile:Profile) {
        
        //API to delete chat.
        Helper.showProgressIndicator(withMessage: StringConstants.DeletingAccount())
        let params = ["chatId":selectedProfile.chatDocIDFromAPI]
        //DELETE /Messages/{all}/{messageIds}
        API.requestDELETEURL(serviceName: APINAMES.deleteChat,
                             withStaticAccessToken:false,
                             params: params,
                             success: { (response) in
                                Helper.hideProgressIndicator()
                                if (response.null != nil){
                                    return
                                }
                                
                                let statuscode = response.dictionaryObject!["code"] as! Int
                                switch (statuscode) {
                                case 200:
                                    
                                    Helper.deleteUserChat(userDeatils: selectedProfile.userId)
                                    self.requestAPIS()
                                    self.view.showErrorMessage(titleMsg: StringConstants.success(), andMessage:StringConstants.coversationDeleted())
                                   // self.moveToRootVc()
                                case 401:
                                    Helper.changeRootVcInVaildToken()
                                    break
                                default:
                                    self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: response.dictionaryObject![LoginResponse.errorMessage] as! String)
                                }
                                
        },
                             failure: { (Error) in
                                Helper.hideProgressIndicator()
                                self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: Error.localizedDescription)
        })
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        switch tableView {
        case matchesTableView:
            break
        default:
            if indexPath.row != 0 {
                if (editingStyle == UITableViewCellEditingStyle.delete) {
                    self.confirmationForDeleteChat(indexpath: indexPath.row-1)
                }
            }
        }
      
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 100))
        returnedView.backgroundColor = .clear
        return returnedView
        
    }
 
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch tableView {
        case matchesTableView:
            return 70
            break
        default:
           return 20
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch tableView {
        case matchesTableView:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MatchUserPostTVCell.self)) as? MatchUserPostTVCell else{fatalError()}
            let data = self.createPostVM.momentDetailsArray[indexPath.row]
            cell.momentPostModel = data
            cell.updateUIForPageControl()
            cell.profileButtonOutlet.tag = indexPath.row
            cell.chatButtonOutlet.tag = indexPath.row
            cell.deleteOptionBtnOutlet.tag  = indexPath.row
           // self.currentIndex = indexPath.row
            cell.setMatchUserPostData(userMomentModel: data)
            cell.index = indexPath.row
            cell.delegate = self
            
            return cell
        default:
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DatumChatHeader") as! SectionHeaderCell
                cell.activeChatLabel.text = StringConstants.activeCahts()
                cell.noActiveChats.text = StringConstants.chatDefaultText()
                cell.backgroundColor = tableView.backgroundColor
                return cell
            }else {
                let dataObject = self.matchesVm.profilesWithChat[indexPath.row-1]
                let cell = tableView.dequeueReusableCell(withIdentifier: "DatumChatTableCell") as! ChatListTableViewCell
                cell.profileVmObj =  dataObject
                cell.backgroundColor = Helper.getUIColor(color:"F8F8F8")
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case matchesTableView:
            return self.createPostVM.momentDetailsArray.count
        default:
            if self.chatListViewModel != nil {
                if self.matchesVm.profilesWithChat.count != 0 {
                    return self.matchesVm.profilesWithChat.count+1
                }else {
                    return 1
                }
            }
            return 1
        }
    }
    
    @objc func longTap(_ sender: UIGestureRecognizer){
        print("Long tap")
        openAlert(index: sender.view!.tag)
    }
    func openAlert(index : Int) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let favoriteObj = self.matchesVm.profilesWithChat[index]
        
        if  favoriteObj.firstName.count > 0 {
            
            let receiverName = favoriteObj.firstName
            receiverId = favoriteObj.userId
            
            var alertTitle = String(format:StringConstants.viewProfile(),receiverName)

            if (favoriteObj.isMatchedUser == 1) {
                alertTitle = String(format:StringConstants.unMatchProfile(),receiverName)
                
                alert.addAction(UIAlertAction(title: alertTitle, style: .default, handler: {(action:UIAlertAction!) in
                    self.showConfirmUnMatchAlert()
                    self.matchesVm.profilesWithChat.remove(at: index)
                }))
            }
           
            alertTitle = String(format:StringConstants.DeleteChat(),receiverName)
            
            alert.addAction(UIAlertAction(title: alertTitle, style: .default, handler: {(action:UIAlertAction!) in
                self.confirmationForDeleteChat(indexpath:index)
                
            }))
            
            
            alert.addAction(UIAlertAction(title: StringConstants.actionSheetCancelText(), style: .cancel, handler: {(_) in }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }

    func showConfirmUnMatchAlert() {
        let alert = UIAlertController(title: nil, message: StringConstants.confirmUnmatch(), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: StringConstants.actionSheetCancelText(), style: .cancel, handler: {(_) in }))
        
        alert.addAction(UIAlertAction(title: StringConstants.unamtch(), style: .default, handler: {(action:UIAlertAction!) in
            self.unmatchUser()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    /// method for unmatch user.
    ///
    /// - Parameter userDetails: contains profile details.
    func unmatchUser() {
        
        let params = [likeProfileParams.targetUserId:receiverId]
        
        Helper.showProgressIndicator(withMessage:StringConstants.unMatch())
        
        API.requestPOSTURL(serviceName: APINAMES.UnMatchProfile,
                           withStaticAccessToken:false,
                           params: params,
                           success: { (response) in
                            Helper.hideProgressIndicator()
                            if (response.null != nil){
                                return
                            }
                            
                            let statuscode = response.dictionaryObject!["code"] as! Int
                            switch (statuscode) {
                            case 200:
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue:"unmatchedUser"), object: nil)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                                    Helper.hideProgressIndicator()
                                   // self.moveToBack()
                                    Helper.deleteUserChat(userDeatils:self.receiverId)
                                    self.mainTableView.reloadData()
                                })
                            case 401:
                                Helper.changeRootVcInVaildToken()
                                break
                            default:
                                Helper.hideProgressIndicator()
                                self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: response.dictionaryObject![LoginResponse.errorMessage] as! String)
                            }
                            
        },
                           failure: { (Error) in
                            Helper.hideProgressIndicator()
                            self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: Error.localizedDescription)
                            Helper.hideProgressIndicator()
        })
    }

}


// MARK: - UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension DatumChatVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            self.updateHeight()
            return self.matchesVm.profilesWithOutChat.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "defaultCell", for: indexPath) as! DatumChatCollectionCell
            cell.backgroundColor = Helper.getUIColor(color:"F8F8F8")
            cell.likesLabel.text = StringConstants.likes()
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DatumChatCollectionCell", for: indexPath) as! DatumChatCollectionCell
            cell.displayUserDetails(userData:self.matchesVm.profilesWithOutChat[indexPath.row])
                cell.initialSetup()
                cell.backgroundColor = Helper.getUIColor(color:"F8F8F8")
            return cell
        }
    }
    
    func openInAppPurchaseView() {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = InAppPuchasePopupView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.loadInAppPurchaseView()
        window.addSubview(videoView)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if(Helper.isUserBoostActive()){
                openMyLikesVC()
            } else {
               // if Helper.isSubscribedForPlans() {
                    self.tabBarController?.selectedIndex = 3
                    if  let navVc = self.tabBarController?.viewControllers![1] as? UINavigationController {
                        DispatchQueue.main.asyncAfter(wallDeadline:.now()+0.2, execute:{
                            if let currentProspectVc = navVc.viewControllers[0] as? ProspectVC {
                                let selectedIndex = 2
                                let screenSize = NSInteger(self.view.frame.size.width)*selectedIndex
                                currentProspectVc.mainCollectionView.setContentOffset(CGPoint(x:screenSize,y:0), animated:true)
                            }
                        })
                    }
             //   }
//                else {
//                    self.openInAppPurchaseView()
//                }
            }
        } else {
            let controller = Helper.getSBWithName(name: "Chat").instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            controller.favoriteObj = self.matchesVm.profilesWithOutChat[indexPath.row]
            self.navigationController?.pushViewController(controller, animated:true)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 120)
    }
    
    
}




// MARK: - UIScrollViewDelegate
extension DatumChatVC :UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainScrollView {
            let scrollContentOffset = self.mainScrollView.contentOffset
            self.leadingConstraintOfSelectedBtnView.constant = scrollContentOffset.x/2
            if self.mainScrollView.contentOffset.x == 0 {
                messagesLabel.textColor = Colors.AppBaseColor
                matchesLabel.textColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
                createPostButton.isHidden = true
                //making The Player nil While switching the tab
                if playingCell?.localPlayer != nil {
                    playingCell!.localPlayer!.pause()
                    playingCell!.localPlayer!.shutdown()
                    playingCell!.localPlayer!.view.removeFromSuperview()
                    playingCell!.localPlayer = nil
                    playingCell!.playingIndex = -1
                }
            } else if (self.mainScrollView.contentOffset.x == self.view.frame.size.width){
                matchesLabel.textColor = Colors.AppBaseColor
                messagesLabel.textColor =  #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
                createPostButton.isHidden = false
                //                let indexPath = IndexPath(row: 0, section: 0)
                //                guard let videoCell = matchesTableView?.cellForRow(at: indexPath) as? MatchUserPostTVCell else{return}
                //                DispatchQueue.main.async {
                //                    self.playVideoOnTheCell(cell: videoCell, indexPath: indexPath)
                //                    }
                guard let indexPaths = self.matchesTableView.indexPathsForVisibleRows else {return}
                var cells = [MatchUserPostTVCell]()
                
                for ip in indexPaths{
                    if let videoCell = self.matchesTableView.cellForRow(at: ip) as? MatchUserPostTVCell{
                        cells.append(videoCell)
                    }
                }
                let cellCount = cells.count
                if cellCount == 0 {return}
                if cellCount == 1{
                    if visibleIP != indexPaths[0]{
                        visibleIP = indexPaths[0]
                    }
                    if let videoCell = cells.last{
                        DispatchQueue.main.async {
                            self.playVideoOnTheCell(cell: videoCell, indexPath: indexPaths.last!)
                            
                        }
                    }
                }
                for i in 0..<cellCount{
                    let cellRect = self.matchesTableView.rectForRow(at:indexPaths[i])
                    let intersect = cellRect.intersection(self.matchesTableView.bounds)
                    //                curerntHeight is the height of the cell that
                    //                is visible
                    let currentHeight = intersect.height
                    let cellHeight = cells[i].frame.size.height
                    //                0.95 here denotes how much you want the cell to display
                    //                for it to mark itself as visible,
                    //                .95 denotes 95 percent,
                    //                you can change the values accordingly
                    
                    if currentHeight > (cellHeight * 0.95){
                        if visibleIP != indexPaths[i]{
                            visibleIP = indexPaths[i]
                            DispatchQueue.main.async {
                                self.playVideoOnTheCell(cell: cells[i], indexPath: indexPaths[i])
                            }
                        }
                    }
                    else{
                        if aboutToBecomeInvisibleCell != indexPaths[i].row{
                            aboutToBecomeInvisibleCell = (indexPaths[i].row)
                            self.stopPlayBack(cell: cells[i], indexPath: indexPaths[i])
                        }
                    }
                }
            }
        }
        if scrollView == self.matchesTableView{
            
            //Stop scrolling in table view reachs it's top
            //            if scrollView.contentOffset.y <= 0 {
            //                scrollView.contentOffset = CGPoint.zero
            //            }
            
            guard let indexPaths = self.matchesTableView.indexPathsForVisibleRows else {return}
            var cells = [MatchUserPostTVCell]()
            
            for ip in indexPaths{
                if let videoCell = self.matchesTableView.cellForRow(at: ip) as? MatchUserPostTVCell{
                    cells.append(videoCell)
                }
            }
            let cellCount = cells.count
            if cellCount == 0 {return}
            if cellCount == 1{
                if visibleIP != indexPaths[0]{
                    visibleIP = indexPaths[0]
                }
                if let videoCell = cells.last{
                    DispatchQueue.main.async {
                        self.playVideoOnTheCell(cell: videoCell, indexPath: indexPaths.last!)
                        
                    }
                }
            }
            for i in 0..<cellCount{
                let cellRect = self.matchesTableView.rectForRow(at:indexPaths[i])
                let intersect = cellRect.intersection(self.matchesTableView.bounds)
                //                curerntHeight is the height of the cell that
                //                is visible
                let currentHeight = intersect.height
                let cellHeight = cells[i].frame.size.height
                //                0.95 here denotes how much you want the cell to display
                //                for it to mark itself as visible,
                //                .95 denotes 95 percent,
                //                you can change the values accordingly
                if currentHeight > (cellHeight * 0.95){
                    if visibleIP != indexPaths[i]{
                        visibleIP = indexPaths[i]
                        DispatchQueue.main.async {
                            self.playVideoOnTheCell(cell: cells[i], indexPath: indexPaths[i])
                        }
                    }
                }
                else{
                    if aboutToBecomeInvisibleCell != indexPaths[i].row{
                        aboutToBecomeInvisibleCell = (indexPaths[i].row)
                        self.stopPlayBack(cell: cells[i], indexPath: indexPaths[i])
                    }
                }
            }
        }
        
    }
   
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        switch scrollView {
        case mainTableView:
            if #available(iOS 10.0, *) {
                mainTableView.refreshControl = self.chatRefreshControl
            } else {
                mainTableView.addSubview(self.chatRefreshControl)
            }
            break
        case matchesTableView:
            if #available(iOS 10.0, *) {
                matchesTableView.refreshControl = self.matchesRefreshControl
            } else {
                matchesTableView.addSubview(self.matchesRefreshControl)
            }
            break
        default:
            break
        }
    }
    
}

// MARK: - Conforming MatchUserPostTVCellDelegate
extension DatumChatVC: MatchUserPostTVCellDelegate{
  
    func getLikersOnTap(index: Int) {
        guard let likersVc = self.storyboard?.instantiateViewController(withIdentifier: "LikersViewController") as? LikersViewController else{return}
        let data = self.createPostVM.momentDetailsArray[index]
        likersVc.postId = data.postId
        self.navigationController?.pushViewController(likersVc, animated: true)
    }
    
//    func singleTapOnCell(index: Int, cell: MatchUserPostTVCell) {
//
//        if self.currentIndex == index && self.playingCell?.localplayer!.player?.isMuted == false {
//
//            self.playingCell?.localplayer!.player?.isMuted = true
//            self.playingCell?.soundVideoImage.image = #imageLiteral(resourceName: "Sound Off")
//
//        }else if self.currentIndex == index && self.playingCell?.localplayer!.player?.isMuted == true {
//            self.playingCell?.localplayer!.player?.isMuted = false
//            self.playingCell?.soundVideoImage.image = #imageLiteral(resourceName: "Sound On")
//        }
//
//
////            //if video is already playing of that cell then stop playing that video
////            self.stopPlayingCurrentVideo()
////            self.playingIndex = -1
////        }else{
////            let data = self.createPostVM.momentDetailsArray[index]
////            var stringUrl = ""
////            for url in data.url{
////               stringUrl = url
////            }
////            let url = URL(string: stringUrl)
////            self.play(url, cell: cell)
////            self.player?.bufferState = .readyToPlay
////            self.playingIndex = index
////        }
//    }

    
    func likeButtonTap(index: Int, isSelected: Bool, momentModel: MomentsModel) {
       self.createPostVM.likeAndUnlikePostService(index: index, isSelected: isSelected, momentModel: momentModel)
    }
    
    func commentButtonTap(index: Int) {
        guard let commentVC = self.storyboard?.instantiateViewController(withIdentifier: "CommentsViewController") as? CommentsViewController else{return}
        let data = self.createPostVM.momentDetailsArray[index]
        self.currentIndex =  index
        commentVC.postId = data.postId
        commentVC.userId = data.userId
        commentVC.delegate = self
        self.navigationController?.pushViewController(commentVC, animated: true)
    }

}

// MARK: - Conforming CommentsViewControllerDelegate
extension DatumChatVC: CommentsViewControllerDelegate{
    
    func commentCount(count: Int) {
        let data = createPostVM.momentDetailsArray[currentIndex]
        data.commentCount = data.commentCount + count
        createPostVM.momentDetailsArray[currentIndex] = data
        let indexPath = IndexPath(row: currentIndex, section: 0)
        self.matchesTableView.reloadRows(at: [indexPath], with: .automatic)

    }
}

extension DatumChatVC {
    
    /// report user button action
    ///
    /// - Parameter userDetails: contains the profile details.
    func reportedPost() {
        Helper.showAlertWithMessage(withTitle: StringConstants.reportThanks(), message:StringConstants.reportFeedBack(), onViewController: self)
    }
    
    
    /// reporting user.
    ///
    /// - Parameters:
    ///   - reason: reson for reporting
    ///   - userId: user id to report
    func reportPostForReason(reason:String,postId:String) {
        let params = [likeProfileParams.targetPostId:postId,
                      likeProfileParams.message:"reporting",
                      likeProfileParams.reason:reason]
        
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        
        API.requestPOSTURL(serviceName: APINAMES.reportPost,
                           withStaticAccessToken:false,
                           params: params,
                           success: { (response) in
                            Helper.hideProgressIndicator()
                            if (response.null != nil){
                                return
                            }
                            
                            let statuscode = response.dictionaryObject!["code"] as! Int
                            switch (statuscode) {
                            case 200:
                                self.reportedPost()
                            case 401:
                                Helper.changeRootVcInVaildToken()
                                break
                            default:
                                self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: response.dictionaryObject![LoginResponse.errorMessage] as! String)
                            }
                            
        },
                           failure: { (Error) in
                            
                            Helper.hideProgressIndicator()
                            
                            self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: Error.localizedDescription)
                            Helper.hideProgressIndicator()
        })
        
    }
    
}
extension DatumChatVC: deactivateReasonsDelegate {
    
    func userSelectedDoneButton(selectedReason: String) {
        print(selectedReason)
    }
    
    func userSelectedReasonForReport(selectedReason: ReportReason) {
        self.reportPostForReason(reason: selectedReason.nameOfTheReason, postId: self.createPostVM.postId)
    }
    
}
