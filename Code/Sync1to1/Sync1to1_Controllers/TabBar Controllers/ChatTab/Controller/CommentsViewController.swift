//
//  CommentsViewController.swift
//  MQTT Chat Module
//
//  Created by 3Embed Software Tech Pvt Ltd on 11/10/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


protocol CommentsViewControllerDelegate: class {
    func commentCount(count: Int)
}

class CommentsViewController: UIViewController {



    // MARK: - Outlets
    @IBOutlet weak var commentTableView: UITableView!
    @IBOutlet weak var inputToolBar: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var textView: GrowingTextView!
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendButton: UIButton!
    
    
     // MARK: - Variables And Declarations
    var userId: String?
    var commentsViewModel = CommentViewModel()
    var canServiceCall: Bool = false
    var postId: String?
    var commentModel:CommentModel?
    var isTextViewActive: Bool = false
    let commentPostVM   = CommentViewModel()
    var disposeBag = DisposeBag()
    var isSearchPeople = false
    var comment:String = ""
    var newCommentCount: Int = 0
    weak var delegate: CommentsViewControllerDelegate?
    var commentRefreshControl: UIRefreshControl = UIRefreshControl()


    struct cellIdentifier {
        static let commentTableViewCell = "CommentTableViewCell"
    }
    
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
     //   self.commentServiceCall()
        setViewUI()
        self.commentTableView.estimatedRowHeight = 120
        self.commentTableView.rowHeight = UITableViewAutomaticDimension
        // *** Customize GrowingTextView ***
        textView.layer.cornerRadius = 4.0
        sendButton.isEnabled = false
        // *** Listen to keyboard show / hide ***
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: .UIKeyboardWillChangeFrame, object: nil)
        
        // *** Hide keyboard when tapping outside ***
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        tapGesture.cancelsTouchesInView = true
        tapGesture.delegate = self
        view.addGestureRecognizer(tapGesture)
        self.commentsViewModel.gettingCommentOfUserPost(userPostId: postId!)
        didGetResponse()
        initialSetup()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.navigationBar.layer.zPosition = 0;
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.textView.resignFirstResponder()
        self.commentRefreshControl.endRefreshing()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.userImageView.makeCornerRadius(radius: userImageView.frame.size.width / 2)
      //  self.textView.becomeFirstResponder()
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            if self.commentsViewModel.commentModelArray.count != 0{
                let indexPath = IndexPath(row: self.commentsViewModel.commentModelArray.count - 1, section: 0)
                self.commentTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
//        DispatchQueue.main.async {
//            if self.commentsViewModel.commentModelArray.count != 0{
//                let indexPath = IndexPath(row: 0, section: 0)
//                self.commentTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
//            }
//        }
    }
    
    func setViewUI(){
        let userData = Database.shared.fetchResults()
        if !userData.isEmpty
        {
            if let userDetails = userData.first {
                let image = userDetails.profilePictureURL
                
                self.userImageView.setImageOn(imageUrl: image, defaultImage: #imageLiteral(resourceName: "defaultImage"))
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func initialSetup(){
        commentRefreshControl.tintColor = Colors.AppBaseColor
        commentRefreshControl.addTarget(self, action: #selector(CommentsViewController.refresh), for: UIControlEvents.valueChanged)
        commentTableView.addSubview(commentRefreshControl)
    }
    
    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    
    //Adding UIRefreshControl to Comment VC
    @objc func refresh(senderRC: UIRefreshControl) {
        self.commentPostVM.gettingCommentOfUserPost(userPostId: postId!)
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            senderRC.endRefreshing()
        }
    }
    
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        if let endFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var keyboardHeight = UIScreen.main.bounds.height - endFrame.origin.y
            if keyboardHeight == 0{
               // self.searchTableView.isHidden = true
            }
            if #available(iOS 11, *) {
                if keyboardHeight > 0 {
                    keyboardHeight = keyboardHeight - view.safeAreaInsets.bottom
                }
            }
            textViewBottomConstraint.constant = keyboardHeight + 8
            view.layoutIfNeeded()
            //            self.scrollToBottom()
        }
    }

    //MARK:- Button Action
    
    @IBAction func sendButtonAction(_ sender: Any) {
        
        self.commentsViewModel.postId = postId!
        self.commentsViewModel.comment = self.textView.text
        commentModel?.comment = self.textView.text
        self.commentsViewModel.postingCommentOnUserPost()
        self.textView.text = " "
        self.sendButton.isEnabled = false
        self.newCommentCount = self.newCommentCount + 1
    }
    
    
    @IBAction func backbuttonAction(_ sender: Any) {
        if self.delegate != nil && self.newCommentCount > 0{
            self.delegate?.commentCount(count: self.newCommentCount)
        }
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    /// After Getting Response
    func didGetResponse(){
        
        if !commentsViewModel.commentPostVM_resp.hasObservers {
            commentsViewModel.commentPostVM_resp.subscribe(onNext: { success in
                switch success.0 {
                case CommentViewModel.responseType.getCommnent:
                    if success.1 {
                        DispatchQueue.main.async {
                        self.textView.becomeFirstResponder()
                        self.commentTableView.reloadData()
                        }
                    }
                case CommentViewModel.responseType.postComment:
                    if success.1 {
                        DispatchQueue.main.async {
                        self.commentTableView.reloadData()
                      }
                    }
                default:
                    break
                }
            }).disposed(by: disposeBag)
        }
    }
 

    

}

extension CommentsViewController: GrowingTextViewDelegate {
    
    // *** Call layoutIfNeeded on superview for animation when changing height ***
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.isTextViewActive = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.isTextViewActive = false
        self.commentsViewModel.comment =  self.textView.text
    }
    
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
     //   let stringArray = textView.text.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        
       // let text = (textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)
       let trimmed = textView.text.trimmingCharacters(in: .whitespacesAndNewlines)

        if !trimmed.isEmpty{
            sendButton.isEnabled = true
        } else {
            sendButton.isEnabled = false
        }
       
        
//        //let words = textView.text.byWords
//        if stringArray.last == nil  {
//            removeTableData()
//        } else {
//            let lastString = stringArray.last!
//
//            if lastString == "" {
//                removeTableData()
//                return
//            }
//
//            if lastString.count > 0 {
//                if lastString.first == "@",lastString.count > 1 {
//                    isSearchPeople = true
//                    var removeSymbol = lastString
//                    removeSymbol = String(removeSymbol.dropFirst())
//                    getPeopleService(searchString:removeSymbol)
//                } else if lastString.first == "#",lastString.count > 1 {
//                    isSearchPeople = false
//
//                    var removeSymbol = lastString
//                    removeSymbol = String(removeSymbol.dropFirst())
//                    getTagsService(searchString: removeSymbol)
//                } else {
//                    removeTableData()
//                }
//            }
//        }
    }
    

}

//MARK:- UITableViewDelegate, UITableViewDataSource
extension CommentsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return self.commentsViewModel.commentModelArray.count
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as? CommentTableViewCell else{fatalError()}
            let data = self.commentsViewModel.commentModelArray[indexPath.row]
            cell.setCellData(modelData: data)
        
            cell.delegate = self
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let indexPassed: Bool = indexPath.row >= self.commentsViewModel.commentModelArray.count - 10
        if canServiceCall && indexPassed{
            canServiceCall = false
            //self.commentServiceCall()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.commentTableView{
            let data = self.commentsViewModel.commentModelArray[indexPath.row]
            let text = data.commenterName! + "  " + data.comment!
            let font = UIFont.init(name: "CircularAirPro-Book", size: 15)
            let width = self.view.frame.size.width - 60
            let height = text.height(withConstrainedWidth: width, font: font!)
            let cellHeight = height < 19 ? 19 : height
            return cellHeight + 40
        }else{
            return 55.0
        }
    }
    
}

//MARK:- Commenttableviewcelldelegate
extension CommentsViewController: CommentTableViewCellDelegate{

    func userNameGetClicked(name: String){
        guard let profileVC = storyboard?.instantiateViewController(withIdentifier: StoryBoardIdentifier.profileVc) as? ProfileDetailVC else {return}
        profileVC.profileDeatilVM.profileDetails.userName = name
        self.navigationController?.pushViewController(profileVC, animated: true)
        //self.navigationController?.present(profileVc, animated: true, completion:nil)

    }
    
}

//MARK:- Tap gesture  delegate
extension CommentsViewController: UIGestureRecognizerDelegate{
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool{
        if self.isTextViewActive {
            return true
        }
        return false
    }
}
