//
//  LikersViewController.swift
//  Datum
//
//  Created by Rahul Sharma on 20/06/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift

class LikersViewController: UIViewController {

    
    let createPostVM = CreatePostViewModel()
    var disposeBag = DisposeBag()
    var postId: String?
    var likersName: String?
    
    @IBOutlet weak var mainTableView: UITableView!
    
    @IBAction func backButtonAction(_ sender: UIBarButtonItem) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.createPostVM.gettingLikersName(userPostId: postId!)
        didGetResponse()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       //self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.tabBar.isHidden = false
   }
  
    func likeButtonTap(index: Int, isSelected: Bool, momentModel: MomentsModel) {
        self.createPostVM.likeAndUnlikePostService(index: index, isSelected: isSelected, momentModel: momentModel)
    }
    
    
    func didGetResponse(){
        
        if !createPostVM.createPostVM_resp.hasObservers {
            createPostVM.createPostVM_resp.subscribe(onNext: { success in
                switch success.0 {
                case CreatePostViewModel.responseType.getLikers:
                    if success.1 {
                        DispatchQueue.main.async {
                            self.mainTableView.reloadData()
                        }
                    }
                default:
                    break
                }
            }).disposed(by: disposeBag)
        }
    }
    
}

extension LikersViewController: UITableViewDataSource,UITableViewDelegate{
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return createPostVM.momentDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LikersTableViewCell.self)) as? LikersTableViewCell{
            let data = self.createPostVM.momentDetailsArray[indexPath.row]
            cell.setCellData(modelData: data)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
