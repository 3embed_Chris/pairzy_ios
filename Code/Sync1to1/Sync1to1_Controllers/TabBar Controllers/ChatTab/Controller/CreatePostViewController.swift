//
//  CreatePostViewController.swift
//  dub.ly
//
//  Created by Shivansh on 1/31/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

 class CreatePostViewController: UIViewController,UITextViewDelegate {

    //MARK: -  Variables And Declarations
    var selectedImage:UIImage?
    var postDetailsCell:PostingTableViewCell?
    var newPostDetails:NewPostModel?
    var isForVideo = true
    var mediaPath:String = ""
    var isEdit: Bool = false
    var caption: String = ""
    var recordedVideoUrlPath = ""
    var disposeBag = DisposeBag()
    let createPostVM = CreatePostViewModel()
    let datumChatVC = DatumChatVC()

    //MARK: - Outlets
    @IBOutlet weak var postButtonOutlet: UIBarButtonItem!
    @IBOutlet weak var postTableView: UITableView!
    
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
      // Do any additional setup after loading the view.
        self.navigationItem.title = "Create Post"
        newPostDetails = NewPostModel()
        newPostDetails?.isVideo = isForVideo
        newPostDetails?.mediaPath = mediaPath
        postButtonOutlet.isEnabled = false
        let font = UIFont(name: "CircularAirPro-Bold", size: 20.0)!
        let attributed = [ NSAttributedStringKey.font: font]
        postButtonOutlet.setTitleTextAttributes(attributed, for: .normal)
        if self.isEdit{
           // self.setPostDetailModel()
        }
       // didGetResponse()

    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        postDetailsCell?.captionTextView.delegate = self
         postDetailsCell?.captionTextView.becomeFirstResponder()
    }
    
    //    func setPostDetailModel(){
    //        self.newPostDetails?.isEdit = true
    //        self.newPostDetails?.postId = socialModel?.postId
    //        if let channelId = socialModel?.channelId, channelId != ""{
    //            let selectedChannel = ChannelModel(socialModel: socialModel!)
    //        }else{
    //            self.newPostDetails?.channel = nil
    //            showChannels = false
    //            newPostDetails?.category = CategoryListModel(socialModel: socialModel!)
    //        }
    //        newPostDetails?.selectedAddress = AddressModel(socialModel: socialModel!)
    //        self.caption = (socialModel?.title)!
    //    }
    
    @IBAction func playPauseVideoBtn(_ sender: Any) {
        
    }
    //MARK:- UITextViewDelegates

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == StringConstants.captionPlaceholderText() {
            textView.text = ""
            textView.textColor = UIColor.black
            textView.font = UIFont(name: "CircularAirPro-Book", size: 18.0)
        }
    }
    
    
    @objc func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
//        if (text == "\n") {
//            dismissKeyboard()
//            return false
//        }
        
        let trimmed = textView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if !trimmed.isEmpty{
            postButtonOutlet.isEnabled = true
        } else {
            postButtonOutlet.isEnabled = false
        }
        if textView.text.count >= 100{
            Helper.showAlertMessageViewFromTopNavBar(showOnView: self.view, message: "Maximum of 100 characters")
            return false
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
        let stringArray = textView.text.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        newPostDetails?.caption = textView.text!
        
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            postButtonOutlet.isEnabled = false
            textView.textColor = UIColor.lightGray
            textView.font = UIFont(name: "CircularAirPro-Book", size: 13.0)
        }else{
        postButtonOutlet.isEnabled = true
        self.createPostVM.caption =  (self.postDetailsCell?.captionTextView.text)!
    }
    
    }
    
    @IBAction func postBtnAction(_ sender: Any) {
        //checking mandatory things before posting.
        
        self.view.endEditing(true)
        newPostDetails?.caption = (self.postDetailsCell?.captionTextView.text!)!
        newPostDetails?.image = postDetailsCell?.shareImageView.image
        
        if isForVideo{
            newPostDetails?.image = postDetailsCell?.shareImageView.image
        }
        
        if (newPostDetails?.caption.isEmpty)! {
            Helper.showAlertMessageViewFromTopNavBar(showOnView: self.view, message: StringConstants.captionEmptyAlert())
            return
        } else {
//            if isForVideo{
          //      self.createPostVM.uploadVideoToCloudinaryandCreateUserPost(video:URL(string: mediaPath)!)
//            }else{
//                self.createPostVM.uploadImageToCloudinaryandCreateUserPost(image: selectedImage!)
//
//            }
            
            NotificationCenter.default.post(name: NSNotification.Name("createNewPost"), object: newPostDetails!)
            self.dismiss(animated:true, completion:nil)
            
        }
        
    }
   
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /// Going to Chat VC After Creating Post Successfully
    func goToDatumChatVC(){
        guard let chatVC = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIdentifier.DatumChatVC) as? DatumChatVC else {return}
        self.navigationController?.pushViewController(chatVC, animated: true)
        //self.tabBarController?.selectedIndex = 2

    }
  
//    /// Logic after Coming Response from API
//    func didGetResponse() {
//        if !createPostVM.createPostVM_resp.hasObservers {
//            createPostVM.createPostVM_resp.subscribe(onNext: { success in
//                switch success.0 {
//                case CreatePostViewModel.responseType.createUserPost:
//                    if success.1 {
//                        self.dismiss(animated: true, completion: {
//
////                            let name = NSNotification.Name(rawValue: "PostUpdateNotification")
////                            NotificationCenter.default.post(name: name, object: self, userInfo: nil)
//                        })
//                        } else {
//                        Helper.showAlertWithMessage(withTitle: "Message", message: StringConstants.captionEmptyAlert(), onViewController: self)
//                       }
//                default:
//                    break
//                }
//            })//.disposed(by: disposeBag)
//        }
//    }

}


// MARK: - UITableViewDelegate,UITableViewDataSource
extension CreatePostViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
  
          return 1
    
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
                let stringArray = postDetailsCell?.captionTextView.text.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
                let lastString = stringArray!.last!
                postDetailsCell?.captionTextView.text = postDetailsCell?.captionTextView.text.replacingLastOccurrenceOfString(lastString, with:"")
    }
    
  
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
              return 225
     
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
                if postDetailsCell == nil {
                    postDetailsCell = tableView.dequeueReusableCell(withIdentifier: "PostingTableViewCell", for:indexPath) as? PostingTableViewCell
                    postDetailsCell?.captionTextView.delegate = self
                }
        
                    if self.isForVideo{
                        let url = self.mediaPath
                        let endIndex = url.index(url.endIndex, offsetBy: -3)
                        var truncated = url.substring(to: endIndex)
                        truncated = truncated + "jpg"
//                        postDetailsCell?.shareImageView.contentMode = UIViewContentMode.scaleAspectFill
                        postDetailsCell?.shareImageView.setImageOn(imageUrl: truncated, defaultImage: #imageLiteral(resourceName: "defaultPicture"))
                    }else{
//                        postDetailsCell?.shareImageView.contentMode = UIViewContentMode.scaleAspectFit
                        postDetailsCell?.shareImageView.setImageOn(imageUrl: self.mediaPath, defaultImage: #imageLiteral(resourceName: "defaultPicture"))
                    }
                    
                    if self.caption != ""{
                        postDetailsCell?.captionTextView.text = createPostVM.caption
                        self.caption = ""
                    }else{
                    postDetailsCell?.shareImageView.image = selectedImage
                }
                
                if isForVideo {
                    postDetailsCell?.videoPlayPauseBtn.isHidden = false
                }
                
                return postDetailsCell!
        }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == self.postTableView {
            self.view.endEditing(true)
        }
    }
    
}



extension StringProtocol where Index == String.Index {
    var byWords: [SubSequence] {
        var byWords: [SubSequence] = []
        enumerateSubstrings(in: startIndex..., options: .byWords) { _, range, _, _ in
            byWords.append(self[range])
        }
        return byWords
    }
}


extension String
{
    func replacingLastOccurrenceOfString(_ searchString: String,
                                         with replacementString: String,
                                         caseInsensitive: Bool = true) -> String
    {
        let options: String.CompareOptions
        if caseInsensitive {
            options = [.backwards, .caseInsensitive]
        } else {
            options = [.backwards]
        }
        
        if let range = self.range(of: searchString,
                                  options: options,
                                  range: nil,
                                  locale: nil) {
            
            return self.replacingCharacters(in: range, with: replacementString)
        }
        return self
    }
}

