//
//  VideoPreviewViewController.swift
//  dub.ly
//
//  Created by DINESH GUPTHA on 12/17/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import SCRecorder
import AVKit
import AssetsLibrary
import Photos

protocol VideoPreviewViewControllerDelegate: class{
    func dismissImagePicker()
}

class VideoPreviewViewController: UIViewController {
    
    @IBOutlet weak var viewForPlayer: UIView!
    
    @IBOutlet weak var saveLabel: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var storyLabel: UILabel!
    @IBOutlet weak var shareBtn: UIButton!

    @IBOutlet weak var topMenuView: UIView!
    var videoConfig:SCFilterImageView?
    @IBOutlet weak var bottomMenuView: UIView!
    
    weak var delegate: VideoPreviewViewControllerDelegate?
    var session = SCRecordSession()
    let player = SCPlayer()
    //    let storyAPIObj = StoryAPI()
    //    var selectedAudio:Audio?
    let createPostVM = CreatePostViewModel()
    var isFromProfile = false
    var audioPlayer:AVAudioPlayer?
    var videoUrl: String?
    var videoAssest:AVAsset?
    var addVideoAsset = true
    var isForStory = false
    var isForInstagramCamera = false
    private var buttonTxt = ""
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //        if !isForStory {
        //            self.saveLabel.isHidden = true
        //            self.saveBtn.isHidden = true
        //            self.storyBtn.isHidden = true
        //            self.storyLabel.isHidden = true
        //        } else {
        //            self.shareBtn.isHidden = true
        //        }
    }

    
    
    
    override func viewDidLayoutSubviews() {
        
    }
    
    func changeButtonTitle( buttonTxt : String = "Share" ){
        self.buttonTxt = buttonTxt
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        shareBtn.setTitle(buttonTxt, for: .normal)
        self.navigationController?.isNavigationBarHidden = true
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if addVideoAsset {
            addVideoAsset = false
            if !isForInstagramCamera {
                videoAssest = session.assetRepresentingSegments()
            }
            player.setItemBy(videoAssest)
            
            let playerView = SCVideoPlayerView.init(player: self.player)
            playerView.playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspect
            playerView.frame = self.viewForPlayer.bounds
            playerView.autoresizingMask = self.viewForPlayer.autoresizingMask
            self.viewForPlayer.addSubview(playerView)
            player.play()
            player.loopEnabled = true
            player.delegate = self
            viewForPlayer.bringSubview(toFront: topMenuView)
            viewForPlayer.bringSubview(toFront: bottomMenuView)
          //  trim()
        } else {
            player.play()
            self.audioPlayer?.play()
        }
    }
    
    func addSwipableFilters() {
        let swipableFilter = SCSwipeableFilterView(frame: self.viewForPlayer.bounds)
        swipableFilter.filters = [SCFilter (ciFilterName:"CIPhotoEffectChrome"),SCFilter (ciFilterName:"CIMinimumComponent")]
        player.scImageView = swipableFilter
        self.viewForPlayer.addSubview(swipableFilter)
    }
    
    func saveFinalVideo(videoUrl:URL,isUpload:Bool,isShare:Bool) {
       
        if isFromProfile{
            let notificationName = NSNotification.Name(rawValue: "PostCapturedVideo")
            NotificationCenter.default.post(name: notificationName, object: self, userInfo: ["video": videoUrl as Any])
            let videoNotificationName = NSNotification.Name(rawValue: "SignUpProfileVideo")
            NotificationCenter.default.post(name: videoNotificationName, object: self, userInfo: ["video": videoUrl as Any])
            
            self.navigationController?.popToRootViewController(animated: false)
            delegate?.dismissImagePicker()
  
        }else if isShare {
        //moving to share screen.
        self.moveToShareScreen(videoPath: videoUrl.absoluteString)
        } else {
        //save to gallery.
        self.saveVideoToGallery(filePath: videoUrl)
        }
    }
    
  
    
    @IBAction func saveBtnAction(_ sender: Any) {
        mergeSegments(isUpload:false,isShare:false)
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.player.pause()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        
   
            mergeSegments(isUpload:false, isShare:true)
        
        
    }
    
    func moveToShareScreen(videoPath:String) {
        
        //getting thumbnail image.
        var thumbNailImage = UIImage()
        do {
            let imageGenerator = AVAssetImageGenerator(asset: videoAssest!)
            imageGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imageGenerator.copyCGImage(at: CMTimeMake(3, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            thumbNailImage = thumbnail
        }catch{
            print("Error is : \(error)")
        }
        
        
        DispatchQueue.main.async {
            self.player.pause()
            self.audioPlayer?.pause()
            let mainStoryBoard = UIStoryboard(name:"DatumTabBarControllers", bundle:nil)
            let shareVc = mainStoryBoard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
            self.navigationController?.isNavigationBarHidden = false
            shareVc.mediaPath = videoPath
            shareVc.selectedImage = thumbNailImage
            shareVc.isForVideo = true
            self.navigationController?.pushViewController(shareVc, animated:true)
            
            Helper.hideProgressIndicator()
        }
        
    }
    
    
    func mergeSegments(isUpload:Bool,isShare:Bool) {
        
        
        if let url = self.videoUrl{
            self.moveToShareScreen(videoPath: url)
        }else{
            session.mergeSegments(usingPreset: AVAssetExportPresetHighestQuality) { (url, error) in
                if (error == nil) {
                    let urlInNs:URL = URL(string: (url?.absoluteString)!)!
                    self.saveFinalVideo(videoUrl:urlInNs, isUpload:isUpload,isShare:isShare)
                } else {
                    debugPrint(error)
                }
            }
        }
    }


func updateFilterToVideoOverLay() {
    videoConfig = SCFilterImageView(frame: self.view.bounds)
    videoConfig!.filter = SCFilter(ciFilterName:"CIPhotoEffectInstant");
    player.scImageView?.backgroundColor = UIColor.clear
    player.scImageView = videoConfig!
}

  func mergeFilesWithUrl(audioUrl:URL,isUpload:Bool,isShare:Bool){
        let mixComposition : AVMutableComposition = AVMutableComposition()
        var mutableCompositionVideoTrack : [AVMutableCompositionTrack] = []
        var mutableCompositionAudioTrack : [AVMutableCompositionTrack] = []
        let totalVideoCompositionInstruction : AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()


        //start merge

        let aVideoAsset : AVAsset = videoAssest!
        let aAudioAsset : AVAsset = AVAsset(url: audioUrl)
        mutableCompositionVideoTrack.append(mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)!)
        mutableCompositionAudioTrack.append( mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)!)

        let aVideoAssetTrack : AVAssetTrack = aVideoAsset.tracks(withMediaType: AVMediaType.video)[0]
        let aAudioAssetTrack : AVAssetTrack = aAudioAsset.tracks(withMediaType: AVMediaType.audio)[0]


        do{
            try mutableCompositionVideoTrack[0].insertTimeRange(CMTimeRangeMake(kCMTimeZero, aVideoAssetTrack.timeRange.duration), of: aVideoAssetTrack, at: kCMTimeZero)

            //In my case my audio file is longer then video file so i took videoAsset duration
            //instead of audioAsset duration

            try mutableCompositionAudioTrack[0].insertTimeRange(CMTimeRangeMake(kCMTimeZero, aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: kCMTimeZero)
        } catch let error {
            print(error.localizedDescription)
        }

        totalVideoCompositionInstruction.timeRange = CMTimeRangeMake(kCMTimeZero,aVideoAssetTrack.timeRange.duration )

        let mutableVideoComposition : AVMutableVideoComposition = AVMutableVideoComposition()
        mutableVideoComposition.frameDuration = CMTimeMake(1, 30)

        //mutableVideoComposition.renderSize = CGSizeMake(1280,720)

        let timeStamp = Helper.getcurrentDataAndtimeForSave()

        //find your video on this URl
        let savePathUrl : URL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents"+"/"+"\(timeStamp)"+".mp4")

        let assetExport: AVAssetExportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
        assetExport.outputFileType = AVFileType.mp4
        assetExport.outputURL = savePathUrl
        assetExport.shouldOptimizeForNetworkUse = true

        assetExport.exportAsynchronously { () -> Void in
            switch assetExport.status {

            case AVAssetExportSessionStatus.completed:
//                print("hello dinesh")
//                 if isShare {
//                    self.moveToShareScreen(videoPath:savePathUrl.absoluteString)
//                } else {
                    //save to galley.
                     self.saveVideoToGallery(filePath: savePathUrl)
//                }
            case  AVAssetExportSessionStatus.failed:
                print("failed \(String(describing: assetExport.error))")
//                Helper.HideProgressIndiactor()()
            case AVAssetExportSessionStatus.cancelled:
                print("cancelled \(String(describing: assetExport.error))")
//                Helper.HideProgressIndiactor()()
            default:
                print("complete")
            }
        }


    }


func saveVideoToGallery(filePath:URL) {
    print(filePath)
    PHPhotoLibrary.shared().performChanges({
        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: filePath)
    }) { (success, error) -> Void in
        
        
        Helper.hideProgressIndicator()
        
        if success {
            
            //popup alert success
            let alert = UIAlertController(title: "Video Saved", message: "Video successfully saved to Photos library", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            //popup alert unsuccess
            let alert = UIAlertController(title: "Failed", message: "Video Failed to save in Photos library", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}


func exportAsset(_ asset: AVAsset, fileName: String) {
    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    let trimmedSoundFileURL = documentsDirectory.appendingPathComponent(fileName)
    print("saving to \(trimmedSoundFileURL.absoluteString)")
    
    if FileManager.default.fileExists(atPath: trimmedSoundFileURL.absoluteString) {
        print("sound exists, removing \(trimmedSoundFileURL.absoluteString)")
        do {
            if try trimmedSoundFileURL.checkResourceIsReachable() {
                print("is reachable")
            }
            
            try FileManager.default.removeItem(atPath: trimmedSoundFileURL.absoluteString)
        } catch {
            print("could not remove \(trimmedSoundFileURL)")
            print(error.localizedDescription)
        }
    }
    
    print("creating export session for \(asset)")
    
    if let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A) {
        exporter.outputFileType = AVFileType.m4a
        exporter.outputURL = trimmedSoundFileURL
        
        let duration = CMTimeGetSeconds(asset.duration)
        if duration < 5.0 {
            print("sound is not long enough")
            return
        }
        // e.g. the first 5 seconds
        let startTime = CMTimeMake(0, 1)
        let stopTime = CMTimeMake(Int64(self.session.duration.seconds), 1)
        exporter.timeRange = CMTimeRangeFromTimeToTime(startTime, stopTime)
        
        // do it
        exporter.exportAsynchronously(completionHandler: {
            print("export complete \(exporter.status)")
            
            switch exporter.status {
            case  AVAssetExportSessionStatus.failed:
                
                if let e = exporter.error {
                    print("export failed due to session: \(self.session.duration.seconds)")
                    print("export failed due to session: \(asset.duration.seconds)")
                    print("export failed \(e)")
                }
                
            case AVAssetExportSessionStatus.cancelled:
                print("export cancelled \(String(describing: exporter.error))")
            case AVAssetExportSessionStatus.completed:
                do {
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                    try AVAudioSession.sharedInstance().setActive(true)
                    
                    self.audioPlayer = try AVAudioPlayer(contentsOf: trimmedSoundFileURL)
                    self.audioPlayer?.enableRate = true
                    self.audioPlayer?.prepareToPlay()
                    self.audioPlayer?.numberOfLoops = -1
                    self.audioPlayer?.play()
                    
                } catch let error {
                    print(error.localizedDescription)
                }
                
            default:
                print("export complete")
            }
        })
    } else {
        print("cannot create AVAssetExportSession for asset \(asset)")
        }
    
    }

}

    

extension VideoPreviewViewController:SCPlayerDelegate {
    
}


extension Double {
    func toInt() -> Int? {
        if self > Double(Int.min) && self < Double(Int.max) {
            return Int(self)
        } else {
            return nil
        }
    }
}

extension Double {
    func convertDurationToMs() -> String?  {
        var durationInDouble = self * 1000.0
        durationInDouble = durationInDouble.rounded()
        let durationInInt = durationInDouble.toInt()
        let durationString:String = "\(durationInInt!)"
        return durationString
    }
}
