//
//  ProfileViewModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 23/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift

class ProfileViewModel: NSObject {
    
    enum ResponseType:Int {
        case liked = 0
        case unliked = 1
        case boost = 2
        case superLiked = 3
        case rewind = 4
        case getProfileData = 5
        case coinBalance = 6
        case unsubscribed = 7
        
    }
    
    let disposeBag = DisposeBag()
    let subject_response = PublishSubject<(ResponseType,Bool)>()
    let subject_response_forLike = PublishSubject<(ResponseType,Bool,Profile)>()
    
    var profileModelData = [Profile]()
    var apiCall = ProfileAPICalls()
    var errorMsg = ""
    
    /// method for requesting profiles.
    func requestForProfiles(offset:Int , limit:Int) {
        
        let apiCall = ProfileAPICalls()
        apiCall.requestForGetProfiles(offset: offset , limit:limit)
        
        apiCall.subject_response
            .subscribe(onNext: {response in
                
                Helper.hideProgressIndicator()
                self.handleAllAPIsResponse(type:.getProfileData,response: response,selectedProfile: Profile.fetchprofileDataObj(userDataDictionary:[:]))
            }, onError: {error in
                Helper.hideProgressIndicator()
                self.errorMsg = error.localizedDescription
                self.subject_response.onNext((.getProfileData, false))
            })
            .disposed(by:disposeBag)
    }
    
    
    
    /// get coin balance
    func getCoinBalance(){
        
        let apiCalls = CoinsAPICalls()
        
        apiCalls.requestForGetCoinBalance()
        apiCalls.subject_response
            .subscribe(onNext: {response in
                
                Helper.hideProgressIndicator()
                self.handleAllAPIsResponse(type:.coinBalance,response: response,selectedProfile: Profile.fetchprofileDataObj(userDataDictionary:[:]))
                
            }, onError: { error in
                Helper.hideProgressIndicator()
                self.errorMsg = error.localizedDescription
                self.subject_response.onNext((.coinBalance, false))
            })
            .disposed(by:disposeBag)
    }
    
    
    /// method for requeting like a profile.
    ///
    /// - Parameter selectedProfile: contains profile details.
    func likeProfile(selectedProfile:Profile)  {
        let params = [likeProfileParams.targetUserId:selectedProfile.userId] as [String : Any]
        
        let requestData = RequestModel().likeProfileRequestDetails(Details:params)
        
        apiCall = ProfileAPICalls()
        
        apiCall.requestLikeProfile(requestLikeProfile:requestData)
        
        apiCall.subject_response
            .subscribe(onNext: {response in
                
                Helper.hideProgressIndicator()
                
                self.handleAllAPIsResponse(type:.liked,response: response, selectedProfile: selectedProfile )
                
            }, onError: {error in
                Helper.hideProgressIndicator()
                self.errorMsg = error.localizedDescription
                self.subject_response.onNext((.liked, false))
            })
            .disposed(by:disposeBag)
    }
    
    
    /// method for requting dislike profile.
    ///
    /// - Parameter selectedProfile:contains profile details.
    func disLikeProfile(selectedProfile:Profile)  {
        let params = [likeProfileParams.targetUserId:selectedProfile.userId] as [String : Any]
        
        let requestData = RequestModel().dislikeProfileRequestDetails(Details:params)
        
        apiCall = ProfileAPICalls()
        
        apiCall.requestDisLikeProfile(requestDisLikeProfile:requestData)
        
        apiCall.subject_response
            .subscribe(onNext: {response in
                
                Helper.hideProgressIndicator()
                self.handleAllAPIsResponse(type:.unliked,response: response,selectedProfile: selectedProfile)
                
            }, onError: {error in
                Helper.hideProgressIndicator()
                self.errorMsg = error.localizedDescription
                self.subject_response.onNext((.unliked, false))
            })
            .disposed(by:disposeBag)
    }
    
    
    /// method for requting dislike profile.
    ///
    /// - Parameter selectedProfile:contains profile details.
    func superLikeProfile(selectedProfile:Profile)  {
        let params = [likeProfileParams.targetUserId:selectedProfile.userId] as [String : Any]
        let requestData = RequestModel().dislikeProfileRequestDetails(Details:params)
        AudioHelper.sharedInstance.playCoinsSound()
        apiCall = ProfileAPICalls()
        apiCall.requestSuperLikeProfile(requestDisLikeProfile:requestData)
        apiCall.subject_response
            .subscribe(onNext: {response in
                
                Helper.hideProgressIndicator()
                self.handleAllAPIsResponse(type:.superLiked,response: response,selectedProfile: selectedProfile)
                
            }, onError: {error in
                Helper.hideProgressIndicator()
                self.errorMsg = error.localizedDescription
                self.subject_response.onNext((.superLiked, false))
            })
            .disposed(by:disposeBag)
    }
    
    
    func rewindProfile(){
        apiCall = ProfileAPICalls()
        apiCall.rewindProfiles()
        apiCall.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                self.handleAllAPIsResponse(type:.rewind,response: response,selectedProfile: Profile.fetchprofileDataObj(userDataDictionary:[:]))
            }, onError: {error in
                Helper.hideProgressIndicator()
                self.errorMsg = error.localizedDescription
                self.subject_response.onNext((.rewind, false))
            })
            .disposed(by:disposeBag)
    }
    
    
    func boostProfile(){
        
        apiCall = ProfileAPICalls()
        apiCall.boostProfile()
        Helper.showProgressIndicator(withMessage:"Boosting ...")
        apiCall.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                self.handleAllAPIsResponse(type:.boost,response: response,selectedProfile: Profile.fetchprofileDataObj(userDataDictionary:[:]))
                
            }, onError: {error in
                Helper.hideProgressIndicator()
                self.errorMsg = error.localizedDescription
                self.subject_response.onNext((.boost, false))
            })
            .disposed(by:disposeBag)
    }
    
    
    func handleAllAPIsResponse(type: ResponseType,response: ResponseModel,selectedProfile:Profile){
        
        switch response.statusCode {
            
        case 200,201: //200 - Success , 201 - success with matched
            switch type {
                
            case .liked:
                
                if let remainingLikes = response.response["remainsLikesInString"] as? String{
                    if let hasLikes = Int(remainingLikes) {
                        Helper.updateRemainingLikes(likes:hasLikes)
                    }
                }
                
                if let remainingLikes = response.response["remainsLikesInString"] as? Int{
                    Helper.updateRemainingLikes(likes:remainingLikes)
                }
                
                if let refreshTime = response.response["nextLikeTime"] as? Double{
                    Helper.updateExpireTimeForLikes(timestamp: Float(refreshTime))
                }
                
                if let refreshTime = response.response["nextLikeTime"] as? Float{
                    Helper.updateExpireTimeForLikes(timestamp: refreshTime)
                }
            
                self.subject_response.onNext((.liked,true))
                break
                
            case .unliked:
                self.subject_response.onNext((.unliked,true))
                break
                
            case .boost:
                if let coinWallet = response.response["coinWallet"] as? [String:Any] {
                    if let coinbal = coinWallet["Coin"] as? Int {
                        UserDefaults.standard.set(coinbal, forKey: "coinBalance")
                    }
                }
                
                Helper.updateBoostDetails(boostDetails:response.response)
                self.subject_response.onNext((.boost,true))
               
                break
                
            case .superLiked:
                if let coinWallet = response.response["coinWallet"] as? [String:Any] {
                    if let coinbal = coinWallet["Coin"] as? Int {
                        UserDefaults.standard.set(coinbal, forKey: "coinBalance")
                    }
                }
                self.subject_response.onNext((.superLiked,true))
                break
                
            case .rewind:
                
                
                if let remainingLikes = response.response["remainsRewindsInString"] as? String{
                    if let hasLikes = Int(remainingLikes) {
                        Helper.updateRemainingRewinds(rewinds:hasLikes)
                    }
                }
                
                if let remainingLikes = response.response["remainsRewindsInString"] as? Int{
                    Helper.updateRemainingRewinds(rewinds:remainingLikes)
                }
                
                if let refreshTime = response.response["nextRewindTime"] as? Float{
                    Helper.updateExpireTimeForRewinds(timestamp: refreshTime)
                }
                
                if let refreshTime = response.response["nextRewindTime"] as? Double{
                    Helper.updateExpireTimeForRewinds(timestamp: Float(refreshTime))
                }
                
                
                self.subject_response.onNext((.rewind,true))
                break
                
            case .getProfileData:
                
                
                if let remainingLikes = response.response["remainsLikesInString"] as? String{
                    if let hasLikes = Int(remainingLikes) {
                        Helper.updateRemainingLikes(likes:hasLikes)
                    }
                }
                
                if let remainingLikes = response.response["remainsLikesInString"] as? Int{
                    Helper.updateRemainingLikes(likes:remainingLikes)
                }
                
                if let remainingLikes = response.response["remainsRewindsInString"] as? String{
                    if let hasLikes = Int(remainingLikes) {
                        Helper.updateRemainingRewinds(rewinds:hasLikes)
                    }
                }
                
                if let remainingLikes = response.response["remainsRewindsInString"] as? Int{
                    Helper.updateRemainingRewinds(rewinds:remainingLikes)
                }
                
                if let refreshTime = response.response["nextLikeTime"] as? Double{
                    Helper.updateExpireTimeForLikes(timestamp: Float(refreshTime))
                }
                
                if let refreshTime = response.response["nextLikeTime"] as? Float{
                    Helper.updateExpireTimeForLikes(timestamp: refreshTime)
                }
                
                if let refreshTime = response.response["nextRewindTime"] as? Float{
                    Helper.updateExpireTimeForRewinds(timestamp: refreshTime)
                }
                
                if let refreshTime = response.response["nextRewindTime"] as? Double{
                    Helper.updateExpireTimeForRewinds(timestamp: Float(refreshTime))
                }
                
                if let boostDetails = response.response["boost"] as? [String:Any]{
                    Helper.updateBoostDetails(boostDetails:boostDetails)
                    let simpleDic = ["boost":boostDetails]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue:NSNotificationNames.notificationForBoostUpdate), object: simpleDic)
                }
                
                var profileModelData = [Profile]()
                if let profileResponse = response.response["data"] as? [AnyObject] {
                    for (index, _) in profileResponse.enumerated() {
                        let profileDetails = profileResponse[index] as! [String:Any]
                        profileModelData.append(Profile.fetchprofileDataObj(userDataDictionary:profileDetails))
                    }
                }
                self.profileModelData = profileModelData
                self.subject_response.onNext((.getProfileData,true))
                break
                
                
            case .coinBalance:
                if let data = response.response[ApiResponseKeys.data] as? [String:Any] {
                    let coinBalance = CoinBalanceModel.init(data: data)
                    UserDefaults.standard.set(coinBalance.coin, forKey: "coinBalance")
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "updateCoinsBal"), object: self, userInfo:[:])
                    self.subject_response.onNext((.coinBalance,true))
                }
                break
            case .unsubscribed:
                break
            }
            
        case 409:
            if type == .rewind {
                 self.subject_response.onNext((.unsubscribed,false))
                //false -- dont revert the profile
            }else if type == .liked {
                 //false -- dont revert the profile
                self.subject_response.onNext((.unsubscribed,false))
            } else {
                self.subject_response.onNext((.unsubscribed,true))
                self.subject_response_forLike.onNext((.unsubscribed,true,selectedProfile))
                
                //true - revert the profile immediate
            }
            break
        default: //fails
                self.errorMsg = response.response["message"] as! String
                self.subject_response.onNext((type,false))
                break
            }
    }
}
