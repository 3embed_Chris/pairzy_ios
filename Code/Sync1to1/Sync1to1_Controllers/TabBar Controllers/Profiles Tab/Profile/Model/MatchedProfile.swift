//
//  MatchedProfiel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 24/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class MatchedProfile: NSObject {

    var name:String = ""
    var userId:String = ""
    var firstLikedByMe:Bool = true
    var profilePic:String = ""
    var matchDate:String = ""
    
    init(matchedUserDetails:[String:Any]) {
        if (matchedUserDetails[MatchedProfileData.firstLikedId] as? String == Helper.getMQTTID()) {
            self.userId = matchedUserDetails[MatchedProfileData.secondLikedId] as! String
            self.firstLikedByMe = true
            self.name = matchedUserDetails[MatchedProfileData.secondLikedByName] as! String
            self.profilePic = matchedUserDetails[MatchedProfileData.secondLikedByPhoto] as! String
        } else {
            
            
            if let userId = matchedUserDetails[MatchedProfileData.firstLikedId] as? String {
                 self.userId = userId
            }
            
            if let name = matchedUserDetails[MatchedProfileData.firstLikedByName] as? String {
                self.name = name
            }
            
            if let profilePic = matchedUserDetails[MatchedProfileData.firstLikedByPhoto] as? String {
                 self.profilePic = profilePic
            }
            
            
            if let userId = matchedUserDetails[MatchedProfileData.firstLikedIdChat] as? String {
                self.userId = userId
            }
            
            if let name = matchedUserDetails[MatchedProfileData.firstLikedByNameChat] as? String {
                self.name = name
            }
            
            if let profilePic = matchedUserDetails[MatchedProfileData.firstLikedByPhotoChat] as? String {
                self.profilePic = profilePic
            }
            
           
            self.firstLikedByMe = false
            
           
            
            
            
            
            
        }
        
        if let dateOfBirth = matchedUserDetails[MatchedProfileData.matchDate] as? Int {
            self.matchDate = String(dateOfBirth)
        }
    }
}
