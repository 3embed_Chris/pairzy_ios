//
//  ProfileData.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 13/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//


import Foundation


struct ReportReason {
    var nameOfTheReason:String = ""
    var reasonId:String = ""
    
    init(reportDettails:String) {
        self.nameOfTheReason = reportDettails
    }
    
}





struct Profile
{
    var firstName: String
    var location : String
    var gender : String
    var mobileNumber : String
    var emailID    : String
    var countryCode : String
    var distance:String
    var isHiddenDistance:Int
    
    var heightInCM : Float
    var dateOfBirth : Date
    var profilePicture : String
    var profileVideo : String
    var profileVideoThumbnail : String
    var otherVideos : [String]
    var otherPics : [String]
    var moments = [MomentsModel]()
    var userId: String
    var userAge:String
    var isHiddenUserAge:Int
    var work: String
    var job: String
    var education:String
    var onlineStatus:Int
    var isUserBlocked:Int
    var isBlockedByOther:Int
    var instaGramToken:String
    var instaGramProfileId:String
    var about:String
    var yesVotes:String = " "
    var noVotes:String = " "
    var liked = 0
    var likedPost = false
    var superLiked = 0
    var unliked = 0
    
    var hasProfileVideo = 0
    var creation : Double = 0.0
    var matchDate:String
    var userdeepLink:String
    var activityStatus:Int
    var currentPhotoIndex = 0
    var totalPhotosAndVideos = 0
    var chatInitiatedBy = 1
    var messageType = "0"
    var isSelfMsg : Bool
    
    var allMedia:[String]
    var myPreferences = [PreferencesModel]()
    
    var isCoversationStarted = 0
    
    var isMatchedBySuperLike = 0
    
    var isMatchedUser = 0
    
    var likeCount = 0
    var superLikeCount = 0
    var rewindCount = 0
    var totalUnread = 0
    var lastMessage = ""
    var lastMessageTime:Double = 0.0
    var chatDocIDFromAPI:String
    
    
    //User Post
    var userName = ""
    var type = ""
    var typeFlag = 2
    var postedOn = 0.00
    var createdOn = 0
    var date = ""
    var description = ""
    var status = 0
    //var url = ""
   // var likeCount = 0
    var commentCount = 0
    var postId = ""
    
    static func fetchprofileDataObj(userDataDictionary:[String :Any]) -> Profile {
        
        var profileDataObj = Profile(firstName: "",
                                     location: "",
                                     gender: "",
                                     mobileNumber: "",
                                     emailID: "",
                                     countryCode: "",
                                     distance:"less than",
                                     isHiddenDistance: 0,
                                     heightInCM: 0.0,
                                     dateOfBirth: Date(),
                                     profilePicture: "",
                                     profileVideo: "", profileVideoThumbnail: "",
                                     otherVideos: [],
                                     otherPics: [],
                                     moments: [],
                                     userId:"",
                                     userAge:"", isHiddenUserAge: 0,
                                     work:"",
                                     job:"",
                                     education:"",
                                     onlineStatus:0, isUserBlocked: 0, isBlockedByOther: 0, instaGramToken: "", instaGramProfileId: "", about: "", yesVotes: " ", noVotes: " ", liked: 0, likedPost: false, superLiked: 0, unliked: 0, hasProfileVideo: 0, creation:0.0, matchDate:"",
                                     userdeepLink:"",
                                     activityStatus:100, currentPhotoIndex:0, totalPhotosAndVideos:0, chatInitiatedBy:1, messageType: "0", isSelfMsg : false, allMedia:[], myPreferences: [], isCoversationStarted:0,isMatchedBySuperLike:0,isMatchedUser:0, likeCount: 0, superLikeCount: 0, rewindCount: 0, totalUnread: 0, lastMessage: "", lastMessageTime: 0.0, chatDocIDFromAPI:"", userName: "",type: "",typeFlag:0,postedOn:0.00,createdOn: 0,date:"",description:"",status:0, commentCount: 0,postId:"")

        
        if let userName = userDataDictionary["userName"] as? String{
            profileDataObj.userName = userName
        }
        
        if let userId = userDataDictionary["userId"] as? String{
            profileDataObj.userId = userId
        }
        if let type = userDataDictionary["type"] as? String{
            profileDataObj.type = type
        }
        if let type = userDataDictionary["messageType"] as? String{
            profileDataObj.messageType = type
        }
        if let typeFlag = userDataDictionary["typeFlag"] as? Int{
            profileDataObj.typeFlag = typeFlag
        }
        if let createdOn = userDataDictionary["createdOn"] as? Int{
            profileDataObj.createdOn = createdOn
        }
        if let description = userDataDictionary["description"] as? String{
            profileDataObj.description = description
        }
        
        if let yesVotes = userDataDictionary["yesVotes"] as? Int{
            profileDataObj.yesVotes = String(yesVotes)
        }
        
        if let noVotes = userDataDictionary["noVotes"] as? Int{
            profileDataObj.noVotes = String(noVotes)
        }
        
//        if let url = userDataDictionary["url"] as? String{
//            profileDataObj.url = url
//        }
        if let postedOn = userDataDictionary["postedOn"] as? Int{
            profileDataObj.postedOn = Double(postedOn)
        }
        if let postedOn = userDataDictionary["postedOn"] as? Double{
            profileDataObj.postedOn = postedOn
        }
        if let status = userDataDictionary["status"] as? Int{
            profileDataObj.status = status
        }
        if let likeCount = userDataDictionary["likeCount"] as? Int{
            profileDataObj.likeCount = likeCount
        }
        if let commentCount = userDataDictionary["commentCount"] as? Int{
            profileDataObj.commentCount = commentCount
        }
        if let date = userDataDictionary["date"] as? String{
            profileDataObj.date = date
        }
        if let liked = userDataDictionary["liked"] as? Bool{
            profileDataObj.likedPost = liked
        }

        //guard let userID = UserDefaults.standard.value(forKey: UserDefaultsKeys.userID) as? String else { return }
        let userId = UserLoginDataHelper.getUserId()
            if userDataDictionary["senderId"] as? String == userId{
                profileDataObj.isSelfMsg = true
            }else{
                profileDataObj.isSelfMsg = false
                
        }
        
       
        
      
        
        
//        if let likers = userDataDictionary["Likers"] as? [String]{
//            profileDataObj.userName = userName
//        }
//
        
        if let postId = userDataDictionary["postId"] as? String{
            profileDataObj.postId = postId
        }
        
        if let instaToken = userDataDictionary[ServiceInfo.InstaGramToken] as? String{
            profileDataObj.instaGramToken = instaToken
        }
        
        if let chatId = userDataDictionary["chatId"] as? String{
            profileDataObj.chatDocIDFromAPI = chatId
        }
        if let about = userDataDictionary["about"] as? String{
            profileDataObj.about = about
        }
        
        if let chatId = userDataDictionary["chatId"] as? String{
            profileDataObj.chatDocIDFromAPI = chatId
        }
        
        if let chatInitiatedBy = userDataDictionary["initiated"] as? Int
        {
            profileDataObj.chatInitiatedBy = chatInitiatedBy
        }
        
        if let isSupperLikedMe = userDataDictionary[ServiceInfo.isSuperLikedMe] as? Int {
            profileDataObj.isMatchedBySuperLike = isSupperLikedMe
        }
        
        if let isSupperLikedMe = userDataDictionary["totalUnread"] as? Int {
            profileDataObj.totalUnread = isSupperLikedMe
        }
        if let creation = userDataDictionary["timestamp"] as? Double {
            profileDataObj.lastMessageTime = creation/1000
        }
        
        
        if let instaToken = userDataDictionary["payload"] as? String{
            profileDataObj.lastMessage = instaToken
        }
        
        if let isMatchedUser = userDataDictionary[profileData.isMatched] as? Int {
            profileDataObj.isMatchedUser = isMatchedUser
        }
        if let isMatchedUser = userDataDictionary["isMatchedUser"] as? Int {
            profileDataObj.isMatchedUser = isMatchedUser
        }
        
        if let instaId = userDataDictionary[ServiceInfo.InstaGramProfileId] as? String{
            profileDataObj.instaGramProfileId = instaId
        }
        
        if let firstName = userDataDictionary[profileData.Fname] as? String{
            profileDataObj.firstName = firstName
        }
        
        if let userBlock = userDataDictionary[profileData.IsUserBlocked] as? Int{
            profileDataObj.isUserBlocked = userBlock
        }
        if let userBlock = userDataDictionary["isBlockedByMe"] as? Int{
            profileDataObj.isUserBlocked = userBlock
        }
        
        if let userBlock = userDataDictionary["isBlocked"] as? Int{
            profileDataObj.isBlockedByOther = userBlock
        }
        
        if let isCoversationStarted = userDataDictionary[profileData.isCoversationStarted] as? Int{
            profileDataObj.isCoversationStarted = isCoversationStarted
        }
        
        if let userId = userDataDictionary[profileData.opponentId] as? String{
            profileDataObj.userId = userId
        }
        
        if let userId = userDataDictionary["recipientId"] as? String{
            profileDataObj.userId = userId
        }
        
        
        
        if let userId = userDataDictionary["_id"] as? String{
            profileDataObj.userId = userId
        }
        
        if let distance = userDataDictionary[profileData.distance] as? [String:Any]{
            if let dist =  distance[profileData.IsHidden] as? Int {
                profileDataObj.isHiddenDistance = dist
            }
            if let distValue =  distance[profileData.Value] as? Double {
                
                let tempDist = distValue * 1.6094
                
                if distValue == 0.0 || distValue < 1.0 {
                     profileDataObj.distance = StringConstants.lessThan()
                }else {
                    let roundedVal = Double(round(distValue))
                    let valueInInteger = Int(roundedVal)
                    profileDataObj.distance = String(valueInInteger)
                }
            }
            
            if let distValue =  distance[profileData.Value] as? Float {
                if distValue == 0.0 || distValue < 1.0 {
                     profileDataObj.distance = StringConstants.lessThan()
                }else {
                    let dist = distValue
                    let valueInInteger = Int(dist)
                    profileDataObj.distance = String(valueInInteger)
                }
                
            }
        }
        
        
        
        
        if let count = userDataDictionary[profileData.count] as? [String:Any]{
            if let likeC =  count["like"] as? Int {
                profileDataObj.likeCount = likeC
            }
            if let rewindC =  count["rewind"] as? Int {
                profileDataObj.rewindCount = rewindC
            }
            if let supeC =  count["superlike"] as? Int {
                profileDataObj.superLikeCount = supeC
            }
        }
        
        
        if let creation = userDataDictionary[ApiResponseKeys.creation] as? Double {
            profileDataObj.creation = creation/1000
        }
        
        if let deepLink = userDataDictionary[profileData.deepLink] as? String {
            profileDataObj.userdeepLink = deepLink
        }
        
        if let status = userDataDictionary[profileData.activityStatus] as? Int {
            profileDataObj.activityStatus = status
        }
        
        if let gender = userDataDictionary[profileData.Gender] as? String{
            profileDataObj.gender = gender
        }
        
        if let dateOfBirth = userDataDictionary[MatchedProfileData.matchDate] as? Int {
            profileDataObj.matchDate = String(dateOfBirth)
        }
        
        if let emailID = userDataDictionary[profileData.Email] as? String{
            profileDataObj.emailID = emailID
        }
        
        if let dateOfBirth = userDataDictionary[profileData.DateOfBirth] as? Int {
            profileDataObj.userAge = String(Helper.dateFromMilliseconds(timestamp:String(dateOfBirth)).age)
        }
        
        
        if let age = userDataDictionary[profileData.Age] as? [String:Any] {
            if let userAge = age[profileData.Value] as? Int {
                profileDataObj.userAge = String(userAge)
            }
            if let ageHidden = age[profileData.IsHidden] as? Int {
                profileDataObj.isHiddenUserAge = ageHidden
            }
        }
        
        if let mobileNumber = userDataDictionary[profileData.MobileNumber] as? String{
            profileDataObj.mobileNumber = mobileNumber
        }
        
        if let heightInCM = userDataDictionary[profileData.Height] as? Float{
            profileDataObj.heightInCM = heightInCM
        }
        
        if let work = userDataDictionary[profileData.work] as? String{
            profileDataObj.work = work
        }
        
        if let job = userDataDictionary[profileData.job] as? String{
            profileDataObj.job = job
        }
        
        if let education = userDataDictionary[profileData.education] as? String{
            profileDataObj.education = education
        }
        if let onlineStatus = userDataDictionary[profileData.onlineStatus] as? Int{
            profileDataObj.onlineStatus = onlineStatus
        }
        
        
        if let otherVideos = userDataDictionary[profileData.otherfileVideo] as? [String]{
            profileDataObj.otherVideos = otherVideos
        }
        
        var hasProfilePic = 0
        
        if let profilePic = userDataDictionary[profileData.profilePic] as? String{
            profileDataObj.profilePicture = profilePic
            if(profilePic.count>5) {
                hasProfilePic = 1
                profileDataObj.allMedia.insert( profileDataObj.profilePicture, at: 0)
            }
        }
        if let otherPics = userDataDictionary[profileData.otherProfilePic] as? [String]{
            profileDataObj.otherPics = otherPics
        }
//
        
        
        profileDataObj.hasProfileVideo = 0
        
        if let profileVideo = userDataDictionary[profileData.profileVideo] as? String{
            profileDataObj.profileVideo = profileVideo
            if(profileVideo.count>5) {
                if let url = URL(string: profileDataObj.profileVideo) {
                    if url.pathExtension.count != 0 {
                        profileDataObj.hasProfileVideo = 1
                        profileDataObj.allMedia.insert(profileDataObj.profileVideo, at:0)
                    }
                }
            }
        }
        if let thumbnail = userDataDictionary[ServiceInfo.profileVideoThumbnail] as? String{
            profileDataObj.profileVideoThumbnail = thumbnail
        }
        
        if let allMedia = userDataDictionary[profileData.otherProfilePic] as? [String]{
            profileDataObj.allMedia = allMedia
            if(hasProfilePic != 0) {
                if(profileDataObj.hasProfileVideo != 0) {
                    profileDataObj.allMedia.insert(profileDataObj.profileVideo, at:0)
                    profileDataObj.allMedia.insert(profileDataObj.profilePicture, at:1)
                } else{
                    profileDataObj.allMedia.insert(profileDataObj.profilePicture, at:0)
                }
            }
            
        }
        
        if let allMedia = userDataDictionary["url"] as? [String]{
            profileDataObj.allMedia = allMedia
        }
        
        profileDataObj.totalPhotosAndVideos = profileDataObj.otherPics.count + hasProfilePic + profileDataObj.hasProfileVideo
        
        
        if let countryCode = userDataDictionary[profileData.CountCode] as? String {
            profileDataObj.countryCode = countryCode
        }
        
        if let momentsImages = userDataDictionary[ApiResponseKeys.moments] as? [[String:Any]] {
            for momentDetails in momentsImages {
                profileDataObj.moments.append(MomentsModel(momentsData: momentDetails))
            }
        }
        
        if let myPref = userDataDictionary[ApiResponseKeys.myPreferences] as? [[String:Any]] {
            for cat in myPref {
                profileDataObj.myPreferences.append(PreferencesModel.init(preferences: cat ))
            }
        }
        
        if let myPref = userDataDictionary["myPrefrances"] as? [[String:Any]] {
            for cat in myPref {
                profileDataObj.myPreferences.append(PreferencesModel.init(preferences: cat ))
                
            }
        }
        
        if let liked = userDataDictionary[profileData.Liked] as? Int{
            profileDataObj.liked = liked
        }
        
        
        if let unliked = userDataDictionary[profileData.Unliked] as? Int{
            profileDataObj.unliked = unliked
        }
        
        if let superliked = userDataDictionary[profileData.Superliked] as? Int{
            profileDataObj.superLiked = superliked
        }
        
        
        return profileDataObj
    }
    
    func dateFromMilliseconds(timestamp:String) -> Date {
        return Date(timeIntervalSince1970: TimeInterval(timestamp)!/1000)
    }
}



