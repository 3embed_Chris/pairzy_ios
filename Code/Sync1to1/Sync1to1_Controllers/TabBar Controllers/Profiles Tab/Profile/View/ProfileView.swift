//
//  ProfileViewModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 13/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import VGPlayer

class ProfileView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    @IBOutlet weak var userProfileImage: UIImageView!
    
    @IBOutlet weak var distnceLabel: UILabel!
    
    @IBOutlet weak var genderAndHeight: UILabel!
    
      @IBOutlet weak var playPauseButton: UIButton!
    
    @IBOutlet weak var prefButtonOutlet: UIButton!
    
    @IBOutlet weak var reportButtonOutlet: UIButton!
    
    @IBOutlet weak var UserNameLabel: UILabel!
    
    @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var dislikeButton: UIButton!
    @IBOutlet weak var viewForVideo: UIView!

}
