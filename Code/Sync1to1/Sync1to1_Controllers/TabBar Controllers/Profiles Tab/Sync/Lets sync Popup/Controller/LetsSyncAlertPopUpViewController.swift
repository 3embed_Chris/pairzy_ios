//
//  LetsSyncAlertPopUpViewController.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 30/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

protocol LetsSyncAlertButtonPressedDelegate: class{ //getting delegate methods for the button actions
    func nowButtonPressed()
    func LaterButtonPressed()
}

class LetsSyncAlertPopUpViewController: UIViewController {

   weak var letsSyncAlertButtonPressedDelegate : LetsSyncAlertButtonPressedDelegate? = nil
    
    //MARK:- VIEW DELEGATES
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- BUTTON ACTIONS
    
    /// NOW BUTTON ACTION
    ///
    /// - Parameter sender: NOEW BUTTON.
    @IBAction func nowButtonAction(_ sender: Any)
    {
         self.dismiss(animated: false, completion: nil)
        
        if (letsSyncAlertButtonPressedDelegate != nil){
            letsSyncAlertButtonPressedDelegate?.nowButtonPressed()
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    }

    
    /// LATxER BUTTON ACTION
    ///
    /// - Parameter sender: later button.
    @IBAction func laterButtonAction(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
        if (letsSyncAlertButtonPressedDelegate != nil){
            letsSyncAlertButtonPressedDelegate?.LaterButtonPressed()
        }
    }
}
