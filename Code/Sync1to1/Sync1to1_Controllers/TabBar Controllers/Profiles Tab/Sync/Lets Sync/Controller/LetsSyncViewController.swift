//
//  LetsSyncViewController.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 24/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher


protocol LetsSyncVCDelegate: class {
    func openChatVcForProifle(matchedProifle:MatchedProfile)
}

class LetsSyncViewController: UIViewController {
    
    struct Constants {
        static let letsSyncChatSegue = "letsSyncChatSegue"
        static let letsSyncRequestSegue = "letsSyncRequestSegue"
        static let openLetsSyncPopUpSegue = "openLetsSyncPopUpSegue"
    }
    
    @IBOutlet weak var matchTextLabelOutlet: UILabel!
    @IBOutlet weak var syncPersonImageViewOutlet: UIImageView!
    @IBOutlet weak var yourPicImageViewOutlet: UIImageView!
    
    @IBOutlet var chatButtonOutlet: UIButton!
    @IBOutlet weak var profile1View: UIView!
    @IBOutlet weak var profile2View: UIView!
    @IBOutlet weak var profileImage1: UIImageView!
    @IBOutlet weak var profileImage2: UIImageView!
    
    @IBOutlet weak var dateBtnView: UIView!
    @IBOutlet weak var chatBtnView: UIView!
    @IBOutlet weak var keepSwipingBtnView: UIView!
    
    @IBOutlet weak var heart1: HeartBeatingImageView!
    @IBOutlet weak var heart2: HeartBeatingImageView!
    @IBOutlet weak var heart3: HeartBeatingImageView!
    @IBOutlet weak var heart4: HeartBeatingImageView!
    @IBOutlet weak var heart5: HeartBeatingImageView!
    @IBOutlet weak var heart6: HeartBeatingImageView!
    @IBOutlet weak var heart7: HeartBeatingImageView!
    @IBOutlet weak var getOnDateBtn: UIButton!
    @IBOutlet weak var keepSwipingBtn: UIButton!
    @IBOutlet weak var itsMatchLabel: UILabel!
    
    var matchedProfile:MatchedProfile! = nil
   weak var delegate:LetsSyncVCDelegate! = nil
     var popUpFor:CoinsPopupFor = .forAll
    //MARK: - VIEW DELEGATES
    
    var selecteDateType:DateType = .none
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.matchTextLabelOutlet.text = String(format: StringConstants.youAndLike(), matchedProfile.name)
        //"You and \(matchedProfile.name) like each other"
        getOnDateBtn.setTitle(StringConstants.getOnADate(), for: .normal)
        keepSwipingBtn.setTitle(StringConstants.keepSwiping(), for: .normal)
        let url = URL(string: matchedProfile.profilePic)
        syncPersonImageViewOutlet.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "user_default_image"), options:nil, progressBlock: nil, completionHandler: nil)
        itsMatchLabel.text = StringConstants.itsMatch()
        let dbObj = Database.shared
        let userData = dbObj.userDataObj
        
        let userProfileUrl = URL(string: (userData?.profilePictureURL)!)
        yourPicImageViewOutlet.kf.setImage(with:userProfileUrl , placeholder: #imageLiteral(resourceName: "user_default_image"), options:nil, progressBlock: nil, completionHandler: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(videoCallIntiated), name:NSNotification.Name(rawValue:NSNotificationNames.notficationForVideoCallStarted), object: nil)
        
        let titleText = StringConstants.chatWith().appending(matchedProfile.name)
        self.chatButtonOutlet.setTitle(titleText, for: .normal)
        
        initialSetup()
        // Do any additional setup after loading the view.
    }
    
    
    func initialSetup(){
        
        profile1View.layer.cornerRadius = profile1View.frame.size.height/2
        profile2View.layer.cornerRadius = profile2View.frame.size.height/2
        
        profileImage1.layer.cornerRadius = profileImage1.frame.size.height/2
        profileImage2.layer.cornerRadius = profileImage2.frame.size.height/2
        Helper.setShadow(sender: profile1View )
        Helper.setShadow(sender: profile2View )
        
        dateBtnView.layer.cornerRadius = dateBtnView.frame.size.height/2
        chatBtnView.layer.cornerRadius = chatBtnView.frame.size.height/2
        keepSwipingBtnView.layer.cornerRadius = chatBtnView.frame.size.height/2
        
        dateBtnView.layer.borderWidth = 2
        dateBtnView.layer.borderColor = UIColor.white.cgColor
        
        chatBtnView.layer.borderWidth = 2
        chatBtnView.layer.borderColor = UIColor.white.cgColor
        
        keepSwipingBtnView.layer.borderWidth = 2
        keepSwipingBtnView.layer.borderColor = UIColor.white.cgColor
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // self.heart3.startHeartBeating()
        self.heart1.startHeartBeatAnimation()
        self.heart2.startHeartBeatAnimation()
        self.heart3.startHeartBeatAnimation()
        self.heart4.startHeartBeatAnimation()
        self.heart5.startHeartBeatAnimation()
        self.heart6.startHeartBeatAnimation()
        self.heart7.startHeartBeatAnimation()
        Helper.hidePushNotifications(hidePush: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Helper.hidePushNotifications(hidePush: true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Helper.hidePushNotifications(hidePush: false)
    }
    
   @objc func videoCallIntiated() {
        self.dismiss(animated: true, completion:nil)
        self.removeObserer()
    }
    
    func removeObserer() {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showPopup(){
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let syncView = dateCallPopUpView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        syncView.loadPopUpView()
        syncView.datePopUpDelegate = self
        window.addSubview(syncView)
        
    }
    
    
    //MARK: -  BUTTON ACTIONS
    
    
    /// button action for start conversation.
    ///
    /// - Parameter sender: start conversation button
    @IBAction func startConversationButtonAction(_ sender: Any)
    {
       self.delegate.openChatVcForProifle(matchedProifle: matchedProfile)
       self.removeObserer()
        self.dismiss(animated: false, completion:nil)
    }
    
    
    /// sync date button action.
    ///
    /// - Parameter sender: sync date button.
    @IBAction func syncDateButtonAction(_ sender: Any)
    {
        if Helper.isUserHasMinimumCoins(minValue:coinsFor.createDate) {
            showPopup()
        } else {
            showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coinEmpty"), mainTitle: StringConstants.CoinsGotOver(), subTitle: StringConstants.NoEnoughCoins() + StringConstants.dateLower(), buttnTitle: StringConstants.BuyCoins(), dontShowHide: true)
         //   Helper.showAlertWithMessage(withTitle:ActionSheetButtonNames.oops, message:ActionSheetButtonNames.noCoinsForDate, onViewController:self)
        }
    }
    
    
    /// view more profiles button action.
    ///
    /// - Parameter sender: view more profiles button.
    @IBAction func viewMoreProfileButtonAction(_ sender: Any)
    {
        self.removeObserer()
        self.dismiss(animated: true, completion: nil)
    }
    
    
   
    
}

extension LetsSyncViewController:spendSomeCoinPopUpDelegate {
    
    func coinBtnPressed(sender:UIButton) {
        let title = sender.title(for: .normal)
        if title == StringConstants.BuyCoins() {
            gotoCoinBalanceScreen()
            return
        }
        
        if self.popUpFor == .forDate {
            self.gotoRescheduleVC(flag: false, dateType: selecteDateType)
            Helper.showCoinsDeductPopup(flag: false, popUpFor: .forDate)
        }
        if self.popUpFor == .forChat {
            
            Helper.showCoinsDeductPopup(flag: false, popUpFor: .forChat)
        }
       
        
    }
    
    
    func gotoCoinBalanceScreen()
    {
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "addCoin") as! AddCoinsVC
        userBasics.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.pushViewController(userBasics, animated: true)
        return
    }
}

//MARK:- SYNC DATE ALERT DELAGATES.

extension LetsSyncViewController {
    
   
    /// method for intiate video call.
    ///
    /// - Parameter data: data contains matched profile details.
    func startVideoCall(data:MatchedProfile,dateIdForCall:String) {
        
        guard let ownID = Helper.getMQTTID() else { return }
        MQTTCallManager.sendcallAvilibilityStatus(status: 0, topic: MQTTTopic.callsAvailability + ownID)
        UserDefaults.standard.set(true, forKey: "iscallBtnCliked")
        
        let userID = data.userId
        let registerNum = ""
        let dict = ["callerId": userID,
                    "callType" : CallTypes.videoCall ,
                    "registerNum": registerNum,
                    "callId": randomString(length: 100),
                    "callerIdentifier": ownID,
                    "dateId":dateIdForCall] as [String:Any]
        
        UserDefaults.standard.set(dict, forKey: "storeIndexPath")
        MQTT.sharedInstance.subscribeTopic(withTopicName: MQTTTopic.callsAvailability + userID , withDelivering: .atLeastOnce)
        
        //for VideoCall
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = IncomingVideocallView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.tag =  17
        videoView.setCallId()
        videoView.otherCallerId = userID
        videoView.calling_userName.text = String(format:"%@",(data.name))
        videoView.userImageView.kf.setImage(with: URL(string: (data.profilePic)), placeholder: UIImage(), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
        })
        
        videoView.addCameraView()
        window.addSubview(videoView)
    }
    
}
extension LetsSyncViewController: DatePopUpDelegate {
    
    func VoiceCallDateBtnPressed() {
        selecteDateType = .audioDate
        gotoRescheduleVC(flag: true, dateType: .audioDate)
        
    }
    
    
    func canceled() {}
    
    func VideoCallDateBtnPressed() {
        selecteDateType = .videoDate
         gotoRescheduleVC(flag: true, dateType: .videoDate)
    }
    
    func physicalDateBtnPressed() {
       // dateButtonTapped()
        selecteDateType = .inPersonDate
        gotoRescheduleVC(flag: false, dateType:.inPersonDate)
        
    }
    
    
    func gotoRescheduleVC(flag:Bool,dateType:DateType){
        
        let vc = UIStoryboard(name:"DatumTabBarControllers", bundle: nil).instantiateViewController(withIdentifier: "planDate") as! ReScheduleVC
        vc.createDateVM.isForCallDate = flag
        vc.createDateVM.matchedProfile = self.matchedProfile
        vc.createDateVM.isFromMatch = true
        vc.createDateVM.dateType = dateType

        let preferenceNav: UINavigationController =  UINavigationController(rootViewController: vc)
        preferenceNav.isNavigationBarHidden = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func dateButtonTapped() {
        
        if Helper.isUserHasMinimumCoins(minValue: coinsFor.sendUnMatchMessage) {
            if  Helper.isShowCoinsDeductPopup(popUpFor: .forChat) {
                showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coins"), mainTitle: StringConstants.spendSomeCoinsForDate(), subTitle: StringConstants.getChanceTodate(), buttnTitle: StringConstants.hundredCoins(), dontShowHide: true)
                self.popUpFor = .forDate
            }else {
                gotoRescheduleVC(flag: false, dateType:.inPersonDate)
            }
        } else {
            showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coinEmpty"), mainTitle: StringConstants.CoinsGotOver(), subTitle: StringConstants.NoEnoughCoins() + StringConstants.dateLower(), buttnTitle: StringConstants.BuyCoins(), dontShowHide: true)
        }
        
    }
    
    func chatButtonTapped() {
        
        if Helper.isUserHasMinimumCoins(minValue: coinsFor.sendUnMatchMessage) {
            if  Helper.isShowCoinsDeductPopup(popUpFor: .forChat) {
                showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coins"), mainTitle: StringConstants.spendCoinsForChat(), subTitle: StringConstants.getChanceChat() , buttnTitle: StringConstants.hundredCoins(), dontShowHide: true)
                self.popUpFor = .forChat
            }else {
                
            }
        } else {
            showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coinEmpty"), mainTitle: StringConstants.CoinsGotOver(), subTitle: StringConstants.NoEnoughCoins() + StringConstants.chatLower(), buttnTitle: StringConstants.BuyCoins(), dontShowHide: true)
        }
        
    }
    
    
    
    func showCoinsPopupForChat(icon: UIImage, mainTitle: String, subTitle: String, buttnTitle: String, dontShowHide: Bool){
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let coinsView = spendSomeCoinPopUp(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        coinsView.loadPopUpView()
        coinsView.updatePopup(icon: icon, mainTitle: mainTitle, subTitle: subTitle, buttnTitle: buttnTitle, dontShowHide: dontShowHide)
        coinsView.spendSomeCoinDelegate = self
        window.addSubview(coinsView)
    }
    
}
