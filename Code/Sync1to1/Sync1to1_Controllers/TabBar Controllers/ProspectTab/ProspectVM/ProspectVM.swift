//
//  ProspectVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 25/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ProspectVM: NSObject {
    
    
    let disposeBag = DisposeBag()
    let ProspectVM_response = PublishSubject<[Profile]>()
    
    var supperLikesBy = [Profile]()
    var likesBy  =  [Profile]()
    var myLikes =  [Profile]()
    var mySupperLikes = [Profile]()
    var recentVisitors = [Profile]()
    var myUnLikes = [Profile]()
    var onlineUsers = [Profile]()
    
    
    var collectionView:UICollectionView? = nil
    
    
    
    
    /// Get OnlineUsers
    func getOnlineUsers(offset:Int,limit:Int){
        let apiCall = ProspectAPICalls()
        apiCall.requestForOnlineUsers(offset: offset, limit: limit)
        apiCall.subject_response
            .subscribe(onNext: { response in
                Helper.hideProgressIndicator()
                let dictForSave =  response.response
                var tempOnlineUsers = [Profile]()
                if let dataDict = dictForSave["data"] as? [[String:Any]] {
                    for profile in dataDict {
                        tempOnlineUsers.append(Profile.fetchprofileDataObj(userDataDictionary:profile))
                    }
    
                }
                self.onlineUsers = tempOnlineUsers
                self.collectionView?.reloadData()
                self.ProspectVM_response.onNext(self.onlineUsers)
                
            }, onError: { error in
                Helper.hideProgressIndicator()
                Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
            })
            .disposed(by:disposeBag)
    }
    
    
    
    /// Get superLiked me ,supperLikesBy
    func getSuperLikedMe(offset:Int,limit:Int){
        let apiCall = ProspectAPICalls()
        apiCall.requestForSuperLikedBy(offset:offset,limit:limit)
        
        apiCall.subject_response
            .subscribe(onNext: { response in
                
                Helper.hideProgressIndicator()
                self.handleGetPreferenceResponse(responseModel: response)
            }, onError: { error in
                Helper.hideProgressIndicator()
                Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
            })
            .disposed(by:disposeBag)
    }
    
    
    
    /// get My Super Likes
    func getMySuperLikes(offset:Int,limit:Int){
        let apiCall = ProspectAPICalls()
        apiCall.requestForMySuperLikes(offset:offset,limit:limit)
        apiCall.subject_response.subscribe(onNext: { response in
            print(response)
            Helper.hideProgressIndicator()
            let dictForSave =  response.response
            var tempMySupperLikes = [Profile]()
            if let dataDict = dictForSave["data"] as? [[String:Any]] {
                
                for profile in dataDict {
                    tempMySupperLikes.append(Profile.fetchprofileDataObj(userDataDictionary:profile))
                }
                
            }
            self.mySupperLikes = tempMySupperLikes
            self.collectionView?.reloadData()
            self.ProspectVM_response.onNext(self.mySupperLikes)
            
        }, onError: { error in
            Helper.hideProgressIndicator()
            Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
            
        }).disposed(by: disposeBag)
        
        
    }
    
    /// get My Super Likes
    func getMyLikes(offset:Int,limit:Int){
        let apiCall = ProspectAPICalls()
        apiCall.requestForMyLikes(offset:offset,limit:limit)
        apiCall.subject_response.subscribe(onNext: { response in
            Helper.hideProgressIndicator()
            let dictForSave =  response.response
             var tempMyLikes =  [Profile]()
            if let dataDict = dictForSave["data"] as? [[String:Any]] {
            
                for profile in dataDict {
                    tempMyLikes.append(Profile.fetchprofileDataObj(userDataDictionary:profile))
                }
                
            }
            self.myLikes = tempMyLikes
            self.collectionView?.reloadData()
            self.ProspectVM_response.onNext(self.myLikes)
            
        }, onError: { error in
            Helper.hideProgressIndicator()
            Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
            
        }).disposed(by: disposeBag)
        
        
    }
    
    /// likes by
    func getLikesBy(offset:Int,limit:Int){
        let apiCall = ProspectAPICalls()
        apiCall.requestForLikesBy(offset:offset,limit:limit)
        apiCall.subject_response.subscribe(onNext: { response in
            Helper.hideProgressIndicator()
            var tempLikesBy = [Profile]()
            
            let dictForSave =  response.response
            if let dataDict = dictForSave["data"] as? [[String:Any]] {
                for profile in dataDict {
                    tempLikesBy.append(Profile.fetchprofileDataObj(userDataDictionary:profile))
                }
               
            }
            self.likesBy = tempLikesBy
            self.collectionView?.reloadData()
            self.ProspectVM_response.onNext(self.likesBy)
        }, onError: { error in
            Helper.hideProgressIndicator()
            Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
            
        }).disposed(by: disposeBag)
        
    }
    
    /// get My Super Likes
    func getRecentVisitors(offset:Int,limit:Int){
        let apiCall = ProspectAPICalls()
        apiCall.requestForRecentVisitors(offset:offset,limit:limit)
        apiCall.subject_response.subscribe(onNext: { response in
            Helper.hideProgressIndicator()
            let dictForSave =  response.response
            var tempRecentVisitors = [Profile]()
            if let dataDict = dictForSave["data"] as? [[String:Any]] {
                for profile in dataDict {
                    tempRecentVisitors.append(Profile.fetchprofileDataObj(userDataDictionary:profile))
                }
            }
            self.recentVisitors = tempRecentVisitors
            self.collectionView?.reloadData()
            self.ProspectVM_response.onNext(self.recentVisitors)
        }, onError: { error in
            Helper.hideProgressIndicator()
            Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
            
        }).disposed(by: disposeBag)
        
        
    }
    
    func getMyUnLikes(offset:Int,limit:Int){
        let apiCall = ProspectAPICalls()
        apiCall.requestForMyUnLikes(offset:offset,limit:limit)
        apiCall.subject_response.subscribe(onNext: { response in
            Helper.hideProgressIndicator()
            let dictForSave =  response.response
             var tempMyUnlikes = [Profile]()
            if let dataDict = dictForSave["data"] as? [[String:Any]] {
            
                for profile in dataDict {
                    tempMyUnlikes.append(Profile.fetchprofileDataObj(userDataDictionary:profile))
                }
               
            }
            self.myUnLikes = tempMyUnlikes
            self.collectionView?.reloadData()
            self.ProspectVM_response.onNext(self.myUnLikes)
            
        }, onError: { error in
            Helper.hideProgressIndicator()
            Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
            
        }).disposed(by: disposeBag)
        
        
    }
    
    /// get coin balance
    func getCoinBalance(){
        
        let apiCalls = CoinsAPICalls()
        
        apiCalls.requestForGetCoinBalance()
        apiCalls.subject_response
            .subscribe(onNext: {response in
                if let data = response.response[ApiResponseKeys.data] as? [String:Any] {
                    let coinBalance = CoinBalanceModel.init(data: data)
                    UserDefaults.standard.set(coinBalance.coin, forKey: "coinBalance")
                }
               self.ProspectVM_response.onNext(self.supperLikesBy)
            }, onError: { error in
                self.ProspectVM_response.onError(error)
            })
            .disposed(by:disposeBag)
    }
    
    
    func handleGetPreferenceResponse(responseModel: ResponseModel) {
        
        guard let statuscode:API.ErrorCode = API.ErrorCode(rawValue: responseModel.statusCode) else { return }
        
        switch (statuscode) {
            
        case .Success:
            
            
            
            if let dataDict = responseModel.response["data"] as? [[String:Any]] {
                var tempSupperLikesBy = [Profile]()
                for profile in dataDict {
                    tempSupperLikesBy.append(Profile.fetchprofileDataObj(userDataDictionary:profile))
                }
                self.getCoinBalance()
                self.supperLikesBy = tempSupperLikesBy
                self.collectionView?.reloadData()
                
                
                self.ProspectVM_response.onNext(self.supperLikesBy)
            }
            
            break
        case .PreconditionFailed:
            break
            
        default:
            print(responseModel.response)
        }
        
    }
    
}
