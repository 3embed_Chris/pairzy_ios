//
//  ProspectVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 23/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class ProspectVC: UIViewController {
    
    var onlineOffset = 0
    var onlineLimit = 20
    
    let topLabels = [StringConstants.online(),StringConstants.superLikedMe(),StringConstants.likesMe(),StringConstants.recentVisitors(),StringConstants.passed(),StringConstants.myLikes(),StringConstants.mySuperLikes()]

    var currentLabel = 0
    let prospectVM = ProspectVM()
    var profileForChatSelected = Profile.fetchprofileDataObj(userDataDictionary: [:])
    var popUpFor:CoinsPopupFor = .forAll
    
    @IBOutlet weak var topCatView: UIView!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var topLabelsCollectonView: UICollectionView!
  
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var balanceBtn: UIButton!
  
    @IBOutlet weak var profileImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prospectVM.collectionView = mainCollectionView
      //  didGetResponse()
        let nameOfObserVer = NSNotification.Name(rawValue: "updateCoinsBal")
        NotificationCenter.default.setObserver(self, selector: #selector(updateCoinBalance), name: nameOfObserVer, object: nil)

        balanceBtn.layer.borderColor = Colors.coinBalanceBtnBorder.cgColor
        balanceBtn.layer.borderWidth = 1
        balanceBtn.layer.cornerRadius = 5
        balanceBtn.layer.cornerRadius = balanceBtn.frame.size.height/2
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        requestApisForupdate()
        updateCoinBalance()
        setProfileImage(profileImage: profileImage)
        setUpProfileImageUI(profileImage: profileImage)
        self.navigationController?.navigationBar.isHidden = false
    }
    

    
    @objc func updateCoinBalance(){
        var balance = " 0"
        if let getBal = UserDefaults.standard.object(forKey: "coinBalance") as? Int {
            if getBal > 1000 {
                let floatVal = Float(Float(getBal)/1000.0)
                let inter = floor(floatVal)
                let decimal = floatVal.truncatingRemainder(dividingBy: 1)
                if decimal == 0.0{
                    balance = "\(Int(inter))k"
                }else {
                    balance = "\(Float(Float(getBal)/1000.0))k"
                }
            } else {
                balance = " " + String(getBal)
            }
            balanceBtn.setTitle(balance, for: .normal)
        } else {
            balanceBtn.setTitle(" 0", for: .normal)
        }
    }
    
    func subscribeForEvents() {
        NotificationCenter.default.addObserver(self, selector: #selector(requestApisForupdate), name:NSNotification.Name(rawValue: NSNotificationNames.notificationFortappedOnPushNotification), object: nil)
    }
    
    
   
    @IBAction func searchAction(_ sender: Any) {
        
        
    }

    
    @IBAction func optionsAction(_ sender: Any) {
        
        
    }
    
    @IBAction func viewProfileAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: SegueIds.ProspectsVCToProfileVC, sender: nil)

    }
    
    @IBAction func balanceBtnAction(_ sender: Any) {
        //checking coins plans details from  appstore available or not.
        if(Helper.getCoinPLansFromAppStore().count > 0) {
            let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "addCoin") as! AddCoinsVC
            userBasics.navigationController?.setNavigationBarHidden(true, animated: false)
            self.navigationController?.pushViewController(userBasics, animated: true)
        }
    }
    
    
    
   @objc func requestApisForupdate() {
        
        prospectVM.getSuperLikedMe(offset: onlineOffset, limit: onlineLimit)
        prospectVM.getLikesBy(offset: onlineOffset, limit: onlineLimit)
        prospectVM.getMySuperLikes(offset: onlineOffset, limit: onlineLimit)
        prospectVM.getMyLikes(offset: onlineOffset, limit: onlineLimit)
        prospectVM.getMyUnLikes(offset: onlineOffset, limit: onlineLimit)
        prospectVM.getOnlineUsers(offset: onlineOffset, limit: onlineLimit)
        prospectVM.getRecentVisitors(offset: onlineOffset, limit: onlineLimit)
    }
    
    func setUpProfileImageUI(profileImage:UIImageView){
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        profileImage.layer.borderWidth = 1
        profileImage.layer.borderColor = UIColor.white.cgColor
    }
    
    func setProfileImage(profileImage:UIImageView){
        let userData = Database.shared.fetchResults()
        if let userDetails = userData.first{
            if let urlString:String = userDetails.profilePictureURL {
                if urlString.count>0 {
                    let url = URL(string: urlString)
                    var imageName = "man_default"
                    if userDetails.gender == "Male" {
                        imageName = "man_default"
                    }else {
                        imageName = "woman_default"
                    }
                    self.profileImage.kf.setImage(with:url, placeholder:UIImage.init(named:"02m"))
                }else{
                    profileImage.image = #imageLiteral(resourceName: "Splash")
                }
            }
        }
        
    }

    func getData(){
        
        switch currentLabel {
        case 0:
            prospectVM.getOnlineUsers(offset: onlineOffset, limit: onlineLimit)
            break
        case 1:
            prospectVM.getSuperLikedMe(offset: onlineOffset, limit: onlineLimit)
            break
        case 2:
            prospectVM.getLikesBy(offset: onlineOffset, limit: onlineLimit)
            break
        case 3:
            prospectVM.getRecentVisitors(offset: onlineOffset, limit: onlineLimit)
            break
        case 4:
            prospectVM.getMyUnLikes(offset: onlineOffset, limit: onlineLimit)
            break
        case 5:
            prospectVM.getMyLikes(offset: onlineOffset, limit: onlineLimit)
            break
        case 6:
            prospectVM.getMySuperLikes(offset: onlineOffset, limit: onlineLimit)
            break
            
        default:
            break
        }
   
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openSelectedProfile(openProfile:Profile) {
        
        profileForChatSelected = openProfile
        let profileVc = self.storyboard?.instantiateViewController(withIdentifier:StoryBoardIdentifier.profileVc) as! ProfileDetailVC
        profileVc.profileDeatilVM.profileDetails = openProfile
       
        self.navigationController?.present(profileVc, animated: true, completion:nil)
    }
    
  
    
}

extension ProspectVC {
    
    func didGetResponse(){
        prospectVM.ProspectVM_response.subscribe(onNext:{ success in
           self.updateCoinBalance()
        }).disposed(by: prospectVM.disposeBag)
    }
    
}


// MARK: - UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension ProspectVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return topLabels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == topLabelsCollectonView {
            let cell:TopLabelCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopLabelCollectionCell", for: indexPath) as! TopLabelCollectionCell
            
            
            cell.bottomIndicatorView.tag = indexPath.row
            let viewIn = UIView()
            viewIn.tag = currentLabel
            cell.updateCell(title: topLabels[indexPath.row],view: viewIn)
            
            return cell
            
        } else {
            let cell:ProspectCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIds.ProspectCollectionCell, for: indexPath) as! ProspectCollectionCell
            cell.mainTableView.tag = indexPath.row
            
            switch indexPath.row {
            case 0: //Online
                cell.data = prospectVM.onlineUsers
                cell.defaultIcon.image = #imageLiteral(resourceName: "prospectsEmpty")
                cell.emptyScreenTitle.text = topLabels[indexPath.row]
                cell.emptySubTitle.text = StringConstants.onlineMsg()
                cell.updateCell()
                break
                
            case 1://superLikesBy
                cell.data = [Profile]()
                cell.defaultIcon.image = #imageLiteral(resourceName: "super_like")
                cell.emptyScreenTitle.text = topLabels[indexPath.row]
                cell.emptySubTitle.text = StringConstants.superLikedMeMsg()
                cell.data = prospectVM.supperLikesBy
                cell.updateCell()
                break
                
            case 2: // likesBy
                cell.data = [Profile]()
                cell.defaultIcon.image = #imageLiteral(resourceName: "likes_me")
                cell.emptyScreenTitle.text = topLabels[indexPath.row]
                cell.emptySubTitle.text = StringConstants.likesMeMsg()
                cell.data = prospectVM.likesBy
                cell.updateCell()
                break
                
            case 3: //RecentVisitors
                cell.data = [Profile]()
                 cell.defaultIcon.image = #imageLiteral(resourceName: "recent_visitors")
                cell.emptyScreenTitle.text = topLabels[indexPath.row]
                cell.emptySubTitle.text = StringConstants.recentVisitorsMsg()
                cell.data = prospectVM.recentVisitors
                cell.updateCell()
                break
                
            case 4://my Unlikes
                cell.data = [Profile]()
                cell.defaultIcon.image = #imageLiteral(resourceName: "recent_visitors")
                cell.emptyScreenTitle.text = topLabels[indexPath.row]
                cell.emptySubTitle.text = StringConstants.passedMsg()
                cell.data = prospectVM.myUnLikes
                cell.updateCell()
                break
                
            case 5://myLikes
                cell.data = [Profile]()
                cell.defaultIcon.image = #imageLiteral(resourceName: "likes_me")
                cell.emptyScreenTitle.text = topLabels[indexPath.row]
                cell.emptySubTitle.text = StringConstants.myLikesMsg()
                cell.data = prospectVM.myLikes
                cell.updateCell()
                break
                
            case 6: //mySuperLikes
                cell.data = [Profile]()
                 cell.defaultIcon.image = #imageLiteral(resourceName: "super_like")
                cell.emptyScreenTitle.text = topLabels[indexPath.row]
                cell.emptySubTitle.text = StringConstants.mySuperLikesMsg()
                cell.data = prospectVM.mySupperLikes
                cell.updateCell()
                break
                
            default:
                break
            }
            
            cell.selectedDelegate = self
            
            return cell
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize.zero
        
        if collectionView == topLabelsCollectonView {
            
            let tempString = topLabels[indexPath.row]
            
            
            
            
            let font :UIFont = UIFont.init(name: CircularAir.Book, size: CGFloat(12))!
            let size: CGSize = tempString.size(withAttributes: [kCTFontAttributeName as NSAttributedStringKey: font])
            
            var cellWidth  = size.width+40
            let cellHight  = topLabelsCollectonView.frame.size.height
            
            if  cellHight > cellWidth {
                cellWidth = cellHight
            }
            return CGSize(width: cellWidth, height: cellHight)
        }
        else {
            
            let cellWidth = UIScreen.main.bounds.width
            let cellHight = collectionView.frame.size.height
            size.height =  cellHight
            size.width  =  cellWidth
            return size
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == topLabelsCollectonView) {
            let selectedIndex = indexPath.row
            let screenSize = NSInteger(self.view.frame.size.width)*selectedIndex
            self.mainCollectionView.setContentOffset(CGPoint(x:screenSize,y:0), animated:true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
}

// MARK: - UIScrollViewDelegate
extension ProspectVC: UIScrollViewDelegate {
    
    
    func requestRespectiveApi() {
        switch currentLabel {
        case 0:
            prospectVM.getOnlineUsers(offset: onlineOffset, limit: onlineLimit)
            break
        case 1:
            prospectVM.getSuperLikedMe(offset: onlineOffset, limit: onlineLimit)
            break
        case 2:
            prospectVM.getLikesBy(offset: onlineOffset, limit: onlineLimit)
            break
        case 3:
            prospectVM.getRecentVisitors(offset: onlineOffset, limit: onlineLimit)
            break
        case 4:
            prospectVM.getMyUnLikes(offset: onlineOffset, limit: onlineLimit)
            break
        case 5:
            prospectVM.getMyLikes(offset: onlineOffset, limit: onlineLimit)
            break
        case 6:
            prospectVM.getMySuperLikes(offset: onlineOffset, limit: onlineLimit)
            break
            
        default:
            break
        }
    }
    
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if scrollView == mainCollectionView {
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            currentLabel = Int(pageNumber)
            
            topLabelsCollectonView.selectItem(at: IndexPath(row: currentLabel, section: 0), animated: true, scrollPosition: .centeredHorizontally)
            
            requestRespectiveApi()
            
            topLabelsCollectonView.reloadData()
            mainCollectionView.reloadData()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == mainCollectionView {
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            currentLabel = Int(pageNumber)
            
            topLabelsCollectonView.selectItem(at: IndexPath(row: currentLabel, section: 0), animated: true, scrollPosition: .centeredHorizontally)
            
            requestRespectiveApi()
            
            topLabelsCollectonView.reloadData()
            mainCollectionView.reloadData()
        }
        
    }
  
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let movedOffset: CGFloat = scrollView.contentOffset.y
        
        
        if scrollView != mainCollectionView {
            self.view.endEditing(true)
            let pageWidth: CGFloat = 350          //171 - 64
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            
            if self.navigationController != nil {
                
                if ratio > 0 {
                    
                  //  Helper.addShadowToNavigationBar(controller: self.navigationController!)
                    Helper.setShadow(sender: topCatView)
                }else {
                    
                  //  Helper.removeShadowToNavigationBar(controller: self.navigationController!)
                Helper.removeShadow(sender: topCatView)
                }
                
            }
        }
        
    }
    
}

extension ProspectVC:selectedProifleDelegate {
    func openProfileWithDetails(selectedProfile:Profile) {
        self.openSelectedProfile(openProfile:selectedProfile)
    }
}

extension ProspectVC:profileInterestUpdated {
    func profileUpdatedWithStatus(status:profileActivityStatus) {
        
    }
    
    func openChatForProfile(profile:Profile) {
        self.profileForChatSelected = profile
        checkForCoins()
    }
    
    
        func checkForCoins(){
    
                if Helper.isUserHasMinimumCoins(minValue: coinsFor.sendUnMatchMessage) {
                    if  Helper.isShowCoinsDeductPopup(popUpFor: .forChat) {
                        showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coins"), mainTitle: StringConstants.spendCoinsForChat(), subTitle: StringConstants.getChanceChat(), buttnTitle: StringConstants.hundredCoins(), dontShowHide: true)
                        self.popUpFor = .forChat
                    }else {
                     openChatForProfileDetails()
                    }
                } else {
                    showCoinsPopupForChat(icon: #imageLiteral(resourceName: "coinEmpty"), mainTitle: StringConstants.CoinsGotOver(), subTitle: StringConstants.NoEnoughCoins() + StringConstants.superLike(), buttnTitle: StringConstants.BuyCoins(), dontShowHide: true)
                }
        }
    
    
    func showCoinsPopupForChat(icon: UIImage, mainTitle: String, subTitle: String, buttnTitle: String, dontShowHide: Bool){
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let coinsView = spendSomeCoinPopUp(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        coinsView.loadPopUpView()
        coinsView.updatePopup(icon: icon, mainTitle: mainTitle, subTitle: subTitle, buttnTitle: buttnTitle, dontShowHide: dontShowHide)
        coinsView.spendSomeCoinDelegate = self
        window.addSubview(coinsView)
    }
}
    
extension ProspectVC:spendSomeCoinPopUpDelegate {
    
    func coinBtnPressed(sender: UIButton) {
        
        let title = sender.title(for: .normal)
        if title == StringConstants.BuyCoins() {
            let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "addCoin") as! AddCoinsVC
            userBasics.navigationController?.setNavigationBarHidden(true, animated: false)
            self.navigationController?.pushViewController(userBasics, animated: true)
            return
        }
        
        openChatForProfileDetails()
        
    }

    
    func openChatForProfileDetails() {
        Helper.updateNewMatchChatDetails(isOpenChat: true)
        self.tabBarController?.selectedIndex = 3
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when){
            let notificationName = NSNotification.Name(rawValue: "SyncChatNotification")
            NotificationCenter.default.post(name: notificationName, object: self, userInfo: ["receiverID":self.profileForChatSelected])
        }
    }
}
