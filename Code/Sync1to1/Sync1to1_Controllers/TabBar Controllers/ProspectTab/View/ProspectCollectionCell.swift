//
//  ProspectCollectionCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 23/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit



protocol selectedProifleDelegate: class  {
    func openProfileWithDetails(selectedProfile:Profile)
}

class ProspectCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var noContentView: UIView!
    @IBOutlet weak var emptyScreenTitle: UILabel!
    @IBOutlet weak var emptySubTitle: UILabel!
    @IBOutlet weak var defaultIcon: UIImageView!
    
    var index = 0
    var onlineOffset = 0
    var onlineLimit = 20
    
    var data:[Profile] = [Profile]()
   weak var selectedDelegate:selectedProifleDelegate? = nil
    let prospectVM = ProspectVM()
    var indicator:UIActivityIndicatorView!
    var refreshControl: UIRefreshControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        indicator = UIActivityIndicatorView()
        indicator = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        indicator.color = Colors.SeparatorDark
        indicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        indicator.center = self.center
        indicator.startAnimating()
        //mainTableView.addSubview(indicator)
        
        onlineOffset = 0
        onlineLimit = 20
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ProspectCollectionCell.refresh), for: UIControlEvents.valueChanged)
        mainTableView.refreshControl = self.refreshControl
        refreshControl.tintColor = Colors.AppBaseColor
        mainTableView.backgroundColor = Colors.ScreenBackground
        didGetRespons()
    }
    
    func updateCell(){
       
        mainTableView.reloadData()
    }
    
    
   @objc func refresh() {
        self.indicator.stopAnimating()
        self.indicator.removeFromSuperview()
        index = 0
        onlineOffset = 0
        onlineLimit = 20
        getData()
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            self.refreshControl.endRefreshing()
        }
    }
    
    
    func getData(){
        
        switch mainTableView.tag {
        case 0:
            prospectVM.getOnlineUsers(offset: onlineOffset, limit: onlineLimit)
            break
        case 1:
            prospectVM.getSuperLikedMe(offset: onlineOffset, limit: onlineLimit)
            break
        case 2:
            prospectVM.getLikesBy(offset: onlineOffset, limit: onlineLimit)
            break
        case 3:
            prospectVM.getRecentVisitors(offset: onlineOffset, limit: onlineLimit)
            break
        case 4:
            prospectVM.getMyUnLikes(offset: onlineOffset, limit: onlineLimit)
            break
        case 5:
            prospectVM.getMyLikes(offset: onlineOffset, limit: onlineLimit)
            break
        case 6:
            prospectVM.getMySuperLikes(offset: onlineOffset, limit: onlineLimit)
            break
            
        default:
            break
        }
        
    }
    
}
extension ProspectCollectionCell: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.data.count == 0 {
            self.indicator.startAnimating()
            mainTableView.backgroundColor = UIColor.clear
        }
        else {
            mainTableView.backgroundColor = Colors.ScreenBackground
            self.indicator.stopAnimating()
            self.indicator.removeFromSuperview()
        }
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProspectTableCell", for: indexPath) as! ProspectTableCell
        cell.updateData(data: self.data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.selectedDelegate?.openProfileWithDetails(selectedProfile:data[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row + 1 == self.data.count {
            
            if self.data.count >= (index+1) * 20 {
                self.index = index + 1
                self.onlineOffset = index * 20
                self.onlineLimit = (index+1) * 20
                self.getData()
            }
        }else {
            if indexPath.row  == 0 {
                self.index = self.data.count/20
                self.onlineOffset = 0
                self.onlineLimit = 20
                
            }
        }
    }
}
extension ProspectCollectionCell {
    
    func didGetRespons(){
        
        prospectVM.ProspectVM_response.subscribe(onNext:{ success in
            if self.index == 0 {
                self.data = success
            }else {
                for eachProfile in success {
                    self.data.append(eachProfile)
                }
            }
//            self.indicator.stopAnimating()
//            self.indicator.removeFromSuperview()
            self.mainTableView.reloadData()
            
        }).disposed(by: prospectVM.disposeBag)
        
    }
    
    
    
}
