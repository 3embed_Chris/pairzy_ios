//
//  ProspectTableCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 23/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class ProspectTableCell: UITableViewCell {
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subTitle: UILabel!
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func initialSetup(){
        profilePic.layer.cornerRadius = profilePic.frame.size.height/2
    }
    
    
    func updateData(data: Profile){
        let newUrl = URL(string: data.profilePicture)
        
        var imageName = "man_default"
        if data.gender == "Male" {
            imageName = "man_default"
        }else {
            imageName = "woman_default"
        }
        
        profilePic.kf.setImage(with:newUrl, placeholder:UIImage.init(named:imageName))
        nameLabel.text = data.firstName
        if data.creation == 0.0 {
            timeLabel.text = ""
        } else {
            let  date:Date = Date(timeIntervalSince1970: Double(data.creation))
            timeLabel.text = CalenderUtility.getCurrentTime(fromDate: date)
        }
        
        if data.work.count != 0 {
            self.subTitle.text = data.work
        }else if data.education.count != 0 {
            self.subTitle.text = data.education
        }else{
            self.subTitle.text = data.job
        }
        
    }
    
}
