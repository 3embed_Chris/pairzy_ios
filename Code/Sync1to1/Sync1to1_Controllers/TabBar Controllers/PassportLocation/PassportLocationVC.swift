//
//  PassportLocationVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 17/07/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import CoreLocation

protocol PassportLocationVCDelegate: class {
    func didSelectAddress(address: Location)
}

class PassportLocationVC: UIViewController {
    
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var titleViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var screenTitle: UILabel!

    let locationManager = CLLocationManager()
    let selectLocVM = PassportLocationVM()
    var locationObj:Location? = nil
    // var selectedAdreess:Location? = nil
    var locationsArr = [Location]()
    var recentPassPorts = [Location]()
    var selectedIndex:IndexPath = IndexPath(row: -1, section: -1)
    weak var delegate:PassportLocationVCDelegate? = nil
    var selectedAddresses  = Helper.getOtherLocations()
    var selectedAdreess:Location? =  Helper.getFeatureLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        didGetLocation()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateLocation), name:NSNotification.Name(rawValue: NSNotificationNames.notificationForLocationUpdate), object: nil)
        Helper.addDefaultNavigationGesture(VC: self)
        if(Helper.isIPhonex()){
            self.titleViewHeightConstraint.constant = 44
        }
        screenTitle.text = StringConstants.location()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addObserver()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.removeObserver()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addNewLocAction(_ sender: Any) {
          checkLocationEnabled(isToAddNew: true)
//        if Helper.isSubscribedForPlans() {
//            checkLocationEnabled(isToAddNew: true)
//        }else {
//            openInAppPurchaseView()
//        }
    }
    
    func openInAppPurchaseView() {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = InAppPuchasePopupView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.loadInAppPurchaseView()
        window.addSubview(videoView)
    }
    
    
    func openLocationVC() {
        let locationVC = Helper.getSBWithName(name: "DatumTabBarControllers").instantiateViewController(withIdentifier: "LocationVC") as! ChooseLocationVC
        locationVC.delegate = self
        locationVC.locationVM.isFromEdit = true
        locationVC.locationVM.isForPassPort = true
        self.navigationController?.pushViewController(locationVC, animated: true)
    }
    
    
    func openMapVC(){
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "LocationSelectVC") as! MapVC
        userBasics.isForPassport = true
        // userBasics.isForMatch = true
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
    
    
}

extension PassportLocationVC {
    /// observing selectLocation View Model
    func didGetLocation(){
        self.selectLocVM.SelectLocVMResponse.subscribe(onNext: { success in
            
            
        },onError : { error in
            print(error)
        }).disposed(by: selectLocVM.disposeBag)
    }
    
}


// MARK: - UITableViewDataSource
extension PassportLocationVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        if selectLocVM.addressList.count == 0 && selectLocVM.recentSearchList.count == 0 && selectLocVM.savedAddressList.count == 0 {
            
        }else{
            
        }
        if section == 1 {
            if Helper.getIsFeatureExist() || selectedAdreess != nil || recentPassPorts.count != 0{
                return selectedAddresses.count
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "currentLocation", for: indexPath) as! MyCurrentLocationCell
        
        if indexPath.section == 0 {
            cell.updateCurrentLocation()
            if Helper.getIsFeatureExist() {
                cell.tickSelected.isSelected = false
            }else {
                cell.tickSelected.isSelected = true
            }
            
        }else {
            if selectedAdreess != nil {
                cell.updateRecentPassLoc(location: selectedAddresses[indexPath.row])
                
                if (selectedAddresses[indexPath.row].latitude.rounded(toPlaces: 2) == selectedAdreess?.latitude.rounded(toPlaces: 2)) && (selectedAddresses[indexPath.row].longitude.rounded(toPlaces: 2) == selectedAdreess?.longitude.rounded(toPlaces: 2))  {
                    cell.tickSelected.isSelected = true
                }else {
                    cell.tickSelected.isSelected = false
                }
            }
            cell.tickSelected.tag = indexPath.row
        }
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! SectionHeaderCell 
        
        
        switch section {
        case 0:
            cell.updateHeader(title: "CURRENT LOCATION")
            cell.headingLabel.font = UIFont (name: CircularAir.Bold, size: 14)
        // cell.separator.isHidden = false
        case 1:
            cell.updateHeader(title: "RECENT PASSPORT LOCATIONS")
             cell.headingLabel.font = UIFont (name: CircularAir.Bold, size: 14)
        //cell.separator.isHidden = false
        default:
            break
        }
        
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let header:AddLocationFooterView = AddLocationFooterView().shared
        header.footerDelegate = self
        return header
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "FooterForLocation")
//        return cell
    }
    
}

// MARK: - UITableViewDelegate
extension PassportLocationVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
       
        self.selectedIndex = indexPath
        if indexPath.section == 0 {
            let location = Utility.getCurrentAddress()
            delegate?.didSelectAddress(address: location)
            Helper.storeIsFeatureExist(isExist: false)
            Helper.saveFeaturLocation(data: Location(data: [:]))
          //  updateUserLocation()
            checkLocationEnabled(isToAddNew: false)
            
            
        }else {
            delegate?.didSelectAddress(address: selectedAddresses[indexPath.row])
            Helper.saveFeaturLocation(data: selectedAddresses[indexPath.row])
          //  updateUserLocation()
            checkLocationEnabled(isToAddNew: false)
            Helper.storeIsFeatureExist(isExist: true)

        }
        
        // self.navigationController?.popViewController(animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 48
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 1 {
            return 50
        }
        return 0
    }
    
}


//MARK:- LocationManager Delegate
extension PassportLocationVC : LocationManagerDelegate {
    func didUpdateLocation(location: Location, search: Bool, update: Bool) {
        
    }
    
    
    func didFailToUpdateLocation() {
        
    }
    
    func didChangeAuthorization(authorized: Bool) {
        
    }
    func didUpdateSearch(locations: [Location]) {
        selectLocVM.addressList = locations
        mainTableView.reloadData()
    }
    
}

extension PassportLocationVC:UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let _: CGFloat = scrollView.contentOffset.y
        if scrollView == mainTableView {
            if scrollView.contentOffset.y > 0 {
                Helper.setShadow(sender: headView)
            }else{
                Helper.removeShadow(sender: headView)
            }
        }
    }
    
}

extension PassportLocationVC {
    
    // setup the View
    func setUI() {
        selectLocVM.locationManager = LocationManager.shared
        selectLocVM.locationManager?.delegate = self
        _ = "" //Utility.getAddress()
        mainTableView.backgroundColor =  Colors.ScreenBackground
        }
    
    
}


extension PassportLocationVC : ChooseLocationVCDelegate {
    
    func didSelectAddress(address: Location) {
        if address.latitude != 0.0 || address.longitude != 0 {
            guard selectedAddresses.count > 0 else {
                self.selectedAddresses.insert(address, at: 0)
                Helper.saveOtherLocations(data: selectedAddresses)
                self.mainTableView.reloadData()
                return
            }
            for each in selectedAddresses {
                if (each.latitude.rounded(toPlaces: 2) == address.latitude.rounded(toPlaces: 2)) && (each.longitude.rounded(toPlaces: 2) == address.longitude.rounded(toPlaces: 2)){
                    print("its same")
                    return
                }else {
                    self.selectedAddresses.insert(address, at: 0)
                    Helper.saveOtherLocations(data: selectedAddresses)
                    self.mainTableView.reloadData()
                    
                }
            }
    }
}
    
    @objc func updateLocation(notification: NSNotification){
        var selectedAddr = notification.object as! Location
        if selectedAddr.latitude != 0.0 || selectedAddr.longitude != 0 {
            guard selectedAddresses.count > 0 else {
                self.selectedAddresses.insert(selectedAddr, at: 0)
                Helper.saveOtherLocations(data: selectedAddresses)
                self.mainTableView.reloadData()
                return
            }
//            for each in selectedAddresses {
//                if (each.latitude.rounded(toPlaces: 2) == selectedAddr.latitude.rounded(toPlaces: 2)) && (each.longitude.rounded(toPlaces: 2) == selectedAddr.longitude.rounded(toPlaces: 2)){
//                    print("its same")
//                    return
//                }else{
//
//                    self.selectedAddresses.insert(selectedAddr, at: 0)
//                    Helper.saveOtherLocations(data: selectedAddresses)
//                    self.mainTableView.reloadData()
//
//
//                }
//            }
            for (index,each) in selectedAddresses.enumerated() {
            
                if (each.latitude.rounded(toPlaces: 2) == selectedAddr.latitude.rounded(toPlaces: 2)) && (each.longitude.rounded(toPlaces: 2) == selectedAddr.longitude.rounded(toPlaces: 2)){
                    print("its same")
                    
                    return
                }else {
                    
                    if index == selectedAddresses.count - 1 {
                        self.selectedAddresses.insert(selectedAddr, at: 0)
                        Helper.saveOtherLocations(data: selectedAddresses)
                        self.mainTableView.reloadData()
                    }
                }
            
            }
            
        }
    }
    

    
    func updateUserLocation() {
        
         Helper.showProgressIndicator(withMessage: StringConstants.updating())
        let locDetails:[String:Any?] = Helper.getUserLocation()
        var localTimeZoneName: String { return TimeZone.current.identifier }

        var latitude = 0.0
        var longitude = 0.0
        var isPassportUser = false
        var featAddress = ""

        if Platform.isSimulator {
            print("Running on Simulator")
            latitude = 13.028694
            longitude = 77.589564
        } else {

         if Helper.getIsFeatureExist(){
                
                let featureLoc:Location =  Helper.getFeatureLocation()
                featAddress =  featureLoc.city + ", " + featureLoc.state + ", " + featureLoc.country
                latitude = featureLoc.latitude
                longitude = featureLoc.longitude
                if featAddress.count <  4 {
                    featAddress = featureLoc.locationDescription
                    
                    if featAddress.count < 4 {
                        featAddress = featureLoc.secondaryText
                    }
                }
                isPassportUser = true
                
        
            }else {
                if let lat = locDetails["lat"] as? Double {
                    latitude = lat
                }
                if let long = locDetails["long"] as? Double {
                     longitude = long
                }
                
                
                let location = Utility.getCurrentAddress()
                featAddress = location.city + ", " + location.state + ", " + location.country
                isPassportUser = false
            }
        }
    
        
        let userIp = Helper.getUserIP()
        
        let params =    [
            ServiceInfo.iPAddress:userIp,
            ServiceInfo.latitude            :latitude,
            ServiceInfo.longitude           :longitude,
            ServiceInfo.timeZone            :localTimeZoneName,
            ServiceInfo.isPassportLocation : isPassportUser,
            "address": featAddress] as [String: AnyObject]
   
        
        API.requestPatchURL(serviceName: APINAMES.updateLocation,
                            withStaticAccessToken:false,
                            params: params,
                            success: { (response) in
                                Helper.hideProgressIndicator()
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NSNotificationNames.userPassportedLocation), object:self, userInfo: params)
                                self.navigationController?.popViewController(animated: true)
        },
                            failure: { (Error) in
                                
        })
    }
    
    
    
    func checkLocationEnabled(isToAddNew: Bool){
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
//                let alert = UIAlertController(title: StringConstants.locationDesabled(), message: StringConstants.youNeedToEnable(), preferredStyle: .alert)
//
//                // Add "OK" Button to alert, pressing it will bring you to the settings app
//                alert.addAction(UIAlertAction(title: StringConstants.ok(), style: .default, handler: { action in
//                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
//                }))
//              self.present(alert, animated: true, completion: nil)
//                // self.present(alert, animated: true)
//                print(false,"No access")
                
                locationManager.startUpdatingLocation()
                if isToAddNew {
                    openMapVC()
                }else {
                    updateUserLocation()
                }
                print(true,"Access")
                
            case .authorizedAlways, .authorizedWhenInUse:
                locationManager.startUpdatingLocation()
                if isToAddNew {
                     openMapVC()
                }else {
                    updateUserLocation()
                }
                print(true,"Access")
            }
            
        }
    }
}
extension PassportLocationVC: footerViewDelegate{
    
    func footerViewTapped(){

        checkLocationEnabled(isToAddNew: true)
        
 //        if Helper.isSubscribedForPlans() {
//            checkLocationEnabled(isToAddNew: true)
//
//        }else {
//            openInAppPurchaseView()
//        }
        
    }
    
}

