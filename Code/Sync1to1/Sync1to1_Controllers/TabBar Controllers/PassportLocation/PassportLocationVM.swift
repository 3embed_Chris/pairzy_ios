//
//  PassportLocationVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 17/07/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PassportLocationVM: NSObject {
    
    let SelectLocVMResponse                 = PublishSubject<Bool>()
    let disposeBag = DisposeBag()
    
    
    var addressList                         = [Location]()
    var recentSearchList                    = [Location]()
    var savedAddressList                    = [Location]()
    var locationManager:LocationManager?    = nil
    var locationSelected:Location?          = nil
    var dataAddress:[String : Any]          = [:]
    var didSelectRow = false
    
   
}
