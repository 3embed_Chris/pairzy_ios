//
//  SelectLocationVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 19/07/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import GoogleMaps
protocol SelectLocationVCDelegate: class {
    func didSelectAddress(address: Location)
}

class SelectLocationVC: UIViewController {
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var buttonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var noContentView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var emptyScreenImage: UIImageView!
    @IBOutlet weak var emptyScreenLabel: UILabel!
    @IBOutlet weak var addressSearchBar: UISearchBar!
    
    let selectLocVM = SelectLocationVM()
    weak var delegate:SelectLocationVCDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        didGetLocation()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.dismissKeyBoard()
        self.removeObserver()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyBoard() {
        self.view.endEditing(true)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.dismissKeyBoard()
        }) { (Bool) in
            UIView.animate(withDuration: 0.3, animations: {
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
}

extension SelectLocationVC {
    /// observing selectLocation View Model
    func didGetLocation(){
        self.selectLocVM.SelectLocVMResponse.subscribe(onNext: { success in
            // Utility.saveAddress(location: self.selectLocVM.dataAddress)
            self.closeAction(UIButton())
    
        },onError : { error in
            print(error)
        })
    }
    
}


// MARK: - UITableViewDataSource
extension SelectLocationVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectLocVM.addressList.count == 0 {
            noContentView.isHidden = false
        }else{
            noContentView.isHidden = true
        }
        return selectLocVM.addressList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
       let cell = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell", for: indexPath) as! LocationTableViewCell
                cell.setValue(data: selectLocVM.addressList[indexPath.row])
                
    
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
     
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectLocHeader") as! SectionHeaderCell
        
        if selectLocVM.addressList.count > 0{
            cell.updateHeader(title: StringConstants.results())
        }else {
            cell.updateHeader(title: StringConstants.noResults())
        }
  
        return cell
        
        
    }
}

// MARK: - UITableViewDelegate
extension SelectLocationVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        var data:Location! = Utility.getCurrentAddress()
       
            if selectLocVM.addressList.count > 0 {
                data = selectLocVM.addressList[indexPath.row]
                self.delegate?.didSelectAddress(address: data)
            }
        self.closeAction(UIButton())
        selectLocVM.didSelectRow = true
    }
}

extension SelectLocationVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
        self.mainTableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text!.sorted().count > 0 {
            mainTableView.reloadData()
        }
        selectLocVM.locationManager?.search(searchText: searchBar.text!)
        
    }
}


//MARK:- LocationManager Delegate
extension SelectLocationVC : LocationManagerDelegate {
    func didUpdateLocation(location: Location, search: Bool, update: Bool) {
        selectLocVM.getLocation(location:location)
    }
    
    func didFailToUpdateLocation() {
        
    }
    
    func didChangeAuthorization(authorized: Bool) {
        
    }
    func didUpdateSearch(locations: [Location]) {
        selectLocVM.addressList = locations
        mainTableView.reloadData()
    }
    
}

extension SelectLocationVC:UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let movedOffset: CGFloat = scrollView.contentOffset.y
        if scrollView == mainTableView {
            if scrollView.contentOffset.y > 0 {
                Helper.setShadow(sender: headView)
            }else{
                Helper.removeShadow(sender: headView)
            }
        }
    }
    
}


extension SelectLocationVC {
    
    // setup the View
    func setUI() {
        emptyScreenLabel.textColor  =  Colors.PrimaryText
        self.addressSearchBar.tintColor = Colors.PrimaryText
        selectLocVM.locationManager = LocationManager.shared
        selectLocVM.locationManager?.delegate = self
        let locationSelected = Utility.getCurrentAddress()
        let textFieldInsideSearchBar = self.addressSearchBar.value(forKey: "searchField") as! UITextField
        textFieldInsideSearchBar.font = UIFont.systemFont(ofSize: 14)
        mainTableView.backgroundColor = Colors.ScreenBackground
        addressSearchBar.placeholder = StringConstants.searchAddress()
        if locationSelected.fullText.sorted().count == 0 {
            buttonWidthConstraint.constant = 10
            headView.updateConstraints()
            headView.layoutIfNeeded()
            closeButton.isHidden = true
            
        }
        addressSearchBar.becomeFirstResponder()
    }
    
    
}


