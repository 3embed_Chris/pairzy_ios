//
//  SelectLocationVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 19/07/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SelectLocationVM: NSObject {
    let SelectLocVMResponse                 = PublishSubject<Bool>()
    var addressList                         = [Location]()
    var recentSearchList                    = Helper.getOtherLocations()
    var savedAddressList                    = Helper.getRecentPassPortLoca()
    var locationManager:LocationManager?    = nil
    var locationSelected:Location?          = nil
    var dataAddress:[String : Any]          = [:]
    
    var didSelectRow = false
    
    
    /// this method having subscription to operationZoneAPI defined in LocationAPICalls
    ///
    /// - Parameter location: it is of type Location
    func getLocation(location: Location) {
       
            self.dataAddress = [
                GoogleKeys.PlaceId             : location.placeId,
                GoogleKeys.Description         : location.locationDescription,
                GoogleKeys.MainText            : location.mainText,
                GoogleKeys.AddressName         : location.name,
                GoogleKeys.AddressStreet       : location.street,
                GoogleKeys.AddressCity         : location.city,
                GoogleKeys.AddressState        : location.state,
                GoogleKeys.AddressZIP          : location.zipcode,
                GoogleKeys.AddressCountry      : location.country,
                GoogleKeys.LocationLat         : location.latitude,
                GoogleKeys.LocationLong        : location.longitude]
                self.SelectLocVMResponse.onNext(true)
         
            if self.didSelectRow == true {
                self.SelectLocVMResponse.onNext(false)
            }
          
    }
    
}
