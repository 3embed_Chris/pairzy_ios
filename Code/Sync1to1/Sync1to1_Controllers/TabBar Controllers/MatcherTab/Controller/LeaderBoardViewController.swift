//
//  LeaderBoardViewController.swift
//  Datum
//
//  Created by Rahul Sharma on 27/09/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher


class LeaderBoardViewController: UIViewController {

    // MARK:- IBOutlet
    @IBOutlet weak var leaderBoardTableView: UITableView!
    @IBOutlet weak var closeButtonOutlet: UIButton!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var cityNameLabel: UILabel!
    
    // MARK:- IBOutlet
    let matchPairVM = MatchPairVM()
    var disposeBag = DisposeBag()
    var offset = 0
    var limit = 100
    var index = 0    
    
    //    MARK:- IBACtion
    @IBAction func closeLeaderBoardControllerAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        didGetResponse()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.matchPairVM.getLeaderBoard(offset: 0, limit: 50)
        self.navigationController?.navigationBar.isHidden = false
        //Helper.blurEffect(bg:leftBackGroundImage, blurrVal: 32)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    func didGetResponse(){
        matchPairVM.matchedPairVM_resp.subscribe(onNext: { success in
            switch success.0 {
            case MatchPairVM.responseType.getLeaderBoard:
                if success.1 {
                    Helper.hideProgressIndicator()
                    if self.matchPairVM.leaderBoardArray.count > 0 {
                        DispatchQueue.main.async {
                            if  self.matchPairVM.leaderBoardArray[0].city!.count > 0{
                                UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
                                    self.cityNameLabel.text = self.matchPairVM.leaderBoardArray[0].city
                                }, completion: nil)
                                
                             }
                           self.leaderBoardTableView.reloadData()
                        }
                    }
                }else{
                    //No LeaderBoard Screen
                    UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseIn, animations: {
                            UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
                                self.cityView.isHidden = false
                                self.cityNameLabel.text = self.matchPairVM.city
                            }, completion: nil)
                            
                       
                    }, completion: nil)
                }
       
            default:
                break
            }
        }).disposed(by: disposeBag)
    }
    
   
}


// MARK: - UITableViewDataSource and UITableViewDelegateMethods

extension LeaderBoardViewController: UITableViewDataSource,UITableViewDelegate {
    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if  self.matchPairVM.leaderBoardArray.count > 0 {
             return self.matchPairVM.leaderBoardArray.count
        }else{
            return 0
        }
       
    }


     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LeaderBoardTVCell.self)) as? LeaderBoardTVCell else {fatalError()}
        let data = self.matchPairVM.leaderBoardArray[indexPath.row]
        switch indexPath.row {
            
        case 0:
            cell.setLeaderBoardData(leaderBoardModel: data)
            cell.setBadgeForProfile(badgeImageType: #imageLiteral(resourceName: "firstPrize"))
        case 1:
            cell.setLeaderBoardData(leaderBoardModel: data)
            cell.setBadgeForProfile(badgeImageType: #imageLiteral(resourceName: "secondPrize"))
        case 2:
            cell.setLeaderBoardData(leaderBoardModel: data)
            cell.setBadgeForProfile(badgeImageType: #imageLiteral(resourceName: "thirdPrize"))
       default:
            cell.setLeaderBoardData(leaderBoardModel: data)
            cell.setCountForProfile(profileCount: (indexPath.row + 1))
            
        }
        
        
        // Configure the cell...
        
        return cell
    }


 }
