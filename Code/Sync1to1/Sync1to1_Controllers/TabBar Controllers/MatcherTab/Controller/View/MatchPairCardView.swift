//
//  MatchPairCardView.swift
//  Datum
//
//  Created by Rahul Sharma on 10/10/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

class MatchPairCardView: UIView {
    
    @IBOutlet weak var leftProfileView: UIView!
    @IBOutlet weak var rightProfileView: UIView!
    @IBOutlet weak var rightProfileImageView: UIImageView!
    @IBOutlet weak var leftProfileImageView: UIImageView!
    @IBOutlet weak var leftProfileName: UILabel!
    @IBOutlet weak var leftProfileHeight: UILabel!
    @IBOutlet weak var leftProfileWork: UILabel!
    @IBOutlet weak var leftProfilePlace: UILabel!
    @IBOutlet weak var rightProfileName: UILabel!
    @IBOutlet weak var rightProfileHeight: UILabel!
    @IBOutlet weak var rightProfileWork: UILabel!
    @IBOutlet weak var rightProfilePlace: UILabel!
    @IBOutlet weak var leftWorkLabelheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightWorkLabelheightConstraint: NSLayoutConstraint!

    
    var obj:MatchPairCardView? = nil
    var matchedPairDetails:MatchPairModel? = nil
    var matchedPairProfile = [MatchPairModel]()
    var imageName = "man_default1"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpProfileView()
        
    }
    
    var shared:MatchPairCardView {
        
        if obj == nil {
            obj = UINib(nibName: "MatchPairCardView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as? MatchPairCardView
            obj?.frame = UIScreen.main.bounds
        }
        return obj!
    }
    
    func setUpProfileView(){
        self.leftProfileView.layer.cornerRadius = 6.0
        self.leftProfileView.clipsToBounds = true
        self.rightProfileView.layer.cornerRadius = 6.0
        self.rightProfileView.clipsToBounds = true
        self.rightProfileImageView.clipsToBounds = true
        self.leftProfileImageView.clipsToBounds = true
    }
    
    func updateBothProfileView(){
        if let matchedPairData = matchedPairDetails {
            let user1Age = "\(matchedPairData.user1_age ?? 0)"
            self.leftProfileName.text = matchedPairData.user1_name
            self.leftProfileName.text = matchedPairData.user1_name!.appending(", ").appending(user1Age)
            
            self.leftProfileHeight.text = matchedPairData.user1_heightInFeet
            self.leftProfileImageView.setImageOn(imageUrl: matchedPairData.user1_profilePic, defaultImage: UIImage.init(named:"02m")!)
            self.leftProfilePlace.text = matchedPairData.user1_place
            if matchedPairData.user1_worksAt == ""{
                self.leftWorkLabelheightConstraint.constant = 0
            } else{
                self.leftWorkLabelheightConstraint.constant = 21
                self.leftProfileWork.text = matchedPairData.user1_worksAt
            }
            let user2Age = "\(matchedPairData.user2_age ?? 0)"
            self.rightProfileName.text  = matchedPairData.user2_name
            self.rightProfileName.text = matchedPairData.user2_name!.appending(", ").appending(user2Age)
            self.rightProfileHeight.text = matchedPairData.user2_heightInFeet
            self.rightProfileImageView.setImageOn(imageUrl: matchedPairData.user2_profilePic, defaultImage: UIImage.init(named:"02m")!)
            self.rightProfilePlace.text = matchedPairData.user2_place
            if matchedPairData.user2_worksAt == ""{
                self.rightWorkLabelheightConstraint.constant = 0
            } else{
                self.rightWorkLabelheightConstraint.constant = 21
                self.rightProfileWork.text = matchedPairData.user2_worksAt
            }
            self.layoutIfNeeded()
        }
    }
    
    
    
}
