//
//  MatchMakerCVCell.swift
//  Datum
//
//  Created by Rahul Sharma on 04/11/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher
import Koloda

class MatchMakerCVCell: UICollectionViewCell {
    
    @IBOutlet weak var kolodaCardView: KolodaView!
    @IBOutlet weak var likeButtonView: UIView!
    @IBOutlet weak var unlikeButtonView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var seperatorView: UIView!
    
    var delegate:MatchMakerCVCellDelegate?
    var selectedIndex:IndexPath = IndexPath(row: 0, section: 0)
    var matchPairVM = MatchPairVM()
    var matchedPairProfile = [MatchPairModel]()
    var disposeBag = DisposeBag()
    var cardProfile:MatchPairModel? = nil
    var offset = 0
    var limit = 100
    var index = 0
    let overLay:ExampleOverlayView = ExampleOverlayView().shared
   
    
    override func awakeFromNib() {
        kolodaCardView.backgroundColor = UIColor.clear
        kolodaCardView.dataSource = self
        kolodaCardView.delegate = self
        setUpProfileView()
        kolodaCardView.isPairKoloda = true
        isMatchPair = true
        seperatorView.backgroundColor = Colors.SeparatorLight
    }    
    
    func updateMatchPairView(matchedProfile: MatchPairModel ){
        self.cardProfile = matchedProfile
        self.kolodaCardView.resetCurrentCardIndex()
    
    }
    
    
    
    //    MARK: - Action
    @IBAction func profileLikeAction(_ sender: UIButton) {
        //  Helper.showProgressWithoutBackg(withMessage: "")
        //        if matchPairVM.matchPairArray.count > 0{
        //            let currentPairId = matchPairVM.matchPairArray[0].pairId
        //            matchPairVM.postLikeAndUnlikeMatchPair(statusCode: 1, pairId: currentPairId!)
        //        }
        
        let selectedButton = sender as! UIButton
        likeButtonTapped(selectedButton: selectedButton)
    }
    
    
    @IBAction func profileUnlikeAction(_ sender: UIButton) {
        
        //  Helper.showProgressWithoutBackg(withMessage: "")
        //        if matchPairVM.matchPairArray.count > 0{
        //            let currentPairId = matchPairVM.matchPairArray[0].pairId
        //            matchPairVM.postLikeAndUnlikeMatchPair(statusCode: 0, pairId: currentPairId!)
        //        }
        let selectedButton = sender as! UIButton
        unlikeButtonTapped(selectedButton: selectedButton)
        
    }
    
    
    func setUpProfileView(){
        self.kolodaCardView.layer.cornerRadius = 10.0
        //self.kolodaCardView.layer.borderWidth = 0.5
       // self.kolodaCardView.layer.borderColor =  #colorLiteral(red: 0, green: 0, blue: 0.1607843137, alpha: 0.2891175176)
        self.kolodaCardView.clipsToBounds = true
        
    }
    
    
    
    func likeButtonTapped(selectedButton:UIButton) {
        kolodaCardView?.swipe(.right)
    }
    
    func unlikeButtonTapped(selectedButton:UIButton) {
        kolodaCardView?.swipe(.left)
        //checkHasProfiles(callAPi: true)
    }
    

    
    func requestLikeApi(matchedProfileDetails:MatchPairModel) {
        if matchPairVM.matchPairArray.count > 0{
            let currentPairId = matchedProfileDetails.pairId
            matchPairVM.postLikeAndUnlikeMatchPair(statusCode: 1, pairId: currentPairId!)
        }
        
    }
    
    func requestUnlikeApi(matchedProfileDetails:MatchPairModel) {
        if matchPairVM.matchPairArray.count > 0{
            let currentPairId = matchedProfileDetails.pairId
            matchPairVM.postLikeAndUnlikeMatchPair(statusCode: 0, pairId: currentPairId!)
        }
    }
    
}

protocol MatchMakerCVCellDelegate{
    func removeMatchPairProfile(indexPath: IndexPath)
    
}

// MARK: KolodaViewDelegate

extension MatchMakerCVCell: KolodaViewDelegate {
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        if(direction == .right) {
            self.requestLikeApi(matchedProfileDetails: self.cardProfile!)
            delegate?.removeMatchPairProfile(indexPath: self.selectedIndex)
            //  self.removeProfileFromCards(removeProfile: self.cardProfiles[kolodaCardView.currentCardIndex-1])
            //removeProfileFromGridView(removeProfile: self.cardProfiles[kolodaCardView.currentCardIndex-1])
        } else if(direction == .left) {
            self.self.requestUnlikeApi(matchedProfileDetails: self.cardProfile!)
            delegate?.removeMatchPairProfile(indexPath: self.selectedIndex)
            //self.removeProfileFromCards(removeProfile: self.cardProfiles[kolodaCardView.currentCardIndex-1])
            //  self.requestUnlikeApi(ProfileDetails: self.cardProfiles[kolodaCardView.currentCardIndex-1])
            //            updateUndoButton(enable:true)
            //   removeProfileFromGridView(removeProfile: self.cardProfiles[kolodaCardView.currentCardIndex-1])
        } else if(direction == .up) {
            //self.requestLikeApi(matchedProfileDetails: self.cardProfile!)
            // self.removeProfileFromCards(removeProfile: self.cardProfiles[kolodaCardView.currentCardIndex-1])
        }
        if(kolodaCardView.currentCardIndex-1>=0){
            _  = updateBothProfileView(selectedPair: self.cardProfile!)
        }
    }
    
    
    func koloda(_ koloda: KolodaView, didSelectCardAtPoint index: CGPoint) {
        
    }
    
    //    func shakeTheCardView(cardView:CardViewClass) {
    //        cardView.shakeView()
    //        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
    //    }
    //
    //    func hideTutorialView(hide:Bool,cardView:CardViewClass) {
    //        cardView.tutorialView.isHidden = hide
    //    }
}

// MARK: KolodaViewDataSource

extension MatchMakerCVCell: KolodaViewDataSource {
    
    //delegates
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
       // return self.cardProfiles.count
        return 1
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .fast
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        if cardProfile != nil{
            
             return updateBothProfileView(selectedPair: self.cardProfile!)
        }
        return UIView()
       
         //return updateBothProfileView()
    }
    
    func updateBothProfileView(selectedPair: MatchPairModel) -> UIView{
        let cardView:MatchPairCardView = MatchPairCardView().shared
        cardView.matchedPairDetails = selectedPair
        cardView.updateBothProfileView()
        return cardView
    }
    
        func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
            return Bundle.main.loadNibNamed(IDENTIFIER.OverlayView, owner: self, options: nil)?[0] as? OverlayView
        }
    
}

