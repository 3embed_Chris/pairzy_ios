//
//  MatchMakerVC.swift
//  Datum
//
//  Created by Rahul Sharma on 29/08/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher
import Koloda

class MatchMakerVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var balanceBtn: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var noPairView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var tutorialView: UIView!
    
    let profileVm = ProfileViewModel()
    var matchedPairProfile = [MatchPairModel]()
    var cardProfiles = [MatchPairModel]()
    let matchPairVM = MatchPairVM()
    var disposeBag = DisposeBag()
    var matchMakerRefreshControl: UIRefreshControl = UIRefreshControl()
    var offset = 0
    var limit = 100
    var index = 0
    var rows = 10
    
    //    MARK :- View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileVm.getCoinBalance()
        matchMakerRefreshControl.tintColor = Colors.AppBaseColor
        balanceBtn.layer.borderColor = Colors.coinBalanceBtnBorder.cgColor
        balanceBtn.layer.borderWidth = 1
        balanceBtn.layer.cornerRadius = 5
        balanceBtn.layer.cornerRadius = balanceBtn.frame.size.height/2
        NotificationCenter.default.setObserver(self, selector: #selector(userPassportedLocation), name:NSNotification.Name(rawValue: NSNotificationNames.userPassportedLocation), object: nil)
        self.matchPairVM.getMatchedPairs(offset: self.offset, limit: self.limit)
        let nameOfObserVer = NSNotification.Name(rawValue: "updateCoinsBal")
        NotificationCenter.default.setObserver(self, selector: #selector(updateCoinBalance), name: nameOfObserVer, object: nil)
        matchMakerRefreshControl.addTarget(self, action: #selector(self.refreshMatchMakerTV), for: UIControlEvents.valueChanged)
        if #available(iOS 10.0, *) {
            mainTableView.refreshControl = self.matchMakerRefreshControl
        } else {
            mainTableView.addSubview(self.matchMakerRefreshControl)
        }
        didGetResponse()
        // Do any additional setup after loading the view.
        showTutorialView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setProfileImage(profileImage: profileImageView)
        setUpProfileImageUI(profileImage: profileImageView)
        updateCoinBalance()
        self.navigationController?.navigationBar.isHidden = false
        if matchPairVM.matchPairArray.count == 0{
            self.matchPairVM.getMatchedPairs(offset: self.offset, limit: self.limit)
        }
        //Helper.blurEffect(bg:leftBackGroundImage, blurrVal: 32)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        matchMakerRefreshControl.didMoveToSuperview()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        matchMakerRefreshControl.endRefreshing()
    }
    @IBAction func viewProfileButtonAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: SegueIds.MatchmakerVCToProfileVC, sender: nil)
    }
    
    @IBAction func openLeaderBoardControllerAction(_ sender: UIButton) {
        
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:"LeaderBoardViewController") as! LeaderBoardViewController
        if let nav = self.navigationController {
            nav.present(userBasics, animated: true, completion: nil)
        }else{
            self.present(userBasics, animated: true, completion: nil)
        }
    }
    
   
    @IBAction func balanceBtnAction(_ sender: UIButton) {
        //checking coins plans details from  appstore available or not.
        if(Helper.getCoinPLansFromAppStore().count > 0) {
            let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "addCoin") as! AddCoinsVC
            userBasics.navigationController?.navigationBar.isHidden = true
            self.navigationController?.pushViewController(userBasics, animated: true)
        }
    }
    
    @IBAction func unlikeAction(_ sender: Any) {
        let selectedButton = sender as! UIButton
        requestLikeApi(matchedProfileDetails: matchPairVM.matchPairArray[selectedButton.tag])
        //self.removeMatchPairProfile(indexPath: IndexPath(row: selectedButton.tag, section: 0))
        
        self.matchPairVM.matchPairArray.remove(at:selectedButton.tag)
        let indepath = IndexPath(row: selectedButton.tag, section: 0)
        mainTableView.deleteRows(at: [indepath], with: UITableViewRowAnimation.left)
        mainTableView.reloadData()
    }
    
    
    @IBAction func likeAction(_ sender: Any) {
        let selectedButton = sender as! UIButton
        requestUnlikeApi(matchedProfileDetails: matchPairVM.matchPairArray[selectedButton.tag])
       // self.removeMatchPairProfile(indexPath: IndexPath(row: selectedButton.tag, section: 0))
        
        self.matchPairVM.matchPairArray.remove(at:selectedButton.tag)
        let indepath = IndexPath(row: selectedButton.tag, section: 0)
        mainTableView.deleteRows(at: [indepath], with: UITableViewRowAnimation.right)
        mainTableView.reloadData()
    }
    
    @objc func refreshMatchMakerTV(senderRC: UIRefreshControl) {
        self.matchPairVM.getMatchedPairs(offset: offset, limit: limit)
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            senderRC.endRefreshing()
        }
    }
    
    func showTutorialView(){
        let isNewUser = Helper.showPairTutorialView()
        if(!isNewUser) {
            Helper.updateShowPairTutorialStatus()
            tutorialView.isHidden = false
        }else{
            tutorialView.isHidden = true
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      tutorialView.isHidden = true
    }
    
    func requestLikeApi(matchedProfileDetails:MatchPairModel) {
        if matchPairVM.matchPairArray.count > 0{
            let currentPairId = matchedProfileDetails.pairId
            matchPairVM.postLikeAndUnlikeMatchPair(statusCode: 1, pairId: currentPairId!)
        }
    }
    
    func requestUnlikeApi(matchedProfileDetails:MatchPairModel) {
        if matchPairVM.matchPairArray.count > 0{
            let currentPairId = matchedProfileDetails.pairId
            matchPairVM.postLikeAndUnlikeMatchPair(statusCode: 0, pairId: currentPairId!)
        }
    }
    
    @objc func userPassportedLocation(notification:NSNotification){
        self.matchedPairProfile.removeAll()
        self.cardProfiles.removeAll()
      // self.kolodaCardView.resetCurrentCardIndex()
        self.matchPairVM.getMatchedPairs(offset: offset, limit: limit)
    }
    
    func setUpProfileImageUI(profileImage:UIImageView){
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        profileImage.layer.borderWidth = 1
        profileImage.layer.borderColor = UIColor.white.cgColor
    }
    
    func setProfileImage(profileImage:UIImageView){
        let userData = Database.shared.fetchResults()
        if let userDetails = userData.first{
            if let urlString:String = userDetails.profilePictureURL {
                if urlString.count>0 {
                    let url = URL(string: urlString)
                    var imageName = "man_default"
                    if userDetails.gender == "Male" {
                        imageName = "man_default"
                    }else {
                        imageName = "woman_default"
                    }
                    self.profileImageView.kf.setImage(with:url, placeholder:UIImage.init(named:"02m"))
                }else{
                    profileImage.image = #imageLiteral(resourceName: "Splash")
                }
            }
        }
    }
    
   @objc func updateCoinBalance(){
        var balance = " 0"
        if let getBal = UserDefaults.standard.object(forKey: "coinBalance") as? Int {
            if getBal > 1000 {
                let floatVal = Float(Float(getBal)/1000.0)
                let inter = floor(floatVal)
                let decimal = floatVal.truncatingRemainder(dividingBy: 1)
                if decimal == 0.0{
                    balance = "\(Int(inter))k"
                }else {
                    balance = "\(Float(Float(getBal)/1000.0))k"
                }
            } else {
                balance = " " + String(getBal)
            }
            balanceBtn.setTitle(balance, for: .normal)
        } else {
            balanceBtn.setTitle(" 0", for: .normal)
        }
    }

    func didGetResponse(){

        matchPairVM.matchedPairVM_resp.subscribe(onNext: { success in
            switch success.0 {
            case MatchPairVM.responseType.getMatchedPairs:
                if success.1 {
                    Helper.hideProgressIndicator()
                    if self.matchPairVM.matchPairArray.count > 0 {
                        DispatchQueue.main.async {
                            let cardView:MatchPairCardView = MatchPairCardView().shared
                            self.noPairView.isHidden = true
                          //  self.updateMatchedPairProfiles(matchedPairData: self.matchPairVM.matchPairArray)
                            self.mainTableView.isHidden = false
                            cardView.matchedPairProfile = self.matchPairVM.matchPairArray
                            self.matchMakerRefreshControl.endRefreshing()
                            self.mainTableView.reloadData()
                           // cardView.matchedPairDetails = self.matchPairVM.matchPairArray[0]
                            //cardView.updateBothProfileView()
                        }
                    }else{
                        self.matchPairVM.getMatchedPairs(offset: self.offset, limit: self.limit)
                    }
                }else{
                    let cardView:MatchPairCardView = MatchPairCardView().shared
                    self.noPairView.isHidden = false
                    self.mainTableView.isHidden = true
                }
            case MatchPairVM.responseType.likeOrUnlikeMatchedPairs:
                Helper.hideProgressIndicator()
                if success.1 {
                    let cardView:MatchPairCardView = MatchPairCardView().shared
                    if self.matchPairVM.matchPairArray.count > 0{
                      //  self.matchPairVM.matchPairArray.remove(at: 0)
                        if self.matchPairVM.matchPairArray.count > 0{
//                            DispatchQueue.main.async {
//                                cardView.matchedPairDetails = self.matchPairVM.matchPairArray[0]
//                                cardView.updateBothProfileView()
//                                //self.updateBothProfileView(selectedPair: self.matchPairVM.matchPairArray[0])
//                            }
                        }else{
                            if self.matchPairVM.totalPairs > (self.offset + self.limit){
                                self.index = self.index + 1
                                self.offset = self.index * 100
                                self.limit = (self.index+1) * 100
                                self.matchPairVM.getMatchedPairs(offset: self.offset, limit: self.limit)
                                //self.matchPairVM.getMatchedPairs(offset: (self.offset + self.limit), limit: self.limit)
                            } else{
                                let cardView:MatchPairCardView = MatchPairCardView().shared
                                self.noPairView.isHidden = false
                                self.mainTableView.isHidden = true
                            }
                        }
                    }
                } else {
                    self.matchPairVM.getMatchedPairs(offset: self.offset, limit: self.limit)
                }
            default:
                break
            }
        }).disposed(by: disposeBag)
    } 
}

extension MatchMakerVC: MatchMakerCVCellDelegate{
    func removeMatchPairProfile(indexPath: IndexPath) {
        self.matchPairVM.matchPairArray.remove(at:indexPath.row)
        mainTableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.right)
       // self.mainTableView.reloadData()  //deleteItems(at: [indexPath])
    }
}

// MARK: - UITableViewDelegate,UITableViewDataSource
extension MatchMakerVC: UITableViewDelegate,UITableViewDataSource {
        // MARK: UITableViewDataSource
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.matchPairVM.matchPairArray.count
        }
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MatchPairCell", for: indexPath) as! MatchPairCell
            if self.matchPairVM.matchPairArray.count > 0{
                cell.likeBtn.tag = indexPath.row
                cell.unlikeBtn.tag = indexPath.row
                cell.updateBothProfileView(matchedPairData:self.matchPairVM.matchPairArray[indexPath.row])
            }
            return cell
        }
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let rightAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("rightAction tapped")
            self.requestLikeApi(matchedProfileDetails: self.matchPairVM.matchPairArray[indexPath.row])
            self.matchPairVM.matchPairArray.remove(at:indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
            success(true)
        })
        rightAction.backgroundColor = Colors.likeBtnGreenColor
        rightAction.image = UIImage(named: "pairLike")
        // rightAction.title = "like"
        rightAction.image?.withRenderingMode(.alwaysOriginal)
        return UISwipeActionsConfiguration(actions: [rightAction])
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let leftAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("leftAction tapped")
            self.requestUnlikeApi(matchedProfileDetails: self.matchPairVM.matchPairArray[indexPath.row])
            self.matchPairVM.matchPairArray.remove(at:indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.right)
            success(true)
        })
        leftAction.backgroundColor = Colors.unlikeBtnGreenColor
        leftAction.image = UIImage(named: "pairUnlike")
        //  leftAction.title = "nope"
        leftAction.image?.withRenderingMode(.alwaysOriginal)
        return UISwipeActionsConfiguration(actions: [leftAction])
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return 340
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //Enable cell editing methods.
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        print("done")
    }

}


