//
//  MatchPairCell.swift
//  Datum
//
//  Created by Rahul Sharma on 19/11/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

class MatchPairCell: UITableViewCell {

    @IBOutlet weak var leftProfileView: UIView!
    @IBOutlet weak var rightProfileView: UIView!
    @IBOutlet weak var rightProfileImageView: UIImageView!
    @IBOutlet weak var leftProfileImageView: UIImageView!
    @IBOutlet weak var leftProfileName: UILabel!
    @IBOutlet weak var leftProfileHeight: UILabel!
    @IBOutlet weak var leftProfileWork: UILabel!
    @IBOutlet weak var leftProfilePlace: UILabel!
    @IBOutlet weak var rightProfileName: UILabel!
    @IBOutlet weak var rightProfileHeight: UILabel!
    @IBOutlet weak var rightProfileWork: UILabel!
    @IBOutlet weak var rightProfilePlace: UILabel!
    @IBOutlet weak var leftWorkLabelheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightWorkLabelheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewCellContainerView: UIView!
    @IBOutlet weak var unlikeBtn: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    
    
    var imageName = "man_default1"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(MatchPairCell.pangesture))
        //tableViewCellContainerView.addGestureRecognizer(gesture)
        ///gesture.delegate = self/
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateBothProfileView(matchedPairData:MatchPairModel){
      //  if let matchedPairData = matchedPairDetails {
            let user1Age = "\(matchedPairData.user1_age ?? 0)"
            self.leftProfileName.text = matchedPairData.user1_name
            self.leftProfileName.text = matchedPairData.user1_name!.appending(", ").appending(user1Age)
            
            self.leftProfileHeight.text = matchedPairData.user1_heightInFeet
            self.leftProfileImageView.setImageOn(imageUrl: matchedPairData.user1_profilePic, defaultImage: UIImage.init(named:"02m")!)
            self.leftProfilePlace.text = matchedPairData.user1_place
            if matchedPairData.user1_worksAt == ""{
                self.leftWorkLabelheightConstraint.constant = 0
            } else{
                self.leftWorkLabelheightConstraint.constant = 21
                self.leftProfileWork.text = matchedPairData.user1_worksAt
            }
            let user2Age = "\(matchedPairData.user2_age ?? 0)"
            self.rightProfileName.text  = matchedPairData.user2_name
            self.rightProfileName.text = matchedPairData.user2_name!.appending(", ").appending(user2Age)
            self.rightProfileHeight.text = matchedPairData.user2_heightInFeet
            self.rightProfileImageView.setImageOn(imageUrl: matchedPairData.user2_profilePic, defaultImage: UIImage.init(named:"02m")!)
            self.rightProfilePlace.text = matchedPairData.user2_place
            if matchedPairData.user2_worksAt == ""{
                self.rightWorkLabelheightConstraint.constant = 0
            } else{
                self.rightWorkLabelheightConstraint.constant = 21
                self.rightProfileWork.text = matchedPairData.user2_worksAt
            }
            self.layoutIfNeeded()
       // }
    }
}




