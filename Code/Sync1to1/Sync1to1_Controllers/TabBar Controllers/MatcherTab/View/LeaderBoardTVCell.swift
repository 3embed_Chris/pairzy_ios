//
//  LeaderBoardTVCell.swift
//  Datum
//
//  Created by Rahul Sharma on 27/09/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

class LeaderBoardTVCell: UITableViewCell {

    
    //    MARK:- Outlet
    @IBOutlet weak var profileImageBadge: UIImageView!
    @IBOutlet weak var profileCountLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var matchPairImageView: UIImageView!
    @IBOutlet weak var dollarImageView: UIImageView!
    @IBOutlet weak var matchPairCountLabel: UILabel!
    @IBOutlet weak var coinsAmountLabel: UILabel!
     var leaderBoardModelData: LeaderBoardProfileModel?
//    setting Up rounded Image
    
    func setUpRoundedProfileImageUI(){
       self.profileImageView.makeCornerRadius(radius: profileImageView.frame.size.width / 2)
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpRoundedProfileImageUI()
        profileCountLabel.isHidden = true
        // Initialization code
    }

    /// To set profile model data in in cell
    ///
    /// - Parameter profileModel: profileModel
    func setLeaderBoardData(leaderBoardModel: LeaderBoardProfileModel){
        self.leaderBoardModelData = leaderBoardModel
        guard let leaderBoard = leaderBoardModelData else{return}
        self.profileImageView.setImageOn(imageUrl: leaderBoard.profilePic, defaultImage: #imageLiteral(resourceName: "defaultImage"))
        self.userNameLabel.text = leaderBoard.userName
        self.coinsAmountLabel.text = String(leaderBoard.coinEarnByVote!)
        self.matchPairCountLabel.text = String(leaderBoard.userpairCount!)
      
    }
    
    func setBadgeForProfile(badgeImageType:UIImage){
        self.profileCountLabel.isHidden = true
        self.profileImageBadge.isHidden = false
        self.profileImageBadge.image = badgeImageType
    }
    
    func setCountForProfile(profileCount:Int){
        self.profileCountLabel.isHidden = false
        self.profileImageBadge.isHidden = true
       //self.profileCountLabel.text = String(profileCount)
        self.profileCountLabel.text = "\(profileCount)"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
