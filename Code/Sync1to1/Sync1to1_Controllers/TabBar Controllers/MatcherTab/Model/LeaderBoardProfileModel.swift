//
//  LeaderBoardProfileModel.swift
//  Datum
//
//  Created by Rahul Sharma on 27/09/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation

class LeaderBoardProfileModel: NSObject {
  
    var userName:String?
    var profilePic:String?
    var userpairCount:Int?
    var city:String?
    var coinEarnByVote:Int?
    var recordTotal:Int?
    
    init(leaderBoardData:[String:Any]) {
        
        if let userName = leaderBoardData["userName"] as? String {
            self.userName = userName
        }
        if let profilePic = leaderBoardData["profilePic"] as? String {
            self.profilePic = profilePic
        }
        if let userpairCount = leaderBoardData["userpairCount"] as? Int {
            self.userpairCount = userpairCount
        }
        if let city = leaderBoardData["city"] as? String {
            self.city = city
        }
        
        if let coinEarnByVote = leaderBoardData["coinEarnByVote"] as? Int {
            self.coinEarnByVote = coinEarnByVote
        }
        
        if let recordTotal = leaderBoardData["recordTotal"] as? Int {
            self.recordTotal = recordTotal
        }
 }

}
