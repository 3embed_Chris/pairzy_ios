//
//  MatchPairModel.swift
//  Datum
//
//  Created by Rahul Sharma on 11/09/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

class MatchPairModel: NSObject {
    
    var matchedPreferencesCount:Int?
    var matchedPreferencesPercentage:String?
    var pairId:String?
    var longitude:Double = 0.0
    var latitude:Double = 0.0
    
    var user1_Id:String?
    var user1_name:String?
    var user1_profilePic:String?
    var user1_height:Int?
    var user1_heightInFeet:String?
    var user1_location:[String:Double]?
    var user1_place:String?
    var user1_age:Int?
    var user1_worksAt:String?
    
    
    var user2_Id:String?
    var user2_name:String?
    var user2_profilePic:String?
    var user2_height:Int?
    var user2_heightInFeet:String?
    var user2_location:[String:Double]?
    var user2_place:String?
    var user2_age:Int?
    var user2_worksAt:String?
    
    var recordTotal:Int?
    
    
    init(matchedPairData:[String:Any]) {
        
        if let matchedPreferencesCount = matchedPairData["matchedPreferencesCount"] as? Int {
            self.matchedPreferencesCount = matchedPreferencesCount
        }
        if let matchedPreferencesPercentage = matchedPairData["matchedPreferencesPercentage"] as? String {
            self.matchedPreferencesPercentage = matchedPreferencesPercentage
        }
        if let pairId = matchedPairData["pairId"] as? String {
            self.pairId = pairId
        }
        
        if let location = matchedPairData["user1_location"] as? [String:Any]{
            if let long = matchedPairData["long"] as? Double {
                self.longitude = long
            }
            if let lat = matchedPairData["lat"] as? Double{
                self.latitude = lat
                
            }
        }
        if let location = matchedPairData["user2_location"] as? [String:Any]{
            if let long = matchedPairData["long"] as? Double {
                self.longitude = long
            }
            if let lat = matchedPairData["lat"] as? Double{
                self.latitude = lat
                
            }
        }
        
        
        if let user1_Id = matchedPairData["user1"] as? String {
            self.user1_Id = user1_Id
        }
        
        if let user1_name = matchedPairData["user1_name"] as? String {
            self.user1_name = user1_name
        }
        
        if let user1_profilePic = matchedPairData["user1_profilePic"] as? String {
            self.user1_profilePic = user1_profilePic
        }
        if let user1_height = matchedPairData["user1_height"] as? Int {
            self.user1_height = user1_height
        }
        if let user1_heightInFeet = matchedPairData["user1_heightInFeet"] as? String {
            self.user1_heightInFeet = user1_heightInFeet
        }
        
        if let user1_place = matchedPairData["user1_place"] as? String {
            self.user1_place = user1_place
        }
        if let user1_age = matchedPairData["user1_age"] as? Int {
            self.user1_age = user1_age
        }
        if let user1_worksAt = matchedPairData["user1_worksAt"] as? String {
            self.user1_worksAt = user1_worksAt
        }
        
        
        if let user2_Id = matchedPairData["user2"] as? String {
            self.user2_Id = user2_Id
        }
        
        if let user2_name = matchedPairData["user2_name"] as? String {
            self.user2_name = user2_name
        }
        
        if let user2_profilePic = matchedPairData["user2_profilePic"] as? String {
            self.user2_profilePic = user2_profilePic
        }
        if let user2_height = matchedPairData["user2_height"] as? Int {
            self.user2_height = user2_height
        }
        if let user2_heightInFeet = matchedPairData["user2_heightInFeet"] as? String {
            self.user2_heightInFeet = user2_heightInFeet
        }
        
        if let user2_place = matchedPairData["user2_place"] as? String {
            self.user2_place = user2_place
        }
        if let user2_age = matchedPairData["user2_age"] as? Int {
            self.user2_age = user2_age
        }
        if let user2_worksAt = matchedPairData["user2_worksAt"] as? String {
            self.user2_worksAt = user2_worksAt
        }
        
        if let recordTotal = matchedPairData["recordTotal"] as? Int {
            self.recordTotal = recordTotal
        }
        
    }
    
    
}


