//
//  MatchPairVM.swift
//  Datum
//
//  Created by Rahul Sharma on 11/09/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxAlamofire

class MatchPairVM: NSObject {
    
    
    let disposeBag = DisposeBag()
    let matchedPairVM_resp = PublishSubject<(responseType, Bool)>()
    var latitude = 0.00
    var longitude = 0.00
    var userId:String = ""
    var message = ""
    var city = " "
    var totalPairs = 0
    let userData = Database.shared.fetchResults()
    var matchPairArray = [MatchPairModel]()
    var leaderBoardArray = [LeaderBoardProfileModel]()
    
    enum responseType:Int {
        case  getMatchedPairs = 0
        case  likeOrUnlikeMatchedPairs = 1
        case  getLeaderBoard = 2
    }
    
    func getUserId() {
        
        let userData = Database.shared.fetchResults()
        if !userData.isEmpty
        {
            if let userDetails = userData.first {
                self.userId = userDetails.userId!
            }
        }
    }
    
    func getMatchedPairs(offset:Int, limit:Int){
        self.getUserId()
        let matchPairAPICall = MatchPairsAPICalls()
        if !matchPairAPICall.subject_response.hasObservers {
            matchPairAPICall.subject_response
                .subscribe(onNext: {response in
                    Helper.hideProgressIndicator()
                    self.handleMatchedPairResponse(response: response)
                }, onError: {error in
                    Helper.hideProgressIndicator()
                    self.message = error.localizedDescription
                    self.matchedPairVM_resp.onError(error)
                })
                .disposed(by:disposeBag)
        }
        DispatchQueue.global(qos: .background).async{
            matchPairAPICall.requestForMatchedPairs(offset:offset , limit:limit)
        }
        
        
        
    }
    
    /// handleUserBioPostResponse
    ///
    /// - Parameter response: response
    func handleMatchedPairResponse(response: ResponseModel){
        guard let statusCode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode) else {
            return
        }
        if statusCode == API.ErrorCode.Success {
            
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            if let totalPairs = response.response["recordTotal"] as? Int  {
                self.totalPairs = totalPairs
            }
            if let matchedPairResponse = response.response["data"] as? [[String:Any]] {
                self.matchPairArray = matchedPairResponse.map({ (data) -> MatchPairModel in
                    MatchPairModel(matchedPairData: data)
                    
                })
            }
            print(matchPairArray.count)
            self.matchedPairVM_resp.onNext((.getMatchedPairs,true))
        } else {
            Helper.hideProgressIndicator()
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            if let totalPairs = response.response["recordTotal"] as? Int {
                self.totalPairs = totalPairs
            }
            self.matchedPairVM_resp.onNext((.getMatchedPairs,false))
        }
    }
    
    
    
    func postLikeAndUnlikeMatchPair(statusCode: Int,pairId: String) {
      //  Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        let params = [ServiceInfo.pairId: pairId,
                      ServiceInfo.statusCode: statusCode] as [String : Any]
        
        let matchPairAPICall = MatchPairsAPICalls()
        if !matchPairAPICall.subject_response.hasObservers {
            matchPairAPICall.subject_response
                .subscribe(onNext: {response in
                    Helper.hideProgressIndicator()
                    self.handleLikeAndUnlikeMatchPairResponse(response: response)
                }, onError: {error in
                    Helper.hideProgressIndicator()
                    self.message = error.localizedDescription
                    self.matchedPairVM_resp.onError(error)
                })
                .disposed(by:disposeBag)
        }
        matchPairAPICall.requestlikeAndUnlikePostMatchedPairsResponse(data: params)
    }
    
    /// handleUserBioPostResponse
    ///
    /// - Parameter response: response
    func handleLikeAndUnlikeMatchPairResponse(response: ResponseModel){
        guard let statusCode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode) else {
            return
        }
        if statusCode == API.ErrorCode.Success {
            Helper.hideProgressIndicator()
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            if let totalPairs = response.response["recordTotal"] as? Int {
                self.totalPairs = totalPairs
            }
            self.matchedPairVM_resp.onNext((.likeOrUnlikeMatchedPairs,true))
        } else {
            Helper.hideProgressIndicator()
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            if let totalPairs = response.response["recordTotal"] as? Int {
                self.totalPairs = totalPairs
            }
            self.matchedPairVM_resp.onNext((.likeOrUnlikeMatchedPairs,false))
        }
    }
    
    
    
    
    
    
    
    func getLeaderBoard(offset:Int, limit:Int){
        let matchPairAPICall = MatchPairsAPICalls()
        if !matchPairAPICall.subject_response.hasObservers {
            matchPairAPICall.subject_response
                .subscribe(onNext: {response in
                    Helper.hideProgressIndicator()
                    self.handleGetLeaderBoardResopnse(response: response)
                }, onError: {error in
                    Helper.hideProgressIndicator()
                    self.message = error.localizedDescription
                    self.matchedPairVM_resp.onError(error)
                })
                .disposed(by:disposeBag)
        }
        DispatchQueue.global(qos: .background).async{
            matchPairAPICall.requestForGetLeaderboard(offset:offset , limit:limit)
        }
        
        
        
    }
    
    /// handleUserBioPostResponse
    ///
    /// - Parameter response: response
    func handleGetLeaderBoardResopnse(response: ResponseModel){
        guard let statusCode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode) else {
            return
        }
        if statusCode == API.ErrorCode.Success {
            
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            
            //            var matchedPairData = [MatchPairModel]()
            //            if let matchedPairResponse = response.response["data"] as? [AnyObject] {
            //                for (index, _) in matchedPairResponse.enumerated() {
            //                    let matchedPairProfile = matchedPairResponse[index] as! [String:Any]
            //                    matchedPairData.append(matchedPairProfile)
            //                }
            //            }
            //            self.matchPairArray = matchedPairData
            
            if let leaderBoardDataResponse = response.response["data"] as? [[String:Any]] {
                self.leaderBoardArray = leaderBoardDataResponse.map({ (data) -> LeaderBoardProfileModel in
                    LeaderBoardProfileModel(leaderBoardData: data)
                    
                })
            }
            print(leaderBoardArray.count)
            self.matchedPairVM_resp.onNext((.getLeaderBoard,true))
        } else {
            Helper.hideProgressIndicator()
            if let msg = response.response["message"] as? String {
                self.message = msg
            }
            if let city = response.response["city"] as? String {
                self.city = city
            }
            if let totalPairs = response.response["recordTotal"] as? Int {
                self.totalPairs = totalPairs
            }
            self.matchedPairVM_resp.onNext((.getLeaderBoard,false))
        }
    }
    
    
}
