//
//  MatchPrefernceViewModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 23/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxAlamofire

class MatchPrefernceViewModel: NSObject {
        
    let disposeBag = DisposeBag()
    let subject_response = PublishSubject<[MatchPrefernceModel]>()
    
    
    func getMatchPrefernces() {
        
        //showing progress indicator on window.
        
        let apiCall = MatchPreferncesAPICalls()
        
        apiCall.requestGetMatchPrefernces()
        
        apiCall.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                self.handleGetPreferncesApiResponse(responseModel: response)
            }, onError: {error in
                Helper.hideProgressIndicator()
                Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
            })
            .disposed(by:disposeBag)
    }
    
    
    func handleGetPreferncesApiResponse(responseModel:ResponseModel) {
        
        let statuscode = responseModel.statusCode
        switch (statuscode) {
        case 200:
            var prefModelData = [MatchPrefernceModel]()
            
            //saving prefrences in userdefaults.
            let dictForSave =  responseModel.response
            UserDefaults.standard.setValue(dictForSave, forKey:"userPrefrences/")
            UserDefaults.standard.synchronize()
            
            if let dataDict: NSArray = responseModel.response["data"] as? NSArray {
                if let preferDict = dataDict[0] as? [String:Any] {
                    if let preferArray:NSArray = preferDict["searchPreferences"] as? NSArray {
                        for (index, _) in preferArray.enumerated() {
                            let prefDetails = preferArray[index] as! [String:Any]
                            prefModelData.append(MatchPrefernceModel.init(prferDetails: prefDetails))
                        }
                    }
                }
            }
        self.subject_response.onNext(prefModelData)
        default: break
        }
    }
}
