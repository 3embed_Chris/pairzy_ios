//
//  PrefernceModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 29/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class MatchPrefernceModel: NSObject {
    
    var prefernceId:String = ""
    var prefernceType:Int = 0
    var prefernceTitle:String = ""
    var prefernceIcon:String = ""
    var optionsForMenu:[String] = []
    var selectedValuesForMenu:[String] = []
    
    var optionalValues:[Int] = []
    var selectedValues:[Int] = []
    var selectedUnit:String = ""
    var optinalUnits:[String] = []
    
    var multyFactor:[Int] = []
    
    init(prferDetails:[String:Any]) {
 
        if let prefId = prferDetails["_id"] as? String {
            self.prefernceId = prefId
        }
        
        if let stateCode = prferDetails["TypeOfPreference"] as? String {
            if let stateCodeInt = Int(stateCode){
                self.prefernceType = stateCodeInt
            }
        }
        if let stateCodeInt = prferDetails["TypeOfPreference"] as? Int {
                self.prefernceType = stateCodeInt
        }
        
        if let prefTitle = prferDetails["PreferenceTitle"] as? String {
            self.prefernceTitle = prefTitle
        }
        
        if let prefIcon = prferDetails["icon"] as? String {
            self.prefernceIcon = prefIcon
        }
        
        if let Options:[String] = prferDetails["OptionsValue"] as? [String] {
            self.optionsForMenu  = Options
            if Int(Options[0]) != nil {
                self.optionalValues  = Options.map { Int($0)!}
            }
        }
        
        if let Options:[Int] = prferDetails["OptionsValue"] as? [Int] {
            self.optionalValues  = Options
        }
        if let slectedValue:[String] = prferDetails["selectedValue"] as? [String] {
            self.selectedValuesForMenu  = slectedValue
            if slectedValue.count != 0 {
                
                if Int(slectedValue[0]) != nil {
                    self.selectedValues  = slectedValue.map { Int($0)!}
                }
                
            }
            
        }
        
        if let slectedValues:[Int] = prferDetails["selectedValue"] as? [Int] {
            self.selectedValues  = slectedValues
        }
        
        if let prefIcon = prferDetails["selectedUnit"] as? String {
            self.selectedUnit = prefIcon
        }
        
        if let OptionalUnits:[String] = prferDetails["optionsUnits"] as? [String] {
            self.optinalUnits  = OptionalUnits
        }
    }
}
