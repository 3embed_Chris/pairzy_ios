//
//  DropDownTableViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 08/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class DropDownTableViewCell: UITableViewCell {
    
//    @IBOutlet weak var iconImage: UIImageView!
    
    @IBOutlet weak var preferenceTitle: UILabel!
    
    @IBOutlet weak var preferenceValue: UILabel!
    
    @IBOutlet weak var dropDownbutton: UIButton!
    
    var selectedArrv:[String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

