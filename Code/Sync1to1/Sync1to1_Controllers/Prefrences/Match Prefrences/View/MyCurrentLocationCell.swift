//
//  MyCurrentLocationCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 28/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class MyCurrentLocationCell: UITableViewCell {
    
    @IBOutlet weak var myCurrentLocLabel: UILabel!
    @IBOutlet weak var currentAddress: UILabel!
    @IBOutlet weak var tickSelected: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func updateCell(){

        if Helper.getIsFeatureExist() {
            let location = Helper.getFeatureLocation()
           myCurrentLocLabel.text = ""
            let address = location.city + ", " + location.state + ", " + location.country
//            if location.city.count == 0 {
//                address = location.state + ", " + location.country
//            }
            if address.count > 9 {
                currentAddress.text = address
            }else if location.locationDescription.count != 0 {
                currentAddress.text = location.locationDescription
            }else{
                currentAddress.text = location.secondaryText
            }
            
            if currentAddress.text?.count ?? 0 < 9 {
                self.showCurrentAddress()
            }
        }else{
            self.showCurrentAddress()
        }
    }
    
    
    func showCurrentAddress(){
        myCurrentLocLabel.text = StringConstants.myCurrentLocation()
        let location = Utility.getCurrentAddress()
        currentAddress.text = location.city + ", " + location.state + ", " + location.country
    }
    
    
    func updateBuyCoins(){
        
        myCurrentLocLabel.text = StringConstants.BuyCoins()
        currentAddress.text = StringConstants.coinsUnlockFeatures()
    }
    
    func updateRecentPassLoc(location: Location) {
        myCurrentLocLabel.text = ""
        var address = location.city + ", " + location.state + ", " + location.country
        if location.city.count < 2 {
            address = location.state + ", " + location.country
        }
        currentAddress.text = location.fullText
        
        if address.count > 9 {
            myCurrentLocLabel.text = address
        }else if location.locationDescription.count != 0 {
            myCurrentLocLabel.text = location.locationDescription
            
        }else{
             myCurrentLocLabel.text = location.secondaryText
        }

        if location.fullText.count < 10 {
            currentAddress.text = location.locationDescription
        }
  
    }
    
    func updateCurrentLocation(){
        myCurrentLocLabel.text = StringConstants.myCurrentLocation()
        var location = Utility.getCurrentAddress()
        currentAddress.text = location.city + ", " + location.state + ", " + location.country
        
        var address = location.city + ", " + location.state + ", " + location.country
        
        if address.count > 9 {
            currentAddress.text = address
        }else if location.locationDescription.count != 0 {
            currentAddress.text = location.locationDescription
        }else{
            currentAddress.text = location.secondaryText
        }
        
    }
    
}
