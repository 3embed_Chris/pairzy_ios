//
//  RangeSliderTableViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 08/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MARKRangeSlider

class RangeSliderTableViewCell: UITableViewCell {

//    @IBOutlet weak var iconImage: UIImageView!
    
    @IBOutlet weak var prefrenceTypeTitle: UILabel!
    

    @IBOutlet weak var segmentWidthConstarint: NSLayoutConstraint!
    
    @IBOutlet weak var preferenceValueLabel: UILabel!
    
    
    @IBOutlet weak var segementControlOutlet: UISegmentedControl!
    
    @IBOutlet weak var rangeSliderOutlet: MARKRangeSlider!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.rangeSliderOutlet.disableOverlapping = true
        self.segementControlOutlet.tintColor = Colors.AppBaseColor
       
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
