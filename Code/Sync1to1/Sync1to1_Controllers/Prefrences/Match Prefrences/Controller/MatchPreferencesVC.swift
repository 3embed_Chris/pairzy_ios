//
//  MatchPreferencesVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 26/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MARKRangeSlider
import RxSwift
import RxAlamofire

protocol preferncesUpdated: class {
    func prefrenceUpdateCompleted()
}

class MatchPreferencesVC: UIViewController{
    
    struct Constants {
        static let Inch_In_CM:Float = 2.54
        static let numberOfInchesInFeet:Float = 12
        static let Miles_In_Km :CGFloat = 0.6214
        static let Km_In_Miles :CGFloat = 1.6094
        static let lowestCMValue:CGFloat = 120
        static let highestCMValue : CGFloat = 310
        static let lowestMilesDistanceValue:CGFloat = 0
        static let highestMilesDistanceValue:CGFloat = 1000
        static let highestKMDistanceValue:CGFloat = 1600
        static let lowestAgeValue:Int = 18
        static let highestAgeValue:Int = 60
        static let minimumDistanceBetweenTwoPointsInSlider:CGFloat = 2
        static let feetValue = StringConstants.feet()
        static let cmValue = StringConstants.cm()
        static let kmValue = StringConstants.km()
        static let milesValue = StringConstants.miles()
        static let cancelButtonTitle = StringConstants.cancel()
        static let toNotifications = "prefrencesToNotification"
        static let genderID = "5d6384b9b80db667866938f3"
        static let ageID = "5d638428b80db667866938f1"
        static let heightID = "5d638500b80db667866938f4"
        static let distanceID = "5d63845ab80db667866938f2"
    }
    
    let disposebag = DisposeBag()
    weak var delegate: preferncesUpdated?
    
    let preferncesVm = MatchPrefernceViewModel()
    
    @IBOutlet weak var prefrenceTableView: UITableView!
    @IBOutlet weak var preferredGenderOutlet: UILabel!
    @IBOutlet weak var prefrencesView: UIView!
    @IBOutlet weak var ageSliderOutlet: MARKRangeSlider!
    @IBOutlet weak var ageValueLabelOutlet: UILabel!
    @IBOutlet weak var heightSliderOutlet: MARKRangeSlider!
    @IBOutlet weak var heightValueLabelOutlet: UILabel!
    @IBOutlet weak var heightSwitchSegmentControlOutlet: UISegmentedControl!
    @IBOutlet weak var distanceValueLabelOutlet: UILabel!
    @IBOutlet weak var distanceSwitchSegmentControlOutlet: UISegmentedControl!
    @IBOutlet weak var distanceSliderOutlet: MARKRangeSlider!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleHeader: UIView!
    @IBOutlet weak var topBackBtn: UIButton!
    @IBOutlet weak var screenTitle: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    @IBOutlet weak var checkBoxTableView: UITableView!
    @IBOutlet weak var checkBoxTableHeight: NSLayoutConstraint!
    @IBOutlet weak var optionsView: UIView!
    
    var trackPrefrenceChanges = false
    var isComingFromSignUp = false
    var userData : User!
    var preferenceArray = [MatchPrefernceModel]()
    var selectedAdreess:Location? = nil
    
    var selectedOptions:[String] = []
    var selctedOption = ""
    var selectedIndex:IndexPath = IndexPath(row: -1, section: -1)
    var selectedOptionValueIndx = IndexPath(row: -1, section: -1)
    //MARK:- VIEW DELEGATES.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        getPrefrencesFromDB()
        //  getPrefrencesFromUserDefault()
        self.getDefaultPrefrences()
        screenTitle.textColor = Colors.PrimaryText
        subTitle.textColor = Colors.AppBaseColor
        screenTitle.text = StringConstants.my()
        subTitle.text = StringConstants.preferences()
        initialSetup()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // prefrenceTableView.reloadData()
        self.titleLabel.textColor = UIColor.clear
        titleHeader.backgroundColor = UIColor.clear
        self.topBackBtn.isHidden = true
        titleHeader.isHidden = true
        setTableHight(optionsCount: 0)
    }
    
    func updatePrefrencesInUserDefaults() {
        
    }
    
    
    func getPrefrencesFromUserDefault() {
        if(isPrefrencesAvailable()) {
            showUserPrefrences()
        } else {
            Helper.showProgressIndicator(withMessage: StringConstants.GettiingPrefrences())
        }
    }
    
    //  performSegue(withIdentifier: "toPassport", sender: self)
    
    func showUserPrefrences() {
        
        var prefModelData = [MatchPrefernceModel]()
        
        let savedDict:[String:Any] = UserDefaults.standard.value(forKey: "userPrefrences/") as! [String : Any]
        
        let ResponseMod = ResponseModel.init(statusCode: 200, response:savedDict)
        
        if let dataDict: NSArray = ResponseMod.response["data"] as? NSArray {
            if let preferDict = dataDict[0] as? [String:Any] {
                if let preferArray:NSArray = preferDict["searchPreferences"] as? NSArray {
                    for (index, _) in preferArray.enumerated() {
                        let prefDetails = preferArray[index] as! [String:Any]
                        prefModelData.append(MatchPrefernceModel.init(prferDetails: prefDetails))
                    }
                }
            }
        }
        
        self.preferenceArray = prefModelData
        self.prefrencesView.isHidden = false
        self.prefrenceTableView.reloadData()
    }
    
    
    /// method to check document availble or not.
    ///
    /// - Returns: true if doc avaialble otherwise false.
    func isPrefrencesAvailable() -> Bool {
        let matchesDocId = "userPrefrences/"
        if Helper.isKeyPresentInUserDefaults(key: matchesDocId) {
            return true
        }
        return false
    }
    
    
    //MARK:- CLASS METHODS.
    
    /// METHOD FOR UPDATING TEXT WHEN USER CHANGES SLIDER
    ///
    /// - Parameter reloadAtIndex: is the indexpath for updating text.
    func updatePrefrenceValue(reloadAtIndex:IndexPath) {
        let rangeSliderCell: RangeSliderTableViewCell = self.prefrenceTableView.cellForRow(at: reloadAtIndex) as! RangeSliderTableViewCell
        
        let matchPref = preferenceArray[reloadAtIndex.row]
        
        if( matchPref.prefernceId == Constants.heightID) {
            rangeSliderCell.rangeSliderOutlet.disableOverlapping = true
            let selectedValueMinimum = CGFloat(rangeSliderCell.rangeSliderOutlet.leftValue)
            let selectedValueMaximum = CGFloat(rangeSliderCell.rangeSliderOutlet.rightValue)
            
            if(rangeSliderCell.segementControlOutlet.selectedSegmentIndex == 1) {
                rangeSliderCell.preferenceValueLabel.text = "\(Int(selectedValueMinimum))" + StringConstants.cm() + " - \(Int(selectedValueMaximum))" + StringConstants.cm()
                
            } else {
                let minFeetValue = self.getFeetAndInchesFrom(centimeters: Float(selectedValueMinimum))
                let maxFeetValue = self.getFeetAndInchesFrom(centimeters: Float(selectedValueMaximum))
                rangeSliderCell.preferenceValueLabel.text = "\(minFeetValue) - \(maxFeetValue)"
            }
            
        } else if ( matchPref.prefernceId == Constants.ageID) {
            rangeSliderCell.rangeSliderOutlet.disableOverlapping = true
            if(rangeSliderCell.rangeSliderOutlet.rightValue == rangeSliderCell.rangeSliderOutlet.maximumValue) {
                rangeSliderCell.preferenceValueLabel.text = "\(Int(rangeSliderCell.rangeSliderOutlet.leftValue))" + "-" + "\(Int(rangeSliderCell.rangeSliderOutlet.rightValue))" + "+"
                
            } else {
                
                rangeSliderCell.preferenceValueLabel.text = "\(Int(rangeSliderCell.rangeSliderOutlet.leftValue))" + "-" + "\(Int(rangeSliderCell.rangeSliderOutlet.rightValue))"
            }
        }
        
        if ( matchPref.prefernceId == Constants.distanceID) {
            
            rangeSliderCell.rangeSliderOutlet.disableOverlapping = false
            if rangeSliderCell.segementControlOutlet.selectedSegmentIndex == 0 {
                rangeSliderCell.preferenceValueLabel.text = "\(Int(rangeSliderCell.rangeSliderOutlet.leftValue))" + StringConstants.mi() + " - \(Int(rangeSliderCell.rangeSliderOutlet.rightValue))" + StringConstants.mi()
            }else{
                 rangeSliderCell.preferenceValueLabel.text = "\(Int(rangeSliderCell.rangeSliderOutlet.leftValue))" + StringConstants.km() + " - \(Int(rangeSliderCell.rangeSliderOutlet.rightValue))" + StringConstants.km()
                
            }
        }
    }
    
    /// open gender prefernces.
    ///
    /// - Parameter selectedIndex: is indexpath for opening options.
    func openGenderPreferences(typeId: Int){
        
        if  let droDownCell: DropDownTableViewCell = self.prefrenceTableView.cellForRow(at: selectedIndex) as? DropDownTableViewCell {
            
            let prefrenceDict = preferenceArray[selectedIndex.row]
            var selctedValues = prefrenceDict.optionsForMenu
                if typeId == 2{
                    if selectedOptions.count == 0 {
                        selectedOptions = prefrenceDict.selectedValuesForMenu
                    }else{
                        droDownCell.selectedArrv = selectedOptions
                    }
                    if selectedOptions.count > 1{
                        droDownCell.preferenceValue.text = selectedOptions[0] + " + \(selectedOptions.count - 1)"
                    }else if selectedOptions.count == 1 {
                       droDownCell.preferenceValue.text = selectedOptions[0]
                    }else{
                        droDownCell.preferenceValue.text = "NA"
                    }
                    
                }else{
                    
                    droDownCell.preferenceValue.text = selctedOption

                }
            setTableHight(optionsCount: 0)
        }
        
    }
    
    @IBAction func checkBoxAction(_ sender: Any) {
        
        let checkBox = sender as! UIButton
        let checkBoxIndexPath =  IndexPath(row: checkBox.tag, section:0)
        if  let droDownCell: CheckBoxTableViewCell = self.prefrenceTableView.cellForRow(at: checkBoxIndexPath) as? CheckBoxTableViewCell {
            droDownCell.checkBoxButton.setImage(#imageLiteral(resourceName: "check_on"), for: .normal)
        }

    }
    
  
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// button for dismiss
    ///
    /// - Parameter sender: is close button.
    @IBAction func closeButtonAction(_ sender: Any)
    {
        // if it is from signup opening home otherwise dismissing.
        
        if self.isComingFromSignUp {
            redirectToHomeVc()
        } else {
            // showAlertToBuyCoins()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    /// action for gender button
    ///
    /// - Parameter sender: gender button
    @IBAction func genderPickerButtonAction(_ sender: Any)
    {
        let dropDownButton = sender as! UIButton
        let buttonIndexPath =  IndexPath(row: dropDownButton.tag, section:1)
        selectedIndex = buttonIndexPath
        
        
        let prefrenceDict = preferenceArray[buttonIndexPath.row]
        let selctedValues = prefrenceDict.optionsForMenu
        checkBoxTableView.reloadData()
        setTableHight(optionsCount: selctedValues.count)
        
        
    }
    
    
    /// done button action.
    ///
    /// - Parameter sender: done button.
    @IBAction func doneButtonAction(_ sender: Any) {
        let doneButton = sender as! UIButton
        doneButton.addZoomEffect()
        
        //if there is no changes in prefrences then dismising Vc directly without calling API.
        if(!trackPrefrenceChanges && !self.isComingFromSignUp){
            self.dismiss(animated: true, completion: nil)
            return
        }
        self.postUserPreferences()
    }
    
    func storeupdatedPreferences() {
        
    }
    
    
    func showUserPrefrences(gender:String ,age:String,Height:String,distance:String) {
        
        
        
    }
    
    
    func showAlertToBuyCoins() {
        
        let alertmsg = StringConstants.doYouWantToUpdate()
        let alertTitle = StringConstants.updateChanges()
        
        let alert:UIAlertController=UIAlertController(title:alertTitle, message: alertmsg, preferredStyle: UIAlertControllerStyle.alert)
        
        let cameraAction = UIAlertAction(title: StringConstants.yes(), style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.doneButtonAction(UIButton())
            
        }
        
        let cancelAction = UIAlertAction(title: StringConstants.no(), style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.dismiss(animated: true, completion: nil)
        }
        
        
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    /// method for requesting get preferences api.
    func getDefaultPrefrences() {
        
        preferncesVm.getMatchPrefernces()
        preferncesVm.subject_response
            .subscribe(onNext: { prefModelData in
                self.preferenceArray = prefModelData
                self.prefrencesView.isHidden = false
                self.prefrenceTableView.reloadData()
                
            }, onError: {error in
                
            })
            .disposed(by:disposebag)
        
    }
    
    func getPrefrencesFromDB() {
        if(checkMatchesDocument()) {
            
            
            
        } else {
            
        }
    }
    
    /// method to check document availble or not.
    ///
    /// - Returns: true if doc avaialble otherwise false.
    func checkMatchesDocument() -> Bool {
        let matchesDocId = "userPrefData/"
        if Helper.isKeyPresentInUserDefaults(key: matchesDocId) {
            return true
        }
        return false
    }
    
    
    
    /// method for requesting post preferences api.
    func postUserPreferences() {
        
        Helper.showProgressIndicator(withMessage: StringConstants.UpdatingPrefrences())
        
        var selectedValues : [AnyObject] = [AnyObject]()
        
        var arrayForNewPref:[AnyObject] = [AnyObject]()
        var isPassportUser = false
        
        for (index, _) in preferenceArray.enumerated() {
            
            let prefrenceDict = preferenceArray[index]
            
            switch prefrenceDict.prefernceType {
            case 1,2:
                
                let dropDownIndexpath =  IndexPath(row: index, section:1)
                
                
                if let dropDownCell: DropDownTableViewCell = self.prefrenceTableView.cellForRow(at: dropDownIndexpath) as? DropDownTableViewCell {
                    
                    let locDetails:[String:Any?] = Helper.getUserLocation()
                    var latitude = 0.0
                    var longitude = 0.0
                    let hasValue:String = locDetails["hasLocation"] as! String
                    
                    if Helper.getIsFeatureExist(){
                        
                        let featureLoc:Location =  Helper.getFeatureLocation()
                        latitude = featureLoc.latitude
                        longitude = featureLoc.longitude
                        isPassportUser = true
                        
                    }else {
                        
                        if(hasValue == "1") {
                            if let lat = locDetails["lat"] as? Double {
                                latitude = lat
                            }
                            if let long = locDetails["long"] as? Double{
                                longitude = long
                            }
                        }
                        isPassportUser = false
                    }
                    
                    var tempGender:[String] = []
                    if prefrenceDict.prefernceType == 2{
                        if dropDownCell.selectedArrv.count == 0 {
                            tempGender = prefrenceDict.selectedValuesForMenu
                        }else{
                            tempGender = dropDownCell.selectedArrv
                        }
                        
                    }else {
                    if dropDownCell.preferenceValue.text == StringConstants.male(){
                        tempGender.append("Male")
                    }else if dropDownCell.preferenceValue.text == StringConstants.female() {
                        tempGender.append("Female")
                        
                    }else if dropDownCell.preferenceValue.text == StringConstants.female(){
                        tempGender.append("Transgender")
                    }else {
                        tempGender.append(dropDownCell.preferenceValue.text ?? "")
                    }
                }
                    let selctedDic  = [ "pref_id":prefrenceDict.prefernceId,
                                        "selectedValues": tempGender as Any,
                                        ServiceInfo.latitude: latitude,
                                        ServiceInfo.longitude: longitude
                        ] as [String : Any]
                    
                    
                    if(isPrefrencesAvailable()) {
                        let savedDict:[String:Any] = UserDefaults.standard.value(forKey: "userPrefrences/") as! [String : Any]
                        if let data: NSArray = savedDict["data"] as? NSArray {
                            if let tempArray:[String:Any] = data[0] as? [String:Any] {
                                if let preferArray:NSArray = tempArray["searchPreferences"] as? NSArray {
                                    for (index, _) in preferArray.enumerated() {
                                        var prefDetails = preferArray[index] as! [String:Any]
                                        if(prefDetails["_id"] as? String == prefrenceDict.prefernceId) {
                                            let arrayForSelectedValues = [dropDownCell.preferenceValue.text]
                                            prefDetails["selectedValues"] = arrayForSelectedValues
                                            arrayForNewPref.append(prefDetails as AnyObject)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    selectedValues.append(selctedDic as AnyObject)
                }
           
                
            default:
                
                let sliderIndexPath =  IndexPath(row: index, section:1)
                
                let rangeSliderCell: RangeSliderTableViewCell = self.prefrenceTableView.cellForRow(at: sliderIndexPath) as! RangeSliderTableViewCell
                
                var selectedUnit = ""
                if prefrenceDict.optinalUnits.count == 1 {
                    selectedUnit = prefrenceDict.selectedUnit
                } else {
                    selectedUnit = prefrenceDict.optinalUnits[rangeSliderCell.segementControlOutlet.selectedSegmentIndex]
                }
                
                var selctedDic:[String : Any] = [:]
                if selectedUnit == StringConstants.mi() {
                    
                    let selectedValueMinimum = self.convertMilesToKilometers(speedInMPH: CGFloat(rangeSliderCell.rangeSliderOutlet.leftValue))
                    let selectedValueMaximum = self.convertMilesToKilometers(speedInMPH: CGFloat(rangeSliderCell.rangeSliderOutlet.rightValue))
                    
                    
                    selctedDic  = [ "pref_id":prefrenceDict.prefernceId,
                                    "selectedUnit":selectedUnit,
                                    "selectedValues": [Int(selectedValueMinimum),Int(selectedValueMaximum)]] as [String : Any]
                    
                }else{
                    
                    selctedDic  = [ "pref_id":prefrenceDict.prefernceId,
                                    "selectedUnit":selectedUnit,
                                    "selectedValues": [Int(rangeSliderCell.rangeSliderOutlet.leftValue),Int(rangeSliderCell.rangeSliderOutlet.rightValue)]
                        ] as [String : Any]
                    
                }
                
                
                
                if(isPrefrencesAvailable()) {
                    let savedDict:[String:Any] = UserDefaults.standard.value(forKey: "userPrefrences/") as! [String : Any]
                    if let data: NSArray = savedDict["data"] as? NSArray {
                        if let tempArray:[String:Any] = data[0] as? [String:Any] {
                            if let preferArray:NSArray = tempArray["searchPreferences"] as? NSArray {
                                for (index, _) in preferArray.enumerated() {
                                    var prefDetails = preferArray[index] as! [String:Any]
                                    if(prefDetails["_id"] as? String == prefrenceDict.prefernceId) {
                                        let arrayForSelectedValues = [Int(rangeSliderCell.rangeSliderOutlet.leftValue),Int(rangeSliderCell.rangeSliderOutlet.rightValue)]
                                        prefDetails["selectedValues"] = arrayForSelectedValues
                                        prefDetails["selectedUnit"] = selectedUnit
                                        arrayForNewPref.append(prefDetails as AnyObject)
                                    }
                                }
                            }
                        }
                    }
                }
                selectedValues.append(selctedDic as AnyObject as AnyObject)
            }
        }
        
        let locDetails:[String:Any?] = Helper.getUserLocation()
        var latitude = 0.0
        var longitude = 0.0
        var featAddress = ""
        let hasValue:String = locDetails["hasLocation"] as! String
        
        if Helper.getIsFeatureExist(){
            
            
            let featureLoc:Location =  Helper.getFeatureLocation()
            featAddress =  featureLoc.city + ", " + featureLoc.state + ", " + featureLoc.country
            latitude = featureLoc.latitude
            longitude = featureLoc.longitude
            if featAddress.count < 9 {
                featAddress = featureLoc.locationDescription
                if featAddress.count < 8 {
                    featAddress = featureLoc.secondaryText
                }
            }
            
            isPassportUser = true
            
        }else {
            
            if(hasValue == "1") {
                if let lat = locDetails["lat"] as? Double {
                    latitude = lat
                }
                if let long = locDetails["long"] as? Double {
                    longitude = long
                }
            }
            let location = Utility.getCurrentAddress()
            featAddress = location.city + ", " + location.state + ", " + location.country
            let deviceLocation = Helper.getDeviceLocation()
            if featAddress.count  < 8 {
                featAddress = deviceLocation.city + ", " + deviceLocation.state + ", " + deviceLocation.country
            }
            isPassportUser = false
        }
        
        
        
        //saving new data to userdefaults.
        //saving prefrences in userdefaults.
        
        let newPrefDict:[String:Any] = ["searchPreferences":arrayForNewPref]
        let saveArray:[Any] = [newPrefDict]
        let dictForSave:[String:Any] = ["data":saveArray]
        UserDefaults.standard.setValue(dictForSave, forKey:"userPrefrences/")
        UserDefaults.standard.synchronize()
        
        let params = ["preferences":selectedValues,
                      ServiceInfo.isPassportLocation : isPassportUser,
                      ServiceInfo.latitude: latitude,
                      ServiceInfo.longitude: longitude,
                      "address": featAddress] as [String : Any]
        
        
        API.requestPOSTURL(serviceName: APINAMES.Matchpreferences,
                           withStaticAccessToken:false,
                           params: params,
                           success: { (response) in
                            Helper.hideProgressIndicator()
                            if (response.null != nil){
                                return
                            }
                            
                            self.handlePostPrefAPIResponse(response: response.dictionaryObject!)
        },
                           failure: { (Error) in
                            self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: Error.localizedDescription)
                            Helper.hideProgressIndicator()
        })
        
    }
    
    
    /// handle handle Post Preferences API Response
    ///
    /// - Parameter response: respnse details.
    func handlePostPrefAPIResponse(response:[String:Any]) {
        let statuscode = response["code"] as! Int
        switch (statuscode) {
        case 200:
            self.delegate?.prefrenceUpdateCompleted()
            
            if self.isComingFromSignUp {
                redirectToHomeVc()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
            
      case 401:
                Helper.changeRootVcInVaildToken()
            break
        default:
            self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage: response[LoginResponse.errorMessage] as! String)
        }
    }
    
    func redirectToHomeVc() {
        requestForPostDeepLink()
        self.performSegue(withIdentifier: SegueIds.matchPrefToTabBarController, sender: self.userData)
    }
    
    
    //updating user deepLink.
    func requestForPostDeepLink() {
        
    }
    
    
    /// method will trigger when user changes slider.
    ///
    /// - Parameter sender: slider outlet.
    @IBAction func segmentChanged(_ sender: Any) {
        
        trackPrefrenceChanges = true
        
        let segement = sender as! UISegmentedControl
        
        var selectedUnit = ""
        selectedUnit = segement.titleForSegment(at:segement.selectedSegmentIndex)!
        
        
        let matchPref = preferenceArray[segement.tag]
        let sliderIndexPath =  IndexPath(row: segement.tag, section:1)
        
        let sliderCell: RangeSliderTableViewCell = self.prefrenceTableView.cellForRow(at: sliderIndexPath) as! RangeSliderTableViewCell
        
        
        if ( matchPref.prefernceId == Constants.distanceID) {
            
            var optinalValueMinimum  = CGFloat(sliderCell.rangeSliderOutlet.minimumValue)
            var optinalValueMaximum  = CGFloat(sliderCell.rangeSliderOutlet.maximumValue)
            var selectedValueMinimum = CGFloat(sliderCell.rangeSliderOutlet.leftValue)
            var selectedValueMaximum = CGFloat(sliderCell.rangeSliderOutlet.rightValue)
            
            if(selectedUnit == "km") {
                selectedValueMinimum = self.convertMilesToKilometers(speedInMPH: selectedValueMinimum)
                selectedValueMaximum = self.convertMilesToKilometers(speedInMPH: selectedValueMaximum)
                
                optinalValueMinimum = self.convertMilesToKilometers(speedInMPH: optinalValueMinimum)
                optinalValueMaximum = self.convertMilesToKilometers(speedInMPH: optinalValueMaximum)
                
            } else {
                selectedValueMinimum = self.convertKilometersToMiles(speedInMPH: selectedValueMinimum)
                selectedValueMaximum = self.convertKilometersToMiles(speedInMPH: selectedValueMaximum)
                
                optinalValueMinimum = self.convertKilometersToMiles(speedInMPH: optinalValueMinimum)
                optinalValueMaximum = self.convertKilometersToMiles(speedInMPH: optinalValueMaximum)
            }
            
            sliderCell.rangeSliderOutlet.setMinValue(optinalValueMinimum, maxValue:optinalValueMaximum)
            sliderCell.rangeSliderOutlet.setLeftValue(selectedValueMinimum, rightValue:selectedValueMaximum)
            
            //    sliderCell.preferenceValueLabel.text = "\(Int(sliderCell.rangeSliderOutlet.leftValue))" + "-" + "\(Int(sliderCell.rangeSliderOutlet.rightValue))"
            
            if selectedUnit == "km" {
                
                
                sliderCell.preferenceValueLabel.text = "\(Int(sliderCell.rangeSliderOutlet.leftValue))" + StringConstants.km() + " - \(Int(sliderCell.rangeSliderOutlet.rightValue))" + StringConstants.km()
            }else{
                
                sliderCell.preferenceValueLabel.text = "\(Int(sliderCell.rangeSliderOutlet.leftValue))" + StringConstants.mi() + " - \(Int(sliderCell.rangeSliderOutlet.rightValue))" + StringConstants.mi()
            }
        }
        
        if( matchPref.prefernceId == Constants.heightID) {
            
            let selectedValueMinimum = CGFloat(sliderCell.rangeSliderOutlet.leftValue)
            let selectedValueMaximum = CGFloat(sliderCell.rangeSliderOutlet.rightValue)
            
            if(selectedUnit == "ft") {
                let minFeetValue = self.getFeetAndInchesFrom(centimeters: Float(selectedValueMinimum))
                let maxFeetValue = self.getFeetAndInchesFrom(centimeters: Float(selectedValueMaximum))
                sliderCell.preferenceValueLabel.text = "\(minFeetValue) - \(maxFeetValue)"
                
            } else {
                //sliderCell.preferenceValueLabel.text = "\(Int(selectedValueMinimum)) - \(Int(selectedValueMaximum))"
                sliderCell.preferenceValueLabel.text = "\(Int(selectedValueMinimum))" + StringConstants.cm() + " - \(Int(selectedValueMaximum))" + StringConstants.cm()
            }
        }
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        
        trackPrefrenceChanges = true
        
        let rangeSlider = sender as! MARKRangeSlider
        let sliderIndexPath =  IndexPath(row: rangeSlider.tag, section:1)
        self.updatePrefrenceValue(reloadAtIndex: sliderIndexPath)
    }
    
    func convertKmToMiles() {
        
    }
    
    func convertMilesToKm() {
        
    }
    
    // Convert from miles to kilometers (Double)
    func convertMilesToKilometers(speedInMPH:CGFloat) ->CGFloat {
        let speedInKPH = speedInMPH * 1.60934
        return speedInKPH as CGFloat
    }
    
    // Convert from kilometers to miles (Double)
    func convertKilometersToMiles(speedInMPH:CGFloat) ->CGFloat {
        let speedInKPH = speedInMPH / 1.60934
        return speedInKPH as CGFloat
    }
    
    
    // Convert from inches to cm
    func convertInchesToCentimeters(depthInInches:CGFloat) ->CGFloat {
        let depthInCentimeters = depthInInches * 2.54
        return depthInCentimeters.rounded() as CGFloat
    }
    
    // Convert from cm to inches
    func convertCentimetersToInches(depthInCentimeters:CGFloat) ->CGFloat {
        let depthInInches = depthInCentimeters / 2.54
        return depthInInches.rounded() as CGFloat
    }
    
    func convertFeetToCm(feetVALUE:CGFloat) -> CGFloat {
        let depthInCentimeters = feetVALUE * 2.54 * 12
        return depthInCentimeters.rounded() as CGFloat
    }
    
    func convertCmToFett(cmValue:CGFloat)  ->CGFloat {
        let depthInInches = cmValue / (12*2.54)
        return depthInInches.rounded() as CGFloat
    }
    
    func getFeetAndInchesFrom(centimeters:Float) ->String
    {
        let numInches = roundf(centimeters/Constants.Inch_In_CM)
        let feet:Int = Int(numInches/Constants.numberOfInchesInFeet)
        let inches:Float = roundf(numInches.truncatingRemainder(dividingBy: Constants.numberOfInchesInFeet))
        return String(format: "\(feet)'\(Int(inches))\"")
    }
}
//MARK:- LocationManager Delegate
extension MatchPreferencesVC : LocationManagerDelegate {
    func didFailToUpdateLocation() {
        
    }
    // func didUpdateLocation(location: Location, search: Bool) {
    func didUpdateLocation(location: Location, search:Bool,update:Bool) {
        
        
    }
    
    func didChangeAuthorization(authorized: Bool) {
        
    }
    
    func didUpdateSearch(locations: [Location]) {
        
        
        
    }
}


// MARK: - UITableViewDelegate,UITableViewDataSource
extension MatchPreferencesVC : UITableViewDelegate,UITableViewDataSource  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == checkBoxTableView {
            if selectedIndex.row != -1 {
                return 1
            }else { return 0 }
        }else{
            return 2
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == checkBoxTableView {
            let prefrenceDict = preferenceArray[selectedIndex.row]
            let selctedValues = prefrenceDict.optionsForMenu
            return selctedValues.count+1
        }else {
            switch section {
            case 0:
                return 1
            default:
                if(preferenceArray.count > 0) {
                    Helper.hideProgressIndicator()
                }
                return preferenceArray.count;
            }
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if tableView == checkBoxTableView {
            
            return 40
        }else{
            
            if section == 1 {
                return 100
            }
            else {
                return 0
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == checkBoxTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckBoxTableViewCell", for: indexPath) as! CheckBoxTableViewCell
            cell.checkBoxButton.tag = indexPath.row
            let prefrenceDict = preferenceArray[selectedIndex.row]
            if indexPath.row == prefrenceDict.optionsForMenu.count{
                cell.ethnicityValueLabel.text = "Done"
                cell.ethnicityValueLabel.textColor = Colors.Red
                cell.checkBoxButton.isHidden = true
                cell.ethnicityValueLabel.textAlignment = .center
            }else{
                
                let prefrenceDict = preferenceArray[selectedIndex.row]
                if prefrenceDict.optionsForMenu.count > indexPath.row {
                    cell.ethnicityValueLabel.text = prefrenceDict.optionsForMenu[indexPath.row]
                    cell.ethnicityValueLabel.textColor = Colors.PrimaryText
                    cell.checkBoxButton.isHidden = false
                    cell.ethnicityValueLabel.textAlignment = .left
                    cell.checkBoxButton.isSelected = false
                    if prefrenceDict.prefernceType == 2 {
                        let uniqval = selectedOptions.removingDuplicates()
                        let op = prefrenceDict.optionsForMenu[indexPath.row]
                        if uniqval.contains(op){
                            cell.checkBoxButton.isSelected = true
                        }else{
                            cell.checkBoxButton.isSelected = false
                        }
                    }else{
                        if selctedOption != prefrenceDict.optionsForMenu[indexPath.row] {
                            cell.checkBoxButton.isSelected = false
                        }else{
                            cell.checkBoxButton.isSelected = true
                        }
                    }
                }
            }
            return cell
        }else {
            
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "currentLocationCell", for: indexPath) as! MyCurrentLocationCell
                let selectedAddr = Helper.getFeatureLocation()
                // cell.locationLabel.text = StringConstants.location()
                if cell.currentAddress.text == selectedAddr.name {
                    self.trackPrefrenceChanges = false
                }else {
                    self.trackPrefrenceChanges = true
                }
                cell.updateCell()
                return cell
            }
            let matchPref = preferenceArray[indexPath.row]
            
            switch matchPref.prefernceType {
            case 1,2:
                let dropDownCell:DropDownTableViewCell = tableView.dequeueReusableCell(withIdentifier: "dropDownCell") as! DropDownTableViewCell
                dropDownCell.preferenceTitle.text = matchPref.prefernceTitle
                dropDownCell.dropDownbutton.tag = indexPath.row
                
                
                if(matchPref.selectedValuesForMenu.count > 1) {
                    dropDownCell.preferenceValue.text = matchPref.selectedValuesForMenu[0] +
                        " + \(matchPref.selectedValuesForMenu.count - 1)"
                }else if(matchPref.selectedValuesForMenu.count == 1) {
                    dropDownCell.preferenceValue.text = matchPref.selectedValuesForMenu[0]
                }else {
                    dropDownCell.preferenceValue.text = ""
                }
                
//                if matchPref.prefernceType == 2 {
//                    if selectedOptions.count == 0 {
//                        selectedOptions = matchPref.selectedValuesForMenu  //previous val
//                        dropDownCell.selectedArrv = selectedOptions
//                    }else{
//                        dropDownCell.selectedArrv = selectedOptions
//                    }
//                }
                
                return dropDownCell
            default:
                let sliderCell:RangeSliderTableViewCell =  tableView.dequeueReusableCell(withIdentifier:"rangeSliderCell") as! RangeSliderTableViewCell
                sliderCell.prefrenceTypeTitle.text  = matchPref.prefernceTitle
                
                sliderCell.rangeSliderOutlet.leftThumbView.isHidden = false
                sliderCell.segmentWidthConstarint.constant = 0;
                if matchPref.optinalUnits.count == 1 {
                    sliderCell.segementControlOutlet.isHidden = true
                    sliderCell.rangeSliderOutlet.minimumDistance = 1.0
                    
                    if matchPref.selectedValues.count != 0 && matchPref.optionalValues.count != 0 {
                        sliderCell.rangeSliderOutlet.setMinValue(CGFloat(matchPref.optionalValues[0]), maxValue:CGFloat(matchPref.optionalValues[1]))
                        sliderCell.rangeSliderOutlet.setLeftValue(CGFloat(matchPref.selectedValues[0]), rightValue:CGFloat(matchPref.selectedValues[1]))
                        //for age preferences.
                        if(matchPref.optionalValues[1] == matchPref.selectedValues[1]) {
                            sliderCell.preferenceValueLabel.text = "\(Int(sliderCell.rangeSliderOutlet.leftValue))" + "-" + "\(Int(sliderCell.rangeSliderOutlet.rightValue))" + "+"
                        } else {
                            sliderCell.preferenceValueLabel.text = "\(Int(sliderCell.rangeSliderOutlet.leftValue))" + "-" + "\(Int(sliderCell.rangeSliderOutlet.rightValue))"
                        }
                    }
                    
                } else {
                    
                    sliderCell.segementControlOutlet.isHidden = false
                    
                    //setting segemented control titles.
                    for (index, _) in matchPref.optinalUnits.enumerated() {
                        if (matchPref.optinalUnits[index] == matchPref.selectedUnit) {
                            sliderCell.segementControlOutlet.selectedSegmentIndex = index
                        }
                        sliderCell.segementControlOutlet.setTitle(matchPref.optinalUnits[index], forSegmentAt: index)
                    }
                    sliderCell.segmentWidthConstarint.constant = 80;
                    
                    if( matchPref.prefernceId == Constants.heightID) {
                        sliderCell.rangeSliderOutlet.minimumDistance = 3.0
                        
                        let optinalValueMinimum  = CGFloat(matchPref.optionalValues[0])
                        let optinalValueMaximum  = CGFloat(matchPref.optionalValues[1])
                        let selectedValueMinimum = CGFloat(matchPref.selectedValues[0])
                        let selectedValueMaximum = CGFloat(matchPref.selectedValues[1])
                        
                        if(matchPref.selectedUnit == "ft") {
                            let minFeetValue = self.getFeetAndInchesFrom(centimeters: Float(selectedValueMinimum))
                            let maxFeetValue = self.getFeetAndInchesFrom(centimeters: Float(selectedValueMaximum))
                            sliderCell.preferenceValueLabel.text = "\(minFeetValue) - \(maxFeetValue)"
                            
                        } else {
                            sliderCell.preferenceValueLabel.text = "\(Int(selectedValueMinimum))" + StringConstants.cm() + " - \(Int(selectedValueMaximum))" + StringConstants.cm()
                        }
                        
                        sliderCell.rangeSliderOutlet.setMinValue(optinalValueMinimum, maxValue:optinalValueMaximum)
                        sliderCell.rangeSliderOutlet.setLeftValue(selectedValueMinimum, rightValue:selectedValueMaximum)
                        sliderCell.rangeSliderOutlet.leftThumbView.isHidden = false
                        
                    } else if ( matchPref.prefernceId == Constants.distanceID) {
                        
                        sliderCell.rangeSliderOutlet.leftThumbView.isHidden = true
                        
                        
                        sliderCell.rangeSliderOutlet.minimumDistance = 1.0
                            var optinalValueMinimum  = CGFloat(matchPref.optionalValues[0])
                            var optinalValueMaximum  = CGFloat(matchPref.optionalValues[1])
                       
                            var selectedValueMinimum = CGFloat(matchPref.selectedValues[0])
                            var selectedValueMaximum = CGFloat(matchPref.selectedValues[1])
                            
                       
                        
                        
                        if(matchPref.selectedUnit == "km") {
                            
                        } else {
                            selectedValueMinimum = self.convertKilometersToMiles(speedInMPH: selectedValueMinimum)
                            selectedValueMaximum = self.convertKilometersToMiles(speedInMPH: selectedValueMaximum)
                            
                            optinalValueMinimum = self.convertKilometersToMiles(speedInMPH: optinalValueMinimum)
                            optinalValueMaximum = self.convertKilometersToMiles(speedInMPH: optinalValueMaximum)
                        }
                        
                        sliderCell.rangeSliderOutlet.setMinValue(optinalValueMinimum, maxValue:optinalValueMaximum)
                        sliderCell.rangeSliderOutlet.setLeftValue(selectedValueMinimum, rightValue:selectedValueMaximum)
                        
                        
                        if(matchPref.selectedUnit == "km") {
                            sliderCell.preferenceValueLabel.text = "\(Int(sliderCell.rangeSliderOutlet.leftValue))" + StringConstants.km() + "-" + "\(Int(sliderCell.rangeSliderOutlet.rightValue))" + StringConstants.km()
                        }else{
                            sliderCell.preferenceValueLabel.text = "\(Int(sliderCell.rangeSliderOutlet.leftValue))" + StringConstants.mi() + "-" + "\(Int(sliderCell.rangeSliderOutlet.rightValue))" + StringConstants.mi()
                        }
                    }
                    
                }
                
                sliderCell.rangeSliderOutlet.tag = indexPath.row
                sliderCell.segementControlOutlet.tag = indexPath.row
                
                return sliderCell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == checkBoxTableView {
            let prefrenceDict = preferenceArray[selectedIndex.row]
            let selctedValues = prefrenceDict.optionsForMenu
            
            if selctedValues.count == indexPath.row {
             return 56
            }
            return 46
        }else {
            
            // 3 is slider
            //1 is drop down
            //4 is slider
            if indexPath.section == 1 {
                let matchPref = preferenceArray[indexPath.row]
                
                switch matchPref.prefernceType {
                case 1,2:
                    return 60
                default:
                    return 105
                }
            }else {
                return 105
            }
        }
    }
    
    func openInAppPurchaseView() {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let videoView = InAppPuchasePopupView(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        videoView.loadInAppPurchaseView()
        window.addSubview(videoView)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == checkBoxTableView {
            selectedOptionValueIndx = indexPath
            
            let prefrenceDict = preferenceArray[selectedIndex.row]
            let selctedValues = prefrenceDict.optionsForMenu
            
            if selctedValues.count == indexPath.row {
                setTableHight(optionsCount: 0)
                let typeid = preferenceArray[selectedIndex.row].prefernceType
                openGenderPreferences(typeId: typeid)
            }else {
                let typeid = preferenceArray[selectedIndex.row].prefernceType
                if typeid == 2 {
                    
                    if selectedOptions.contains(selctedValues[indexPath.row]) {
                        if let index = selectedOptions.index(of: selctedValues[indexPath.row]) {
                            selectedOptions.remove(at: index)
                        } else {
                            // not found
                        }
                    }else {
                        selectedOptions.append(selctedValues[indexPath.row])
                    }
                }else{
                    selctedOption = selctedValues[indexPath.row]
                }
                checkBoxTableView.reloadData()
            }
        }else{
            if indexPath.section == 0 {
//                if  Helper.isSubscribedForPlans() {
                    openLocationVC()
//                } else {
//                    openInAppPurchaseView()
//                }
            }
        }
    }
    func openLocationVC() {
        let changeLocationVC = Helper.getSBWithName(name: "DatumTabBarControllers").instantiateViewController(withIdentifier: "Location") as! PassportLocationVC
        changeLocationVC.delegate = self
        
        self.navigationController?.pushViewController(changeLocationVC, animated: true)
    }
}

extension MatchPreferencesVC :UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let movedOffset: CGFloat = scrollView.contentOffset.y
        if scrollView == prefrenceTableView {
            self.view.endEditing(true)
            let pageWidth: CGFloat = 88
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            if ratio > 0 {
                if movedOffset >= 20 {
                    self.topBackBtn.isHidden = false
                    self.titleLabel.textColor = Colors.PrimaryText .withAlphaComponent(ratio)
                    self.titleHeader.backgroundColor = Colors.SecondBaseColor //.withAlphaComponent(ratio)
                    Helper.setShadow(sender: self.titleHeader)
                    if  ratio <= 1 && ratio >= 0{
                        self.titleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle)*ratio)
                    }
                    if movedOffset > 88 {
                        self.titleLabel.font = UIFont (name: CircularAir.Bold, size: CGFloat(fontSize.navigationTitle))
                    }
                    self.titleHeader.isHidden = false
                    self.view.layoutIfNeeded()
                }
            }else {
                self.titleLabel.textColor = UIColor.clear
                self.titleHeader.backgroundColor = UIColor.clear
                Helper.removeShadow(sender: self.titleHeader)
                self.topBackBtn.isHidden = true
                self.titleHeader.isHidden = true
                
            }
        }
    }
}


extension MatchPreferencesVC : PassportLocationVCDelegate {
    
    func didSelectAddress(address: Location) {
        selectedAdreess = address
        Helper.saveFeaturLocation(data: address)
        prefrenceTableView.reloadData()
        let dbObj = Database.shared
        dbObj.userDataObj.latitude =  String(address.latitude)
        dbObj.userDataObj.longitude = String(address.longitude)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
}


extension MatchPreferencesVC {
    
    //    /// initial view setup
    func initialSetup(){
        Helper.setShadow(sender: checkBoxTableView)
      optionsView.layer.cornerRadius = 12
        optionsView.layer.borderWidth = 1
        optionsView.layer.borderColor = Colors.SeparatorLight.cgColor
        setTableHight(optionsCount: 0)
    }
    
    /// sets Table Hight as per number of cell
    func setTableHight(optionsCount: Int){
        UIView.animate(withDuration: 0.5) {
            if optionsCount == 0 {
                self.checkBoxTableHeight.constant = CGFloat(0)

            }else{
                self.checkBoxTableHeight.constant = CGFloat((optionsCount * 46)+54)

            }
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        }
      //  checkBoxTableView.reloadData()
    }
 
}


extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}
