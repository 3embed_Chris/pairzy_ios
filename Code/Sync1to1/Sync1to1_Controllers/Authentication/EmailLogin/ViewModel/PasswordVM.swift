//
//  PasswordVM.swift
//  Datum
//
//  Created by apple on 16/05/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PasswordVM: NSObject {
    
    var authentication = Authentication()
    let disposeBag = DisposeBag()
    let passwordVM_resp = PublishSubject<Bool>()
    var message = ""
    var locAuth = LocalAuthentication(data: [:])
   
 func updateNewPassword() {
        let authentication = Authentication()
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        let requestData = RequestModel()
        let params = [ServiceInfo.email: locAuth.email,
                      ServiceInfo.password: locAuth.password,
            ] as [String : Any]
        requestData.ApiRequesDetails = params
        if !authentication.subject_response.hasObservers {
            authentication.subject_response
                .subscribe(onNext: {response in
                    Helper.hideProgressIndicator()
                    self.handleUpdateNewPasswordResponse(responseModel: response)
                }, onError: {error in
                    Helper.hideProgressIndicator()
                    self.message = error.localizedDescription
                    self.passwordVM_resp.onError(error)
                })
                .disposed(by:disposeBag)
        }
        authentication.updateNewPassword(newPasswordData: requestData)
    }
    


func handleUpdateNewPasswordResponse(responseModel: ResponseModel){


    let statuscode:API.ErrorCode = API.ErrorCode(rawValue: responseModel.statusCode)!
    switch (statuscode) {
    case .Success:
        self.passwordVM_resp.onNext(true)
    default:
        if let msg = responseModel.response["message"] as? String {
            self.message = msg
        }
        self.passwordVM_resp.onNext(false)
        print(responseModel.response)
        break
    }
    
  }
}
