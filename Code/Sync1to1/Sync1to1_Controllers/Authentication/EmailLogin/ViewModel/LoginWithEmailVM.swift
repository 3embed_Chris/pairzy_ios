//
//  LoginWithEmailVM.swift
//  Datum
//
//  Created by apple on 16/05/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class LoginWithEmailVM: NSObject {
    
    enum ResponseType: Int {
        case emailLoginResp = 0
        case forgotPwdResp = 1
        case emaiIdExistVerify = 2
    }
    
    let disposeBag = DisposeBag()
    let loginWithEmailVM_resp = PublishSubject<(ResponseType,Bool)>()
    var message = ""
    var locAuth = LocalAuthentication(data: [:])

    
    func loginWithEmail(){
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        var pushToken = ServiceInfo.constantPushToken
        if let token = UserDefaults.standard.value(forKey: SyUserdefaultKeys.kNSUPushNotificationTokenKey) as? String
        {
            pushToken = token
        }
        var appVersion = "1.0"
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            appVersion = version
        }
        
//
//        let jpgURL = URL(string:locAuth.profileVideo)?
//            .deletingPathExtension()
//            .appendingPathExtension("jpg")
        
        let requestData = RequestModel()
        let params = [ServiceInfo.emailId: locAuth.email,
                      ServiceInfo.password: locAuth.password,
//                      ServiceInfo.deviceOs : UIDevice.current.systemVersion,
//                      ServiceInfo.deviceId:UIDevice.current.identifierForVendor?.uuidString as AnyObject,
//                      ServiceInfo.deviceModelKey:Helper.DeviceModel,
//                      ServiceInfo.DeviceType:"1" as AnyObject,
//                      ServiceInfo.appVersion           :appVersion,
//                      ServiceInfo.PushToken:pushToken as AnyObject,
//                      ServiceInfo.deviceMakeKey:UIDevice.current.name as AnyObject
            ] as [String:Any]
        requestData.ApiRequesDetails = params
        let authentication = Authentication()
        authentication.loginWithEmail(emailDetails: requestData)
        authentication.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()

                self.handleEmailLoginResponse(responseModel: response)
            }, onError: { (error) in
                Helper.hideProgressIndicator()

            }).disposed(by: disposeBag)
    }
    
    
    func emailIdExistVerification(emailId: String){
        let authentication = Authentication()
     //   let requestData = RequestModel()
        let params = [ServiceInfo.email: emailId] as [String : Any]
       // requestData.ApiRequesDetails = params
      //  if !authentication.subject_response.hasObservers{
            authentication.subject_response
                .subscribe(onNext: {response in
                    Helper.hideProgressIndicator()
                    self.handleEmailIdExistVerifyResponse(responseModel: response)
                }, onError: { (error) in
                    Helper.hideProgressIndicator()
                    self.message = error.localizedDescription
                    self.loginWithEmailVM_resp.onError(error)
                }).disposed(by: disposeBag)
        //}
        authentication.patchEmailIdExistsVerificaiton(emailId: params)
    }
    
    
    func postForgotPassword(emailId:String){
        let authentication = Authentication()
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        let requestData = RequestModel()
        let params = [ServiceInfo.email: emailId] as [String : Any]
        requestData.ApiRequesDetails = params
        if !authentication.subject_response.hasObservers {
            authentication.subject_response
                .subscribe(onNext: {response in
                    Helper.hideProgressIndicator()
                    self.handleForgotPassword(responseModel: response)
                }, onError: {error in
                    Helper.hideProgressIndicator()
                    self.message = error.localizedDescription
                    self.loginWithEmailVM_resp.onError(error)
                })
                .disposed(by:disposeBag)
        }
        authentication.forgotPassword(emailId: requestData)
    }
    

    
    func handleForgotPassword(responseModel: ResponseModel){
        let statuscode:API.ErrorCode = API.ErrorCode(rawValue: responseModel.statusCode)!
        switch (statuscode) {
        case .Success:
            self.loginWithEmailVM_resp.onNext((.forgotPwdResp, true))
        default:
            Helper.hideProgressIndicator()
            if let msg = responseModel.response["message"] as? String {
                self.message = msg
            }
             self.loginWithEmailVM_resp.onNext((.forgotPwdResp, false))
             print(responseModel.response)
        }
        
    }
  
    
    func handleEmailIdExistVerifyResponse(responseModel: ResponseModel){
        
        let statuscode:API.ErrorCode = API.ErrorCode(rawValue: responseModel.statusCode)!
        switch (statuscode) {
        case .Success:
            self.loginWithEmailVM_resp.onNext((.emaiIdExistVerify, true))
        default:
            Helper.hideProgressIndicator()
            if let msg = responseModel.response["message"] as? String {
                self.message = msg
            }
            self.loginWithEmailVM_resp.onNext((.emaiIdExistVerify, false))
            print(responseModel.response)
            break
        }
        
    }
    
    
    func handleEmailLoginResponse(responseModel: ResponseModel){
        guard let statuscode:API.ErrorCode = API.ErrorCode(rawValue: responseModel.statusCode) else {
            return
        }
        switch (statuscode) {
        case .Success:
            let response = responseModel.response
            if let dict = response["data"] as? [String:Any]{
                
                //making userdata object.
                let userData = User.init(userDataDictionary: dict)
                //saving userdata in core data.
                Database.shared.addUserdataToDatabase(withUserdata:userData)
                
                if let count = dict[profileData.count] as? [String:Any]{
                    Helper.saveCountsObj(data: count)
                }
                
                        self.loginWithEmailVM_resp.onNext((.emailLoginResp,true))
                        
                        if let sessionToken = dict["token"] as? String{
                            UserLoginDataHelper.saveUserToken(token:sessionToken)
                                }
                //   Saving userId In User Default After Login Response Came
                if let userId = dict["_id"] as? String {
                    UserLoginDataHelper.saveUserId(userId: userId)
                }
                if let userId = dict["userId"] as? String {
                    UserLoginDataHelper.saveUserId(userId: userId)
                }
                        if let dataForSubscrption =  responseModel.response["data"] as? [String:Any] {
                            
                            if let isUserPurchased = dataForSubscrption["subscription"] as? [Any] {
                                if isUserPurchased.count > 0 {
                                    if let purchaseDetails = isUserPurchased[0] as? [String:Any] {
                                        if let isPurchased = purchaseDetails["subscriptionId"] as? String,isPurchased == "Free Plan" {
                                            Helper.saveIsUserPurchased(flag: false)
                                        } else {
                                            Helper.saveIsUserPurchased(flag: true)
                                        }
                                        Helper.savePurchasedPlan(data:purchaseDetails)
                                    }
                                }
                                print("data for subscrption")
                            }
                        }
                        
                        
                        //updating signupData in couchDb.
                        let couchbaseObj = Couchbase.sharedInstance
                        let usersDocVMObject = UsersDocumentViewModel(couchbase: couchbaseObj)
                        usersDocVMObject.updateUserDoc(data: responseModel.response["data"] as! [String : Any], withLoginType: "1")
                        
                        let dataDict = responseModel.response["data"] as! [String:Any]
                        Helper.savePreferences(data: dataDict)
                        
                        
                        //subscribing For Push notification topic.
                        let appDelegateObj = UIApplication.shared.delegate! as! AppDelegate
                        appDelegateObj.subscribeForPushNotificatiionTopic()
                        
                        Helper.showCoinsDeductPopup(flag: true, popUpFor: .forAll)
                        
                        
                        MQTT.sharedInstance.disconnectMQTTConnection()
                        if let userID = Helper.getMQTTID() {
                            MQTT.sharedInstance.connect(withClientId: userID)
                            
                        }
            }
            
        case .Banned:
            Helper.hideProgressIndicator()
            if let msg = responseModel.response["message"] as? String {
                self.message = msg
            }
            self.loginWithEmailVM_resp.onNext((.emailLoginResp,false))
        default:
            Helper.hideProgressIndicator()
            if let msg = responseModel.response["message"] as? String {
                self.message = msg
            }
            self.loginWithEmailVM_resp.onNext((.emailLoginResp,false))
            break
           
        }
    }
}

