//
//  EmailVerifyVM.swift
//  Datum
//
//  Created by Rahul Sharma on 16/05/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class EmailVerifyVM: NSObject {
    
        let disposeBag = DisposeBag()
        let emailVerifyVM_resp = PublishSubject<Bool>()
        var message = ""
        var locAuth = LocalAuthentication(data: [:])
    
    //func emailIdOtpVerification(emailId: String, otp: String){
    func emailIdOtpVerification(){

        let authentication = Authentication()
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        let requestData = RequestModel()
        let params = [ServiceInfo.email: locAuth.email,
                      ServiceInfo.otp: locAuth.otp
                      ] as [String : Any]
        requestData.ApiRequesDetails = params
        if !authentication.subject_response.hasObservers {
            authentication.subject_response
                .subscribe(onNext: {response in
                    Helper.hideProgressIndicator()
                    self.handleEmailIdOtpVerificationResponse(responseModel: response)

                }, onError: {error in
                    Helper.hideProgressIndicator()
                    self.message = error.localizedDescription
                    self.emailVerifyVM_resp.onError(error)
                })
                .disposed(by:disposeBag)
        }
        authentication.postVerifyOTPForEmailLogin(otpDetails: requestData)
    }

    
    func handleEmailIdOtpVerificationResponse(responseModel: ResponseModel){
        
        let statuscode:API.ErrorCode = API.ErrorCode(rawValue: responseModel.statusCode)!
        switch (statuscode) {
        case .Success:
            self.emailVerifyVM_resp.onNext(true)
        default:
            if let msg = responseModel.response["message"] as? String {
                self.message = msg
            }
            self.emailVerifyVM_resp.onNext(false)
            print(responseModel.response)
            break
        }
        
    }
    
    
}
