//
//  PasswordVC.swift
//  Datum
//
//  Created by apple on 15/05/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
class PasswordVC: UIViewController {

    @IBOutlet weak var createPasswordLAbel: UILabel!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var confirmPasswordHeight: NSLayoutConstraint!
    @IBOutlet weak var passwordSecond: UIView!
    @IBOutlet weak var separatorFirst: UIView!
    @IBOutlet weak var enterYour: UILabel!
    var emailLoginVC = LoginWithEmailVC()

    @IBOutlet weak var passwordMessage: UILabel!
    
    @IBOutlet weak var eyeOldPaswordButton: UIButton!
    
    @IBOutlet weak var eyeNewPaswordButton: UIButton!
    var isForNewPassword = false
    let passwordVM = PasswordVM()
    var isForFacebook = false
    var disposeBag = DisposeBag()
    var message = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
      passwordTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
      newPasswordTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        initialSetup()
        didGetResponse()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        passwordTF.becomeFirstResponder()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserver()
    }
    
    func initialSetup(){
        backBtn.tintColor = Colors.AppBaseColor
        passwordTF.textColor = Colors.PrimaryText
        newPasswordTF.textColor = Colors.PrimaryText
        createPasswordLAbel.textColor = Colors.AppBaseColor
        passwordMessage.text = StringConstants.minPasswordLengthMessage()
        passwordMessage.textColor = Colors.Red
        if isForNewPassword {
            createPasswordLAbel.text = StringConstants.enterNewPassword()
            passwordTF.placeholder = StringConstants.enterNewPassword()
            newPasswordTF.placeholder = StringConstants.confirmNewPassword()
            confirmPasswordHeight.constant = 50
            newPasswordTF.isHidden = false
            enterYour.text = ""
            passwordSecond.isHidden = false

        }else{
            createPasswordLAbel.text = StringConstants.password()
            passwordTF.placeholder = StringConstants.enterYourPassword()
            newPasswordTF.isHidden = true
            confirmPasswordHeight.constant = 0
            enterYour.text = StringConstants.enterYour()
            enterYour.textColor = Colors.PrimaryText
            passwordSecond.isHidden = true
        }
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
        self.passwordVM.locAuth.password = passwordTF.text!
        checkPassword()
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func toggleOldPasswordSecurity(sender: UIButton) {
        self.passwordTF.isSecureTextEntry = !self.passwordTF.isSecureTextEntry
        if passwordTF.isSecureTextEntry {
            eyeOldPaswordButton.setImage(#imageLiteral(resourceName: "Hide"), for: .normal)
        } else {
            eyeOldPaswordButton.setImage(#imageLiteral(resourceName: "View"), for: .normal)
        }
    }
    
    @IBAction func toggleNewPasswordSecurity(sender: UIButton) {
        self.newPasswordTF.isSecureTextEntry = !self.newPasswordTF.isSecureTextEntry
        if newPasswordTF.isSecureTextEntry {
            eyeNewPaswordButton.setImage(#imageLiteral(resourceName: "Hide"), for: .normal)
        } else {
            eyeNewPaswordButton.setImage(#imageLiteral(resourceName: "View"), for: .normal)
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func gotoNameVC(){
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.userBasicSI) as! UserBaiscDetailsVC
        userBasics.basicDetails = .name
        userBasics.isForFacebook = self.isForFacebook
        userBasics.detailsVM.locAuth = self.passwordVM.locAuth
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
    func goToEmailLoginVC(){
        let loginEmailVC = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:"LoginWithEmail") as! LoginWithEmailVC
        isForNewPassword = false
        self.navigationController?.pushViewController(loginEmailVC, animated: true)
    }
    
   
    func didGetResponse(){
        passwordVM.passwordVM_resp.subscribe(onNext: {success in
            if success{
                self.goToEmailLoginVC()
            }else{
                Helper.showAlertWithMessage(withTitle: "Message", message: self.passwordVM.message, onViewController: self)
            }
        }, onError: { (error) in
            print(error)
        }).disposed(by: passwordVM.disposeBag)
        
    }
}


// MARK: - UITextFieldDelegate
extension PasswordVC: UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField : UITextField) {
        
        switch textField {
        case passwordTF :
            if passwordTF.text?.count != 0 {
                self.separatorFirst.backgroundColor = Colors.AppBaseColor
            }else{
                self.separatorFirst.backgroundColor = Colors.SeparatorDark
            }
           passwordSecond.backgroundColor = Colors.SeparatorDark
            break
        case newPasswordTF :
            if newPasswordTF.text?.count != 0 {
                passwordSecond.backgroundColor = Colors.AppBaseColor

            }else {
                passwordSecond.backgroundColor = Colors.SeparatorDark
            }
           self.separatorFirst.backgroundColor = Colors.SeparatorDark
            break
        default:
            break
        }
        
         if isForNewPassword {
            if ((passwordTF.text?.count)! >= 6) && ((newPasswordTF.text?.count)! >= 6) {
                nextBtn.isEnabled = true
              //  nextBtn.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
                nextBtn.isSelected = true
            }
            else{
                nextBtn.isEnabled = false
                //nextBtn.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
                nextBtn.isSelected = false

            }
         }else {
            if ((passwordTF.text?.count)! >= 6){
                nextBtn.isEnabled = true
                nextBtn.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
            }
            else{
                nextBtn.isEnabled = false
                nextBtn.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case passwordTF :
            if isForNewPassword {
                newPasswordTF.becomeFirstResponder()
            }else{
                passwordTF.resignFirstResponder()
            }
        default:
            newPasswordTF.resignFirstResponder()
            break
        }
    }
    
    
    /// check password field
    func checkPassword(){
        if isForNewPassword && isMatchPassword(){
            self.passwordVM.updateNewPassword()
            self.passwordVM.passwordVM_resp.subscribe(onNext: { success in
                //go back to rootVC loginView controller
                Helper.showAlertWithMessageOnwindow(withTitle: "Success", message: "Password successfully updated")
            }).disposed(by: self.passwordVM.disposeBag)
        }else if isForNewPassword == false {
            gotoNameVC()
        }
}



    func isMatchPassword()-> Bool{
        if isForNewPassword {
            guard self.passwordTF.text!.count != 0 else {
                return false
            }
            guard self.newPasswordTF.text?.count != 0 else {
                return false
            }
            guard self.passwordTF.text == self.newPasswordTF.text else {
                Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: StringConstants.pwdDoesntMatch(), onViewController: self)
                return false
            }
            return true
        }
        return false
    }

}
