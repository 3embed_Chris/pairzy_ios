//
//  LoginWithEmailVC.swift
//  Datum
//
//  Created by apple on 16/05/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift


class LoginWithEmailVC: UIViewController {
    
    @IBOutlet weak var loginWithLabel: UILabel!
    @IBOutlet weak var emailAndPasswordLable: UILabel!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var emailSeparator: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var eyeButtonOutlet: UIButton!
    @IBOutlet weak var passwordViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordMessage: UILabel!
    @IBOutlet weak var passwordSeparator: UIView!
    
    let emailVM = LoginWithEmailVM()
    var isForVerifyEmail = false
    var isForgotPasswordClicked = false
    var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        didGetResponse()
         initialSetup()
        // Do any additional setup after loading the view.

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        emailTF.becomeFirstResponder()
    }
    
    
    func initialSetup(){
        
        emailTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        passwordTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        forgotPasswordBtn.setTitleColor(Colors.AppBaseColor, for: .normal)
        emailTF.textColor = Colors.PrimaryText
        emailTF.placeholder = StringConstants.enterYourEmail()
        passwordTF.placeholder = StringConstants.enterYourPassword()
        passwordMessage.text = StringConstants.minPasswordLengthMessage()
        passwordMessage.textColor = Colors.Red
        
        forgotPasswordBtn.setTitle(StringConstants.forgotPassword(), for: .normal)
        backBtn.tintColor = Colors.AppBaseColor

        emailAndPasswordLable.text = StringConstants.emailAndPassword()
        emailAndPasswordLable.textColor = Colors.AppBaseColor
        loginWithLabel.text = StringConstants.loginWith()
       // passwordView.isHidden = false
        passwordViewHeightConstraint.constant = 98
        passwordSeparator.isHidden =  false
        forgotPasswordBtn.isHidden = false
        
        
    }
    
    @IBAction func togglePasswordSecurity(sender: UIButton) {
        self.passwordTF.isSecureTextEntry = !self.passwordTF.isSecureTextEntry
        
        if passwordTF.isSecureTextEntry {
            eyeButtonOutlet.setImage(#imageLiteral(resourceName: "Hide"), for: .normal)
        } else {
            eyeButtonOutlet.setImage(#imageLiteral(resourceName: "View"), for: .normal)
        }

    }
    
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        
        loginWithLabel.text = "Enter Your"
        emailAndPasswordLable.text = StringConstants.email()
        isForVerifyEmail = true
      //  passwordView.isHidden = true
        
        UIView.animate(withDuration: 0.3, animations: {
            self.passwordViewHeightConstraint.constant = 0
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
        
        if let emailId = emailTF.text {
            self.emailVM.emailIdExistVerification(emailId: emailId)
        }
        emailVM.locAuth.email = emailTF.text!
        
    }

     @IBAction func backBtnAction(_ sender: Any) {
        self.view.endEditing(true)
        if isForVerifyEmail{
            UIView.animate(withDuration: 0.3, animations: {
                self.passwordViewHeightConstraint.constant = 98
                self.view.updateConstraints()
                self.view.layoutIfNeeded()
            })
            
            isForVerifyEmail = false
        }else{
            self.navigationController?.popViewController(animated: true)

     }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        self.view.endEditing(true)
        
    }
    
    
    func gotoEmailVC(){
        let emailVC = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:"LoginWithEmail") as! LoginWithEmailVC
        emailVC.isForVerifyEmail = true
        emailVC.emailVM.locAuth = self.emailVM.locAuth
        self.navigationController?.pushViewController(emailVC, animated: true)
        }
    
    func gotoEmailVerificationVC(){
        let emailVerifyOtpVC = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:"EmailVerificationVC") as! EmailVerificationVC
      //  emailVerifyOtpVC.isForVerifyEmail = true
        emailVerifyOtpVC.emailVerifyVM.locAuth = self.emailVM.locAuth
        self.navigationController?.pushViewController(emailVerifyOtpVC, animated: true)
    }
    
    
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destination.
//        // Pass the selected object to the new view controller.
//    }
    
    


}
extension LoginWithEmailVC: UITextFieldDelegate{
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//
//        switch textField {
//        case emailTF:
//            emailTF.becomeFirstResponder()
//        default:
//            passwordTF.becomeFirstResponder()
//        }
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case emailTF:
            passwordTF.becomeFirstResponder()
            return true
        default:
            passwordTF.resignFirstResponder()
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
       
        
    }
    
    @objc func textFieldDidChange(_ textField : UITextField) {
        
        updateNextButtonStatus(textField: textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case emailTF:
            passwordTF.becomeFirstResponder()
        default:
            passwordTF.resignFirstResponder()
        }
    }
    
    func updateNextButtonStatus(textField:UITextField) {
        
        switch textField {
        case emailTF:
            if Helper.isValidEmail(text: textField.text!) {
                                self.nextBtn.isEnabled = true
                                self.emailVM.locAuth.email = self.emailTF.text!
                                self.nextBtn.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
            }else {
                                self.nextBtn.isEnabled = false
                                self.nextBtn.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
            }
            break
        default :
            if (textField.text?.sorted().count)! != 0 && (textField.text?.count)! >= 6 {
                 self.emailVM.locAuth.password = self.passwordTF.text!
                nextBtn.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
                nextBtn.isEnabled = true

                
            }
            else{
                nextBtn.isEnabled = false
                nextBtn.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
            }
        }
    }
    
}

extension LoginWithEmailVC {
    
    func didGetResponse(){
        if !emailVM.loginWithEmailVM_resp.hasObservers {
            emailVM.loginWithEmailVM_resp.subscribe(onNext: { success in
                switch success.0 {
                case LoginWithEmailVM.ResponseType.emailLoginResp:
                    if success.1 {
                        Helper.hideProgressIndicator()
                        Helper.makeTabbarAsRootVc(forVc: self)
                    }else{
//
                        Helper.showAlertWithMessage(withTitle: "Message", message: self.emailVM.message, onViewController: self)
                    }
                case LoginWithEmailVM.ResponseType.emaiIdExistVerify:
                    if success.1 {
                        if self.isForVerifyEmail {
                            self.emailVM.postForgotPassword(emailId: self.emailTF.text!)
                        }else{
                        self.emailVM.loginWithEmail()
                        print("Email Id Registered")
                        }
                    }else{
                        
                        Helper.showAlertWithMessage(withTitle: "Message", message: self.emailVM.message, onViewController: self)
                    }
                case LoginWithEmailVM.ResponseType.forgotPwdResp:
                    if success.1 {
                        self.gotoEmailVerificationVC()
                    }else{
                         Helper.showAlertWithMessage(withTitle: "Message", message: self.emailVM.message, onViewController: self)
                    }
                }
            }).disposed(by: disposeBag)
        }
    }
    
}
