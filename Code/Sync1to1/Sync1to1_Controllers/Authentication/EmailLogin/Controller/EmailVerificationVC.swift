//
//  EmailVerificationVC.swift
//  Datum
//
//  Created by apple on 16/05/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
struct Constant {
    static var totalTime = 59.0
    static let elapsingTime = 1.0
    static let authKeyidentifier = "authVerificationID"
    
}
//LoginWithEmail
class EmailVerificationVC: UIViewController {
    
    @IBOutlet weak var verfyCodeLable: UILabel!
    @IBOutlet weak var verificationCodeSentTo: UILabel!
    @IBOutlet weak var emailLAbel: UILabel!
    @IBOutlet weak var otpTF: UITextField!
    @IBOutlet weak var nextBtn: UIButton!
    
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var resendButtonOutlet: UIButton!
    
   // let phNum_response = PublishSubject<responseType>()
    let emailVerifyVM = EmailVerifyVM()
    var timer:Timer!
    var userData:User!
    var remainingTime:Double!
    var otpVerificationForSignUp:Bool = false
    var disposeBag = DisposeBag()
    let emailVM = LoginWithEmailVM()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        remainingTime = Constant.totalTime
        activateTimer()
        didGetResponse()
        emailLAbel.text = emailVerifyVM.locAuth.email
        initialSetup()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        removeObserver()
    }
    
     func initialSetup(){
        backBtn.tintColor = Colors.AppBaseColor
        otpTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        otpTF.textColor = Colors.PrimaryText
        otpTF.placeholder = StringConstants.verificationCode()
        
        verfyCodeLable.text = StringConstants.verificationCode()
        verificationCodeSentTo.text = StringConstants.verificationCodeSent()
        
    }
    
    
    @IBAction func nextBtnAction(_ sender: Any) {
        
        self.emailVerifyVM.locAuth.email = emailLAbel.text!
        self.emailVerifyVM.locAuth.otp = self.otpTF.text!
        self.emailVerifyVM.emailIdOtpVerification()
    }
    
  
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func resendCodeAction(_ sender: Any) {
        self.resendButtonOutlet.isHidden = true
        self.timerLabel.isHidden = false
        activateTimer()
        self.emailVM.postForgotPassword(emailId: self.emailVerifyVM.locAuth.email)
        
    }
    
    func goToConfirmPasswordVC(){
        let passwordVC = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:"PasswordVC") as! PasswordVC
        passwordVC.isForNewPassword = true
        passwordVC.passwordVM.locAuth = self.emailVerifyVM.locAuth
         passwordVC.passwordVM.locAuth.email = emailLAbel.text!
        self.navigationController?.pushViewController(passwordVC, animated: true)
    }
    
    //MARK:- CLASS LOCAL METHODS.
    
    /// timer for request new password.
    func activateTimer()
    {
        remainingTime = Constant.totalTime
        if (timer != nil)
        {
            timer.invalidate()
        }
        timer = Timer.scheduledTimer(timeInterval: Constant.elapsingTime, target: self, selector: #selector(countDown), userInfo: nil, repeats: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    /// decreasing time.
    @objc func countDown()
    {
        if remainingTime > 0{
            remainingTime = remainingTime - Constant.elapsingTime
            self.resendButtonOutlet.isHidden = true
            self.timerLabel.isHidden = false
            timerLabel.text = "00:\(Int(remainingTime))"
            timerLabel.textColor = Helper.getUIColor(color: "484848")
            timerLabel.font = UIFont(name: timerLabel.font.fontName, size: 12)
            Constant.totalTime = remainingTime
        }
        else {
            timer.invalidate()
            Constant.totalTime = 59.0
            //timeLabel.shakeView()
            timerLabel.text = StringConstants.resendCode()
            self.resendButtonOutlet.isHidden = false
            self.timerLabel.isHidden = true
            resendButtonOutlet.setTitle(StringConstants.resendCode(), for: .normal)
            timerLabel.textColor = Colors.AppBaseColor
            timerLabel.font = UIFont(name: timerLabel.font.fontName, size: 20)
            
        }
        
    }
    
    
}


extension EmailVerificationVC: UITextFieldDelegate{
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        otpTF.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       // otpTF.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    @objc func textFieldDidChange(_ textField : UITextField) {
        if otpTF.text?.sorted().count == 4 {
            self.view.endEditing(true)
            nextBtn.isEnabled = true
            nextBtn.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
            
        }else {
            nextBtn.isEnabled = false
            nextBtn.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
        }
    }

    
}

extension EmailVerificationVC {
    
    func didGetResponse(){
        emailVerifyVM.emailVerifyVM_resp.subscribe(onNext: {success in
            if success {
                 self.goToConfirmPasswordVC()
            }else{
                Helper.showAlertWithMessage(withTitle: "Message", message: self.emailVerifyVM.message, onViewController: self)
            }
        }, onError: { (error) in
            print(error)
        }).disposed(by: emailVerifyVM.disposeBag)
        
    }
    
    
}
