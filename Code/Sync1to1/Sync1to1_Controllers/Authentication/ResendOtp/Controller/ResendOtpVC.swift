//
//  ResendOtpVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 28/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class ResendOtpVC: UIViewController {
    
    struct Constant {
        static var totalTime = 25.0
        static let elapsingTime = 1.0
        static let authKeyidentifier = "authVerificationID"
        
    }
    
    
    @IBOutlet weak var screenTitle: UILabel!
    @IBOutlet weak var secondTitle: UILabel!
    @IBOutlet weak var mobileIcon: UIImageView!
    @IBOutlet weak var smsInboxIcon: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var resendBtn: UIButton!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var sentMsgViewBottomConstrint: NSLayoutConstraint!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var phNumberBtn: UIButton!
    @IBOutlet weak var verifyYourMobileNum: UILabel!
    @IBOutlet weak var checkYourSMSBox: UILabel!
    
    var locAuth = LocalAuthentication(data: [:])
    let phoneNumVM = PhoneNumViewModel()
    var timer:Timer!
    var userData:User!
    var remainingTime:Double!
    var otpVerificationForSignUp:Bool = false
    var phoeNumberForResetPasswod:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenTitle.text = StringConstants.ifYouHaventReceivCode()
        secondTitle.text = StringConstants.code()
        remainingTime = Constant.totalTime
        secondTitle.textColor = Colors.AppBaseColor
        phNumberBtn.setTitleColor(Colors.AppBaseColor, for: .normal)
        verifyYourMobileNum.text = StringConstants.verifyYourNumber()
        checkYourSMSBox.text = StringConstants.checkYourSMSInbox()
        activateTimer()
        didGetResponse()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // self.activateTimer()
        self.sentMsgViewBottomConstrint.constant = 1000
        imageWidth.constant = 0
        mobileNumber.text = locAuth.countryCode + locAuth.phoneNumber
    }
    
   
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func resendCodeAction(_ sender: Any) {
        Helper.showProgressIndicator(withMessage: "")
        self.sentMsgViewBottomConstrint.constant = 0
        imageWidth.constant = 0
        phoneNumVM.locAuth = self.locAuth
        phoneNumVM.requestForPhoneNumberCheck()
    
    }
    
    
    @IBAction func phoneNumberAction(_ sender: Any) {
        let controller = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: StoryBoardIdentifier.phoneNumberSI) as! PhoneNumberViewController
        controller.phoneNumVM.isFromResendOtp = true
        controller.phoneNumVM.locAuth = self.locAuth
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    //MARK:- CLASS LOCAL METHODS.
    
    /// timer for request new password.
    func activateTimer()
    {
        remainingTime = Constant.totalTime
        if (timer != nil)
        {
            timer.invalidate()
        }
        timer = Timer.scheduledTimer(timeInterval: Constant.elapsingTime, target: self, selector: #selector(countDown), userInfo: nil, repeats: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    /// decreasing time.
   @objc func countDown()
    {
        if remainingTime > 0{
            remainingTime = remainingTime - Constant.elapsingTime
            self.resendBtn.isEnabled = false
            timeLabel.text = "00:\(Int(remainingTime))"
            timeLabel.textColor = Helper.getUIColor(color: "484848")
            timeLabel.font = UIFont(name: timeLabel.font.fontName, size: 28)
            Constant.totalTime = remainingTime
        }
        else {
            timer.invalidate()
            Constant.totalTime = 25.0
            //timeLabel.shakeView()
            timeLabel.text = StringConstants.resendCode()
            self.resendBtn.isEnabled = true
            //  resendBtn.setTitle("resend code", for: .normal)
            timeLabel.textColor = Helper.getUIColor(color: "Colors.AppBaseColor")
            timeLabel.font = UIFont(name: timeLabel.font.fontName, size: 20)
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension ResendOtpVC {
    
    func didGetResponse(){
        phoneNumVM.phNum_response.subscribe(onNext: {success in
            UIView.animate(withDuration: 0.5, animations: {
                self.sentMsgViewBottomConstrint.constant = 0
                Helper.hideProgressIndicator()
                self.imageWidth.constant = 150
            })
            
        }).disposed(by: phoneNumVM.disposeBag)
    }
    
    
}
