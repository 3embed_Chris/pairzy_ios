//
//  AddMediaVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 14/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa



class AddMediaVM: NSObject {
    
    
    static var ProfImageUrl = "NA"
    var authentication = Authentication()
    let disposeBag = DisposeBag()
    let AddMediaVM_resp = PublishSubject<ResponseType>()
    var view = UIView()
    
    var locAuth = LocalAuthentication(data: [:])

    enum ResponseType:Int {
        case postProfile = 0
        case updateImage = 1
        case serverError = 2
    }
    
    
    func getCloudinaryDetails(){
        
        let apiCall = Authentication()
        
        apiCall.getCloudinaryCredential()
        apiCall.subject_response
            .subscribe(onNext: {response in
               self.handleCloudinaryDetailsResponse(responseModel: response)
            }, onError: {error in
                Helper.hideProgressIndicator()
                Helper.showAlertMessageViewFromTopNavBar(showOnView: self.view, message: error.localizedDescription)
                self.AddMediaVM_resp.onNext(.serverError)

            })
            .disposed(by:disposeBag)
    }
    
    
    
    func handleCloudinaryDetailsResponse(responseModel: ResponseModel){
        
        let statuscode:API.ErrorCode = API.ErrorCode(rawValue: responseModel.statusCode)!
        
        switch statuscode {
        case .Success:
             if  let data = responseModel.response["data"] as? [String:Any] {
            Helper.saveCloudinaryDetails(data: data)
             
             }
            print("yyeyrfrfc ")
        default:
            self.AddMediaVM_resp.onNext(.serverError)
            break
        }
    }
    
   
    
    func postProfileDetail(){
        
        let currentDate = locAuth.dateOfBirth           //Date()
        let currentTimeStamp = currentDate.millisecondsSince1970
        locAuth.profilePic = AddMediaVM.ProfImageUrl
        var pushToken = ServiceInfo.constantPushToken
        if let token = UserDefaults.standard.value(forKey: SyUserdefaultKeys.kNSUPushNotificationTokenKey) as? String
        {
            pushToken = token
        }
        var appVersion = "1.0"
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            appVersion = version
        }
        
        var voipPushToken = ""
        
        if let voipPush = UserDefaults.standard.value(forKey:"callPushToken") as? String {
            voipPushToken = voipPush
        }
        
        var apnsPushToken = ""
        if let apnsPush = UserDefaults.standard.value(forKey:"deviceTokenString") as? String {
            apnsPushToken = apnsPush
        }
        
        let jpgURL = URL(string:locAuth.profileVideo)?
            .deletingPathExtension()
            .appendingPathExtension("jpg")

        let url = jpgURL?.absoluteString
        
        let params = [ServiceInfo.CountCode: locAuth.countryCode,
                      ServiceInfo.phoneNumber: locAuth.countryCode + locAuth.phoneNumber,
                      ServiceInfo.OTP: locAuth.otp,
                      ServiceInfo.email: locAuth.email,
                      //ServiceInfo.password: locAuth.password,
                      ServiceInfo.Fname: locAuth.name,
                      ServiceInfo.DOB: currentTimeStamp,
                      ServiceInfo.Gender: locAuth.gender,
                      ServiceInfo.profilePic: locAuth.profilePic,
                      ServiceInfo.profileVideo: locAuth.profileVideo,
                      ServiceInfo.deviceOs : UIDevice.current.systemVersion,
                      ServiceInfo.deviceId:UIDevice.current.identifierForVendor?.uuidString as AnyObject,
                      ServiceInfo.deviceModelKey:Helper.DeviceModel,
                      ServiceInfo.DeviceType:"1" as AnyObject,
                      ServiceInfo.appVersion           :appVersion,
                      ServiceInfo.PushToken:pushToken as AnyObject,
                      ServiceInfo.apnsPush: apnsPushToken,
                      ServiceInfo.voipiospush: voipPushToken,
                      ServiceInfo.profileVideoThumbnail: url ?? "",
                      ServiceInfo.deviceMakeKey:UIDevice.current.name as AnyObject] as [String:Any]
        let requestData = RequestModel().signUpRequestDetails(signupDetails:params)
        
        let apiCall = Authentication()
        apiCall.requestToPostProfile(profileDetail: requestData)
        apiCall.subject_response
            .subscribe(onNext: {response in
                self.handlePostProfileResponse(responseModel: response)
            }, onError: {error in
                Helper.hideProgressIndicator()
                Helper.showAlertMessageViewFromTopNavBar(showOnView: self.view, message: error.localizedDescription )
                self.AddMediaVM_resp.onNext(.serverError)
            })
            .disposed(by:disposeBag)
    }

    
    
    //handle the post profile response
    func handlePostProfileResponse(responseModel: ResponseModel){
        let statuscode:API.ErrorCode = API.ErrorCode(rawValue: responseModel.statusCode)!
        
        switch statuscode {
        case .Success:
            
            if  let data = responseModel.response["data"] as? [String:Any] {
               let userdata = User.init(userDataDictionary: (data))
                    if let sessionToken = data["token"] as? String{
                        UserLoginDataHelper.saveUserToken(token:sessionToken)
                    }
                //   Saving userId In User Default After Login Response Came
                if let userId = data["_id"] as? String {
                    UserLoginDataHelper.saveUserId(userId: userId)
                }
                if let userId = data["userId"] as? String {
                    UserLoginDataHelper.saveUserId(userId: userId)
                }
                    
                    //  saving userdata in core data.
                    Database.shared.addUserdataToDatabase(withUserdata:userdata)
                
            }
            //updating signupData in couchDb.
            let couchbaseObj = Couchbase.sharedInstance
            let usersDocVMObject = UsersDocumentViewModel(couchbase: couchbaseObj)
            usersDocVMObject.updateUserDoc(data: responseModel.response["data"] as! [String : Any], withLoginType: "1")
            
            
            
            MQTT.sharedInstance.disconnectMQTTConnection()
            if let userID = Helper.getMQTTID() {
                MQTT.sharedInstance.connect(withClientId: userID)
            }
            
            //subscribing For Push notification topic.
            let appDelegateObj = UIApplication.shared.delegate! as! AppDelegate
            appDelegateObj.subscribeForPushNotificatiionTopic()
            
            Helper.showCoinsDeductPopup(flag: true, popUpFor: .forAll)
        
            self.AddMediaVM_resp.onNext(.postProfile)
            
            break
        default:
            self.AddMediaVM_resp.onNext(.serverError)
            break
        }
    }
}


struct Env {
    
    private static let production : Bool = {
        #if DEBUG
        print("DEBUG")
        return false
        #elseif ADHOC
        print("ADHOC")
        return false
        #else
        print("PRODUCTION")
        return true
        #endif
    }()
    
    static func isProduction () -> Bool {
        return self.production
    }
    
}
