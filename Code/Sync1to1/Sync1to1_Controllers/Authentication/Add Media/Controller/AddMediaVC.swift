//
//  AddMediaVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 21/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import VGPlayer
import AVKit
import Kingfisher

protocol AddMediaVCDelegate: class {
    func didTapBack()
}
class AddMediaVC: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addMediaButtonOutlet: UIButton!
    @IBOutlet weak var mediaBackGroundView: UIView!
    @IBOutlet weak var skipButtonOutlet: UIButton!
    @IBOutlet weak var changeButtonOutlet: UIButton!
    @IBOutlet weak var addMediaLabel: UILabel!
    @IBOutlet weak var bottomViewConstarint: NSLayoutConstraint!
    @IBOutlet weak var mediaTopView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var playVideoButton: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var myBestLabel: UILabel!
    
    var localplayer : VGPlayer?
    var recordedVideoUrlPath = ""
    
    enum mediaType {
        case image
        case video
    }
    
    var mediaFor = mediaType.image
    var isFacebookImage = false       // if true dont upload
    var pickerObj:ImagePickerModel? = ImagePickerModel.sharedImagePicker
    let addMediaVM = AddMediaVM()
    var isForFacebook = false
   weak var delegate:AddMediaVCDelegate?

    let captureSession = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer?
    var stillImageOutput : AVCapturePhotoOutput?
    let joinUsVM = JoinUSVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inialSetup()
        setupUI()
      //  didGetPreferences()
        didGetResponse()
        updateFbImage()
        proceedWithCameraAccess()
        addMediaVM.getCloudinaryDetails()
        
        var nameOfObserVer = NSNotification.Name(rawValue: "SignUpProfileImage")
        NotificationCenter.default.addObserver(self, selector: #selector(postSignUpProfileImage), name: nameOfObserVer, object: nil)
        
        nameOfObserVer = NSNotification.Name(rawValue: "SignUpProfileVideo")
        NotificationCenter.default.addObserver(self, selector: #selector(postSignUpProfileVideo), name: nameOfObserVer, object: nil)
        let audioSession = AVAudioSession.sharedInstance()
        if audioSession.isOtherAudioPlaying {
            _ = try? audioSession.setCategory(AVAudioSessionCategoryAmbient, with: AVAudioSessionCategoryOptions.mixWithOthers)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
     //  stopVideoPlay()
        let audioSession = AVAudioSession.sharedInstance()
        if audioSession.isOtherAudioPlaying {
            _ = try? audioSession.setCategory(AVAudioSessionCategoryAmbient, with: AVAudioSessionCategoryOptions.mixWithOthers)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nextBtn.isUserInteractionEnabled = true
        skipButtonOutlet.isUserInteractionEnabled = true
        addMediaVM.view = self.view
         pickerObj?.delegate = self
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func stopVideoPlay(){
        if((localplayer) != nil) {
            localplayer?.pause()
        }
    }
    
    func updateFbImage(){
        if isForFacebook {
            if mediaFor == .image {
                if !addMediaVM.locAuth.profilePic.isEmpty {
                    let url = URL(string: addMediaVM.locAuth.profilePic)
                    let tempImageView = UIImageView()
                    self.profileImage.kf.indicatorType = .activity
                    self.profileImage.kf.indicator?.startAnimatingView()
                    profileImage.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "user_default_image"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
                        self.profileImage.kf.indicator?.stopAnimatingView()
                    })
          self.didPickImage(selectedImage: profileImage.image ?? UIImage() , tag:1, videoUrl:"" ,sourceType:0, cameraDeviceMode: 0 )
                }
            }
        }
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
     //   stopVideoPlay()
        nextBtn.isUserInteractionEnabled = false
        switch mediaFor {
        case .image:
            skipButtonOutlet.isHidden = true
            if isForFacebook {
                if !isFacebookImage {
                    uploadImageToCloudinary(image: profileImage.image!)
                }else {
                    self.imageUploadResponseHandle(url: self.addMediaVM.locAuth.profilePic)
                }
            }else {
                uploadImageToCloudinary(image: profileImage.image!)
            }
            
        case .video:
            skipButtonOutlet.isHidden = false
            if(recordedVideoUrlPath != "") {
                if let recordeVideo = getVideoUrl()  {
                    uploadVideoToCloudinary(video:recordeVideo)
                }
            }
            //recordedVideoUrlPath
        }
    }
    
    @IBAction func skipButtonAction(_ sender: Any) {
         skipButtonOutlet.isUserInteractionEnabled = false
     //   stopVideoPlay()
        pushToEULAVc()
        
//        if self.isForFacebook {
//            let socialLoginModel:SocialLogin = SocialLogin.shared
//            let requestData:RequestModel = RequestModel().faceBookLoginDetail(fbDetails: self.addMediaVM.locAuth)
//            socialLoginModel.facebookLoginAPI(data: requestData)
//        }else {
//            self.addMediaVM.postProfileDetail()
//        }
    }
    
    @IBAction func addMediaButtonAction(_ sender: Any) {
        
        self.perform(#selector(self.setProfilePic), with: self, afterDelay: 0.2)
       
    }
    
    @IBAction func videoPlayButtonAction(_ sender: Any) {
        if(self.localplayer?.state == .playFinished) {
            self.localplayer?.displayView.replayButton.sendActions(for: .touchUpInside)
        } else {
            self.localplayer?.displayView.playButtion.sendActions(for: .touchUpInside)
        }
    }
    
    @objc func setProfilePic(){
        var forVideo = false
        
        if(self.mediaFor == .video) {
            forVideo = true
        }
        self.pickerObj?.setProfilePic(controller: self , tag: 0, pickerForVideo:forVideo)
    }
    
    @objc func postSignUpProfileImage(_ notification: NSNotification){
            let userInfo = notification.userInfo as! [String: Any]
            guard var chosen = userInfo["image"]  as? UIImage else { return }
            if(chosen.size.width > 499){
                chosen = chosen.resizeImage(image: chosen, targetSize: CGSize(width: 500, height: 500))
            }
            chosen = chosen.compressTo(0.5)!
            //removed = 1
            // chosen = fixOrientation(img: chosen)
//            self.sourceType = picker.sourceType.rawValue
//            if picker.sourceType.rawValue == 1 {
//                self.cameraDeviceMode = picker.cameraDevice.rawValue
//
//            }
            //pickerController.dismiss(animated: true, completion: nil)
            let story = UIStoryboard.init(name: "Chat", bundle: nil)
            let preview =  story.instantiateViewController(withIdentifier: "ImagePickerPreview") as? ImagePickepreviewController
            guard let imageToSend = chosen as? UIImage else { return }
            preview?.imageArray = [imageToSend]
            preview?.delegate = self
            preview?.isComingFromCamera = true
            self.present(preview!, animated: true, completion: nil)
            
        }
    @objc func postSignUpProfileVideo(_ notification: NSNotification){
        
        let filename = getDocumentsDirectory().appendingPathComponent(Helper.getCurrentTimeStamp())
        let userInfo = notification.userInfo as! [String: Any]
        if let videoURL = userInfo["video"] as? NSURL {
            
            if var image = thumbnail(sourceURL: videoURL) as? UIImage{
                image = fixOrientation(img: image)
                
                //                    if picker.sourceType.rawValue == 1 { //
                //                        self.cameraDeviceMode = picker.cameraDevice.rawValue
                //
                //                        if picker.cameraDevice == .front {
                //                            image = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: .upMirrored)
                //                        }
                //                    }
                
                if let data = UIImagePNGRepresentation(image) {
                    try? data.write(to: filename)
                }
                
                //                   removed = 1
                //
                //                    delegate?.didPickImage(selectedImage: image, tag: removed, videoUrl: videoURL.absoluteString!, sourceType: self.sourceType,cameraDeviceMode: self.cameraDeviceMode )
                //                    pickerController.dismiss(animated: true, completion: nil)
                
                if(mediaFor == mediaType.video) {
                    guard let videoPath = videoURL.absoluteString else {
                        return
                    }
                    profileImage.image = image
                    addMediaButtonOutlet.isHidden = true
                    addMediaLabel.isHidden = true
                    changeButtonOutlet.isHidden = false
                    nextBtn.isEnabled = true
                   // URL(string:videoUrl)
                    if videoPath.count > 0 {
                        videoView.isHidden = false
                        playVideoButton.isHidden = false
                        recordedVideoUrlPath = videoPath
                        showVideoForUrl(videoUrl: videoPath)
                        profileImage.isHidden = true
                    }
                    
                }
                
            }
        }
        
    }

    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        var newSize: CGSize
        if widthRatio > heightRatio {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }

    
    //fetching local documentry.
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
    
    func thumbnail(sourceURL:NSURL) -> UIImage {
        let asset = AVAsset(url: sourceURL as URL)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        let time = CMTime(seconds: 1, preferredTimescale: 1)
        do {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            
            let image = UIImage(cgImage: imageRef)
            let imgData = NSData(data: UIImageJPEGRepresentation((image), 0.2)!)
            let compressedImage = UIImage(data: imgData as Data)
            print(imgData.length/1024)
            return compressedImage!
        } catch {
            return #imageLiteral(resourceName: "play")
        }
    }
    func proceedWithCameraAccess(){
        // handler in .requestAccess is needed to process user's answer to our request
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { success in
            if success { // if request is granted (success is true)
                DispatchQueue.main.async {
                print("true")
                    
                }
            } else { // if request is denied (success is false)
                // Create Alert
                let alert = UIAlertController(title: StringConstants.camera(), message: StringConstants.cameraNeccesary(), preferredStyle: .alert)
                
                // Add "OK" Button to alert, pressing it will bring you to the settings app
                alert.addAction(UIAlertAction(title: StringConstants.ok(), style: .default, handler: { action in
                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                }))
                // Show the alert with animation
                self.present(alert, animated: true)
            }
        }
    }
    
    
    
    func showVideoForUrl(videoUrl:String) {
        if(videoUrl.count > 5) {
            
            let url = URL(string:videoUrl)
            if url != nil {
                localplayer = VGPlayer(URL: url!)
            }
            
            localplayer?.delegate = self
            videoView.addSubview((localplayer?.displayView)!)
            localplayer?.displayView.bringSubview(toFront:(localplayer?.displayView.playButtion)!)
            localplayer?.displayView.clipsToBounds = true
            videoView.bringSubview(toFront:playVideoButton)
            localplayer?.backgroundMode = .proceed
            localplayer?.displayView.backgroundColor = UIColor.white
            localplayer?.displayView.delegate = self
            localplayer?.displayView.snp.makeConstraints { [weak self] (make) in
                guard let strongSelf = self?.videoView else { return }
                make.edges.equalTo(strongSelf)
            }
        }
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        pickerObj = nil
        self.mediaFor = mediaType.image
        delegate?.didTapBack()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeButtonAction(_ sender: Any) {
        var forVideo = false
        if(mediaFor == .video) {
            forVideo = true
        }
        pickerObj?.setProfilePic(controller: self , tag: 1,pickerForVideo:forVideo)
     //   stopVideoPlay()
    }
        
    func uploadImageToCloudinary(image: UIImage) {
        
        let lang = Helper.getCludinaryDetails()
        
        let cloudinary = CLDCloudinary(configuration: CLDConfiguration(cloudName:lang["cloudName"] as! String
            , secure: true))
        
        let params = CLDUploadRequestParams()
        params.setResourceType(CLDUrlResourceType.image)
        let data = UIImageJPEGRepresentation(image, 1.0)
        let timestamp = Helper.getCurrentTimeStamp()
        params.setPublicId("\(timestamp)")
        Helper.showProgressIndicator(withMessage: StringConstants.Uploading())
        cloudinary.createUploader().upload(data: data!, uploadPreset: lang["uploadPreset"] as! String, params: params, progress:{ (progress) in
            
        }, completionHandler: { (result, error) in
            Helper.hideProgressIndicator()
            DispatchQueue.main.async {
                if error != nil {
                    print(error)
                    
                } else {
                    if let result = result{
                        let uploadedVideoUrl = result.resultJson["url"] as? String
                        self.imageUploadResponseHandle(url: uploadedVideoUrl ?? "")
                    }
                }
            }
        })
    }
 
    func imageUploadResponseHandle(url: String){
        self.addMediaVM.locAuth.profilePic = url
        AddMediaVM.ProfImageUrl = url
        let dbObj = Database.shared
        dbObj.userDataObj.profilePictureURL = url
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        self.pushVCToMediaVc()
        
    }
    
    
    func getVideoUrl() -> URL? {
        let asset = self.localplayer?.player?.currentItem?.asset
        if asset == nil {
            return nil
        }
        if let urlAsset = asset as? AVURLAsset {
            return urlAsset.url
        }
        return nil
    }
    
    func uploadVideoToCloudinary(video: URL) {  //deu1yq6y6
        
        let lang = Helper.getCludinaryDetails()
        let cloudinary = CLDCloudinary(configuration: CLDConfiguration(cloudName: lang["cloudName"] as! String, secure: true))
        //self.uploadingProgress.setProgress(uploadedProgress, animated: true)
        let params = CLDUploadRequestParams()
        params.setResourceType(CLDUrlResourceType.video)
        let timestamp = Helper.getCurrentTimeStamp()
        params.setPublicId("\(timestamp)")
       Helper.showProgressIndicator(withMessage: StringConstants.Uploading())
        do {
            cloudinary.createUploader().upload(url: video, uploadPreset: lang["uploadPreset"] as! String, params: params, progress: { (progress) in
                
                
            }, completionHandler: { (result, error) in
                  Helper.hideProgressIndicator()
                DispatchQueue.main.async {
                    if error != nil {
                        print("failed to upload")
                    } else {
                        if let result = result{
                            var uploadedVideoUrl = result.resultJson["url"] as? String
                            uploadedVideoUrl = uploadedVideoUrl?.replace(target: "video/upload", withString: "video/upload/q_60")
                            
                            self.addMediaVM.locAuth.profileVideo = uploadedVideoUrl ?? ""
                            self.pushToEULAVc()
                            
//                            if self.isForFacebook {
//                                    let socialLoginModel:SocialLogin = SocialLogin.shared
//                                    let requestData:RequestModel = RequestModel().faceBookLoginDetail(fbDetails: self.addMediaVM.locAuth)
//                                    socialLoginModel.facebookLoginAPI(data: requestData)
//
//                            }else {
//                              self.addMediaVM.postProfileDetail()
//                            }
                            
                            if UserLoginDataHelper.hasToken() {
                              //  self.addMediaVM.updateVideo(video:uploadedVideoUrl!)
                            }
                       
                        }
                    }
                }
            })
        }
    }
 
}
extension AddMediaVC {
    
    
    /// observable for add media view model
    func didGetResponse(){
        addMediaVM.AddMediaVM_resp.subscribe(onNext: { success in
           
            if success == AddMediaVM.ResponseType.serverError {
                self.nextBtn.isUserInteractionEnabled = true
                self.skipButtonOutlet.isUserInteractionEnabled = true
            }else{
               // self.pushToJoinUsVc()
                
            }
            
            
        }).disposed(by: addMediaVM.disposeBag)
    }
    
    
   
    func showPopup(message: String){
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let tryAgainView = TryAgainViewC(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        tryAgainView.loadPopUpView(title: message)
       // tryAgainView.delegate = self
        window.addSubview(tryAgainView)
        window.makeKeyAndVisible()
    }
    
    
    
}


extension AddMediaVC: ImagePickerModelDelegate {
    func didPickImage(selectedImage: UIImage , tag:Int, videoUrl:String ,sourceType:Int, cameraDeviceMode: Int ) {
        if isForFacebook {
            let fbprofile = Helper.getSocialProfile()
            let url = URL(string: fbprofile.profilePic )
            let tempImageView = UIImageView()
            tempImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "user_default_image"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
            
            if selectedImage.isEqual( tempImageView.image) {
                isFacebookImage = true
            }else {
                isFacebookImage = false
            }
        }
       
        if tag == 0 {
            addMediaButtonOutlet.isHidden = false
            addMediaLabel.isHidden = false
            changeButtonOutlet.isHidden = true
            nextBtn.isEnabled = false
            profileImage.image = UIImage()
            
            if(mediaFor == mediaType.video) {
                profileImage.isHidden = false
            //    stopVideoPlay()
                videoView.isHidden = true
                playVideoButton.isHidden = true
                recordedVideoUrlPath = ""
            }
        }
        else {
            profileImage.image = selectedImage
            addMediaButtonOutlet.isHidden = true
            addMediaLabel.isHidden = true
            changeButtonOutlet.isHidden = false
            nextBtn.isEnabled = true

            if(mediaFor == mediaType.video) {
                if videoUrl.count > 0 {
                    videoView.isHidden = false
                    playVideoButton.isHidden = false
                    recordedVideoUrlPath = videoUrl
                    showVideoForUrl(videoUrl: videoUrl)
                    profileImage.isHidden = true
                }
                
            }
            
        }
   }
}
extension AddMediaVC: VGPlayerDelegate {
    func vgPlayer(_ player: VGPlayer, playerFailed error: VGPlayerError) {
        print(error)
    }
    
    func vgPlayer(_ player: VGPlayer, stateDidChange state: VGPlayerState) {
        print("player State ",state)
        if (player.state == .playing) {
            self.playVideoButton.isSelected = true
        } else {
            self.playVideoButton.isSelected = false
        }
    }
    
    func vgPlayer(_ player: VGPlayer, bufferStateDidChange state: VGPlayerBufferstate) {
        print("buffer State", state)
    }
    
}

extension AddMediaVC: VGPlayerViewDelegate {
    func vgPlayerView(_ playerView: VGPlayerView, willFullscreen fullscreen: Bool) {
        
    }
    
    func vgPlayerView(didTappedClose playerView: VGPlayerView) {
        if playerView.isFullScreen {
            playerView.exitFullscreen()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func vgPlayerView(didDisplayControl playerView: VGPlayerView) {
        //        UIApplication.shared.setStatusBarHidden(!playerView.isDisplayControl, with: .fade)
    }
}


extension AddMediaVC {
    
    func didGetPreferences(){
        
        joinUsVM.joinUSVM_resp.subscribe(onNext:{ success in
            
            self.nextBtn.isUserInteractionEnabled = true
            self.joinUsVM.myPreferences = Helper.getPreference()
            if self.joinUsVM.myPreferences.count != 0{
                
                let prefTypeId = self.joinUsVM.myPreferences[0].arrayOfSubPreference[0].prefTypeId
                switch prefTypeId {
                case 1,2,10:
                    self.preferenceTableVC()
                case 5:
                    self.pushTotextFieldTypeVC()
                default:
                    print("")
                }
            }
        }).disposed(by: joinUsVM.disposeBag)
        
    }
    
    /// creates object of gende
    func preferenceTableVC() {
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.PreferenceTable) as! PreferencesTableVC
        self.navigationController?.pushViewController(userBasics, animated: true)
        
    }
    
    func pushTotextFieldTypeVC(){
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.TextFieldTypeVC) as! PreferencesTFVC
        userBasics.screenIndex = 0
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
}


extension AddMediaVC : ImagepreviewDelegate {
    
    func didCancelCliked() {
        print("cancel")
    }
    
    func didSendCliked(medias: [Any]) {
        print(medias)
        for (index, media) in medias.enumerated() {
            if index == 0 {
                updatePhoto(media: media)
            }
        }
        
        //        for (index, media) in medias.enumerated() {
        //            if index == 0 {
        //                self.uploadMedia(withMedia : media)
        //            }
        //            else {
        //                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
        //                    self.uploadMedia(withMedia : media)
        //                }
        //            }
        //        }
        // self.dismiss(animated: true, completion: nil)
    }
    
    
    func updatePhoto(media: Any){
        
        //  if let mediaObj = media as? TLPHAsset {
        //   if mediaObj.type == .photo {
        if let currentImage = media as? UIImage{
            profileImage.image = currentImage
            addMediaButtonOutlet.isHidden = true
            addMediaLabel.isHidden = true
            changeButtonOutlet.isHidden = false
            nextBtn.isEnabled = true
            
            
            
        }
        // }
        // }
    }
    
}
