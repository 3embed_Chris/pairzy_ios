//
//  AddMediaUIE.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 14/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

extension AddMediaVC:AddMediaVCDelegate {
  
    func didTapBack() {
        mediaType.image
    }
    
    func setupUI() {
        switch mediaFor {
        case .image:
            skipButtonOutlet.isHidden = true
            addMediaLabel.text = StringConstants.addYourPhoto()
           
            break
        case .video:
           
            skipButtonOutlet.isHidden = false
            addMediaLabel.text = StringConstants.addYourVideo()
            self.titleLabel.text = StringConstants.video()
        }
    }
    
    func inialSetup(){
        skipButtonOutlet.setTitle(StringConstants.skip(), for: .normal)
        changeButtonOutlet.setTitle(StringConstants.change(), for: .normal)
        // Do any additional setup after loading the view.
        self.mediaBackGroundView.transform = CGAffineTransform(rotationAngle: -0.1)
        changeButtonOutlet.isHidden =  true
        self.mediaBackGroundView.layer.borderColor = UIColor.lightGray.cgColor
        self.mediaTopView.layer.borderColor = UIColor.lightGray.cgColor
        Helper.setShadow(sender: mediaBackGroundView)
        mediaTopView.layer.borderWidth = 1
        mediaTopView.layer.borderColor = Helper.getUIColor(color: "DBDBDB").cgColor
        mediaBackGroundView.layer.borderWidth = 1
        mediaBackGroundView.layer.borderColor = Helper.getUIColor(color: "DBDBDB").cgColor
        mediaBackGroundView.transform.rotated(by: 30)
        titleLabel.textColor = Colors.AppBaseColor
        backBtn.tintColor = Colors.AppBaseColor
        myBestLabel.text = StringConstants.myBest()
    }
    
//
//    func pushToJoinUsVc() {
////        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.joinUssgSI) as! joinUsMessageVC
////        userBasics.joinUsVM.locAuth  = self.addMediaVM.locAuth
////        self.navigationController?.pushViewController(userBasics, animated: true)
//        joinUsVM.getPreferences()
//    }
//
//    func pushVCToMediaVc() {
//        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.addMediaSI) as! AddMediaVC
//        userBasics.mediaFor = .video
//        userBasics.addMediaVM.locAuth = self.addMediaVM.locAuth
//        userBasics.isForFacebook = self.isForFacebook
//        self.navigationController?.pushViewController(userBasics, animated: true)
//    }
    
    func pushToJoinUsVc() {
//        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.joinUssgSI) as! joinUsMessageVC
//        userBasics.joinUsVM.locAuth  = self.addMediaVM.locAuth
//        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
    func pushVCToMediaVc() {
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.addMediaSI) as! AddMediaVC
        userBasics.mediaFor = .video
        userBasics.addMediaVM.locAuth = self.addMediaVM.locAuth
        userBasics.isForFacebook = self.isForFacebook
        userBasics.delegate = self
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
    func pushToEULAVc() {
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.EULAViewController) as! EULAViewController
        userBasics.addMediaVM.locAuth  = self.addMediaVM.locAuth
        userBasics.isForFb = self.isForFacebook
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
}
