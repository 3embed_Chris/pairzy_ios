//
//  LocalAuthentication.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 14/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class LocalAuthentication: NSObject {
    var phoneNumber = ""
    var countryCode = ""
    var countryImage = UIImage()
    var otp = ""
    var isNewUser:Int = 1
    var email = ""
    var name  = ""
    var dateOfBirth = Date()
    var gender:Int = 0
    var profilePic = ""
    var profileVideo = ""
    var lastName = ""
    var id = ""
    var dobString = ""
    var genderString = ""
    var fullName = ""
    var password = ""
    var newPassword = ""
    var work = ""
    var job = ""
    var education = ""
    
    init(data:[String:Any]) {
        if let titleTemp = data[AppConstant.facebookConstant.FirstName] as? String{
            name                         = titleTemp
        }
        
        if let titleTemp = data[AppConstant.facebookConstant.LastName] as? String{
            lastName             = titleTemp
        }
        if let titleTemp = data[AppConstant.facebookConstant.Email] as? String{
            email                         = titleTemp
        }
        if let titleTemp = data[AppConstant.facebookConstant.Id_FB] as? String{
            id             = titleTemp
        }
        if let titleTemp = data[AppConstant.facebookConstant.Picture] as? [String:Any]{
            if let titleTemp2 = titleTemp[AppConstant.facebookConstant.Data] as? [String:Any]{
                if let titleTemp3 = titleTemp2[AppConstant.facebookConstant.URL] as? String{
                    
                    //fetching large image from Facebook Graph API instead of defalut image provided by FB.
                    
                    profilePic = "https://graph.facebook.com//"+(self.id)+"/picture?type=large"
                }
            }
        }
        if let dateOfBirth = data[AppConstant.facebookConstant.DateOfBirth] as? String {
            self.dobString = dateOfBirth
        }
        if let dateOfBirth = data[AppConstant.facebookConstant.DateOfBirth] as? String {
            self.dateOfBirth = DateHelper().getCurrentDate(forDate: dateOfBirth)!
        }
        
        if let titleTemp = data[AppConstant.facebookConstant.Gender] as? String{
            genderString                         = titleTemp
            if titleTemp == "male" {
                self.gender = 1
            }else if titleTemp == "female"{
                self.gender = 2
            }
        }
        if let titleTemp = data[AppConstant.facebookConstant.Name] as? String{
            fullName                         = titleTemp
        }
        
        if name.count == 0 {
            name = fullName
        }
        
    }
    
}
