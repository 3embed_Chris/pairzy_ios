//
//  PhoneNumberViewController.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 15/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class MobileNumberDetails {
    var phoneNumber:String = ""
    var countryCode:String = ""
    var countryImage:UIImage = UIImage()
    
    init(code:String,mobileNumber:String,countryImag:UIImage) {
        phoneNumber = mobileNumber
        countryCode = code
        countryImage = countryImag
    }
}
class PhoneNumberViewController: UIViewController{
    
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var viewForOtpSentMsg: UIView!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBOutlet weak var sentMsgViewBottomConstrint: NSLayoutConstraint!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var enterYourLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var separator: UIView!
    
    let phoneNumVM = PhoneNumViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        didGetResponse()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
        addObserver()
        nextButtonOutlet.isUserInteractionEnabled = true
        self.sentMsgViewBottomConstrint.constant = 1000
        imageWidth.constant = 0
        
        if phoneNumberTextField.text?.count != 0 {
            if Helper.isPhoneNumValid(phoneNumber: phoneNumberTextField.text!, code: countryCodeLabel.text!) {
                nextButtonOutlet.isEnabled = true
                nextButtonOutlet.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
            }
            else{
                nextButtonOutlet.isEnabled = false
                nextButtonOutlet.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
            }
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        phoneNumVM.isFromResendOtp = false
        //  self.view.endEditing(true)
        removeObserver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        phoneNumberTextField.becomeFirstResponder()
    }
    
    @IBAction func countryListButtonAction(_ sender: Any) {
        let controller : VNHCountryPicker = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: StoryBoardIdentifier.countryPickerSI) as! VNHCountryPicker
        controller.delegate = self
        let newNav = UINavigationController(rootViewController: controller)
        newNav.isNavigationBarHidden = false
        self.navigationController?.present(newNav, animated: true, completion:nil)
    }
    
    
    @IBAction func nextButtonAction(_ sender: Any) {
        
        nextButtonOutlet.isUserInteractionEnabled = false
        if (!phoneNumberTextField.text!.isPhoneNumber){
            self.view.showErrorMessage(titleMsg: StringConstants.message(), andMessage: StringConstants.InvalidPhoneNumber())
        }else {
            
            self.view.endEditing(true)
            
            
            phoneNumVM.phoneNumbObj = MobileNumberDetails.init(code: countryCodeLabel.text!, mobileNumber: phoneNumberTextField.text!, countryImag: countryImageView.image!)
            
            phoneNumVM.locAuth.countryCode = countryCodeLabel.text!
            phoneNumVM.locAuth.phoneNumber = phoneNumberTextField.text!
            phoneNumVM.locAuth.countryImage = countryImageView.image!
            
            
            if(phoneNumVM.forEditProfile) {
                
                let userData = Database.shared.fetchResults()
                let selectedValue = countryCodeLabel.text!+phoneNumberTextField.text!
                
                if userData[0].mobileNumber == selectedValue {
                    Helper.showAlertMessageViewFromTopNavBar(showOnView: self.view, message: StringConstants.phAlredyExist())
                    nextButtonOutlet.isUserInteractionEnabled = true
                    
                    return
                }
                else {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.sentMsgViewBottomConstrint.constant = 0
                        self.view.updateConstraints()
                        self.view.layoutIfNeeded()
                    })
                    
                    Helper.showProgressWithoutBackg(withMessage: "")
                    
                    let dbObj = Database.shared
                    dbObj.userDataObj.mobileNumber = selectedValue
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    phoneNumVM.requestForPhoneNumberCheck()
                }
                
                
            }else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.sentMsgViewBottomConstrint.constant = 0
                    self.view.updateConstraints()
                    self.view.layoutIfNeeded()
                })
                
                Helper.showProgressIndicator(withMessage: StringConstants.VerifyNumber())
                phoneNumVM.requestForPhoneNumberCheck()
            }
            
        }
        
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.endEditing(true)
        }){ (Bool) in
            UIView.animate(withDuration: 0.3, animations: {
                self.dismiss(animated: true, completion: nil)
            })
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}

// MARK: - Country  code Picker Delegate

extension PhoneNumberViewController : VNHCountryPickerDelegate{
    
    /// method for finish country picking.
    ///
    /// - Parameter country: gives selected country details.
    func didPickedCountry(country: VNHCounty) {
        self.countryCodeLabel.text = country.dialCode
        
        self.countryImageView.image = countryImage(code:country.code)
        
    }
    fileprivate func countryImage(code: String) -> UIImage {
        let bundle = "assets.bundle/"
        return UIImage(named: bundle + code.lowercased() + ".png") ?? UIImage()
    }
}


extension PhoneNumberViewController {
    
    func didGetResponse(){
        phoneNumVM.phNum_response.subscribe(onNext: { success in
            self.view.endEditing(true)
            UIView.animate(withDuration: 0.5, animations: {
                self.sentMsgViewBottomConstrint.constant = 0
                self.imageWidth.constant = 150
                self.view.updateConstraintsIfNeeded()
                self.view.layoutIfNeeded()
                Helper.hideProgressIndicator()
            }){ (complited) in
                self.perform(#selector(self.gotoNextSentImage), with: self, afterDelay: 0.4)
                
            }
            
        }, onError: { (error) in
            print(error)
            
        }).disposed(by: phoneNumVM.disposeBag)
    }
    
    
    @objc func gotoNextSentImage(){
        
        let controller = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: StoryBoardIdentifier.numberVerificationSI) as! VerifyCodeVC
        controller.verifyCodeVM.phoneNumbObj = self.phoneNumVM.phoneNumbObj
        if phoneNumVM.forEditProfile {
            controller.verifyCodeVM.otpVerificationForSignUp = false
        }else {
            controller.verifyCodeVM.otpVerificationForSignUp = true
        }
        controller.verifyCodeVM.locAuth = self.phoneNumVM.locAuth
        if phoneNumVM.forEditProfile {
            controller.isForEdit = true
        }else {
            controller.isForEdit = false
        }
        self.view.endEditing(true)
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    /// initialSetup
    func initialSetup(){
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            self.countryCodeLabel.text = VNHCountryPicker.dialCode(code: countryCode).dialCode
            self.countryImageView.image = VNHCountryPicker.dialCode(code: countryCode).flag
        }
        phoneNumberTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        nextButtonOutlet.isEnabled = false
        nextButtonOutlet.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
        phoneNumVM.doneBtn = self.nextButtonOutlet
        phoneNumVM.view = self.view
        phoneNumVM.topConstraint = self.sentMsgViewBottomConstrint
        closeButton.tintColor = Colors.AppBaseColor
        enterYourLabel.textColor = Colors.PrimaryText
        enterYourLabel.text = StringConstants.enterYour()
        numberLabel.text = StringConstants.number()
        phoneNumberTextField.placeholder = StringConstants.phoneNumber()
        numberLabel.textColor = Colors.AppBaseColor
        countryCodeLabel.textColor = Colors.PrimaryText
        phoneNumberTextField.tintColor = Colors.AppBaseColor
        separator.backgroundColor = Colors.SeparatorDark
        
        if phoneNumVM.isFromResendOtp {
            phoneNumberTextField.text = phoneNumVM.locAuth.phoneNumber
            countryCodeLabel.text = phoneNumVM.locAuth.countryCode
            countryImageView.image = phoneNumVM.locAuth.countryImage
            nextButtonOutlet.isEnabled = true
            nextButtonOutlet.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
        }
        
        if phoneNumVM.forEditProfile {
            let userData = Database.shared.fetchResults()
            let countryCode =  userData[0].countryCode!
            let phoneNumber = userData[0].mobileNumber!.replacingOccurrences(of: countryCode, with: "")
            let countryObj = VNHCountryPicker.dialCode(code: countryCode)
            
            phoneNumberTextField.text = phoneNumber
            countryCodeLabel.text = userData[0].countryCode
            countryImageView.image = countryObj.flag
            
            let selectedValue = countryCodeLabel.text!+phoneNumberTextField.text!
            
            
            if userData[0].mobileNumber == selectedValue {
                nextButtonOutlet.isEnabled = false
                nextButtonOutlet.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
            }
            
        }
        
    }
    
}

