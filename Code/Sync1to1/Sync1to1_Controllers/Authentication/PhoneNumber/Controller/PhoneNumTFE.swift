//
//  PhoneNumTFE.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 26/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit


// MARK: - UITextFieldDelegate
extension PhoneNumberViewController: UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        if Helper.isNumber(text: string) {
            let phoneNumber = self.phoneNumberTextField.text!
            let countryCode = self.countryCodeLabel.text!
            
            if Helper.isPhoneNumValid(phoneNumber: newString, code: countryCode) {
                nextButtonOutlet.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
               nextButtonOutlet.isEnabled = false
                
            }else{
                nextButtonOutlet.isEnabled = true
                nextButtonOutlet.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
            }
//            else{
//                if (textField.text?.count)! >= 15 {
//                    return false
//                }
//                return true
//            }
           return Helper.isPhoneNumValid(phoneNumber: phoneNumber, code: countryCode)
           
        }else {
            if range.length == 1 {
                return true
            }
            else {
                 return false
            }
            
        }

        
    }
   
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
   @objc func textFieldDidChange(_ textField : UITextField){
            let phoneNumber = self.phoneNumberTextField.text!
            let countryCode = self.countryCodeLabel.text!
            if Helper.isPhoneNumValid(phoneNumber: phoneNumber, code: countryCode) {
                nextButtonOutlet.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
                nextButtonOutlet.isEnabled = false
            }
            else{
               nextButtonOutlet.isEnabled = true
               nextButtonOutlet.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
          }
    }

    
}
extension PhoneNumberViewController {
    
    func scrollViewDidScroll(_ sender: UIScrollView) {
        // Update the page when more than 50% of the previous/next page is visible
        let movedOffset: CGFloat = sender.contentOffset.y
      

    }
    
}





