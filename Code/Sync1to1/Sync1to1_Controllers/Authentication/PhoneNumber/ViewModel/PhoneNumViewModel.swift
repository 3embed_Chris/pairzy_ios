//
//  PhoneNumViewModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 29/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PhoneNumViewModel: NSObject {
    
    enum responseType:Int{
        case phoneNum = 0
        case requestOtp = 1
        
    }
    
    var authentication = Authentication()
    let disposeBag = DisposeBag()
    let phNum_response = PublishSubject<responseType>()
    var locAuth = LocalAuthentication(data: [:])
    var phoneNumbObj:MobileNumberDetails? = nil
    var isFromResendOtp = false
    var forEditProfile = false
    
    var doneBtn:UIButton? = nil
    var view:UIView? = nil
    var topConstraint: NSLayoutConstraint? = nil
    
    func requestForPhoneNumberCheck() {
        

        let params = [ServiceInfo.phoneNumberWIthCode: locAuth.countryCode + locAuth.phoneNumber] as [String:Any]
        let requestData = RequestModel().signUpRequestDetails(signupDetails:params)

        let apiCall = Authentication()
        apiCall.requestPhoneNumberCheck(phoneData:requestData)
      
        apiCall.subject_response
            .subscribe(onNext: {response in
                self.handlePhoneNumberCheckAPIResponse(responseModel: response)
            }, onError: { (error) in
               
                Helper.showAlertMessageViewFromTopNavBar(showOnView: self.view!, message: error.localizedDescription )
                self.doneBtn?.isUserInteractionEnabled = true
                self.topConstraint?.constant = 1000
            }).disposed(by: disposeBag)

    }
    
    
    
    
    
    
    
    
    func handlePhoneNumberCheckAPIResponse(responseModel:ResponseModel) {
        
        // let statuscode = responseModel.statusCode
         let statuscode:API.ErrorCode = API.ErrorCode(rawValue: responseModel.statusCode)!
        var type = 1
        if forEditProfile {
            type = 3
        }else {
            type = 1
        }
    
       
        switch (statuscode) {
//        case .Success:
//            Helper.showAlertWithMessageOnwindow(withTitle: "", message:responseModel.response["message"] as! String )
//            break
        case .PreconditionFailed,.Success:
            
            let params = [ServiceInfo.phoneNumberWIthCode:locAuth.countryCode + locAuth.phoneNumber,
                          ServiceInfo.type: type,
                          ServiceInfo.deviceId            :UIDevice.current.identifierForVendor!.uuidString] as [String:Any]
            let requestData = RequestModel().verificationCodeRequestDetails(verifyCodeDetails:params)
            let apiCall = Authentication()
            apiCall.requestForOtp(verificationCodeData: requestData)
            apiCall.subject_response
                .subscribe(onNext: {response in
                    
                    let errNum:API.ErrorCode? = API.ErrorCode(rawValue: response.statusCode)!
                    if errNum == .Success {
                  self.phNum_response.onNext(.requestOtp)
                    }
                }, onError: { (error) in
                    print(error)
                    Helper.showAlertMessageViewFromTopNavBar(showOnView: self.view!, message: error.localizedDescription )
                    self.doneBtn?.isUserInteractionEnabled = true
                    self.topConstraint?.constant = 1000
                }).disposed(by: disposeBag)

            break
        default:
            print(responseModel.response)
        }
    }
    
}
