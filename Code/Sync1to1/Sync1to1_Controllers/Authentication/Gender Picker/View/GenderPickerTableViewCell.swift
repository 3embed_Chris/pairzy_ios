//
//  GenderPickerTableViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 21/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class GenderPickerTableViewCell: UITableViewCell {

    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var checkSymbolBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
     // checkSymbolBtn.isHidden = true
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(optionDetails: optionsModel){
        checkSymbolBtn.tintColor = Colors.AppBaseColor
        genderLabel.text = optionDetails.optionName
        if optionDetails.isOptionSelected == 1 {
            checkSymbolBtn.isHidden = false
            genderLabel.textColor = Colors.PrimaryText
        }else {
            checkSymbolBtn.isHidden = true
            genderLabel.textColor = Helper.getUIColor(color: "999999")
        }
    }
    
    func updateGender(gender: String,selected: Bool){
        checkSymbolBtn.tintColor = Colors.AppBaseColor
        genderLabel.text = gender
        if selected {
            checkSymbolBtn.isHidden = false
            genderLabel.textColor = Colors.PrimaryText
        }else {
            checkSymbolBtn.isHidden = true
            genderLabel.textColor = Helper.getUIColor(color: "999999")
        }
    }
    func updateLanguage(language:LanguageModel,selected: Bool){
        checkSymbolBtn.tintColor = Colors.AppBaseColor
        genderLabel.text = language.name
        if selected {
            checkSymbolBtn.isHidden = false
            genderLabel.textColor = Colors.PrimaryText
        }else {
            checkSymbolBtn.isHidden = true
            genderLabel.textColor = Helper.getUIColor(color: "999999")
        }
    }

}
