//
//  GenderPickerVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 30/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift


class GenderPickerVM: NSObject {
    
    //Variable and Constants
    let disposeBag = DisposeBag()
    let denderPickerVM_resp = PublishSubject<Bool>()
    var locAuth = LocalAuthentication(data: [:])
    let myPreference:[PreferencesModel] = Helper.getPreference()
    var isNotPrefer = false
    
    // var type1And2:[PreferenceModel] = []
    
    
    struct Constant {
        static let Inch_In_CM:Float = 2.54
    }
    
    enum PreferenceType:Int {
        case Gender = 0
        case Hight = 1
        case Ethnicity = 2
        case Kids = 3
        case FamilyPlans = 4
        
        case HighestLeval = 5
        case ReligiousBelief = 6
        case Politics = 7
        case Drinking = 8
        case Smoking = 9
        case Marijuana = 10
        case Drugs = 11
    }
    
    let screenWidth = UIScreen.main.bounds.size.width
    var preferenceType = PreferenceType.Gender
    var selectedIndex:IndexPath? = IndexPath(row: 0, section: 0)

    var feet = "1 ft"
    var centimeter = "0 inches"
    var centimetersValue = [String]()
    var feetValue = [String]()
    
    var dataForHeightPicker:[String]
    {
        var arrayHeights = [String]()
        for index in 60...84 {
            let cmValue = Float(index)*Constant.Inch_In_CM
            arrayHeights.append("\(HeightConverter.getFeetAndInchesFrom(centimeters:cmValue))  \(cmValue) cm")
            centimetersValue.append("\(cmValue)")
            feetValue.append("\(HeightConverter.getFeetAndInchesFrom(centimeters:cmValue))")
        }
        return arrayHeights
    }
    
    
}
