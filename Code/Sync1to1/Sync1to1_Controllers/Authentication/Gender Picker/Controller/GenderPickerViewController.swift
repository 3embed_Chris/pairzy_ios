//
//  GenderPickerViewController.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 21/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class GenderPickerViewController: UIViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var screenTitle: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var topLineWidth: NSLayoutConstraint!
    @IBOutlet weak var preferNotToSay: UILabel!
    @IBOutlet weak var preferNotToSauyBtn: UIButton!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var preferNotToSayView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    
    
    var forEditProfile = false
    var isForFacebook = false
    
    let genderPickerVM = GenderPickerVM()
    var selectedIndex:IndexPath = IndexPath(row: 0, section: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpPreferenceType()
        preferNotToSay.text = StringConstants.prefferNotTo()
        self.nextButton.isEnabled = true
        topLineView.backgroundColor = Colors.AppBaseColor
        backBtn.tintColor = Colors.AppBaseColor
        screenTitle.textColor = Colors.PrimaryText
        subTitle.textColor = Colors.AppBaseColor
        skipBtn.setTitle(StringConstants.skip(), for: .normal)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if forEditProfile {
            topLineWidth.constant = 0
        }
        if isForFacebook {
            if genderPickerVM.locAuth.genderString == "male" {
                genderPickerVM.selectedIndex = IndexPath(row: 0, section: 0)
            }else if genderPickerVM.locAuth.genderString == "female"{
                genderPickerVM.selectedIndex = IndexPath(row: 1, section: 0)
            }else {
                self.nextButton.isEnabled = false
                genderPickerVM.selectedIndex = IndexPath(row: -1, section: 0)
            }
        }
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
            if(forEditProfile) {
                let selectedValue = Gender.GenderArray[(genderPickerVM.selectedIndex?.row)!]
                let dbObj = Database.shared
                dbObj.userDataObj.gender = selectedValue
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                
                
                self.navigationController?.popViewController(animated: true)
                return
            }
            updateAuth()
            pushVCToMediaVc()
       
    }
    
    
    @IBAction func skipBtnaction(_ sender: Any) {
        
        self.nextButtonAction(UIButton())
    }
    
    func updateAuth(){
        if genderPickerVM.isNotPrefer {
            
        }else {
            if genderPickerVM.selectedIndex?.row == 0 {
                genderPickerVM.locAuth.gender = 1
                genderPickerVM.locAuth.genderString = StringConstants.male().lowercased()
            }else if genderPickerVM.selectedIndex?.row == 1 {
                genderPickerVM.locAuth.gender = 2
                genderPickerVM.locAuth.genderString = StringConstants.female().lowercased()
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func preferNotToSayAction(_ sender: UIButton) {
        
        if sender.currentImage == #imageLiteral(resourceName: "notPreferUnSelected") {
            preferNotToSauyBtn.setImage(#imageLiteral(resourceName: "notPreferSelected"), for: .normal)
            genderPickerVM.isNotPrefer = true
        }else if sender.currentImage == #imageLiteral(resourceName: "notPreferSelected") {
            genderPickerVM.isNotPrefer = false
            preferNotToSauyBtn.setImage(#imageLiteral(resourceName: "notPreferUnSelected"), for: .normal)
        }
        mainTableView.reloadData()
    }
    
}

extension GenderPickerViewController {
    
    /// based on the preference type update the screen
    func setUpPreferenceType(){
        
        screenTitle.text = StringConstants.specifyYour()
        subTitle.text = StringConstants.gender()
        topLineWidth.constant = genderPickerVM.screenWidth
        skipBtn.isHidden = true
        preferNotToSayView.isHidden = true
        
    }
    
    
    /// creates object of addMediaVewController and push that viewController
    func pushVCToMediaVc() {
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.addMediaSI) as! AddMediaVC
        self.navigationController?.pushViewController(userBasics, animated: true)
        userBasics.addMediaVM.locAuth = genderPickerVM.locAuth
       userBasics.isForFacebook = self.isForFacebook
        //genderPickerVM
    }
    
}


