//
//  GenderPickerTVE.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 30/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit


// MARK: - UITableViewDelegate,UITableViewDataSource
extension GenderPickerViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Gender.GenderArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let picker = tableView.dequeueReusableCell(withIdentifier: String(describing:GenderPickerTableViewCell.self), for: indexPath) as! GenderPickerTableViewCell
        
        var flag = true
        if genderPickerVM.isNotPrefer {
            flag = false
        }
        if indexPath != genderPickerVM.selectedIndex {
            flag = false
        }
        picker.updateGender(gender: Gender.GenderArray[indexPath.row],selected:flag)
        
        return picker
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if genderPickerVM.isNotPrefer {
            
        }else{
            var array = [IndexPath]()
            if genderPickerVM.selectedIndex != nil {
                array.append(genderPickerVM.selectedIndex!)
            }
            if genderPickerVM.selectedIndex != indexPath {
                array.append(indexPath)
            }
            
            genderPickerVM.selectedIndex = indexPath
            mainTableView.reloadRows(at: array, with: .fade)
            
            self.nextButton.isEnabled = true
            
        }
    
    }
    
}

