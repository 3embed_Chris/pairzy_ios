//
//  JoinUSVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 14/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class JoinUSVM: NSObject {
    
    var authentication = Authentication()
    let disposeBag = DisposeBag()
    let joinUSVM_resp = PublishSubject<Bool>()
    var myPreferences = [PreferencesModel]()
    
    var locAuth = LocalAuthentication(data: [:])
    
    func getPreferences(){
        let apiCall = Authentication()
        apiCall.requestForGetPreference()
        //Helper.showProgressIndicator(withMessage: StringConstants.GettiingPrefrences)
        apiCall.subject_response
            .subscribe(onNext: { response in
                self.handleGetPreferenceResponse(responseModel: response)
            }, onError: { error in
               // Helper.hideProgressIndicator()
        
                Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
            })
            .disposed(by:disposeBag)
    }
    
    func handleGetPreferenceResponse(responseModel: ResponseModel) {
        
        let statuscode:API.ErrorCode = API.ErrorCode(rawValue: responseModel.statusCode)!
        switch (statuscode) {
            
        case .Success:
            let dictForSave =  responseModel.response
            UserDefaults.standard.setValue(dictForSave, forKey:"userPrefrences/")
            UserDefaults.standard.synchronize()
            let dataDict = responseModel.response["data"] as! [String:Any]
            Helper.savePreferences(data: dataDict)
            let appde = UIApplication.shared.delegate as! AppDelegate
            appde.checkForUpdateLocation()
            self.joinUSVM_resp.onNext(true)
            break
            
        default:
            print(responseModel.response)
        }
        
    }
    
}
