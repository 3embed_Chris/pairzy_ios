//
//  PreferenceModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 16/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
class PreferencesModel: NSObject {
    var title = ""
    var arrayOfSubPreference = [PreferenceSubModel]()
    
    init(preferences:[String:Any]){
        //  var myPreferences = [PreferenceSubModel]()
        if  let catName = preferences["title"] as? String {
            title = catName
        }
        if let subCats = preferences["data"] as? [[String:Any]] {
            
            for subCat in subCats {
                arrayOfSubPreference.append(PreferenceSubModel.init(preference: subCat , index:arrayOfSubPreference.count))
            }
        }
        
    }
}

class PreferenceSubModel: NSObject {
    
    var prefId = ""
    var prefMainTitle = ""
    var prefSubTitle = ""
    var options = [optionsModel]()
    var selectedValues:[String] = []
    var prefTypeId:Int = 0
    var isDone:Int = 0
    var isSelectedOption = 0
    var preferenceIndex = 0
    
    
    init(preference:[String:Any],index:Int){
        preferenceIndex = index
        if let id = preference[ApiResponseKeys.preferenceId] as? String {
            self.prefId = id
        }
        if let title = preference[ApiResponseKeys.prefSubTitle] as? String {
            self.prefMainTitle = title
        }
        if let subtitle = preference[ApiResponseKeys.prefMainTitle] as? String {
            self.prefSubTitle = subtitle
        }
        
        if let selectedValues:[String] = preference[ApiResponseKeys.prefSelectedValue] as? [String] {
            self.selectedValues = selectedValues
        }
        
        if let options:[String] = preference[ApiResponseKeys.prefOptions] as? [String] {
            for  singleOption in options  {
                self.options.append(optionsModel(name: singleOption,userSelectedValues:self.selectedValues))
            }
        }
        
        if let prefTypeId = preference[ApiResponseKeys.type] as? Int {
            self.prefTypeId = prefTypeId
        }
        if let isDone = preference[ApiResponseKeys.isDone] as? Int {
            self.isDone = isDone
        }
    }
}

class optionsModel: NSObject {
    
    var optionName = ""
    var isOptionSelected = 0
    
    init(name:String,userSelectedValues:[String]) {
        optionName = name
        if(userSelectedValues .contains(name)) {
            isOptionSelected = 1
        }
    }
}
