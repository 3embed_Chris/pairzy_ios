//
//  EULAViewController.swift
//  Datum
//
//  Created by Rahul Sharma on 08/08/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import WebKit

class EULAViewController: UIViewController,WKUIDelegate,WKNavigationDelegate {
    
    //MARK:- Outlet
    @IBOutlet weak var licenseWebView: WKWebView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var termsAndC: UITextView!
    @IBOutlet weak var acceptButtonOutlet: UIButton!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    let addMediaVM = AddMediaVM()
    let joinUsVM = JoinUSVM()
    var isForFb = false
    var nextPreferenceIndex = 0
    var locAuth = LocalAuthentication(data: [:])
    
    override func loadView() {
        super.loadView()
        let termAndCUrl = URL.init(string: SERVICE.TermAndCondition)
        let myRequest = URLRequest(url: termAndCUrl!)
        licenseWebView.load(myRequest)
        licenseWebView.uiDelegate = self
        licenseWebView.navigationDelegate = self
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setAttributedString()
        setUpAcceptButtonUI()
        didGetResponse()
        didGetPreferences()

    }
    


    func setUpAcceptButtonUI(){
        acceptButtonOutlet.layer.cornerRadius = 5
        acceptButtonOutlet.clipsToBounds = true
    }



    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //    @IBAction func backButtonAction(_ sender: UIBarButtonItem) {
    //         self.navigationController?.popViewController(animated: true)
    //    }
    
    
    @IBAction func acceptButtonAction(_ sender: UIButton) {
        
        if self.isForFb {
            let socialLoginModel:SocialLogin = SocialLogin.shared
            let requestData:RequestModel = RequestModel().faceBookLoginDetail(fbDetails: self.addMediaVM.locAuth)
            socialLoginModel.controllerCurrent = self
             socialLoginModel.facebookLoginAPI(data: requestData)
        }else {
            self.addMediaVM.postProfileDetail()
        }
        
        
    }
    func setAttributedString(){
        let str = StringConstants.BySigning()
        let attributedString = NSMutableAttributedString(string: str)
        
        var foundRange = attributedString.mutableString.range(of: StringConstants.termsOfService())
        attributedString.addAttribute(kCTForegroundColorAttributeName as NSAttributedStringKey, value:  Colors.PrimaryText , range: foundRange)
        attributedString.addAttribute(kCTUnderlineStyleAttributeName as NSAttributedStringKey, value: NSUnderlineStyle.styleSingle.rawValue, range: foundRange)
        attributedString.addAttribute(.link, value: SERVICE.TermAndCondition, range: foundRange)
        
        
        foundRange = attributedString.mutableString.range(of: StringConstants.privacyPolicy())
        attributedString.addAttribute(kCTForegroundColorAttributeName as NSAttributedStringKey, value:  Colors.PrimaryText , range: foundRange)
        attributedString.addAttribute(.link, value: SERVICE.PrivacyPolicy, range: foundRange)
        attributedString.addAttribute(kCTUnderlineStyleAttributeName as NSAttributedStringKey, value: NSUnderlineStyle.styleSingle.rawValue, range: foundRange)
        
        termsAndC.attributedText = attributedString
        termsAndC.textAlignment = .center
    }
    
    
    /// observable for add media view model
    func didGetResponse(){
        addMediaVM.AddMediaVM_resp.subscribe(onNext: { success in
            
            if success == AddMediaVM.ResponseType.serverError {
//                self.nextBtn.isUserInteractionEnabled = true
//                self.skipButtonOutlet.isUserInteractionEnabled = true
            }else{
               self.joinUsVM.getPreferences()
            }
            
            
        }).disposed(by: addMediaVM.disposeBag)
    }
    
    
    //Handling the Loading page on the webView
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print("Start Loading Webpage...")
        
        activityIndicator.startAnimating()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Completed Loading webpage")
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    
    
    func privacyPolicyAction(){
        let privacyPolicyUrl = URL.init(string: SERVICE.PrivacyPolicy)
        let myRequest = URLRequest(url: privacyPolicyUrl!)
        licenseWebView.load(myRequest)
    }
    func termsandCAction(){
        let termAndCUrl = URL.init(string: SERVICE.TermAndCondition)
        let myRequest = URLRequest(url: termAndCUrl!)
        licenseWebView.load(myRequest)
    }
    
   
    
}

extension EULAViewController : UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if URL.absoluteString == SERVICE.PrivacyPolicy{
            privacyPolicyAction()
        }else{
            self.termsandCAction()
        }
        return false
    }
    
    
}

extension EULAViewController {
    
    func didGetPreferences(){
        
        joinUsVM.joinUSVM_resp.subscribe(onNext:{ success in
            
           // self.nextBtn.isUserInteractionEnabled = true
            self.joinUsVM.myPreferences = Helper.getPreference()
            if self.joinUsVM.myPreferences.count != 0 {
                
               var prefTypeId = 0
                let category = self.joinUsVM.myPreferences.count
                
                for i in 0..<category{
                    if self.joinUsVM.myPreferences[i].arrayOfSubPreference.count != 0 {
                         prefTypeId = self.joinUsVM.myPreferences[i].arrayOfSubPreference[0].prefTypeId
                        self.nextPreferenceIndex = i
                        break
                    }
                }
                
                switch prefTypeId {
                case 1,2,10:
                    self.preferenceTableVC()
                case 5:
                    self.pushTotextFieldTypeVC()
                default:
                    print("")
                }
            }else{
                Helper.makeTabbarAsRootVc(forVc: self)
            }
        }).disposed(by: joinUsVM.disposeBag)
        
    }
    
    /// creates object of gende
    func preferenceTableVC() {
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.PreferenceTable) as! PreferencesTableVC
        userBasics.isFirstTime = true
       userBasics.currentPrefCategoryIndex = self.nextPreferenceIndex
        self.navigationController?.pushViewController(userBasics, animated: true)
        
    }
    
    func pushTotextFieldTypeVC(){
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.TextFieldTypeVC) as! PreferencesTFVC
        userBasics.currentPrefCategoryIndex = self.nextPreferenceIndex
        userBasics.screenIndex = 0
        userBasics.isFirstTime = true
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
}

