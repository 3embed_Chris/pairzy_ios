//
//  joinUsMessageVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 21/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import MBCircularProgressBar

class joinUsMessageVC: UIViewController {
    
    @IBOutlet weak var progressView: MBCircularProgressBarView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var relationShipStartsWith: UILabel!
    @IBOutlet weak var aCommunityOfPeople: UILabel!
    
    
    let joinUsVM = JoinUSVM()
    
    var progressTimer:Timer? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        progressView.value = 0
        showPI()
        startTimer()
        initialSetup()
        didGetResponse()
         
      // joinUsVM.getPreferences()
        // Do any additional setup after loading the view.
    }
    func initialSetup(){
        relationShipStartsWith.text = StringConstants.relationShip()
        aCommunityOfPeople.attributedText = self.getAttributedText(text: StringConstants.aCommunityOf(),subText:StringConstants.joinUs(),color: Colors.AppBaseColor)
    }
    
    
    //Multiple ColourText
    func getAttributedText(text: String,subText:String,color:UIColor) -> NSMutableAttributedString{
        let range = (text.lowercased() as NSString).range(of: subText.lowercased())
        let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: color , range: range)
        return attributedString
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nextBtn.isUserInteractionEnabled = true
    }
    
  //  func initialSetup(){
        
        //    Helper.updateText(text: (setManualButton.titleLabel?.text)!, subText: StringConstants.Manually, setManualButton, color: Helper.getUIColor(color: Colors.AppBaseColor),link: "")
        
   // }
    
    
    @IBAction func nextButtonAction(_ sender: Any) {
        nextBtn.isUserInteractionEnabled = false
       joinUsVM.getPreferences()
        
    }
    
    
    @objc func showPI(){
        DispatchQueue.main.async() { // or you can use
            self.setup()
        }
    }
    
    func setup(){
        let when = DispatchTime.now()
        DispatchQueue.main.asyncAfter(deadline: when){
            self.progressView.value = 0
            self.animate()
        }
    }
    
    func animate() {
        DispatchQueue.main.async(){
            UIView.animate(withDuration: 4) {
                self.progressView.value = 100
                self.progressView.progressAngle = 100
            }
        }
    }
    
    /// starts the timer to autoscroll
    func startTimer(){
        DispatchQueue.main.async(){
            self.animate()
        }
        
        self.progressTimer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.showPI), userInfo: nil, repeats: true)
    }
    
    
    /// stops timer of autoscroll
    func stopTimer(){
        self.progressTimer?.invalidate()
    }
    
}

extension joinUsMessageVC {
    
    func didGetResponse(){
        
        joinUsVM.joinUSVM_resp.subscribe(onNext:{ success in
            
            self.nextBtn.isUserInteractionEnabled = true
            self.joinUsVM.myPreferences = Helper.getPreference()
            if self.joinUsVM.myPreferences.count != 0{
                
                let prefTypeId = self.joinUsVM.myPreferences[0].arrayOfSubPreference[0].prefTypeId
                switch prefTypeId {
                case 1,2,10:
                    self.preferenceTableVC()
                case 5:
                    self.pushTotextFieldTypeVC()
                default:
                    print("")
                }
            }
        }).disposed(by: joinUsVM.disposeBag)
        
    }
    
    /// creates object of gende
    func preferenceTableVC() {
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.PreferenceTable) as! PreferencesTableVC
        self.navigationController?.pushViewController(userBasics, animated: true)
        
    }
    
    func pushTotextFieldTypeVC(){
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.TextFieldTypeVC) as! PreferencesTFVC
        userBasics.screenIndex = 0
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
}






