//
//  WebViewController.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 28/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {

    @IBOutlet weak var webViewOutlet: UIWebView!
    
    var navTitle = StringConstants.termsAndConditions()
    var link = ""
    
    
    var temp = "https://www.google.co.in"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.title = navTitle
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if link.count == 0 {
             self.webViewOutlet.loadRequest(URLRequest(url: URL(string: temp )!))
        }
        else {
             self.webViewOutlet.loadRequest(URLRequest(url: URL(string: link )!))
        }
 
    }
    
    
    /// button for dismiss the controller.
    ///
    /// - Parameter sender: close button.
    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion:nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
