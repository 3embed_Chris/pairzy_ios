//
//  LaunchVC.swift
//  Datum
//
//  Created by 3 Embed on 28/08/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import AudioToolbox
import AVKit

class LaunchVC: UIViewController {

    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var logoImage: UIImageView!
    
    var player:AVPlayer?
    var isUserChangedScreen = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let audioSession = AVAudioSession.sharedInstance()
        if audioSession.isOtherAudioPlaying {
            _ = try? audioSession.setCategory(AVAudioSessionCategoryAmbient, with: AVAudioSessionCategoryOptions.mixWithOthers)
        }
         PlayVideo()
        //redirecting to landing screen or tabbar based on condition.
       
        
        // Do any additional setup after loading the view.
    }

    
    func PlayVideo(){
        
        guard let path = Bundle.main.path(forResource: "splash", ofType:"mp4") else {
          print("video.m4v not found")
            return
        }
      
        
        player = AVPlayer(url: URL(fileURLWithPath: path))
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.videoView.bounds
        self.videoView.layer.addSublayer(playerLayer)
        player?.play()
        player?.actionAtItemEnd = AVPlayerActionAtItemEnd.none
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isUserChangedScreen = true
        self.navigationController?.isNavigationBarHidden = true
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        isUserChangedScreen = false
        self.navigationController?.isNavigationBarHidden = false
        player?.pause()
    }
    
    
    @objc func playerItemDidReachEnd() {
        let appDelegateObj = UIApplication.shared.delegate as! AppDelegate
         appDelegateObj.redirectingStoryBoard()
    }
    
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
