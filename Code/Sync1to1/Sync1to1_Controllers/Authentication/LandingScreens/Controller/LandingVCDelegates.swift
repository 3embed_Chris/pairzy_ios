//
//  LandingVCDelegates.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 29/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit


// MARK: - UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension LandingVC: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return landingScreens.screens.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: LandingCollectionCell.self), for: indexPath) as! LandingCollectionCell
        
        cell.updateCell(data: landingScreens.screens[indexPath.item])
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size = CGSize.zero
        let cellWidth = UIScreen.main.bounds.width
        let cellHight = collectionView.frame.size.height
        size.height =  cellHight
        size.width  =  cellWidth
        return size
    }
}

// MARK: - UICollectionViewDelegate
extension LandingVC : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

// MARK: - UIScrollViewDelegate
extension LandingVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.landingViewOutlet .pageViewoutlet.currentPage = Int(pageNumber)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.landingViewOutlet .pageViewoutlet.currentPage = Int(pageNumber)
    }
    
}
