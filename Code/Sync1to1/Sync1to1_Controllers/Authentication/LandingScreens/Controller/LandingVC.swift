//
//  LandingVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 01/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import AccountKit


class LandingVC: UIViewController {
    
    @IBOutlet weak var landingViewOutlet: LandingView!
    @IBOutlet weak var collectionViewHight: NSLayoutConstraint!
    @IBOutlet weak var viewTop: NSLayoutConstraint!
    @IBOutlet weak var arrowBtn: UIButton!
    @IBOutlet weak var facebookBtnTopSpace: NSLayoutConstraint!
    @IBOutlet weak var arrowTopSpace: NSLayoutConstraint!
    @IBOutlet weak var loginWithPh: UIButton!
    @IBOutlet weak var weDontPostAnything: UILabel!
    @IBOutlet weak var weTakeYourPrivacy: UILabel!
    @IBOutlet weak var otherUserCannotContact: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    
    @IBOutlet weak var loginWithEmailBtn: UIButton!
    
    
    @IBOutlet weak var checkBox: UIButton!
    
    
    
    @IBAction func loginWithEmail(_ sender: UIButton) {
        //gotoEmailLoginVC()
    }
    
    @IBAction func checkBoxBtnAction(_ sender: UIButton) {
      //  sender.isSelected = !sender.isSelected
        
    }
    
    var landingScreens = LandingModelLibrary()
    var scrollTimer: Timer? = nil
    let disposeBag = DisposeBag()
    let verifyCodeVM = VerifyCodeVM()

    var socialLoginModel:SocialLogin = SocialLogin.shared
    var dataSocial:LocalAuthentication = Helper.getSocialProfile()
    
    //MARK:- VIEW DELEGATES.
    override func viewDidLoad() {
        super.viewDidLoad()
        LandingVM.sharedInstance().accountKitInitialization()
        didGetResponse()
        // Do any additional setup after loading the view.
        self.facebookBtnTopSpace.constant = 5
        socialLoginModel.socialDelegate = self
        self.landingViewOutlet.bottomText.isHidden = false
        self.landingViewOutlet.nextScreenText.isHidden = true
        arrowBtn.isHidden = true
        self.landingViewOutlet.collectionTitleView.isHidden = false
        let hight = CGFloat(UIScreen.main.bounds.size.height/1.5)
        let bottomHt = (UIScreen.main.bounds.size.height - hight)
        if bottomHt < 287 {
            collectionViewHight.constant = hight - 40
        }else {
            collectionViewHight.constant = hight
        }
        setAttributedString()
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
        startTimer()
        loginWithPh.setTitle(StringConstants.loginWithPh(), for: .normal)
        loginWithPh.setTitleColor(Colors.AppBaseColor, for: .normal)
       // loginWithEmailBtn.setTitle(StringConstants.loginWithEmail(), for: .normal)
       // loginWithEmailBtn.setTitleColor(Colors.AppBaseColor, for: .normal)
        weDontPostAnything.text = StringConstants.weDontPostAnything()
        weTakeYourPrivacy.text = StringConstants.weTakeYourPrivacy()
        otherUserCannotContact.text = StringConstants.otherUsersCannot()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
         let language = Helper.getSelectedLanguage()
        languageLabel.text = language.name
        addObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        removeObserver()
        Helper.hideProgressIndicator()
    }
    
    
    //MARK: -  BUTTON ACTIONS
    @IBAction func facebookLoginBtn(_ sender: Any) {
         UIView.animate(withDuration: 0.5, animations: {
            self.viewTop.constant = -(self.collectionViewHight.constant+20)
            self.facebookBtnTopSpace.constant = 100
            
            //    self.bottmpartView.constant = UIScreen.main.bounds.size.height
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
            self.stopTimer()
        }, completion: { (finshed) in
            if(finshed) {
                UIView.animate(withDuration: 0.5, animations: {
                    self.arrowTopSpace.constant = 70
                    
                    self.landingViewOutlet.pageViewoutlet.isHidden = true
                    self.arrowBtn.isHidden = false
                    self.landingViewOutlet.collectionTitleView.isHidden = true
                    self.landingViewOutlet.bottomText.isHidden = true
                    self.landingViewOutlet.nextScreenText.isHidden = false
                    self.view.updateConstraints()
                    self.view.layoutIfNeeded()
                })
            }
        })
    }
    
    @IBAction func selectLanguageAction(_ sender: Any) {
        let profileVc = self.storyboard?.instantiateViewController(withIdentifier:"selectLanguage") as! AppLanguageVC
        self.present(profileVc, animated: true, completion: nil)
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        UIView.animate(withDuration: 0.5) {
            self.viewTop.constant = 0
            //self.facebookBtnTopSpace.constant = 10
            self.facebookBtnTopSpace.constant = 5
            self.arrowTopSpace.constant = 0
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
            self.startTimer()
            self.landingViewOutlet.pageViewoutlet.isHidden = false
            self.landingViewOutlet.collectionTitleView.isHidden = false
            self.arrowBtn.isHidden = true
            self.landingViewOutlet.bottomText.isHidden = false
            self.landingViewOutlet.nextScreenText.isHidden = true
        }
    }
    
    ///When user will tap on Signup button then this mithod will be called
    @IBAction func loginWithFacebook(_ sender: Any) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        socialLoginModel.faceBookLoginAction(controller: self)
       
        
    }
    
    ///When user will tap on Login button then this mithod will be called
    @IBAction func loginButtonClicked(_ sender: Any) {
//        let phoneNumber = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: StoryBoardIdentifier.phoneNumberSI)
//        let navigationController = UINavigationController(rootViewController: phoneNumber)
//        navigationController.isNavigationBarHidden = true
//        self.navigationController?.present(navigationController, animated: true, completion: nil)
//
        loginWithPhone()
    }
    
    
    
    func setAttributedString(){
        // landingViewOutlet.termsAndC.textColor = Colors.SeconderyText
        let str = StringConstants.BySigning()
        let attributedString = NSMutableAttributedString(string: str)
        
        var foundRange = attributedString.mutableString.range(of: StringConstants.termsOfService())
        attributedString.addAttribute(kCTForegroundColorAttributeName as NSAttributedStringKey, value:  Colors.PrimaryText , range: foundRange)
        attributedString.addAttribute(kCTUnderlineStyleAttributeName as NSAttributedStringKey, value: NSUnderlineStyle.styleSingle.rawValue, range: foundRange)
        attributedString.addAttribute(.link, value: SERVICE.TermAndCondition, range: foundRange)
        
        
        foundRange = attributedString.mutableString.range(of: StringConstants.privacyPolicy())
        attributedString.addAttribute(kCTForegroundColorAttributeName as NSAttributedStringKey, value:  Colors.PrimaryText , range: foundRange)
        attributedString.addAttribute(.link, value: SERVICE.PrivacyPolicy, range: foundRange)
        attributedString.addAttribute(kCTUnderlineStyleAttributeName as NSAttributedStringKey, value: NSUnderlineStyle.styleSingle.rawValue, range: foundRange)
        
        landingViewOutlet.termsAndC.attributedText = attributedString
        landingViewOutlet.termsAndC.textAlignment = .center
    }
    
   
    
}

extension LandingVC {
    
    
    /// starts the timer to autoscroll
    func startTimer(){
        scrollTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(LandingVC.scrollToNextCell), userInfo: nil, repeats: true)
    }
    
    
    /// stops timer of autoscroll
    func stopTimer(){
        self.scrollTimer?.invalidate()
    }
    
    
    /// scrolls to next cell
    @objc func scrollToNextCell(){
        
        let cellSize = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height/2)
        let contentOffset = landingViewOutlet.landingCollectionView.contentOffset
        
        if landingViewOutlet.landingCollectionView.contentSize.width <=  (landingViewOutlet.landingCollectionView.contentOffset.x + cellSize.width) {
            landingViewOutlet.landingCollectionView.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
        }
        else {
            landingViewOutlet.landingCollectionView.scrollRectToVisible(CGRect(x:contentOffset.x + cellSize.width  , y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
        }
        
    }
}

//Social Login delegate
extension LandingVC:socialLoginDelegate ,UITextViewDelegate {
    func didLogin(data: LocalAuthentication) {
       Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        dataSocial = data
         gotoEmailVC()
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print(URL)
        return true
    }
    
    //emailVC
    func gotoEmailVC(){
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.userBasicSI) as! UserBaiscDetailsVC
        userBasics.basicDetails = .EmailAddress
        userBasics.detailsVM.locAuth = self.dataSocial
        userBasics.isForFacebook = true
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
    func gotoEmailLoginVC(){
        let logiWithEmailVC = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier: "LoginWithEmail") as! LoginWithEmailVC
        self.navigationController?.pushViewController(logiWithEmailVC, animated: true)
    }
}


extension LandingVC: AKFViewControllerDelegate {
    
    //called in view did load
    //    func accountKitInitialization(){
    //        if _accountKit == nil {
    //         _accountKit = AKFAccountKit(responseType: .accessToken)
    //        }
    //    }
    
    //view will appear
    func checkAccountKitIsLoggedIn(){
        
        if LandingVM.sharedInstance()._accountKit?.currentAccessToken != nil{
            // if the user is already logged in, go to the main screen
            print("Already Logged in")
            DispatchQueue.main.async(execute: {
                self.verifyPhoneNumber()
            })
        }
        else{
            // Show the login screen
        }
        
    }
    
    
    //Login with phone number button action
    func loginWithPhone(){
        let id = UUID.init().uuidString
        let vc = (LandingVM.sharedInstance()._accountKit?.viewControllerForPhoneLogin(with: nil, state: id))!
        vc.isSendToFacebookEnabled = true
        self.prepareLoginViewController(loginViewController: vc)
        self.present(vc as UIViewController, animated: true, completion: nil)
    }
    
    
    //MARK: - Helper Methods
    func prepareLoginViewController(loginViewController: AKFViewController) {
        loginViewController.delegate = self
        
        //Costumize the theme
        let theme:Theme = Theme.default()
        theme.headerBackgroundColor = Colors.AppBaseColor
        theme.headerTextColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        theme.iconColor = UIColor(red: 0.325, green: 0.557, blue: 1, alpha: 1)
        theme.inputTextColor = UIColor(white: 0.4, alpha: 1.0)
        theme.statusBarStyle = .default
        theme.textColor = UIColor(white: 0.3, alpha: 1.0)
        theme.titleColor = UIColor(red: 0.247, green: 0.247, blue: 0.247, alpha: 1)
        theme.buttonBackgroundColor = Colors.AppBaseColor
        theme.iconColor = Colors.AppBaseColor
        loginViewController.setTheme(theme)
    }
    
    
    
    func viewController(viewController: UIViewController!, didCompleteLoginWithAccessToken accessToken: AccessToken!, state: String!) {
        // print("did complete login with access token \(accessToken.tokenString) state \(state)")
    }
    
    // handle callback on successful login to show authorization code
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
        print("didCompleteLoginWithAuthorizationCode")
    }
    
    func viewControllerDidCancel(_ viewController: (UIViewController & AKFViewController)!) {
        // ... handle user cancellation of the login process ...
        print("viewControllerDidCancel")
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didFailWithError error: Error!) {
        // ... implement appropriate error handling ...
        //print("\(viewController ?? <#default value#>) did fail with error: \(error.localizedDescription)")
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWith accessToken: AccessToken!, state: String!) {
        print("didCompleteLoginWith")
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        verifyPhoneNumber()
    }
    
    
    
    func verifyPhoneNumber(){
        
        LandingVM.sharedInstance()._accountKit.requestAccount { (account, error) in
            
            if error == nil {
                
                // let accountId = account?.accountID
                
                if let currentEmail = account?.emailAddress{
                    self.verifyCodeVM.locAuth.email = currentEmail
                }
                
                if let phone = account?.phoneNumber {
                    self.verifyCodeVM.locAuth.phoneNumber = phone.phoneNumber
                }
                if let contryCode = account?.phoneNumber?.countryCode {
                    self.verifyCodeVM.locAuth.countryCode = "+" + contryCode
                }
                
                self.requestOtp()
                
            }
        }
    }
    
}

extension LandingVC {
    
    func requestOtp(){
        let params = [ServiceInfo.phoneNumberWIthCode: self.verifyCodeVM.locAuth.countryCode + self.verifyCodeVM.locAuth.phoneNumber,
                      ServiceInfo.type: 1,
                      ServiceInfo.deviceId: UIDevice.current.identifierForVendor!.uuidString] as [String:Any]
        let requestData = RequestModel().verificationCodeRequestDetails(verifyCodeDetails:params)
        let apiCall = Authentication()
        apiCall.requestForOtp(verificationCodeData: requestData)
        apiCall.subject_response
            .subscribe(onNext: {response in
                
                let errNum:API.ErrorCode? = API.ErrorCode(rawValue: response.statusCode)!
                if errNum == .Success {
                    self.verifyCodeVM.requestToVerifyCode(isNewRegist: 1 ,otp: "111111")
                }
            }, onError: { (error) in
                print(error)
                Helper.showAlertMessageViewFromTopNavBar(showOnView: self.view!, message: error.localizedDescription )
            }).disposed(by: disposeBag)
    }
    
    func didGetResponse(){
        verifyCodeVM.verifyCodeVM_resp.subscribe(onNext: { success in
            if success == VerifyCodeVM.UserType.forEditProfile {
                Helper.hideProgressIndicator()
                self.dismiss(animated: true, completion: nil)
            }
            if success == VerifyCodeVM.UserType.newUser {
                Helper.hideProgressIndicator()
                self.gotoNextVC()
            }else if success == VerifyCodeVM.UserType.oldUser{
                Helper.hideProgressIndicator()
                Helper.makeTabbarAsRootVc(forVc: self)
            }else {
                
                //                DispatchQueue.main.async(){
                //                     Helper.hideProgressIndicator()
                //                }
                
                //            KRProgressHUD.dismiss() {
                //                self.showPopup(message: self.verifyCodeVM.message)
                //            }
                
                
                
                //            self.nextBtn.isUserInteractionEnabled = true
                //            self.didNotGetCode.isUserInteractionEnabled = true
                //
                
                //   Helper.showAlertWithMessage(withTitle: Message.Error, message: self.verifyCodeVM.message, onViewController: self)
            }
        })
   }
    //emailVC
    func gotoNextVC(){
        self.view.endEditing(true)
        self.verifyCodeVM.locAuth.otp = "111111"
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.userBasicSI) as! UserBaiscDetailsVC
        userBasics.basicDetails = .EmailAddress
        userBasics.detailsVM.locAuth = self.verifyCodeVM.locAuth
        
        self.navigationController?.pushViewController(userBasics, animated: true)
        
    }
    
    func showPopup(message: String){
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let tryAgainView = TryAgainViewC(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        tryAgainView.loadPopUpView(title: message)
        //tryAgainView.delegate = self
        window.addSubview(tryAgainView)
        window.makeKeyAndVisible()
    }
    
    
}
