//
//  LandingModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 26/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

struct LandingModel {
    var screenTitle = ""
    var screenImage = UIImage()
}

/// its is having landing screen list and icons
class LandingModelLibrary {
    
     var screens: [LandingModel] {
        
        var screen1 = LandingModel()
        screen1.screenTitle = StringConstants.landingScreen1()
        screen1.screenImage = #imageLiteral(resourceName: "landing1")
        
        var screen2 = LandingModel()
        screen2.screenTitle = StringConstants.landingScreen2()
        screen2.screenImage = #imageLiteral(resourceName: "landing3")
        
        var screen3 = LandingModel()
        screen3.screenTitle = StringConstants.landingScreen3()
        screen3.screenImage = #imageLiteral(resourceName: "landing4")
        
        var screen4 = LandingModel()
        screen4.screenTitle = StringConstants.landingScreen4()
        screen4.screenImage = #imageLiteral(resourceName: "landing2")
        
        
        return [screen1,screen2,screen3,screen4]
        
    }
}
