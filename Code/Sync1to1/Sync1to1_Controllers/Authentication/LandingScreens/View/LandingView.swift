//
//  LandingView.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 02/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit


class LandingView: UIView {
    
    @IBOutlet weak var signupButtionView: UIView!
    @IBOutlet weak var loginButtonView: UIView!
    @IBOutlet weak var pageViewoutlet: UIPageControl!
    @IBOutlet weak var landingCollectionView  : UICollectionView!
    @IBOutlet weak var downArrow  : UIButton!
    @IBOutlet weak var collectionTitleView: UIView!
    @IBOutlet weak var bottomText: UIView!
    @IBOutlet weak var nextScreenText: UIView!
    @IBOutlet weak var termsAndC: UITextView!
    @IBOutlet weak var signupWithFbBtn  : UIButton!
   
    var landingScreens = LandingModelLibrary()
    var timer: Timer!
    var maximumOffSet: CGFloat?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }
  
    
    /// initial view updates
    func initialSetup(){
        loginButtonView.layer.borderColor = UIColor.white.cgColor
        pageViewoutlet.numberOfPages = landingScreens.screens.count
        downArrow.transform = CGAffineTransform.init(rotationAngle: CGFloat(-Double.pi))
        pageViewoutlet.currentPageIndicatorTintColor = Colors.AppBaseColor
   }
    

    
}


