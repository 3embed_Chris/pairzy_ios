//
//  LandingCollectionCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 26/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class LandingCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var screenTitle: UILabel!
    @IBOutlet weak var screenImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    /// updates the cell
    ///
    /// - Parameter data: is of type landing Model having screen image and messages
    func updateCell(data: LandingModel){
        screenTitle.text = data.screenTitle
        screenImage.image = data.screenImage
    }    
}
