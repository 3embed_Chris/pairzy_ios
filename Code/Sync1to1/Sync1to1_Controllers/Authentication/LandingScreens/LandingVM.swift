//
//  LandingVM.swift
//  Datum
//
//  Created by apple on 19/04/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import AccountKit

class LandingVM: NSObject {
    
    /// Shared instance object for gettting the singleton object
    
    static var obj:LandingVM? = nil
    
    class func sharedInstance() -> LandingVM {
        if obj == nil {
            obj = LandingVM()
        }
        return obj!
    }
    
    var _accountKit:AKFAccountKit!
    
    //called in view did load
    func accountKitInitialization(){
        if _accountKit == nil {
            _accountKit = AKFAccountKit(responseType: .accessToken)
        }
    }
    
    
}
