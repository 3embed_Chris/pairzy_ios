//
//  InstagramLoginVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 11/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift

protocol instagramLoginDelegate: class {
    func loggedInsSuccessfully(response:[String:Any])
    func loginWithError(errorMessage:String)
}


class InstagramLoginVC: UIViewController {
    
    @IBOutlet weak var webview: UIWebView!
    weak var delegate:instagramLoginDelegate? = nil
    
    let myProfileVM = MyProfileVM()
    let instaVM = InstagramVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webview.backgroundColor = .white
        didGetResponse()
        instaVM.loginWithInstagram()
        clearsWebHistory()
        
        // Do any additional setup after loading the view.
    }
    
    
    
    ///  Remove all cache
    func clearsWebHistory(){
        
        // Remove all cache
        URLCache.shared.removeAllCachedResponses()
        
        // Delete any associated cookies
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
        
    }
    
    /// opens instagram login screen
    func openInstaLoginPage() {
//        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [InstaAPI.INSTAGRAM_AUTHURL,InstaAPI.INSTAGRAM_CLIENT_ID,InstaAPI.INSTAGRAM_REDIRECT_URI, InstaAPI.INSTAGRAM_SCOPE])
    let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token", arguments: [InstaAPI.INSTAGRAM_AUTHURL,InstaAPI.INSTAGRAM_CLIENT_ID,InstaAPI.INSTAGRAM_REDIRECT_URI])
        let urlRequest = URLRequest.init(url: URL.init(string: authURL)!)
        webview.delegate = self
        webview.loadRequest(urlRequest)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}


// MARK: - UIWebViewDelegate
extension InstagramLoginVC: UIWebViewDelegate{
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request:URLRequest, navigationType: UIWebViewNavigationType) -> Bool{
        return checkRequestForCallbackURL(request: request)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        Helper.hideProgressIndicator()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print(error)
    }
    
    
    /// check requestfor callback
    ///
    /// - Parameter request: is of type URLRequest
    /// - Returns: returns true or false
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        let requestURLString = (request.url?.absoluteString)! as String
        if requestURLString.hasPrefix(InstaAPI.INSTAGRAM_REDIRECT_URI) {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            return false;
        }
        return true
    }
    
    
    /// handles authentication key and saves to user defaults
    ///
    /// - Parameter authToken: authentication key is of type string
    func handleAuth(authToken: String) {
        Helper.showProgressIndicator(withMessage: StringConstants.fetchingDetail())
        print("Instagram authentication token ==", authToken)
        self.instaVM.instaProfile.instaAuthentication = authToken
        let dbObj = Database.shared
        dbObj.userDataObj.instaGramToken =  authToken
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        UserDefaults.standard.setValue(authToken, forKey:"instaAuthentication")
        UserDefaults.standard.synchronize()
        
        
        instaVM.getUserDetails()
    }
    
    
}


// MARK: - Observer for ViewModel
extension InstagramLoginVC {
    
    
    /// observable for instagram viewModel
    func didGetResponse(){
        self.instaVM.instagramVM_resp.subscribe(onNext:{ success in
            switch success.0 {
            case InstagramVM.ResponseType.openInsta:
                self.openInstaLoginPage()
                break
            case InstagramVM.ResponseType.getUserDeatil:
                break
            case InstagramVM.ResponseType.getUserMedia:
                self.dismiss(animated: true, completion: nil)
                break
            case InstagramVM.ResponseType.getUserOtherMedia:
                self.dismiss(animated: true, completion: nil)
                break
                
            }
        }).disposed(by: instaVM.disposeBag)
    }
}
