//
//  InstagramVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 25/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class InstagramVM: NSObject {
   
    enum ResponseType:Int {
        case openInsta = 0
        case getUserDeatil = 1
        case getUserMedia = 2
        case getUserOtherMedia = 3
    }
    
    var delegate:instagramLoginDelegate? = nil
    var otherUserMedia:Bool = false
    var otherUserId = ""
    var instaProfile = InstagramProfile(data: [:])
    let myProfileVM = MyProfileVM()
    var authToken = ""
    let disposeBag = DisposeBag()
    let instagramVM_resp = PublishSubject<(ResponseType,InstagramProfile)>()
    
    
    /// checks the is user alreadylogged in datails is there
    ///
    /// - Returns:  Bool - if user has authentication returns true else false ,and instagramProfile object
    func isUserHasAuthToken() -> (Bool,InstagramProfile) {
        if let loggedData = UserDefaults.standard.value(forKey:"instaLoggedDetails") as? [String:Any] {
            self.instaProfile.updateUser(data:loggedData)
            if let authKey = UserDefaults.standard.value(forKey:"instaAuthentication") as? String {
                self.instaProfile.instaAuthentication = authKey
            }
            if self.instaProfile.instaAuthentication != "" {
                return (true,self.instaProfile)
            }
        }
        return (false,self.instaProfile)
    }
    
    
    ///  for opening instgram login page.
    ///
    /// - Parameters:
    ///   - userId:
    ///   - isSelf:
    
    func loginWithInstagram() {
        self.instagramVM_resp.onNext((.openInsta,instaProfile))
        Helper.showProgressIndicator(withMessage: "Loading.. ")
    }

    
    //gives login username, id , email,profile picture and etc.
    func getUserDetails() {
        let instApiName = InstaAPI.instagramBaseApi+"self/?access_token="+self.instaProfile.instaAuthentication
        InstagramApi.requestGETURL(serviceName:instApiName,success: { (response) in
            // response from instagram api.
            print("response details:%@",response)
            
            if let instaDetailss = response[ApiResponseKeys.data].dictionaryObject {
                self.instaProfile.updateUser(data:instaDetailss)
                
                UserDefaults.standard.setValue(instaDetailss, forKey:"instaLoggedDetails")
                UserDefaults.standard.synchronize()
            }
             let dbObj = Database.shared
            dbObj.userDataObj.instaGramProfileId =  self.instaProfile.instaId
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            self.updateProfile()
            if(self.otherUserMedia) {
                self.getOtherUserMedia()
            } else {
                self.getUserMedia()
            }
            //  self.delegate?.loggedInsSuccessfully(response:response.dictionaryObject!)
            
        }, failure: { (error) in
            Helper.hideProgressIndicator()
            // error details from instagram api.
            self.delegate?.loginWithError(errorMessage:error.localizedDescription)
            print("error details:%@",error.localizedDescription)
        })
    }
    
    //gives login username, id , email,profile picture and etc.
    func getUserMedia() {
        let instApiName = InstaAPI.instagramBaseApi+"self/media/recent/?access_token="+self.instaProfile.instaAuthentication
        InstagramApi.requestGETURL(serviceName:instApiName,success: { (response) in
            // response from instagram api.
            print("User media:%@",response) // update instaProfile
         
            if let instaDetailss = response[ApiResponseKeys.data].arrayObject {
                self.instaProfile.updateMediaDetails(data:instaDetailss as! [[String : Any]])
            }
  
            Helper.hideProgressIndicator()
            self.instagramVM_resp.onNext((.getUserMedia,self.instaProfile))
           // self.dismiss(animated: true, completion: nil)
        }, failure: { (error) in
            // error details from instagram api.
            print("error details:%@",error.localizedDescription)
            Helper.hideProgressIndicator()
        })
    }
    
    
    /// gives members media
    func getOtherUserMedia() {
        let instApiName = InstaAPI.instagramBaseApi+otherUserId+"/media/recent/?access_token="+self.instaProfile.instaAuthentication
        InstagramApi.requestGETURL(serviceName:instApiName,success: { (response) in
            // response from instagram api.
            print("Other media:%@",response)
            if let instaDetailss = response[ApiResponseKeys.data].arrayObject {
                self.instaProfile.updateMediaDetails(data:instaDetailss as! [[String : Any]])
            }
            
            Helper.hideProgressIndicator()
            self.instagramVM_resp.onNext((.getUserOtherMedia,self.instaProfile))
           // self.dismiss(animated: true, completion: nil)
        }, failure: { (error) in
            // error details from instagram api.
            print("error details:%@",error.localizedDescription)
            Helper.hideProgressIndicator()
        })
    }
    
    
    /// if user connects to instagram updates the profile with instagram Id and Token
    func updateProfile(){
        
        let locDetails:[String:Any?] = Helper.getUserLocation()
        var latitude = 0.0
        var longitude = 0.0
        let hasValue:String = locDetails["hasLocation"] as! String
        
        if Helper.getIsFeatureExist(){
            
            let featureLoc:Location =  Helper.getFeatureLocation()
            latitude = featureLoc.latitude
            longitude = featureLoc.longitude
            
            
        }else {
            if(hasValue == "1") {
                if let lat = locDetails["lat"] as? Double {
                    latitude = lat
                }
                if let long = locDetails["long"] as? Double {
                    longitude = long
                }
            }
        }
        let params = [ ServiceInfo.latitude: latitude,
                       ServiceInfo.longitude: longitude,
                       ServiceInfo.InstaGramProfileId: instaProfile.instaId,
                       ServiceInfo.InstaGramToken: self.instaProfile.instaAuthentication] as [String : Any]
        print(params)
        myProfileVM.updateProfile(params: params)
        
    }
    
}
