//
//  PreferencesTFVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 28/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class PreferencesTFVC: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var descprtionLabel: UILabel!
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var lineWidth: NSLayoutConstraint!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var calenderIcon: UIButton!
    @IBOutlet weak var skipBtn: UIButton!
    
    @IBOutlet weak var preferNotToSay: UILabel!
    @IBOutlet weak var preferNotToSayBtn: UIButton!
    
    @IBOutlet weak var backButtonOutlet: UIButton!
    var isForEditProfile = false
    var isVcForSignUp = false
    var screenIndex = 0
    var currentPrefCategoryIndex = 0
    var currentPrefSubCategoryIndex = 0
    
    var nextPrefCategoryIndex = 0
    var nextPrefSubCategoryIndex = 0
    var isFirstTime = false
    var totalCount = 0
    
    var myPreference:[PreferencesModel] = Helper.getPreference()
    var profileDetile = [PreferencesModel]()
    let screenWidth = UIScreen.main.bounds.size.width
    
    let preferenceVM = PreferenceVM()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        didGetResponse()
        checkTotalCount()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nextBtn.isUserInteractionEnabled = true
        if isForEditProfile {
            skipBtn.isHidden = true
           myPreference = profileDetile
            if myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex].selectedValues.count != 0 {
                let slectedVal = myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex].selectedValues[0]
                textField.text = slectedVal
            }
         
        }else {
            skipBtn.isHidden = false
        }
        if isFirstTime{
            self.backButtonOutlet.isHidden = true
        }else {
            self.backButtonOutlet.isHidden = false
        }
        addObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        removeObserver()
        if isFirstTime{
            self.backButtonOutlet.isHidden = true
        }else {
            self.backButtonOutlet.isHidden = false
        }
        
    }
  
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textField.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        nextButtonTapped(isNextButton: true)
    }
    
    @IBAction func preferNotToSayAction(_ sender: UIButton) {
     
        if sender.currentImage == #imageLiteral(resourceName: "notPreferUnSelected") {
            preferNotToSayBtn.setImage(#imageLiteral(resourceName: "notPreferSelected"), for: .normal)
           
            nextBtn.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
            nextBtn.isEnabled = true
            textField.text! = ""
            textField.isUserInteractionEnabled = false
        }else if sender.currentImage == #imageLiteral(resourceName: "notPreferSelected") {
            nextBtn.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
            preferNotToSayBtn.setImage(#imageLiteral(resourceName: "notPreferUnSelected"), for: .normal)
            nextBtn.isEnabled = false
            textField.isUserInteractionEnabled = true
        }
        
    }
    
    
    func nextButtonTapped(isNextButton:Bool) {
        let currentPref = myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex]
        
        preferenceVM.selectedPrefId = currentPref.prefId
        if textField.text!.count == 0 {
            preferenceVM.selectedValuesObj.append("NA")
        }else{
          preferenceVM.selectedValuesObj.append(textField.text!)
        }
        nextBtn.isUserInteractionEnabled = false
      
        if(isNextButton) {
            preferenceVM.updatePreference()
            updateLocalPrefsWithSelectedValues(selectedValues:[textField.text!])

        }
    }
    
    @IBAction func skipBtnAction(_ sender: Any) {
       // self.nextVcSetup()
        self.view.endEditing(true)
       Helper.makeTabbarAsRootVc(forVc: self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    func checkTotalCount(){
        totalCount = 0
        let category =  myPreference.count
        for i in 0..<category{
            totalCount = (totalCount +  myPreference[i].arrayOfSubPreference.count)
        }
    }
    
}


// MARK: -
extension PreferencesTFVC {

    func didGetResponse(){
        
        self.preferenceVM.preferenceVM_resp.subscribe(onNext: { success in
            self.nextBtn.isUserInteractionEnabled = true
            if(self.isForEditProfile) {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.nextVcSetup()
            }
            
        }, onError: { (error) in
            print(error)
            
        }).disposed(by: preferenceVM.disposeBag)
    }
    
    
    /// initial setup
    func initialSetup(){
        if isForEditProfile {
            lineWidth.constant = 0
        }else {
            lineWidth.constant = screenWidth/4
        }
        lineWidth.constant = screenWidth/4
        textField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        nextBtn.isEnabled = false
        nextBtn.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
        captionLabel.textColor = Colors.AppBaseColor
        calenderIcon.isHidden = true
        textField.tintColor = Colors.AppBaseColor
        topLineView.backgroundColor = Colors.AppBaseColor
        skipBtn.setTitle(StringConstants.skip(), for: .normal)
        setUpDetails()
        
    }
    
    /// based on the details type updates the screen
    func setUpDetails() {
        
        skipBtn.isHidden = false
        let categoryCount = myPreference[currentPrefCategoryIndex].arrayOfSubPreference.count
        let prefModel = myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex]
        switch prefModel.prefTypeId {
        case 5:
            //fetching crrent pref details
            let currentPref = myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex]
            titleLabel.text = currentPref.prefMainTitle.localizedCapitalized
            captionLabel.text = currentPref.prefSubTitle.localizedCapitalized
            
            if(currentPref.selectedValues.count>0){
                textField.text = currentPref.selectedValues[0]
                nextBtn.isEnabled = true
                nextBtn.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
            }
            
            textField.isUserInteractionEnabled = true
            textField.placeholder = currentPref.options[0].optionName
            self.descprtionLabel.text = StringConstants.thisIsHowInDatum()    //"Please enter your \(currentPref.prefSubTitle)"
            
            if isForEditProfile {
                lineWidth.constant = 0
            }else {
                lineWidth.constant = screenWidth/CGFloat(categoryCount - currentPrefSubCategoryIndex)
            }
            
            
            break
            
        default:
            print("")
        }
        
    }
    
    /// from the current screent to next screen
    func nextVcSetup(){
        
        if screenIndex+1 == totalCount {
            Helper.makeTabbarAsRootVc(forVc: self)
            
        }
        else if(currentPrefSubCategoryIndex == myPreference[currentPrefCategoryIndex].arrayOfSubPreference.count - 1) {
            //go to next category
            nextPrefCategoryIndex = currentPrefCategoryIndex+1
            
            if myPreference[nextPrefCategoryIndex].arrayOfSubPreference.count == 0 {
                if nextPrefCategoryIndex < totalCount {
                    nextPrefCategoryIndex += 1
                }
            }
            
            
            nextPrefSubCategoryIndex = 0
            gotoNextVC()
        }
        else {
            // go to next subCategory in current category.
            nextPrefCategoryIndex  = currentPrefCategoryIndex
            nextPrefSubCategoryIndex = currentPrefSubCategoryIndex+1
            gotoNextVC()
        }
        
    }
    
    
    func updateLocalPrefsWithSelectedValues(selectedValues:[String]) {
        
        
        let CurrentPref =  myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex]
        
        
        
        if  var data = UserDefaults.standard.object(forKey: "preferences") as? [String:Any] {
            if  var allCategories = data[ApiResponseKeys.myPreferences] as? [[String:Any]] {
                print("after update SelectedValues:",allCategories)
                for (categoryIndex,singleCategory) in allCategories.enumerated() {
                    
                    let loopCatTitle = singleCategory["title"] as! String
                    
                    if(CurrentPref.prefMainTitle == loopCatTitle) {
                        
                        
                        if var subCats = singleCategory["data"] as? [[String:Any]] {
                            
                            
                            for (subCatIndex,subCat) in subCats.enumerated() {
                                
                                var newSubCat = subCat
                                
                                
                                if let prefId =  subCat[ApiResponseKeys.preferenceId] as? String {
                                    if prefId == CurrentPref.prefId {
                                        newSubCat["selectedValues"] = selectedValues
                                        subCats[subCatIndex] = newSubCat
                                        allCategories[categoryIndex]["data"] = subCats
                                        print("before updating:",data)
                                        data[ApiResponseKeys.myPreferences] = allCategories
                                        print("after updating:",data)
                                        Helper.savePreferences(data:data)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if  var allCategories = data["myPrefrances"] as? [[String:Any]] {
                print("after update SelectedValues:",allCategories)
                for (categoryIndex,singleCategory) in allCategories.enumerated() {
                    
                    let loopCatTitle = singleCategory["title"] as! String
                    
                    if(CurrentPref.prefMainTitle == loopCatTitle) {
                        
                        
                        if var subCats = singleCategory["data"] as? [[String:Any]] {
                            
                            
                            for (subCatIndex,subCat) in subCats.enumerated() {
                                
                                var newSubCat = subCat
                                
                                
                                if let prefId =  subCat[ApiResponseKeys.preferenceId] as? String {
                                    if prefId == CurrentPref.prefId {
                                        newSubCat["selectedValues"] = selectedValues
                                        subCats[subCatIndex] = newSubCat
                                        allCategories[categoryIndex]["data"] = subCats
                                        print("before updating:",data)
                                        data[ApiResponseKeys.myPreferences] = allCategories
                                        print("after updating:",data)
                                        Helper.savePreferences(data:data)
                                    }
                                }
                            }
                        }
                    }
                }
            }
          
            
        }        //myPrefrances
    }
    
    func gotoNextVC(){
        let prefModel = myPreference[nextPrefCategoryIndex].arrayOfSubPreference[nextPrefSubCategoryIndex]
        switch prefModel.prefTypeId {
        case 5:
            pushToNextVC()
            
        case 1:
            pushVCToPreferenceTableVc(count: 2)
            break
        default:
            print("")
        }
        
    }
    
    func pushToNextVC(){
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.TextFieldTypeVC) as! PreferencesTFVC
        
        userBasics.isVcForSignUp = false
        userBasics.screenIndex = screenIndex+1
        userBasics.currentPrefCategoryIndex = self.nextPrefCategoryIndex
        userBasics.currentPrefSubCategoryIndex = self.nextPrefSubCategoryIndex
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
    //creates object of GenderPickerViewController and push that ViewController
    func pushVCToPreferenceTableVc(count: Int) {
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.PreferenceTable) as! PreferencesTableVC
        
        userBasics.screenIndex = screenIndex+1
        
        userBasics.currentPrefCategoryIndex = self.nextPrefCategoryIndex
        userBasics.currentPrefSubCategoryIndex = self.nextPrefSubCategoryIndex
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
    
}
// MARK: - UITextFieldDelegate
extension PreferencesTFVC: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField : UITextField) {
        
        if textField.text?.sorted().count != 0 {
            nextBtn.isEnabled = true
            nextBtn.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
            nextBtn.isUserInteractionEnabled = true
        }
        else{
            nextBtn.isEnabled = false
            nextBtn.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
        }
    }
}
