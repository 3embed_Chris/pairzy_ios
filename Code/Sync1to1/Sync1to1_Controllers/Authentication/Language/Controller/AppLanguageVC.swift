//
//  AppLanguageVC.swift
//  Datum
//
//  Created by 3 Embed on 12/03/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

class AppLanguageVC: UIViewController {

    let languages:[LanguageModel] = Languages.menuList
    var selectedIndex:IndexPath = IndexPath(row: 0, section: 0)
    
 @IBOutlet weak var screenTitle: UILabel!
 @IBOutlet weak var subTitle: UILabel!
 @IBOutlet weak var mainTableView: UITableView!
 @IBOutlet weak var nextBtn: UIButton!
    
 @IBAction func doneBtnAction(_ sender: Any) {
    //   Helper.showProgressIndicator(withMessage: StringConstants.Loading())
    let language:[String:Any] = ["code":self.languages[selectedIndex.row].code,
                                 "name":self.languages[selectedIndex.row].name]
    Helper.saveLanguageCode(data: language)
    _ = UIApplication.shared.delegate as! AppDelegate
    let langCode = Helper.getSelectedLanguage()
    OneSkyOTAPlugin.setLanguage(langCode.code)
    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
        appDelegate.window?.rootViewController?.presentedViewController!.dismiss(animated: true, completion: nil)
        (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
    }
    DispatchQueue.main.async(execute: {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let splash = storyboard.instantiateInitialViewController()
        
        let window = UIApplication.shared.keyWindow
        window?.rootViewController = splash
    })
}
    
    @IBAction func closeAction(_ sender: Any) {
     self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenTitle.text = StringConstants.selectLanguage()
        nextBtn.tintColor = Colors.AppBaseColor
        screenTitle.textColor = Colors.AppBaseColor
        checkSelectedLanguage()
        // Do any additional setup after loading the view.
    }
    
    func checkSelectedLanguage(){
        let langCode = Helper.getSelectedLanguage()
        for (langIndex,language) in languages.enumerated() {
            if language.code == langCode.code {
                selectedIndex.row = langIndex
            }
        }
    }
    
    
}

// MARK: - UITableViewDelegate,UITableViewDataSource
extension AppLanguageVC: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let picker = tableView.dequeueReusableCell(withIdentifier: "LanguageCell", for: indexPath) as? GenderPickerTableViewCell else { return UITableViewCell()}
        
        var flag = true
        
        if indexPath != selectedIndex {
            flag = false
        }
        picker.updateLanguage(language:languages[indexPath.row],selected:flag)
        
        return picker
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var array = [IndexPath]()
        if selectedIndex != nil {
            array.append(selectedIndex)
        }
        if selectedIndex != indexPath {
            array.append(indexPath)
        }
        
        selectedIndex = indexPath
        mainTableView.reloadRows(at: array, with: .fade)
        // self.nextButton.isEnabled = true
    }
}

