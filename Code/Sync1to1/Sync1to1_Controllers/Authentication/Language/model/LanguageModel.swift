//
//  LandingModel.swift
//  Datum
//
//  Created by 3 Embed on 12/03/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//


import UIKit
struct LanguageModel {
    var name = ""
    var code = ""
}


struct Languages {
    static var menuList: [LanguageModel] {
        
        var menu1 = LanguageModel()
        menu1.name = "Vietnamese"
        menu1.code = "vi"
        
        var menu2 = LanguageModel()
        menu2.name = "English"
        menu2.code = "en"
        
        let list:[LanguageModel] = [menu2]
        return list
    }
}
