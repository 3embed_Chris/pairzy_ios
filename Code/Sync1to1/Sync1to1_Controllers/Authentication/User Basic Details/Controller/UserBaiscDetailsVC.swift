//
//  UserBaiscDetailsVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 21/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit


enum detailsType {
    case EmailAddress
    case name
    case dateOfBirth
}

class UserBaiscDetailsVC: UIViewController {        // used for name screen ,dateOfBirth ,emailaddress
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var descprtionLabel: UILabel!
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var lineWidth: NSLayoutConstraint!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var calenderIcon: UIButton!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var emailAdressLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    

    
    var isVcForEdit = false
    var forEditProfile = false
    var isForFacebook = false
    //  var fbProfile:SocialProfile = Helper.getSocialProfile()
    
    var valueForEditProfile = ""
    var screenIndex = 0
    var currentPrefCategoryIndex = 0
    var currentPrefSubCategoryIndex = 0
    var nextPrefCategoryIndex = 0
    var nextPrefSubCategoryIndex = 0
    var totalCount = 0
    var email = ""
    let detailsVM = UserBasicDetailsVM()
    
    
    var basicDetails = detailsType.EmailAddress
    let screenWidth = UIScreen.main.bounds.size.width
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        didGetResponse()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
        if(forEditProfile) {
            if basicDetails == detailsType.EmailAddress {
                self.textField.autocapitalizationType = .none
                self.textField.keyboardType = .emailAddress
                self.textField.text = email
                
            }else if basicDetails == detailsType.dateOfBirth {
                let date =  Date(timeIntervalSince1970: TimeInterval(valueForEditProfile)!/1000)
                guard let dateString = DateHelper().getDateOfb(fromDate: date) else {
                    return
                }
                self.textField.text = dateString
            }
        }else if isForFacebook {
            if detailsVM.locAuth != nil {
                if basicDetails == detailsType.EmailAddress {
                    self.textField.autocapitalizationType = .none
                    self.textField.keyboardType = .emailAddress
                    self.textField.text = detailsVM.locAuth.email
                    updateNextButtonStatus(textField: textField)
                }else if basicDetails == detailsType.dateOfBirth {
                    self.textField.text = detailsVM.locAuth.dobString
                    updateNextButtonStatus(textField: textField)
                }else if basicDetails == detailsType.name{
                    self.textField.text = detailsVM.locAuth.name
                    updateNextButtonStatus(textField: textField)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        removeObserver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textField.becomeFirstResponder()
        if basicDetails == .dateOfBirth {
            self.CalenderAction(calenderIcon)
        }
        enableUserInteractionCheck()
    }
    
    func enableUserInteractionCheck() {
        nextBtn.isUserInteractionEnabled = true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        if self.basicDetails == .EmailAddress {
            self.navigationController?.popToRootViewController(animated: true)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func CalenderAction(_ sender: Any) {
        performSegue(withIdentifier: String(describing: DatePickerVC.self), sender: self)
        
    }
    
    

    
    @IBAction func nextButtonAction(_ sender: Any) {
        if(forEditProfile) {
            let dbObj = Database.shared
            var selectedType = self.basicDetails
            
            switch selectedType {
            case .dateOfBirth:
                let currentDate = self.detailsVM.locAuth.dateOfBirth           //Date()
                let currentTimeStamp = currentDate.millisecondsSince1970
                dbObj.userDataObj.dateOfBirthTimeStamp = currentTimeStamp
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                self.navigationController?.popViewController(animated: true)
                return
                
            case .EmailAddress:
                dbObj.userDataObj.emailId = self.textField.text
                
                if textField.text?.count != 0 {
                    detailsVM.locAuth.email = textField.text!
                }
                
                detailsVM.requestForEmailIdCheck()
                break
            default:
                break
            }
            
        }else {
            
            nextVcSetup()
        }
      nextBtn.isUserInteractionEnabled = false
        
        
    }
    @IBAction func skipBtnAction(_ sender: Any) {
        self.nextButtonAction(UIButton())
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == String(describing: DatePickerVC.self) {
            
            if let viewController: DatePickerVC = segue.destination as? DatePickerVC {
                
                viewController.Response_Picker.subscribe(onNext: { success in
                    print(success)
                    self.detailsVM.locAuth.dateOfBirth = success
                    
                    guard let dateString = DateHelper().getDateOfb(fromDate: success) else {
                        return
                    }
                    print("\(dateString)")
                    self.textField.text = dateString
                    if self.textField.text?.sorted().count != 0 {
                        self.nextBtn.isEnabled = true
                        self.nextBtn.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
                    }
                    else{
                        self.nextBtn.isEnabled = false
                        self.nextBtn.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
                    }
                    
                })
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}


// MARK: -
extension UserBaiscDetailsVC {
    /// observer to the UserBasicDetailsVM
    func didGetResponse(){
        detailsVM.userBasicDetailsVM_resp.subscribe(onNext: { success in
            switch success {
            case UserBasicDetailsVM.ResponseType.emailRegist:
                Helper.showAlertWithMessage(withTitle: StringConstants.error(), message: self.detailsVM.message, onViewController: self)
                self.nextBtn.isUserInteractionEnabled = true
                break
            case UserBasicDetailsVM.ResponseType.emailNotRegist:
                
                if self.forEditProfile {
                    self.navigationController?.popViewController(animated: true)
                }else {
                    
                    
                    self.pushVCForType(detailsType: .name)
                }
                self.nextBtn.isUserInteractionEnabled = true
                break
                
            }
            
        })
    }
    
}
