//
//  UserBasicDetailsUIE.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 13/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

extension UserBaiscDetailsVC {
    
    
    /// initial setup
    func initialSetup(){
        if forEditProfile {
            lineWidth.constant = 0
        }else {
            lineWidth.constant = screenWidth/4
        }
        textField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        nextBtn.isEnabled = false
        nextBtn.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
        calenderIcon.isHidden = true
        detailsVM.view = self.view
        detailsVM.doneBtn = self.nextBtn
        titleLabel.text = StringConstants.enterYour()
        titleLabel.textColor = Colors.PrimaryText
        captionLabel.textColor = Colors.AppBaseColor
        textField.tintColor = Colors.AppBaseColor
        topLineView.backgroundColor = Colors.AppBaseColor
        backBtn.tintColor = Colors.AppBaseColor
        skipBtn.setTitle(StringConstants.skip(), for: .normal)
        setUpDetails()
        
    }
    
    /// based on the details type updates the screen
    func setUpDetails() {
        
        skipBtn.isHidden = true
        switch basicDetails {
        case .name:
            titleLabel.text = StringConstants.enterYour()
            self.captionLabel.text = StringConstants.name()
            self.descprtionLabel.text = StringConstants.thisIsHowInDatum()
            self.textField.placeholder = StringConstants.enterYourName()
            
            if forEditProfile {
                lineWidth.constant = 0
            }else {
                
                UIView.animate(withDuration: 1.0, animations: {
                    self.lineWidth.constant = (self.screenWidth/4)*2
                    self.topLineView.layoutIfNeeded()
                }){ (Bool) in
                    
                }
                
            }
            
            calenderIcon.isHidden = true
            textField.isUserInteractionEnabled = true
            textField.keyboardType = UIKeyboardType.default
            
        case .dateOfBirth:
            titleLabel.text = StringConstants.enterYour()
            print("fetching DOB")
            self.captionLabel.text = StringConstants.birthDay()
            self.descprtionLabel.text = StringConstants.DOBDescrption()
            self.textField.keyboardType = .numberPad
            
            if forEditProfile {
                lineWidth.constant = 0
            }else {
                
                UIView.animate(withDuration: 1.0, animations: {
                    self.lineWidth.constant = (self.screenWidth/4)*3
                    self.topLineView.layoutIfNeeded()
                }){ (Bool) in
                    
                }
            }
            self.textField.placeholder = StringConstants.DOBPlaceHolder()
            calenderIcon.isHidden = false
            textField.isUserInteractionEnabled = false
            
        case .EmailAddress:
            titleLabel.text = StringConstants.enterYour()
            textField.autocapitalizationType = .none
            textField.keyboardType = UIKeyboardType.emailAddress
            self.captionLabel.text = StringConstants.emailAddress()
            textField.isUserInteractionEnabled = true
            print("fetching emial address")
            self.descprtionLabel.text = StringConstants.pleaseEnterEmail()
            if forEditProfile {
                lineWidth.constant = 0
            }else {
                
                UIView.animate(withDuration: 1.0, animations: {
                    self.lineWidth.constant = self.screenWidth/4
                    self.topLineView.layoutIfNeeded()
                }){ (Bool) in
                    
                }
            }
            
            calenderIcon.isHidden = true
            
        }
    }
    
    /// from the current screent to next screen
    func nextVcSetup(){
        switch basicDetails {
        case .EmailAddress:
            if textField.text?.count != 0 {
                detailsVM.locAuth.email = textField.text!
            }
              detailsVM.requestForEmailIdCheck()
        case .name:
            if textField.text?.count != 0 {
                detailsVM.locAuth.name = textField.text!
            }
            self.pushVCForType(detailsType: .dateOfBirth)
            
        case .dateOfBirth:
            nextBtn.isEnabled = true
            pushVCToGenderVc() //1 - gender
        }
    }
    
    
    //creates object of UserBaiscDetailsVC and push that ViewController
    func pushVCForType(detailsType:detailsType) {
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.userBasicSI) as! UserBaiscDetailsVC
        userBasics.basicDetails = detailsType
        userBasics.isForFacebook = self.isForFacebook
        userBasics.detailsVM.locAuth = self.detailsVM.locAuth
        
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
    //creates object of GenderPickerViewController and push that ViewController
    func pushVCToGenderVc() {
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.genderPickerSI) as! GenderPickerViewController
        userBasics.genderPickerVM.locAuth = self.detailsVM.locAuth
        userBasics.isForFacebook = self.isForFacebook
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
}
