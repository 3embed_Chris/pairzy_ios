//
//  UserBasicDetailsTFE.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 13/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit


// MARK: - UITextFieldDelegate
extension UserBaiscDetailsVC: UITextFieldDelegate {
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.basicDetails != .name {
            
            if range.length == 1 {
                return true
            }
            if string.isBlank{
                return false
            }else  {
                if range.length == 0 || range.length == 1{
                    return true
                }
                else {
                    return false
                }
            }
        }
        
        return true
        
    }
    
    @objc func textFieldDidChange(_ textField : UITextField) {
        if isVcForEdit {
            switch basicDetails {
            case .EmailAddress:
                updateNextButtonStatus(textField: textField)
                break
            case .name:
                updateNextButtonStatus(textField: textField)
                break
            case .dateOfBirth:
                break
            }
        }
        else {
            updateNextButtonStatus(textField: textField)
        }
    }
    
    
    
    func updateNextButtonStatus(textField:UITextField) {
        
        switch basicDetails {
        case .EmailAddress:
            if Helper.isValidEmail(text: textField.text!) {
                
                self.nextBtn.isEnabled = true
                self.detailsVM.emailId = self.textField.text!
                self.nextBtn.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
            }else {
                self.nextBtn.isEnabled = false
                self.nextBtn.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
            }
            break
        default :
            if (textField.text?.sorted().count)! != 0 && (textField.text?.count)! >= 2 {
                nextBtn.isEnabled = true
                nextBtn.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
            }
            else{
                nextBtn.isEnabled = false
                nextBtn.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
            }
        }
    }
    
    
    
}
