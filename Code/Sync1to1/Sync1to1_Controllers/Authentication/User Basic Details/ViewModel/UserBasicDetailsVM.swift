//
//  UserBasicDetailsVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 13/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class UserBasicDetailsVM: NSObject {
    
    enum ResponseType: Int{
        case emailRegist = 0
        case emailNotRegist = 1
      
    }
   
    var authentication = Authentication()
    let disposeBag = DisposeBag()
    let userBasicDetailsVM_resp = PublishSubject<ResponseType>()
    var emailId = ""
    var message = ""
    var view:UIView? = nil
    var doneBtn:UIButton? = nil
    var locAuth = LocalAuthentication(data: [:])
    
    func requestForEmailIdCheck() {
        Helper.showProgressIndicator(withMessage: StringConstants.Verifying())
        let params = [ServiceInfo.email: locAuth.email] as [String:Any]
        let requestData = RequestModel().signUpRequestDetails(signupDetails:params)
        let apiCall = Authentication()
        apiCall.requestEmailCheck(emailData:requestData)
        apiCall.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                self.handleEmailCheckAPIResponse(responseModel: response)
            }, onError: { error in
                Helper.showAlertMessageViewFromTopNavBar(showOnView: self.view!, message: error.localizedDescription )
                self.doneBtn?.isUserInteractionEnabled = true
            })
            .disposed(by:disposeBag)
    }
    
    func handleEmailCheckAPIResponse(responseModel:ResponseModel) {
        
        let statuscode:API.ErrorCode = API.ErrorCode(rawValue: responseModel.statusCode)!
        switch (statuscode) {
        case .Success:
            // self.userBasicDetailsVM_resp.onNext(.emialRegist)
            //Helper.showAlertWithMessageOnwindow(withTitle: Message.Error, message:responseModel.response["message"] as! String )
            message  = responseModel.response["message"] as! String
            self.userBasicDetailsVM_resp.onNext(.emailRegist)
            print(responseModel.response)
        case .PreconditionFailed:
            self.userBasicDetailsVM_resp.onNext(.emailNotRegist)
            print(responseModel.response)
        default:
            print(responseModel.response)
        }
    }
    
}
