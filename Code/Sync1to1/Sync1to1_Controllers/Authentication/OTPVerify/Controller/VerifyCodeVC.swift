//
//  VerifyCodeVC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 21/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import KRProgressHUD

class VerifyCodeVC: UIViewController {
    
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var didNotGetCode: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var verificationLabel: UILabel!
    @IBOutlet weak var separator: UIView!
    
    let verifyCodeVM = VerifyCodeVM()
    var isForEdit:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        didGetResponse()
        nextBtn.isEnabled = false
        nextBtn.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
        otpTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        verifyCodeVM.doneBtn = self.nextBtn
        verifyCodeVM.view = self.view
        otpTextField.placeholder = StringConstants.enterOtp()
        backBtn.tintColor = Colors.AppBaseColor
        mobileNumberLabel.textColor = Colors.PrimaryText
        mobileNumberLabel.text = StringConstants.mobileNumber()
        verificationLabel.textColor = Colors.AppBaseColor
        otpTextField.textColor = Colors.PrimaryText
        didNotGetCode.setTitle(StringConstants.didnotGetCode(), for: .normal)
        didNotGetCode.setTitleColor(Colors.PrimaryText, for: .normal)
        separator.backgroundColor = Colors.SeparatorDark
        verificationLabel.text = StringConstants.verification()
        otpTextField.placeholder = StringConstants.enterOtp()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        didNotGetCode.isUserInteractionEnabled = true
        addObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserver()
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        nextBtn.isUserInteractionEnabled = true
        didNotGetCode.isUserInteractionEnabled = true
       // otpTextField.becomeFirstResponder()
        self.perform(#selector(self.autoFillOtp), with: self, afterDelay: 0.2)
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        
        var type = 1
        if otpTextField.text?.sorted().count == 6 {
            
            if(verifyCodeVM.otpVerificationForSignUp) {
                type = 1
                
                //userData.countryCode+userData.mobileNumber
            } else if isForEdit {
                type = 3
                
            }
            else {
                type = 2
                
                //params[ServiceInfo.phoneNumberWIthCode]= phoeNumberForResetPasswod
            }
            
            let otpString = otpTextField.text!
            
            
            
            
            nextBtn.isUserInteractionEnabled = false
            Helper.showProgressIndicator(withMessage: StringConstants.Verifying())
            
            if isForEdit {
                verifyCodeVM.isForEdit = true
                verifyCodeVM.updatePhoneNumber(otp: otpString)  //
               // verifyCodeVM.requestToVerifyCode(type: type ,otp: otpString)
            }else {
               verifyCodeVM.requestToVerifyCode(isNewRegist: type ,otp: otpString)
                
            }
            
            
        }
    }
    
    @IBAction func resendOtpAction(_ sender: Any) {
        didNotGetCode.isUserInteractionEnabled = false
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.ResendOtpVC) as! ResendOtpVC
        userBasics.locAuth = self.verifyCodeVM.locAuth
        self.view.endEditing(true)
        self.navigationController?.pushViewController(userBasics, animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
   @objc func autoFillOtp(){
        otpTextField.text = "111111"
        nextButtonAction(nextBtn)
    }
 
}

extension VerifyCodeVC {
    
    
    
    func didGetResponse(){
        verifyCodeVM.verifyCodeVM_resp.subscribe(onNext: { [weak self] success in
            if success == VerifyCodeVM.UserType.forEditProfile {
                Helper.hideProgressIndicator()
                self?.dismiss(animated: true, completion: nil)
            }
            if success == VerifyCodeVM.UserType.newUser {
                 Helper.hideProgressIndicator()
                self?.gotoNextVC()
            }else if success == VerifyCodeVM.UserType.oldUser{
                   Helper.hideProgressIndicator()
                Helper.makeTabbarAsRootVc(forVc: self ?? UIViewController())
//                UIView.animate(withDuration: 0.3, animations: {
//                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
//                        appDelegate.window?.rootViewController?.presentedViewController!.dismiss(animated: true, completion: {
//
//                        })
//            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
//                    }
//                })
       
            }else {
                
//                DispatchQueue.main.async(){
//                     Helper.hideProgressIndicator()
//                }
              
                KRProgressHUD.dismiss() {
                    self?.showPopup(message: self?.verifyCodeVM.message ?? "")
                }
                
                self?.nextBtn.isUserInteractionEnabled = true
                self?.didNotGetCode.isUserInteractionEnabled = true
               
                
             //   Helper.showAlertWithMessage(withTitle: Message.Error, message: self.verifyCodeVM.message, onViewController: self)
            }
        })
    }
    
    //emailVC
    func gotoNextVC(){
        self.view.endEditing(true)
        self.verifyCodeVM.locAuth.otp = otpTextField.text!
        let userBasics = Helper.getSBFromVC(vc: self).instantiateViewController(withIdentifier:StoryBoardIdentifier.userBasicSI) as! UserBaiscDetailsVC
        userBasics.basicDetails = .EmailAddress
        userBasics.detailsVM.locAuth = self.verifyCodeVM.locAuth
        
        self.navigationController?.pushViewController(userBasics, animated: true)
        
    }
    
    func showPopup(message: String){
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let tryAgainView = TryAgainViewC(frame: CGRect(x: 0, y: 0 ,width : window.frame.width, height: window.frame.height))
        tryAgainView.loadPopUpView(title: message)
        tryAgainView.delegate = self
        window.addSubview(tryAgainView)
        window.makeKeyAndVisible()
    }
    
    
}
extension VerifyCodeVC : TryAgainPopUpDelegate {
    func tryAgainBtnPresse() {
        Helper.hideProgressIndicator()
    }
  
}


