//
//  VerifyCodeTFE.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 13/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

extension VerifyCodeVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    @objc func textFieldDidChange(_ textField : UITextField) {
        if otpTextField.text?.sorted().count == 6 {
            self.view.endEditing(true)
            nextBtn.isEnabled = true
            nextBtn.setImage(#imageLiteral(resourceName: "next_on"), for: .normal)
            self.nextButtonAction(self.nextBtn)
            
        }else {
            nextBtn.isEnabled = false
            nextBtn.setImage(#imageLiteral(resourceName: "next_off"), for: .normal)
        }
    }
    
}
