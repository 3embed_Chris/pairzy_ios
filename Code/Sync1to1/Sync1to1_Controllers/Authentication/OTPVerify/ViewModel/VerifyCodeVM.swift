//
//  VerifyCodeVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 13/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class VerifyCodeVM: NSObject {
    
    enum UserType: Int {
        case newUser = 0
        case oldUser = 1
        case respFalse = 3
        case forEditProfile = 4
    }
    
    var authentication = Authentication()
    let disposeBag = DisposeBag()
    let verifyCodeVM_resp = PublishSubject<UserType>()
    var message = ""
    
    var phoneNumbObj:MobileNumberDetails? = nil
    var otpVerificationForSignUp:Bool = false
    var phoeNumberForResetPasswod:String = ""
    var locAuth = LocalAuthentication(data: [:])
    var doneBtn: UIButton? = nil
    var view:UIView? = nil
    
    var isForEdit:Bool = false
    
    /// method for requesting api.
    ///
    /// - Parameter params: contains all the api request details.
    func requestToVerifyCode(isNewRegist: Int , otp: String) {
        
     
        var pushToken = ServiceInfo.constantPushToken
        if let token = UserDefaults.standard.value(forKey: SyUserdefaultKeys.kNSUPushNotificationTokenKey) as? String
        {
            pushToken = token
        }
        var appVersion = "1.0"
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            appVersion = version
        }
        
        var voipPushToken = ""
        
        if let voipPush = UserDefaults.standard.value(forKey:"callPushToken") as? String {
            voipPushToken = voipPush
        }
        
        var apnsPushToken = ""
        if let apnsPush = UserDefaults.standard.value(forKey:"deviceTokenString") as? String {
            apnsPushToken = apnsPush
        }
        
        let params = [ ServiceInfo.OTP :(otp as NSString).integerValue,
                       ServiceInfo.type: Double(isNewRegist),
                       ServiceInfo.phoneNumberWIthCode :(self.locAuth.countryCode) + (self.locAuth.phoneNumber),
                       ServiceInfo.PushToken:pushToken as AnyObject,
                       ServiceInfo.voipiospush: voipPushToken,
                       ServiceInfo.apnsPush: apnsPushToken,
                       ServiceInfo.DeviceType:"1" as AnyObject,
                       ServiceInfo.deviceMakeKey:UIDevice.current.name as AnyObject,
                       ServiceInfo.deviceModelKey: Helper.DeviceModel,                         //UIDevice.current.model as AnyObject,
            ServiceInfo.appVersion        :appVersion,
            ServiceInfo.deviceOs          : UIDevice.current.systemVersion,
            ServiceInfo.deviceId:UIDevice.current.identifierForVendor?.uuidString as AnyObject] as [String:Any]
        
        
        
        
        
        let requestData = RequestModel().verificationCodeRequestDetails(verifyCodeDetails:params)
        let apiCall = Authentication()
        apiCall.requestVerificationCode(verificationCodeData:requestData)
        
        apiCall.subject_response
            .subscribe(onNext: {response in
               Helper.hideProgressIndicator()
                self.handleVerifyCodeResponse(responseModel: response)
            }, onError: { (error) in
                print(error)
                Helper.hideProgressIndicator()
                Helper.showAlertMessageViewFromTopNavBar(showOnView: self.view!, message: error.localizedDescription )
                self.doneBtn?.isUserInteractionEnabled = true
            }).disposed(by: disposeBag)
    }
    
    func updatePhoneNumber(otp:String){
        
        let paramsForPatch = [
            ServiceInfo.CountCode: self.locAuth.countryCode,
            ServiceInfo.phoneNumberWIthCode : self.locAuth.phoneNumber,
            ServiceInfo.OTP :(otp as NSString).integerValue,
            ServiceInfo.deviceId:UIDevice.current.identifierForVendor?.uuidString as AnyObject] as [String:Any]
        
        
        let apiCall = Authentication()
        apiCall.requestToPatchMobileNumber(profileDetail:paramsForPatch)
        
        apiCall.subject_response
            .subscribe(onNext: {response in
                  Helper.hideProgressIndicator()
                guard let statuscode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode) else {
                    return
                }
                if statuscode == API.ErrorCode.Success {
                    self.verifyCodeVM_resp.onNext(.forEditProfile)
                }else {
                    self.message =  response.response[LoginResponse.errorMessage] as! String
                    self.verifyCodeVM_resp.onNext(.respFalse)
                }
            }, onError: { (error) in
                print(error)
                Helper.showAlertMessageViewFromTopNavBar(showOnView: self.view!, message: error.localizedDescription )
                self.doneBtn?.isUserInteractionEnabled = true
            }).disposed(by: disposeBag)
        
    }
//    isPassportLocation = 0;
//    job = Bansn;
//    location =         {
//    latitude = "13.02860712168089";
//    longitude = "77.58949709132089";
//    };
    
    /// handle verify code api response
    ///
    /// - Parameter responseModel: contains api response and status code.
    func handleVerifyCodeResponse(responseModel:ResponseModel) {
        
        //  let statuscode = responseModel.statusCode
        guard let statuscode:API.ErrorCode = API.ErrorCode(rawValue: responseModel.statusCode) else {
            return
        }
        switch (statuscode) {
        case .Success:
            let response = responseModel.response
            if let dict = response["data"] as? [String:Any]{
                
                
                if let count = dict[profileData.count] as? [String:Any]{
                    Helper.saveCountsObj(data: count)
                }
                
                if let userType = dict["isNewUser"] as? Int {
                    if userType == 1 {
                        self.verifyCodeVM_resp.onNext(.newUser)
                    }
                    else {
                        self.verifyCodeVM_resp.onNext(.oldUser)
                        //Helper.getIsFeatureExist()

                       //  Helper.storeIsFeatureExist(isExist: false)
                        if let isFeatureExist = dict["isPassportLocation"] as? Int {
                            if isFeatureExist == 0 {
                                 Helper.storeIsFeatureExist(isExist: false)
                            }else {
                                Helper.storeIsFeatureExist(isExist: true)
                                
                            }
                                let locAddr  = Location(data: [:])
                                
                                if let loc = dict["location"] as? [String:Any] {
                                    if let temp = loc["latitude"] as? Double {
                                        locAddr.latitude = temp
                                        
                                    }
                                    if let temp = loc["longitude"] as? Double {
                                        locAddr.longitude = temp
                                    }
                                    if let temp = loc["address"] as? String {
                                        locAddr.name = temp
                                    }
                                    if let temp = loc["address"] as? String {
                                        locAddr.fullText = temp
                                        locAddr.secondaryText = temp
                                        locAddr.locationDescription = temp
                                    }
                                   
                                    if isFeatureExist == 0 {
                                        Utility.saveCurrentAddress(lat:Float(locAddr.longitude), long:Float(locAddr.latitude), address:locAddr)
                                    }
                                    
                                    Helper.saveFeaturLocation(data: locAddr)
                                }
                            }
                        
                        
                        
                        if let sessionToken = dict["token"] as? String{
                            UserLoginDataHelper.saveUserToken(token:sessionToken)
                        }
                        
                        //   Saving userId In User Default After Login Response Came
                        if let userId = dict["_id"] as? String {
                            UserLoginDataHelper.saveUserId(userId: userId)
                        }
                        if let userId = dict["userId"] as? String {
                            UserLoginDataHelper.saveUserId(userId: userId)
                        }
                        //making userdata object.
                        let userData = User.init(userDataDictionary: dict)
                        
                        //saving userdata in core data.
                        Database.shared.addUserdataToDatabase(withUserdata:userData)
                        
                        
                        if let dataForSubscrption =  responseModel.response["data"] as? [String:Any] {
               
                            if let isUserPurchased = dataForSubscrption["subscription"] as? [Any] {
                                if isUserPurchased.count > 0 {
                                    if let purchaseDetails = isUserPurchased[0] as? [String:Any] {
                                        if let isPurchased = purchaseDetails["subscriptionId"] as? String,isPurchased == "Free Plan" {
                                            Helper.saveIsUserPurchased(flag: false)
                                        } else {
                                            Helper.saveIsUserPurchased(flag: true)
                                        }
                                        Helper.savePurchasedPlan(data:purchaseDetails)
                                    }
                                }
                                print("data for subscrption")
                            }
                            
                            
                            
                        }
                        
                        
                        //updating signupData in couchDb.
                        let couchbaseObj = Couchbase.sharedInstance
                        let usersDocVMObject = UsersDocumentViewModel(couchbase: couchbaseObj)
                        usersDocVMObject.updateUserDoc(data: dict, withLoginType: "1")
                        
                        let dataDict = responseModel.response["data"] as! [String:Any]
                        let newDict =  Helper.nullKeyRemoval(data: dataDict)
                        Helper.savePreferences(data: newDict)
                        //subscribing For Push notification topic.
                        let appDelegateObj = UIApplication.shared.delegate! as! AppDelegate
                        appDelegateObj.subscribeForPushNotificatiionTopic()
                        Helper.showCoinsDeductPopup(flag: true, popUpFor: .forAll)
                        MQTT.sharedInstance.disconnectMQTTConnection()
                        
                        if let userID = Helper.getMQTTID() {
                            MQTT.sharedInstance.connect(withClientId: userID)
                            
                        }
                    }
                }
                
            }            
            break
            
        default:
            message =  responseModel.response[LoginResponse.errorMessage] as! String
            self.verifyCodeVM_resp.onNext(.respFalse)
            
            //  Helper.showAlertWithMessageOnwindow(withTitle: Message.Error, message: responseModel.response[LoginResponse.errorMessage] as! String)
        }
    }
}
