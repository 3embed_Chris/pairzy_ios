//
//  DatePickerVC.swift
//  Loopz
//
//  Created by 3Embed on 29/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class DatePickerVC: UIViewController {

    var ModeDate = true
    var TitleString = StringConstants.dateOfBirth()
    var Response_Picker = PublishSubject<Date>()
    
    
    @IBOutlet weak var headLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setUI()
      addingMinMaxToDatePicker()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.3) {
            self.view.backgroundColor = Helper.getUIColor(color: "484848").withAlphaComponent(0.2)
            self.heightConstraint.constant = 260
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        }
    }
    
    
    

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    ///Button Actions
    @IBAction func closeAction(_ sender: Any) {
        self.view.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.3) {
            self.heightConstraint.constant = 0
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func doneAction(_ sender: Any) {
        
        //sending time stamp in milliseconds
        let epochValueForSelectedDate = datePicker.date.timeIntervalSince1970*1000
        
        let  presentTime = Date().timeIntervalSince1970*1000
        if ModeDate == false {
//        if epochValueForSelectedDate/1000 - presentTime/1000 < 7200 {
//            Helper.showAlertWithMessage(withTitle: "Error", message: "Please shedule the date atleast after two hours from present time.", onViewController:self)
//            return
//        }
    }
        Response_Picker.onNext(self.datePicker.date)
        self.dismiss(animated: true, completion: nil)
    }
    
}
