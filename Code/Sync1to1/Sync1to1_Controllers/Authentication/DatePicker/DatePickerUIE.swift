//
//  DatePickerUIE.swift
//  Loopz
//
//  Created by 3Embed on 29/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

extension DatePickerVC {
    func setUI() {
        
        self.heightConstraint.constant = 0
        headLabel.text = TitleString
        doneButton.setTitle(StringConstants.doneLower(), for: .normal)
        if ModeDate {
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        }else{
            datePicker.datePickerMode = .time
            datePicker.maximumDate = Date()
        }
        
        // let date = Calendar.current.date(byAdding: .year, value: -4, to: Date())
        
        
    }
    
    
    
    /// adding Maximum and Minimum dates for date picker
    func addingMinMaxToDatePicker() {
        
        self.heightConstraint.constant = 0
        headLabel.text = TitleString
        
        if ModeDate {
            datePicker.datePickerMode = .date
            
            var components = DateComponents()
            components.year = -Int(AppSettings.MaximumAge)!
            let minDate = Calendar.current.date(byAdding: components, to: Date())
            
            components.year =  -Int(AppSettings.MinimuAge)!
            let maxDate = Calendar.current.date(byAdding: components, to: Date())
            
            //setting default age  -- maximum age (age less than 18 years from current date).
            datePicker.date = Calendar.current.date(byAdding: components, to: Date())!
            
            datePicker.minimumDate = minDate
            datePicker.maximumDate = maxDate
            
            
        }else{
            datePicker.datePickerMode = .dateAndTime
              datePicker.minimumDate = Date()
        }
        
        
    }
}
