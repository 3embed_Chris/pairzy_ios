//
//  UserData.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

struct User
{
    var firstName: String = ""
    var location : String = ""
    var preferredSex : String = ""
    var gender : String = ""
    var mobileNumber : String = ""
    var emailID    : String = ""
    var countryCode : String = ""
    var latitude : String = ""
    var longitude : String = ""
    var heightInCM : Float = 0.0
    var dateOfBirth : String = ""
    var profilePicture : String = ""
    var profileVideo : String = ""
    var otherVideos : [String] = []
    var otherPics : [String] = []
    var userId: String = ""
    var dateOfBirthTimeStamp:String = ""
    var aboutYou:String = ""
    var work:String = ""
    var job: String = ""
    var education = ""
    var dontShowMyAge = false
    var dontShowMyDist = true
    var instaGramProfileId = ""
    var instaGramToken = ""
    
  
    var userOtherLocations : [String:Any] = [:] // latitude,longitude,add name and add details(full address)
    var featureLocation :[String:Any] = [:] // latitude,longitude,add name and add details(full address)
    var currentLocation:[String:Any] = [:] // latitude,longitude,add name and add details(full address)
    
    init(userDataDictionary:[String:Any]) {
        
        if let firstName = userDataDictionary[ServiceInfo.Fname] as? String{
            self.firstName = firstName
        }
        
        if let userId = userDataDictionary[ServiceInfo.userId] as? String{
            self.userId = userId
        }
       
        if let gender = userDataDictionary[ServiceInfo.Gender] as? String{
            self.gender = gender
        }
        
        if let dateOfBirt = userDataDictionary[ServiceInfo.DOB] as? Int{
            self.dateOfBirthTimeStamp = String(dateOfBirt)
        }
        
        
        if let abou = userDataDictionary[ServiceInfo.aboutYou] as? String{
            self.aboutYou = abou
        }
        
        
        
        if let work = userDataDictionary["work"] as? String{
            self.work = work
        }
        
        if let education = userDataDictionary["education"] as? String{
            self.education = education
        }
        
        if let job = userDataDictionary["job"] as? String{
            self.job = job
        }
        
        if let dontShowMyAge = userDataDictionary["dontShowMyAge"] as? Bool{
            self.dontShowMyAge = dontShowMyAge
        }
        
        if let dontShowMyDist = userDataDictionary["dontShowMyDist"] as? Bool{
            self.dontShowMyDist = dontShowMyDist
        }
        
        
        
        //FOR SIGNUP
        if let dateOfBirth = userDataDictionary[ServiceInfo.DateOfbirth] as? String{
            self.dateOfBirth = dateOfBirth
        }
        
        //FOR LOGIN
        if let dateOfBirth = userDataDictionary[ServiceInfo.DateOfbirth] as? Int{
            self.dateOfBirth = String(dateOfBirth)
        }
        
        //FOR LOGIN
        if let dateOfBirth = userDataDictionary[ServiceInfo.DateOfbirth] as? Double{
            self.dateOfBirth = String(dateOfBirth)
        }
        
        if let emailID = userDataDictionary[ServiceInfo.Email] as? String{
            self.emailID = emailID
        }
        
        if let preferredSex = userDataDictionary[ServiceInfo.PrefSex] as? String{
            self.preferredSex = preferredSex
        }
        
        if let mobileNumber = userDataDictionary[ServiceInfo.phoneNumber] as? String{
            self.mobileNumber = mobileNumber
        }
        
        if let heightInCM = userDataDictionary[ServiceInfo.Height] as? Float{
            self.heightInCM = heightInCM
        }
        
        if let heightInCM = userDataDictionary[ServiceInfo.Height] as? String{
            self.heightInCM = Float(heightInCM)!
        }
        
        if let latitude = userDataDictionary[ServiceInfo.latitude] as? String{
            self.latitude = latitude
        }
        
        if let longitude = userDataDictionary[ServiceInfo.longitude] as? String{
            self.longitude = longitude
        }
        
        if let locationName = userDataDictionary[ServiceInfo.LocationName] as? String{
            self.location = locationName
        }
        
        if let profileVideo = userDataDictionary[ServiceInfo.profileVideo] as? String{
            self.profileVideo = profileVideo
        }
        
        if let otherVideos = userDataDictionary[ServiceInfo.otherfileVideo] as? [String]{
            self.otherVideos = otherVideos
        }
        
        if let otherPics = userDataDictionary[ServiceInfo.otherProfilePic] as? [String]{
            self.otherPics = otherPics
        }
        
        if let profilePic = userDataDictionary[ServiceInfo.profilePic] as? String{
            self.profilePicture = profilePic
        }
        
        if let instaToken = userDataDictionary[ServiceInfo.InstaGramToken] as? String{
            self.instaGramToken = instaToken
        }
        
        if let instaId = userDataDictionary[ServiceInfo.InstaGramProfileId] as? String{
            self.instaGramProfileId = instaId
        }
        
        if(!self.profileVideo.isBlank) {
             self.otherVideos.append(self.profilePicture)
        }
        
        if(!self.profilePicture.isBlank) {
            self.otherPics.append(self.profilePicture)
        }
        
        
        if let countryCode = userDataDictionary[ServiceInfo.CountCode] as? String {
            self.countryCode = countryCode
        }
    
    }
    
    
    
    
}

