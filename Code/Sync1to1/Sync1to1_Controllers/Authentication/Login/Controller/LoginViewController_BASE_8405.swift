//
//  LoginViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 02/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Foundation
import Firebase
import RxSwift
import RxAlamofire


class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginViewOutlet: LoginView!
    let disposeBag = DisposeBag()
    
    
    
    struct Constant{
        static let loggingInText = "Logging in..."
        static let constantPushToken = "iOS Simulator Push Token"
        static let deviceModelKey = "deviceModel"
        static let deviceMakeKey = "deviceMake"
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String
        {
            self.loginViewOutlet.countryCodeLabel.text = VNHCountryPicker.dialCode(code: countryCode).dialCode
            self.loginViewOutlet.countryFlagImageView.image = VNHCountryPicker.dialCode(code: countryCode).flag
        }
        let navleftItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back_btn_off"), style:.plain, target: self, action: #selector(backcliked))
        self.navigationItem.setLeftBarButton(navleftItem, animated: true)
        
        //for testing
        self.loginViewOutlet.phoneNumberTextFieldOutlet.text = "8553328349"
        self.loginViewOutlet.passwordTextFieldOutlet.text = "password"
        
    }
    
    func backcliked() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    
    ///  when user touches view textField editing is ended.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func forgotBtncliked(_ sender: Any)
    {
        
        
        let actionSheetController = UIAlertController (title: "RESET PASSWORD", message: "Select option to reset password", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        //Add Cancel-Action
        actionSheetController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        //Add reset password by phonenumber-Action
        actionSheetController.addAction(UIAlertAction(title: "Phone Number", style: UIAlertActionStyle.default, handler: { (actionSheetController) -> Void in
            self.performSegue(withIdentifier: IDENTIFIER.GotoResetPasword, sender: true)
        }))
        
        //Add reset password by email-Action
        actionSheetController.addAction(UIAlertAction(title: "Email", style: UIAlertActionStyle.default, handler: { (actionSheetController) -> Void in
            //segue to reset password
            self.performSegue(withIdentifier: IDENTIFIER.GotoResetPasword, sender: false)
        }))
        
        
        //present actionSheetController
        present(actionSheetController, animated: true, completion: nil)
    }
    
    //MARK:- All Button Action
    
    ///When user will tap on next button then this mithod will be called
    @IBAction func loginButtonClicked(_ sender: Any) {
        
        if self.loginViewOutlet.checkMendatoryFields()
        {
            self.requestForLogin()
        }
    }
    
    /// Login web service
 
    func requestForLogin() {
        
        let progressHUD = ProgressHUD(text: Constant.loggingInText)
        self.view.addSubview(progressHUD)
        
        if let userMobileNumber = loginViewOutlet.phoneNumberTextFieldOutlet.text,
            let password = loginViewOutlet.passwordTextFieldOutlet.text,
            let countryCode = loginViewOutlet.countryCodeLabel.text
        {
            var pushToken = Constant.constantPushToken
            if let token = UserDefaults.standard.value(forKey: SyUserdefaultKeys.kNSUPushNotificationTokenKey) as? String
            {
                pushToken = token
            }
            
            let mobileNo = ("\(countryCode)\(userMobileNumber)")
            let params =    [
                ServiceInfo.phoneNumberWIthCode:mobileNo as AnyObject,
                ServiceInfo.password:password as AnyObject,
                ServiceInfo.PushToken:pushToken as AnyObject,
                ServiceInfo.DeviceType:"1" as AnyObject,
                Constant.deviceMakeKey:UIDevice.current.systemName as AnyObject,
                Constant.deviceModelKey:UIDevice.current.model as AnyObject,
                ServiceInfo.deviceId:UIDevice.current.identifierForVendor?.uuidString as AnyObject
                ] as [String: AnyObject]
        
        
        
            
        let apiCall = RxSwiftAPI()
        
        apiCall.requestPOSTURL(serviceName: APINAMES.LOGIN,
                           withStaticAccessToken:true,
                           params: params)
            
        apiCall.subject_response
        
            .subscribe(onNext: {response in
                progressHUD.hide()
                self.handleLoginAPIResponse(response: response)
                
            }, onError: {error in
                print("\(error)")
                self.view.showErrorMessage(titleMsg: Message.Error, andMessage: error.localizedDescription)
                progressHUD.hide()
                
            })
            
         .disposed(by:disposeBag)
            
        }
    }
    
    
    func handleLoginAPIResponse(response:[String:Any]) {
        let statuscode = response["code"] as! Int
        switch (statuscode) {
        case 200:
            
            if let dict = response["data"] as? [String:Any]{
                if let sessionToken = dict["token"] as? String{
                    self.addToken(token: sessionToken)
                }
            }
            
            
            
            let storyBoard = UIStoryboard.init(name: "TabBarControllers", bundle: nil)
            self.view.window?.rootViewController = storyBoard.instantiateViewController(withIdentifier: "TabBarController")
        default:
            print(response[LoginResponse.errorMessage])
            self.view.showErrorMessage(titleMsg: Message.Error, andMessage:response[LoginResponse.errorMessage] as! String)
        }
    }
    
    ///When user will tap on country code button then this mithod will be called
    @IBAction func countryCodeButtonClicked(_ sender: Any) {
        self.performSegue(withIdentifier: IDENTIFIER.CountryPicker, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == IDENTIFIER.CountryPicker
        {
            let controller : VNHCountryPicker = segue.destination as! VNHCountryPicker
            controller.delegate = self
        }
        else if segue.identifier == IDENTIFIER.GotoResetPasword {
            
            let viewcontroller = segue.destination as! ResetpasswordViewController
            viewcontroller.mobileNO =  loginViewOutlet.phoneNumberTextFieldOutlet.text!
            let phone = sender as! Bool
            viewcontroller.resetPasswordThroughPhone = phone
        }
    }
}

//MARK:- Country Code Picker Delegate

extension LoginViewController : VNHCountryPickerDelegate{
    
    func didPickedCountry(country: VNHCounty) {
        self.loginViewOutlet.countryCodeLabel.text = country.dialCode
        self.loginViewOutlet.countryFlagImageView.image = country.flag
    }
}
