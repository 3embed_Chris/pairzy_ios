//
//  PreviewViewController.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 25/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase
import RxSwift
import VGPlayer
import SnapKit

class PreviewViewController: UIViewController {
    
    struct Constant {
        static let cancelButtonTitle = StringConstants.cancel()
        
        static let constantPushToken = "iOS Simulator Push Token"
        static let currentLocation = "Bangalore"
        static let currentLatitude = "10.344"
        static let currentLongitude = "13.566"
        static let apiRequiredDateFormat = "dd/MM/yyyy"
        
    }
    
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var viewForVideoPreview: UIView!
    var userData : User!
    var player : VGPlayer?
    
    @IBOutlet weak var editButtonOutlet: BorderedButton!
    
    @IBOutlet weak var doneButtonIOutlet: BorderedButton!
    
    @IBOutlet weak var imaegViewOutlet: UIImageView!
    @IBOutlet weak var nameLabelOutlet: UILabel!
    @IBOutlet weak var sexAndHeightLabelOutlet: UILabel!
    
    var recordedVideoLocalFileUrl:URL?
    var userProfilePic:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if((player) != nil) {
            player?.pause()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if(recordedVideoLocalFileUrl != nil) {
            self.showVideoForUrl(videoUrl: recordedVideoLocalFileUrl!)
            self.imaegViewOutlet.isHidden = true
        } else {
            if (userProfilePic != nil){
                self.imaegViewOutlet.image = userProfilePic
            }
        }
        
        if (userData != nil){
            if userData.firstName != ""{
                var dobInYear = ""
                if(userData.dateOfBirth != "") {
                    dobInYear = String(Helper.dateFromMilliseconds(timestamp: userData.dateOfBirth).age)
                }
                self.nameLabelOutlet.text = "\(userData.firstName.capitalized), \(dobInYear)"
            }
            
            if userData.gender != "", userData.heightInCM != 0.00{
                let heightInFeet = HeightConverter.getFeetAndInchesFrom(centimeters: userData.heightInCM)
                self.sexAndHeightLabelOutlet.text = "\(userData.gender), \(heightInFeet)"
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIds.previewTopresentFilters{
            let navController = segue.destination as! UINavigationController
            let controller = navController.childViewControllers.first as! MatchPreferencesVC
            controller.isComingFromSignUp = true
            controller.userData = self.userData
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func showVideoForUrl(videoUrl:URL) {
        if videoUrl != nil {
            player = VGPlayer(URL: videoUrl)
            player?.delegate = self
            viewForVideoPreview.addSubview((player?.displayView)!)
            self.viewForVideoPreview.bringSubview(toFront: self.nameLabelOutlet)
            self.viewForVideoPreview.bringSubview(toFront: self.sexAndHeightLabelOutlet)
            self.viewForVideoPreview.bringSubview(toFront: self.doneButtonIOutlet)
            self.viewForVideoPreview.bringSubview(toFront: self.editButtonOutlet)
            
            self.imaegViewOutlet.isHidden = true
            player?.backgroundMode = .proceed
            player?.play()
            //player?.displayView.backgroundColor = UIColor.white
            player?.displayView.delegate = self
            player?.displayView.snp.makeConstraints { [weak self] (make) in
                guard let strongSelf = self else { return }
                make.edges.equalTo(strongSelf.view)
            }
        } else {
            self.imaegViewOutlet.isHidden = false
        }
    }
    
    @IBAction func editButtonAction(_ sender: Any) {
        
        let vc = UIStoryboard(name:"DatumTabBarControllers", bundle: nil).instantiateViewController(withIdentifier: "editProfileVc") as! EdiitProfileViewController
        let preferenceNav: UINavigationController = UINavigationController(rootViewController: vc)
        self.present(preferenceNav, animated: true, completion: nil)
    }
    
    @IBAction func doneButtonAction(_ sender: Any)
    {
        self.requestupdateMedia()
    }
    
    
    func requestupdateMedia(){
            
        Helper.showProgressIndicator(withMessage: StringConstants.updateMediaMessage())

        let dbObj = Database.shared
        
        let params =
            [ ServiceInfo.UserImageLink          : dbObj.userDataObj.profilePictureURL! ,
              ServiceInfo.UservideoLink        : dbObj.userDataObj.profileVideoUrl!,
              ServiceInfo.otherfileVideo       : [String](),
              ServiceInfo.otherProfilePic       : [String]()
            ]  as [String:Any]
        
        let requestDetails = RequestModel().updateMediaDetails(media:params)
        
        let mediaApi = MediaAPICalls()
        
        mediaApi.updateMedia(mediaDetails:requestDetails)
        
        mediaApi.subject_response
            .subscribe(onNext: {response in
                
                switch response.statusCode {
                case 200:
                    
                    self.performSegue(withIdentifier: SegueIds.previewTopresentFilters, sender: nil)
                    
                    break;
                default:
                    self.view.showErrorMessage(titleMsg: StringConstants.error(), andMessage:response.response[LoginResponse.errorMessage] as! String)
                }
                
                Helper.hideProgressIndicator()
            }, onError: {error in
                Helper.hideProgressIndicator()
                Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
            })
            .disposed(by:disposeBag)
        
        //downloading image For better and fast caching in tabbar. (OPTIONAL)

        let url = URL(string: dbObj.userDataObj.profilePictureURL! )
        let tempImageView = UIImageView()
        
        
        tempImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "user_default_image"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
    }

}

extension PreviewViewController: VGPlayerDelegate {
    func vgPlayer(_ player: VGPlayer, playerFailed error: VGPlayerError) {
        print(error)
    }
    func vgPlayer(_ player: VGPlayer, stateDidChange state: VGPlayerState) {
        print("player State ",state)
        if(player.state == .playFinished) {
            //player.play()
        }
    }
    func vgPlayer(_ player: VGPlayer, bufferStateDidChange state: VGPlayerBufferstate) {
        print("buffer State", state)
    }
}

extension PreviewViewController: VGPlayerViewDelegate {
    func vgPlayerView(_ playerView: VGPlayerView, willFullscreen fullscreen: Bool) {
        
    }
    
    func vgPlayerView(didTappedClose playerView: VGPlayerView) {
        if playerView.isFullScreen {
            playerView.exitFullscreen()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func rvgPlayerView(didDisplayControl playerView: VGPlayerView) {
        UIApplication.shared.setStatusBarHidden(!playerView.isDisplayControl, with: .fade)
    }
}
