//
//  MessageAndTwoButtonsViewController.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 05/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

protocol AlertButtonPressedDelegate: class { //getting delegate methods for the button actions
    func yesButtonPressed()
    func noButtonPressed()
}

class MessageAndTwoButtonsViewController: UIViewController {
    
    @IBOutlet weak var titleOutlet: UILabel!
    @IBOutlet weak var messageOutlet: UILabel!
    @IBOutlet weak var leftButtonOutlet: UIButton!
    @IBOutlet weak var rightButtonOutlet: UIButton!
    
    
    var titleString:String?{//setting title for the alert message
        didSet {}}
    
    var nonDestructiveButtonTitleString:String?{ //setting left button title
        didSet {}}
    
    var destructiveButtonTitleString:String?{ //setting right button title
        didSet {}}
    
    var messageInAlert:String? //message In the Alert
    
    weak var alertButtonPressedDelegate:AlertButtonPressedDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (self.titleString != nil){
            self.titleOutlet.text = self.titleString
        }
        
        if (self.messageInAlert != nil){
            self.messageOutlet.text = self.messageInAlert
        }
        
        if (self.nonDestructiveButtonTitleString != nil){
            self.leftButtonOutlet.setTitle(nonDestructiveButtonTitleString, for: .normal)
        }
        
        if (self.destructiveButtonTitleString != nil){
            self.rightButtonOutlet.setTitle(destructiveButtonTitleString, for: .normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func yesButtonAction(_ sender: Any) //called when user pressed yes button
    {
        if (alertButtonPressedDelegate != nil){
            self.dismiss(animated: false, completion: nil)
            alertButtonPressedDelegate?.yesButtonPressed()
        }
    }
    
    @IBAction func noButtonAction(_ sender: Any) //called when user pressed no button
    {
        if (alertButtonPressedDelegate != nil){
            alertButtonPressedDelegate?.noButtonPressed()
            self.dismiss(animated: false, completion: nil)
        }
    }
}
