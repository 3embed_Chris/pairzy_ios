//
//  SignupView.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 02/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class SignupView: UIView {
    
    @IBOutlet weak var userDetailsViewOutlet: UserDetailsView!
    @IBOutlet weak var termAndConditionView: UIView!
    @IBOutlet weak var nextButtonView: UIView!
    @IBOutlet weak var checkBoxButtonOutlet: UIButton!
    
}
