//
//  MobileView.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 09/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

protocol verificationCodeDelegate {
    func verificationCodeWritingCompleted()
}

class MobileView: UIView {
    
    @IBOutlet weak var firstcCodeTextField: UITextField!
    @IBOutlet weak var secondCodeTextField: UITextField!
    @IBOutlet weak var thirdCodeTextField:  UITextField!
    @IBOutlet weak var fourthCodeTextField: UITextField!
    @IBOutlet weak var fifthCodeTextField: UITextField!
    @IBOutlet weak var sixthCodeTextField: UITextField!

    var verificationCodeDelegate:verificationCodeDelegate?
}

extension MobileView: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // On inputing value to textfield
        if ((textField.text?.characters.count)! < 1  && string.characters.count > 0){
            let nextTag = textField.tag + 1;
            
            // get next responder
            let nextResponder = textField.superview?.viewWithTag(nextTag);
            textField.text = string;
            
            if (nextResponder == nil){
                textField.resignFirstResponder()
                if (verificationCodeDelegate != nil){
                    verificationCodeDelegate?.verificationCodeWritingCompleted()
                }
            }
            nextResponder?.becomeFirstResponder();
            return false;
        }
        else if ((textField.text?.characters.count)! >= 1  && string.characters.count == 0){
            // on deleting value from Textfield
            let previousTag = textField.tag - 1;
            
            // get next responder
            var previousResponder = textField.superview?.viewWithTag(previousTag);
            
            if (previousResponder == nil){
                previousResponder = textField.superview?.viewWithTag(1);
            }
            textField.text = "";
            previousResponder?.becomeFirstResponder();
            return false;
        }
        return true;
    }
}
