//
//  imagePickerModel.swift
//  UFly
//
//  Created by 3Embed on 18/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import AVKit
import TLPhotoPicker
import Photos

protocol ImagePickerModelDelegate: class{
    
    /// this method asks the delegate to set the selected Image
    ///
    /// - Parameters:
    ///   - selectedImage: UIImage Type
    ///   - tag: tag is integerType
    func didPickImage(selectedImage: UIImage , tag:Int,videoUrl:String,sourceType: Int,cameraDeviceMode: Int)
}

class ImagePickerModel: NSObject {
    
    //variables
    var pickerController:UIViewController = UIViewController()
    weak var delegate:ImagePickerModelDelegate? = nil
    var removed = 0
    var sourceType = 3     // 0 for photoLibrary , 1 for Camera ,2 savedPhotosAlbum
    var cameraDeviceMode = 3  // 0 for front Camera ,1 for back Camera
    var chosen = UIImage()
    var imagePicker = UIImagePickerController()
    
    
    private static var pickerObj: ImagePickerModel? = nil
    
    //Create Shared Instance
    static var sharedImagePicker: ImagePickerModel {
        if pickerObj == nil {
            pickerObj = ImagePickerModel()
        }
        return pickerObj!
    }
    
    
    /// this method adds actions for image picker that is takePhoto,pickFromLibrary,Cancel and Remove Action
    ///Remove Action Enabled based on tag if tag 1 - RemoveAction enabled,tag 0 - RemoveAction disabled
    /// - Parameters:
    ///   - controller: this parameter is of type UIViewController
    ///   - tag: tag is of type Integer, it will be having 0 0r 1
    func setProfilePic(controller:UIViewController ,tag:Int,pickerForVideo:Bool) {
        
        pickerController = controller
        
       // imagePicker.isEditing = true
       // imagePicker.allowsEditing = true
        var cameraTitle = "Take photo"
        
        if(pickerForVideo) {
            imagePicker.mediaTypes = ["public.movie"]
            cameraTitle = "Take video"
        } else {
            imagePicker.mediaTypes = ["public.image"]
            cameraTitle = "Take photo"
        }
        
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle:.actionSheet)
        
        let takePhoto = UIAlertAction(title: cameraTitle, style: UIAlertActionStyle.default, handler: { (take) in
            
            if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
               
//                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
//                self.imagePicker.cameraDevice = .front
//
//                controller.present(self.imagePicker, animated: true, completion: nil)
                let picker = YPImagePicker()
                if pickerForVideo{
                    picker.showsVideosVC = true
                    picker.showsCameraVC = false
                }else{
                    picker.showsVideosVC = false
                    picker.showsCameraVC = true
                }
                DispatchQueue.main.async {
                    self.pickerController.present(picker, animated: true, completion: nil)
                }
                
            }
            else {
                self.noCamera(controller: controller)
            }
        })
        
        let pickFromLibrary = UIAlertAction(title: "Choose from library", style: UIAlertActionStyle.default, handler:{(pickFrom) in
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            controller.present(self.imagePicker, animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{(cancel) in })
        if tag == 1 {
            let removePhoto: UIAlertAction = UIAlertAction(title: "Remove",
                                                           style: .default ,handler:{ action -> Void in
                                                            self.removePhoto(controller: controller ,tag: tag)
            })
            alert.addAction(removePhoto)
        }
        alert.addAction(takePhoto)
        alert.addAction(pickFromLibrary)
        alert.addAction(cancelAction)
        controller.present(alert, animated: true, completion: nil)
        
    }
    
    
    /// this method is to handle the crash if device is not having camera
    ///
    /// - Parameter controller: controller parmeter is of type UIViewController
    func noCamera(controller:UIViewController){
        let alertVC = UIAlertController(
            title: "No camera",
            message: "No Camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        controller.present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    
    /// this method gets called if the tag value is 1
    /// this sets default image as selected Image and reset the tag as 0
    /// - Parameters:
    ///   - controller: it is of type UIViewController
    ///   - tag: tag is of type Integer
    func removePhoto(controller:UIViewController ,tag:Int)
    {
        // var pickedImage = UIImage()
        removed = 0
        // delegate?.didPickImage(selectedImage: pickedImage ,tag: removed)
        delegate?.didPickImage(selectedImage: UIImage() ,tag: removed, videoUrl:"", sourceType: self.sourceType,cameraDeviceMode: self.cameraDeviceMode )
        
    }
}

extension ImagePickerModel: UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVCaptureFileOutputRecordingDelegate {
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        
    }
    
    
    
    //fetching local documentry.
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
    
    func thumbnail(sourceURL:NSURL) -> UIImage {
        let asset = AVAsset(url: sourceURL as URL)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        let time = CMTime(seconds: 1, preferredTimescale: 1)
        do {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            
            let image = UIImage(cgImage: imageRef)
            let imgData = NSData(data: UIImageJPEGRepresentation((image), 0.2)!)
            let compressedImage = UIImage(data: imgData as Data)
            print(imgData.length/1024)
            return compressedImage!
        } catch {
            return #imageLiteral(resourceName: "play")
        }
    }
    
    func capture(_ output: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL: URL!, fromConnections connections: [Any]!, error: Error!) {
      
    }
   
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.imagePicker = picker
        if picker.mediaTypes == ["public.image"] {
            guard var chosen = info[UIImagePickerControllerOriginalImage] as? UIImage else { return }
            if(chosen.size.width > 499){
                chosen = chosen.resizeImage(image: chosen, targetSize: CGSize(width: 500, height: 500))
            }
            chosen = chosen.compressTo(0.5)!
            removed = 1
           // chosen = fixOrientation(img: chosen)
            self.sourceType = picker.sourceType.rawValue
            if picker.sourceType.rawValue == 1 {
                self.cameraDeviceMode = picker.cameraDevice.rawValue
                
//
//                if picker.cameraDevice == .front {
//                    let flippedImage = UIImage(cgImage: chosen.cgImage!, scale: chosen.scale, orientation: .upMirrored)
//                    delegate?.didPickImage(selectedImage: flippedImage, tag: removed, videoUrl: "", sourceType: self.sourceType,cameraDeviceMode: self.cameraDeviceMode )
//                }else {
//                    delegate?.didPickImage(selectedImage: chosen, tag: removed, videoUrl: "", sourceType: self.sourceType,cameraDeviceMode: self.cameraDeviceMode )
//                }
                
            }
            //else {
//                  delegate?.didPickImage(selectedImage: chosen, tag: removed, videoUrl: "", sourceType: self.sourceType,cameraDeviceMode: self.cameraDeviceMode )
//            }
            
             pickerController.dismiss(animated: true, completion: nil)
            let story = UIStoryboard.init(name: "Chat", bundle: nil)
            let preview =  story.instantiateViewController(withIdentifier: "ImagePickerPreview") as? ImagePickepreviewController
             guard let imageToSend = info[UIImagePickerControllerOriginalImage] as? UIImage else { return }
            preview?.imageArray = [imageToSend]
            preview?.delegate = self
            preview?.isComingFromCamera = true
            pickerController.present(preview!, animated: true, completion: nil)

        }
        else {
            
    
            let filename = getDocumentsDirectory().appendingPathComponent(Helper.getCurrentTimeStamp())
            
            if let videoURL = info[UIImagePickerControllerMediaURL] as? NSURL {
                
                if var image = thumbnail(sourceURL: videoURL) as? UIImage{
                    image = fixOrientation(img: image)
                    
                    if picker.sourceType.rawValue == 1 { //
                        self.cameraDeviceMode = picker.cameraDevice.rawValue
                        
                        if picker.cameraDevice == .front {
                           image = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: .upMirrored)
                        }
                    }
                    
                    if let data = UIImagePNGRepresentation(image) {
                        try? data.write(to: filename)
                    }
                    
                    removed = 1
                    
                    delegate?.didPickImage(selectedImage: image, tag: removed, videoUrl: videoURL.absoluteString!, sourceType: self.sourceType,cameraDeviceMode: self.cameraDeviceMode )
                    pickerController.dismiss(animated: true, completion: nil)
                   
                }
            }
            
        }
    }
}
  



extension UIImage {
    // MARK: - UIImage+Resize
    func compressTo(_ expectedSizeInMb:Float) -> UIImage? {
        let sizeInBytes = expectedSizeInMb * 1024 * 1024
        var needCompress:Bool = true
        var imgData:Data?
        var compressingValue:CGFloat = 1.0
        while (needCompress && compressingValue > 0.0) {
            if let data:Data = UIImageJPEGRepresentation(self, compressingValue) {
                if data.count < Int(sizeInBytes) {
                    needCompress = false
                    imgData = data
                } else {
                    compressingValue -= 0.1
                }
            }
        }
        
        if let data = imgData {
            if (data.count < Int(sizeInBytes)) {
                return UIImage(data: data)
            }
        }
        return nil
    }
    
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        var newSize: CGSize
        if widthRatio > heightRatio {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}


extension ImagePickerModel : ImagepreviewDelegate {
    
    func didCancelCliked() {
        print("cancel")
    }
    
    func didSendCliked(medias: [Any]) {
        print(medias)
         for (index, media) in medias.enumerated() {
            if index == 0 {
                updatePhoto(media: media)
            }
        }
   
        //        for (index, media) in medias.enumerated() {
        //            if index == 0 {
        //                self.uploadMedia(withMedia : media)
        //            }
        //            else {
        //                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
        //                    self.uploadMedia(withMedia : media)
        //                }
        //            }
        //        }
       // self.dismiss(animated: true, completion: nil)
    }
    
    
    func updatePhoto(media: Any){
        
      //  if let mediaObj = media as? TLPHAsset {
         //   if mediaObj.type == .photo {
        if let currentImage = media as? UIImage{
                    //                    self.addImageMessage(withImage: currentImage, andMediaType: "1")
//                    if imagePicker.sourceType.rawValue == 1 {
//                        if imagePicker.cameraDevice == .front {
//                            let flippedImage = UIImage(cgImage: currentImage.cgImage!, scale: chosen.scale, orientation: .upMirrored)
//                            delegate?.didPickImage(selectedImage: flippedImage, tag: removed, videoUrl: "", sourceType: self.sourceType,cameraDeviceMode: self.cameraDeviceMode )
//                        }else {
//                            delegate?.didPickImage(selectedImage: currentImage, tag: removed, videoUrl: "", sourceType: self.sourceType,cameraDeviceMode: self.cameraDeviceMode )
//                        }
//                    }else {
//                        delegate?.didPickImage(selectedImage: currentImage, tag: removed, videoUrl: "", sourceType: self.sourceType,cameraDeviceMode: self.cameraDeviceMode )
//                    }
            
            delegate?.didPickImage(selectedImage: currentImage, tag: removed, videoUrl: "", sourceType: self.sourceType,cameraDeviceMode: self.cameraDeviceMode )
            
            
                }
           // }
       // }
    }
    
}
