//
//  SuccessView.swift
//  Loopz
//
//  Created by 3Embed on 27/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import MBCircularProgressBar
protocol SuccessViewDelegate :class{
    func isProgressBarHidden(flag: Bool)
}


class SuccessView: UIView {
    
    @IBOutlet weak var progressView: MBCircularProgressBarView!
    @IBOutlet weak var successButton: UIButton!
    @IBOutlet weak var successLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var sentLabel: UILabel!
    
    var progressTimer:Timer? = nil
    weak var delegate:SuccessViewDelegate? = nil
    private static var obj: SuccessView? = nil
    static var shared: SuccessView {
        
        if obj == nil {
            obj = UINib(nibName: "CommonAlertView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as? SuccessView
            obj?.frame = UIScreen.main.bounds
            obj?.successLabel.text = ""
            obj?.sentLabel.text = ""
            obj?.progressView.progressColor = Colors.AppBaseColor
        }
        obj?.progressView.value = 0
        obj?.holderView.isHidden = true
        return obj!
    }
    
    private func setup() {
        var when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            self.animate()
        }
        let window:UIWindow = UIApplication.shared.delegate!.window!!
        window.addSubview(self)
        when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
              self.hide()
        }
    }
    
    func showPI() {
        DispatchQueue.main.async() { // or you can use
            self.setup()
        }
    }
    
    func hide() {
        DispatchQueue.main.async() { // or you can use
            if (SuccessView.obj != nil) {
                self.removeFromSuperview()
                if self.delegate != nil{
                    self.delegate?.isProgressBarHidden(flag: true)
                }
            }
        }
    }
    
    func animate() {
        DispatchQueue.main.async(){
            UIView.animate(withDuration: 2) {
                SuccessView.obj?.holderView.isHidden = false
                SuccessView.obj?.progressView.value = 100
            }
        }
    }
}
