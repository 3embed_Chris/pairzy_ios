//
//  FBLoginHandler.swift
//  FaceBookLoginSwift
//
//  Created by Apple on 21/01/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Accounts


protocol facebookLoginDelegate {
    /**
     *  Facebook login is success
     *
     *  @param userInfo Userdict
     */
    
    func didFacebookUserLogin(withDetails userInfo: NSDictionary)
    /**
     *  Login failed with error
     *
     *  @param error error
     */
    
    func didFailWithError(_ error: Error?)
    
    /**
     *  User cancelled
     */
    
    func didUserCancelLogin()
}



class FBLoginHandler: NSObject {

    static var share:FBLoginHandler?
    
 var readPermission:[String] = NSArray() as! [String]
 //  let readPermission: [ReadPermission] = [.publicProfile, .email, .userFriends]
    var parameters:[String:String] = ["fields": "picture, email, name, first_name, last_name"]
    var delegate: facebookLoginDelegate?


    class func sharedInstance() -> FBLoginHandler {
        
        if (share == nil) {
            
            share = FBLoginHandler.self()
        }
        return share!
    }
    
    override init() {
        super.init()
        
//        self.readPermission = ["public_profile", "email", "user_friends"]
//        self.parameters = ["fields": "picture, email, name, about, first_name, last_name"]
        
    }
    
   @objc func sendBackTheFacebookDetails(fbDetails: Any) {
        
        if (self.delegate != nil)  {
            self.delegate?.didFacebookUserLogin(withDetails: fbDetails as! NSDictionary)
        }
    }

    
    func login(withFacebook viewController: UIViewController) {
        
//        ["public_profile", "email", "user_friends"]
//
        readPermission.append("public_profile")
        readPermission.append("email")
        //readPermission.append("user_friends")
      //  readPermission.append("user_birthday")
     //   readPermission.append("user_photos")
        //readPermission.append("user_likes")
     //   readPermission.append("user_gender")
      //  readPermission.append("user_age_range")//user_age_range
        
        let login = FBSDKLoginManager()
        login.loginBehavior = FBSDKLoginBehavior.native
        login.logOut()
    
        login.logIn(withReadPermissions: self.readPermission,from: viewController) {  (result:FBSDKLoginManagerLoginResult?,error:Error?) in
        
            if (error != nil) {
                
                print("Login error : \(String(describing: error?.localizedDescription))")
                
                if (self.delegate != nil) {
                    self.delegate?.didFailWithError(error)
                }
            }
            else if (result?.isCancelled)! {
                
                print("Cancelled")
                if (self.delegate != nil) {
                    self.delegate?.didUserCancelLogin()
                }
            }
            else {
                print("Logged in")
                self.getDetailsFromFacebook()
            }
        }
        
    }
    
    func getDetailsFromFacebook() {
        
        if (FBSDKAccessToken.current() != nil) {
            
            let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "picture, email, name, about, first_name, last_name,id"])
            let connection = FBSDKGraphRequestConnection()
            connection.add(graphRequest, completionHandler: { (connection, result, error) in
                if error != nil {
                    //do something with error
                    if (self.delegate != nil) {
                        self.delegate?.didFailWithError(error)
                    }
                } else {
                    //do something with result
                    self.perform(#selector(self.sendBackTheFacebookDetails(fbDetails:)), with: result, afterDelay: 0.6)
                }
            })
            
            connection.start()
        }
        
    }
}

