//
//  CloudinaryHelper.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 14/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Foundation
//import Cloudinary

class CloudinaryHelper: NSObject {
    
    static let sharedInstance = CloudinaryHelper()
    
    let cloudinary = CLDCloudinary(configuration: CLDConfiguration(cloudName:"deu1yq6y6", secure: true))
    
    func uploadImage(image: UIImage, onCompletion: @escaping (_ status: Bool, _ url: String?) -> Void) {
        
        let params = CLDUploadRequestParams()
        params.setResourceType(CLDUrlResourceType.image)
        let data = UIImageJPEGRepresentation(image, 1.0)
        let timestamp = Helper.getCurrentTimeStamp()
        params.setPublicId("\(timestamp)")
        cloudinary.createUploader().upload(data: data!, uploadPreset: "salesPaddock", params: params, progress: nil, completionHandler: { (result, error) in
            if error != nil {
                onCompletion(false,"")
            } else {
                if let result = result{
                    onCompletion(true,result.resultJson["url"] as? String ?? "")
                }
            }
        })
    }
    
    func uploadVideo(video: URL, onCompletion: @escaping (_ status: Bool, _ url: String?) -> Void) {
        
        let params = CLDUploadRequestParams()
        params.setResourceType(CLDUrlResourceType.video)
        let timestamp = Helper.getCurrentTimeStamp()
        params.setPublicId("\(timestamp)")
        do {
            cloudinary.createUploader().upload(url: video, uploadPreset: "salesPaddock", params: params, progress: nil, completionHandler: { (result, error) in
                if error != nil {
                    onCompletion(false,"")
                } else {
                    if let result = result{
                        onCompletion(true,result.resultJson["url"] as? String ?? "")
                    }
                }
            })
        }
    }
}
