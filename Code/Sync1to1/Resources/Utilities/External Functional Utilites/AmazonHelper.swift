//
//  AmazonHelper.swift
//  Trustpals
//
//  Created by Dinesh Guptha Bavirisetti on 30/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import AWSS3
import AWSCore

let REGION: AWSRegionType = .USEast1

class AmazonHelper: NSObject {
    
    private static var manager: AmazonHelper? = nil
    
    static var shared: AmazonHelper {
        if (manager == nil) {
            manager = AmazonHelper()
        }
        return manager!
    }
    
    override init() {
        
        let credentialProvider = AWSStaticCredentialsProvider(accessKey: Keys.awsAccessKey,
                                                              secretKey: Keys.awsSecretKey)
        
        let configuration = AWSServiceConfiguration(region: REGION,
                                                    credentialsProvider: credentialProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }
    
    /// Upload Image
    ///
    /// - Parameters:
    ///   - image: UIImage
    ///   - isThumbnail: for video thumbnail it is true & for normal image it is false
    ///   - completion: Handler
    func upload(image: UIImage, isThumbnail: Bool,
                completion: @escaping(_ success: Bool, _ imageURL: String) -> Void) {
        
        let imageName = "Sync1To1" + "Image" + self.getCurrentTimeStamp() + ".png"
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imageName)
        
        let data = UIImageJPEGRepresentation(image, 1.0)
        do {
            try data?.write(to: fileURL)
        }
        catch {
            print("Could not write to file.")
        }
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = fileURL
        uploadRequest.key = imageName
        uploadRequest.bucket = Keys.photoUploadBucket
        uploadRequest.contentType = "image/png"
        uploadRequest.acl = .publicRead
        
        let transferManager = AWSS3TransferManager.default()
        
        transferManager.upload(uploadRequest).continueWith(executor: AWSExecutor.mainThread(),
                                                           block: { (task:AWSTask<AnyObject>) -> Any? in
                                                            
                                                            if let error = task.error as NSError? {
                                                                if error.domain == AWSS3TransferManagerErrorDomain, let code = AWSS3TransferManagerErrorType(rawValue: error.code) {
                                                                    switch code {
                                                                    case .cancelled, .paused:
                                                                        break
                                                                    default:
                                                                        print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error.localizedDescription)")
                                                                    }
                                                                }
                                                                else {
                                                                    print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error.localizedDescription)")
                                                                }
                                                                completion(false, "")
                                                            }
                                                            
                                                            if task.result != nil {
                                                                
                                                                let url = AWSS3.default().configuration.endpoint.url
                                                                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                                                                completion(true, (publicURL?.absoluteString)!)
                                                            }
                                                            return nil
        })
        
        uploadRequest.uploadProgress = {(bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                
                print("total prgress = \(bytesSent) \(totalBytesSent) \(totalBytesExpectedToSend)")
            })
        }
    }
    
    /// Upload Video
    ///
    /// - Parameters:
    ///   - videoPath: local path of video
    ///   - videoURL: local url of video
    ///   - completion: Handler
    
    func uploadVideo(videoURL: URL,onStart : @escaping((_ uploadRequest : AWSS3TransferManagerUploadRequest) -> Void),
            completion: @escaping(_ success: Bool, _ imageURL: String) -> Void) {
        
        let serialQueue = DispatchQueue(label: "queuename")
        serialQueue.sync {
            
            let data = NSData(contentsOf: videoURL)
            
            let videoName = "SYNC1TO1" + "Video" + self.getCurrentTimeStamp() + ".mp4"
            let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(videoName)
            
            do {
                try data?.write(to: fileURL)
            }
            catch {
                print("Could not write to file.")
            }
            
            let uploadRequest = AWSS3TransferManagerUploadRequest()!
            uploadRequest.body = fileURL
            uploadRequest.key = videoName
            uploadRequest.bucket = Keys.videoUploadBucket
            uploadRequest.contentType = "video/mp4"
            uploadRequest.acl = .publicRead
            
            onStart(uploadRequest)
            
            let transferManager = AWSS3TransferManager.default()
            
            transferManager.upload(uploadRequest).continueWith(executor: AWSExecutor.mainThread(),
                                                               block: { (task:AWSTask<AnyObject>) -> Any? in
                                                                
                                                                if let error = task.error as NSError? {
                                                                    if error.domain == AWSS3TransferManagerErrorDomain, let code = AWSS3TransferManagerErrorType(rawValue: error.code) {
                                                                        switch code {
                                                                        case .cancelled, .paused:
                                                                            break
                                                                        default:
                                                                            print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error.localizedDescription)")
                                                                        }
                                                                    }
                                                                    else {
                                                                        print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error.localizedDescription)")
                                                                    }
                                                                    completion(false, "")
                                                                }
                                                                
                                                                if task.result != nil {
                                                                    
                                                                    let url = AWSS3.default().configuration.endpoint.url
                                                                    let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                                      
                                                                    
                                                                    
                                                                    completion(true, (publicURL?.absoluteString)!)
                                                                }
                                                                return nil
            })
        }
    }
    
    /// Delete file (image/video/thumbnail) from Amazon
    func deleteFileFromAmazon(name: String, isItImage: Bool) {
        
        let s3 = AWSS3.default()
        let deleteObjectRequest = AWSS3DeleteObjectRequest()
        if isItImage{
            deleteObjectRequest?.bucket = Keys.photoUploadBucket
        }
        else{
            deleteObjectRequest?.bucket = Keys.videoUploadBucket
        }
        deleteObjectRequest?.key = name
        s3.deleteObject(deleteObjectRequest!).continueWith { (task:AWSTask) -> AnyObject? in
            if let error = task.error {
                print("Error occurred: \(error)")
                return nil
            }
            print(name,"deleted successfully.")
            return nil
        }
    }
    
    /// get current time stamp
    ///
    /// - returns: current time stamp
    func getCurrentTimeStamp() -> String {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddhhmmssa"
        formatter.locale = Locale.current
        let result = formatter.string(from: date)
        return result
    }
}
