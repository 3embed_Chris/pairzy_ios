//
//  GoogleKeys.swift
//  UFly
//
//  Created by 3Embed on 26/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class GoogleKeys: NSObject {

    static var myCountry                                = ""
    static let PlacesAPILink                            = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&location=%f,%f&radius=1000000&amplanguage=%@&key=%@"
    static let PlaceEnlarge                             = "https://maps.googleapis.com/maps/api/place/details/json?reference=%@&key=%@"
    
   
    static var GoogleMapKey:String {
//        let appValue = Utility.getAppConfig().MapKeys
//        if appValue.count != 0 {
//            return appValue[0]
//        }
        return "AIzaSyDqeYzVSRGs_LPYHTLtmjXYM4pjNA4m1zw"
    }
    static let Description                              = "description"
    static let Reference                                = "reference"
    static let Predictions                              = "predictions"
    static let AddressName                              = "Name"
    static let AddressStreet                            = "Street"
    static let AddressCity                              = "City"
    static let AddressState                             = "State"
    static let AddressZIP                               = "ZIP"
    static let AddressCountry                           = "Country"
    static let AddressSubLocality                       = "SubLocality"
    static let LocationLat                              = "Lat"
    static let LocationLong                             = "Long"
    static let Structure                                = "structured_formatting"
    static let MainText                                 = "main_text"
    static let ZoneId                                   = "zoneId"
    static let ZoneName                                 = "zoneName"
    static let CityID                                   = "cityId"
    static let CityName                                 = "city"
    static let SubAdministrativeArea                    = "SubAdministrativeArea"
    static let PlaceId                                  = "id"
}
