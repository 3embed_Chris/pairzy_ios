//
//  HeightConverter.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 02/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class HeightConverter{
    
    struct Constant {
        static let Inch_In_CM:Float = 2.54
        static let numberOfInchesInFeet:Float = 12
    }
    
    class func getFeetAndInchesFrom(centimeters:Float) ->String
    {
        let numInches = roundf(centimeters/Constant.Inch_In_CM)
        let feet:Int = Int(numInches/Constant.numberOfInchesInFeet)
        let inches:Float = roundf(numInches.truncatingRemainder(dividingBy: Constant.numberOfInchesInFeet))
        if Int(inches) == 0{
            return String(format: "\(feet) ft")
        }
        return String(format: "\(feet) ft \(Int(inches)) in")
    }
}

extension Date {
    var age: Int {
        return Calendar.current.dateComponents([.year], from: self, to: Date()).year!
    }
}

extension Float {
    func round(_ decimalPlace:Int)->Float{
        let format = NSString(format: "%%.%if", decimalPlace)
        let string = NSString(format: format, self)
        return Float(atof(string.utf8String))
    }
}
