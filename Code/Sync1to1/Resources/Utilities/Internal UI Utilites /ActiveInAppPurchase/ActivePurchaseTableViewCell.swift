//
//  ActivePurchaseTableViewCell.swift
//  Datum
//
//  Created by Shivansh on 9/19/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class ActivePurchaseTableViewCell: UITableViewCell {

    @IBOutlet weak var featureImage: UIImageView!
    @IBOutlet weak var featureTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
