//
//  ActivePurchasePlanView.swift
//  Datum
//
//  Created by Shivansh on 9/19/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class ActivePurchasePlanView: UIView {
    
    @IBOutlet weak var featuresTableView: UITableView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var subView: UIView!
    var datumFeatures = DatupPlusCollection.features
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    /// method to load loadActivePurchaseView xib.
    func loadActivePurchaseView() {

        Bundle.main.loadNibNamed("ActivePurchasePlanView", owner:self, options:nil)
        self.mainView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        
        self.mainView.frame = self.bounds
        self.addSubview(self.mainView)
        self.mainView.layoutIfNeeded()

        self.subView.layer.cornerRadius = 12
        
        var frameForAnimation = self.subView.frame
        frameForAnimation.origin.y = -900
        self.subView.frame = frameForAnimation
        self.subView.layoutIfNeeded()

        UIView.animate(withDuration:0.3, delay:0.0, usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 0.7, options: [], animations:
            {
                frameForAnimation.origin.y = 0
                self.subView.frame = frameForAnimation
        }, completion:{ (finished) in
            if(finished) {
                
            }
        })
        
    }
    
    func removeViewWithAnimation() {
        var frameForAnimation = self.subView.frame
        frameForAnimation.origin.y = 0
        self.subView.frame = frameForAnimation
        self.subView.layoutIfNeeded()
        
        UIView.animate(withDuration:0.5, delay: 0.0, usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.5, options: [], animations:
            {
                frameForAnimation.origin.y = self.frame.size.height + 500
                self.subView.frame = frameForAnimation
        }, completion:{ (finished) in
            if(finished) {
                self.removeFromSuperview()
            }
        })
    }
    
    @IBAction func okButtonAction(_ sender: Any) {
        removeViewWithAnimation()
    }
    
    
}


extension ActivePurchasePlanView:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datumFeatures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "ActivePurchaseTableViewCell", bundle: nil), forCellReuseIdentifier: "ActivePurchaseCell")
       let featureCell = tableView.dequeueReusableCell(withIdentifier:"ActivePurchaseCell", for:indexPath) as! ActivePurchaseTableViewCell
        featureCell.featureImage.image = UIImage.init(named:datumFeatures[indexPath.row].featureImage)
        featureCell.featureTitle.text = datumFeatures[indexPath.row].featureTitle
        return featureCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

}
