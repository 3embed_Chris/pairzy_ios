//
//  AddLocationFooterView.swift
//  Datum
//
//  Created by 3 Embed on 11/03/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
protocol footerViewDelegate: class {
    func footerViewTapped()
}

class AddLocationFooterView: UIView {

    
    @IBOutlet weak var addNewLocationLabel: UILabel!
    
    weak var footerDelegate:footerViewDelegate? = nil
    
    @IBAction func addNewLocationAction(_ sender: Any) {
        
        if self.footerDelegate != nil{
            self.footerDelegate!.footerViewTapped()
        }else{
            print("delegate is nil")
        }
       
    }
    
    
    var obj: AddLocationFooterView? = nil
    var shared: AddLocationFooterView {
        if obj == nil {
            obj = UINib(nibName: "AddLocationFooterView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as? AddLocationFooterView
            obj?.frame = UIScreen.main.bounds
            obj?.addNewLocationLabel.text = StringConstants.addNewLocation()
        }
        return obj!
    }
}
