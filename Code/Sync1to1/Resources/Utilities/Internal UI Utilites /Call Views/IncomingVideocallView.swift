//
//  IncomingVideocallView.swift
//  webRtc_module
//
//  Created by Imma Web Pvt Ltd on 08/09/17.
//  Copyright © 2017 3embed. All rights reserved.
//

import UIKit
import AVFoundation
import CocoaLumberjack
import RxSwift

class IncomingVideocallView: UIView,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var previewForVideo: UIImageView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var remoteView: RTCEAGLVideoView!
    // @IBOutlet weak var localCameraPerviewView: RTCEAGLVideoView!
    @IBOutlet weak var localCameraPerviewView: RTCCameraPreviewView!
    @IBOutlet weak var callEndBtn: UIButton!
    @IBOutlet weak var callAcceptBtn: UIButton!
    @IBOutlet weak var mainViewFoRating: UIView!
    @IBOutlet weak var videoCallLbl: UILabel!
    @IBOutlet weak var videoCallIcon: UIImageView!
    @IBOutlet weak var incomingVidLbl: UILabel!
    @IBOutlet weak var declineLbl: UILabel!
    @IBOutlet weak var acceptlbl: UILabel!
    @IBOutlet weak var switch_btn: UIButton!
    @IBOutlet weak var endVcall_btn: UIButton!
    @IBOutlet weak var muteBtn: UIButton!
    @IBOutlet weak var doneButtonOutlet: UIButton!
    @IBOutlet weak var floatRatingView: FloatRatingView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var calling_userName: UILabel!
    @IBOutlet weak var calling_status: UILabel!
    @IBOutlet weak var localCameraPerviewViewConstantY: NSLayoutConstraint!
    @IBOutlet weak var localViewConstantY: NSLayoutConstraint!
    @IBOutlet weak var endBtnConstantY: NSLayoutConstraint!
    
    
    var showRatingView = false
    let syncDateVm = SyncDateeRequestVM()
    let disposeBag = DisposeBag()
    var webRtc: webRTC?
    var callId : String?
    var messageDict : [String:Any]?
    var timer : Timer?
    var endCallTimer: Timer?
    var callerId : String?
    var isSwitch : Bool?
    var chatViewObj : ChatViewController? = nil
    var player: AVAudioPlayer?
    var tapGester : UITapGestureRecognizer?
    var swipeGester : UISwipeGestureRecognizer?
    var panGester : UIPanGestureRecognizer?
    var isvideoSwiped = false
    var otherCallerId : String?  = ""
    var dateId:String! = ""
    
    
    var captureSession = AVCaptureSession()
    var sessionOutput = AVCaptureStillImageOutput()
    var previewLayer = AVCaptureVideoPreviewLayer()
    var isDisconnected: Bool = false
    var localViewFrame = CGRect.zero


    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    //set data here
    func setMessageData(messageData:[String : Any]) {
        messageDict = messageData
        callId = messageData["callId"] as? String
        callerId = messageData["callerId"] as? String
        dateId = messageData["dateId"] as? String
        if let hasImage = messageData["callerImage"] as? String {
            let newUrl = URL(string: hasImage)
            userProfileImageView.kf.setImage(with:newUrl, placeholder:UIImage.init(named:"02m"))
        } else {
            userProfileImageView.image = UIImage.init(named:"02m")
        }
        
        if let hasName = messageData["callerName"] as? String {
            userNameLabel.text = hasName
        }
        userProfileImageView.layer.borderColor = UIColor.white.cgColor
    }
    
    private func commonInit(){
        
        Helper.hidePushNotifications(hidePush: true)
        
        Bundle.main.loadNibNamed("IncomingVideocallView", owner: self, options: nil)
        
        contentView.frame = bounds
        contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth,UIViewAutoresizing.flexibleHeight]
        addSubview(contentView)
        declineLbl.text = StringConstants.decline()
        acceptlbl.text = StringConstants.accept()
        videoCallLbl.text = StringConstants.datumVideoCall()
        localCameraPerviewView.isHidden = true
        switch_btn.isHidden = true
        endVcall_btn.isHidden = true
        muteBtn.isHidden = true
        isSwitch = false
        userImageView.isHidden = true
        calling_userName.isHidden = true
        calling_status.isHidden = true
        
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
        userImageView.clipsToBounds = true
        doneButtonOutlet.setTitle(StringConstants.done(), for: .normal)
        
        
        
        
        //start timer 60 sec for incoming calling screen
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(timeoutRemoveScreen), userInfo: nil, repeats: false)
        
        if self.floatRatingView.delegate != nil {
            self.floatRatingView.delegate = self
        }
        
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        self.requestToSetRateForVideoCall()
        self.removeFromSuperview()
        Helper.hidePushNotifications(hidePush: false)
    }
    
    func requestToSetRateForVideoCall() {
        
        let syncDateVm = SyncDateeRequestVM()
        let disposeBag = DisposeBag()
        
        
        
        let params = [profileData.dateId:dateId,
                      profileData.rate:self.floatRatingView.rating
            ] as [String : Any]
        
        syncDateVm.rateForVideoCall(params:params)
        
        syncDateVm.syncDateSubjectResponse
            .subscribe(onNext: { apiStatus in
                
            }, onError:{ error in
                
            })
            .disposed(by:disposeBag)
    }
    
    func requestForEndDate() {
        
        
        syncDateVm.requestForEndDate(dateID:dateId)
        
        syncDateVm.syncDateSubjectResponse
            .subscribe(onNext: { apiStatus in
                
            }, onError:{ error in
                
            })
            .disposed(by:disposeBag)
    }
    
    
    @objc func panGesterCliked(_ panGester: UIPanGestureRecognizer){
        
        if panGester.state == .began || panGester.state == .changed{
            let traslation = panGester.translation(in: UIApplication.shared.keyWindow)
            panGester.view?.center =  CGPoint(x: panGester.view!.center.x + traslation.x, y: panGester.view!.center.y + traslation.y)
            
            panGester.setTranslation(CGPoint.zero, in: UIApplication.shared.keyWindow)
        }
        
    }
    
    
    @objc func swipeGesterCliked(_ swipeGester: UISwipeGestureRecognizer){
        
        
        if isvideoSwiped == false{
            
            self.frame = CGRect.init(x: 50, y: 300, width:150, height: 150)
            self.layer.cornerRadius = self.frame.size.width/2
            self.clipsToBounds = true
            
            self.localCameraPerviewView.isHidden = true
            self.switch_btn.isHidden = true
            self.endVcall_btn.isHidden = true
            self.muteBtn.isHidden = true
            isvideoSwiped = true
            if self.chatViewObj != nil {self.chatViewObj?.inputToolbar.isHidden = false}
            self.addGestureRecognizer(panGester!)
            
        }else{
            
        }
        
    }
    
    @objc func tapGesterCliked(){
        if isvideoSwiped == false{
            UIView.animate(withDuration: 3 ,delay:5 , options: .showHideTransitionViews , animations: {
                //                self.localViewConstantY.constant = -(self.localViewConstantY.constant + self.localView.frame.size.height)
                self.endBtnConstantY.constant =  -(self.endBtnConstantY.constant + self.endVcall_btn.frame.size.height)
            }) { (isCompl) in}
            
        }else{
            isvideoSwiped = false
            self.frame = CGRect.init(x: 0, y: 0, width:UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.layer.cornerRadius = 0
            self.clipsToBounds = false
            //            self.localView.isHidden = false
            self.localCameraPerviewView.isHidden = false
            self.switch_btn.isHidden = false
            self.endVcall_btn.isHidden = false
            self.muteBtn.isHidden = false
            if self.chatViewObj != nil {
                self.chatViewObj?.inputToolbar.contentView?.textView?.resignFirstResponder()
                self.chatViewObj?.inputToolbar.isHidden = true}
            self.removeGestureRecognizer(panGester!)
        }
    }
    
    
    
    
    //timeout remove audioScreen
    @objc func timeoutRemoveScreen() {
        
        Helper.hidePushNotifications(hidePush: false)
        
        UserDefaults.standard.set(false, forKey: "iscallgoingOn")
        UserDefaults.standard.synchronize()
        
        timer?.invalidate()
        player?.stop()
        webRtc?.disconnect()
        DDLogDebug("***\n\n**********time out called ***************\n*****")
        guard let userID =  Helper.getMQTTID() else { return }
        MQTTCallManager.sendcallAvilibilityStatus(status: 1, topic: "CallsAvailability/" + userID)
        
        if  var callerID = messageDict?["callerIdentifier"] as? String {
            
            if userID == callerID {
                callerID = messageDict?["callerId"] as! String
            }
            
            MQTTCallManager.sendTimeoutcallRequestt(callID: callId!, callerID: callerID, type: 7, targetID: messageDict?["callerIdentifier"] as! String, callType: CallTypes.videoCall)
            
        }
        
        if self.chatViewObj != nil {self.chatViewObj?.inputToolbar.isHidden = false}
        self.removeFromSuperview()
        Helper.hidePushNotifications(hidePush: false)
        
    }
    
    
    
    
    //init webRtc
    func initWebrtc(messageData: [String:Any])  {
        
        UserDefaults.standard.set(true, forKey: "iscallgoingOn")
        UserDefaults.standard.synchronize()
        
        tapGester = UITapGestureRecognizer.init(target: self, action: #selector(tapGesterCliked))
        tapGester?.numberOfTapsRequired = 1
        tapGester?.delegate = self
        remoteView.subviews[0].addGestureRecognizer(tapGester!)
        
        swipeGester = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeGesterCliked))
        swipeGester?.numberOfTouchesRequired = 1
        swipeGester?.direction = .down
        contentView.addGestureRecognizer(swipeGester!)
        panGester = UIPanGestureRecognizer.init(target: self, action: #selector(panGesterCliked))
        
        webRtc = webRTC.init(localView: self.localCameraPerviewView, remoteView: self.remoteView, callID: messageData["callId"] as! String)
        callId = messageData["callId"] as? String
        webRtc?.delegate = self
        messageDict = messageData
    }
    
    
    @IBAction func endBtncliked(_ sender: Any) {
        
        timer?.invalidate()
        webRtc?.disconnect()
        guard let userID = Helper.getMQTTID() else { return }
        
        UserDefaults.standard.set(false, forKey: "iscallgoingOn")
        UserDefaults.standard.synchronize()
        
        MQTTCallManager.sendcallAvilibilityStatus(status: 1, topic:  MQTTTopic.callsAvailability + userID)
        
        var dateIdFromAccept = ""
        
        if let dateId = messageDict?["dateId"] as? String {
            dateIdFromAccept = dateId
        }
        
        MQTTCallManager.sendEndcallRequest(callID: callId!, callerID: callerId!, targetID: userID, callType: CallTypes.videoCall,dateID:dateIdFromAccept)
        
        
        ///play endcall sound here
        self.playSound("end_of_call", loop: 0)
        
        let when = DispatchTime.now() + 0.30
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if self.chatViewObj != nil {self.chatViewObj?.inputToolbar.isHidden = false}
            
            self.captureSession.stopRunning()
            self.removeFromSuperview()
            Helper.hidePushNotifications(hidePush: false)
            //            if(self.showRatingView) {
            //               self.mainViewFoRating.isHidden = false
            //            } else {
            //                self.removeFromSuperview()
            //                Helper.hidePushNotifications(hidePush: false)
            //            }
            
        }
    }
    
    @IBAction func acceptBtnCliked(_ sender: Any) {
        
        
        hideSubviews()
        player?.stop()
        timer?.invalidate()
        
        showRatingView = true
        
        MQTTCallManager.sendAcceptCallStatus(messageData: messageDict!)
        // self.removeCameraLayer()
        self.initWebrtc(messageData:messageDict! )
        
        // webRtc?.switchLocalRemoteView(_localCameraPerviewView: self.localCameraPerviewView, _remoteView: self.remoteView)
        
    }
    
    
    func switchViews() {
        
        webRtc?.switchLocalRemoteView(_localView: nil , _remoteView: self.remoteView)
        localCameraPerviewView.isHidden = false
        userImageView.isHidden = true
        calling_userName.isHidden = true
        calling_status.isHidden = true
        
    }
    
    
    
    func hideSubviews() {
        localCameraPerviewView.isHidden = false
        userName.isHidden = true
        videoCallLbl.isHidden = true
        videoCallIcon.isHidden = true
        incomingVidLbl.isHidden = true
        callEndBtn.isHidden = true
        callAcceptBtn.isHidden = true
        declineLbl.isHidden = true
        acceptlbl.isHidden = true
        
        switch_btn.isHidden = false
        endVcall_btn.isHidden = false
        muteBtn.isHidden = false
        
        userImageView.isHidden = true
        calling_userName.isHidden = true
        calling_status.isHidden = true
    }
    
    
    /*methodes after pick call*/
    
    
    //switch camera button action//
    @IBAction func switchBtnCliked(_ sender: Any) {
        webRtc?.switchCamera()
        //        if isSwitch == false{
        //            isSwitch = true
        //            webRtc?.client?.swapCameraToBack()
        //        }else{
        //            isSwitch = false
        //            webRtc?.client?.swapCameraToFront()
        //        }
        
    }
    
    
    //    func appClient(_ client: ARDAppClient!, didReceiveRemoteVideoTrack remoteVideoTrack: RTCVideoTrack!) {
    //        self.previewForVideo.isHidden = true
    //    }
    
    
    //end video button action//
    @IBAction func endVideocallCliked(_ sender: Any) {
        self.endVideoCall()
    }
    
    
    func endVideoCall(){
        player?.stop()
        timer?.invalidate()
        webRtc?.disconnect()
        guard let userID = Helper.getMQTTID() else { return }
        MQTTCallManager.sendcallAvilibilityStatus(status: 1, topic: MQTTTopic.callsAvailability + userID)
        
        if let uuid = messageDict?["userId"] as? String{
            messageDict?["callerId"] = uuid
            messageDict?["callerIdentifier"] = userID
        }
        
        if  let callerID = messageDict?["callerId"] as? String {
            
            if userID != callerID {
                otherCallerId = callerID
            }
            
            var tempDateID = ""
            if let datId = messageDict?["dateId"] as? String {
                tempDateID = datId
            }
            
            MQTTCallManager.sendEndcallRequest(callID: callId!, callerID: otherCallerId!, targetID: messageDict?["callerIdentifier"] as! String, callType: CallTypes.videoCall,dateID:tempDateID)
        }
        
        ///play endcall sound here
        self.playSound("end_of_call", loop: 0)
        
        UserDefaults.standard.set(false, forKey: "iscallgoingOn")
        UserDefaults.standard.synchronize()
        
        let when = DispatchTime.now() + 0.30
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if self.chatViewObj != nil {self.chatViewObj?.inputToolbar.isHidden = false}
            self.captureSession.stopRunning()
            self.removeFromSuperview()
            Helper.hidePushNotifications(hidePush: false)
            
            if(self.showRatingView){
                self.requestForEndDate()
            }
        }
    }
    
    
    
    func playSound(_ soundName: String,loop: Int){
        
        guard let url = Bundle.main.url(forResource: soundName, withExtension: "wav")else{ return}
        do {
            
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            player  = try AVAudioPlayer(contentsOf: url)
            player?.delegate = self
            player?.numberOfLoops = loop
            guard let player = player else { return}
            player.play()
            
        }catch let error{
            DDLogDebug("error \(error.localizedDescription)")
        }
        
    }
    
    
    
    
    //mute button action//
    @IBAction func muteBtnCliked(_ sender: Any) {
        
        
        if muteBtn.isSelected == true{
            webRtc?.client?.unmuteAudioIn()
            muteBtn.isSelected = false
        }else{
            muteBtn.isSelected = true
            webRtc?.client?.muteAudioIn()}
        
    }
    
    
    func setCallId() {
        callId = randomString(length: 100)
        hideSubviews()
        localCameraPerviewView.isHidden = true
        userImageView.isHidden = false
        calling_userName.isHidden = false
        calling_status.isHidden = false
    }
    
    
    
    
    //Add camera layer
    func addCameraView(){
        
        // let device  = AVCaptureDevice.defaultDevice(withDeviceType: AVCaptureDevice.DeviceType.builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: AVCaptureDevice.Position.front)
        guard let device = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.front).devices.first else {
            return
        }
        do {
            
            if device != nil{
                let input = try AVCaptureDeviceInput.init(device: device)
                if captureSession.canAddInput(input){
                    captureSession.addInput(input)
                    sessionOutput.outputSettings = [AVVideoCodecKey  : AVVideoCodecJPEG]
                    
                    if captureSession.canAddOutput(sessionOutput){
                        captureSession.addOutput(sessionOutput)
                        captureSession.startRunning()
                        previewLayer = AVCaptureVideoPreviewLayer.init(session: captureSession)
                        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                        previewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                        
                        self.contentView.layer.addSublayer(previewLayer)
                        previewLayer.position = CGPoint.init(x: self.frame.width/2, y: self.frame.height/2)
                        previewLayer.bounds = self.frame
                        
                        
                        self.remoteView.bringSubview(toFront: self.previewForVideo)
                        self.contentView.bringSubview(toFront: callEndBtn)
                        self.contentView.bringSubview(toFront: acceptlbl)
                        self.contentView.bringSubview(toFront: declineLbl)
                        self.contentView.bringSubview(toFront: callAcceptBtn)
                        self.contentView.bringSubview(toFront: videoCallLbl)
                        self.contentView.bringSubview(toFront: videoCallIcon)
                        self.contentView.bringSubview(toFront: userName)
                        self.contentView.bringSubview(toFront: incomingVidLbl)
                        self.contentView.bringSubview(toFront: switch_btn)
                        self.contentView.bringSubview(toFront: endVcall_btn)
                        self.contentView.bringSubview(toFront: muteBtn)
                        self.contentView.bringSubview(toFront: userImageView)
                        self.contentView.bringSubview(toFront: calling_status)
                        self.contentView.bringSubview(toFront: calling_userName)
                        self.contentView.bringSubview(toFront: self.mainViewFoRating)
                        userProfileImageView.image = userImageView.image
                        userNameLabel.text = calling_userName.text
                        userProfileImageView.layer.borderColor = UIColor.white.cgColor
                        
                    }
                }
                
            }
        }
        catch{
            
            DDLogDebug("print error")
        }
    }
    
    //remove camera layer
    func removeCameraLayer(){
        self.contentView.layer.addSublayer(previewLayer)
        captureSession.stopRunning()
        previewLayer.removeFromSuperlayer()
    }
    
    /// Time action after trying to reconnect call
    ///
    /// - Parameter timer: timer objec
    @objc func endCallAfterRconnectionFail(_ timer: Timer){
        if self.isDisconnected{
            self.endVideoCall()
        }
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if(touch.view == self.mainViewFoRating) {
            return false
        }
        return touch.view == gestureRecognizer.view
    }
    
}

extension IncomingVideocallView: AVAudioPlayerDelegate{
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool){
        
        DDLogDebug("sound finished here ...")
        
    }
}

extension IncomingVideocallView : FloatRatingViewDelegate
{
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        
    }
}


extension IncomingVideocallView : webRTCdelegate{
    
    func appClientStatus(_ client: ARDAppClient, status: ARDAppClientState) {
        
        DDLogDebug("webRTC status changed =\(status)")
        
        switch (status){
        case ARDAppClientState.connected :
            showRatingView = true
            self.previewForVideo.isHidden = true
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, animations: {
                    self.localCameraPerviewView.frame = self.localViewFrame
                    self.layoutIfNeeded()
                    self.removeCameraLayer()
                })
                self.timer?.invalidate()
                self.isDisconnected = false
                if let endTimer = self.endCallTimer{
                    endTimer.invalidate()
                    self.endCallTimer = nil
                    self.calling_status.isHidden = true
                }
            }
            break
        case ARDAppClientState.connecting :
            DispatchQueue.main.async {
                self.localViewFrame = self.localCameraPerviewView.frame
                self.localCameraPerviewView.frame = self.frame
                self.layoutIfNeeded()
                self.switchViews()
            }
            break
        case ARDAppClientState.disconnected:
            DispatchQueue.main.async {
                self.isDisconnected = true
                self.calling_status.isHidden = false
                self.calling_status.text = StringConstants.reconnecting()
                self.endCallTimer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(self.endCallAfterRconnectionFail(_:)), userInfo: nil, repeats: false)
            }
            break
        }
        
    }
    
}




