//
//  TimerPopUpView.swift
//  Datum
//
//  Created by Shivansh on 9/19/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

enum timerPopType {
    case boost
    case outOfLikes
    case outOfRewinds
}

class TimerPopUpView: UIView {
    
    @IBOutlet weak var featureImage: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var subView: UIView!
    var timerForOutOflikes:Timer? = nil
    var popupFor = timerPopType.boost
    
    @IBOutlet weak var featureTitle: UILabel!
    
    @IBOutlet weak var countDownTimerLable: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadTimerPopupView() {
        Bundle.main.loadNibNamed("TimerPopUpView", owner:self, options:nil)
        self.mainView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        
        self.mainView.frame = self.bounds
        self.addSubview(self.mainView)
        self.mainView.layoutIfNeeded()
        
        self.subView.layer.cornerRadius = 18
        
        var frameForAnimation = self.subView.frame
        frameForAnimation.origin.y = -900
        self.subView.frame = frameForAnimation
        self.subView.layoutIfNeeded()
        updateFeatureTitle()
        updateTime()
        addTimerForOutOfLikes()
        
        UIView.animate(withDuration:0.3, delay:0.0, usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 0.7, options: [], animations:
            {
                frameForAnimation.origin.y = 0
                self.subView.frame = frameForAnimation
        }, completion:{ (finished) in
            if(finished) {
                
            }
        })
        
        
        
        
        
        
    }
    
    @IBAction func okButtonAction(_ sender: Any) {
        removeViewWithAnimation()
    }
    
    
    func addTimerForOutOfLikes() {
        //adding timer for updating timer for outoflikes.
        timerForOutOflikes = Timer.scheduledTimer(timeInterval:1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    func updateFeatureTitle() {
        if popupFor == timerPopType.boost{
            self.featureTitle.text = StringConstants.boostWillEnd()
            self.featureImage.image = UIImage.init(named: "boost")
        } else if popupFor == timerPopType.outOfRewinds {
            self.featureTitle.text = StringConstants.outOfRewind()
            self.featureImage.image = UIImage.init(named: "unlimited_rewinds")
        }
        else {
            self.featureTitle.text = StringConstants.outOfLikes()
            self.featureImage.image = #imageLiteral(resourceName: "DPLikes")
        }
    }
    
    @objc func updateTime() {
        if popupFor == timerPopType.boost{
            let boostData = Helper.getActiveBoostDetails()
            
            if let boostDetails = boostData["boost"] as? [String:Any] {
                
                if let expireTime = boostDetails["expire"] as? Double {
                    let expireDate:Date = Date(timeIntervalSince1970: Double(expireTime/1000))
                    let diffrenceDate = expireDate.offsetFrom(date:Date())
                    self.countDownTimerLable.text = diffrenceDate
                }else {
                    // no timer details available.
                    self.countDownTimerLable.text  = ""
                }
                
            }else {
                if let expireTime = boostData["expire"] as? Double {
                    let expireDate:Date = Date(timeIntervalSince1970: Double(expireTime/1000))
                    let diffrenceDate = expireDate.offsetFrom(date:Date())
                    self.countDownTimerLable.text = diffrenceDate
                }else{
                    self.countDownTimerLable.text  = ""
                    
                }
                
            }
            
            
        }
//        else if popupFor == timerPopType.outOfRewinds {
//            if(Helper.isUserOutOfRewinds()) {
//                let  syncDate:Date = Date(timeIntervalSince1970: Double(Helper.newTimeForOutOfRewinds()))
//                let diffrenceDate = syncDate.offsetFrom(date: Date())
//                self.countDownTimerLable.text = diffrenceDate
//            } else {
//                // no timer details available.
//                self.countDownTimerLable.text  = ""
//            }
//        } else {
//            if(Helper.isUserOutOfLikes()) {
//                let  syncDate:Date = Date(timeIntervalSince1970: Double(Helper.newTimeForOutOfLikes()))
//                let diffrenceDate = syncDate.offsetFrom(date: Date())
//                self.countDownTimerLable.text = diffrenceDate
//            } else {
//                // no timer details available.
//                self.countDownTimerLable.text  = ""
//            }
//        }
    }
    
    func removeViewWithAnimation() {
        var frameForAnimation = self.subView.frame
        frameForAnimation.origin.y = 0
        self.subView.frame = frameForAnimation
        self.subView.layoutIfNeeded()
        
        self.timerForOutOflikes?.invalidate()
        
        UIView.animate(withDuration:0.5, delay: 0.0, usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.5, options: [], animations:
            {
                frameForAnimation.origin.y = self.frame.size.height + 500
                self.subView.frame = frameForAnimation
        }, completion:{ (finished) in
            if(finished) {
                self.removeFromSuperview()
            }
        })
    }
    
}
