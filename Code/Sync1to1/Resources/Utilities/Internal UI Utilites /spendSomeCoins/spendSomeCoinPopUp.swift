//
//  spendSomeCoinPopUp.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 01/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

protocol spendSomeCoinPopUpDelegate: class{ //getting delegate methods for the button actions
    func coinBtnPressed(sender: UIButton)
    
}

class spendSomeCoinPopUp: UIView {
    
    @IBOutlet weak var imageForCoinsSpend: UIImageView!
    @IBOutlet weak var messageForCoinsSpend: UILabel!
    @IBOutlet weak var titleForCoinsSpend: UILabel!
    @IBOutlet weak var subView: UIView!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var coinsButton: UIButton!
    
    @IBOutlet weak var dontShowViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dontShowView: UIView!
    var message = StringConstants.getChanceSuperLike()
    var title = StringConstants.spendCoinsToInPerson()
    weak var spendSomeCoinDelegate : spendSomeCoinPopUpDelegate? = nil
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.removeViewWithAnimation()
        
    }
    
    @IBAction func coinBtnAction(_ sender: Any) {
        let btn = sender as! UIButton
        self.spendSomeCoinDelegate?.coinBtnPressed(sender: btn)
        self.removeViewWithAnimation()
    }
    
    @IBAction func dontShowAgainPopUp(_ sender: UIButton) {
        
    }

    // method to load reasons xib.
    func loadPopUpView() {
        // ContactUS name of the XIB.
        Bundle.main.loadNibNamed("SpendCoinsPopUp", owner:self, options:nil)

        self.messageForCoinsSpend.text = message
        self.titleForCoinsSpend.text = title

        self.backgroundColor = UIColor.black.withAlphaComponent(0.26)
        self.subView.layer.cornerRadius = 20
        Helper.setShadow(sender: self.subView)
        self.mainView.frame = self.bounds
        self.addSubview(self.mainView)
        self.mainView.layoutIfNeeded()

        self.subView.transform = CGAffineTransform(scaleX: 0.2, y:0.2)
        UIView.animate(withDuration:1.0, delay: 0.0, usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.5, options: [], animations:
            {
                self.subView.transform = CGAffineTransform(scaleX: 1.0, y:1.0)
        }, completion:nil)
    }
    
    
    func updatePopup(icon:UIImage,mainTitle: String,subTitle: String,buttnTitle: String,dontShowHide: Bool) {
        dontShowViewHeight.constant = 0
        self.imageForCoinsSpend.image = icon
        self.titleForCoinsSpend.text = mainTitle
        self.messageForCoinsSpend.text = subTitle
        self.coinsButton.setTitle(buttnTitle, for: .normal)
    }

    
//    func removeViewWithAnimation() {
//        self.subView.transform = CGAffineTransform.identity
//        UIView.animate(withDuration: 0.1, animations: {
//            self.subView.transform = CGAffineTransform(scaleX: 1.01, y:1.01)
//        } ,completion:{ (finished) in
//            if(finished) {
//                self.alpha = 1.0
//                UIView.animate(withDuration:0.1, animations: {        //changed 0.5 to 0.1
//                    self.alpha = 0
//                    self.subView.transform = CGAffineTransform(scaleX: 0.2, y:0.2)
//                }, completion: { (finished) in
//                    if(finished) {
//                        self.removeFromSuperview()
//                    }
//                })
//            }
//        })
//    }
    
    func removeViewWithAnimation() {
        var frameForAnimation = self.bounds
        frameForAnimation.origin.y = 0
        self.mainView.frame = frameForAnimation
        self.mainView.layoutIfNeeded()
        
        self.subView.transform = CGAffineTransform.identity
        UIView.animate(withDuration: 0.1, animations: {
            self.subView.transform = CGAffineTransform(scaleX: 1.01, y:1.01)
        } ,completion:{ (finished) in
            if(finished) {
                self.alpha = 1.0
                UIView.animate(withDuration:0.5, animations: {
                    self.alpha = 0
                    self.subView.transform = CGAffineTransform(scaleX: 0.2, y:0.2)
                }, completion: { (finished) in
                    if(finished) {
                        self.removeFromSuperview()
                    }
                })
            }
        })
    }
   
}
