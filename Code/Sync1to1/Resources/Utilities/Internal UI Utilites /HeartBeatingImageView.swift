//
//  HeartBeatingImageView.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 27/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import PulsingHalo

struct Constants {
    static let animationKeyForScale = "transform.scale"
    static let animationKeyForRotation = "transform.rotation"
    static let animationKeyForPulse = "pulse"
}
class HeartBeatingImageView: UIImageView {
    
    struct Constant {
        static let animationDuration = 0.3
        static let currentValueOfImage = 1.0
        static let transforimgValueOfImage = 0.7
        static let animationKey = "animateOpacity"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func startHeartBeating(){
        self.image = #imageLiteral(resourceName: "heart_icon")
        let animation = CABasicAnimation(keyPath: Constants.animationKeyForScale)
        animation.duration = Constant.animationDuration
        animation.repeatCount = Float.greatestFiniteMagnitude
        animation.autoreverses = true
        animation.fromValue = Constant.currentValueOfImage
        animation.toValue = Constant.transforimgValueOfImage
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        self.layer.add(animation, forKey: Constant.animationKey)
    }
    
    func startHeartBeatAnimation() {
        let animation = CABasicAnimation(keyPath: Constants.animationKeyForScale)
        animation.duration = Constant.animationDuration
        animation.repeatCount = Float.greatestFiniteMagnitude
        animation.autoreverses = true
        animation.fromValue = Constant.currentValueOfImage
        animation.toValue = Constant.transforimgValueOfImage
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        self.layer.add(animation, forKey: Constant.animationKey)
    }
}

extension UIImageView {
   
    func startHeartBeat() {
        let animation = CABasicAnimation(keyPath: Constants.animationKeyForScale)
        animation.duration = 0.4
        animation.repeatCount = Float.greatestFiniteMagnitude
        animation.autoreverses = true
        animation.fromValue = 1.0
        animation.toValue = 0.5
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        self.layer.add(animation, forKey: "animateOpacity")
    }
}

extension UIButton {
    func haloEffectForBoost(){
        let halo = PulsingHaloLayer()
        halo.haloLayerNumber = 1
        halo.radius = self.frame.size.width/2
        self.layer.insertSublayer(halo, below: self.layer)
        halo.backgroundColor = Colors.boostColor.cgColor
        halo.position = self.center
        halo.start()
    }
}
