//
//  HeaderView.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 08/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

class HeaderView: UIView {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var backgroundImageView: UIView!
    
    
    
    
    
    func getUserimage(){
        let userData = Database.shared.fetchResults()
        if !userData.isEmpty
        {
            if let userDetails = userData.first {
                userName.text = userDetails.first_name
            
                if userDetails.work?.count != 0 {
                    self.subTitle.text = userDetails.work
                }else if userDetails.education?.count != 0 {
                    self.subTitle.text = userDetails.education
                }else if userDetails.job?.count != 0 {
                    self.subTitle.text = userDetails.job
                }else {
                    self.subTitle.text = userDetails.gender
                }
                
                if let dob = userDetails.dateOfBirthTimeStamp as String? {
                    if(dob != "") {
                        userName.text = userDetails.first_name?.appending(", ".appending(String(Helper.dateFromMilliseconds(timestamp:String(dob)).age)))
                    }
                }
                
                
                if let urlString:String = userDetails.profilePictureURL {
                    if urlString.characters.count>0 {
                        let url = URL(string: urlString)
                        var imageName = "man_default"
                        if userDetails.gender == "Male" {
                            imageName = "man_default"
                        }else {
                            imageName = "woman_default"
                        }
                        userImageView.kf.setImage(with: url, placeholder: UIImage.init(named: imageName), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
                        //backgroundImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "user_default_image"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: nil)
                    }else{
                        userImageView.image = #imageLiteral(resourceName: "Splash")
                      //  backgroundImageView.image = #imageLiteral(resourceName: "Splash")
                    }
                }
            }
        }
    }
}


extension UIView {
    
    func startBlink() {
        UIView.animate(withDuration: 0.8,
                       delay:0.0,
                       options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                       animations: { self.alpha = 0 },
                       completion: nil)
    }
    
    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}
