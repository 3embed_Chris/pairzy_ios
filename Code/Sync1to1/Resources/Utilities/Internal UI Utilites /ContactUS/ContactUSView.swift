//
//  ContactUSView.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 06/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

protocol contactusDelegate: class {
    func doneButtonSelected(name:String,emailAddress:String,issue:String)
}



class ContactUSView: UIView {
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var mainView: UIView!
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
   weak var contactDelegate:contactusDelegate? = nil
    var selectedReasonForContact = ""
    
    @IBOutlet weak var doneButtonOutlet: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var issueTextView: UITextView!
    
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.removeViewWithAnimation()
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        self.endEditing(true)
        
        let nameText = self.nameTextField.text!
        let emailText = self.emailTextField.text!
        let issueText = self.issueTextView.text!
        
        if(!nameText.isBlank && !emailText.isBlank && !issueText.isBlank) {
            self.removeViewWithAnimation()
            self.contactDelegate?.doneButtonSelected(name: nameText, emailAddress:emailText, issue: issueText)
        } else {
            if(nameText.isBlank) {
                self.nameTextField.shake()
            } else if (emailText.isBlank){
                self.emailTextField.shake()
            } else if(issueText.isBlank) {
                self.issueTextView.shake()
            }
        }
    }
    
    
    func removeViewWithAnimation() {
        var frameForAnimation = self.bounds
        frameForAnimation.origin.y = 0
        self.mainView.frame = frameForAnimation
        self.mainView.layoutIfNeeded()
        
        self.subView.transform = CGAffineTransform.identity
        UIView.animate(withDuration: 0.1, animations: {
            self.subView.transform = CGAffineTransform(scaleX: 1.01, y:1.01)
        } ,completion:{ (finished) in
            if(finished) {
                self.alpha = 1.0
                UIView.animate(withDuration:0.5, animations: {
                    self.alpha = 0
                    self.subView.transform = CGAffineTransform(scaleX: 0.2, y:0.2)
                }, completion: { (finished) in
                    if(finished) {
                        self.removeFromSuperview()
                    }
                })
            }
        })
    }
    
    ///  when user touches view textField editing is ended.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }
    
    func updatePlaceHolderText() {
        if(selectedReasonForContact == StringConstants.askQuestion()) {
            self.issueTextView.text = StringConstants.yourQuestion()
            issueTextView.textColor = Helper.getUIColor(color: "#B2B2B2")
        } else if(selectedReasonForContact == StringConstants.makeSuggetion()) {
            self.issueTextView.text = StringConstants.yourSuggesion()
            issueTextView.textColor = Helper.getUIColor(color: "#B2B2B2")
        }else {
            self.issueTextView.text = StringConstants.yourIssue()
            issueTextView.textColor = Helper.getUIColor(color: "#B2B2B2")
        }
        
    }
    
    /// fetch detials from core data and displaying in view.
    func getUserDetails(){
        let userData = Database.shared.fetchResults()
        if !userData.isEmpty
        {
            if let userDetails = userData.first
            {
                if let fName = userDetails.first_name {
                    self.nameTextField.text = fName
                }
                
                if let emailId = userDetails.emailId {
                    self.emailTextField.text = emailId
                }
            }
        }
    }
    
    /// method to load reasons xib.
    func loadContactUsView() {
        // ContactUS name of the XIB.
        Bundle.main.loadNibNamed("ContactUS", owner:self, options:nil)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.titleLabel.text = selectedReasonForContact
        nameTextField.placeholder = StringConstants.name()
        emailTextField.placeholder = StringConstants.emailAddress()
        updatePlaceHolderText()
        getUserDetails()
        self.mainView.frame = self.bounds
        self.addSubview(self.mainView)
        self.mainView.layoutIfNeeded()
        var frameForAnimation = self.subView.frame
        frameForAnimation.origin.y = -900
        self.subView.frame = frameForAnimation
        self.subView.layoutIfNeeded()
        
        UIView.animate(withDuration:0.6, delay:0.0, usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 0.7, options: [], animations:
            {
                frameForAnimation.origin.y = 0
                self.subView.frame = frameForAnimation
        }, completion:{ (finished) in
            if(finished) {
                
            }
        })
    }
}

extension ContactUSView: UITextViewDelegate,UITextFieldDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if (textView.text == StringConstants.yourSuggesion())||(textView.text == StringConstants.yourQuestion())||(textView.text == StringConstants.yourIssue()) {
            textView.textColor = Colors.PrimaryText
            textView.text = ""
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            self.updatePlaceHolderText()
            
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField.tag == 0) {
            self.emailTextField.becomeFirstResponder()
        } else if(textField.tag == 1){
            self.issueTextView.becomeFirstResponder()
        } else {
            self.endEditing(true)
        }
        return true
    }
    
}






