//
//  MediaDownloader.swift
//  MQTT Chat Module
//
//  Created by Sachin Nautiyal on 21/02/2018.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

import UIKit
import Alamofire
import Photos

/// Download and save video to camera roll
final class VideoFetcher {
    private let fileManager: FileManager
    
    init(fileManager: FileManager = FileManager.default) {
        self.fileManager = fileManager
    }
    
    func downloadAndSave(videoUrl: URL,fileName : String, progress : @escaping (Progress) -> Void, completionBlock: @escaping (URL?) -> Void) {
        let destination: (URL, HTTPURLResponse) -> (URL, DownloadRequest.DownloadOptions) = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent("\(fileName)")
            return (documentsURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        DispatchQueue.global(qos: .default).async {
            Alamofire.download(videoUrl, to: destination)
                .downloadProgress(closure: { (prgs) in
                    progress(prgs)
                })
                .response(completionHandler: { response in
                    guard response.error == nil,
                        let destinationUrl = response.destinationURL else {
                            completionBlock(nil)
                            return
                    }
                    completionBlock(destinationUrl)
//                    PHPhotoLibrary.shared().performChanges({
//                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: destinationUrl)
//                    }, completionHandler: {
//                        succeeded, error in
//                        guard error == nil, succeeded else {
//                            return
//                        }
//                    })
                })
        }
    }
}

class MediaDownloader {
    func SaveMedia(withURL urlString: String, andData data: Data) -> URL? {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let fileName = urlString as NSString
        let filePath="file://\(documentsPath)/\(fileName.lastPathComponent)"
        let fileExists = FileManager().fileExists(atPath: filePath)
        let mediaURL = URL(string:filePath)!
        if(fileExists) {
            // File is already Saved
            print("Media Already Exist")
            return mediaURL
        }
        else {
            //Save it locally.
            do {
                try data.write(to: mediaURL, options: [])
                print("media Saved")
                return mediaURL
                
            } catch let error {
                print("Got Error on writing the data",error.localizedDescription)
                return nil
            }
        }
    }
}
