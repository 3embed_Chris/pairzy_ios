//
//  RewardPopup.swift
//  Datum
//
//  Created by 3 Embed on 19/12/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

class RewardPopup: UIView {
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var coins: UILabel!
    @IBOutlet weak var chancesLeft: UILabel!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var okBtn: UIButton!
 
    @IBAction func closeBtnAction(_ sender: Any) {
        self.removeViewWithAnimation()
        
    }
    
    @IBAction func okBtnAction(_ sender: Any) {
        self.removeViewWithAnimation()
    }
    
  
    // method to load reasons xib.
    func loadPopUpView() {
        // ContactUS name of the XIB.
        Bundle.main.loadNibNamed("RewardPopup", owner:self, options:nil)
        
        self.backgroundColor = UIColor.black.withAlphaComponent(0.26)
        self.subView.layer.cornerRadius = 20
        Helper.setShadow(sender: self.subView)
        self.mainView.frame = self.bounds
        self.addSubview(self.mainView)
        self.mainView.layoutIfNeeded()
        
        self.subView.transform = CGAffineTransform(scaleX: 0.2, y:0.2)
        UIView.animate(withDuration:1.0, delay: 0.0, usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.5, options: [], animations:
            {
                self.subView.transform = CGAffineTransform(scaleX: 1.0, y:1.0)
        }, completion:nil)
    }
    
    func removeViewWithAnimation() {
        var frameForAnimation = self.bounds
        frameForAnimation.origin.y = 0
        self.mainView.frame = frameForAnimation
        self.mainView.layoutIfNeeded()
        
        self.subView.transform = CGAffineTransform.identity
        UIView.animate(withDuration: 0.1, animations: {
            self.subView.transform = CGAffineTransform(scaleX: 1.01, y:1.01)
        } ,completion:{ (finished) in
            if(finished) {
                self.alpha = 1.0
                UIView.animate(withDuration:0.5, animations: {
                    self.alpha = 0
                    self.subView.transform = CGAffineTransform(scaleX: 0.2, y:0.2)
                }, completion: { (finished) in
                    if(finished) {
                        self.removeFromSuperview()
                    }
                })
            }
        })
    }
    
}
