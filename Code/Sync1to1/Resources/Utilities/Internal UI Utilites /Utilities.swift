//
//  Utilities.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 15/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

class Utility {
    
    var lightBlueColor: UIColor!{
        var color = UIColor.blue
        if #available(iOS 10.0, *) {
            color = UIColor(displayP3Red: 0.0, green: 0.659, blue: 1.0, alpha: 1.0)
        }
        return color
    }
    
    var lightYellowColor: UIColor!{
        var color = UIColor.yellow
        if #available(iOS 10.0, *) {
            color = UIColor(displayP3Red: 1.0, green: 0.761, blue: 0.0, alpha: 1.0)
        }
        return color
    }
    
    var lightGreenColor: UIColor!{
        var color = UIColor.green
        if #available(iOS 10.0, *) {
            color = UIColor(displayP3Red: 0.298, green: 0.800, blue: 0.573, alpha: 1.0)
        }
        return color
    }
    
    var lightRedColor: UIColor!{
        var color = UIColor.red
        if #available(iOS 10.0, *) {
            color = UIColor(displayP3Red: 1.0, green: 0.282, blue: 0.282, alpha: 1.0)
        }
        return color
    }
    
    func relativePast(for date : Date) -> String {
        
        let units = Set<Calendar.Component>([.year, .month, .day, .hour, .minute, .second, .weekOfYear])
        let components = Calendar.current.dateComponents(units, from: date, to: Date())
        
        if components.year! > 0 {
            return "\(components.year!) " + (components.year! > 1 ? "years ago" : "year ago")
            
        } else if components.month! > 0 {
            return "\(components.month!) " + (components.month! > 1 ? "months ago" : "month ago")
            
        } else if components.weekOfYear! > 0 {
            return "\(components.weekOfYear!) " + (components.weekOfYear! > 1 ? "weeks ago" : "week ago")
            
        } else if (components.day! > 0) {
            return (components.day! > 1 ? "\(components.day!) days ago" : "Yesterday")
            
        } else if components.hour! > 0 {
            return "\(components.hour!) " + (components.hour! > 1 ? "hours ago" : "hour ago")
            
        } else if components.minute! > 0 {
            return "\(components.minute!) " + (components.minute! > 1 ? "minutes ago" : "minute ago")
            
        } else {
            return "\(components.second!) " + (components.second! > 1 ? "seconds ago" : "second ago")
        }
    }
    
    class func saveCurrentAddress(lat:Float, long:Float, address:Location) {
        UserDefaults.standard.set(lat, forKey: "currentLatitude")
        UserDefaults.standard.set(long, forKey: "currentLongitude")
        let currentAddress = ["city": address.city,
                    "state": address.state,
                    "country": address.country,
                    "fullAddress":address.fullText]
        UserDefaults.standard.set(currentAddress, forKey: "currentAddress")
        UserDefaults.standard.synchronize()
    }
    
    class func getCurrentAddress() -> Location {
        let data = Location.init(data: [:])
        if UserDefaults.standard.string(forKey: "currentLongitude") != nil && UserDefaults.standard.string(forKey: "currentLatitude") != nil {
            data.latitude = UserDefaults.standard.double(forKey: "currentLatitude")
            data.longitude = UserDefaults.standard.double(forKey: "currentLongitude")
            if let address = UserDefaults.standard.object(forKey: "currentAddress") as? [String:Any] {
                data.city = address["city"] as! String
                data.state = address["state"] as! String
                data.country = address["country"] as! String
                data.fullText = address["fullAddress"] as! String
            }
        }else{
            data.latitude = 0
            data.longitude = 0
            data.fullText = ""
            LocationManager.shared.start()
        }
        return data
    }
    
   
    
}
