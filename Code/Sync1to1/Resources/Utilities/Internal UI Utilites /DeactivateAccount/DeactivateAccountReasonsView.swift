//
//  DeactivateAccountReasonsView.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 22/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

protocol deactivateReasonsDelegate : class{
    func userSelectedDoneButton(selectedReason:String)
    func userSelectedReasonForReport(selectedReason:ReportReason)
}


class DeactivateAccountReasonsView: UIView {

    @IBOutlet weak var subView: UIView!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var reasonTableView: UITableView!
    var deactivateReasons = [String]()
    weak var deactDelegate:deactivateReasonsDelegate? = nil

    var reportReasons:[ReportReason] = []
    var isViewForReason =  false

    var selectedIndex:Int = 200

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var messageLabel: UILabel!


    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    /// method to load reasons xib.
    func loadDeactivateView() {

        // DeactivteAccountReasons name of the XIB.
        if(isViewForReason) {
            Bundle.main.loadNibNamed("ReportReasonsView", owner:self, options:nil)
        }else {
            Bundle.main.loadNibNamed("DeactivteAccountReasons", owner:self, options:nil)
        }

        self.mainView.backgroundColor = UIColor.black.withAlphaComponent(0.7)

        self.mainView.frame = self.bounds
        self.addSubview(self.mainView)
        self.mainView.layoutIfNeeded()

        self.subView.transform = CGAffineTransform(scaleX: 0.2, y:0.2)
        UIView.animate(withDuration:1.0, delay: 0.0, usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.5, options: [], animations:
            {
                self.subView.transform = CGAffineTransform(scaleX: 1.0, y:1.0)
        }, completion:nil)
    }


    func removeViewWithAnimation() {
        self.subView.transform = CGAffineTransform.identity
        UIView.animate(withDuration: 0.1, animations: {
            self.subView.transform = CGAffineTransform(scaleX: 1.01, y:1.01)
        } ,completion:{ (finished) in
            if(finished) {
                self.alpha = 1.0
                UIView.animate(withDuration:0.5, animations: {
                    self.alpha = 0
                    self.subView.transform = CGAffineTransform(scaleX: 0.2, y:0.2)
                }, completion: { (finished) in
                    if(finished) {
                        self.removeFromSuperview()
                    }
                })
            }
        })
    }

    /// tapgesture to remove popup.
    ///
    /// - Parameter sender: view.
    @IBAction func tapGestureForCloseView(_ sender: Any) {
        self.removeViewWithAnimation()
    }


    /// cancel button action.
    ///
    /// - Parameter sender: cancel button.
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.removeViewWithAnimation()
    }


    /// done button action
    //
    /// - Parameter sender: done button.
    @IBAction func doneButtonAction(_ sender: Any) {
        if(selectedIndex == 200) {

        } else {
            self.removeViewWithAnimation()
            self.deactDelegate?.userSelectedDoneButton(selectedReason:self.deactivateReasons[selectedIndex])
        }
    }

}


extension DeactivateAccountReasonsView:UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isViewForReason) {
            return reportReasons.count
        }else {
            return deactivateReasons.count
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }


    /// cell for row index is tableview data source method
    /// it will trigger when
    /// - Parameters:
    ///   - tableView: reason tableview.
    ///   - indexPath: indexpath for data.
    /// - Returns: reson tableview cell.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        reasonTableView.register(UINib(nibName: "ReasonsTableViewCell", bundle: nil), forCellReuseIdentifier: "reasonCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "reasonCell", for: indexPath) as! ReasonsTableViewCell

        if(indexPath.row == deactivateReasons.count - 1 || indexPath.row == reportReasons.count-1 ) {
            cell.borderView.isHidden = true
        } else {
            cell.borderView.isHidden = false
        }

        if(isViewForReason) {
//            cell.reasonLabel.isHidden = true
            cell.reasonLabel?.text = reportReasons[indexPath.row].nameOfTheReason
            cell.selectionButtonOutlet.isHidden = true
            return cell
        }
        cell.reasonLabel.isHidden = false
        cell.labelForReporteason?.isHidden = true
        if(indexPath.row == selectedIndex) {
            cell.selectionButtonOutlet.isSelected = true
        } else {
            cell.selectionButtonOutlet.isSelected = false
        }

        cell.reasonLabel.text = deactivateReasons[indexPath.row]

        return cell
    }


    /// delegate method for update selected reason.
    ///
    /// - Parameters:
    ///   - tableView: reason tableview
    ///   - indexPath: selected indexpath.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        if(isViewForReason) {
            self.removeViewWithAnimation()
            self.deactDelegate?.userSelectedReasonForReport(selectedReason: self.reportReasons[selectedIndex])
            return
        }

        tableView.reloadData()
        animateselectedButton()
    }

    func animateselectedButton() {
        let cell = self.reasonTableView.cellForRow(at:IndexPath.init(row: selectedIndex, section: 0)) as! ReasonsTableViewCell
        cell.selectionButtonOutlet.addZoomEffect()
    }
}
