//
//  ReasonsTableViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 22/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class ReasonsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var reasonLabel: UILabel!
    
    @IBOutlet weak var selectionButtonOutlet: UIButton!
    
    @IBOutlet weak var borderView: UIView!
    
    
    @IBOutlet weak var labelForReporteason: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
