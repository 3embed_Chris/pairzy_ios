//
//  CampingViewC.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 16/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

protocol CampingDelegate : class{ //getting delegate methods for the button actions
    func sendBtnPressed()
    
}

class CampingView: UIView {
    
    
    weak var delegate : CampingDelegate? = nil
    var campaginDetails:[String:Any] = [:]
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet var mainView: UIView!
    
    
    @IBOutlet weak var campainImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.removeViewWithAnimation()
        
    }
    
    
    @IBAction func sendNotificationAction(_ sender: Any) {
        self.delegate?.sendBtnPressed()
        self.removeViewWithAnimation()
        
    }
    
    func updateView(){
        title.text = campaginDetails["title"] as! String
        subTitle.text = campaginDetails["descrptin"] as! String
        
        let newUrl = URL(string: campaginDetails["imageUrl"] as! String)
        campainImage.kf.setImage(with:newUrl, placeholder:UIImage.init(named:"02m"))
        
    }
    
    
    
    // method to load reasons xib.
    func loadPopUpView() {
        // ContactUS name of the XIB.
        Bundle.main.loadNibNamed("CampaginView", owner:self, options:nil)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.26)
        self.subView.layer.cornerRadius = 20
        Helper.setShadow(sender: self.subView)
        self.mainView.frame = self.bounds
        self.addSubview(self.mainView)
        self.mainView.layoutIfNeeded()
        
        self.subView.transform = CGAffineTransform(scaleX: 0.2, y:0.2)
        UIView.animate(withDuration:1.0, delay: 0.0, usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.5, options: [], animations:
            {
                self.subView.transform = CGAffineTransform(scaleX: 1.0, y:1.0)
        }, completion:nil)
    }
    
    func removeViewWithAnimation() {
        self.subView.transform = CGAffineTransform.identity
        UIView.animate(withDuration: 0.1, animations: {
            self.subView.transform = CGAffineTransform(scaleX: 1.01, y:1.01)
        } ,completion:{ (finished) in
            if(finished) {
                self.alpha = 1.0
                UIView.animate(withDuration:0.5, animations: {
                    self.alpha = 0
                    self.subView.transform = CGAffineTransform(scaleX: 0.2, y:0.2)
                }, completion: { (finished) in
                    if(finished) {
                        self.removeFromSuperview()
                    }
                })
            }
        })
    }
}

