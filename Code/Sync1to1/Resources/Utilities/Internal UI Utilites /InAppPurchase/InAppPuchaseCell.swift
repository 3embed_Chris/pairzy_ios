//
//  InAppPuchaseCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 01/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class InAppPuchaseCell: UICollectionViewCell {

    @IBOutlet weak var featuredescrption: UILabel!
    @IBOutlet weak var featureTitle: UILabel!
    @IBOutlet weak var featureImage: UIImageView!
    @IBOutlet weak var MainHeader: UILabel!
    
    
    
    //@IBOutlet weak var backView: UIView!
   // @IBOutlet weak var backgroundImage: UIGradientImageView!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
    }

//    func updateCell(colorss:[CGColor]){
//        backgroundImage.gradientForMatch()
//
//    }

    func updateCell(data: DatumPlusObj){
       featureTitle.text = data.featureTitle
       featuredescrption.text = data.featureDescrption
       featureImage.image = UIImage.init(named:data.featureImage)
    }
    
}
