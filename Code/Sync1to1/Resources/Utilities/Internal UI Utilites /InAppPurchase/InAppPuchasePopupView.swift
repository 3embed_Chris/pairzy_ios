//
//  InAppPuchasePopupView.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 01/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit



class InAppPuchasePopupView: UIView {
    
    
    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var subView: UIView!
    var scrollTimer: Timer? = nil
    var timerForOutOflikes:Timer? = nil
    
    @IBOutlet weak var subscribeButton: UIButton!
    @IBOutlet weak var dPlusCollectionView: UICollectionView!
    @IBOutlet weak var lastPurchaseView: UIView!
    @IBOutlet weak var lastPMonthsCount: UILabel!
    @IBOutlet weak var lastPmonthLabel: UILabel!
    @IBOutlet weak var lastPAmount: UILabel!
    
    @IBOutlet weak var firstPurchaseView: UIView!
    @IBOutlet weak var firstPMonthsCount: UILabel!
    @IBOutlet weak var firstPmonthLabel: UILabel!
    @IBOutlet weak var firstPAmount: UILabel!
    
    @IBOutlet weak var middlePurchaseView: UIView!
    @IBOutlet weak var midlePMonthsCount: UILabel!
    @IBOutlet weak var midlePmonthLabel: UILabel!
    @IBOutlet weak var midlePAmount: UILabel!
    
    @IBOutlet weak var firstLabel: paddingLabel!
    @IBOutlet weak var secondLabel: paddingLabel!
    @IBOutlet weak var noThanksLabel: UILabel!
    @IBOutlet weak var recurringBillLabel: UILabel!
    @IBOutlet weak var iAPBillingTerms: UITextView!
    
    @IBOutlet weak var firstPurchasePerMonthAmt: UILabel!
    @IBOutlet weak var secondMonthlyAmt: UILabel!
    @IBOutlet weak var thirdMonthlyAmt: UILabel!
    
    
    var datumFeatures = DatupPlusCollection.screens
    var plans = [PurchasePlanModel]()
    let purchaseVM = AppStoreKitHelper()
    var purchasePlan = [InAppPurchaseModel]()
    var currentPlan = 0
    let getDatumPlusVm = GetDatumPlusVM()
    var isPopUpForLikes = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    /// method to load InAppPurchaseView xib.
    func loadInAppPurchaseView() {
        // Helper.showProgressIndicator(withMessage: StringConstants.Loading)
        // DeactivteAccountReasons name of the XIB.
        Bundle.main.loadNibNamed("InAppPuchasePopupView", owner:self, options:nil)
        self.mainView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        
        self.mainView.frame = self.bounds
        self.addSubview(self.mainView)
        self.mainView.layoutIfNeeded()
        self.pageControll.numberOfPages = datumFeatures.count
        firstLabel.backgroundColor = Colors.AppBaseColor
        secondLabel.backgroundColor = Colors.AppBaseColor
        noThanksLabel.text = StringConstants.noThanks()
        self.subscribeButton.setTitle(StringConstants.subscribe(), for: .normal)
        dPlusCollectionView.reloadData()
        dPlusCollectionView.layoutIfNeeded()
        
        if Helper.isSubscribedForPlans() {
            self.subscribeButton.isEnabled = false
        }else {
            self.subscribeButton.isEnabled = true
        }
        self.firstLabel.text = StringConstants.bestValue()
        self.secondLabel.text = StringConstants.mostPopular()
        var frameForAnimation = self.subView.frame
        frameForAnimation.origin.y = -900
        self.subView.frame = frameForAnimation
        self.subView.layoutIfNeeded()
        didGetPlanResponse()
        didGetResponse()
        updatePurchasePlan()
        UIView.animate(withDuration:0.3, delay:0.0, usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 0.7, options: [], animations:
            {
                frameForAnimation.origin.y = 0
                self.subView.frame = frameForAnimation
        }, completion:{ (finished) in
            if(finished) {
                
            }
        })
        
        self.firstLabel.drawText(in:self.firstLabel.frame)
        self.secondLabel.drawText(in:self.secondLabel.frame)
        pageControll.currentPageIndicatorTintColor = Colors.AppBaseColor
        purchaseVM.clearIncompleteTansact()
        
        if(!isPopUpForLikes) {
            startTimer()
        } else {
         //   addTimerForOutOfLikes()
        }
        
        if Helper.isSubscribedForPlans() {
            //highlight the current active plan if it is activated.
            
            let purchasedID = Helper.getPurchasedID()
            if(purchasedID == AppConstant.threeMonthSubID) {
                highlightMiddleView()
            } else if(purchasedID == AppConstant.sixMonthSubID) {
                highlightFirsstView()
            } else if(purchasedID == AppConstant.oneYearSubID) {
                highlightThirdView()
            }else {
                highlightMiddleView()
            }
        } else {
            //no subscrption plan is activated.
            highlightMiddleView()
        }
    }
    
    func updatePurchasePlan()
    {
        
        self.purchasePlan = Helper.getPurchasePlans()
        if purchasePlan.count != 0 {
            
            firstPAmount.text = purchasePlan[0].currencySymbol+String(purchasePlan[0].price)
            firstPMonthsCount.text = String(purchasePlan[0].monthsCount)
            firstPmonthLabel.text = StringConstants.months()
           // let amt  = Double(purchasePlan[0].price)
            //firstPurchasePerMonthAmt.text = purchasePlan[0].currencySymbol+String(amt.rounded(toPlaces: 3)) + StringConstants.perMonth()
            
            
            midlePAmount.text = purchasePlan[1].currencySymbol+String(purchasePlan[1].price)
            midlePMonthsCount.text = String(purchasePlan[1].monthsCount)
            midlePmonthLabel.text = StringConstants.months()
           // let amtt = Double(purchasePlan[1].price/6)
          //  secondMonthlyAmt.text = purchasePlan[1].currencySymbol+String(amtt.rounded(toPlaces: 3)) + StringConstants.perMonth()
            
            lastPAmount.text = purchasePlan[2].currencySymbol+String(purchasePlan[2].price)
            lastPMonthsCount.text = String(purchasePlan[2].monthsCount)
            lastPmonthLabel.text = StringConstants.months()
            //let amtth = Double(purchasePlan[2].price/12)
          //  thirdMonthlyAmt.text = purchasePlan[2].currencySymbol+String(amtth.rounded(toPlaces: 3)) + StringConstants.perMonth()
        }
        
        
    }
    
    
    func highlightMiddleView() {
        currentPlan = 1
        
        //self.middlePurchaseView.layer.cornerRadius = 2
        self.middlePurchaseView.layer.borderWidth = 3
        self.middlePurchaseView.layer.borderColor = Colors.AppBaseColor.cgColor
        Helper.setShadow(sender: self.middlePurchaseView)
        
        self.lastPurchaseView.layer.borderWidth = 0
        self.firstPurchaseView.layer.borderWidth = 0
        
        self.firstLabel.isHidden = true
        self.secondLabel.isHidden = false
        
        
        midlePMonthsCount.textColor = Colors.AppBaseColor
        midlePmonthLabel.textColor = Colors.AppBaseColor
        midlePAmount.textColor = Colors.AppBaseColor
        secondMonthlyAmt.textColor = Colors.AppBaseColor
        
        
        firstPMonthsCount.textColor = Colors.PrimaryText
        firstPmonthLabel.textColor = Colors.PrimaryText
        firstPAmount.textColor = Colors.PrimaryText
        
        
        lastPMonthsCount.textColor = Colors.PrimaryText
        lastPmonthLabel.textColor = Colors.PrimaryText
        lastPAmount.textColor = Colors.PrimaryText
        
        
        addAnimationToLabel(label: self.secondLabel)
    }
    
    
    func highlightFirsstView() {
        
        currentPlan = 0
        //self.firstPurchaseView.layer.cornerRadius = 2
        self.firstPurchaseView.layer.borderWidth = 3
        self.firstPurchaseView.layer.borderColor = Colors.AppBaseColor.cgColor
        Helper.setShadow(sender: self.firstPurchaseView)
        
        self.middlePurchaseView.layer.borderWidth = 0
        self.lastPurchaseView.layer.borderWidth = 0
        
        self.firstLabel.isHidden = false
        self.secondLabel.isHidden = true
        
        firstPMonthsCount.textColor = Colors.AppBaseColor
        firstPmonthLabel.textColor = Colors.AppBaseColor
        firstPAmount.textColor = Colors.AppBaseColor
        firstPurchasePerMonthAmt.textColor = Colors.AppBaseColor
        
        midlePMonthsCount.textColor = Colors.PrimaryText
        midlePmonthLabel.textColor = Colors.PrimaryText
        midlePAmount.textColor = Colors.PrimaryText
        
        lastPMonthsCount.textColor = Colors.PrimaryText
        lastPmonthLabel.textColor = Colors.PrimaryText
        lastPAmount.textColor = Colors.PrimaryText
        
        addAnimationToLabel(label: self.firstLabel)
        
    }
    
    func addAnimationToLabel(label:UILabel) {
        
        label.transform = CGAffineTransform(scaleX: 0.4, y:0.4)
        UIView.animate(withDuration:0.3, delay: 0.0, usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.5, options: [], animations:
            {
                label.transform = CGAffineTransform(scaleX: 1.0, y:1.0)
        }, completion:nil)
    }
    
    
    func highlightThirdView() {
        currentPlan = 2
        //self.lastPurchaseView.layer.cornerRadius = 2
        self.lastPurchaseView.layer.borderWidth = 3
        self.lastPurchaseView.layer.borderColor =  Colors.AppBaseColor.cgColor
        Helper.setShadow(sender: self.lastPurchaseView)
        
        self.middlePurchaseView.layer.borderWidth = 0
        self.firstPurchaseView.layer.borderWidth = 0
        
        self.firstLabel.isHidden = true
        self.secondLabel.isHidden = true
        
        firstPMonthsCount.textColor = Colors.PrimaryText
        firstPmonthLabel.textColor = Colors.PrimaryText
        firstPAmount.textColor = Colors.PrimaryText
        
        midlePMonthsCount.textColor = Colors.PrimaryText
        midlePmonthLabel.textColor = Colors.PrimaryText
        midlePAmount.textColor = Colors.PrimaryText
        
        lastPMonthsCount.textColor = Colors.AppBaseColor
        lastPmonthLabel.textColor = Colors.AppBaseColor
        lastPAmount.textColor = Colors.AppBaseColor
        thirdMonthlyAmt.textColor = Colors.AppBaseColor
    }
    
    
    func startTimer(){
        scrollTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollToNextCell), userInfo: nil, repeats: true)
    }
    
    func stopTimer(){
        self.timerForOutOflikes?.invalidate()
        self.scrollTimer?.invalidate()
    }
    
    @objc func scrollToNextCell(){
        let cellSize = CGSize(width: self.dPlusCollectionView.frame.size.width, height: self.dPlusCollectionView.frame.size.height)
        let contentOffset = self.dPlusCollectionView.contentOffset
        
        if self.dPlusCollectionView.contentSize.width <=  (self.dPlusCollectionView.contentOffset.x + cellSize.width) {
            self.dPlusCollectionView.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
        }
        else {
            self.dPlusCollectionView.scrollRectToVisible(CGRect(x:contentOffset.x + cellSize.width  , y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
        }
    }
    
    @IBAction func nothanksButtonAction(_ sender: Any) {
        self.removeViewWithAnimation()
    }
    
    @IBAction func continueButtonAction(_ sender: Any) {
        let tappedButton = sender as! UIButton
        tappedButton.smallZoomEffect()
        if purchasePlan.count != 0 {
            purchaseVM.purchase(productID: purchasePlan[currentPlan].productId)
        }
    }
    
    func removeViewWithAnimation() {
        var frameForAnimation = self.subView.frame
        frameForAnimation.origin.y = 0
        self.subView.frame = frameForAnimation
        self.subView.layoutIfNeeded()
        
        UIView.animate(withDuration:0.5, delay: 0.0, usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.5, options: [], animations:
            {
                frameForAnimation.origin.y = self.frame.size.height + 500
                self.subView.frame = frameForAnimation
        }, completion:{ (finished) in
            if(finished) {
                self.removeFromSuperview()
                self.stopTimer()
            }
        })
    }
    
    @IBAction func firstPurchaseAction(_ sender: Any) {
        if !Helper.isSubscribedForPlans() {
            highlightFirsstView()
        }
    }
    
    
    @IBAction func secondPurchaseAction(_ sender: Any) {
        if !Helper.isSubscribedForPlans() {
            highlightMiddleView()
        }
    }
    
    @IBAction func thirdPurchaseAction(_ sender: Any) {
        if !Helper.isSubscribedForPlans() {
            highlightThirdView()
        }
    }
    
}


extension InAppPuchasePopupView:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datumFeatures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: "InAppPuchaseCell", bundle: nil), forCellWithReuseIdentifier:"inappcell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "inappcell", for: indexPath) as! InAppPuchaseCell
        cell.updateCell(data: datumFeatures[indexPath.row])
        if(indexPath.row == 0) {
//            if(Helper.isUserOutOfLikes()) {
//                let  syncDate:Date = Date(timeIntervalSince1970: Double(Helper.newTimeForOutOfLikes()))
//                let diffrenceDate = syncDate.offsetFrom(date: Date())
//                cell.featureTitle.text = diffrenceDate
//                cell.featuredescrption.text = StringConstants.msgForOutOfLikes()
//            }
        }
        if(indexPath.row != 0) {
            cell.MainHeader.isHidden = true
        } else {
            cell.MainHeader.isHidden = false
        }
        return cell
    }
    
    @objc func updateTimer() {
        if(Helper.isUserOutOfLikes()) {
            if let cellForLikes = self.dPlusCollectionView.cellForItem(at:IndexPath(row: 0, section: 0)) as? InAppPuchaseCell {
                let  syncDate:Date = Date(timeIntervalSince1970: Double(Helper.newTimeForOutOfLikes()))
                let diffrenceDate = syncDate.offsetFrom(date: Date())
                cellForLikes.featureTitle.text = diffrenceDate
                cellForLikes.featuredescrption.text = StringConstants.msgForOutOfLikes()
            }
        } else {
            
        }
    }
    
    
    func addTimerForOutOfLikes() {
        if(Helper.isUserOutOfLikes()) {
            //adding timer for updating timer for outoflikes.
            timerForOutOflikes = Timer.scheduledTimer(timeInterval:1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height:  collectionView.frame.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.pageControll.currentPage = Int(pageNumber)
        
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.pageControll.currentPage = Int(pageNumber)
        
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.pageControll.currentPage = Int(pageNumber)
        
    }
    
}

extension InAppPuchasePopupView {
    
    
    func didGetResponse(){
        self.getDatumPlusVm.getDatumPlusVM_resp.subscribe(onNext: { success in
            
            switch success {
            case GetDatumPlusVM.respType.getPurchasePlan:
                self.plans = self.getDatumPlusVm.purchasePlan
                
                break
            case GetDatumPlusVM.respType.subscription:
                self.removeViewWithAnimation()
                //   Helper.showAlertWithMessageOnwindow(withTitle:ActionSheetButtonNames.Success, message:ActionSheetButtonNames.planPurchased)
                break
            case .getCoinPlan:
                break
                
            case .faild:
                self.removeViewWithAnimation()
                Helper.showAlertWithMessageOnwindow(withTitle:StringConstants.subscription(), message:StringConstants.subscriptionFailed())
                
            }
            
        }).disposed(by:getDatumPlusVm.disposeBag)
        
    }
    
    func didGetPlanResponse(){
        self.purchaseVM.purchasePlan_resp.subscribe(onNext:{ success in
            
            switch success {
                
            case AppStoreKitHelper.ResponseType.purchased:
                self.getDatumPlusVm.subscribePlan()
                break
            case AppStoreKitHelper.ResponseType.receivedPlans:
                self.updatePurchasePlan()
                break
            case .purchaseCoins:
                break
            }
            
            Helper.hideProgressIndicator()
        }).disposed(by: purchaseVM.disposeBag)
    }
}


extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
