//
//  UIGradientImageView.swift
//  Pickrg
//
//  Created by Dinesh Guptha Bavirisetti on 18/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class UIGradientImageView: UIImageView {
    
    let myGradientLayer: CAGradientLayer
    
    override init(frame: CGRect){
        myGradientLayer = CAGradientLayer()
        super.init(frame: frame)
        self.setup()
        addGradientLayer()
        
    }
    
    func addGradientLayer(){
        self.backgroundColor = UIColor.clear
        if myGradientLayer.superlayer == nil{
            myGradientLayer.backgroundColor = UIColor.clear.cgColor
            self.layer.addSublayer(myGradientLayer)
        }
    }
    
    required init(coder aDecoder: NSCoder){
        myGradientLayer = CAGradientLayer()
        super.init(coder: aDecoder)!
        self.setup()
        addGradientLayer()
    }
    
    func getColors() -> [CGColor] {
        return [UIColor(red: 0, green:0, blue: 0 ,alpha: 0.3).cgColor,UIColor(red: 0, green:0, blue: 0 ,alpha: 0.3).cgColor,UIColor.clear.cgColor,UIColor.clear.cgColor,UIColor(red: 0, green:0, blue: 0 ,alpha: 0.3).cgColor,UIColor(red: 0, green:0, blue: 0 ,alpha: 0.3).cgColor]
    }
    
    
    func getLocations() -> [CGFloat]{
        return [0,0.1,0.20,0.8,0.9,1]
        
    }
    
    func setup() {
        myGradientLayer.startPoint = CGPoint(x: 0.6, y: 0)
        myGradientLayer.endPoint = CGPoint(x: 0.6, y: 1)
        
        let colors = getColors()
        myGradientLayer.colors = colors
        myGradientLayer.isOpaque = false
        myGradientLayer.locations = getLocations() as [NSNumber]?
    }
    
    
    func gradientForMatch() {
        let colors:[CGColor] =  [UIColor(red: 0, green:0, blue: 0 ,alpha: 0.8).cgColor,UIColor(red: 0, green:0, blue: 0 ,alpha: 0.8).cgColor,UIColor.clear.cgColor,Helper.getUIColor(color: "2AAEFA").withAlphaComponent(0.8).cgColor,Helper.getUIColor(color: "2AAEFA").withAlphaComponent(0.8).cgColor]
        myGradientLayer.startPoint = CGPoint(x: 0.6, y: 0)
        myGradientLayer.endPoint = CGPoint(x: 0.6, y: 1)
        myGradientLayer.colors = getColors()
        myGradientLayer.isOpaque = false
        myGradientLayer.locations = getLocations() as [NSNumber]?
        //  [UIColor(red: 0, green:0, blue: 0 ,alpha: 0.3).cgColor,UIColor.clear.cgColor,UIColor.clear.cgColor,UIColor(red: 0, green:0, blue: 0 ,alpha: 0.3).cgColor]
    }
    
    func setTopGradient(){
        
        myGradientLayer.startPoint = CGPoint(x: 0.6, y: 0)
        myGradientLayer.endPoint = CGPoint(x: 0.6, y: 1)
        myGradientLayer.colors = [UIColor(red: 0, green:0, blue: 0 ,alpha: 0.8).cgColor,UIColor.clear.cgColor,UIColor.clear.cgColor,UIColor.clear.cgColor]
        myGradientLayer.isOpaque = false
        myGradientLayer.locations = getLocations() as [NSNumber]?
        
    }
    
    func getAppBaseColor() -> [CGColor]{
        return [Colors.AppBaseColor.cgColor,
                Colors.AppBaseGradient.cgColor]
        
    }
    
    func setDiagonalGradient(){
        myGradientLayer.startPoint = CGPoint(x: 1, y: 1)
        myGradientLayer.endPoint = CGPoint(x: 0, y: 0)
        let colors = getAppBaseColor()
        myGradientLayer.colors = colors
        myGradientLayer.isOpaque = false
        myGradientLayer.locations =  [0.4,  0.9]
    }
    
    func disabledColor(){
        myGradientLayer.startPoint = CGPoint(x: 1, y: 1)
        myGradientLayer.endPoint = CGPoint(x: 0, y: 0)
        let colors = [Colors.AppBaseColor.cgColor,
                      Colors.AppBaseGradient.cgColor]
        myGradientLayer.colors = colors
        myGradientLayer.isOpaque = false
        myGradientLayer.locations =  [0.4,  0.9]
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        myGradientLayer.frame = self.layer.bounds
    }
    
}
