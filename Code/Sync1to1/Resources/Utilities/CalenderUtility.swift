//
//  CalenderUtility.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 19/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


/// Used for getting all the nessecary details about that perticular month or year, which you want to show in your calender.
class CalenderUtility {
    
    struct Constants {
        static let timeFormatInYMDay = "yyyy-MM-dd" //example :- 2017-12-11
        static let timeInDayFormat = "EEEE" //example :- MONDAY
        static let timeInMonthAndYearFormat = "MMMM yyyy" //example :- January 2017
        static let timeInMonthDayAndYearFormat = "MMMM dd, yyyy" //example :- January 22, 2017
        static let timeInAMPMFormat = "hh:mm a" //example :- 10:30 AM
        static let timeInDatFormatWithThreeLetters = "EEE" //example :- MON
        static let currentTimeZone = "GMT" // you can change it to whatever you wants like IST, GMT etc
    }
    
    static let calendar = Calendar.current
    
    /// Used for getting number of days depends on the year and month
    ///
    /// - Parameters:
    ///   - month: month you want to know the number of days
    ///   - year: year of the month
    /// - Returns: number of days depend upon the month and year you passed
    
    class func getNumberOfDays(inMonth month:Int,andYear year:Int) -> Int{
        let calender = Calendar.current
        var components = DateComponents.init()
        
        components.year = year
        components.month = month
        
        let date = calender.date(from: components)
        let range:Range = calender.range(of: .day, in: .month, for: date!)!
        
        return range.upperBound-1
    }
    
    class func getDateWithoutTime(fromDate date:Date) -> Date{
        let dateFormat = DateFormatter()
        dateFormat.timeZone = TimeZone(abbreviation: Constants.currentTimeZone)
        dateFormat.dateFormat = Constants.timeFormatInYMDay
        let thisDateStr = dateFormat.string(from: date)
        let thisDate = dateFormat.date(from: thisDateStr)
        return thisDate!
        
    }
    
    /// This method is used for getting the current day in string format by taing the reference of date month and year.
    ///
    ///It will check all the conditions on the behalf of the date month and year, and only return you the day which you can use wherever you want.
    /// - Parameters:
    ///   - date: current Date for which you want the day.
    ///   - month: month is related to the day you passed.
    ///   - year: year is related to the day you passed.
    /// - Returns: A string format value for that perticluar day.
    
    class func getCurrentDay(forDate date:Int, inMonth month:Int, andYear year: Int) -> String?
    {
        let dateFormat = DateFormatter()
        dateFormat.timeZone = TimeZone(abbreviation: Constants.currentTimeZone)
        dateFormat.dateFormat = Constants.timeFormatInYMDay
        let thisDate = dateFormat.date(from:"\(year)-\(month)-\(date)")
        dateFormat.dateFormat = Constants.timeInDayFormat
        let name = dateFormat.string(from: thisDate!)
        return name
    }
    
    ///Used for gives you the date in the format of Date() class object.
    ///
    /// - Parameters:
    ///   - date: Date in Int format
    ///   - month: Month in Int format
    ///   - year: Year in Int format
    /// - Returns: It returns a Date object.
    
    class func getCurrentDate(forDate date:Int, inMonth month:Int, andYear year: Int) -> Date?{
        let dateFormat = DateFormatter()
        dateFormat.timeZone = TimeZone(abbreviation: Constants.currentTimeZone)
        dateFormat.dateFormat = Constants.timeFormatInYMDay
        let thisDate = dateFormat.date(from:"\(year)-\(month)-\(date)")
        return thisDate
    }
    
    
    
    /// Used for get current month with the year from given date
    ///
    /// - Parameter date: date from where you will get month and year
    /// - Returns: String format of month and year (in MMMM yyyy)
    class func getCurrentMonthWithYear(fromDate date:Date) -> String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = Constants.timeInMonthAndYearFormat
        let dateWithMonthAndYear = dateFormat.string(from: date)
        return dateWithMonthAndYear.uppercased()
    }
    
    
    
    /// Used for get current day and month with the year from given date
    ///
    /// - Parameter date: date from where you will get month and year
    /// - Returns: String format of month and year (in MMMM dd, yyyy)
    class func getCurrentMonthAndDayWithYear(fromDate date:Date) -> String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = Constants.timeInMonthDayAndYearFormat
        let dateWithMonthAndYear = dateFormat.string(from: date)
        return dateWithMonthAndYear.uppercased()
    }
    
    
    
    /// Will return the current year in respect of the date passed
    ///
    /// - Parameter date: year from where you want the yeaR
    /// - Returns: year in the format of String (in yyyy format)
    class func getCurrentYear(fromDate date:Date) -> Int{
        let year = calendar.component(.year, from: date)
        return year
    }
    
    /// Will return the current month in respect of the date passed
    ///
    /// - Parameter date: year from where you want the month
    /// - Returns: year in the format of String (in mm format)
    class func getCurrentMonth(fromDate date:Date) -> Int
    {
        let month = calendar.component(.month, from: date)
        return month
    }
    
    /// Will return the current date in respect of the date passed
    ///
    /// - Parameter date: year from where you want the date
    /// - Returns: year in the format of String (in dd format)
    class func getCurrentDateNumber(fromDate date:Date) -> Int{
        let day = calendar.component(.day, from: date)
        return day
    }
    
    
    
    /// This method will get current time from the day you are passing
    ///
    /// - Parameter date: date you want the time
    /// - Returns: it will return you a date in the format of "hh:mm a"
    ///
    /// for example :- 10:30 AM
    
    class func getCurrentTime(fromDate date : Date) -> String
    {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = Constants.timeInAMPMFormat
        let dateWithMonthAndYear = dateFormat.string(from: date)
        return dateWithMonthAndYear.uppercased()
    }
    
    
    /// This method will get current Day from the date you are passing
    ///
    /// - Parameter date: date you want the Day.
    /// - Returns: it will return you a date in the format of "EEE"
    ///
    /// for example :- MON
    class func getCurrentDay(fromDate date:Date) -> String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = Constants.timeInDatFormatWithThreeLetters
        let dateWithMonthAndYear = dateFormat.string(from: date)
        return dateWithMonthAndYear.uppercased()
    }
}
