//
//  Extensions.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 09/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class Extensions: NSObject {
    
}

extension String
{
    /// Encode to Base64
    /// - Returns: Returns Encoded String
    func encodeToBase64() -> String {
        
        var res:String? = ""
        let utf = self.data(using: String.Encoding.utf8)
        res = utf?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue:0))
        return res!
    }
}

extension UIView
{
    func  showErrorMessage(titleMsg: String, andMessage: String){
        
        let alertController = UIAlertController(title: titleMsg, message: andMessage, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        let appDelegateObj = UIApplication.shared.delegate! as! AppDelegate
        // show the alert
        var controller = appDelegateObj.window!.rootViewController
        if (appDelegateObj.window!.rootViewController as? UINavigationController) != nil
        {
            let navControllr = appDelegateObj.window!.rootViewController as! UINavigationController
            controller = navControllr.visibleViewController
        }
        else
        {
            controller = appDelegateObj.window!.rootViewController
        }
        controller?.present(alertController, animated: true, completion: nil)
    }
}

extension UIColor{
    
    convenience init(hex: Int, alpha: Double = 1.0) {
        self.init(red: CGFloat((hex>>16)&0xFF)/255.0, green:CGFloat((hex>>8)&0xFF)/255.0, blue: CGFloat((hex)&0xFF)/255.0, alpha:  CGFloat(255 * alpha) / 255)
    }
    
}

extension UIViewController
{
    func addToken(token : String,userId : String)
    {
        UserDefaults.standard.setValue(token, forKey: SyUserdefaultKeys.userToken)
        UserDefaults.standard.setValue(userId, forKey: SyUserdefaultKeys.userID)
        UserDefaults.standard.synchronize()
    }
}
