//
//  LNHELPER.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 09/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import NotificationCenter
import UserNotifications
import EventKit

class LNHELPER: NSObject {
    
    struct typeOfLocaNotification {
        static let notificationFor24Hrs = "1"
        static let notificationFor1Hr  = "2"
        static let notificationFor5minutes = "3"
    }
    
    //creating 3 different types of local notifications.
    //24 hours before date shedule.
    //1 hour before date shedule and
    //5 minutes before date shedule.
    func craateNotificationForDateReminder(dateDetails:SyncDate) {
        
        //getting date from unix time stamp(epoch value) and creating unique identifier for every notification.
        let dateSheduledTime = Date(timeIntervalSince1970: dateDetails.dateTime)
        let idForNotification = "date_id_"+dateDetails.dateId+String(describing: dateSheduledTime)
        
        //removing 24hours from sheduled Date and sheduling date.
        var modifyedDate = removeHoursFromDate(hoursToRemove:-24,fromDate:dateSheduledTime)
        addLocalNotification(sheduledTime:modifyedDate,uniqueIdForNotification: idForNotification+"/24hours",dateDetails:dateDetails,typeOfThePush:typeOfLocaNotification.notificationFor24Hrs)
        
        //removing 1hours from sheduled Date and sheduling date.
        modifyedDate = removeHoursFromDate(hoursToRemove:-1,fromDate:dateSheduledTime)
        addLocalNotification(sheduledTime:modifyedDate,uniqueIdForNotification: idForNotification+"/1hour",dateDetails:dateDetails,typeOfThePush:typeOfLocaNotification.notificationFor1Hr)
        
        //removing 5 minutes from sheduled Date and sheduling date.
        modifyedDate = removeMinutesFromDate(minutesToRemove:-5,fromDate:dateSheduledTime)
        addLocalNotification(sheduledTime:modifyedDate,uniqueIdForNotification: idForNotification+"/5minutes", dateDetails:dateDetails,typeOfThePush:typeOfLocaNotification.notificationFor5minutes)
        

    }
    

    func cancelNotificationIfAlreadyExist(uniqId: String){
        UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
            for request in requests {
                if request.identifier == uniqId {
                    UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["identifier"])
                }
            }
        }
    }
    
    
    //creating local notification for date Reminder.
    func craateNotificationForPendingDateReminder(dateDetails:SyncDate) {
        //getting date from unix time stamp(epoch value) and creating unique identifier for every notification.
        let dateSheduledTime = Date(timeIntervalSince1970: dateDetails.dateTime)
        let idForNotification = "date_id_"+dateDetails.dateId+String(describing: dateSheduledTime)
        
        //removing 2hours from sheduled Date and sheduling date.
        let modifyedDate = removeHoursFromDate(hoursToRemove:-2,fromDate:dateSheduledTime)
        addLocalNotificationForPendingReminder(sheduledTime:modifyedDate,uniqueIdForNotification: idForNotification+"/1hour",dateDetails:dateDetails)
    }
    
    //method to remove X hours from date object.
    func removeHoursFromDate(hoursToRemove:Int,fromDate:Date) -> Date{
        let calendar = NSCalendar.autoupdatingCurrent
        return  calendar.date(byAdding:.hour, value:hoursToRemove, to: fromDate)!
    }
    
    //method to remove X minutes from date object.
    func removeMinutesFromDate(minutesToRemove:Int,fromDate:Date) -> Date{
        let calendar = NSCalendar.autoupdatingCurrent
        return  calendar.date(byAdding:.minute, value:minutesToRemove, to: fromDate)!
    }
    
    //time formatter for notification.
    func getTimeForDate(timeForDate:Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        formatter.amSymbol = "am"
        formatter.pmSymbol = "pm"
        let dateString = formatter.string(from:timeForDate)
        return dateString // "4:44 pm
    }
    
    // method for triggering notification.
    func addLocalNotification(sheduledTime:Date,uniqueIdForNotification:String,dateDetails:SyncDate,typeOfThePush:String) {
        let content = UNMutableNotificationContent()
        
        let dateSheduledTime = Date(timeIntervalSince1970: dateDetails.dateTime)
        let timeString = getTimeForDate(timeForDate: dateSheduledTime)
        self.cancelNotificationIfAlreadyExist(uniqId: uniqueIdForNotification)
        
        var bodyMessage = ""
        
        if(typeOfThePush == typeOfLocaNotification.notificationFor24Hrs) {
            bodyMessage = "This is a reminder that you and \(dateDetails.nameOfTheProfile) have a date scheduled tomorrow at \(timeString)."
        } else if(typeOfThePush == typeOfLocaNotification.notificationFor1Hr) {
            bodyMessage = "This is a reminder that you and \(dateDetails.nameOfTheProfile) have a date scheduled in 1 hour at \(timeString)."
        } else {
            bodyMessage = "This is a reminder that you and \(dateDetails.nameOfTheProfile) have a date scheduled in 5 minutes at \(timeString)."
        }
        
        content.body = bodyMessage
        content.categoryIdentifier = "remnder notification"
        content.userInfo = ["type":"20"]
        let interval = sheduledTime.timeIntervalSinceNow
        if(interval > 0) {
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: interval, repeats: false)
            let request = UNNotificationRequest.init(identifier: uniqueIdForNotification, content: content, trigger: trigger)
            let center = UNUserNotificationCenter.current()
            center.add(request, withCompletionHandler: { (error) in
            })
        }
    }
    
    func addLocalNotificationForPendingReminder(sheduledTime:Date,uniqueIdForNotification:String,dateDetails:SyncDate) {
        let content = UNMutableNotificationContent()
        let dateSheduledTime = Date(timeIntervalSince1970: dateDetails.dateTime)
        
        var bodyMessage = ""
        bodyMessage = "Please confirm, reschedule or reject the date  request with \(dateDetails.nameOfTheProfile) before it expires."
        
        content.body = bodyMessage
        content.categoryIdentifier = "remnder notification"
        content.userInfo = ["type":"20"]
        let interval = sheduledTime.timeIntervalSinceNow
        if(interval > 0) {
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: interval, repeats: false)
            let request = UNNotificationRequest.init(identifier: uniqueIdForNotification, content: content, trigger: trigger)
            let center = UNUserNotificationCenter.current()
            center.add(request, withCompletionHandler: { (error) in
            })
        }
        
    }
    
    
    /// method to verify sheduledNotification
    ///
    /// - Parameter identifierName: name of the sheduled notification.
    /// - Returns:
    func isNotificationSheduledAlready(identifierName:String) -> Bool {
        var isNotificationSheduledAlready = false
        
        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (requests) in
            for request in requests {
                if request.identifier == identifierName {
                    //Notification already exists. Do stuff.
                    isNotificationSheduledAlready =  true
                } else if request === requests.last {
                    //All requests have already been checked and notification with identifier wasn't found. Do stuff.
                    isNotificationSheduledAlready =  false
                }
            }
        })
        
        return isNotificationSheduledAlready
    }
    
    func removeAllSheduledNotifications() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    
    func removePendingNotifications() {
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
}



/// creating calender events and reminders
class CalenderHelper:NSObject {
    
    
    func createCalenderEvent(profile:SyncDate)  {
        let eventStore : EKEventStore = EKEventStore()
        // 'EKEntityTypeReminder' or 'EKEntityTypeEvent'
        
        self.deleteEvents()
        eventStore.requestAccess(to: .event, completion: {
            granted, error in
            if (granted) && (error == nil) {
                print("granted \(granted)")
                print("error  \(String(describing: error))")
                let dateOn:Date = Date(timeIntervalSince1970: Double(profile.dateTime))
                let event:EKEvent = EKEvent(eventStore: eventStore)
                event.title = StringConstants.titleForCalenderEvent()
                event.startDate = dateOn
                event.endDate = dateOn
                event.notes = profile.requestedFor + " with " + profile.nameOfTheProfile + " on Pairzy"
                //event.notes = "you have " + profile.requestedFor + " with " + profile.nameOfTheProfile + " on Pairzy"
                event.calendar = eventStore.defaultCalendarForNewEvents
                
                do {
                    try eventStore.save(event, span:.thisEvent)
                } catch let error {
                    print("Reminder failed with error \(error.localizedDescription)")
                }
                
                print("Saved Event")
            }
        })
        
        
        // This lists every reminder
        let predicate = eventStore.predicateForReminders(in: [])
        eventStore.fetchReminders(matching: predicate) { reminders in
            for reminder in reminders! {
                print(reminder.title)
            }}
        
    }
    
    
    func deleteEvents(){
        
        let eventStore : EKEventStore = EKEventStore()
        // What about Calendar entries?
        let startDate = Date().addingTimeInterval(-60*60*24)
        let endDate = Date().addingTimeInterval(60*60*24*3)
        let predicate2 = eventStore.predicateForEvents(withStart: startDate, end: endDate, calendars: nil)
        
        print("startDate:\(startDate) endDate:\(endDate)")
        let eV = eventStore.events(matching: predicate2) as [EKEvent]
        
        if eV != nil {
            for i in eV {
                print("Title  \(String(describing: i.title))" )
                print("stareDate: \(i.startDate ?? Date())" )
                print("endDate: \(String(describing: i.endDate))" )
                
                if i.title == StringConstants.titleForCalenderEvent() {
                    do{
                        (try eventStore.remove(i, span: EKSpan.thisEvent, commit: true))
                    }
                    catch let error {
                        print(error)
                    }
                    
                    
                }
            }
            
        }
        
    }
    
    
    /// Fetch all events from selected Titles calendar.
    /// - parameter calendarTitle: String value of calendar title
    func fetchEventsFromCalendar(calendarTitle: String) -> Void {
        let eventStore : EKEventStore = EKEventStore()
        let calendars = eventStore.calendars(for: .event)
        //calendars.removeAll()
        
        for calendar:EKCalendar in calendars {
            
            if calendar.title == calendarTitle {
                let selectedCalendar = calendar
                
                let startDate = Date()
                let endDate = Date(timeIntervalSinceNow: 60*60*24*30)
                let predicate = eventStore.predicateForEvents(withStart: startDate as Date, end: endDate as Date, calendars: [selectedCalendar])
                let events = eventStore.events(matching: predicate) as [EKEvent]
                
                print("Events: \(events)")
                for event in events {
                    print("Event Title : \(String(describing: event.title)) Event ID: \(String(describing: event.eventIdentifier))")
                }
                
            }
        }
    }
}


//When the user matches with someone. type == 1
//
//When the user likes someone.type == 2
//
//When the user receives a video message.type == 3
//
//When the user views a video message. type == 4
//
//When the user receives a video call (sync date). type == 5
//
//When the user misses a video call (sync date). type == 6
//
//When the user receives a sync date request. type == 7
//
//When the user reschedules a sync date. type == 8
//
//When the user confirms a sync date. type == 9
//
//When the user rejects a sync date.10




