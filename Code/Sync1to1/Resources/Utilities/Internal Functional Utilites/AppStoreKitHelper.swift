//
//  AppStoreKitHelper.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 20/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//


import StoreKit
import RxSwift
import RxCocoa

class AppStoreKitHelper: NSObject {
    
    var products: [String: SKProduct] = AppConstant.products
    let purchasePlan_resp = PublishSubject<ResponseType>()
    let disposeBag = DisposeBag()
    
    
    enum ResponseType:Int {
        case purchased = 0
        case receivedPlans = 1
        case purchaseCoins = 2
    }
    
    
    func fetchProducts() {
        if (SKPaymentQueue.canMakePayments()) {
            let productIDs = Set([AppConstant.coinsSubId750,AppConstant.coinsSubId1500,AppConstant.coinsSubId2000,AppConstant.threeMonthSubID,AppConstant.sixMonthSubID,AppConstant.oneYearSubID])
            let productsRequest = SKProductsRequest(productIdentifiers: productIDs)
            productsRequest.delegate = self
            productsRequest.start()
            print("Fetching Products")
        } else {
            //we cant make purchases
            print("Cant make purchase")
        }
    }
    
    func clearIncompleteTansact(){
       // SKPaymentQueue.default().add(self)
    }
    
    func removeObserver(){
        SKPaymentQueue.default().remove(self)
    }
    
}

extension AppStoreKitHelper: SKProductsRequestDelegate,SKPaymentTransactionObserver {
    
    
    func purchase(productID: String) {
        if let product = products[productID] {
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(payment)
            SKPaymentQueue.default().add(self)
        }
    }
    
    
    func restorePurchases() {
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    func productsRequest (_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        products = [:]
        var purchaseDict = [[String:Any]]()
        
        let count : Int = response.products.count
        if (count>0) {
            
            let allProducts = response.products
            for validProduct in allProducts {
                print(validProduct.localizedTitle)
                print(validProduct.localizedDescription)
                print(validProduct.price)
                products[validProduct.productIdentifier] = validProduct
                AppConstant.products[validProduct.productIdentifier] = validProduct
                
                let planDict = ["productId" : validProduct.productIdentifier,
                                "productTitle":validProduct.localizedTitle,
                                "productDescript":validProduct.localizedDescription,
                                "price": validProduct.price,
                                "currencyCode": validProduct.priceLocale.currencyCode! ,
                                "currencySymbol": validProduct.priceLocale.currencySymbol!] as [String : Any]
                purchaseDict.append(planDict)
            }
            
            Helper.savePurchasePlans(data: purchaseDict)
            self.purchasePlan_resp.onNext(.receivedPlans)
            
        } else {
            Helper.hideProgressIndicator()
            print("in valid product")
        }
    }
    
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Error Fetching product information");
        Helper.hideProgressIndicator()
    }
    
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        print("paymentQueueRestoreCompletedTransactionsFinished")
        
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        Helper.hideProgressIndicator()
        print("restoreCompletedTransactionsFailedWithError")
    }
    
    func requestDidFinish(_ request: SKRequest) {
        print("finished")
        Helper.hideProgressIndicator()
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, removedTransactions transactions: [SKPaymentTransaction]) {
        print("removedTransactions")
        
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedDownloads downloads: [SKDownload]) {
        print("updatedDownloads")
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, shouldAddStorePayment payment: SKPayment, for product: SKProduct) -> Bool {
        return true
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        Helper.hideProgressIndicator()
        
        for transaction:AnyObject in transactions {
            
            
            if let trans = transaction as? SKPaymentTransaction {
                
                
                switch trans.transactionState {
                    
                case .purchased:
                    
                    let subscriptionProductIDs = Set([AppConstant.threeMonthSubID,AppConstant.sixMonthSubID,AppConstant.oneYearSubID])
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    if subscriptionProductIDs.contains(transactions[0].payment.productIdentifier) {
                        //user purchased subscrption plan.
                        
                        ///save only if subscription API is success
                        
                        AppConstant.transactionId = transactions[0].transactionIdentifier!
                        AppConstant.purchsedProductId = transactions[0].payment.productIdentifier
                        let purchaseDetails = ["productId": transactions[0].payment.productIdentifier,
                                               "transactionId": transactions[0].transactionIdentifier!,
                                               "transationDate": transactions[0].transactionDate!] as [String : Any]
                        
                        Helper.savePurchasedPlan(data: purchaseDetails)
                        self.purchasePlan_resp.onNext(.purchased)
                        
                    } else {
                        SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                        // user purchased coins.
                        self.purchasePlan_resp.onNext(.purchaseCoins)
                    }
                    
                    
                    break
                    
                    
                case .failed: //failed to purchase.
                    UserDefaults.standard.set(false, forKey: "purchased")
                    UserDefaults.standard.synchronize()
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                case .restored:             //  print("Already Purchased");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                case .deferred: //purchase  is in pending.
                    print("deferred")
                    break
                    
                case .purchasing:
                    print("purchasing-- Transaction is being added to the server queue")
                default: break
                    
                }}}
    }
    
    
    func receiptValidation() {
        let SUBSCRIPTION_SECRET = "9528e075268d4f528e3178415847bc2d"
        let receiptPath = Bundle.main.appStoreReceiptURL?.path
        if FileManager.default.fileExists(atPath: receiptPath!){
            var receiptData:NSData?
            do{
                receiptData = try NSData(contentsOf: Bundle.main.appStoreReceiptURL!, options: NSData.ReadingOptions.alwaysMapped)
            }
            catch{
                print("ERROR: " + error.localizedDescription)
            }
            //let receiptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            let base64encodedReceipt = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithCarriageReturn)
            
            print(base64encodedReceipt!)
            // https://buy.itunes.apple.com/verifyReceipt
            
            let requestDictionary = ["receipt-data":base64encodedReceipt!,"password":SUBSCRIPTION_SECRET]
            
            guard JSONSerialization.isValidJSONObject(requestDictionary) else {  print("requestDictionary is not valid JSON");  return }
            do {
                let requestData = try JSONSerialization.data(withJSONObject: requestDictionary)
                let validationURLString = "https://buy.itunes.apple.com/verifyReceipt"  // this works but as noted above it's best to use your own trusted server
                guard let validationURL = URL(string: validationURLString) else { print("the validation url could not be created, unlikely error"); return }
                let session = URLSession(configuration: URLSessionConfiguration.default)
                var request = URLRequest(url: validationURL)
                request.httpMethod = "POST"
                request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
                let task = session.uploadTask(with: request, from: requestData) { (data, response, error) in
                    if let data = data , error == nil {
                        do {
                            let appReceiptJSON = try JSONSerialization.jsonObject(with: data)
                            print("success. here is the json representation of the app receipt: \(appReceiptJSON)")
                            
                            
                            let receiptDta = appReceiptJSON as! [String:Any]
                            
                            if let latestSubscription = receiptDta["latest_receipt_info"] as? [[String:Any]] {
                                print(latestSubscription)
                                Helper.saveReceiptData(data:latestSubscription)
                            }
                            
                            // if you are using your server this will be a json representation of whatever your server provided
                        } catch let error as NSError {
                            print("json serialization failed with error: \(error)")
                        }
                    } else {
                        print("the upload task returned an error: \(String(describing: error))")
                    }
                }
                task.resume()
            } catch let error as NSError {
                print("json serialization failed with error: \(error)")
            }
            
        }
    }
}
