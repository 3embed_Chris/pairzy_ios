//
//  SocialLogin.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 05/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FacebookCore
import FacebookLogin

protocol socialLoginDelegate: class {
    func didLogin(data:LocalAuthentication)
}

class SocialLogin: NSObject {
    
    var authentication = Authentication()
    let disposeBag = DisposeBag()
    
    var controllerCurrent:UIViewController? = nil
    weak var socialDelegate:socialLoginDelegate? = nil
    let socialLogin_resp = PublishSubject<Bool>()

    static var shareObjSocial: SocialLogin? = nil
    let joinUsVM = JoinUSVM()
    /// Create Shared Instance
    static var shared: SocialLogin {
        if shareObjSocial == nil {
            shareObjSocial = SocialLogin()
        }
        return shareObjSocial!
    }
    
    //Facebook login action
    func faceBookLoginAction(controller : UIViewController) {
        //Helper.showProgressIndicator(withMessage: StringConstants.Loading)
        controllerCurrent = controller as? LandingVC
        let fbHandler:FBLoginHandler = FBLoginHandler.sharedInstance()
        fbHandler.delegate = self
        fbHandler.login(withFacebook: controller)
    }
}

/*****************************************************************/
// MARK: - Facebook Login Delegate
/*****************************************************************/

extension SocialLogin : facebookLoginDelegate {
    
    func didFacebookUserLogin(withDetails userInfo:NSDictionary) {
        Helper.saveSocialProfile(data: userInfo as! [String : Any])
        let fbProfile = LocalAuthentication.init(data: userInfo as! [String : Any])
        checkFbIdIsRegisterd(data: fbProfile)
    }
    func checkFbIdIsRegisterd(data:LocalAuthentication){
        
        Helper.showProgressIndicator(withMessage: StringConstants.Login())
        let apiCall = Authentication()
        apiCall.requestIsRegisteredFBId(fbId: data.id )
        
        apiCall.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                let statuscode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode)!
                
                if statuscode == .Success {
                let requestData:RequestModel = RequestModel().faceBookLoginDetail(fbDetails: data)
                    self.facebookLoginAPI(data: requestData)
                }
                else {
                    if self.socialDelegate != nil {
                        self.socialDelegate?.didLogin(data: data)
                    }
                }
            }, onError: { (error) in
                Helper.hideProgressIndicator()
                Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
                
            }).disposed(by: disposeBag)
        
    }
    func storeToken(responseData:ResponseModel){
        
        if let dict = responseData.response["data"] as? [String:Any]{
            if let sessionToken = dict["token"] as? String{
                UserLoginDataHelper.saveUserToken(token:sessionToken)
            }
            //   Saving userId In User Default After Login Response Came
            if let userId = dict["_id"] as? String {
                UserLoginDataHelper.saveUserId(userId: userId)
            }
            if let userId = dict["userId"] as? String {
                UserLoginDataHelper.saveUserId(userId: userId)
            }
            let userData = User.init(userDataDictionary: responseData.response["data"] as? [String : Any] ?? [:])
                
            if let isUserPurchased = dict["subscription"] as? [Any] {
                if isUserPurchased.count > 0 {
                    if let purchaseDetails = isUserPurchased[0] as? [String:Any] {
                        if let isPurchased = purchaseDetails["subscriptionId"] as? String,isPurchased == "Free Plan" {
                            Helper.saveIsUserPurchased(flag: false)
                        } else {
                            Helper.saveIsUserPurchased(flag: true)
                        }
                        Helper.savePurchasedPlan(data:purchaseDetails)
                    }
                }
                //check is user passported or not
                if let isFeatureExist = dict["isPassportLocation"] as? Int {
                    if isFeatureExist == 0 {
                        Helper.storeIsFeatureExist(isExist: false)
                    }else {
                        Helper.storeIsFeatureExist(isExist: true)
                        var locAddr  = Location(data: [:])
                        if let loc = dict["location"] as? [String:Any] {
                            if let temp = loc["latitude"] as? Double {
                                locAddr.latitude = temp
                            }
                            if let temp = loc["longitude"] as? Double {
                                locAddr.longitude = temp
                                
                            }
                            if let temp = loc["address"] as? String {
                                locAddr.name = temp
                            }
                            if let temp = loc["address"] as? String {
                                locAddr.fullText = temp
                                locAddr.secondaryText = temp
                                locAddr.locationDescription = temp
                            }
                            Helper.saveFeaturLocation(data: locAddr)
                        }
                    }
                }
                
                print("data for subscrption")
            }
            MQTT.sharedInstance.disconnectMQTTConnection()
            
            //saving userdata in core data.
            Database.shared.addUserdataToDatabase(withUserdata:userData)
            //updating signupData in couchDb.
            let couchbaseObj = Couchbase.sharedInstance
            let usersDocVMObject = UsersDocumentViewModel(couchbase: couchbaseObj)
            usersDocVMObject.updateUserDoc(data: responseData.response["data"] as! [String : Any], withLoginType: "1")
            if let userId = dict["_id"] as? String {
                MQTT.sharedInstance.connect(withClientId: userId)
            }
        }
        
    }
    func didFailWithError(_ error: Error?) {
        Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error!.localizedDescription)
        Helper.hideProgressIndicator()
    }
    
    func didUserCancelLogin() {
        Helper.hideProgressIndicator()
    }

    /// calls facebook login Api
    ///
    /// - Parameter data: is of type RequestModel,required parameters for fb login
    func facebookLoginAPI(data: RequestModel){
        Helper.showProgressIndicator(withMessage: StringConstants.Login())
        let apiCall = Authentication()
        didGetPreferences()
        apiCall.requestForFaceBookLogin(userDetail: data)
        
        apiCall.subject_response
            .subscribe(onNext: {response in
                Helper.hideProgressIndicator()
                //  let statuscode = responseModel.statusCode
                let statuscode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode)!
                
                if statuscode == .Success {
                    self.storeToken(responseData: response)
                    Helper.makeTabbarAsRootVc(forVc: self.controllerCurrent!)
                }
                else if statuscode == .Created {
                    self.storeToken(responseData: response)
                  //  self.pushToJoinUs()
                    self.joinUsVM.getPreferences()
                }
                else {
                    Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: response.response["message"] as! String)
                    self.socialLogin_resp.onNext(false)
                }
                
                
            }, onError: { (error) in
                Helper.hideProgressIndicator()
                Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
                
            }).disposed(by: disposeBag)
    }
    
    
    
    func didGetPreferences(){
        
        joinUsVM.joinUSVM_resp.subscribe(onNext:{ success in
            
            // self.nextBtn.isUserInteractionEnabled = true
            self.joinUsVM.myPreferences = Helper.getPreference()
            if self.joinUsVM.myPreferences.count != 0 && self.joinUsVM.myPreferences[0].arrayOfSubPreference.count != 0{
                let prefTypeId = self.joinUsVM.myPreferences[0].arrayOfSubPreference[0].prefTypeId
                switch prefTypeId {
                case 1,2,10:
                    self.preferenceTableVC()
                case 5:
                    self.pushTotextFieldTypeVC()
                default:
                    print("")
                }
            }
        }).disposed(by: joinUsVM.disposeBag)
        
    }
    
    /// creates object of gende
    func preferenceTableVC() {
        let userBasics = Helper.getSBFromVC(vc: self.controllerCurrent!).instantiateViewController(withIdentifier:StoryBoardIdentifier.PreferenceTable) as! PreferencesTableVC
        userBasics.isFirstTime = true
        self.controllerCurrent!.navigationController?.pushViewController(userBasics, animated: true)
        
    }
    
    func pushTotextFieldTypeVC(){
        let userBasics = Helper.getSBFromVC(vc: self.controllerCurrent!).instantiateViewController(withIdentifier:StoryBoardIdentifier.TextFieldTypeVC) as! PreferencesTFVC
        userBasics.screenIndex = 0
        userBasics.isFirstTime = true
        self.controllerCurrent!.navigationController?.pushViewController(userBasics, animated: true)
    }
    
    func pushToJoinUs(){
        let userBasics = Helper.getSBFromVC(vc: self.controllerCurrent!).instantiateViewController(withIdentifier:StoryBoardIdentifier.joinUssgSI) as! joinUsMessageVC
        self.controllerCurrent?.navigationController?.pushViewController(userBasics, animated: true)
    
    }
}



