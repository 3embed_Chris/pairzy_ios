//
//  CoinsHistoryModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 04/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class CoinsHistoryModel: NSObject {
    
    var coinAmount = 0
    var coinClosingBalance = 0
    var coinOpeingBalance = 0
    var coinType = ""
    var cost = 0
    var currency = "N/A"
    var currencySymbol = "N/A"
    var paymentTxnId = "N/A"
    var paymentType = "N/A"
    var timestamp = 0
    var trigger = ""
    var txnId = ""
    var txnType = ""
    var userPurchaseTime = ""
    var txnTypeCode = 0         // 1 - credit, 2- debit, 3-purchase
    
    init(data: [String:Any]) {
        
        if let coinAmount = data[ApiResponseKeys.CoinAmount] as? Int {
            self.coinAmount = coinAmount
        }
        if let coinClosingBal = data[ApiResponseKeys.CoinClosingBalance] as? Int {
            self.coinClosingBalance = coinClosingBal
        }
        if let openingBal = data[ApiResponseKeys.CoinOpeingBalance] as? Int {
            self.coinOpeingBalance = openingBal
        }
        if let coinType = data[ApiResponseKeys.CoinType] as? String {
            self.coinType = coinType
        }
        if let cost = data[ApiResponseKeys.cost] as? Int {
            self.cost = cost
        }
        if let currency = data[ApiResponseKeys.currency] as? String {
            self.currency = currency
        }
        if let currencySymbol = data[ApiResponseKeys.currencySymbol] as? String {
            self.currencySymbol = currencySymbol
        }
        if let payTxnId = data[ServiceInfo.PaymentTxnId] as? String {
            self.paymentTxnId = payTxnId
        }
        if let payType = data[ServiceInfo.PaymentType] as? String{
            self.paymentType = payType
        }
        if let timeStamp = data[ApiResponseKeys.Timestamp] as? Int {
            self.timestamp = timeStamp
        }
        if let trigger = data[ServiceInfo.Trigger] as? String {
            self.trigger = trigger
        }
        if let txnId = data[ApiResponseKeys.TxnId] as? String{
            self.txnId = txnId
        }
        if let txnType = data[ApiResponseKeys.TxnType] as? String {
            self.txnType = txnType
        }
        if let userPurchaseTime = data[ApiResponseKeys.userPurchaseTime] as? String{
            self.userPurchaseTime = userPurchaseTime
        }
        if let txnTypeCode = data[ApiResponseKeys.TxnTypeCode] as? Int {
            self.txnTypeCode = txnTypeCode
        }
    }
}




//coinAmount = 3000;
//coinClosingBalance = 11150;
//coinOpeingBalance = 8150;
//coinType = Coin;
//cost = 500;
//currency = USD;
//currencySymbol = "<null>";
//paymentTxnId = 1234;
//paymentType = card;
//timestamp = 1528192995285;
//trigger = 1;
//txnId = "TXN-ID-5318138271";
//txnType = PURCHSE;
//txnTypeCode = 3;
//userPurchaseTime = "2018-06-05 10:03:03 +0000";



