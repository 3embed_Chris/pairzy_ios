//
//  SubscriptionModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 04/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class SubscriptionModel: NSObject {
    
    var durationInMonths = 0
    var expiryTime = Date()
    var hideAge = 0
    var hideDistance = 0
    var likeCount = 0
    var noAdds = 0
    var passport = 0
    var planId = ""
    var purchaseDate = Date()
    var purchaseTime = Date()
    var readreceipt = 0
    var recentVisitors = 0
    var rewindCount = 0
    var subscriptionId = ""
    var superLikeCount = 0
    var userPurchaseTime = ""
    var whoLikeMe = 0
    var whoSuperLikeMe = 0
    
    
    init(data: [String:Any]) {
        
        if let duration = data[ApiResponseKeys.durationInMonths] as? Int{
            self.durationInMonths = duration
        }
        
        if let expiryTime = data[ApiResponseKeys.ExpiryTime] as? String{
            //self.expiryTime = expiryTime
        }
        if let hideAge = data[ApiResponseKeys.hideAge] as? Int{
            self.hideAge = hideAge
        }
        if let hideDistance = data[ApiResponseKeys.hideDistance] as? Int{
            self.hideDistance = hideDistance
        }
        
        if let likeCount = data[ApiResponseKeys.likeCount] as? Int{
            self.likeCount = likeCount
        }
        if let noAdds = data[ApiResponseKeys.noAdds] as? Int{
            self.noAdds = noAdds
        }
        
        if let passport = data[ApiResponseKeys.passport] as? Int{
            self.passport = passport
        }
        
        if let planId = data[ApiResponseKeys.id] as? String{
          self.planId = planId
        }
      
        if let purchaseDate = data[ApiResponseKeys.purchaseDate] as? String{
           // self.purchaseDate = purchaseDate
        }
        
        if let purchaseTime = data[ApiResponseKeys.purchaseTime] as? String{
            //self.purchaseTime = purchaseTime
        }
        
        
        if let readreceipt = data[ApiResponseKeys.readreceipt] as? Int{
            self.readreceipt = readreceipt
        }
        if let recentVisitors = data[ApiResponseKeys.recentVisitors] as? Int{
            self.recentVisitors = recentVisitors
        }
        
        if let rewindCount = data[ApiResponseKeys.rewindCount] as? Int{
            self.rewindCount = rewindCount
        }
        
        if let subscriptionId = data[ApiResponseKeys.subscriptionId] as? String{
           self.subscriptionId = subscriptionId
        }
        
        if let superLikeCount = data[ApiResponseKeys.superLikeCount] as? Int{
            self.superLikeCount = superLikeCount
        }
        
        if let userPurchaseTime = data[ApiResponseKeys.userPurchaseTime] as? String{
            self.userPurchaseTime = userPurchaseTime
        }
   
        if let whoLikeMe = data[ApiResponseKeys.whoLikeMe] as? Int{
            self.whoLikeMe = whoLikeMe
        }
        
        if let whoSuperLikeMe = data[ApiResponseKeys.whoSuperLikeMe] as? Int{
            self.whoSuperLikeMe = whoSuperLikeMe
        }
    }
}
