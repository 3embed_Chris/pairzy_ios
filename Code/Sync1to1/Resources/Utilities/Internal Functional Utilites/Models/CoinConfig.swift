//
//  CoinConfig.swift
//  Datum
//
//  Created by 3 Embed on 04/09/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class CoinConfig: NSObject{
    var id = ""
    var coinsForBoost = 0
    var coinsForCallDate = 0
    var coinsForInPerson = 0
    var coinsForMsg = 0
    var coinsForReshedule = 0
    var coinsForRewind = 0
    var coinsForSuperLike = 0
    var coinsForVideoDate = 0
    
    init(data: [String:Any]){
        
        if let id = data["_id"] as? String {
            self.id = id
        }
        
        if let dict = data["boostProfileForADay"] as? [String:Any] {
            
            if let coins = dict["Coin"] as? Int {
                self.coinsForBoost = coins
            }
            
        }
        
        if let dict = data["callDateInitiated"] as? [String:Any] {
            
            if let coins = dict["Coin"] as? Int {
                self.coinsForCallDate = coins
            }
            
        }
        
        if let dict = data["audioDate"] as? [String:Any] {
            
            if let coins = dict["Coin"] as? Int {
                self.coinsForCallDate = coins
            }
            
        }
        
        if let dict = data["videoDate"] as? [String:Any] {
            
            if let coins = dict["Coin"] as? Int {
                self.coinsForVideoDate = coins
            }
            
        }
        
        
        
        if let dict = data["inPersonDateInitiated"] as? [String:Any] {
            
            if let coins = dict["Coin"] as? Int {
                self.coinsForInPerson = coins
            }
            
        }
        
        if let dict = data["physicalDate"] as? [String:Any] {
            
            if let coins = dict["Coin"] as? Int {
                self.coinsForInPerson = coins
            }
            
        }
        
        
        
        if let dict = data["perMsgWithoutMatch"] as? [String:Any] {
            
            if let coins = dict["Coin"] as? Int {
                self.coinsForMsg = coins
            }
            
        }
        
        if let dict = data["resheduleDate"] as? [String:Any] {
            
            if let coins = dict["Coin"] as? Int {
                self.coinsForReshedule = coins
            }
            
        }
        
        if let dict = data["rewind"] as? [String:Any] {
            
            if let coins = dict["Coin"] as? Int {
                self.coinsForRewind = coins
            }
            
        }
        
        
        if let dict = data["superLike"] as? [String:Any] {
            
            if let coins = dict["Coin"] as? Int {
                self.coinsForSuperLike = coins
            }
            
        }
        
        if let dict = data["videoDateInitiate"] as? [String:Any] {
            
            if let coins = dict["Coin"] as? Int {
                self.coinsForVideoDate = coins
            }
            
        }
    }
}

