//
//  InstagramProfile.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 25/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class InstagramProfile: NSObject {
    
    var instaId = ""
    var fullName = ""
    var userName = ""
    var profilePicture = ""
    var userMedia = [String]()
    var instaAuthentication = ""

    
    init(data: [String:Any]) {
        super.init()
        self.updateUser(data: data)

    }
    
    func updateMediaDetails(data:[[String:Any]]) {
        for each in data {
            if let images = each["carousel_media"] as? [[String:Any]] {
                for eachImage in images {
                    if let images = eachImage["images"] as? [String:Any] {
                        if let thumbNail = images["standard_resolution"] as? [String:Any] {
                            if let thumbUrl = thumbNail["url"] as? String {
                                self.userMedia.append(thumbUrl)
                            }
                        }
                    }
                }
            }else if let images = each["images"] as? [String:Any] {
                if let thumbNail = images["standard_resolution"] as? [String:Any] {
                    if let thumbUrl = thumbNail["url"] as? String {
                        self.userMedia.append(thumbUrl)
                    }
                }
            }
           
            if let user = each["user"] as? [String:Any] {
                
                if let id = user["id"] as? String {
                    self.instaId = id
                }
                
                if let name = user["full_name"] as? String {
                    self.fullName = name
                }
                if let userName = user["username"] as? String {
                    self.userName = userName
                    
                }
                if let profilepic = user["profile_picture"] as? String {
                    self.profilePicture = profilepic
                }
            }
        }
        
    }

    func updateUser(data: [String:Any]){
      
        if let data = data["standard_resolution"] as? [String:Any] {
            if let otherImage = data["url"] as? String {
                self.userMedia.append(otherImage)
            }
        }
        
        if let authToken = data["instaAuth"] as? String {
            self.instaAuthentication = authToken
        }
        
        if let id = data["id"] as? String {
            self.instaId = id
        }
        
        if let name = data["full_name"] as? String {
            self.fullName = name
        }
        if let userName = data["username"] as? String {
            self.userName = userName
            
        }
        if let profilepic = data["profile_picture"] as? String {
            self.profilePicture = profilepic
        }
    }
    
    
    
    func updateTokenId(instaId: String, instaAuth: String){
        self.instaId = instaId
        fullName = ""
        userName = ""
        profilePicture = ""
        userMedia = [String]()
        instaAuthentication = instaAuth
       
    }
    
}
