//
//  Location.swift
//  UFly
//
//  Created by 3Embed on 08/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import CoreLocation

class Location: NSObject {

    var locationDescription                 = ""
    var latitude:Double                     = 0.0
    var longitude:Double                     = 0.0
    var placeId                             = ""
    var name                                = ""
    var street                              = ""
    var city                                = ""
    var state                               = ""
    var zipcode                             = "0"
    var country                             = ""
    var fullText                            = ""
    var zoneId                              = ""
    var zoneName                            = ""
    var mainText                            = ""
    var tag                                 = ""
    var cityName                            = ""
    var cityId                              = ""
    var distance                            = "0"
    var secondaryText                       = ""
    var reference                           = ""
    
 
    init(data:[String:Any]) {
        super.init()
        name                                = ""
        street                              = ""
        city                                = " "
        state                               = ""
        zipcode                             = "0"
        country                             = ""
        fullText                            = ""
        mainText                            = ""
        tag                                 = ""
        distance                             = "0"
        secondaryText                       = ""
        reference                           = ""
        
        if let titleTemp = data[GoogleKeys.Reference] as? String{
            reference                         = titleTemp
        }
        
        if let titleTemp = data[GoogleKeys.Structure] as? [String:Any] {
            if let titleTemp2 = titleTemp[GoogleKeys.MainText] as? String {
                mainText = titleTemp2
            }
        }
        if let titleTemp = data[GoogleKeys.Structure] as? [String:Any] {
            if let titleTemp2 = titleTemp["secondary_text"] as? String {
                secondaryText = titleTemp2
            }
        }
        if let titleTemp = data[GoogleKeys.Description] as? String{
            locationDescription             = titleTemp
        }
        if let titleTemp = data[GoogleKeys.LocationLat] as? Float{
            latitude             = Double(titleTemp)
        }
        if let titleTemp = data[GoogleKeys.LocationLong] as? Float{
            longitude             = Double(titleTemp)
        }
        if let titleTemp = data[GoogleKeys.LocationLat] as? Double{
            latitude             = titleTemp
        }
        if let titleTemp = data[GoogleKeys.LocationLong] as? Double{
            longitude             = titleTemp
        }
        if let titleTemp = data["secondaryText"] as? String{
            secondaryText  = titleTemp
        }
        if let country = data["country"] as? String {
            self.country = country
        }
        
        if let state = data["state"] as? String {
            self.state = state
        }
        if let city = data["city"] as? String {
            self.cityName = city
            self.city = city
        }
        
         self.updateFromFourSquare(address: data)
        
    }
    
    func update(data:[String:Any], location:CLLocation){
        if let titleTemp = data[GoogleKeys.Reference] as? String{
            self.reference                         = titleTemp
        }
        if let titleTemp = data[GoogleKeys.MainText] as? String{
            self.mainText             = titleTemp
        }
        if let titleTemp = data[GoogleKeys.Description] as? String{
            self.locationDescription             = titleTemp
        }
        if let name = data[GoogleKeys.AddressName] as? String {
            self.name = name
        }
        // Street address
        if let street = data[GoogleKeys.AddressStreet] as? String {
            self.street = street
            if street == name {
                if let name = data[GoogleKeys.SubAdministrativeArea] as? String {
                    self.street = name
                }
                if let name = data[GoogleKeys.AddressSubLocality] as? String {
                    self.street = name
                }
            }
        }
        
        // City
        if let city = data[GoogleKeys.AddressCity] as? String {
            self.city = city
        }
        // State
        if let city = data[GoogleKeys.AddressState] as? String {
            state = city
        }
        
        // Zip code
        if let zip = data[GoogleKeys.AddressZIP] as? String {
            zipcode = zip
        }
        
        // Country
        if let country = data[GoogleKeys.AddressCountry] as? String {
            self.country = country
        }
        if locationDescription.sorted().count > 0 {
            fullText = locationDescription
        }else{
            if mainText.sorted().count > 0 {
                fullText = mainText + ", " + name + ", " + street + ", " + city + ", " + state + ", " + country + ", " + zipcode
            }else{
                fullText = name + ", " + street + ", " + city + ", " + state + ", " + country + ", " + zipcode
            }
        }
        latitude = location.coordinate.latitude
        longitude = location.coordinate.longitude
        // zoneid
        if let zoneid = data[GoogleKeys.ZoneId] as? String {
            zoneId = zoneid
        }
        // zonename
        if let zonename = data[GoogleKeys.ZoneName] as? String {
            zoneName = zonename
        }
       
        if let cityName = data[GoogleKeys.CityName] as? String {
            self.cityName = cityName
        }
        if let cityid = data[GoogleKeys.CityID] as? String {
            cityId = cityid
        }
    }
    
    func updateFromFourSquare(address:[String:Any]){
        
        if let id = address["id"] as? String {
            self.placeId = id
        }
        if let name = address["name"] as? String {
            self.name = name
        }
        if let address = address["address"] as? String {
            self.fullText = address
        }
        if let lang = address["lng"] as? Double {
            self.longitude = lang
        }
        if let lat = address["lat"] as? Double{
            self.latitude = lat
            
        }
        if let country = address["country"] as? String {
            self.country = country
        }
        
        if let state = address["state"] as? String {
            self.state = state
        }
        if let city = address["city"] as? String {
            self.cityName = city
        }
        
        if let location = address["location"] as? [String:Any] {
            
            if let state = location["state"] as? String {
                self.state = state
            }
            if let city = location["city"] as? String {
                self.cityName = city
            }
            if let crossStreet = location["crossStreet"] as? String {
                self.street = crossStreet
            }
            if let address = location["address"] as? String {
                self.fullText = address
            }
            if let distance = location["distance"] as? Int {
               let dist = Float(distance)/1000
               let dista = Helper.clipDigit(valeu: Float(dist), digits: 1)
                    if dista == "0.0"{
                        self.distance = "0.1"
                    }else {
                      
                        self.distance = dista
                        
                    }
                    
        }

            if let country = location["country"] as? String {
                self.country = country
            }
            if let lang = location["lng"] as? Double {
                self.longitude = lang
            }
            if let lat = location["lat"] as? Double{
                self.latitude = lat
                
            }
            
            if let titleTemp = location["secondaryText"] as? String{
                secondaryText  = titleTemp
            }
            if let titleTemp = location["description"] as? String{
                locationDescription  = titleTemp
            }
           
//            if let fullAddr = location["formattedAddress"] as? [String]{
//                for each in fullAddr {
//                    self.fullText = fullText + each + ","
//                }
//            }
            if fullText.count < 10 {
                if let titleTemp = location["description"] as? String{
                    fullText  = titleTemp
                }
            }
            
            
        }
        
    }
}
