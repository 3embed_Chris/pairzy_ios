//
//  CoinBalanceModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 05/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class CoinBalanceModel: NSObject {
    
    var id = ""
    var coin = 0
    
    
    init(data:[String:Any]) {
        if let id = data[ServiceInfo.userId] as? String {
            self.id = id
        }
        if let coins = data[ApiResponseKeys.Coins] as? [String:Any] {
            if let coin = coins[ApiResponseKeys.coin] as? Int {
                self.coin = coin
                AppConstant.coinBalance = coin
            }
        }
    }

}


