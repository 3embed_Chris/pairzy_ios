//
//  PurchasePlanModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 01/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class PurchasePlanModel: NSObject {
    
    var planId = ""
    var actualId = ""
    var cost:Float = 0.0
    var currencyCode = ""
    var currencySymbol = ""
    var descriptions  = ""
    var durationInDays = 0
    var durationInMonths = 0
    var hideAge = 0
    var hideDistance = 0
    
    var likeCount = 0
    var noAdds = 0
    var passport = 0
    var planName = ""
    var readreceipt = 0
    var recentVisitors = 0
    
    var rewindCount = 0
    var superLikeCount = 0
    var whoLikeMe = 0
    var whoSuperLikeMe = 0
    
    
    init(data:[String:Any]){
        
        if let id  =  data[ApiResponseKeys.id] as? String{
            self.planId = id
        }
        if let actId  =  data[ApiResponseKeys.actualId] as? String{
            self.actualId = actId
        }
        if let cost = data[ApiResponseKeys.cost] as? Float {
            self.cost = cost
        }
        if let currencyCode = data[ApiResponseKeys.currencyCode] as? String {
            self.currencyCode = currencyCode
        }
       
        if let currencySymbol = data[ApiResponseKeys.currencySymbol] as? String {
            self.currencySymbol = currencySymbol
        }
        if let descriptions = data[ApiResponseKeys.description] as? String {
            self.descriptions = descriptions
        }
        
        if let durationInDays = data[ApiResponseKeys.durationInDays] as? Int {
            self.durationInDays = durationInDays
        }
        if let durationInMonths = data[ApiResponseKeys.durationInMonths] as? Int {
            self.durationInMonths = durationInMonths
        }
        if let hideAge = data[ApiResponseKeys.hideAge] as? Int {
            self.hideAge = hideAge
        }
        if let hideDistance = data[ApiResponseKeys.hideDistance] as? Int {
            self.hideDistance = hideDistance
        }
        
        if let planName = data[ApiResponseKeys.planName] as? String {
            self.planName = planName
        }
        
        if let likeCount = data[ApiResponseKeys.likeCount] as? Int {
            self.likeCount = likeCount
        }
        if let noAdds = data[ApiResponseKeys.noAdds] as? Int {
            self.noAdds = noAdds
        }
        if let passport = data[ApiResponseKeys.passport] as? Int {
            self.passport = passport
        }
        if let readreceipt = data[ApiResponseKeys.readreceipt] as? Int {
            self.readreceipt = readreceipt
        }
        if let recentVisitors = data[ApiResponseKeys.recentVisitors] as? Int {
            self.recentVisitors = recentVisitors
        }
        
        
        if let rewindCount = data[ApiResponseKeys.rewindCount] as? Int {
            self.rewindCount = rewindCount
        }
        if let superLikeCount = data[ApiResponseKeys.superLikeCount] as? Int {
            self.superLikeCount = superLikeCount
        }
        if let whoLikeMe = data[ApiResponseKeys.whoLikeMe] as? Int {
            self.whoLikeMe = whoLikeMe
        }
        if let whoSuperLikeMe = data[ApiResponseKeys.whoSuperLikeMe] as? Int {
            self.whoSuperLikeMe = whoSuperLikeMe
        }
        
    }

}
//    "_id" = 5b0e879734874c0500481530;
//    cost = 450;
//    currencyCode = USD;
//    currencySymbol = "$";
//    description = "6 months";
//    durationInDays = 183;
//    durationInMonths = 6;
//    hideAge = 1;
//    hideDistance = 1;
//    likeCount = 600;
//    noAdds = 1;
//    passport = 1;
//    planName = "6 months";
//    readreceipt = 1;
//    recentVisitors = 1;
//    rewindCount = 600;
//    superLikeCount = 600;
//    whoLikeMe = 1;
//    whoSuperLikeMe = 1;
