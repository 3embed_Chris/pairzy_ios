//
//  CoinsPlanModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 01/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class CoinsPlanModel: NSObject {
    
    var coinPlanId = ""
    var cost:Float = 0.0
    var currency = ""
    var currencySymbol = ""
    var coins:Int = 0
    var planName = ""
    var actualId = ""
    
    init(data: [String:Any]) {
        
        
        if let id = data[ApiResponseKeys.id] as? String {
            self.coinPlanId = id
        }
        if let cost = data[ApiResponseKeys.cost] as? Float {
        self.cost = cost
        }
        if let currency = data[ApiResponseKeys.currency] as? String {
            self.currency = currency
        }
        if let currencySymbol = data[ApiResponseKeys.currencySymbol] as? String {
            self.currencySymbol = currencySymbol
        }
        
        if let coinObj = data[ApiResponseKeys.noOfCoinUnlock] as? [String:Any] {
            if let coins = coinObj[ApiResponseKeys.coin] as? Int {
                self.coins = coins
            }
        }
        
        if let planName = data[ApiResponseKeys.planName] as? String {
            self.planName = planName
        }
        
        if let actualId = data[ApiResponseKeys.actualId] as? String {
            self.actualId = actualId
        }
    }
}

