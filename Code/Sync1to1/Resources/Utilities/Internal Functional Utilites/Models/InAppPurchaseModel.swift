//
//  InAppPurchaseModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 04/07/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class InAppPurchaseModel: NSObject {
    
   
    var productId = ""
    var planTitle = ""
    var planDiscription = ""
    var price:Float = 0.0
    var currencyCode = ""
    var currencySymbol = ""
    var monthsCount = 0
    var months = 0
   
    
    
    init(data:[String:Any]) {
        
        if let productId = data[InAppPurchase.ProductId] as? String {
            self.productId = productId
            
        }
        if let planTitle = data[InAppPurchase.ProductTitle] as? String {
            self.planTitle = planTitle
        }
        if let planDiscription = data[InAppPurchase.PoductDescript] as? String {
            self.planDiscription = planDiscription
        }
    
        if let price = data[InAppPurchase.Price] as? Float {
            self.price = price
        }
        
        if let price = data[InAppPurchase.Price] as? Int {
            self.price = Float(price)
        }
        if let price = data[InAppPurchase.Price] as? Double {
            self.price = Float(price)
        }
        
        if let currencyCode = data[InAppPurchase.CurrencyCode] as? String {
            self.currencyCode = currencyCode
            
        }
        if let currencySymbol = data[InAppPurchase.CurrencySymbol] as? String {
            self.currencySymbol = currencySymbol
        }
        if productId == AppConstant.threeMonthSubID {
            self.monthsCount = 1
        }else if productId == AppConstant.sixMonthSubID {
            self.monthsCount = 6
        }else if productId == AppConstant.oneYearSubID {
             self.monthsCount = 12
        }
        
    }

}
