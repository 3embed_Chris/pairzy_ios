//
//  USDHelper.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 21/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class USDHelper: NSObject {

   static func stopDisPlayingNotification() {
        UserDefaults.standard.set(false, forKey: SyUserdefaultKeys.displayNotification)
        UserDefaults.standard.synchronize()
    }
    
    static func startDisPlayingNotification() {
        UserDefaults.standard.set(true, forKey: SyUserdefaultKeys.displayNotification)
        UserDefaults.standard.synchronize()
    }
    
    
  
    
    
}
