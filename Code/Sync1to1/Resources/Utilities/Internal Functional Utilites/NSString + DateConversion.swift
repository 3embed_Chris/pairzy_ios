//
//  NSString + DateConversion.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 03/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension Date {
    var millisecondsSince1970:String {
        return String((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}


//Milliseconds to date
extension Double {
    func dateFromMilliseconds() -> Date {
        return Date(timeIntervalSince1970: TimeInterval(self)/1000)
    }
}
