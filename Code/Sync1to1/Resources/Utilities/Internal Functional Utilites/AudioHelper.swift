//
//  AudioHelper.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 25/07/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import AVKit

class AudioHelper: NSObject {
     static let sharedInstance = AudioHelper()
    var audioPlayer: AVAudioPlayer!

    func playCoinsSound() {
        guard let path = Bundle.main.path(forResource: "Coin-collect-sound-effect", ofType: "mp3") else{return}
        guard let url = URL(string: path) else{return}
        do{
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        }catch let error{
            print(error.localizedDescription)
        }
    }
}
