//
//  LocationManager.swift
//  Trustpals
//
//  Created by NABEEL on 12/06/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import RxCocoa
import RxSwift
import RxAlamofire

protocol LocationManagerDelegate : class {
    //Return search address
    func didUpdateSearch(locations: [Location])
    /// When Update Location
    func didUpdateLocation(location: Location, search:Bool,update:Bool)
    
    /// When Failed to Update Location
    func didFailToUpdateLocation()
    
    func didChangeAuthorization(authorized: Bool)
}

class LocationManager: NSObject {
    let disposeBag = DisposeBag()
    
    var latitute: Float = 12.3254453
    var longitude: Float = 77.2456456
    var name: String = ""
    var FullText = ""
    var tempName = ""
    
    var city: String = ""
    var state: String = ""
    var country: String = ""
    var zipcode: String = ""
    var street: String = ""
    var countryCode: String = ""
    var CLlocationObj = CLLocation()
    
    var locationObj: CLLocationManager? = nil
   weak var delegate: LocationManagerDelegate? = nil
    var dbObj = Database.shared
    var mapVC = MapVC()
    
    static var shareObj: LocationManager? = nil
    /// Create Shared Instance
    static var shared: LocationManager {
        if shareObj == nil {
            shareObj = LocationManager()
        }
        return shareObj!
    }
    
    override init() {
        super.init()
        locationObj = CLLocationManager()
        locationObj?.delegate = self
        locationObj?.requestWhenInUseAuthorization()
        
    }
    /// Start Location Update
    func start() {
//        if CLLocationManager.authorizationStatus() == .denied{
//           //show alert to enable location
//           // showAlertToEnableLoc()
//            return
//        }
        locationObj?.startUpdatingLocation()
    }
    /// Stop Location Update
    func stop() {
        locationObj?.stopUpdatingLocation()
    }
    //MARK: - Google Auto Complete
    
    /// Call Google Auto Complete API
    ///
    /// - Parameter searchText: Words to be Searched
    func search(searchText: String) {
        
        // Remove Blank space from word
        let text = searchText.replacingOccurrences(of: " ", with: "")
        
        let latitudeTemp = LocationManager.shared.latitute
        let longitudeTemp = LocationManager.shared.longitude
        let language = "en"
        let key = GoogleKeys.GoogleMapKey
        
        let placeAPI = String(format: GoogleKeys.PlacesAPILink, text,latitudeTemp,longitudeTemp,language,key)
        
        // Call Service
        RxAlamofire.requestJSON(.get, placeAPI, parameters: [:], headers: [:]).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as! [String:Any]
            if let array = bodyIn[GoogleKeys.Predictions] as? [Any] {
                var arrayOfLocation = [Location]()
                for dataFrom in array {
                    let data = Location.init(data: dataFrom as! [String : Any])
                    arrayOfLocation.append(data)
                }
                self.delegate?.didUpdateSearch(locations: arrayOfLocation)
            }
        }, onError: { (Error) in
        }).disposed(by: self.disposeBag)
    }
    
    
    func updateLocationData(location : CLLocation , placeIn:Location,flag:Bool) {
        CLlocationObj = location
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location) {
            
            (placemarks, error) -> Void in
            
            if placemarks == nil {
                return
            }
            
            
            let arrayOfPlaces = placemarks as! [CLPlacemark]
            
            // Place details
            let placemark: CLPlacemark = (arrayOfPlaces[0])
            
            // Address dictionary
            var flagSearch = true
            // Location name
            if self.tempName.sorted().count > 0 {
                self.name = self.tempName
                self.tempName = ""
                flagSearch = true
            }else{
                if let name = placemark.name {
                    self.name = name
                    flagSearch = false
                }
            }
            let placeData = placemark.addressDictionary as! [String : Any]
            // Country
            if let country = placemark.country {
                if GoogleKeys.myCountry.sorted().count == 0 {
                    GoogleKeys.myCountry = country
                }
            }
            placeIn.update(data: placeData, location:self.CLlocationObj)
            if flag {
             Utility.saveCurrentAddress(lat: Float(location.coordinate.latitude), long: Float(location.coordinate.longitude),address: placeIn)
                if let appdele = UIApplication.shared.delegate as? AppDelegate {
                     appdele.saveContext()
                }
            }
            if self.delegate != nil {
                self.delegate?.didUpdateLocation(location: placeIn, search: flagSearch, update:flag)
            }
            
        }
    }
    
    
    func googlePlacesInformation(placeId: String, completion: @escaping (_ place: NSDictionary) -> Void) {
        
        let urlString = String(format: GoogleKeys.PlaceEnlarge,placeId,GoogleKeys.GoogleMapKey)
        RxAlamofire.requestJSON(.get, urlString, parameters: [:], headers: [:]).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as! [String:Any]
            let status = bodyIn["status"] as! String
            if status != "NOT_FOUND" || status != "REQUEST_DENIED" {
                if let results = bodyIn["result"] as? NSDictionary {
                       completion(results)
                }
              //  completion(results)
            }
        }, onError: { (Error) in
        }).disposed(by: self.disposeBag)
    }
    
    
    func getAddressFromPlace(place:Location) {
//        Helper.showPI(string: StringConstants.ValidateLocation)
        googlePlacesInformation(placeId: place.reference,
                                completion:{ (placeFrom) -> Void in
                                    
                                    
                                    //            let searchResult = NSDictionary()
                                    if placeFrom.count > 0
                                    {
                                        self.tempName = placeFrom.value(forKey: "name") as! String
                                        let latitude =  (((placeFrom.value(forKey: "geometry") as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "lat") as! NSNumber)
                                        
                                        let longitude =  (((placeFrom.value(forKey: "geometry") as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "lng") as! NSNumber)
                                        
                                        _ = CLLocation(latitude: CLLocationDegrees(truncating: latitude), longitude: CLLocationDegrees(truncating: longitude))
                                        
                                        //self.updateLocationData(location: locationin , placeIn:place, flag: false)
                                        place.latitude = Double(truncating: latitude)
                                        place.longitude = Double(truncating: longitude)
                                        if self.delegate != nil {
                                            self.delegate?.didUpdateLocation(location: place, search: true, update: true)
                                        }
                                        
                                        
                                    }
        })
    }

    
}

extension LocationManager: CLLocationManagerDelegate {
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch(status) {
            
        case .authorizedAlways, .authorizedWhenInUse:
            
            print("Access")
            locationObj?.startUpdatingLocation()
            delegate?.didChangeAuthorization(authorized: true)
            break
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.first != nil {
            stop()
            let location = locations.first
            latitute = Float((location?.coordinate.latitude)!)
            longitude = Float((location?.coordinate.longitude)!)
            updateLocationData(location : location!, placeIn:Location.init(data: [String:Any]()),flag:true)
         
        }
        
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print("\nDid fail location : %@\n",error.localizedDescription)
            delegate?.didFailToUpdateLocation()
        }
    }
    
    func showAlertToEnableLoc(){
        
        let alert = UIAlertController(title: "Location Services Disabled", message: "You need to enable location services in settings.", preferredStyle: .alert)
        
        // Add "OK" Button to alert, pressing it will bring you to the settings app
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
        }))
        
        let appDelegateObj = UIApplication.shared.delegate! as! AppDelegate
        appDelegateObj.window?.rootViewController?.present(alert, animated: true, completion: nil)
        // self.present(alert, animated: true)
    }
    
    
}
