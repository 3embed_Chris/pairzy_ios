//
//  Helper.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 09/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import KRProgressHUD
import Firebase
import libPhoneNumber_iOS


class Helper: NSObject {
    
    class func blurEffect(bg: UIImageView,blurrVal: Int) {
        let context = CIContext(options: nil)
        let currentFilter = CIFilter(name: "CIGaussianBlur")
        let beginImage = CIImage(image: bg.image ?? UIImage())
        currentFilter!.setValue(beginImage, forKey: kCIInputImageKey)
        currentFilter!.setValue(blurrVal, forKey: kCIInputRadiusKey)

        let cropFilter = CIFilter(name: "CICrop")
        cropFilter!.setValue(currentFilter!.outputImage, forKey: kCIInputImageKey)
        cropFilter!.setValue(CIVector(cgRect: beginImage!.extent), forKey: "inputRectangle")
        
        let output = cropFilter!.outputImage
        let cgimg = context.createCGImage(output!, from: output!.extent)
        let processedImage = UIImage(cgImage: cgimg!)
        bg.image = processedImage
    }
    
    class func saveLanguageCode(data: [String:Any]){
        UserDefaults.standard.setValue(data, forKey: "selectedLanguage")
        UserDefaults.standard.synchronize()
    }
    
    class func getSelectedLanguage() -> LanguageModel{
        var language = LanguageModel()
        if let lang = UserDefaults.standard.value(forKey:"selectedLanguage") as? [String:Any] {
            language.code = lang["code"] as! String
            language.name = lang["name"] as! String
        }else{
            language.code = "en"
            language.name = "English"
        }
        return language
    }
    
    class func getcurrentDataAndtimeForSave()-> String{
        let todayDate:Date = Date()
        return todayDate.millisecondsSince1970
    }
    
    /// To get time past from current time
    ///
    /// - Parameter timeStamp: time stamp
    /// - Returns: time in string
    class func getTimeStamp(timeStamp: Double)->String{
        let timeIntervalFormator = TTTTimeIntervalFormatter()
        let actvityTime: Date = Date(timeIntervalSince1970: timeStamp/1000)
        let interval: TimeInterval  = actvityTime.timeIntervalSinceNow
        let timeStampDate: String = timeIntervalFormator.string(forTimeInterval: interval)
        return timeStampDate
    }
    
    
    class func saveCloudinaryDetails(data: [String:Any]){
        UserDefaults.standard.setValue(data, forKey: "cloudinaryDetails")
        UserDefaults.standard.synchronize()
    }
    
    class func getCludinaryDetails()->[String:Any]{
        var data:[String:Any] = ["cloudName": AppConstant.cloudName,
                                 "uploadPreset": AppConstant.uploadPreset]
        if let tempData = UserDefaults.standard.value(forKey:"cloudinaryDetails") as? [String:Any]{
            data["cloudName"] = tempData["cloudName"]
            data["uploadPreset"] = tempData["uploadPreset"]
        }
        return data
    }
    
    
    class func saveSocialProfile(data: [String:Any]){
        UserDefaults.standard.setValue(data, forKey: "SocialProfile")
        UserDefaults.standard.synchronize()
    }
    
    class func getSocialProfile() -> LocalAuthentication {
        var socialProfile = LocalAuthentication(data: [:])
        if let userInfo = UserDefaults.standard.value(forKey:"SocialProfile") as? [String:Any] {
            let fbProfile = LocalAuthentication.init(data: userInfo)
            socialProfile = fbProfile
        }
        return socialProfile
    }
    
    class func saveCoinConfig(data: [String:Any]){
        
        if let dict =  data["data"] as? [[String:Any]] {
            UserDefaults.standard.setValue(dict[0], forKey: "CoinConfig")
            UserDefaults.standard.synchronize()
        }
    }
    
    class func getCoinConfig() -> CoinConfig{
        var coinConfig = CoinConfig(data:[:])
        if  let data = UserDefaults.standard.value(forKey: "CoinConfig") as? [String:Any] {
            coinConfig = CoinConfig.init(data: data)
            
        }
        return coinConfig
        
    }
    
    
    class func saveCountsObj(data: [String:Any]){
        
        UserDefaults.standard.setValue(data, forKey: "counts")
        
        if let likeC =  data["like"] as? Int {
            UserDefaults.standard.set(likeC, forKey: "likeCount")
        }
        if let rewindC =  data["rewind"] as? Int {
            UserDefaults.standard.set(rewindC, forKey: "rewindCount")
        }
        if let supeC =  data["superlike"] as? Int {
            UserDefaults.standard.set(supeC, forKey: "superLikeCount")
        }
        UserDefaults.standard.synchronize()
    }
    
    
    class func getCounts() ->Profile{
        
        let counts = ["like": UserDefaults.standard.integer(forKey: "likeCount"),
                      "superlike": UserDefaults.standard.integer(forKey: "superLikeCount"),
                      "rewind":UserDefaults.standard.integer(forKey: "likeCount")]
        UserDefaults.standard.setValue(counts, forKey: "counts")
        
        var profileDetails = Profile.fetchprofileDataObj(userDataDictionary:[:])
        if let data = UserDefaults.standard.value(forKey:"counts") as? [String:Any]{
            profileDetails = Profile.fetchprofileDataObj(userDataDictionary:data)
        }
        
        return profileDetails
        
    }
    
    
    class func saveLikeCount(){
        if let likeCount = UserDefaults.standard.integer(forKey: "likeCount") as Int?  {
            let likeC = likeCount + 1
            UserDefaults.standard.set(likeC, forKey: "likeCount")
            UserDefaults.standard.synchronize()
        }
        
    }
    class func saveSuperLikeCount(){
        if let rewindCount = UserDefaults.standard.integer(forKey: "rewindCount") as Int?  {
            let rewindC = rewindCount + 1
            UserDefaults.standard.set(rewindC, forKey: "rewindCount")
            UserDefaults.standard.synchronize()
        }
    }
    
    class func saveRewindCount(){
        if let supCount = UserDefaults.standard.integer(forKey: "superLikeCount") as Int?  {
            let supeC = supCount + 1
            UserDefaults.standard.set(supeC, forKey: "superLikeCount")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    //API response
    class func savePlans(data:[Any]){
        
        UserDefaults.standard.setValue(data, forKey: "plans")
        UserDefaults.standard.synchronize()
        
    }
    
    //data from API
    class  func getPlans() ->[PurchasePlanModel]{
        var purchasPlan = [PurchasePlanModel]()
        if let data = UserDefaults.standard.value(forKey:"plans") as? [Any] {
            for each in data {
                purchasPlan.append(PurchasePlanModel.init(data:each as! [String : Any]))
            }
        }
        return purchasPlan
    }
    
    
    static func isIPhonex() -> Bool {
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            //iPhone X
            return true
        }
        return false
    }
    
    //save purchase plans from appstore
    class func savePurchasedPlan(data:[String:Any]){
        UserDefaults.standard.setValue(data, forKey: "purchaseDetails")
        UserDefaults.standard.synchronize()
    }
    
    class func getPurchasedPlan()->[String:Any]{
        if let purchasedProDetails = UserDefaults.standard.object(forKey: "purchaseDetails") as? [String:Any] {
            return purchasedProDetails
        }
        return [:]
    }
    
    class func getPurchasedID() -> String {
        if let purchaseDetails = Helper.getPurchasedPlan() as? [String:Any] {
            if let planID = purchaseDetails["productId"] as? String {
                return planID
            }
        }
        return ""
    }
    
    
    
    
    class func saveIsUserPurchased(flag:Bool){
        UserDefaults.standard.set(flag, forKey: "purchased")
        UserDefaults.standard.synchronize()
    }
    
    // save all the available Appstore plans
    class func savePurchasePlans(data:[[String:Any]]){
        UserDefaults.standard.setValue(data, forKey:"purchasePlans")
        UserDefaults.standard.synchronize()
    }
    
    // get plans from appstore
    class func getPurchasePlans() ->[InAppPurchaseModel]{
        
        var purchasePlans = [InAppPurchaseModel]()
        if let data = UserDefaults.standard.value(forKey:"purchasePlans") as? [[String:Any]] {
            for each in data {
                purchasePlans.append(InAppPurchaseModel.init(data:each))
            }
        }
        for each in purchasePlans {
            if each.productId == AppConstant.threeMonthSubID {
                purchasePlans[0] = each
            }else if each.productId == AppConstant.sixMonthSubID {
                purchasePlans[1] = each
            }else if each.productId == AppConstant.oneYearSubID{
                purchasePlans[2] = each
            }
        }
        
        return purchasePlans
    }
    
    class func getCoinPLansFromAppStore()->[InAppPurchaseModel]{
        var coinPlans = [InAppPurchaseModel]()
        if let data = UserDefaults.standard.value(forKey:"purchasePlans") as? [[String:Any]] {
            for each in data {
                coinPlans.append(InAppPurchaseModel.init(data:each))
            }
            
            for each in coinPlans {
                if each.productId == AppConstant.coinsSubId750 {
                    coinPlans[0] = each
                }else if each.productId == AppConstant.coinsSubId1500 {
                    coinPlans[1] = each
                }else if each.productId == AppConstant.coinsSubId2000{
                    coinPlans[2] = each
                }
            }
        }
        return coinPlans
    }
    
    class func savePreferences(data: [String:Any]){
        UserDefaults.standard.set(data, forKey:"preferences")
        UserDefaults.standard.synchronize()
        
    }
    
    /// Remove null from Objects
    ///
    /// - Returns: Object without Null
    class func nullKeyRemoval(data:[String:Any]) -> [String:Any] {
        var dict = data
        let keysToRemove = Array(dict.keys).filter { dict[$0] is NSNull }
        for key in keysToRemove {
            dict.removeValue(forKey: key)
        }
        var keysToUpdate = Array(dict.keys).filter { dict[$0] is [String:Any] }
        for key in keysToUpdate {
            dict.updateValue(nullKeyRemoval(data: dict[key] as! [String : Any]), forKey: key)
        }
        keysToUpdate = Array(dict.keys).filter { dict[$0] is [Any] }
        for key in keysToUpdate {
            let dataArray = dict[key] as! [Any]
            var temp:[Any] = []
            for item in dataArray {
                if item is NSNull {
                    
                }else{
                    temp.append(item)
                }
            }
            dict.updateValue(temp, forKey: key)
        }
        return dict
    }
    
    
    
    
    class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class func removeSpecialCharsFromString(text: String) -> String {
        //  let okayChars : Set<Character> =
        //      Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_".characters)
        //  return String(text.characters.filter {okayChars.contains($0) })
        
        let result = String(text.characters.filter { "+01234567890.".characters.contains($0) })
        
        return result
    }
    
    class func  checkCallGoingOn() -> Bool{
        if  let isCallgoingOn = UserDefaults.standard.object(forKey:"iscallgoingOn") as? Bool {
            return isCallgoingOn
        }
        return false
    }
    
    class func defaultTimerArr() -> [String] {
        return ["off","1 Second","2 Second","3 Second","4 Second","5 Second","6 Second","7 Second","8 Second","9 Second","10 Second","15 Second","30 Second","1 Minute","1 Hour","1 Day","1 Week"]
    }
    
    class func timeInsecArr() -> [Int]{
        return [0,1,2,3,4,5,6,7,8,9,10,15,30,60,3600,86400,604800]
    }
    
    //Actionsheet object
    class func initActionSheet(title:String? , message:String?) -> UIAlertController{
        let actionController = UIAlertController.init(title: title, message: message, preferredStyle: .actionSheet)
        
        let action2 = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
        })
        
        actionController.addAction(action2)
        return actionController
    }
    
    
    class func saveMyProfile(data: [String:Any]){
        UserDefaults.standard.set(data, forKey:"MyProfile")
        UserDefaults.standard.synchronize()
        
    }
    class func getMyProfile() -> Profile {
        var profileDetails = Profile.fetchprofileDataObj(userDataDictionary:[:])
        if  let data = UserDefaults.standard.object(forKey: "MyProfile") as? [String:Any] {
            profileDetails = Profile.fetchprofileDataObj(userDataDictionary:data)
        }
        return profileDetails
    }
    
    class func saveCoinBalance(coinBalance: Int){
        UserDefaults.standard.set(coinBalance, forKey: "coinBalance")
        UserDefaults.standard.synchronize()
    }
    
    class func getCoinBalance() -> Int {
        let balance = UserDefaults.standard.integer(forKey: "coinBalance")
        return balance
    }
    
    static func isUserOutOfLikes() -> Bool {
        if  Helper.isSubscribedForPlans() {
            return false
        } else {
            if (self.isKeyPresentInUserDefaults(key:"remainingCoins")) {
                let numberOfLikes = UserDefaults.standard.integer(forKey: "remainingCoins")
                if(numberOfLikes > 0) {
                    return false
                }else {
                    return false  //true
                }
            }
            return false //true
        }
    }
    
    
    static func newTimeForOutOfLikes() -> Float {
        if (self.isKeyPresentInUserDefaults(key:"expireTimeForLikes")) {
            let newLikesTime = UserDefaults.standard.float(forKey:"expireTimeForLikes")
            return newLikesTime
        }
        return 1536196951
    }
    
    
    static func newTimeForOutOfRewinds() -> Float {
        if (self.isKeyPresentInUserDefaults(key:"expireTimeForRewinds")) {
            let newLikesTime = UserDefaults.standard.float(forKey:"expireTimeForRewinds")
            return newLikesTime
        }
        return 1536196951
    }
    
    static func isUserOutOfRewinds() -> Bool {
        if  Helper.isSubscribedForPlans() {
            return false
        } else {
            if (self.isKeyPresentInUserDefaults(key:"remainingRewinds")) {
                let numberOfrewinds = UserDefaults.standard.integer(forKey: "remainingRewinds")
                if(numberOfrewinds > 0) {
                    return false
                }else {
                    return false
                }
            }
            return false
        }
    }
    
    
    
    static func updateRemainingLikes(likes:Int) {
        UserDefaults.standard.set(likes, forKey: "remainingCoins")
        UserDefaults.standard.synchronize()
    }
    
    
    static func updateExpireTimeForLikes(timestamp:Float) {
        let timeInSecs = timestamp/1000
        UserDefaults.standard.set(timeInSecs, forKey:"expireTimeForLikes")
        UserDefaults.standard.synchronize()
    }
    
    
    static func updateExpireTimeForRewinds(timestamp:Float) {
        let timeInSecs = timestamp/1000
        UserDefaults.standard.set(timeInSecs, forKey:"expireTimeForRewinds")
        UserDefaults.standard.synchronize()
    }
    
    static func updateRemainingRewinds(rewinds:Int) {
        UserDefaults.standard.set(rewinds, forKey: "remainingRewinds")
        UserDefaults.standard.synchronize()
    }
    
    
    static func updateBoostDetails(boostDetails:[String:Any]) {
        UserDefaults.standard.set(boostDetails, forKey:"isActiveBoost")
        UserDefaults.standard.synchronize()
    }
    
    
    static func deleteUserChat(userDeatils:Any) {
        
        var memberID = ""
        if let isProfileObj = userDeatils as? Profile {
            memberID = isProfileObj.userId
        } else if let isUserID = userDeatils as? String {
            memberID = isUserID
        } else {
            return
        }
        
        let couchbaseObj = Couchbase.sharedInstance
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        
        let individualChatDocVMObject = IndividualChatViewModel(couchbase: couchbaseObj)
        guard let chatDocID = individualChatDocVMObject.getChatDocID(withreceiverID: memberID, andSecretID: "", withContactObj: nil, messageData: nil, destructionTime: nil, isCreatingChat: false) else {
            //DDLogDebug("error in creating chatdoc \(self)")
            return
        }
        if let chatObj = chatsDocVMObject.getChatObj(fromChatDocID: chatDocID) {
            let chatViewModelObj = ChatViewModel(withChatData: chatObj)
            chatsDocVMObject.clearMessagesInChatDoc(toDocID:chatViewModelObj.docID)
        }
    }
    
    
    
    static func isUserBoostActive() -> Bool {
        if let boostData = UserDefaults.standard.value(forKey:"isActiveBoost") as? [String:Any] {
            if boostData.count>0{
                if let isActiveBoost = boostData["boost"] as? [String:Any] {
                    if let expireTime = isActiveBoost["expire"] as? Double {
                        let expireDate:Date = Date(timeIntervalSince1970: Double(expireTime/1000))
                        return !expireDate.isDateIsPast(date:Date())
                    }
                }
                
                if let expireTime = boostData["expire"] as? Double {
                    let expireDate:Date = Date(timeIntervalSince1970: Double(expireTime/1000))
                    return !expireDate.isDateIsPast(date:Date())
                }
            }
        }
        return false
    }
    
    static func getActiveBoostDetails() -> [String:Any] {
        if let isActiveBoost = UserDefaults.standard.value(forKey:"isActiveBoost") as? [String:Any] {
            return isActiveBoost
        }
        return [:]
    }
    
    class func saveCoinPlans(data: [Any]){
        UserDefaults.standard.set(data, forKey: "coinPlans")
        UserDefaults.standard.synchronize()
    }
    
    
    static func showTutorialView() -> Bool {
        return UserDefaults.standard.bool(forKey:"showTutorial")
    }
    
    static func showPairTutorialView() -> Bool {
        return UserDefaults.standard.bool(forKey:"showPairTutorial")
    }
    
    static func updateShowTutorialStatus() {
        UserDefaults.standard.set(true, forKey:"showTutorial")
    }
    
    static func updateShowPairTutorialStatus() {
        UserDefaults.standard.set(true, forKey:"showPairTutorial")
    }
    
    class func getCoinPlans() ->[CoinsPlanModel] {
        var coinsPlans = [CoinsPlanModel]()
        if  let data = UserDefaults.standard.object(forKey: "coinPlans") as? [Any] {
            for each in data {
                coinsPlans.append(CoinsPlanModel.init(data:each as! [String : Any]))
            }
        }
        return coinsPlans
    }
    
    
    class func saveCoinHistory(data: [String:Any]){
        UserDefaults.standard.set(data, forKey: "coinHistory")
        UserDefaults.standard.synchronize()
    }
    
    
    class func getCoinHistory() ->[[CoinsHistoryModel]]{
        
        var coinHistory = [[CoinsHistoryModel]]()
        
        var tempAllCoins = [CoinsHistoryModel]()
        var tempCoinIn = [CoinsHistoryModel]()
        var tempCoinOut = [CoinsHistoryModel]()
        if  let data = UserDefaults.standard.object(forKey: "coinHistory") as? [String:Any] {
            if let allcoins = data[ApiResponseKeys.AllCoins] as? [Any] {
                for each in allcoins {
                    tempAllCoins.append(CoinsHistoryModel.init(data:each as! [String : Any]))
                }
                coinHistory.append(tempAllCoins)
            }
            
            if let coinsIn = data[ApiResponseKeys.CoinIn] as? [Any] {
                for each in coinsIn {
                    tempCoinIn.append(CoinsHistoryModel.init(data:each as! [String : Any]))
                }
                coinHistory.append(tempCoinIn)
            }
            
            if let coinsOut = data[ApiResponseKeys.CoinOut] as? [Any] {
                for each in coinsOut {
                    tempCoinOut.append(CoinsHistoryModel.init(data:each as! [String : Any]))
                }
                coinHistory.append(tempCoinOut)
            }
            coinHistory.insert(tempCoinOut, at: 1)
            coinHistory.insert(tempCoinIn, at: 2)
            coinHistory.remove(at: 3)
            coinHistory.remove(at: 3)
        }
        return coinHistory
        
    }
    
    static func isUserHasMinimumCoins(minValue:Int) -> Bool {
        //check coins availabel or not.  For creating date user need 100 coins.
        if let getBal = UserDefaults.standard.object(forKey: "coinBalance") as? Int {
            if getBal >= minValue {
                //user has coins allow user to create date.
                return true
            } else {
                // has lessthan 100 coins.
                return false
            }
        } else {
            // no coins available.
            return false
        }
    }
    
    static func showAlertWithMessageOnwindow(withTitle:String,message:String){
        let alert = UIAlertController(title: withTitle, message:message, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        
        let appDelegateObj = UIApplication.shared.delegate! as! AppDelegate
        
        appDelegateObj.window!.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    class func getPreferences() -> [PreferenceSubModel]{
        var myPreferences = [PreferenceSubModel]()
        if  let data = UserDefaults.standard.object(forKey: "preferences") as? [String:Any] {
            let myPref = data[ApiResponseKeys.myPreferences] as! NSArray
            for preferDict in myPref {
                myPreferences.append(PreferenceSubModel.init(preference: preferDict as! [String : Any], index:myPreferences.count))
            }
        }
        return myPreferences
    }
    
    class func getPreference() -> [PreferencesModel]{
        var myPreferences = [PreferencesModel]()
        if  let data = UserDefaults.standard.object(forKey: "preferences") as? [String:Any] {
            if  let myPref = data[ApiResponseKeys.myPreferences] as? [[String:Any]] {
                for cat in myPref {
                    myPreferences.append(PreferencesModel.init(preferences: cat ))
                }
            }
            if  let myPref = data["myPrefrances"] as? [[String:Any]] {
                for cat in myPref {
                    myPreferences.append(PreferencesModel.init(preferences: cat ))
                }
            }
        }
        
        return myPreferences
    }
    
    //Add Shadow
    class func setShadow(sender:UIView) {
        
        sender.layer.shadowOffset = CGSize(width: CGFloat(2), height: CGFloat(2))
        sender.layer.shadowColor = UIColor.lightGray.cgColor
        sender.layer.shadowOpacity = 0.5
    }
    
    class func addDefaultNavigationGesture(VC:UIViewController) {
        VC.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    //removeshadow
    class func removeShadow(sender:UIView) {
        
        sender.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(0))
        sender.layer.shadowColor = UIColor.clear.cgColor
        sender.layer.shadowOpacity = 0
    }
    
    //Add shadow to NavigationController
    class func addShadowToNavigationBar(controller:UINavigationController){
        controller.navigationBar.layer.masksToBounds = false
        controller.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        controller.navigationBar.layer.shadowOpacity = 0.8
        controller.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        controller.navigationBar.layer.shadowRadius = 2
    }
    
    class func removeShadowToNavigationBar(controller:UINavigationController){
        controller.navigationBar.layer.masksToBounds = true
        controller.navigationBar.layer.shadowColor = UIColor.white.cgColor
        controller.navigationBar.layer.shadowOpacity = 0
        controller.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        controller.navigationBar.layer.shadowRadius = 0
    }
    
    
    static func showAlertWithMessage(withTitle:String,message:String, onViewController:UIViewController){
        let alert = UIAlertController(title: withTitle, message:message, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        onViewController.present(alert, animated: true, completion: nil)
    }
    
    
    static func getSBWithName(name:String) -> UIStoryboard {
        return UIStoryboard.init(name: name, bundle: nil)
    }
    
    static func getSBFromVC(vc:UIViewController) -> UIStoryboard {
        return vc.storyboard!
    }
    
    static func updateCardView(name:String, subtitle: String, profilPick: UIImage) -> UIView{
        let cardView:CardViewClass = CardViewClass().shared
        cardView.cardImage.image = profilPick
        cardView.name.text = name
        cardView.placeName.text = subtitle
        cardView.tag = 2012
        return cardView
    }
    
    class func clipDigit(valeu:Float, digits: Int) -> String {
        return String(format:"%.\(digits)f", valeu)
    }
    
    static func getCurrentTimeStamp() -> String {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddhhmmssa"
        formatter.locale = Locale.current
        let result = formatter.string(from: date)
        return result
    }
    

    static func dateFromMilliseconds(timestamp:String) -> Date {
        if timestamp.count != 0 {
            return Date(timeIntervalSince1970: TimeInterval(timestamp)!/1000)
        }
        return Date()
    }
    
    static func getMQTTID() -> String? {
        let dbObj = Database.shared
        let mqttId = dbObj.userDataObj.userId
        return mqttId
    }
    
    
    static func getUserProfilePic() -> String? {
        let dbObj = Database.shared
        let name = dbObj.userDataObj.profilePictureURL
        return name
    }
    
    static func getUserName() -> String? {
        let dbObj = Database.shared
        let name = dbObj.userDataObj.first_name
        return name
    }
    
    static func getLogData() -> UserDetails? {
        let mobileNum = Database.shared.userDataObj
        return mobileNum
    }
    
    static func isSubscribedForPlans() -> Bool{
        
        let dataReceipt = self.getReceiptData()
        
        let purchasedPlan = self.getPurchasedPlan()
        // == purchasedPlan["paymentGatewayTxnId"]
        for each in dataReceipt {
            if let id = each["transaction_id"] as? String {
                if let pauchasedId = purchasedPlan["paymentGatewayTxnId"] as? String {
                    if  pauchasedId == id {
                        print(each)
                        if let dateInms = each["expires_date_ms"] as? String {
                            print("expireDate: \(dateInms)")
                            let expireDate = Helper.dateFromMilliseconds(timestamp: dateInms)
                            if expireDate < Date() {
                                self.saveIsUserPurchased(flag: true)
                                return true
                            }
                        }
                    }
                }
                print(each)
            }
        }
        if (UserDefaults.standard.bool(forKey: "purchased")){
            return true
        }
        return false
    }
    
    class func saveReceiptData(data:[[String:Any]]){
        UserDefaults.standard.setValue(data, forKey:"receiptData")
        UserDefaults.standard.synchronize()
    }
    
    class func getReceiptData() -> [[String:Any]]{
        if let data = UserDefaults.standard.value(forKey:"receiptData") as? [[String:Any]] {
            return data
        }
        return [[:]]
    }
    
    
    
    static func getUserLocation() -> [String:Any] {
        if (self.isKeyPresentInUserDefaults(key:"userLocation")) {
            let savedDetails:[String:Any] = UserDefaults.standard.object(forKey: "userLocation") as! [String : Any]
            let locationDetails:[String:Any] = ["hasLocation":"1",
                                                "lat": savedDetails["lat"] ?? "NA",
                                                "long": savedDetails["long"] ?? "NA"]
            return locationDetails
        } else {
            let noAddressDetails:[String:Any] = ["hasLocation":"0",
                                                 "lat":"NA",
                                                 "long":"NA"]
            return noAddressDetails
        }
    }
    
    static func saveUserLocation(userLocationDetails:[String:Any]){
        let locationDetails = ["lat": userLocationDetails["lat"],
                               "long": userLocationDetails["long"]]
        UserDefaults.standard.set(locationDetails, forKey:"userLocation")
        UserDefaults.standard.synchronize()
    }
    
    static func saveFeaturLocation(data: Location){
        
        let locationAddr = ["name": data.name,
                            "address": data.fullText,
                            "lat": data.latitude,
                            "lng": data.longitude,
                            "city": data.city,
                            "state": data.state,
                            "country": data.country,
                            "secondaryText": data.secondaryText,
                            "description": data.locationDescription] as [String : Any]
        
        UserDefaults.standard.set(locationAddr, forKey: "featureLocation")
        if (data.latitude != 0) || (data.longitude != 0) {
            self.storeIsFeatureExist(isExist: true)
            self.saveRecentPassportLoc(data: data)
        }
        else {
            self.storeIsFeatureExist(isExist: false)
        }
        UserDefaults.standard.synchronize()
    }
    
    
    static func getFeatureLocation() -> Location {
        var locAddr  = Location(data: [:])
        if let featureLoc = UserDefaults.standard.object(forKey: "featureLocation") as? [String:Any]{
            locAddr = Location.init(data: featureLoc )
        }
        return locAddr
    }
    
    
    static func saveOtherLocations(data: [Location]){
        var otherLocationss = [[String:Any]]()
        for each in data {
            otherLocationss.append(["name": each.name,
                                    "address": each.fullText,
                                    "lat": each.latitude,
                                    "lng": each.longitude,
                                    "city": each.city,
                                    "state": each.state,
                                    "country": each.country,
                                    "secondaryText": each.secondaryText,
                                    "description": each.locationDescription])
            print(each)
            
        }
        UserDefaults.standard.set(otherLocationss, forKey: "otherLocations")
        UserDefaults.standard.synchronize()
        
    }
    
    static func saveRecentPassportLoc(data: Location){
        var recentLocations = [[String:Any]]()
        let locationAddr = ["name": data.name,
                            "address": data.fullText,
                            "lat": data.latitude,
                            "lng": data.longitude,
                            "city": data.city,
                            "state": data.state,
                            "country": data.country,
                            "secondaryText": data.secondaryText,
                            "description": data.locationDescription,
                            "formattedAddress": data.fullText] as [String : Any]
        recentLocations.append(locationAddr)
        UserDefaults.standard.set(recentLocations, forKey: "recentPassportLocations")
        UserDefaults.standard.synchronize()
        
    }
    
    
    static func getRecentPassPortLoca() -> [Location]{
        
        var otherLocations = [Location]()
        
        if let locations = UserDefaults.standard.object(forKey: "recentPassportLocations") as? [[String:Any]]{
            for each in locations {
                otherLocations.append(Location.init(data: each ))
                
                print(each)
            }
            
        }
        return otherLocations
    }
    
    
    
    static func getOtherLocations() -> [Location] {
        var otherLocations = [Location]()
        
        if let locations = UserDefaults.standard.object(forKey: "otherLocations") as? [[String:Any]]{
            for each in locations {
                otherLocations.append(Location.init(data: each ))
                
                print(each)
            }
            
        }
        return otherLocations
    }
    
    
    
    
    static func hidePushNotifications(hidePush:Bool) {
        UserDefaults.standard.set(hidePush, forKey:"hidePush")
        UserDefaults.standard.synchronize()
    }
    
    static func isHidePush() -> Bool {
        let hidePush = UserDefaults.standard.bool(forKey: "hidePush")
        return hidePush
    }
    
    static func storeIsFeatureExist(isExist:Bool) {
        UserDefaults.standard.set(isExist, forKey:"isFeatureLoc")
        UserDefaults.standard.synchronize()
    }
    
    static func getIsFeatureExist() -> Bool {
        let showFeat = UserDefaults.standard.bool(forKey: "isFeatureLoc")
        return showFeat
    }
    
    
    static func changeRootVcInVaildToken() {
        
        //unsubscribing For Push notification topic.
        let appDelegateObj = UIApplication.shared.delegate! as! AppDelegate
        
        if(appDelegateObj.window!.rootViewController?.restorationIdentifier != "navController") {
            appDelegateObj.unsubscribeForPushNotificatiionTopic()
            
            UserLoginDataHelper.removeUserToken()
            UserLoginDataHelper.removeUserDefaultsData()
            Database.shared.deleteUserData()
            
            Helper.removeDataFromCouch()
            MQTT.sharedInstance.disconnectMQTTConnection()
            LNHELPER().removeAllSheduledNotifications()
            
            Couchbase.sharedInstance.deleteDatabase()
            
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            appDelegateObj.window?.rootViewController = storyBoard.instantiateViewController(withIdentifier: "navController")
        }
    }
    
    //removing matches data from database.
    static func removeDataFromCouch() {
        
        let matchesDocumentVm = MatchesDocumentViewModel(couchbase: Couchbase())
        guard  let docId = UserDefaults.standard.object(forKey:"userMatchData/") as? String else {return}
        matchesDocumentVm.removeDataFromDocument(withMatchDocumentId: docId)
        UserDefaults.standard.removeObject(forKey: "userMatchData/")
        UserDefaults.standard.synchronize()
    }
    
    
    static func storeIPDetails(ipdetails:[String:Any]) {
        UserDefaults.standard.set(ipdetails, forKey:SyUserdefaultKeys.storIpDetails)
        UserDefaults.standard.synchronize()
    }
    
    static func fetchIpDetails() {
        if let url = URL(string: "http://www.geoplugin.net/json.gp") {
            do {
                let contents = try String(contentsOf: url)
                
                if let data = contents.data(using: String.Encoding.utf8) {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]{
                            Helper.storeIPDetails(ipdetails:json)
                        }
                    } catch {
                        print("Something went wrong")
                    }
                }
            } catch {
                // contents could not be loaded
            }
        } else {
            // the URL was bad!
        }
    }
    
    static func getUserIP() -> String {
        if (self.isKeyPresentInUserDefaults(key:SyUserdefaultKeys.storIpDetails)) {
            let savedDetails:[String:Any] = UserDefaults.standard.object(forKey: SyUserdefaultKeys.storIpDetails) as! [String : Any]
            let userIp = savedDetails["geoplugin_request"]
            return userIp as! String
        } else {
            //default
            return "169.254.66.30"
        }
    }
    
    
    class func isPhoneNumValidForFinal(phoneNumber: String , code:String) -> Bool{
        
        
        let countrydet = CountriesData()
        let selectedCountry = countrydet.selectedCountryDetails(code:code)
        var maximumSelectedCountryCount = 10
        var minimumSelectedCountryCount = 5
        
        
        if let countInString = selectedCountry["max_digits"] as? String {
            if let count = Int(countInString) {
                maximumSelectedCountryCount = count
            }
        }
        
        if let countInString = selectedCountry["min_digits"] as? String {
            if let count = Int(countInString) {
                minimumSelectedCountryCount = count
            }
        }
        
        if(maximumSelectedCountryCount == phoneNumber.count) {
            return true
        } else if minimumSelectedCountryCount > phoneNumber.count {
            return false
        }
        return false
    }
    
    static func getDeviceLocation() -> Location {
        let data = Location.init(data: [:])
        if (self.isKeyPresentInUserDefaults(key:SyUserdefaultKeys.storIpDetails)) {
            let savedDetails:[String:Any] = UserDefaults.standard.object(forKey: SyUserdefaultKeys.storIpDetails) as! [String : Any]
            let userIp = savedDetails["geoplugin_request"]
            
            data.city = savedDetails["geoplugin_city"] as! String
            data.state = savedDetails["geoplugin_regionName"] as! String
            data.country = savedDetails["geoplugin_countryName"] as! String
            if let latitude = savedDetails["geoplugin_latitude"] as? Double {
                data.latitude = latitude
            }
            
            if let latitude = savedDetails["geoplugin_latitude"] as? String {
                data.latitude = Double(latitude) as! Double
            }
            
            if let longitude = savedDetails["geoplugin_longitude"] as? Double {
                data.longitude = longitude
            }
            if let longitude = savedDetails["geoplugin_longitude"] as? String {
                data.longitude = Double(longitude) as! Double
            }
            
        }
        return data
    }
    
    /// validate phone number
    ///
    /// - Parameters:
    ///   - phoneNumber: phoneNumber
    ///   - code: countryCode
    /// - Returns: true or false
    class func isPhoneNumValid(phoneNumber: String , code:String) -> Bool{
        
        
        let countrydet = CountriesData()
        let selectedCountry = countrydet.selectedCountryDetails(code:code)
        var maximumSelectedCountryCount = 10
        
        if let count = selectedCountry["max_digits"] as? String {
            maximumSelectedCountryCount = Int(count) ?? maximumSelectedCountryCount
        }
        if let count = selectedCountry["max_digits"] as? Int {
            maximumSelectedCountryCount = count
        }
        
        if(phoneNumber.count < maximumSelectedCountryCount) {
            return true
        }
        return false
    }
    static func openNewMatchChat() -> Bool {
        return UserDefaults.standard.bool(forKey: SyUserdefaultKeys.openChat)
    }
    
    static func updateNewMatchChatDetails(isOpenChat:Bool) {
        UserDefaults.standard.set(isOpenChat, forKey: SyUserdefaultKeys.openChat)
        UserDefaults.standard.synchronize()
    }
    
    static func getIpAddresses() -> [String] {
        var addresses = [String]()
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return [] }
        guard let firstAddr = ifaddr else { return [] }
        
        // For each interface ...
        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let flags = Int32(ptr.pointee.ifa_flags)
            let addr = ptr.pointee.ifa_addr.pointee
            
            // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
            if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                        let address = String(cString: hostname)
                        addresses.append(address)
                    }
                }
            }
        }
        
        freeifaddrs(ifaddr)
        return addresses
    }
    
    
    
    //Multiple ColourText
    class func getAttributedText(text: String,subText:String,color:UIColor) -> NSMutableAttributedString{
        let range = (text.lowercased() as NSString).range(of: subText.lowercased())
        let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(kCTForegroundColorAttributeName as NSAttributedStringKey, value: color , range: range)
        return attributedString
    }
    
    
    
    static func showAlertMessageViewFromTopNavBar(showOnView:UIView,message:String) {
        
        let labelForMessage = UILabel(frame:CGRect(x: 0, y:20, width:showOnView.frame.size.width, height: 0))
        labelForMessage.text = message
        labelForMessage.textColor = UIColor.white
        labelForMessage.textAlignment = .center
        labelForMessage.backgroundColor = .red
        labelForMessage.numberOfLines = 0
        labelForMessage.minimumScaleFactor = 0.5
        labelForMessage.font = UIFont.systemFont(ofSize: 15)
        labelForMessage.drawText(in: CGRect(x: 20, y: 0, width:showOnView.frame.size.width-40, height:50))
        showOnView.addSubview(labelForMessage)
        labelForMessage.layoutIfNeeded()
        
        UIView.animate(withDuration:0.6 , animations: {
            let newFrame  = CGRect(x: 0, y:20, width:showOnView.frame.size.width, height:40)
            labelForMessage.frame = newFrame
        }, completion: {
            (finshed) in
            if(finshed) {
                // change to desired number of seconds (in this case 1 seconds)
                let when = DispatchTime.now() + 2
                DispatchQueue.main.asyncAfter(deadline: when){
                    // your code with delay
                    UIView.animate(withDuration:0.6, animations: {
                        let newFrame  = CGRect(x: 0, y:20, width:showOnView.frame.size.width, height:0)
                        labelForMessage.frame = newFrame
                    }, completion:{ (finished) in
                        if(finished) {
                            labelForMessage.removeFromSuperview()
                        }
                    })
                }
            }
        })
    }
    
    static func showProgressIndicator(withMessage:String) {
        //UIColor(rgb: 0x05B2AE)
        KRProgressHUD
            .set(style: .custom(background:  UIColor.white, text: Colors.PrimaryText, icon: .white))
            .set(maskType: KRProgressHUDMaskType.black)
            .show(withMessage: withMessage)
        KRProgressHUD.set(activityIndicatorViewStyle: .gradationColor(head: Colors.AppBaseColor, tail: Colors.progressTail))
        
    }
    
    static func showProgressWithoutBackg(withMessage:String) {
        //UIColor(rgb: 0x05B2AE)
        KRProgressHUD
            .set(style: .custom(background: UIColor.clear, text: .white, icon: .white))
            .set(maskType: .white)
            .show(withMessage: withMessage)
        KRProgressHUD.set(activityIndicatorViewStyle: .gradationColor(head: Colors.AppBaseColor, tail: Colors.progressTail))
        //  KRProgressHUD.iconViewSize = CGSize(width: 100, height: 100)
        
    }
    
    static func hideProgressIndicator() {
        KRProgressHUD.dismiss()
    }
    
    static func showCoinsDeductPopup(flag: Bool,popUpFor: CoinsPopupFor){
        switch popUpFor {
        case .forBoost:
            UserDefaults.standard.set(flag, forKey: "dontShowForBoost")
            UserDefaults.standard.synchronize()
            break
        case .forChat:
            UserDefaults.standard.set(flag, forKey: "dontShowForChat")
            UserDefaults.standard.synchronize()
            break
        case .forDate:
            UserDefaults.standard.set(flag, forKey: "dontShowForDate")
            UserDefaults.standard.synchronize()
            break
        case .forSuperLike:
            UserDefaults.standard.set(flag, forKey: "dontShowForSuperLike")
            UserDefaults.standard.synchronize()
            break
        case .forAll:
            UserDefaults.standard.set(flag, forKey: "dontShowForBoost")
            UserDefaults.standard.set(flag, forKey: "dontShowForChat")
            UserDefaults.standard.set(flag, forKey: "dontShowForDate")
            UserDefaults.standard.set(flag, forKey: "dontShowForSuperLike")
            UserDefaults.standard.synchronize()
            
            
        }
        
    }
    
    static func isShowCoinsDeductPopup(popUpFor: CoinsPopupFor) -> Bool{
        
        switch popUpFor {
        case .forBoost:
            let status = UserDefaults.standard.bool(forKey: "dontShowForBoost")
            return status
            
        case .forChat:
            let status = UserDefaults.standard.bool(forKey: "dontShowForChat")
            return status
            
        case .forDate:
            let status = UserDefaults.standard.bool(forKey: "dontShowForDate")
            return status
            
        case .forSuperLike:
            let status = UserDefaults.standard.bool(forKey: "dontShowForSuperLike")
            return status
        case .forAll:
            return true
            
        }
        
        
        
    }
    
    
    
    
    
    
    /// validate phone number
    ///
    /// - Parameters:
    ///   - phoneNumber: phoneNumber
    ///   - code: countryCode
    /// - Returns: true or false
//    class func isPhoneNumValid(phoneNumber: String , code:String) -> Bool{
//        let phoneUtil = NBPhoneNumberUtil()
//        do {
//            ///mobileNumber Validation
//            let phoneNumber = try phoneUtil.parse(phoneNumber, defaultRegion: VNHCountryPicker.dialCode(code:code).code)
//            let validPhoneNumber: Bool = phoneUtil.isValidNumber(phoneNumber)
//
//            return validPhoneNumber
//        }
//        catch let error as NSError {
//            print(error.localizedDescription)
//            return false
//        }
//    }
    //Text is number
    static func isNumber(text:String) -> Bool {
        return !text.isEmpty && text.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    
    static func isChatScreenOpened() -> Bool {
        return UserDefaults.standard.bool(forKey:"chatScreenOpened")
    }
    
    
    static func updateChatOpenStatus(isChatOpen:Bool) {
        UserDefaults.standard.set(isChatOpen, forKey:"chatScreenOpened")
        UserDefaults.standard.synchronize()
    }
    
    //Text is Name
    class func isValidName(text:String) -> Bool {
        
        var returnValue = true
        let mobileRegEx = "^[a-zA-Z]+$"               //"[A-Za-z]{3}"  // {3} -> at least 3 alphabet are compulsory.
        
        do {
            let regex = try NSRegularExpression(pattern: mobileRegEx)
            let nsString = text as NSString
            let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    /// Validate Email address
    class func isValidEmail(text:String) -> Bool {
        
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}", options: .caseInsensitive)
            return regex.firstMatch(in: text, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, text.sorted().count)) != nil
        }
        catch {
            return false
        }
    }
    
    
    
    //Handling Progress Indicator
    class func showPISuccess(delegateFor:UIViewController) {
        let progress: SuccessView = SuccessView.shared
        if progress.delegate != nil {
            progress.delegate = delegateFor as? SuccessViewDelegate
            progress.showPI()
        }
        
    }
    
    
    
    
    static var DeviceModel: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
    
    
    
    
    
    //Get UIColor from HexColorCode
    class func getUIColor(color:String) -> UIColor {
        var cString:String = color.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.sorted().count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    
    static func makeTabbarAsRootVc(forVc:UIViewController) {
        let storyBoard = UIStoryboard.init(name: "DatumTabBarControllers", bundle: nil)
        forVc.view.window?.rootViewController = storyBoard.instantiateViewController(withIdentifier: "TabBarController")
    }
    
    static func createDynamicLinkForProfile(profileDetails:Profile, completion: @escaping (String,Bool) -> ()) {
        var longLink:URL?
        // general link params
        let linkString = SERVICE.staticlinkForDeepLinking
        
        
        var userProfileId = profileDetails.userId
        userProfileId = linkString.appending(userProfileId)
        guard let link = URL(string: userProfileId) else { return }
        let components = DynamicLinkComponents(link: link, domain:"pairzy.page.link")
        
        let bundleID = "com.pipelineAI.pairzy"
        // iOS params
        let iOSParams = DynamicLinkIOSParameters(bundleID: bundleID)
        iOSParams.fallbackURL = URL.init(string: "https://itunes.apple.com/us/app/datum-dating-and-chatting-app/id931927850?ls=1&mt=8")
        //
        components.iOSParameters = iOSParams
        
        let androidParams = DynamicLinkAndroidParameters(packageName:"com.datum.com")
        androidParams.fallbackURL = URL.init(string:"https://play.google.com/store/apps/details?id=com.datum.com")
        
        components.androidParameters = androidParams
        
        // social tag params
        let socialParams = DynamicLinkSocialMetaTagParameters()
        socialParams.title = "See Profile Detail"
        socialParams.descriptionText = "Pairzy is a pair-making app for everyone by everyone!."
        
        
        if  let profilePic:String = profileDetails.profilePicture {
            if(profilePic.count > 5) {
                socialParams.imageURL = URL.init(string: profilePic)
            }
        }
        
        components.socialMetaTagParameters = socialParams
        longLink = components.url
        print(longLink?.absoluteString ?? "")
        
        components.shorten { (shortURL, warnings, error) in
            // Handle shortURL.
            if let error = error {
                print(error.localizedDescription)
                completion("failed to create link",false)
                return
            }
            completion((shortURL?.absoluteString)!,true)
        }
    }
}

extension UIColor {
    convenience init(rgb: UInt) {
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

