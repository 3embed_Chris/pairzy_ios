//
//  Extensions.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 09/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit


class Extensions: NSObject {
    
}

extension String
{
    /// Encode to Base64
    /// - Returns: Returns Encoded String
    func encodeToBase64() -> String {
        
        var res:String? = ""
        let utf = self.data(using: String.Encoding.utf8)
        res = utf?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue:0))
        return res!
    }
    
    /// To create gif url without any modification
    ///
    /// - Returns: gif url
    func makeOriginalGifUrl() -> String{
        let endIndex = self.index(self.endIndex, offsetBy: -3)
        var truncated = self.substring(to: endIndex)
        truncated = truncated + "gif"
        //        let gifString = truncated.replacingOccurrences(of: "upload/", with: "upload/w_200,h_200,c_fill/")
        return truncated
    }
    
    /// To make gif url of reduced quality
    ///
    /// - Returns: return gif url
    func makeGifUrl() -> String {
        let originalGifString = self.makeOriginalGifUrl()
        let gifString = originalGifString.replacingOccurrences(of: "upload/", with: "upload/vs_20,e_loop/")
        //        let gifString = truncated.replacingOccurrences(of: "upload/", with: "upload/w_200,h_200,c_fill/")
        return gifString
    }
    
    
    /// TO make imgae url from any type of URL
    ///
    /// - Returns: image url
    func makeThumbnailUrl() -> String{
        let endIndex = self.index(self.endIndex, offsetBy: -3)
        var truncated = self.substring(to: endIndex)
        truncated = truncated + "jpg"
        return truncated
    }
    
    
    
    
    
    
    
}

extension UIView
{
    func  showErrorMessage(titleMsg: String, andMessage: String){
        
        let alertController = UIAlertController(title: titleMsg, message: andMessage, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: StringConstants.ok(), style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        let appDelegateObj = UIApplication.shared.delegate! as! AppDelegate
        // show the alert
        var controller = appDelegateObj.window!.rootViewController
        if (appDelegateObj.window!.rootViewController as? UINavigationController) != nil
        {
            let navControllr = appDelegateObj.window!.rootViewController as! UINavigationController
            controller = navControllr.visibleViewController
        }
        else
        {
            controller = appDelegateObj.window!.rootViewController
        }
        
        controller?.present(alertController, animated: true, completion: nil)
    }
    
    /// view animation for single tap to like a post
    ///
    /// - Parameters:
    ///   - changeImage: image to change
    ///   - view: image view on which image view change
    func singleTapLikeButtonAnimation(changeImage: UIImage, view: UIImageView){
        UIView.animate(withDuration: 0.2, delay: 0, options: [.allowUserInteraction, .curveEaseIn], animations: {() -> Void in
            self.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        }, completion: {(_ finished: Bool) -> Void in
            view.image = changeImage
            UIView.animate(withDuration: 0.2, delay: 0, options: [.allowUserInteraction, .curveEaseOut], animations: {() -> Void in
                self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: {(_ finished: Bool) -> Void in
                
            })
        })
    }
    
    
    /// To make animation for favourite or like button
    func popUpDoubletapFavouritAnimation() {
        //        let animation = CABasicAnimation(keyPath: "transform.scale")
        //        animation.duration = 0.3
        //        animation.repeatCount = 1
        //        animation.fromValue = 1.0
        //        animation.toValue = 0.5
        //        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        //        self.layer.add(animation, forKey: "zoom")
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .allowUserInteraction, animations: {() -> Void in
            self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.alpha = 1.0
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {() -> Void in
                self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3, delay: 0, options: .allowUserInteraction, animations: {() -> Void in
                    self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    self.alpha = 0.0
                }, completion: {(_ finished: Bool) -> Void in
                    self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                })
            })
        })
    }
    
    
    
    
    
    
    
    
}

extension UIColor{
    
    convenience init(hex: Int, alpha: Double = 1.0) {
        self.init(red: CGFloat((hex>>16)&0xFF)/255.0, green:CGFloat((hex>>8)&0xFF)/255.0, blue: CGFloat((hex)&0xFF)/255.0, alpha:  CGFloat(255 * alpha) / 255)
    }
    
}

