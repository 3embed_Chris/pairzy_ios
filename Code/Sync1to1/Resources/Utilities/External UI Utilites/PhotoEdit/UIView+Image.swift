//
//  UIView+Image.swift
//  Photo Editor
//
//  Created by Mohamed Hamed on 4/23/17.
//  Copyright © 2017 Mohamed Hamed. All rights reserved.
//

import UIKit

extension UIView {
    /**
     Convert UIView to UIImage
     */
    func toImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshotImageFromMyView!
    }
    
    
    //MARK:- View setups
    
    /// To make corner radious to a view
    ///
    /// - Parameter readious: radious of that view
    func makeCornerRadius(radius: CGFloat){
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    /// To make border width of a view
    ///
    /// - Parameters:
    ///   - width: width of border
    ///   - color: color of border
    func makeBorderWidth(width: CGFloat, color: UIColor){
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
    }
}


extension UIView {
    func applyCoinsAnimation(indexAt:Int,fromView:UIView,coins:String) {
        let bubbleImageView = UIImageView(image: UIImage(named: "coin2"))
        let size: CGFloat = 20
        bubbleImageView.frame = CGRect(x: self.frame.size.width - 70, y: fromView.frame.origin.y, width: size, height: size)
        self.addSubview(bubbleImageView)
        
        let zigzagPath = UIBezierPath()
        let oX: CGFloat = bubbleImageView.frame.origin.x
        let oY: CGFloat = bubbleImageView.frame.origin.y
        
        // the moveToPoint method sets the starting point of the line
        zigzagPath.move(to: CGPoint(x: oX, y: oY))
        var ey: CGFloat = 0
        var ex: CGFloat = 0
        
        switch indexAt {
        case 6:
            ex = oX + 27
            ey = oY - 56 * 2
        case 5:
            ex = oX + 20
            ey = oY - 60 * 2
        case 4:
            ex = oX + 3
            ey = oY - 80 * 2
        case 3:
            ex = oX - 34
            ey = oY - 70 * 2
        case 2:
            ex = oX - 20
            ey = oY - 100 * 2
        case 1:
            ex = oX - 3
            ey = oY - 40 * 2
        default:
            break
        }
        
        let coinsText = UILabel()
        if indexAt == 5 {
            coinsText.text = coins
            coinsText.font = UIFont (name: CircularAir.Light, size: CGFloat(0.1))
            coinsText.textColor = Helper.getUIColor(color:"efd101")
            var frameForCoins: CGRect = bubbleImageView.frame
            frameForCoins.origin.x = frameForCoins.origin.x + 100
            frameForCoins.size.width = 120
            frameForCoins.size.height = 40
            coinsText.frame = frameForCoins
            self.addSubview(coinsText)
        }
        
//        // add the end point and the control points
       zigzagPath.addLine(to: CGPoint(x: ex, y: ey))
//
       CATransaction.begin()
        CATransaction.setCompletionBlock({
            // transform the image to be 1.3 sizes larger to
            // give the impression that it is popping
            bubbleImageView.removeFromSuperview()
            coinsText.removeFromSuperview()
        })
        
        var animationTime = 0.5
        if indexAt == 5 {
            animationTime = 0.8
            coinsText.font = UIFont (name: CircularAir.Light, size: CGFloat(0.2))
            coinsText.alpha = 0.2
            UIView.animate(withDuration:0.5, animations: {
                coinsText.alpha = 1.0
                coinsText.font = UIFont (name: CircularAir.Bold, size: CGFloat(40))
            })
        }
        
        
        let pathAnimation = CAKeyframeAnimation(keyPath: "position")
        pathAnimation.duration = animationTime
        pathAnimation.path = zigzagPath.cgPath
        pathAnimation.fillMode = kCAFillModeForwards
        pathAnimation.isRemovedOnCompletion = false
        bubbleImageView.layer.add(pathAnimation, forKey: "movingAnimation")
        coinsText.layer.add(pathAnimation, forKey: "movingAnimation")
        CATransaction.commit()
    }
}
