//
//  UIImageViewExtenstions.swift
//  Photo Editor
//
//  Created by Mohamed Hamed on 5/10/17.
//
//

import Foundation
import UIKit
import Kingfisher


extension UIImageView {
    
    func alphaAtPoint(_ point: CGPoint) -> CGFloat {
        
        var pixel: [UInt8] = [0, 0, 0, 0]
        let colorSpace = CGColorSpaceCreateDeviceRGB();
        let alphaInfo = CGImageAlphaInfo.premultipliedLast.rawValue
        
        guard let context = CGContext(data: &pixel, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: alphaInfo) else {
            return 0
        }
        
        context.translateBy(x: -point.x, y: -point.y);
        
        layer.render(in: context)
        
        let floatAlpha = CGFloat(pixel[3])
        
        return floatAlpha
    }
    
    /// To set Image on a image view
    ///
    /// - Parameters:
    ///   - imageUrl: Url of image to assign on an image view
    ///   - defaultImage: default image view to set on it
    func setImageOn(imageUrl: String?, defaultImage: UIImage){
        if let url = imageUrl {
            self.kf.indicatorType = .activity
            self.kf.indicator?.startAnimatingView()
            if url.count>0{
                let url = URL(string:url)!

            //            DispatchQueue.global().async {
            self.kf.setImage(with: url, placeholder: defaultImage, options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
                    self.kf.indicator?.stopAnimatingView()
            })
        }

            //            }
        }else{
            self.image = defaultImage
        }
    }
 
    
//    /// To set Image on a image view
//    ///
//    /// - Parameters:
//    ///   - imageUrl: Url of image to assign on an image view
//    ///   - defaultImage: default image view to set on it
//    func setImageOn(imageUrl: String?, defaultImage: UIImage){
//        if let url = imageUrl {
//            self.kf.indicatorType = .activity
//            self.kf.indicator?.startAnimatingView()
//            //            DispatchQueue.global().async {
//            self.kf.setImage(with: URL(string:url), placeholder: defaultImage, options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
//                self.kf.indicator?.stopAnimatingView()
//            })
//            //            }
//        }else{
//            self.image = defaultImage
//        }
//    }
}


    
