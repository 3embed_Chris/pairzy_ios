//
//  TextColorChangeForBackgrourndLabel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 03/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class TextColorChangeForBackgrourndLabel: UILabel {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func draw(_ rect: CGRect)
    {
        let shadowOffset = self.shadowOffset
        let textColor = self.textColor
        
        let context = UIGraphicsGetCurrentContext()
        context!.setLineWidth(1)
        context!.setLineJoin(CGLineJoin.round)
        
        context!.setTextDrawingMode(CGTextDrawingMode.stroke);
        self.textColor = UIColor.black
        super.drawText(in: rect)
        
        context!.setTextDrawingMode(CGTextDrawingMode.fill)
        self.textColor = textColor
        self.shadowOffset = CGSize(width: 0, height: 0)
        super.drawText(in: rect)
        
        self.shadowOffset = shadowOffset
    }

}
