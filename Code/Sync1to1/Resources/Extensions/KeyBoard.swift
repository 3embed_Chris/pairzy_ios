//
//  KeyBoardExtention.swift
//  UFly
//
//  Created by 3Embed on 06/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension UIViewController {
    
    
    /// Observer for keyboard
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
     /// remove Observer
    func removeObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //Mark:- Keyboard ANimation
    @objc func keyboardWillShow(_ notification: NSNotification){
        // Do something here
        
        let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        self.view.setNeedsLayout()
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: [UIViewAnimationOptions(rawValue: UInt(curve))], animations: {
            
            let info = notification.userInfo!
            let inputViewFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let screenSize: CGRect = UIScreen.main.bounds
            var frame = self.view.frame
            frame.size.height = screenSize.height - inputViewFrame.size.height
            self.view.frame = frame
           
            self.view.layoutIfNeeded()
        }, completion: nil)

    }
    
    @objc func keyboardWillHide(_ notification: NSNotification){
        let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        self.view.setNeedsLayout()
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: [UIViewAnimationOptions(rawValue: UInt(curve))], animations: {
            let screenSize: CGRect = UIScreen.main.bounds
            var frame = self.view.frame
            frame.size.height = screenSize.height
            self.view.frame = frame
            self.view.layoutIfNeeded()
        }, completion: nil)
        

    }
}
