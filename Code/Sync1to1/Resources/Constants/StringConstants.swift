//
//  StringConstants.swift
//  Datum
//
//  Created by 3 Embed on 18/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

extension String {
    var Localized:String {
        return OneSkyOTAPlugin.localizedString(forKey: self, value: "", table: nil)
    }
}

//class func stringConstants() -> String {  return StringConstants()
class StringConstants {
    
    //LandingVC
    class func landingScreen1() ->String {
        return "Discover new and interesting people nearby.".Localized
    }
    class func landingScreen2() ->String {
        return "Swipe right to like someone or swipe left to pass.".Localized
    }
    class func landingScreen3() ->String {
        return "If they also swipe right then its a match!.".Localized
    }
    class func landingScreen4() ->String {
        return "Only people you've matched with can message you.".Localized
    }
    class func loginWithFB() ->String {
        return "Login with Facebook".Localized
    }
    class func loginWithPh() ->String {
        return "Login with Phone Number".Localized
    }
    class func loginWithEmail() ->String {
        return "Login with Email".Localized
    }
    class func reconnecting() -> String{
        return "Reconnecting...".Localized
    }
    
    class func weDontPostAnything() ->String {
        return "We don't post anything to Facebook".Localized
    }
    
    class func BySigning() ->String {
        return "By Signing in. you agree to our Terms of Service and Privacy Policy".Localized
    }
    class func termsOfService() ->String {
        return "Terms of Service".Localized
    }
    class func privacyPolicy() ->String {
        return "Privacy Policy".Localized
    }
    class func fetchingDetail() ->String {
        return "Fetching details ..".Localized
    }
    class func cameraNeccesary() -> String{
        return "Camera access is absolutely necessary to use this app".Localized
    }
    
    //phoneNumberVC
    class func enterYour() -> String{
        return "Enter your".Localized
    }
    class func number() -> String{
        return "Number".Localized
    }
    class func phoneNumber() -> String{
        return "Phone Number".Localized
    }
    class func sent() -> String{
        return "Sent!".Localized
    }
    class func phAlredyExist() -> String{
        return "Mobile number already exist in the database.".Localized
    }
    //verify code VC
    class func verification() -> String{
        return "Verification".Localized
    }
    class func didnotGetCode() -> String{
        return "I didn't get a code".Localized
    }
    class func enterOtp() -> String{
        return "Enter verification code".Localized
    }
    class func mobileNumber() -> String{
        return "Mobile number".Localized
    }
    class func resendCode() -> String {
        return "resend code".Localized
    }
    
    class func tryAgain() -> String {
        return "Try Again".Localized
    }
    
    class func otpDoentMatch() -> String {
        return "The entered OTP doesnot match.".Localized
    }
    //email Address
    class func emailAddress() -> String {
        return "Email address".Localized
    }
    class func enterYourEmail() -> String {
        return "Enter your email address".Localized
    }
    class func pleaseEnterEmail() -> String {
        return "Please enter your email in order to recover your account later.".Localized
    }
    
    //name VC
    class func name() -> String {
        return "Name".Localized
    }
    class func enterYourName() -> String {
        return "Enter your name".Localized
    }
    class func thisIsHowInDatum() -> String {
        return "This is how it will appear in Pairzy".Localized
    }
    
    //date of birth VC
    class func birthDay() -> String {
        return "Birthday".Localized
    }
    //gender
    class func specifyYour() -> String {
        return "Specify your".Localized
    }
    
    class func gender() -> String {
        return "Gender".Localized
    }
    
    class func male() -> String {
        return "Male".Localized
    }
    class func female() -> String{
        return "Female".Localized
    }
    
    //WebVC
    class func termsAndConditions() -> String {
        return "Terms and Conditions".Localized
    }
    
    // addMedia VC
    class func myBest() -> String {
        return "My best".Localized
    }
    class func picture() -> String {
        return "Picture".Localized
    }
    class func addYourPhoto() -> String {
        return "Add your photo".Localized
    }
    class func addYourVideo() -> String {
        return "Add your video".Localized
    }
    class func change() -> String {
        return "Change".Localized
    }
    class func video() -> String {
        return "Video".Localized
    }
    class func skip() -> String {
        return "Skip".Localized
    }
    class func media() -> String {
        return "Media".Localized
    }
    
    //Join Us VC
    class func relationShip() -> String {
        return "Relationship start with".Localized
    }
    
    class func aCommunityOf() -> String {
        return "A community of people who are ready for something real.Join us.".Localized
    }
    //preferenceVC
    class func prefferNotTo() -> String {
        return "Prefer not to say".Localized
    }
    //tabBar titles
    class func discover() -> String {
        return "Singles".Localized
    }
    class func dates() -> String {
        return "Dates".Localized
    }
    class func chats() -> String {
        return "Matches".Localized
    }
    class func settings() -> String {
        return "Settings".Localized
    }
    //profileDetailVC
    class func recommendTo() -> String {
        return "RECOMMEND TO A FRIEND".Localized
    }
    class func report() -> String {
        return "Report".Localized
    }
    class func lessThanKm() -> String {
        return "less than a kilometer away".Localized
    }
    //options popup
    class func block() -> String {
        return "Block".Localized
    }
    
    class func cancel() -> String {
        return "Cancel".Localized
    }
    
    //prospects Tabs
    class func prospects() -> String {
        return "Prospects".Localized
    }
    
    class func makeMatch() -> String {
        return "Pairs".Localized
    }
    class func online() -> String {
        return "Online".Localized
    }
    class func superLikedMe() -> String {
        return "Super Liked Me".Localized
    }
    class func likesMe() -> String {
        return "Likes Me".Localized
    }
    class func recentVisitors() -> String {
        return "Recent Visitors".Localized
    }
    class func passed() -> String {
        return "Passed".Localized
    }
    class func myLikes() -> String {
        return "My Likes".Localized
    }
    class func mySuperLikes() -> String {
        return "My Super Likes".Localized
    }
    
    //prospects empty screen messages
    class func onlineMsg() -> String {
        return "Sorry no potential matches are currently online.".Localized
    }
    
    class func superLikedMeMsg() -> String {
        return "Sorry no user has super-liked you as yet. Keep your profile fully updated to improve your chances to get matched.".Localized
    }
    class func  likesMeMsg() -> String {
        return "Sorry no user has liked your profile yet. Keep your profile fully updated to improve your chances to get liked.".Localized
    }
    class func recentVisitorsMsg() -> String {
        return "Sorry you have received no profile visits yet.".Localized
    }
    class func passedMsg() -> String {
        return "Way to go! Seems like you don’t swipe left ever !".Localized
    }
    class func myLikesMsg() -> String {
        return "Oops! Seems like you have not swiped right on any profile yet. Update your preferences to get better potential matches.".Localized
    }
    class func mySuperLikesMsg() -> String {
        return "Oops! Seems like you have not used your super like power yet. Click on the little start button to super like.".Localized
    }
    
    //dateTab vc
    class func pending() -> String {
        return "Pending".Localized
    }
    class func upcoming() -> String {
        return "Upcoming".Localized
    }
    class func pastDates() -> String {
        return "Past Dates".Localized
    }
    class func pendingDateMsg() -> String {
        return "You have no pending dates".Localized
    }
    class func pleaseSchedule() -> String {
        return "Please schedule a date with one of your matches".Localized
    }
    class func upcomingDateMsg() -> String {
        return "You have no upcoming dates".Localized
    }
    class func pastDateMsg() -> String {
        return "You have no past dates".Localized
    }
    class func him() -> String {
        return "him".Localized
    }
    class func her() -> String {
        return "her".Localized
    }
    
    
    //Chat VC
    class func matches() -> String {
        return "Matches".Localized
    }
    class func activeCahts() -> String {
        return "Active Chats".Localized
    }
    class func chat() -> String {
        return "Matches".Localized
    }
    class func newsfeed() -> String {
        return "Activity".Localized
    }
    
    class func chatActivity() -> String {
        return "Chat".Localized
    }
    
    class func chatLower() -> String {
        return "chat".Localized
    }
    class func chatDefaultText() -> String {
        return "You dont have active chats".Localized
    }
    class func newMsg() -> String {
        return "New Message".Localized
    }
    class func clearChat() -> String {
        return "Clear Chat".Localized
    }
    class func blockContact() -> String {
        return "Block Contact".Localized
    }
    class func reportSpam() -> String {
        return "Report Spam".Localized
    }
    class func conversationDeleted() -> String {
        return "Conversation Deleted".Localized
    }
    
    //CreatePostVC
   
    class func CreatePost() -> String {
        return "Create Post".Localized
    }
    class func captionPlaceholderText() -> String {
        return "Write your caption here...".Localized
    }
    class func captionEmptyAlert() -> String {
        return "Please enter caption".Localized
    }
    class func PostedSuccessfully() -> String {
        return "Posted Successfully".Localized
    }
    
    //Settings
    class func myPreferences() -> String {
        return "My Preferences".Localized
    }
    class func inviteFriends() -> String {
        return "Invite Friends".Localized
    }
    class func appSettings() -> String {
        return "App Settings".Localized
    }
    class func needHelp() -> String {
        return "Need Help".Localized
    }
    class func datumPlus() -> String {
        return "Pairzy Plus".Localized
    }
    //Need Help
    class func reportIssue() -> String {
        return "Report an issue".Localized
    }
    class func makeSuggetion()  -> String {
        return "Make a suggestion".Localized
    }
    class func askQuestion()  -> String {
        return "Ask a question".Localized
    }
    
    //my preferences
    class func my() -> String {
        return "My".Localized
    }
    class func preferences() -> String {
        return "Preferences".Localized
    }
    class func location() -> String {
        return "Location".Localized
    }
    class func NA() -> String {
        return "NA".Localized
    }
    class func myCurrentLocation() -> String {
        return "My Current Location".Localized
    }
    class func age() -> String {
        return "Age".Localized
    }
    class func distance() -> String {
        return "Distance".Localized
    }
    class func height() -> String {
        return "Height".Localized
    }
    class func feet() -> String {
        return "feet".Localized
    }
    class func cm() -> String {
        return "cm".Localized
    }
    class func km() -> String {
        return "km".Localized
    }
    class func miles() -> String {
        return "miles".Localized
    }
    class func doYouWantToUpdate() -> String {
        return "Do you want to update the changes".Localized
    }
    class func updateChanges() -> String {
        return "Update Changes".Localized
    }
    class func yes() -> String {
        return "Yes".Localized
    }
    class func no() -> String {
        return "No".Localized
    }
    class func ft() -> String {
        return "ft".Localized
    }
    class func dateLower() -> String {
        return "date".Localized
    }
    //location VC
    class func results() -> String {
        return "RESULTS".Localized
    }
    class func noResults() -> String {
        return "NO RESULTS".Localized
    }
    class func locationDesabled() -> String {
        return "Location services is disabled On your app.If you turn Location Services the app will find a point on the map for you".Localized
    }
    class func youNeedToEnable() -> String {
        return "You need to enable location services in settings.".Localized
    }
    class func ok() -> String {
        return "OK".Localized
    }
    class func okLower() -> String {
        return "Ok".Localized
    }
    class func locationStored() -> String {
        return "locationStored".Localized
    }
    
    //Rescchedule
    class func planVideoDate() -> String {
        return "Plan video date".Localized
    }
    class func planAudioDate() -> String {
        return "Plan audio date".Localized
    }
    class func planInPersonDate() -> String {
        return "Plan In person date".Localized
    }
    
    class func suggestNewPlace() -> String {
        return "Suggest a new place and time to meet.will notify you if confirms.".Localized
    }
    class func pleaseScheduleDate() -> String {
        return "Please shedule the date and time.".Localized
    }
    class func reschedule() -> String {
        return "Reschedule".Localized
    }
    class func pleaseSuggestTime() -> String {
        return "Please suggest a new time.".Localized
    }
    class func suggestPlaceAndTime() -> String {
        return "Please suggest place and time.".Localized
    }
    class func dateCreated() -> String {
        return "date created".Localized
    }
    class func spendCoinsToRequest() -> String {
        return "Spend %d coins to request %@ with %@".Localized
    }
    class func confirmDate() -> String {
        return "Confirm date".Localized
    }
    class func pleaseWaitForVideo() -> String {
        return "Please wait until the scheduled time for the video call button to be enabled".Localized
    }
    class func plzWaitForAudio() -> String {
        return "Please wait until the scheduled time for the voice call button to be enabled".Localized
    }
    class func plzWaitForTime() -> String {
        return "Please wait until scheduled time for date location".Localized
    }
    
    class func videoCall() -> String {
        return "Video Call".Localized
    }
    class func voiceCall() -> String {
        return "Voice Call".Localized
    }
    
    class func getDirection() -> String {
        return "Get directions".Localized
    }
    //AppSettings
    
    class func community() -> String {
        return "Community".Localized
    }
    class func legal() -> String {
        return "Legal".Localized
    }
    class func licenses() -> String {
        return "Licenses".Localized
    }
    class func communityGuidlines() -> String {
        return "Community Guidelines".Localized
    }
    class func safetyTips() -> String {
        return "Safety Tips".Localized
    }
    class func logout() -> String {
        return "Logout".Localized
    }
    class func deleteAccount() -> String {
        return "Delete account".Localized
    }
    class func version() -> String {
        return "version".Localized
    }
    class func areYouSureDelete() -> String {
        return "Your account will be deleted and no longer visible by others.".Localized
    }
    class func areYouSureLogout() -> String {
        return "are you sure you want to log out? you will continue to be seen by compatible users in your last known location.".Localized
    }
    //addCoins vc
    class func buyCoinsLower() -> String {
        return "Buy Coins".Localized
    }
    class func coinsUnlockFeatures() -> String {
        return "Coins unlock additional features.".Localized
       // return "Coins unlock additional features like extra features and the ability to view mutual friends.".Localized
    }
    class func superLike() -> String {
        return "superlike".Localized
    }
    class func hundredCoins() -> String {
        return "100 coins".Localized
    }
    class func iamOkToSpend() -> String {
        return  "I am OK to spend %d coins".Localized
    }
    class func youNeedToSpend() -> String {
        return "You need to spend %d Coins to boost your profile".Localized
    }
    class func oopsYouDontHave() -> String {
        return "Oops!! You dont have enough coins to boost your profile.".Localized
    }
//    class func spendCoinsToLet() -> String {
//        return "Spend %d coins to let %@ know you really like %@ by super liking".Localized
//    }
    
    class func spendCoinsToLet() -> String {
        return "Spend %d coins to super like %@".Localized
    }
    class func coins() -> String {
        return "Coins".Localized
    }
    class func coisAddedTo() -> String {
        return "Coins added to your wallet.".Localized
    }
    
    class func areYouSure() -> String {
        return "Are you sure you want to purchase %@ plan?".Localized
    }
    class func coinPurchase() -> String {
        return "Coin purchase".Localized
        
    }
    
    class func coinBalance() -> String {
        return "Coin balance".Localized
    }
    class func earnFreeCoins() -> String {
        return "EARN FREE COINS".Localized
    }
    class func unlockFeat() -> String {
        return "Coins unlock features like extra features and ability to view mutual friends.".Localized
    }
    //Recent Transaction
    class func recentTrans() -> String {
        return "Recent Transactions".Localized
    }
    class func all() -> String {
        return "All".Localized
    }
    class func spent() -> String {
        return "Spent".Localized
    }
    class func bought() -> String {
        return "Bought".Localized
    }
    
    //dates popup
    class func voiceDate() -> String {
        return "Audio Date".Localized
    }
    class func inPersionDate() -> String {
        return "In Person Date".Localized
    }
    class func videoDate() -> String {
        return "Video Call Date".Localized
    }
    class func aAudioDate() -> String {
        return "a audio date".Localized
    }
    class func aVideoDate() -> String {
        return "a video date".Localized
    }
    
    //attach
    class func camera() -> String {
        return "Camera".Localized
    }
    class func camerandVideo() -> String {
        return "Photo and Video Library".Localized
    }
    class func giphy() -> String {
        return "Giphy".Localized
    }
    class func sticker () -> String {
        return "Sticker".Localized
    }
    class func doodle () -> String {
        return  "Doodle".Localized
    }
    class func contact() -> String {
        return "Contact".Localized
    }
    class func documents() -> String {
        return "Documents".Localized
    }
    //options ChatViewController
    class func deleteForEveryone() -> String {
        return "Delete for Everyone".Localized
    }
    class func deleteForMe() -> String {
        return "Delete for Me".Localized
    }
    class func view() -> String {
        return "View".Localized
    }
    class func viewProfile() -> String {
        return "View %@'s Profile".Localized
    }
    class func unMatchProfile() -> String {
        return "Unmatch %@".Localized
    }
    class func reportProfile() -> String {
        return "Report %@".Localized
    }
    class func blockProfile() -> String {
        return "Block %@".Localized
    }
    class func Offline() -> String {
        return "Offline".Localized
    }
    class func typing() -> String {
        return "Typing...".Localized
    }
    
    //screen Titles
    class func familyPlans() -> String {
        return "Family plans".Localized
    }
    class func youAndLike() -> String {
        return "You and %@ like each other".Localized
    }
    class func chatWith() -> String {
        return "Chat with ".Localized
    }
    class func with() -> String {
        return "with".Localized
    }
    
    // date picker
    class func dateOfBirth() -> String {
        return "DATE OF BIRTH".Localized
    }
    class func done() -> String{
        return "DONE".Localized
    }
    class func doneLower() ->String {
        return "Done".Localized
    }
    
    //Edit Profile
    class func dontShowAge() -> String {
        return "Don't show my age".Localized
    }
    class func dontShowDistatnce() -> String {
        return "Don't show my distance".Localized
    }
    class func other() -> String {
        return "Other".Localized
    }
    class func showInstaPhotos() -> String {
        return "Show my Instagram Photos".Localized
    }
    class func connectInstagram() -> String {
        return "Connect Instagram".Localized
    }
    class func controlProfile() -> String {
        return "Controll your profile".Localized
    }
    class func aboutYou() -> String {
        return "About You".Localized
    }
    class func email() -> String {
        return "Email".Localized
    }
    class func phone() -> String {
        return "Phone".Localized
    }
    class func myProfile() -> String {
        return "My Profile".Localized
    }
    
    class func lessThan() -> String{
        return "less than".Localized
    }
    class func myVertues() -> String {
        return "My Virtues".Localized
    }
    class func moments() -> String {
        return "Moments".Localized
    }
    class func myVitals() -> String {
        return "My Vitals".Localized
    }
    class func myVices() -> String {
        return "My Vices".Localized
    }
    
    
    //profileInfo cell
    class func aKmAway() -> String {
        return "a mile away".Localized
    }
    class func kmsAway() -> String {
        return "miles away".Localized
    }
    
    //Instagram
    class func instagramPhotos() -> String {
        return "%d Instagram Photos".Localized
    }
    class func instagramPhoto() -> String {
        return "%d Instagram Photos".Localized
    }
    class func shareYourInstaTo() -> String {
        return "Share your Instagram photos, too".Localized
    }
    
    class func unBlock() -> String {
        return "Unblock".Localized
    }
    
    //Alert
    class func spendSomeCoins() -> String {
        return "Spend some coins for sending  message".Localized
    }
    class func noCamera() -> String {
        return "Camera is not available in your device".Localized
    }
    
    class func message() -> String {
        return "Message".Localized
    }
    class func acceptEULA() -> String {
        return "Please check on the checkbox to accept the EULA".Localized
    }
    class func coinsForMsg() -> String {
        return "200 coins for a message.  ".Localized
    }
    class func iFoundAGirl() -> String {
        return "I found a match for you. Get on Pairzy and let me know what you think ! \n".Localized
    }
    
    class func HomeProfileSearch() -> String {
        return ""
    }
    
    //"Searching the matches around you.."
    // class func HomeNoProfiles() -> String {  return "oops! No new matches around you..passport to a different location ?"
    
    class func HomeNoProfiles() -> String {
        return "There's no one around you. Expand your Discovery settings to see more people".Localized
    }
    class func ThanksForReporting() -> String {
        return "Thanks for reporting".Localized
    }
    class func YourFeedBack() -> String {
        return "Your feedback is important in helping us keep there Pairzy community safe.".Localized
    }
    class func RemoveVideo() -> String {
        return  "Are you sure you want to remove video.".Localized
    }
    class func RemoveImage() -> String {
        return "Are you sure you want to remove image.".Localized
    }
    class func ThanksForFeedBack() -> String {
        return "Thank you for your feedback!".Localized
    }
    class func ThanksForRating() -> String {
        return "Thank you for rating the date".Localized
    }
    class func RateTheDate() -> String {
        return "Please rate the date.".Localized
    }
    class func Blocked() -> String {
        return "Blocked".Localized
        
    }
    class func UnBlocked() -> String {
        return "UnBlocked".Localized
    }
    class func Recomend() -> String {
        return "Recommend to a Friend".Localized
    }
    class func DeletePost() -> String {
        return "Delete Post".Localized
    }
    class func reportAsSpam() -> String {
        return "It's Spam".Localized
    }
    class func reportAsInappropriate() -> String {
        return "It's Inappropriate".Localized
    }
    class func DeleteChat() -> String {
        return "Delete Chat".Localized
    }
    class func SpendSomeCoin() -> String {
        return "Spend some coins for chat before match".Localized
    }
    class func Passport() -> String {
        return "Passport".Localized
    }
    class func msgForOutOfLikes() -> String {
        return "Don't want to wait? Go unlimited!".Localized
    }
    class func DiscoverySetting() -> String {
        return "DISCOVERY SETTINGS".Localized
    }
    class func spendCoinsSuperLike() -> String {
        return "Give away coins to superlike".Localized
    }
    
    class func getChanceSuperLike() -> String{
        return "Get a chance to get matched with your potential match".Localized
    }
    
    class func CoinsGotOver() -> String {
        return "Oops! You are out of coins !".Localized
    }
    class func NoEnoughCoins() -> String {
        return "Sorry You dont have enough coins to ".Localized
    }
    class func BuyCoins() -> String {
        return "BUY COINS".Localized
    }
    
    class func spendCoinsForChat() -> String {
        return "Give away coins to connect".Localized
    }
    class func getChanceChat() -> String {
        return "Get a chance to connect without being matched with your potential match".Localized
    }
    class func spendSomeCoinsForDate() -> String {
        return "Give away coins to date".Localized
    }
    class func getChanceTodate() -> String {
        return "Get a chance to date with your potential match".Localized
    }
    class func titleForCalenderEvent() -> String {
        return "Pairzy".Localized
    }
    class func congrats() -> String {
        return "Congratulations".Localized
    }
    class func outOfLikes() -> String {
        return "Out of likes!!".Localized
    }
    
    
    //  ActionSheetButtonNames
    class func actionSheetCancelText() -> String {
        return "Cancel".Localized
    }
    class func resetPasswordAlertTitle() -> String {
        return "RESET PASSWORD".Localized
    }
    class func resetPasswordAlertMessage() -> String {
        return "Select option to reset password".Localized
    }
    class func resetPasswordAlertPhoneNumber() -> String {
        return "Phone Number".Localized
    }
    
    class func continueM() -> String {
        return "continue".Localized
    }
    class func chatCoinsconsumeMessage() -> String {
        return "if you chat before match it will consume coins.".Localized
    }
    
    class func reportThanks() -> String {
        return "Thanks for reporting".Localized
    }
    class func reportFeedBack() -> String {
        return "Your feedback is important in helping us keep the Pairzy community safe.".Localized
    }
    class func unamtch() -> String {
        return "Unmatch".Localized
    }
    
    class func confirmDelete() -> String {
        return "Are you sure want to delete?".Localized
    }
    class func confirmBlock() -> String {
        return "Are you sure want to block?".Localized
    }
    class func confirmUnBlock() -> String {
        return "Are you sure want to unblock?".Localized
    }
    class func confirmUnmatch() -> String {
        return "Are you sure want to unmatch?".Localized
    }
    
    class func noCoinsForSuperLike() -> String {
        return "Sorry You dont have enough coins to superLike.".Localized
    }
    class func noCoinsForDate() -> String {
        return "Sorry You dont have enough coins to create date.".Localized
    }
    class func noCoinsForMessage() -> String {
        return "Sorry You dont have enough coins to send message.".Localized
    }
    class func oops() -> String {
        return ""  //"Oops"
    }
    class func Oops() -> String {
        return "Oops".Localized
    }
    
    class func success() -> String {
        return  "Success".Localized
    }
    class func planPurchased() -> String {
        return "Plan purchased successfully".Localized
    }
    
    //defaultScreenMessages
    class func Now_Available_Title () -> String {
        return "No one is available right now.".Localized
    }
    class func Now_Available_Message() -> String {
        return "Please look at the video profiles to find a potential match.".Localized
    }
    
    class func Now_Missed_Message() -> String {
        return "You have no missed dates. ".Localized
    }
    class func Future_Pending_Message  () -> String {
        return "You have no pending dates.".Localized
    }
    class func Future_scheduled_Message() -> String {
        return "You have no scheduled dates. Please schedule a date with one of your matches.".Localized
    }
    class func Past_Title() -> String {
        return "You have no past dates.".Localized
    }
    class func Upcoming_Title() -> String {
        return "You have no Upcoming dates.".Localized
    }
    class func Past_Message() -> String {
        return "Please schedule a date with one of your matches.".Localized
    }
    
    //  alertMessage
    class func userBannedFromAdmin() -> String {
        return "Your profile has been banned by the app administrator , please contact our support for any further queries".Localized
    }
    
    // progressIndicatorMessage
    class func Login() -> String {
        return "Logging in...".Localized
    }
    class func Loading() -> String {
        return "Loading ...".Localized
    }
    class func Verifying() -> String {
        return "Verifying...".Localized
    }
    class func signingUp() -> String {
        return "Creating Account".Localized
    }
    class func ResetPassword() -> String {
        return "Reset...".Localized
    }
    class func Sending() -> String {
        return "Sending...".Localized
    }
    class func GettiingPrefrences() -> String {
        return "Getting prefrences ... ".Localized
    }
    class func UpdatingPrefrences() -> String {
        return "Updating prefrences ... ".Localized
    }
    class func UploadingPost() -> String {
        return "Uploading Post ... ".Localized
    }
    class func Uploading() -> String {
        return "Uploading... ".Localized
    }
    class func updateMediaMessage() -> String {
        return "Updating...".Localized
    }
    class func unMatch() -> String {
        return "Unmatch .. ".Localized
    }
    class func reportingUser() -> String {
        return "Reporting user .. ".Localized
    }
    class func setSyncDate() -> String{
        return "Sending date request..".Localized
    }
    class func setResheduleSyncDate() -> String{
        return "Reshduling date..".Localized
    }
    class func deactivatingAccount() -> String {
        return "Deactivating Account..".Localized
    }
    class func DeletingAccount() -> String {
        return "Deleting Account..".Localized
    }
    class func getReportReasons() -> String{
        return "Fetching Report Reasons..".Localized
    }
    class func blockUser() -> String{
        return "Block user .. ".Localized
    }
    class func gettingDeactivateReasons() -> String {
        return "Fetching Deactivate Reasons..".Localized
    }
    class func creatingDate() -> String {
        return "Creating Date..".Localized
    }
    // class func fetchingDetials() -> String {  return "fetching Details.."
    class func postingUpdate() -> String {
        return "Posting Update".Localized
    }
    class func updating() -> String {
        return "Updating".Localized
    }
    class func gettingProspects() -> String {
        return "Getting Prospects".Localized
    }
    class func gettingMatchesPost() -> String {
        return "Getting Matches Post...".Localized
    }
    class func Subscribing  () -> String {
        return "Subscribing".Localized
    }
    class func VerifyNumber  () -> String {
        return "Verifying your number".Localized
    }
    class func VerifyOtp () -> String {
        return "Verifying OTP".Localized
    }
    class func sendingMsg() ->String{
        return "Sending message ...".Localized
    }
    //basicDetailsMsg
    class func DOBDescrption() ->String{
        return "Your age will be public. Must be 19 or over.".Localized
    }
    class func DOBPlaceHolder() ->String{
        return "MM/DD/YYYY".Localized
    }
    //Messages Keys
    // AppMessages
    class func Network_Not_Reachable() -> String {
        return "Network is unreachable.".Localized
    }
    
    class func UserNameMsg       () -> String {
        return "Please enter your firstname.".Localized
    }
    class func EmailMsg         () -> String {
        return "Please enter valid email id.".Localized
    }
    class func InvalidPhoneNumber() -> String {
        return "Please enter valid phone number.".Localized
    }
    class func InvalidPassword () -> String {
        return "Password length should be 6.".Localized
    }
    class func EmailLengthMsg() -> String {
        return "Please enter your email id.".Localized
    }
    class func EmailFormatWrong() -> String {
        return "Entered email address is not in the right format.".Localized
    }
    
    class func DateOfBirth() -> String {
        return "Please enter your date of birth.".Localized
    }
    class func Gender () -> String {
        return "Please enter your gender.".Localized
    }
    class func Height() -> String {
        return "Please enter your height.".Localized
    }
    class func Password() -> String {
        return "Please add your password.".Localized
    }
    class func ConfirmPassword() -> String {
        return "Please enter your confirm password.".Localized
    }
    class func MobileNumber() -> String {
        return "Please enter your mobile number.".Localized
    }
    class func PasswordNotmatching() -> String {
        return "The passwords must match.".Localized
    }
    class func Error() -> String {
        return "Error".Localized
    }
    class func TokenMissingError() -> String {
        return "Something went wrong please try again login, or contact for support.".Localized
    }
    class func SelectGenderMsg   () -> String {
        return "Select your gender.".Localized
    }
    class func WrongOTP          () -> String {
        return "Entered otp is wrong.".Localized
    }
    class func ErrorUser         () -> String {
        return "User data not found.".Localized
    }
    class func ErrornotAuthorised() -> String {
        return "User not Authorised.".Localized
    }
    class func ErrorHeaderMissng() -> String {
        return "Header is Missing.".Localized
    }
    class func ErrorMobileRegister() -> String {
        return "User Mobile Number already register.".Localized
    }
    class func AgreeTNC() -> String {
        return "I agree to Terms and conditions".Localized
    }
    class func TermsAndCondition() -> String {
        return "Please agreee to Terms and conditions".Localized
    }
    class func InvalidNumber() -> String {
        return "IN VALID NUMBER.".Localized
    }
    class func NotFound() -> String {
        return "Invalid request.".Localized
    }
    class func passwordUpdated   () -> String {
        return "Password updated successfully.".Localized
    }
    class func noAdsAvailable() -> String {
        return "No ads available to show.".Localized
    }
    class func chatTextPlaceHolder() -> String {
        return "Type a message".Localized
    }
    class func coversationDeleted() -> String{
        return "Conversation Deleted".Localized
    }
    
    //camping
    class func campaign() -> String {
        return "Campaign".Localized
    }
    
    //audio call xib
    class func speaker() -> String {
        return "Speaker".Localized
    }
    class func muteLower() -> String {
        return "Mute".Localized
    }
    class func mute() -> String {
        return "Mute".Localized
    }
    
    //card xib
    class func lastPhoto() -> String {
        return "LAST".Localized
    }
    class func photo() -> String {
        return "PHOTO".Localized
    }
    class func next() -> String {
        return "NEXT".Localized
    }
    class func openProfile() -> String {
        return "OPEN PROFILE".Localized
    }
    class func chancesLeft() -> String {
        return "%@ chances left today".Localized
    }
    class func months() -> String {
        return "months".Localized
    }
    class func share() -> String {
        return "Share".Localized
    }
    class func delete() -> String {  return "Delete".Localized
    }
    class func saveContact() -> String {
        return "Save Contact".Localized
    }
    class func thisMsgDeleted() -> String {
        return "This message was deleted".Localized
    }
    
    class func thanksForContacting() -> String {
        return "Thanks for Contacting Us".Localized
    }
    class func weWillReach() -> String {
        return "We will respond as soon as possible.".Localized
    }
    class func mailNotAv() -> String {
        return "Mail Not Available".Localized
    }
    class func plzCheckoutApp() -> String {
        return "Please check out this awesome app called Pairzy! You can download it for free on the App Store. \n".Localized
    }
    class func views() -> String {
        return "views".Localized
    }
    class func messanger() -> String {
        return "Check out Hola Messenger for your smartphone. Download it today from App Store".Localized
    }
    class func fetchingChat() -> String {
        return "Fetching Chats...".Localized
        
    }
    class func checkInternet() -> String {
        return "Check your Internet connection".Localized
    }
    class func deleting() -> String {
        return "Deleting...".Localized
    }
    class func secretChat() -> String {
        return "You created a secret chat".Localized
    }
    class func youAddedToChat() -> String {
        return "You added to a secret chat".Localized
    }
    
    //ChatViewController
    class func youGotImage() -> String {
        return "You received an image".Localized
        
    }
    class func youSentImage() -> String {
        return "You sent an image".Localized
    }
    class func youGotVideo() -> String {
        return "New video".Localized
    }
    class func youSentVideo() -> String {
        return "You sent a video".Localized
    }
    class func youGotLoc() -> String {
        return "You received a location".Localized
    }
    class func sentLoc() -> String {
        return "You sent a location".Localized
    }
    class func gotContact() -> String {
        return "You received a contact".Localized
    }
    class func sentContact() -> String {
        return "You sent a contact".Localized
    }
    class func gotAudio() -> String {
        return "You received an audio".Localized
    }
    class func sentAudio() -> String {
        return "You sent an audio".Localized
    }
    class func gotSticker() -> String {
        return "You received a sticker".Localized
    }
    class func sentSticker() -> String {
        return "You sent a sticker".Localized
    }
    class func gotDoodle() -> String {
        return "You received a doodle".Localized
    }
    class func sentDoodle() -> String {
        return "You sent a doodle".Localized
    }
    class func gotGif() -> String {
        return "You received a gif".Localized
    }
    class func sentGif() -> String {
        return "You sent a gif".Localized
    }
    class func gotDoc() -> String {
        return "You received a document".Localized
    }
    class func sentDoc() -> String {
        return "You sent a document".Localized
    }
    
    class func lastSeen() -> String {
        return "last seen".Localized
    }
    class func areYouSureToDelete() -> String {
        return "Are you sure want to delete chat.".Localized
    }
    class func addOnePhoto() -> String {
        return "Add atleast one photo".Localized
    }
    class func warning() -> String {
        return "Warning".Localized
    }
    class func dontHaveCamera() -> String {
        return "You don't have camera".Localized
    }
    class func chooseVideo() -> String {
        return "Choose Video".Localized
    }
    class func chooseImage() -> String {
        return "Choose Image".Localized
    }
    class func uploadToGallery() -> String {
        return "Upload from Gallery".Localized
    }
    class func plzDeleteOneImage() -> String {
        return "Please delete one image to add another image.".Localized
    }
    class func plzDeleteOneVideo() -> String {
        return "Please delete one video to add another video.".Localized
    }
    class func kmAway() -> String {
        return "miles away".Localized
    }
    class func spend() -> String {
        return "Spend".Localized
    }
    class func coinsToRequest() -> String {
        return "coins to request".Localized
    }
    //InappPurchase
    class func unlimitedLikes() -> String {
        return "Unlimited likes".Localized
    }
    class func swipeRight() -> String{
        return "Swipe right as much as you want.".Localized
    }
    
    class func oneFreeBoost() -> String {
        return "1 free boost every month".Localized
    }
    
    class func skipTheQue() -> String {
        return "Skip the line to get more matches!".Localized
    }
    
    class func chooseWhoSee() -> String {
        return "Choose who sees you".Localized
        
    }
    class func onlyBeShown() -> String {
        return "Only be shown to people you've liked".Localized
    }
    class func swipeArround() -> String {
        return "Swipe around the world!".Localized
    }
    class func passportToAny() -> String {
        return "Passport to anywhere".Localized
    }
    class func userIsOnOtherCall() ->String {
        return "User is on other call".Localized
    }
    
    class func controlYourProf() -> String {
        return "Control your profile".Localized
    }
    class func limitWhatOthersSee() -> String {
        return "Limit what other sees about you".Localized
    }
    class func fiveFreeSuperLike() -> String {
        return "5 Free superlikes every 1 day".Localized
    }
    class func threeTimesMoreLikely() -> String {
        return "You're 3 times more likely to get a match!".Localized
    }
    
    class func unlimitedRewind() -> String {
        return "Unlimited rewinds".Localized
    }
    class func goBackToSwipe() -> String {
        return "Go back and swipe again".Localized
    }
    class func turnOffAdvert() -> String {
        return "Turn off adverts".Localized
    }
    class func haveFunSwiping() -> String {
        return "Have fun swiping".Localized
    }
    class func unlockedUnlimitedLike() -> String {
        return "Unlimited Likes.".Localized
    }
    class func unlockedSuperLike() -> String {
        return "Unlimited Superlikes.".Localized
    }
    class func unlockedRewinds() -> String {
        return "Unlimited Rewinds.".Localized
    }
    class func unlockedPassport() -> String {
        return "Unlimited Passport location around the world.".Localized
    }
    class func subscription() -> String {
        return "subscription".Localized
    }
    class func error() -> String{
        return "Error".Localized
    }
    class func installIstagram() -> String{
        return "Please install the Instagram application".Localized
    }
    
    class func subscriptionFailed() -> String {
        return "Sorry,subscription faild try again".Localized
    }
    class func perMonth() -> String {
        return "/mth".Localized
    }
    class func areYouSureToCancel() -> String {
        return "Are you sure you want to cancel the %@ with %@".Localized
    }
    class func areYouSureToReschedule() -> String {
        return "Are you sure you want to reschedule the %@ with %@?".Localized
    }
    class func areYouSureToReject() -> String {
        return "Are you sure you want to reject the %@ with %@".Localized
    }
    class func areYouSureToConfirm() -> String {
        return "Are you sure you want to Confirm the %@ with %@".Localized
    }
    class func boostWillEnd() -> String {
        return "Boost will End in:".Localized
    }
    class func outOfRewind() -> String {
        return "Out of Rewinds!!".Localized
    }
    class func spendCoinsToInPerson() -> String{
        return "Spend some coins for In person date".Localized
    }
    class func yourQuestion() -> String {
        return "Your question".Localized
    }
    class func yourSuggesion() -> String {
        return "Your suggestion".Localized
    }
    class func yourIssue() -> String {
        return "Your Issue".Localized
    }
    class func connecting() -> String {
        return "connecting...".Localized
    }
    class func disconnected() -> String {
        return "disconnected.".Localized
    }
    class func document() -> String {
        return "Document".Localized
    }
    class func gif() -> String {
        return "Gif".Localized
    }
    class func audio() -> String {
        return "Audio".Localized
    }
    class func image() -> String {
        return "image".Localized
    }
    class func you() -> String {
        return "You".Localized
    }
    class func unblockContact() -> String {
        return "UnBlock Contact".Localized
    }
    class func createNewContact() -> String {
        return "Create New Contact".Localized
    }
    class func contactDetails() -> String {
        return "Contact Details".Localized
    }
    class func blockedContactWill() -> String {
        return "Blocked contacts will no longer be able to call you or send you messages.".Localized
    }
    class func status() -> String {
        return "status".Localized
    }
    class func addExistingContact() -> String {
        return "Add to Existing Contact".Localized
    }
    class func openInGoogleMap() -> String{
        return "Open in Google Maps".Localized
    }
    class func openInMaps() -> String{
        return "Open in Maps".Localized
    }
    class func choosePhoto() -> String {
        return  "Choose Photo".Localized
    }
    class func exceededMaxSelect() -> String {
        return "Exceed Maximum Number Of Selection".Localized
    }
    class func original() -> String {
        return "Original".Localized
    }
    class func squire() -> String {
        return "Square".Localized
    }
    class func dateRequestSentTo() -> String {
        return "%@ request sent to %@".Localized
    }
    class func dateReceivedFrom() -> String {
        return "%@ request accepted from %@".Localized
    }
    class func viewad() -> String {
        return "View ad".Localized
    }
    class func viewAdToReciveCoins() -> String {
        return "View ad to receive %d coins".Localized
    }
    class func nullCoins() -> String {
        return "null coins".Localized
    }
    class func editProfile() -> String {
        return "Edit Profile".Localized
    }
    class func noLongerParticipant() -> String {
        return "you are no longer participant in this group.".Localized
    }
    class func youCantSendMsg() -> String {
        return "you can't send message".Localized
    }
    class func subscribe() -> String {
        return "SUBSCRIBE".Localized
    }
    class func noThanks() -> String {
        return "NO THANKS".Localized
    }
    class func mostPopular() -> String {
        return "MOST POPULAR".Localized
    }
    class func bestValue() -> String {
        return "BEST VALUE".Localized
    }
    class func weTakeYourPrivacy() -> String {
        return "We take your privacy seriously.".Localized
    }
    class func otherUsersCannot() -> String {
        return "Other users cannot contact you unless you've already been matched.".Localized
    }
    class func joinUs() -> String {
        return "Join us.".Localized
    }
    class func ifYouHaventReceivCode() -> String {
        return "If you haven't receive a".Localized
    }
    class func code() -> String {
        return "Code".Localized
    }
    class func verifyYourNumber() -> String {
        return "Verify your mobile number".Localized
    }
    class func checkYourSMSInbox() -> String {
        return "Check your SMS inbox".Localized
    }
    class func mi() -> String {
        return "mi".Localized
    }
    class func countryPicker() -> String {
        return "Country Picker".Localized
    }
    class func subscribed() -> String {
        return "Subscribed".Localized
    }
    class func chatWithOtherName() -> String {
        return "Chat with %@".Localized
    }
    class func callDate() -> String {
        return "call date".Localized
    }
    class func setAsProfilePicture() -> String {
        return "Set as profile picture".Localized
    }
    class func coinPlans() -> String {
        return "coin plans".Localized
    }
    class func likes() -> String {
        return "Likes".Localized
    }
    class func choosePlace() -> String {
        return "Choose a place".Localized
    }
    class func keepSwiping() -> String {
        return "Keep Swiping".Localized
    }
    class func notDoneTransaction() -> String {
        return "No transactions".Localized
    }
    class func selectHere() -> String {
        return "Select here".Localized
    }
    class func search() -> String {
        return "Search".Localized
    }
    class func noCoinsAvailabel() -> String {
        return "no coins plan availabe".Localized
    }
    class func searchLocation() -> String {
        return "Search Location".Localized
    }
    class func searchMore() -> String {
        return "Search More".Localized
    }
    class func edit() -> String {
        return "Edit".Localized
    }
    class func connect() -> String {
        return "Connect".Localized
    }
    class func suggestAPlace() -> String {
        return "Suggest a place".Localized
    }
    class func confirmAddress() -> String {
        return "Confirm Address".Localized
    }
    class func oopsNotSwiped() -> String {
        return "Oops! Seems like you have not swiped right on any profile".Localized
    }
    class func addAVideo() -> String {
        return "Add a Video".Localized
    }
    class func searchAddress() -> String {
        return "Search Address".Localized
    }
    class func setAsProfileVideo() -> String {
        return "Set as profile video".Localized
    }
    class func rateTheDate() -> String {
        return "Rate the date".Localized
    }
    class func suggestTime() -> String {
        return "Suggest a time".Localized
    }
    class func itsMatch() -> String {
        return "Its a Match".Localized
    }
    
    class func chooseLocation() -> String {
        return "Choose Location".Localized
    }
    class func addNewLocation() -> String {
        return "Add  a new location".Localized
    }
    class func addAddress() -> String {
        return "Add Address".Localized
    }
    
    class func currentActivePlan() -> String {
        return "Current Active Plan".Localized
    }
    class func dontShowAgain() -> String {
        return "Don't show again".Localized
    }
    class func decline() -> String {
        return "Decline".Localized
    }
    class func incomingVideoCall() -> String {
        return "Incoming Video Call".Localized
    }
    class func accept() -> String {
        return "Accept".Localized
    }
    class func pleaseReportComments() -> String {
        return "Please report your comments".Localized
    }
    class func reportUpperCase() -> String {
        return "REPORT".Localized
    }
    class func entererdOtpDontMatch() -> String {
        return "The entered OTP doesnot match.".Localized
    }
    class func areYouWaiting() -> String {
        return  "Are you waiting for an invitation?!".Localized
    }
    class func youPairedWith() -> String {
        return "You paired with %@".Localized
    }
    class func remove() -> String {
        return "Remove".Localized
    }
    class func currentLocation() -> String {
        return "CURRENT LOCATION".Localized
    }
    class func rescentLocations() -> String {
        return "RECENT PASSPORT LOCATIONS".Localized
    }
    class func update() -> String {
        return "update".Localized
    }
    class func updateAvailable() -> String {
        return "Update Available".Localized
    }
    class func newVersionAvailable() -> String {
        return "A new version of Pairzy is availabe.Please update to version %@ now".Localized
    }
    class func datumAudioCall() -> String {
        return "Pairzy Audio Call".Localized
    }
    class func datumVideoCall() -> String {
        return "Pairzy Video Call".Localized
    }
    class func chancesLeftToday() -> String {
        return "%d chances left today".Localized
    }
    class func enterYourDetails() -> String {
        return "Enter your details".Localized
    }
    class func getOnADate() -> String {
        return "Set up a Date".Localized
    }
    class func profile() -> String {
        return "Profile".Localized
    }
    class func addAnewLocation() -> String {
        return "Add a new Location".Localized
    }
    class func photoLowerCase() -> String {
        return "Photo".Localized
    }
    class func yesterday() -> String {
        return "yesterday".Localized
    }
    //
    class func yearsAgo() -> String {
        return "years ago".Localized
    }
    class func yearAgo() -> String {
        return "year ago".Localized
    }
    class func monthsAgo() -> String {
        return "months ago".Localized
    }
    class func monthAgo() -> String {
        return "month ago".Localized
    }
    
    class func daysAgo() -> String {
        return "days ago".Localized
    }
    
    class func hoursAgo() -> String {
        return "hours ago".Localized
    }
    
    class func hourAgo() -> String {
        return "hour ago".Localized
    }
    
    class func minuteAgo() -> String {
        return "minute ago".Localized
    }
    class func minutesAgo() -> String {
        return "minutes ago".Localized
    }
    
    class func secondsAgo() -> String {
        return "seconds ago".Localized
    }
    class func secondAgo() -> String {
        return "second ago".Localized
    }
    class func weeksAgo() -> String {
        return "weeks ago".Localized
    }
    
    class func weekAgo() -> String {
        return "week ago".Localized
    }
    
    class func selectLanguage() -> String {
        return "Select Language".Localized
    }
    
    class func addAPhoto() -> String {
        return "Add a Photo".Localized
    }
    
    //chatVC
    class func youAreBlocked() -> String {
        return "You can't send messages to this user because you are blocked.".Localized
    }
    
    class func msgBlockedByYou() -> String {
        return "You have blocked this user".Localized

    }
    //Email Login
    
    class func forgotPassword() -> String {
        return "forgot password?".Localized
    }
    
    class func emailAndPassword() -> String {
        return "Email & Password".Localized
    }
    
    class func loginWith() -> String {
        return "Login With".Localized
    }
    
    class func enterYourEmailAddress() -> String {
        return "Enter your email address".Localized
    }
    class func enterYourPassword() -> String {
        return "Enter your password".Localized
    }
    class func enterNewPassword() -> String {
        return "Enter new password".Localized
    }
    
    class func confirmNewPassword() -> String {
        return "Confirm new password".Localized
    }
    class func pwdDoesntMatch() -> String {
        return "Password doesn't match".Localized
    }
    
    class func minPasswordLengthMessage() -> String{
        return "Password must be of minimum 6 letters"
    }
    
    class func verificationCode() -> String {
        return "Verification Code".Localized
    }
    
    class func verificationCodeSent() -> String {
        return "Verification Code has been sent to".Localized
    }
    
    class func password() -> String {
        return "Password".Localized
    }
    
    
    
    //inApp purchase
    class func recurringBilling() -> String {
        return "Recurring billing. cancel any time. Renews at full price.".Localized
    }
    

    class func paymentWillBeCharged() -> String {
        return "By tapping Subscribe,your payment will be charged to your iTunes account, and your subscription will automatically renew for the same package length at the same price until you cancel it in settings in the iTunes store at leat 24 hours prior to the end of the current period. By tapping Continue,you agree to our Privacy Policy and Terms of Service" .Localized
    }
    
    class func takePhoto() -> String {
        return "Add a Photo".Localized
    }
    
    class func aNewVersionAvailable() -> String {
        return "A new version of Pairzy is availabe.Please update to version %@ now".Localized
    }
    
    //update app
    class func upgradeNow() -> String {
        return "Upgrade Now".Localized
    }
    
    
}

