//
//  AppConstant.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 03/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//
import StoreKit
import UIKit

class AppConstant: NSObject {
    
    //Cloudinary
    static let cloudName = "pipelineai"
    static let uploadPreset = "xibtfmla"
    
    static let threeMonthSubID = "com.pairzyPro.oneMonth"
    static let sixMonthSubID = "com.pairzyPro.sixMonths"
    static let oneYearSubID = "com.pairzyPro.yearly"
    static let coinsSubId750    = "com.pairzy_500coins"
    static let coinsSubId1500    = "com.pairzy_1000coins"
    static let coinsSubId2000   = "com.pairzy_2000coins"
   // http://34.209.115.119:8083/chatDoc/test.html
    static let uploadImageURLExtension = "http://34.209.115.119:8083/chatDoc"
    static let uploadedVideoExtension = "http://34.209.115.119:8083/chatDoc"
    static let uploadedAudioExtension = "http://34.209.115.119:8083/chatDoc"
    static let uploadedDocumetExtension = "\(uploadedAudioExtension)"
    static let videoExtension = "\(uploadedAudioExtension)"
    static let googleMapKey =  "AIzaSyCIArI-OqI0rFvcxynEu5G-ifoNHFFzZRM" //"AIzaSyDqeYzVSRGs_LPYHTLtmjXYM4pjNA4m1zw"
    static let mGoogleLegaceyServerKey = "key=AIzaSyASp__OcAYYibW8ITM9nevufWcfF64w3xg"
    static let keyChainAccount = "DatumAppTocken"
    static let muteNotificationAPI = "inactiveNotification" //POST
    static let defaultStatus = "Hey! I'm using Pairzy"
    
    static let getMessages = "Messages" //get
    static let permissionMsg = "Please provide Contact permission. Goto-> Setting -> Hola -> give Contact permission. or Click here"
    static let cameraPermissionMsg = "Please provide Camera permission. Goto-> Setting -> Hola -> give Camera permission"
    static let audioPermissionMsg = "Please provide Camera permission. Goto-> Setting -> Hola -> give Audio permission"
    
    
    
    static var products: [String: SKProduct] = [:]
    static var transactionId = ""
    static var coinBalance = 0
    static var purchsedProductId = ""
    
    
    enum iphoneX:CGFloat{
        case height = 812
        case width = 375
    }
    
    static var deviceId: String {
        
        if let ID: String = UIDevice.current.identifierForVendor?.uuidString {
            return ID
        }
        else {
            return "iPhone_Simulator_ID"
        }
    }
    
    /// This method is used to get current time.
    /// - Returns: Returns Encoded String
    static func getCurrentTimeWithdate() -> String {
        
        let currentDateTime = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
        let dateInStringFormated: String = dateFormatter.string(from: currentDateTime)
        return dateInStringFormated
    }
    
    struct facebookConstant {
        static let FirstName                                = "first_name"
        static let LastName                                 = "last_name"
        static let Email                                    = "email"
        static let Id_FB                                    = "id"
        static let Picture                                  = "picture"
        static let Data                                     = "data"
        static let URL                                      = "url"
        static let Gender                                   = "gender"
        static let DateOfBirth                              = "birthday"
        static let Name                                     = "name"
    }
}

class alert
{
    func msg(message: String, title: String )
    {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertView.addAction(UIAlertAction(title: StringConstants.doneLower(), style: .default, handler: nil))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
}


extension UIViewController
{
    func alertWith(title: String,and message: String, buttonTitle1 :String, buttonTitle2 : String, doneButtonPressed:@escaping (Bool) -> Void, at controller:UIViewController)
    {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertView.addAction(UIAlertAction(title: buttonTitle1,
                                          style: UIAlertActionStyle.default,
                                          handler: {(alert: UIAlertAction!) in doneButtonPressed(true) }))
        
        alertView.addAction(UIAlertAction(title: buttonTitle2,
                                          style: UIAlertActionStyle.destructive,
                                          handler: {(alert: UIAlertAction!) in doneButtonPressed(false) }))
        
        controller.present(alertView, animated: true, completion: nil)
    }
}



//NSUserDeafults Keys
struct LoginResponse {
    static let errorFlag                 = "errFlag"
    static let data                      = "data"
    static let errorMessage              = "message"
    static let code                      = "code"
    static let token                     = "accessToken"
}

//NSUserDeafults Keys
struct SignupResponse {
    static let errorFlag                  = "errorFlag"
    
    static let errorMessage               = "message"
    static let code                       = "code"
}

struct CellIds {
    static let HomeCollectionCell  = "HomeCollectionCell"
    static let MatchMakerCVCell    = "MatchMakerCVCell"
    static let PhotoIndicatorCell  = "PhotoIndicatorCell"
    static let ProfileInfoCell     = "ProfileInfoCell"
    static let ProfileOtherInfoCell = "ProfileOtherInfoCell"
    static let ProfileCollectionCell = "ProfileCollectionCell"
    static let ProfileShareCell      = "ProfileShareCell"
    static let ProspectCollectionCell = "ProspectCollectionCell"
    // static let cellIdentifier = "ChatListTableViewCell"
}

//OneSky AppId and public key
struct OneSky {
    static let PublicKey        = "d7PXvQGn9sMAnjgEo3sgSC7XJj08f00j"
    static let AppId            = "327302"
    static let SecretKey        = ""
}

struct Admob {
    static let appId = "ca-app-pub-4801882000967378~2696976333"
    static let rewardVideoUnitId = "ca-app-pub-4801882000967378/1440870450"
    static let interstitialAdUnitId = "ca-app-pub-4801882000967378/3969819115"
}

struct TapJoy {
    static let appId             = "vGETidBoQRKp_p-lkIGBXgEBedHwF4uQleQZKdYxg8mt-cBpDae9SxVbnPzy"
    static let bannerAdUnitId    = ""
    static let rewardUnitId      = ""
}

struct FourSquare {
    static let clientId = "FISW5FKMS121RFVTSFOJI3XRI33XPUSUWT24RBNGGHBXQMVM"
    static let clientSecret = "OSTMSRZL1XCDV3R2MJ50WY2R1Z2HBBTX3GCLOC1PRPTC13MN"
}

struct CallTypes {
    static let audioCall = "0"
    static let videoCall = "1"
}

struct StoryBoardIdentifier {
    static let phoneNumberSI               = "phoneNumberSI"
    static let datePickerSB                = "DatePicker"
    static let countryPickerSI             = "countryPickerSI"
    static let numberVerificationSI        = "numberVerificationVc"
    static let userBasicSI                 = "userBasicVC"
    static let genderPickerSI              =  "genderPicker"
    static let addMediaSI                  = "addMediaVc"
    static let joinUssgSI                  = "joinUSVc"
    static let ResendOtpVC                 = "ResendOtpVC"
    static let PreferenceTable             = "PreferenceTable"
    static let TextFieldTypeVC             = "TextFieldTypeVC"
    static let profileVc                   = "profileVc" // other profile
    static let myProfile                   = "myProfileViewCon" //own profile
    static let DatumChatVC                 = "DatumChatVC"
    static let MomentsViewController       = "MomentsViewController"
    static let EULAViewController       = "EULAViewController"
    static let MomentsDetailViewController   = "MomentsDetailViewController"
    
    
    
}

struct InAppPurchase {
    
    static let ProductId = "productId"
    static let ProductTitle = "productTitle"
    static let PoductDescript = "productDescript"
    static let Price = "price"
    static let CurrencyCode = "currencyCode"
    static let CurrencySymbol =  "currencySymbol"
    static let Months = "months"
}


//NSUserDeafults Keys
struct Keys {
    static let awsAccessKey = "AKIAIHIC37PKC32GPBAQ"
    static let awsSecretKey = "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"
    static let photoUploadBucket = "datumv3"
    static let videoUploadBucket = "sync1to1"
    static let authorizationForLogin = "KMajNKHPqGt6mykXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj"
}

//Common Request Params
struct  REQUEST{
    
    static let kSessionToken                 = "ent_sess_token"
    static let kDeviceId                     = "ent_dev_id"
    static let kFBId                         = "ent_fbid"
    static let kUserFBId                     = "ent_user_fbid"
    static let kMobileOtp                    = "otp"
}

//Identifier For Sigue
struct IDENTIFIER {
    
    static let LOGIN                          = "LoginVC"
    static let SIGNUP                         = "SignupVC"
    static let TermAndConditions              = "TermAndContions"
    static let CountryPicker                  = "CountryPicker"
    static let MobileVC                       = "MobileVC"
    static let GotoResetPasword               = "gotoReset"
    static let ResetToCountrypicker           = "resetToCountrypicker"
    static let ResetTOnewpassowrd             = "resetTOnewpassowrd"
    static let ResetToOTPVerification         = "phoneNumberToOTPVerification"
    static let OtpToNewPassword               = "otpToNewPassword"
    static let GenderPicker                   = "genderPicker"
    static let OverlayView                    = "OverlayView"
    
}

struct coinsFor {
    static let createDate = 100
    static let sendUnMatchMessage = 100
    static let SuperLike = 100
    static let boost = 100
    static let withOutMatchChat = 200
}



struct AppSettings {
    static let MaximumAge = "55"
    static   let MinimuAge  = "18"
}


struct NSNotificationNames {
    static let notificationForOpenProfile = "openProfile"
    static let notificationForMatchUser = "matchedWithUser"
    static let notificationForBoostUpdate = "boostUpdate"
    static let notificationForUnmatchProfile = "unMatchedProfile"
    static let notificationForGetProfiles = "receivedProfiles"
    static let notificationForPreferncesUpdate = "preferencesUpdated"
    static let notificationToUpdateLikeDislikeProfiles = "profileLikedOrdisLiked"
    static let notificationToUpdateProfileImage = "updateProfilePicture"
    static let notificationFortappedOnPushNotification = "tappedOnPushNotification"
    static let notficationForVideoCallStarted = "videoCallIntiated"
    static let notificationForSyncDatePushTapped = "datePushDateTapped"
    static let notificationForRefreshFutureTab = "RefreshFutureTab"
    static let notificationForRefreshMatchMakerTab = "RefreshMatchMaker"
    static let notificationForDynamicLink = "openProfileFromynamicLink"
    static let notificationForBannedProfile = "profileBannedFromAdmin"
    static let notificationForLocationUpdate = "locationUpdateFromMap"
    static let notificationForCampagin = "receivedCampagin"
    static let notificationForRating  = "ratingUpdated"
    static let deletedUserFromAdmin = "deletedUserFromAdmin"
    static let userPassportedLocation = "userPassportedLocation"
    static let notificationForRefreshCoins = "refreshNewCoins"
    static let notificationForNewADAvailable = "newAdAvailable"
    static let removeAllObserversForController = "removeExisitingObservers"
    static let blockUpdate = "updateBlockStatus"
    static let unBlockUpdate = "updateUnBlockStatus"
    static let updateCoinFromAdmin = "updateCoinFromAdmin"
    static let updateNewsFeedDetails = "updateNewsFeedDetails"
    static let createNewPost = "createNewPost"
    
}

struct InstaAPI{
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    static let INSTAGRAM_CLIENT_ID = "4f5a78c3929d4f44a3b65c943e04c552"
    static let INSTAGRAM_CLIENTSERCRET = "bbe8c7e0b1b6483faa0210a613436a86"
    static let INSTAGRAM_REDIRECT_URI = "https://fetchfile.pairzy.com/policy.html"
    static let INSTAGRAM_ACCESS_TOKEN = "access_token"
    static let INSTAGRAM_SCOPE = "public_content"
    /* add whatever scope you need https://www.instagram.com/developer/authorization/ */
    static let instagramBaseApi = "https://api.instagram.com/v1/users/"
}


struct SERVICE {
    
   static let BASE_URL                        = "https://appapi.pairzy.com/"
 //  static let BASE_URL                       = "https://devapi.pairzy.com/"
    //"http://34.221.2.149:1008/"  "https://apis.findamatch.online/"
    static let MQTT_URL                       = "34.209.115.119"  //"34.221.2.149" //done //"192.241.221.88"
    static let MQTT_PORT:UInt32               = 1883
    static let TermAndCondition               = "https://fetchfile.pairzy.com/TermsAndConditions.html"
    static let PrivacyPolicy                  = "https://fetchfile.pairzy.com/policy.html"
    static let Licenses                       = "https://fetchfile.pairzy.com/Licenses.html"
    static let SafetyTips                     = "https://fetchfile.pairzy.com/SafetyTips.html"
    static let CommunityGuideLines            = "https://fetchfile.pairzy.com/CommunityGuidelines.html"
    static let staticlinkForDeepLinking       = "https://appapi.pairzy.com/documentation#!/searchResult/"
    //"http://107.170.105.134:1008/documentation#!/searchResult/"
    
    static let uploadMultimediaURL = "http://34.209.115.119:8008/"
    static let uploadImageURLExtension =  "http://34.209.115.119:8083/chatDoc" //"http://34.221.2.149/chatDoc"// " http://159.203.135.99/mqttDemo/profilePics"
    static let uploadedVideoExtension =    "http://34.209.115.119:8083/chatDoc"  //"http://34.221.2.149/chatDoc"//"http://159.203.135.99/mqttDemo/uploads"
}

//Request Params For Login/Signup
struct ServiceInfo {
    static let authorization                    = "authorization"
    static let Fname                            = "firstName"
    static let Email                            = "email"
    static let phoneNumber                      = "contactNumber"
    static let password                         = "password"
    static let timeZone                         = "timeZone"
    static let Password                         = "password"
    static let DateOfbirth                      = "dateOfBirth"
    static let Gender                           = "gender"
    static let CountCode                        = "countryCode"
    static let PushToken                        = "pushToken"
    static let DeviceType                       = "deviceType"
    static let PrefSex                          = "pref_sex"
    static let MobileNumber                     = "mobileNumber"
    static let Height                           = "height"
    static let DeviceMake                       = "deviceMake"
    static let DeviceModel                      = "deviceModel"
    static let LocationName                     = "locationName"
    static let FirbaseID                        = "fIberUUId"
    static let profileVideo                     = "profileVideo"
    static let otherfileVideo                   = "otherVideos"
    static let profilePic                       = "profilePic"
    static let otherProfilePic                  = "otherImages"
    static let deviceType                       = "dType"
    static let deviceId                         = "deviceId"
    static let deviceMake                       = "deviceMake"
    static let deviceModel                      = "deviceModel"
    static let emailId                          = "emailId"
    static let longitude                        = "longitude"
    static let latitude                         = "latitude"
    static let placeId                          = "placeId"
    static let addressLine1                     = "line1"
    static let addressLine2                     = "line2"
    static let pincode                          = "postCode"
    static let cityName                         = "city"
    static let stateName                        = "state"
    static let countryName                      = "country"
    static let language                         = "lang"
    static let type                             = "type"
    static let OTP                              = "otp"
    static let phoneNumberWIthCode              = "phoneNumber"
    static let UservideoLink                    = "profileVideo"
    static let UserImageLink                    = "profilePic"
    static let userId                           = "_id"
    static let userID                           = "userId"
    static let pairId                           = "pairId"
    static let statusCode                       = "action"

    static let heightInFeet                     = "heightInFeet"
    static let deviceModelKey                   = "deviceModel"
    static let appVersion                       = "appVersion"
    static let modelNumber                      = "modelNumber"
    static let deviceOs                         = "deviceOs"
    static let deviceMakeKey                    = "deviceMake"
    static let constantPushToken                = "iOS Simulator Push Token"
    static let iPAddress                        = "ip"
    static let otp                              = "opt"
    static let isPassportLocation               = "isPassportLocation"
    
    static let name                             = "name"
    static let email                            = "email"
    static let issue                            = "description"
    static let rateValue                        = "rate"
    static let rateMessage                      = "message"
    
    static let dontShowMyAge                    = "dontShowMyAge"
    static let dontShowMyDist                   = "dontShowMyDist"
    static let profileVideoThumbnail            = "profileVideoThumbnail"
    
    //patch preferences
    static let preferenceId                      = "pref_id"
    static let values                           = "values"
    
    //fbLogin
    static let FbId                             = "fbId"
    
    static let DOB                              = "dob"
    
    //coins Requests params
    
    static let PlanId                           = "planId"
    static let PaymentGatewayTxnId              = "paymentGatewayTxnId"
    static let PaymentGateway                   = "paymentGateway"
    static let UserPurchaseTime                 = "userPurchaseTime"
    static let PaymentType                      = "paymentType"
    static let Trigger                          = "trigger"
    static let PaymentTxnId                     = "paymentTxnId"
    static let InstaGramProfileId               = "instaGramProfileId"
    static let InstaGramToken                   = "instaGramToken"
    static let isSuperLikedMe                   = "isSupperLikedMe"
    
    static let aboutYou = "about"
    static let voipiospush   = "voipiospush"
    static let isIosAppInDevelopment = "isIosAppInDevelopment"
    static let apnsPush = "apnsPush"
    
    
    //CreatePost Params
    
    static let typeFlag                         = "typeFlag"
    static let description                      = "description"
    static let url                              = "url"
    static let postId                           = "postId"
    static let comment                          = "comment"
    
   
    
    
    
    
}

// All API Names
struct APINAMES {
    static let getVersion                      = "version/"

    static let LOGIN                           = "login"
    static let SIGNUP                          = "registerUser"
    
    static let MOBILECODE                      = "OTP"
    static let RESENDCODE                      = "resendVerificationCode"
    static let FORGOTPASSWORD                  = "ForgetPassword"
    static let RESETPASSWORD                   = "password"
    static let VERIFYOTP                       = "verificaitonCode"
    static let RESETPASSWORDPHONE              =  "forgotPasswordByPhoneNumber"
    static let RESETPASSWORDEMAIL              =  "forgotPasswordByEmail"
    static let UpdateMedia                     =  "media"
    static let Matchpreferences                =  "searchPreferences"
    static let getProfiles                     =  "searchResult"
    static let likeProfile                     =  "like"
    static let disLikeProfile                  =  "unLike"
    static let superLikeProfile                =  "supperLike"
    static let getMatches                      = "match"
    static let UnMatchProfile                  = "unMatch"
    static let getInstantMatches               = "like"
    static let getSecondLook                   = "unLike"
    static let blockProfile                    = "block"
    static let pendingCalls                    = "pendingCalls"
    static let setSyncDate                     = "date"
    static let deleteAccount                   = "profile"
    static let deleteChat                      = "Chats"
    static let sendMsgWithOutMatch             = "messageWithoutMatch"
    static let getReportReasons                = "reportUserReasons"
    static let getReportPostReasons            = "reportPostReasons"
    
    static let getCallHistory                 = "callLogs/"
    static let getFutureProfiles              =  "currentDates"
    static let setDateStatus                  = "dateResponse"
    static let getPastDates                   = "pastDates"
    static let setRatingForVideoCall          = "dateRating"
    static let completedate                   = "completeDate"
    static let deactivateAccount              = "deactive"
    static let reportUser                     = "reportUser"
    static let reportPost                     = "reportPost"
    static let blockUser                      = "block"
    static let deactivateReasons              = "deactivateReasons"
    static let askAQuestion                   = "askAQuestion"
    static let makeASuggestion                = "makeASuggestion"
    static let reportAnIssue                  = "reportAnIssue"
    static let postRatingForApp               = "dateRating"
    static let updatedeepLink                 = "deepLink"
    static let updateLocation                 = "location"
    static let getChats                       = "Chats/0"
    static let unBlock                        = "unBlock"
    static let rewind                         = "rewind"
    static let boost                          = "boost"
    static let boostLikes                     = "boostWithLike"
    static let earnCoinsForVideoAd             =  "coinFromVideo"
    //login
    static let FaceBookLogin                    = "faceBooklogin"
    static let checkFbId                       = "checkFbId/"
    
    //datum
    static let emailIdExists                    = "emailIdExistsVerificaiton"
    static let phoneNumberExists                = "phoneNumberExistsVerificaton"
    static let getProfileDetails                = "profile/"
    static let RequestOtp                       = "requestOtp"
    static let profile                          = "profile"
    static let Preferences                      = "preferences"
    static let SupperLikesBy                    = "supperLikesBy"
    static let OnlineUsers                      = "onlineUsers"
    static let MobileNumber                     = "mobileNumber"
    static let getCloudinaryDetails            = "cloudinaryDetails"
    static let forgotPassword                   = "forgotPassword"
    static let newPassword                      = "newPassword"
    static let emailIdExistsVerificaiton        = "emailIdExistsVerificaiton"
    static let emailLogin                       = "login"
    static let verifyOTP                        = "verifyOTP"
    
    //coins
    static let CoinHistory                      = "coinHistory"
    static let CoinPlans                        = "coinPlans"
    static let CurrentBalance                   = "currentCoinBalance"
    static let Plan                             = "plan"
    static let PlanHistory                      = "planHistory"
    static let Subscription                     = "subscription"
    
    
    //prospects
    static let MySuperLikes                     = "mySupperLikes"
    static let RecentVisitores                  = "recentVisitors"
    static let SuperLike                        = "superLike"
    static let LikesBy                          = "likesBy"
    static let MyLikes                          = "myLikes"
    static let MyUnLikes                        = "myUnLikes"
    
    //dates Tab
    static let CurrentDates                     = "currentDates"
    static let coinConfig                       = "coinConfig"
    
    //Create Post API
    
    static let userPost                         = "userPost"
    static let userPostLikeUnlike               = "userPostLikeUnlike"
    static let userPostComment                  = "userPostComment"
    static let getuserPostComment               = "userPostComment/"
    static let userPostLike                     = "userPostLike/"
    
    //    Matcher API
    static let matchedPairs                     = "matchedPairs/"
    static let userActions                      = "userActions/"
    static let leaderBoard                      = "leaderboard/"
}

struct profileData {
    static let Fname                            = "firstName"
    static let Email                            = "email"
    static let EmailId                          = "emailId"
    static let phoneNumber                      = "contactNumber"
    static let DateOfBirth                      = "dateOfBirth"
    static let Gender                           = "gender"
    static let CountCode                        = "countryCode"
    static let MobileNumber                     = "mobileNumber"
    static let Height                           = "height"
    static let profileVideo                     = "profileVideo"
    static let otherfileVideo                   = "oVideo"
    static let profilePic                       = "profilePic"
    static let otherProfilePic                  = "otherImages"
    static let phoneNumberWIthCode              = "phoneNumber"
    static let userId                           = "_id"
    static let distance                         = "distance"
    static let about                            = "about"
    static let onlineStatus                     = "onlineStatus"
    static let opponentId                       = "opponentId"
    static let dateId                           = "dateId"
    static let rate                             = "rate"
    static let deepLink                         = "deepLink"
    static let activityStatus                   = "activityStatus"
    static let otherImages                      = "otherImages"
    static let work                             = "work"
    static let job                              = "job"
    static let education                        = "education"
    static let moments                          = "moments"
    
    
    
    static let Age                              = "age"
    static let Value                            = "value"
    static let IsHidden                         = "isHidden"
    static let IsUserBlocked                    = "blocked"
    static let Liked                            = "liked"
    static let Superliked                       = "superliked"
    static let Unliked                          = "unliked"
    static let isCoversationStarted             = "isConvertationStart"
    static let isMatched                        = "isMatched"
    static let isSupperLikedMe                  = "isSupperLikedMe"
    static let count                            = "count"
    static let isInitiated                      = "initiated"
    static let chatStarted                      = "payload"
}



struct MatchedProfileData {
    static let firstLikedByName                 = "firstLikedByName"
    static let secondLikedByName                = "SecondLikedByName"
    static let matchDate                        = "matchDate"
    static let firstLikedId                     = "firstLikedBy"
    static let secondLikedId                    = "secondLikedBy"
    static let secondLikedByPhoto               = "secondLikedByPhoto"
    static let firstLikedByPhoto                = "firstLikedByPhoto"
    
    static let firstLikedByPhotoChat            =     "gcm.notification.firstLikedByPhoto"
    static let firstLikedIdChat                     =     "gcm.notification.firstLikedBy"
    static let firstLikedByNameChat                 =     "gcm.notification.firstLikedByName"
}


struct  UserDefaultsKeys {
    static let userID = "userID"
    static let indexDocID = "indexDocID"
    static let userName = "userName"
    static let userImage = "userImage"
    static let sessionToken = "token"
    static let pushToken = "pushToken"
    static let callPushToken = "callpushToken"
    static let rechabilityNotificationKey = "rechabilityNotification"
    static let userNumber = "userNumber"
    static let userStatus = "userStatus"
    static let colourKey = "colourKeys"
    static let statusArr = "statusarray"
    static let receiverIdentifier = "receiverIdentifier"
    static let isUserOnchatscreen = "userIsnoChatscreen"
}


struct SegueIds{
    static let contactToInfoTomedia = "goInfoTomedia"
    static let infoToChatView = "infoToChatView"
    static let chatTouserDetails = "chatTouserDetails"
    static let chatTogifsegue = "chatTogifsegue"
    static let chatToGroupInfoView = "chatToGroupInfoView"
    static let chatSegueIdentifier = "segueToChatViewController"
    static let matchPrefToTabBarController = "toTabBarController"
    static let previewTopresentFilters = "presentFiltersSegue"
    static let chatVCToshowLocation = "showLocationSegue"
    static let chatVCTotoContactDetails = "toContactDetailsSegue"
    static let chatVCTogroupInfo = "chatToGroupInfoView"
    static let chatVCTodocumentViewer = "documentViewerSegue"
    static let coinHistToAddCoins = "toAddCoins"
    static let addCoinsToCoinHist = "toCoinHistoryVC"
    static let myProfileToEditProfile = "editProfileSegue"
    static let editProfileSegue = "editProfileSegue"
    static let profileDetailToMomentVC = "profileDetailToMomentVC"
    static let ProfileDetailToMomentDetailVC = "ProfileDetailToMomentDetailVC"
    static let myProfileToMomentVC = "myProfileToMomentVC"
    static let MomentDetailsToCommentVC = "MomentDetailsToCommentVC"
    static let MomentDetailstoLikersVC = "MomentDetailstoLikersVC"
    static let ChatVCToProfileVC = "ChatVCToProfileVC"
    static let HomeVCToProfileVC = "HomeVCToProfileVC"
    static let ProspectsVCToProfileVC = "ProspectsVCToProfileVC"
    static let MatchmakerVCToProfileVC = "MatchmakerVCToProfileVC"
    static let DatesVCToProfileVC    = "DatesVCToProfileVC" 
}



enum groupType:Int {
    case creatGroup
    case addInGroup
    case removeFromGroup
    case makeGroupadmin
    case groupNameChange
    case groupImagechange
    case leaveGroup
}

//LandingPage scroll view Messages
struct ScrollMsg {
    
    static let MsgArray1                        = ["Meet singles near you!", "Unlimited interaction", "Match with members near you."]
    
    static let MsgArray2                        = ["200,000 daters and growing", "with as many as people as you like, for free!", ""]
    //let SCREEN_WIDTH:  Int = Int(UIScreen.main.bounds.size.width)
}

//Font Faimly
struct FontFiamly {
    
    static let gothamBookItalic = "GothamRounded-BookItalic"
    static let gothamMediumItalic = "GothamRounded-MediumItalic"
    static let gothamBoldItalic = "GothamRounded-BoldItalic"
    static let gothamLight = "GothamRounded-Light"
    static let gothamMedium = "GothamRounded-Medium"
    static let gothamBold = "GothamRounded-Bold"
    static let gothamLightItalic = "GothamRounded-LightItalic"
    static let gothamBook = "GothamRounded-Book"
    
    //    static let
}

//Font Gender
struct Gender {
    
    static let GenderArray                      = [StringConstants.male(), StringConstants.female()]
}


struct viewControllerID {
    static let datePickerPopVc = "DatePickerPopUpVc"
    static let syncDateRequestVc = "syncDateVc"
}





//All Response KEYS

struct ResponseKey {
    
    static let errorFlag                    = "errorFlag"
    static let error                        = "error"
    static let errorMessage                 = "errorMessage"
    
    static let data                         = "data"
    
    //register respose keys
    
    static let token                        = "token"
    static let userID                       = "userId"
    static let mobileNo                     = "mobileNo"
    
    
}
struct  SyncProfile {
    static let name = "opponentName"
    static let profilepic = "opponentProfilePic"
    static let rating = "rate"
    static let InitiatedBy = "isInitiatedByMe"
    static let dateTime = "proposedOn"
    static let profileID = "opponentId"
    static let dateId = "data_id"
    static let creationTime = "createdTimestamp"
    static let PlaceName  = "placeName"
    static let requestedFor = "requestedFor"
    static let dateType   = "dateType"
    static let latitude   = "latitude"
    static let longitude  = "longitude"
    static let opponentResponse = "opponentResponse"
    static let isLiveDate = "isLiveDate"
    
    static let mobileNumber = "opponentNumber"
}



//All Request Params
struct RequestParams{
    
    static let MobileNo                     = "phoneNumber"
    static let VCode                        = "otp"
    
    
    //forgotpassword req params
    static let firstname                   = "firstName"
    static let email                       = "email"
    
    //resetpassword req params
    static let otp                         = "otp"
    static let password                    = "password"
    
}

//All NSUserdefaultkey
struct SyUserdefaultKeys {
    
    static let userMobileNo                         = "mobileNum"
    static let userToken                            = "token"
    static let userID                               = "userID"
    static let kNSUSessionTokenKey                  = "TOKEN"
    static let kNSUAppJoinedDateKey                 = "Joined"
    static let kNSUPushNotificationTokenKey         = "DeviceToken"
    static let kNSUUUID                             = "UUID"
    static let KNSProfilePic                        = "profilePic"
    static let KNDeviceName                         = "deviceName"
    
    static let openChat                             = "openChat"
    
    
    static let storIpDetails                        = "userIpDetails"
    static let userDeepLink                         = "loginProifleUrl"
    
    static let displayNotification                  = "displayNotification"
    
    static let unmatchedChatUsers                   = "unMatchedChatUsers"
    
}

struct likeProfileParams {
    static let targetPostId                    =      "targetPostId"
    static let targetUserId                   =       "targetUserId"
    static let syncDate                       =       "proposedOn"
    static let dateType                       =       "dateType"
    static let longitude                      =       "longitude"
    static let latitude                       =       "latitude"
    static let placeName                      =       "placeName"
    
    static let dateId                         =       "date_id"
    static let status                         =       "response"
    static let reason                         =        "reason"
    static let message                        =       "message"
    static let isSchedule                     =       "isSchedule"
}

struct MQTTTopic {
    
    static let messagesTopicName = "Message/"
    static let acknowledgementTopicName = "Acknowledgement/"
    static let onlineStatus = "OnlineStatus/"
    static let typing = "Typ/"
    static let contactSync = "ContactSync/"
    static let userUpdates = "UserUpdate/"
    static let callsAvailability = "CallsAvailability/"
    static let calls  = "Calls/"
    static let deleteUser = "deleteUser/"
    static let proUser = "proUser/"
    
    
    static let pushNotificationTopic = "Message/"
    
}



struct prefernceDetails {
    static let prefId = "_id"
    static let typeOfPreference = "TypeOfPreference"
    static let PreferenceTitle = "PreferenceTitle"
    static let prefIcon = "icon"
    static let OptionsValue = "OptionsValue"
    static let selectedValue = "selectedValue"
    static let selectedUnit = "selectedUnit"
    static let optionsUnits  = "optionsUnits"
}

struct ApiResponseKeys {
    
    static let message       = "message"
    static let data          = "data"
    static let preferenceId  = "pref_id"
    static let prefMainTitle = "label"
    static let prefOptions = "options"
    static let prefSelectedValue = "selectedValues"
    static let prefSubTitle = "title"
    static let type       = "type"
    static let isDone    = "isDone"
    static let priority = "priority"
    static let myPreferences = "myPreferences"
    static let moments = "moments"//myPrefarences
    
    
    //Prospects superlikesBy
    static let countryCode    =   "countryCode"
    static let creation       =   "creation"
    static let dateOfBirth    =   "dateOfBirth"
    static let emailId        =   "emailId"
    static let firstName      =   "firstName"
    static let height         =   "height"
    static let mobileNumber   =   "mobileNumber"
    static let opponentId     =   "opponentId"
    static let profilePic     =   "profilePic"
    
    //plans
    static let id                = "planId"
    static let cost              = "cost"
    static let currency          = "currency"
    static let currencySymbol    = "currencySymbol"
    static let coin              = "Coin"
    static let planName          = "planName"
    static let actualId          = "actualIdForiOS"
    
    static let currencyCode       =  "currencyCode"
    static let description        = "description"
    static let durationInDays     = "durationInDays"
    static let durationInMonths   = "durationInMonths"
    static let hideAge            = "hideAge"
    static let hideDistance       = "hideDistance"
    
    
    
    static let likeCount           =  "likeCount"
    static let noAdds              = "noAdds"
    static let passport            = "passport"
    static let readreceipt         = "readreceipt"
    static let rewindCount         = "rewindCount"
    static let recentVisitors      = "recentVisitors"
    static let superLikeCount      = "superLikeCount"
    static let whoLikeMe           = "whoLikeMe"
    static let whoSuperLikeMe      = "whoSuperLikeMe"
    static let noOfCoinUnlock      = "noOfCoinUnlock"
    static let userPurchaseTime    = "userPurchaseTime"
    static let subscriptionId      = "subscriptionId"
    static let purchaseTime        = "purchaseTime"
    static let purchaseDate        = "purchaseDate"
    static let PlanId              = "planId"
    static let Subscription        = "subscription"
    static let ExpiryTime          = "expiryTime"
    static let Coins               = "coins"
    static let CoinAmount          = "coinAmount"
    static let CoinClosingBalance     = "coinClosingBalance"
    static let CoinOpeingBalance      = "coinOpeingBalance"
    static let CoinType               = "coinType"
    static let Timestamp              = "timestamp"
    static let TxnId                  = "txnId"
    static let TxnType                = "txnType"
    static let TxnTypeCode            = "txnTypeCode"
    static let AllCoins               = "allCoins"
    static let CoinIn                 = "coinIn"
    static let CoinOut                = "coinOut"
    static let pendingDates           = "pendingDates"
    static let upcomingDates          = "upcomingDates"
    
}

struct CircularAir {
    static let Light            = "CircularAirPro-Light"
    static let Book             = "CircularAirPro-Book"
    static let Bold             = "CircularAirPro-Bold"
}

struct fontSize {
    
    static let navigationTitle = 20
    static let screenMainSideTitle = 24
    static let coinsPopUpTitle = 18
    
    
}

enum CoinsPopupFor:Int {
    case forSuperLike = 0
    case forChat = 1
    case forDate = 2
    case forBoost = 3
    case forAll = 4
}

enum DateType:Int{
    case videoDate = 1
    case audioDate = 3
    case inPersonDate = 2
    case none = 4
}

enum DateResponseType:Int {
    case confirm = 1
    case reject = 2
    case reSchedule = 3
    case cancel = 4
}
