//
//  Colors.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 05/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class Colors: NSObject {
    static let AppBaseColor                                 = Helper.getUIColor(color:"#3ED6D4") //pink
    static let AppBaseGradient                              = Helper.getUIColor(color:"#5DFAF8")
    static let SecondBaseColor                              = Helper.getUIColor(color:"#FFFFFF")//White Color
    static let boostColor                                   = Helper.getUIColor(color:"#AE48E4")//White Color
    
    static let PrimaryText                                  = Helper.getUIColor(color:"#484848")//Black
    static let SeconderyText                                = Helper.getUIColor(color:"#868686")
    static let SeconderyLightText                           = Helper.getUIColor(color:"#B2B2B2")
    
    static let ScreenBackground                             = Helper.getUIColor(color:"#F5F5F5")
    static let SeparatorDark                                = Helper.getUIColor(color:"#AAAAAA")
    static let SeparatorLight                               = Helper.getUIColor(color:"#DCE1EF")
    static let BorderColor                                  = Helper.getUIColor(color: "#EBE7E7")
    //static let PlaceHolder                                  = Helper.getUIColor(color:"D8DCE5")
    static let Red                                          = Helper.getUIColor(color:"#F22222")
    static let coinBalBackground                            = Helper.getUIColor(color: "#F9FAFC")
    static let coinBalanceBtnBorder                         = Helper.getUIColor(color: "#FFCC33")
    static let blueColor                                    = Helper.getUIColor(color: "#1974EB")
    static let tabBarUnselected                             = Helper.getUIColor(color: "#434343")
    static let progressTail                                 = Helper.getUIColor(color: "#3ED6D4")
    static let chatFooterView                               = Helper.getUIColor(color: "#EAE0BB")
    static let likeBtnGreenColor                            = Helper.getUIColor(color: "#3CDFB7")
    static let unlikeBtnGreenColor                          = Helper.getUIColor(color: "#FC88A2")

    
}

