//
//  AppConfigViewModal.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 29/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

struct AppConfigViewModal {
    
    static let configFileName = "AppConfig"
    static let extensionName = "plist"
    static let fbEnabeldKey = "Facebook Enabled"
    static let configPlistName = "AppConfig.plist"
    
    
    static func getAppConfigData() -> [String:Any]?{
        
        var appData : [String:Any]?
        if let configFilePath = Bundle.main.url(forResource: AppConfigViewModal.configFileName, withExtension: AppConfigViewModal.extensionName),
            let data = try? Data(contentsOf: configFilePath) {
            if let result = try? PropertyListSerialization.propertyList(from: data, options: [], format: nil) as? [String: Any] {
                appData = result
                return appData
            }
        }
        return appData
    }
    
    static func getPath() -> String {
        let plistFileName = AppConfigViewModal.configPlistName
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentPath = paths[0] as NSString
        let plistPath = documentPath.appendingPathComponent(plistFileName)
        return plistPath
    }

    static func updateValuesInPlist()
    {
        let initialFileURL = URL(fileURLWithPath: Bundle.main.path(forResource: configFileName, ofType: extensionName)!)
        let documentDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last!
        let writableFileURL = documentDirectoryURL.appendingPathComponent(configPlistName, isDirectory: false)
        
        do {
            try FileManager.default.copyItem(at: initialFileURL, to: writableFileURL)
        } catch {
            print("Copying file failed with error : \(error)")
        }
        
        let plistPath = self.getPath()
        if FileManager.default.fileExists(atPath: plistPath) {
            let appConfigData = NSMutableDictionary(contentsOfFile: plistPath)!
            appConfigData.setValue(true, forKey: AppConfigViewModal.fbEnabeldKey)
            appConfigData.write(toFile: plistPath, atomically: true)
        }
    }
}
