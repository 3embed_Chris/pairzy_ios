//
//  LoginView.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 02/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class LoginView: UIView {
    
    struct errorConstant {
        static let alertTitle = "Alert"
        static let notValidEmailError = "Please enter valid Email."
    }
    
    @IBOutlet weak var emailTextFieldView: UIView!
    @IBOutlet weak var emailLabelOutlet: UILabel!
    @IBOutlet weak var emailTextFieldOutlet: UITextField!
    
    @IBOutlet weak var passwordTextFieldView: UIView!
    @IBOutlet weak var passwordLabelOutlet: UILabel!
    @IBOutlet weak var passwordTextFieldOutlet: UITextField!
    
    @IBOutlet weak var phoneNumberTextFieldView: UIView!
    @IBOutlet weak var countryFlagImageView: UIImageView!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var countryCodeButtonlOutlet: UIButton!
    @IBOutlet weak var phoneNumberTextFieldOutlet: UITextField!
    
    
    func checkMendatoryFields() -> Bool{
        
        if !self.validateEmail(withString: emailTextFieldOutlet.text!) {
            //Validate email format
            self.showErrorMessage(titleMsg: errorConstant.alertTitle, andMessage: errorConstant.notValidEmailError)
            emailTextFieldOutlet.text = ""
            emailTextFieldOutlet.becomeFirstResponder()
            return false
        }
        return true
    }
    
    func validateEmail(withString email: String) -> Bool {
        let emailString: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailString)
        return emailTest.evaluate(with: email)
    }
    
}

// MARK - TextField Delegate Method

extension LoginView: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch  textField {
            
        case passwordTextFieldOutlet:
            if (passwordTextFieldOutlet.text?.characters.count)! > 0{
                self.passwordLabelOutlet.isHidden = false;
            }
            else{
                self.passwordLabelOutlet.isHidden = true
            }
            passwordTextFieldOutlet.resignFirstResponder()
            phoneNumberTextFieldOutlet.becomeFirstResponder()
            break
            
        case phoneNumberTextFieldOutlet:
            passwordTextFieldOutlet.resignFirstResponder()
            break
            
        default: break
            
        }
        
        return true
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        switch  textField {
            
        case passwordTextFieldOutlet:
            if (newString.characters.count) > 0{
                self.passwordLabelOutlet.isHidden = false;
            }
            else{
                self.passwordLabelOutlet.isHidden = true
            }
            break
            
        default: break
            
        }
        return true
        
    }
    
}
