//
//  AFWrapper.swift
//  Sales Paddock
//
//  Created by 3Embed on 15/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AFWrapper: NSObject {
    
    class func requestGETURL(serviceName: String,
                             success:@escaping (JSON) -> Void,
                             failure:@escaping (NSError) -> Void) {
        
        let strURL = SERVICE.BASE_URL + serviceName
        let headers = ["authorization":"Basic \("user:3Embed".encodeToBase64())"] as [String: String]
        
        Alamofire.request(strURL,
                          method:.get,
                          parameters: nil,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(completionHandler: { response in
            print(response)
            
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                success(resJson)
            }
            if response.result.isFailure {
                let error : NSError = response.result.error! as NSError
                failure(error)
            }
        })
    }
    
    class func requestPOSTURL(serviceName : String,
                              params : [String : Any]?,
                              success:@escaping (JSON) -> Void,
                              failure:@escaping (Error) -> Void) {
        
        
        if NetworkReachabilityManager()?.isReachable == false {
           // Alert.showErrorMessage(message: "No Network available.")
           // self.showErrorMessage(titleMsg: "Error", andMessage: "No Network available.")
            
            let Alert = alert()
            Alert.msg(message: Message.Network_Not_Reachable, title: Message.ERROR)
            return
        }
        
        print(String(format: "%@ :\n", serviceName))
        print(params!)
        
        let strURL = SERVICE.BASE_URL + serviceName
       // let headers = ["authorization":"Basic \("user:3Embed".encodeToBase64())"] as [String: String]
        let headers = ["authorization":"KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj"] as [String: String]
        
        Alamofire.request(strURL,
                          method:.post,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(completionHandler: { response in
                            
                            print(String(format: "\n\n%@ Response :", serviceName))
                            print(response)
                            
                            //Success
                            if response.result.isSuccess {
                                
                                let statusCode = (response.response?.statusCode)!
                                
                                switch statusCode  {
                                    
                                case 200,201:
                                    let resJson = JSON(response.result.value!)
                                    success(resJson)
                                    break
                                    
                            
                                case 204:
                                    
                                    let resJson = JSON(response.result.value!)
                                    let Alert = alert()
                                    Alert.msg(message: Message.ErrorUser, title: Message.ERROR)
                                   success(resJson)
                                    
                                    break

                             
                                case 400:
                                    let resJson = JSON(response.result.value!)
                                    let d :[String :Any] = resJson.dictionaryObject!
                                    
                                    _ = alert().msg(message: d[SignupResponse.errorMessage] as! String, title: Message.ERROR)
                                    
                                    success(nil)
                                     break
                                
                                case 401:
                                    let resJson = JSON(response.result.value!)
                                    let Alert = alert()
                                    Alert.msg(message: Message.ErrornotAuthorised, title: Message.ERROR)
                                    success(resJson)
                                    break
                                    
                               
                                
                                case 406 :
                                    let resJson = JSON(response.result.value!)
                                    _ = alert().msg(message: Message.ErrorHeaderMissng, title: Message.ERROR)
                                    success(resJson)
                                    break
                                    
                                case 412:
                                    let resJson = JSON(response.result.value!)
                                    let d :[String :Any] = resJson.dictionaryObject!
                                    
//                                    _ = alert().msg(message: d[SignupResponse.errorMessage] as! String, title: Message.ERROR)
                                    
                                    success(nil)
                                    
                                    break
                             
                                
                                case 409:
                                    let resJson = JSON(response.result.value!)
                                    let d :[String :Any] = resJson.dictionaryObject!
                                   
                                    _ = alert().msg(message: d[SignupResponse.errorMessage] as! String, title: Message.ERROR)
                                 
                                    success(nil)
                                   
                                    break
                                    
                                
                                
                                default:
                                    let msg: [String: String] = response.result.value as! [String : String]
                                    let str: String = msg[ResponseKey.message]!
                                    print(str)
                                    let Alert = alert()
                                    Alert.msg(message: str, title: Message.ERROR)
                                    let error : Error = response.result.error! as Error
                                    failure(error)
                                    break
                                }
                            
                           
                            
                            }
                            
                        
                            //Fail
                            if response.result.isFailure {
                                let error : Error = response.result.error! as Error
                                failure(error)
                            }
                          
                          
                          })
    }
    
    class func requestGETURLWithParams(serviceName: String,
                                       params:[String : Any]?,
                                       success:@escaping (JSON) -> Void,
                                       failure:@escaping (NSError) -> Void) {
        
        let strURL = "" + serviceName
        let headers = ["authorization":"Basic \("user:3Embed".encodeToBase64())"] as [String: String]
        
        Alamofire.request(strURL,
                          method:.get,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(completionHandler: { response in
                            
            print(response)
                            
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                success(resJson)
            }
            if response.result.isFailure {
                
                let error : NSError = response.result.error! as NSError
                failure(error)
            }
        })
    }
    
    //put
    
    class func requestPutURL(serviceName : String,
                              params : [String : Any]?,
                              success:@escaping (JSON) -> Void,
                              failure:@escaping (Error) -> Void) {
        
        
        if NetworkReachabilityManager()?.isReachable == false {
            // Alert.showErrorMessage(message: "No Network available.")
            // self.showErrorMessage(titleMsg: "Error", andMessage: "No Network available.")
            
            let Alert = alert()
            Alert.msg(message: Message.Network_Not_Reachable, title: Message.ERROR)
            return
        }
        
        print(String(format: "%@ :\n", serviceName))
        print(params!)
        
        let strURL = SERVICE.BASE_URL + serviceName
        // let headers = ["authorization":"Basic \("user:3Embed".encodeToBase64())"] as [String: String]
        let headers = ["authorization":"KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj"] as [String: String]
        
        Alamofire.request(strURL,
                          method:.put,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(completionHandler: { response in
                            
                            print(String(format: "\n\n%@ Response :", serviceName))
                            print(response)
                            
                            //Success
                            if response.result.isSuccess {
                                
                                let statusCode = (response.response?.statusCode)!
                                
                                switch statusCode  {
                                    
                                case 200,201:
                                    
                                    let resJson = JSON(response.result.value!)
                                    success(resJson)

                                     break
                                    
                                default : break
                                }
                                
                            }
                            
                            
        })
    }
    
}

