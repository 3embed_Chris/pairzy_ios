//
//  PreferenceTVE.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 01/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit


// MARK: - UITableViewDelegate,UITableViewDataSource
extension PreferencesTableVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let prefModel = self.myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex]
        switch prefModel.prefTypeId {
        case 1,2,10:
            return  prefModel.options.count
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let picker = tableView.dequeueReusableCell(withIdentifier: String(describing:GenderPickerTableViewCell.self), for: indexPath) as! GenderPickerTableViewCell
        
        if isNotPrefer {
            self.myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex].options[indexPath.row].isOptionSelected = 0
        }
        
        let prefModel = myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex]
        switch prefModel.prefTypeId {
        case 1,2,10:
            if isNotPrefer {
                myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex].options[indexPath.row].isOptionSelected = 0
                
            }
            
            picker.updateCell(optionDetails: myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex].options[indexPath.row])
            
        default:
            break
        }
        
        return picker
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if isNotPrefer {
            
        }else{
            
            
            let oldprefModel = myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex]
            
            switch oldprefModel.prefTypeId {
            case 1,2,10:
                if oldprefModel.prefTypeId == 1 || oldprefModel.prefTypeId == 10 {
                    if(selectedIndex.row != 300) {
                        oldprefModel.options[selectedIndex.row].isOptionSelected = 0
                        oldprefModel.selectedValues = []
                        preferenceVM.selectedValuesObj = []
                        myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex] = oldprefModel
                    }else {
                        //first time only.
                        for (index,_) in oldprefModel.options.enumerated() {
                            oldprefModel.options[index].isOptionSelected = 0
                        }
                         oldprefModel.selectedValues = []
                        myPreference[currentPrefCategoryIndex].arrayOfSubPreference[currentPrefSubCategoryIndex] = oldprefModel
                        preferenceVM.selectedValuesObj = []
                    }
                }
            default: break
            }
            
            selectedIndex = indexPath

            let selectedPrefModel = myPreference[currentPrefCategoryIndex].arrayOfSubPreference[self.currentPrefSubCategoryIndex]
            let selectedOption = selectedPrefModel.options[indexPath.row]
            
            if(selectedOption.isOptionSelected == 0) {
                selectedOption.isOptionSelected = 1
                
                if (oldprefModel.prefTypeId == 1)||(oldprefModel.prefTypeId == 10) {
                    preferenceVM.selectedValuesObj = [selectedOption.optionName]
                }
                else {
                     preferenceVM.selectedValuesObj.append(selectedOption.optionName)
                    preferenceVM.selectedValuesObj = preferenceVM.selectedValuesObj.uniq()
                }
               
            }else if selectedOption.isOptionSelected == 1 {
                selectedOption.isOptionSelected = 0
                preferenceVM.selectedValuesObj = preferenceVM.selectedValuesObj.filter{$0 != selectedOption.optionName}
            }
            
         //
             preferenceVM.selectedPrefId = selectedPrefModel.prefId
            

            self.updateNextBtn()
            self.mainTableView.reloadData()
        }
        
    }
    
    
    
    func updateNextBtn(){
        
        if  preferenceVM.selectedValuesObj.count == 0 {
            nextBtn.isEnabled = false
        }else {
            nextBtn.isEnabled = true
        }
    }
    
}
