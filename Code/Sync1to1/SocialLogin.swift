//
//  SocialLogin.swift
//  Sync1to1
//
//  Created by 3 Embed on 04/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin

protocol socialLoginDelegate {
    //func didLogin(data:SocialProfile)
}

class SocialLogin: NSObject {
    

    var controllerCurrent:LandingVC? = nil
    var socialDelegate:socialLoginDelegate? = nil
    
    static var shareObjSocial: SocialLogin? = nil
    /// Create Shared Instance
    static var shared: SocialLogin {
        if shareObjSocial == nil {
            shareObjSocial = SocialLogin()
        }
        return shareObjSocial!
    }
    
    //Facebook login action
    func faceBookLoginAction(controller : UIViewController) {
        controllerCurrent = controller as? LandingVC
        let fbHandler:FBLoginHandler = FBLoginHandler.sharedInstance()
        fbHandler.delegate = self
        fbHandler.login(withFacebook : controller)
   }
    
    
}

/*****************************************************************/
// MARK: - Facebook Login Delegate
/*****************************************************************/

extension SocialLogin : facebookLoginDelegate {

    func didFacebookUserLogin(withDetails userInfo:NSDictionary) {
    //   let fbProfile = SocialProfile.init(data: userInfo as! [String : Any])
//        APICall.loginAPI(userName: fbProfile.Email, password: fbProfile.Id, loginType: 2, id: fbProfile.Id) { (success) in
//            if success == true {
//                self.controllerCurrent?.dismiss(animated: true, completion: nil)
//            }else{
//                if self.socialDelegate != nil {
//                   self.socialDelegate?.didLogin(data: fbProfile)
//                }
//            }
//        }
    }
    func didFailWithError(_ error: Error?) {

    }
    func didUserCancelLogin() {

    }
}


