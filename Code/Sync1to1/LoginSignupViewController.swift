//
//  LoginSignupViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 01/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class LoginSignupViewController: UIViewController {
    
    struct Constant {
        static let fbEnabeldKey = "Facebook Enabled"
    }
    
    @IBOutlet weak var loginSignupViewOutlet: LoginSignupView!
    @IBOutlet weak var facebookLoginButtonOutlet: BorderedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.checkForTheFbValidation()
    }
    
    func checkForTheFbValidation() //check if the fb is enabled in app config file then the button will be visible else not.
    {
        AppConfigViewModal.updateValuesInPlist()
        guard let appConfigData = AppConfigViewModal.getAppConfigData() else { return }
        guard let fbEnabled = appConfigData[Constant.fbEnabeldKey] as? Bool else { return }
        self.facebookLoginButtonOutlet.isHidden = !fbEnabled
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        self.loginSignupViewOutlet.scrollViewOutlet.layoutIfNeeded()
        loginSignupViewOutlet.createScrollView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    ///When user will tap on Signup button then this mithod will be called
    @IBAction func signupButtonClicked(_ sender: Any) {
        
        performSegue(withIdentifier:IDENTIFIER.SIGNUP,sender: self)
    }
    
    ///When user will tap on Login button then this mithod will be called
    @IBAction func loginButtonClicked(_ sender: Any) {
        
        performSegue(withIdentifier:IDENTIFIER.LOGIN,sender: self)
    }
    
}
