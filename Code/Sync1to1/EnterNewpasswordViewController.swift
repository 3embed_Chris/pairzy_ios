//
//  EnterNewpasswordViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 5/15/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class EnterNewpasswordViewController: UIViewController {

    @IBOutlet var enterPasswordView: EnterNewpassView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backBtnCliked(_ sender: Any) {
        

        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneBtncliked(_ sender: Any) {
      
        
        if ((enterPasswordView.tempPassTextfiled.text?.characters.count)! > 0 || (enterPasswordView.newpassTextfiled.text?.characters.count)! > 0 || (enterPasswordView.confirmTextfiled.text?.characters.count)! > 0){
            
            if enterPasswordView.newpassTextfiled.text == enterPasswordView.confirmTextfiled.text {
                self.sendResetpasswordRequest()
            }
            
        }
        
        
     
    }
    
    
    func sendResetpasswordRequest(){
        
        let progressHUD = ProgressHUD(text: "Reset...")
        self.view.addSubview(progressHUD)
        
        let uuid: String? = AppConstant.deviceId as String?
        
        if let tempPass = enterPasswordView.tempPassTextfiled.text,
            let password = enterPasswordView.confirmTextfiled.text
        {
            
            let mobileNo = UserDefaults.standard.object(forKey: SyUserdefaultKeys.userMobileNo) as! String
            
            let params =    [
                
                RequestParams.MobileNo          : ("\(mobileNo)") as AnyObject,
                RequestParams.otp               : ("\(tempPass)") as AnyObject,
                RequestParams.password          : ("\(password)") as AnyObject,
                
                ] as [String: AnyObject]
            
            print("Custom Signup Params : \(params)")
            
            AFWrapper.requestPutURL(serviceName: APINAMES.RESETPASSWORD,
                                     params: params,
                                     success: { (response) in
                                        progressHUD.hide()
                                        if response == nil{
                                            return
                                        }
                                        let errFlag = response[LoginResponse.errorFlag].boolValue
                                        switch errFlag {
                                        case false:
                                            
                                             self.navigationController?.popToRootViewController(animated: true)
                                            
                                            break
                                        case true:
                                            self.view.showErrorMessage(titleMsg: Message.ERROR, andMessage:response[LoginResponse.errorMessage].stringValue)
                                            break
                                        }
            },
                                     failure: { (Error) in
                                        self.view.showErrorMessage(titleMsg: Message.ERROR, andMessage: Error.localizedDescription)
                                        progressHUD.hide()
            })
        }
        

        
        
        
        
        
        
        
    }
    
    
}
