//
//  SignupViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 02/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//
import Foundation
import UIKit
import Firebase


class SignupViewController: UIViewController
{
    
    struct Constant {
        static let Inch_In_CM:Float = 2.54
        static let numberOfInchesInFeet:Float = 12
        static let heightBigUnit = "ft"
        static let heightSmallUnit = "inches"
        static let dateWithMonthFormat = "MMMM dd, yyyy"
        static let dateFormat = "YYYY-MM-dd"
        static let apiRequiredDateFormat = "dd/MM/yyyy"
        static let cancelButtonTitle = "Cancel"
        static let signingUpMessage = "Signing in..."
        static let constantPushToken = "iOS Simulator Push Token"
        static let currentLocation = "Bangalore"
        static let currentLatitude = "10.344"
        static let currentLongitude = "13.566"
        static let userExistsAlert = "User Already Exists, try Login or Forgot Password"
    }
    
    @IBOutlet weak var signupViewOutlet: SignupView!
    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var termAndConditionsView: UIView!
    @IBOutlet weak var displayDatelbl: UILabel!
    @IBOutlet weak var displayHeight: UILabel!
    var progressHUD:ProgressHUD?
    
    @IBOutlet weak var heightPickerViewOutlet: UIView!
    @IBOutlet weak var heightPickerOutlet: UIPickerView!
    @IBOutlet weak var datePickerViewOutlet: UIView!
    @IBOutlet weak var datePickerOutlet: UIDatePicker!
    
    var prefSex: String = ""
    var dateOfBirth = Date()
    var feet = "1 ft"
    var centimeter = "0 inches"
    var centimetersValue = [String]()
    var feetValue = [String]()
    
    var dataForHeightPicker:[String]
    {
        var arrayHeights = [String]()
        for index in 0...120 {
            let cmValue = Float(index)*Constant.Inch_In_CM
            arrayHeights.append("\(HeightConverter.getFeetAndInchesFrom(centimeters:cmValue))  \(cmValue) cm")
            centimetersValue.append("\(cmValue)")
            feetValue.append("\(HeightConverter.getFeetAndInchesFrom(centimeters:cmValue))")
        }
        return arrayHeights
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _ = dataForHeightPicker
        self.signupViewOutlet.userDetailsViewOutlet.tableViewOutlet = self.tableViewOutlet
        displayHeight.text = "\(feetValue[0]) / \(centimetersValue[0]) cm"
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            
            self.signupViewOutlet.userDetailsViewOutlet.countryCodeLabel.text = VNHCountryPicker.dialCode(code: countryCode).dialCode
            self.signupViewOutlet.userDetailsViewOutlet.countryFlagImageView.image = VNHCountryPicker.dialCode(code: countryCode).flag
        }
        
        let leftBarItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back_btn_off"), style:.plain, target: self, action: #selector(backAction))
        self.navigationItem.setLeftBarButton(leftBarItem, animated: true)
        
        let FormatDate = DateFormatter()
        FormatDate.dateFormat = Constant.dateFormat
        FormatDate.dateStyle = .medium
        FormatDate.timeStyle = .none
        
        self.displayDatelbl.text = FormatDate.string(from:  datePickerOutlet.date)
        
    }
    
    func backAction() {
        
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.signupViewOutlet.userDetailsViewOutlet.viewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.signupViewOutlet.userDetailsViewOutlet.viewWillDisappear()
        if (self.progressHUD != nil){
            self.progressHUD!.hide()
        }
    }
    
    ///  when user touches view textField editing is ended.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: - All Button Actions
    
    ///When user will tap on date of birth button then this mithod will be called
    @IBAction func dateOfBirthButtonClicked(_ sender: Any) {
        datePickerViewOutlet.isHidden = false
        self.signupViewOutlet.endEditing(true)
        heightPickerViewOutlet.isHidden = true
    }
    
    ///When user will tap on gender button then this mithod will be called
    @IBAction func genderButtonClicked(_ sender: Any) {
        
        self.signupViewOutlet.endEditing(true)
        heightPickerViewOutlet.isHidden = true
        datePickerViewOutlet.isHidden = true
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let closure = { (action: UIAlertAction!) -> Void in
            let index = alert.actions.index(of: action)
            
            if index != nil {
                self.signupViewOutlet.userDetailsViewOutlet.genderTextField.text = Gender.GenderArray[index!]
                self.signupViewOutlet.userDetailsViewOutlet.genderLabel.isHidden = false
                
            }
        }
        
        for i in 0 ..< Gender.GenderArray.count {
            alert.addAction(UIAlertAction(title: Gender.GenderArray[i], style: .default, handler: closure))
        }
        
        alert.addAction(UIAlertAction(title: Constant.cancelButtonTitle, style: .destructive, handler: {(_) in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    ///When user will tap on height button then this mithod will be called
    @IBAction func heightButtonClicked(_ sender: Any) {
        
        self.signupViewOutlet.endEditing(true)
        datePickerViewOutlet.isHidden = true
        heightPickerViewOutlet.isHidden = false
    }
    
    ///When user will tap on height Cancel button then this mithod will be called
    
    @IBAction func heightCancelButtonClicked(_ sender: Any) {
        
        heightPickerViewOutlet.isHidden = true
    }
    
    ///When user will tap on height Done button then this mithod will be called
    
    @IBAction func heightDoneButtonClicked(_ sender: Any) {
        
        let selectedRow = self.heightPickerOutlet.selectedRow(inComponent: 0)
        signupViewOutlet.userDetailsViewOutlet.heighTextField.text = "\(feetValue[selectedRow]) / \(centimetersValue[selectedRow]) cm"
        signupViewOutlet.userDetailsViewOutlet.heightLabel.isHidden = false
        heightPickerViewOutlet.isHidden = true
    }
    
    ///When user will tap on country code button then this mithod will be called
    @IBAction func countryCodeButtonClicked(_ sender: Any) {
        
        self.signupViewOutlet.endEditing(true)
        self.performSegue(withIdentifier:IDENTIFIER.CountryPicker, sender: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == IDENTIFIER.CountryPicker
        {
            let controller : VNHCountryPicker = segue.destination as! VNHCountryPicker
            controller.delegate = self
        }
        else if segue.identifier == IDENTIFIER.MobileVC
        {
            let userData = sender as! UserData
            let controller = segue.destination as! MobileViewController
            controller.userData = userData
        }
    }
    
    ///When user will tap on terms and conditions button then this mithod will be called
    @IBAction func termAndConditionButtonClicked(_ sender: Any) {
        
        self.signupViewOutlet.endEditing(true)
    }
    
    
    ///When user will tap on check Box button then this mithod will be called
    @IBAction func checkBoxButtonClicked(_ sender: Any) {
        
        if !signupViewOutlet.userDetailsViewOutlet.checked {
            self.signupViewOutlet.checkBoxButtonOutlet.setImage(#imageLiteral(resourceName: "checkButtonOn"), for: .normal)
            signupViewOutlet.userDetailsViewOutlet.checked = true
        }
        else {
            self.signupViewOutlet.checkBoxButtonOutlet.setImage(#imageLiteral(resourceName: "checkButtonOff"), for: .normal)
            signupViewOutlet.userDetailsViewOutlet.checked = false
        }
        self.signupViewOutlet.endEditing(true)
    }
    
    ///When user will tap on next button then this mithod will be called
    @IBAction func nextButtonClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let isValid: Bool = self.signupViewOutlet.userDetailsViewOutlet.checkMendatoryFields()
        if isValid == true {
            self.sendSignUpRequest()
        }
    }
    
    ///When user will tap on date picker done button then this mithod will be called
    @IBAction func datePickerDoneButtonClicked(_ sender: Any) {
        
        let FormatDate = DateFormatter()
        FormatDate.dateFormat = Constant.dateWithMonthFormat
        signupViewOutlet.userDetailsViewOutlet.dateOfBirthTextField.text = FormatDate.string(from: datePickerOutlet.date)
        
        FormatDate.dateFormat = Constant.apiRequiredDateFormat
        self.dateOfBirth = datePickerOutlet.date
        
        let isValid: Bool = self.signupViewOutlet.userDetailsViewOutlet.checkUserAge()
        if isValid == false {
            
            self.view.showErrorMessage(titleMsg: Message.ERROR, andMessage: Message.AgeMessage)
            datePickerViewOutlet.isHidden = true
            signupViewOutlet.userDetailsViewOutlet.dateOfBirthTextField.text = ""
            return
        }
        self.signupViewOutlet.userDetailsViewOutlet.dateOfBirthLabel.isHidden = false
        datePickerViewOutlet.isHidden = true
    }
    
    ///When user will tap on date picker cancel button then this mithod will be called
    @IBAction func datePickerCancelButtonClicked(_ sender: Any) {
        
        datePickerViewOutlet.isHidden = true
    }
    
    @IBAction func datePickerChange(_ sender: Any) {
        
        let FormatDate = DateFormatter()
        FormatDate.dateFormat = Constant.dateFormat
        FormatDate.dateStyle = .medium
        FormatDate.timeStyle = .none
        
        self.displayDatelbl.text = FormatDate.string(from:  datePickerOutlet.date)
    }
    
    func getUserDetailsHeightFeetOptions() -> [Any] {
        var feetOptions = [Any]()
        for i in 1..<10 {
            feetOptions.append(Int(i))
        }
        return feetOptions
    }
    
    func getUserDetailsHeightInchesOptions() -> [Any] {
        var inchesOptions = [Any]()
        for i in 0..<12 {
            inchesOptions.append(Int(i))
        }
        return inchesOptions
    }
    
    /// Signup web service
    func sendSignUpRequest() {
        
        progressHUD = ProgressHUD(text:Constant.signingUpMessage)
        self.view.addSubview(progressHUD!)
        
        var pushToken = Constant.constantPushToken
        if let token = UserDefaults.standard.value(forKey: SyUserdefaultKeys.kNSUPushNotificationTokenKey) as? String
        {
            pushToken = token
        }
        let FormatDate = DateFormatter()
        FormatDate.dateFormat = Constant.apiRequiredDateFormat
        let dateOfBirth = FormatDate.string(from: self.dateOfBirth)
        
        let firstName = signupViewOutlet.userDetailsViewOutlet.firstNameTextField.text
        let gender = signupViewOutlet.userDetailsViewOutlet.genderTextField.text
        let pswd = signupViewOutlet.userDetailsViewOutlet.passwordTextField.text
        let mobNumber = signupViewOutlet.userDetailsViewOutlet.phoneNumberTextField.text
        let countryCode = signupViewOutlet.userDetailsViewOutlet.countryCodeLabel.text
        let emailID = signupViewOutlet.userDetailsViewOutlet.emailIDTextField.text
        
        if (gender == Gender.GenderArray[0]) {
            prefSex = Gender.GenderArray[1]
        }
        else if (gender == Gender.GenderArray[1]) {
            prefSex = Gender.GenderArray[0]
        }
        else {
            prefSex = Gender.GenderArray[2]
        }
        
        let selectedRow = self.heightPickerOutlet.selectedRow(inComponent: 0)
        let height = self.centimetersValue[selectedRow]
        
        let params =    [
            ServiceInfo.Fname               :firstName!,
            ServiceInfo.DateOfBirth         :dateOfBirth,
            ServiceInfo.Password            :pswd!,
            ServiceInfo.Gender              :gender!,
            ServiceInfo.MobileNumber        :"\(countryCode!)\(mobNumber!)",
            ServiceInfo.PushToken           :pushToken,
            ServiceInfo.DeviceType          :"1",
            ServiceInfo.Email               :emailID!,
            ServiceInfo.DeviceModel         :UIDevice.current.systemName,
            ServiceInfo.Height              :height,
            ServiceInfo.DeviceMake          :UIDevice.current.model,
            ServiceInfo.LocationName        :Constant.currentLocation,
            ServiceInfo.Latitude            :Constant.currentLatitude,
            ServiceInfo.Longitude           :Constant.currentLongitude,
            ]  as [String:Any]
        
        let userData = UserData.fetchUserDataObj(userDataDictionary: params, existingUserData: nil)
        self.signInUser(withUserData: userData)
//        self.performSegue(withIdentifier: IDENTIFIER.MobileVC, sender: userData)

    }
    
    func signInUser(withUserData userData : UserData) {
        let phoneNum = signupViewOutlet.userDetailsViewOutlet.phoneNumberTextField.text!
        let pswd = signupViewOutlet.userDetailsViewOutlet.passwordTextField.text
        let countryCode = signupViewOutlet.userDetailsViewOutlet.countryCodeLabel.text
        let userPhoneNumber = "\(String(describing: countryCode!))\(phoneNum)"
        let emailID = signupViewOutlet.userDetailsViewOutlet.emailIDTextField.text

        Auth.auth().createUser(withEmail: emailID!, password: pswd!) { (user, error) in
            if (error == nil) {
                PhoneAuthProvider.provider().verifyPhoneNumber(userPhoneNumber) { (verificationID, error) in
                    if let error = error {
                        let controller = UIAlertController(title: "Alert!!!", message: error.localizedDescription, preferredStyle: .alert)
                        controller.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (okAction) in
                            controller.dismiss(animated: true, completion: nil)
                        }))
                        self.deleteUser()
                        self.progressHUD?.hide()
                        self.present(controller, animated: true, completion: nil)
                        
                        return
                    }
                    self.progressHUD?.hide()
                    UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                    self.performSegue(withIdentifier: IDENTIFIER.MobileVC, sender: userData)
                }
            } else {
                print(error!)
                self.progressHUD?.hide()
                let controller = UIAlertController(title: "Alert!!!", message: Constant.userExistsAlert, preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (okAction) in
                    controller.dismiss(animated: true, completion: nil)
                }))
                self.present(controller, animated: true, completion: nil)
                print("user login failed")
            }
        }
    }
    
    func deleteUser(){
        let user = Auth.auth().currentUser
        
        user?.delete { error in
            if let error = error {
                // An error happened.
                let controller = UIAlertController(title: "Alert!!!", message: error.localizedDescription, preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (okAction) in
                    controller.dismiss(animated: true, completion: nil)
                }))
                self.present(controller, animated: true, completion: nil)
                
            } else {
                // Account deleted.
                print("account deleted")
            }
        }
    }
}

// MARK: - Country  code Picker Delegate
extension SignupViewController : VNHCountryPickerDelegate{
    
    func didPickedCountry(country: VNHCounty) {
        self.signupViewOutlet.userDetailsViewOutlet.countryCodeLabel.text = country.dialCode
        self.signupViewOutlet.userDetailsViewOutlet.countryFlagImageView.image = country.flag
    }
}

extension SignupViewController:UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return self.dataForHeightPicker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return self.dataForHeightPicker[row]
    }
}

extension SignupViewController:UIPickerViewDelegate{
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.displayHeight.text =  "\(feetValue[row]) / \(centimetersValue[row]) cm"
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat
    {
        return 60
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
    {
        let picView   = UIView.init(frame: CGRect.init(x: 0, y:0, width:UIScreen.main.bounds.size.width, height:59))
        picView.backgroundColor = UIColor(hex:0xF2F2F2).withAlphaComponent(1.0)
        
        let label = UILabel.init(frame: CGRect.init(x:0, y:0, width:UIScreen.main.bounds.size.width , height: 59))
        label.text =  self.dataForHeightPicker[row]
        label.textAlignment = .center
        picView.addSubview(label)
        return picView
    }
}
