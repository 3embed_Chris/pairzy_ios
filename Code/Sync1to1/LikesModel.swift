//
//  LikesModel.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 25/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class LikesModel: NSObject {
    var countryCode = ""
    var creation : Double = 0.0
    var dateOfBirth = Date()
    var emailId    = ""
    var firstname  = ""
    var height:Float =  0
    var mobileNum = ""
    var opponedId = ""
    var profilePic = ""
    
    init(data:[String:Any]) {
        if let countryCode = data[ApiResponseKeys.countryCode] as? String {
          self.countryCode = countryCode
        }
        if let creation = data[ApiResponseKeys.creation] as? Double {
            self.creation = creation/1000
        }
        if let dateOfBirth = data[ApiResponseKeys.dateOfBirth] as? Int {
            self.dateOfBirth = Helper.dateFromMilliseconds(timestamp:String(dateOfBirth))
        }
        if let emailId = data[ApiResponseKeys.emailId] as? String {
            self.emailId = emailId
        }
        if let firstName = data[ApiResponseKeys.firstName] as? String {
            
            self.firstname = firstName
        }
        if let height = data[ApiResponseKeys.height] as? Float {
            
            self.height = height
        }
        if let mobileNumber = data[ApiResponseKeys.mobileNumber] as? String {
            
            self.mobileNum = mobileNumber
        }
        if let profilepic = data[ApiResponseKeys.profilePic] as? String {
            self.profilePic = profilepic
            
        }
      
    }
    
}
