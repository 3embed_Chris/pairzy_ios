//
//  LoginSignupView.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 02/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
let SCREEN_WIDTH:  Int = Int(UIScreen.main.bounds.size.width)

class LoginSignupView: UIView {
    
    @IBOutlet weak var signupButtionView: UIView!
    @IBOutlet weak var loginButtonView: UIView!
    @IBOutlet weak var scrollViewOutlet: UIScrollView!
    @IBOutlet weak var pageViewoutlet: UIPageControl!
    var timer: Timer!
    var maximumOffSet: CGFloat?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        loginButtonView.layer.borderColor = UIColor.white.cgColor
        let xPosition = UIScreen.main.bounds.width * CGFloat(2)
        self.maximumOffSet = xPosition
    }
    
    /*
     Description:
     Arguments:
     Return Values:
     */
    
    func createScrollView() {
        
        let p = Int(self.pageViewoutlet.numberOfPages)
        
        self.scrollViewOutlet.contentSize = CGSize(width: CGFloat(SCREEN_WIDTH * p), height: CGFloat(self.scrollViewOutlet.frame.size.height))
        
        self.pageViewoutlet.numberOfPages = ScrollMsg.MsgArray1.count
        self.scrollViewOutlet.delegate = self
        self.pageViewoutlet.currentPage = 0
        
        for i in 0..<ScrollMsg.MsgArray1.count {
            self.createPage(withImage: ScrollMsg.MsgArray1[i] , forPage: i, andMsg: ScrollMsg.MsgArray2[i] )
        }
        
        self.addTimerForAutoScroll()
    }
    
    func createPage(withImage msg1: String, forPage page: Int, andMsg msg2: String) {
        
        var label1 = UILabel(frame: CGRect(x: CGFloat(SCREEN_WIDTH * page + 20), y: CGFloat(120), width: CGFloat(SCREEN_WIDTH-40), height: CGFloat(20)))
        
        if page == 2 {
            label1 = UILabel(frame: CGRect(x: CGFloat(SCREEN_WIDTH * page + 20), y: CGFloat(120), width: CGFloat(SCREEN_WIDTH-40), height: CGFloat(40)))
        }
        
        label1.font = UIFont.init(name: FontFiamly.gothamBook, size: CGFloat(20))
        label1.textColor = UIColor.white
        label1.textAlignment = .center
        label1.numberOfLines = 0
        label1.text = msg1
        
        let label2 = UILabel(frame: CGRect(x: CGFloat(SCREEN_WIDTH * page + 20), y: CGFloat(170), width: CGFloat(SCREEN_WIDTH - 40), height: CGFloat(35)))
        
        label2.textColor = UIColor.white
        
        label2.font = UIFont.init(name: FontFiamly.gothamBook, size: CGFloat(16))
        label2.textAlignment = .center
        label2.numberOfLines = 0
        label2.text = msg2
        
        self.scrollViewOutlet.addSubview(label1)
        self.scrollViewOutlet.addSubview(label2)
    }
    
    func addTimerForAutoScroll()
    {
        self.timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector:#selector(onTimer), userInfo: nil, repeats: true)
    }
    
    func onTimer(){
        if self.scrollViewOutlet.contentOffset.x < self.maximumOffSet!{
            scrollViewOutlet.setContentOffset(CGPoint(x: scrollViewOutlet.contentOffset.x+scrollViewOutlet.frame.size.width, y: 0), animated: true)
        }else{
            self.timer.invalidate()
        }
    }
}

extension LoginSignupView: UIScrollViewDelegate{
    
    
    func scrollViewDidScroll(_ sView: UIScrollView) {
        let pageWidth: Int = Int(self.scrollViewOutlet.frame.size.width)
        
        // you need to have a **iVar** with getter for scrollView
        let fractionalPage: Float = Float (self.scrollViewOutlet.contentOffset.x) / Float (pageWidth)
        let pageCurrent: Int = lround(Double(fractionalPage))
        self.pageViewoutlet.currentPage = pageCurrent
        
    }
    
    
}
