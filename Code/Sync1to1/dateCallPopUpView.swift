//
//  dateCallPopUpView.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 04/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

protocol DatePopUpDelegate:class { //getting delegate methods for the button actions
    func VoiceCallDateBtnPressed()
    func VideoCallDateBtnPressed()
    func physicalDateBtnPressed()
    func canceled()
}


class dateCallPopUpView: UIView {
    
    weak var  datePopUpDelegate : DatePopUpDelegate?
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var voiceCallLabel: UILabel!
    @IBOutlet weak var inPersonDateLabel: UILabel!
    @IBOutlet weak var videoCallLabel: UILabel!
    
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.removeViewWithAnimation()
    }
    
    
    @IBAction func VoiceCallDateAction(_ sender: Any) {
        self.datePopUpDelegate?.VoiceCallDateBtnPressed()
        self.removeViewWithAnimation()
    }
    
    @IBAction func videoCallAction(_ sender: Any) {
        self.datePopUpDelegate?.VideoCallDateBtnPressed()
        self.removeViewWithAnimation()
    }
    
    @IBAction func physicalDateAction(_ sender: Any) {
        self.datePopUpDelegate?.physicalDateBtnPressed()
        self.removeViewWithAnimation()
    }
    
    // method to load reasons xib.
    func loadPopUpView() {
        // ContactUS name of the XIB.
        Bundle.main.loadNibNamed("dateCallPopup", owner:self, options:nil)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.26)
        self.subView.layer.cornerRadius = 20
        Helper.setShadow(sender: self.subView)
        self.mainView.frame = self.bounds
        self.addSubview(self.mainView)
        self.mainView.layoutIfNeeded()
        voiceCallLabel.text = StringConstants.voiceDate()
        inPersonDateLabel.text = StringConstants.inPersionDate()
        videoCallLabel.text = StringConstants.videoDate()
        
        self.subView.transform = CGAffineTransform(scaleX: 0.2, y:0.2)
        UIView.animate(withDuration:1.0, delay: 0.0, usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.5, options: [], animations:
            {
                self.subView.transform = CGAffineTransform(scaleX: 1.0, y:1.0)
        }, completion:nil)
    }
    
    func removeViewWithAnimation() {
        self.subView.transform = CGAffineTransform.identity
        UIView.animate(withDuration: 0.1, animations: {
            self.subView.transform = CGAffineTransform(scaleX: 1.01, y:1.01)
        } ,completion:{ (finished) in
            if(finished) {
                self.alpha = 1.0
                UIView.animate(withDuration:0.5, animations: {
                    self.alpha = 0
                    self.subView.transform = CGAffineTransform(scaleX: 0.2, y:0.2)
                }, completion: { (finished) in
                    if(finished) {
                        self.removeFromSuperview()
                        self.datePopUpDelegate?.canceled()
                    }
                })
            }
        })
    }
}
