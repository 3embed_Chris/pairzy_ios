//
//  ResetpasswordViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 5/15/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit



class ResetpasswordViewController: UIViewController {
    
    var mobileNO : String = ""
    @IBOutlet var resetView: ResetpasswordView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        resetView.phoneNUmberTextfiledoutlet.text = mobileNO
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            
            resetView.countrycodeLabel.text = VNHCountryPicker.dialCode(code: countryCode).dialCode
           resetView.counrtyFlagimageicon.image = VNHCountryPicker.dialCode(code: countryCode).flag
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///  when user touches view textField editing is ended.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func backBtnCliked(_ sender: Any) {
       _ =  self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func countrycodebtnCliked(_ sender: Any) {
        
        self.performSegue(withIdentifier: IDENTIFIER.ResetToCountrypicker, sender: self)
}
    
    func validateEmail(withString email: String) -> Bool {
        let emailString: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailString)
        return emailTest.evaluate(with: email)
    }
    
    @IBAction func nextButtonCliked(_ sender: Any) {
        
        if ((resetView.firstNameTextfiled.text?.characters.count)! > 0 || (resetView.emailTextfiled.text?.characters.count)! > 0 ){
            
            
            if self.validateEmail(withString: resetView.emailTextfiled.text!) == false {
                
                    //Validate email format
                    let Alert = alert()
                    Alert.msg(message: Message.EmailMsg, title: Message.ERROR)

                    resetView.emailTextfiled.text = ""
                    resetView.emailTextfiled.becomeFirstResponder()
            }else{
            
                self.sendResetpassRequest()}
        }
        
    }
    
    func sendResetpassRequest() {
        
        let progressHUD = ProgressHUD(text: "Login...")
        self.view.addSubview(progressHUD)
        
        let uuid: String? = AppConstant.deviceId as String?
        
        if let username = resetView.firstNameTextfiled.text,
            let email = resetView.emailTextfiled.text,
            let countryCode = resetView.countrycodeLabel.text,
            let mobielNum = resetView.phoneNUmberTextfiledoutlet.text
        {
            let curDateTime = AppConstant.getCurrentTimeWithdate()
            let mobileNo = ("\(countryCode)\(mobielNum)")
            let params =    [
                
                RequestParams.firstname         : username as AnyObject,
                RequestParams.email             : email as AnyObject,
                RequestParams.MobileNo          : mobileNo as AnyObject,
                

                ] as [String: AnyObject]
            
            print("Custom Signup Params : \(params)")
            
            AFWrapper.requestPOSTURL(serviceName: APINAMES.FORGOTPASSWORD,
                                     params: params,
                                     success: { (response) in
                                        progressHUD.hide()
                                        if response == nil{
                                            return
                                        }
                                        let errFlag = response[LoginResponse.errorFlag].boolValue
                                        switch errFlag {
                                        case false:
                                            self.performSegue(withIdentifier:IDENTIFIER.ResetTOnewpassowrd, sender: self)
                                            
                                            break
                                        case true:
                                            self.view.showErrorMessage(titleMsg: Message.ERROR, andMessage:response[LoginResponse.errorMessage].stringValue)
                                            break
                                        }
            },
                                     failure: { (Error) in
                                        self.view.showErrorMessage(titleMsg: Message.ERROR, andMessage: Error.localizedDescription)
                                        progressHUD.hide()
            })
        }
        
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == IDENTIFIER.ResetToCountrypicker
        {
            let controller : VNHCountryPicker = segue.destination as! VNHCountryPicker
            controller.delegate = self
        }
    }
    
}


//MARK:- Country Code Picker Delegate

extension ResetpasswordViewController : VNHCountryPickerDelegate{
    
    func didPickedCountry(country: VNHCounty) {
        resetView.countrycodeLabel.text = country.dialCode
        resetView.counrtyFlagimageicon.image = country.flag
    }
}

