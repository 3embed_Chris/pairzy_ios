//
//  MyProfileVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 22/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
class MyProfileVM: NSObject{
    
    let disposeBag = DisposeBag()
    let MyProfileVM_resp = PublishSubject<ResponseType>()
    
    var profileAPICall = ProfileAPICalls()
    var profileDetails = Helper.getMyProfile()
    var firstImage = 0
    
    enum ResponseType:Int{
        case updateProfile = 0
        case getProfile = 1
    }
    
    
    func updateProfile(params: [String:Any]){
        
        
        
        let requestData = RequestModel().updateProfile(detail: params)
        profileAPICall.updateProfile(requestData: requestData)
        Helper.showProgressIndicator(withMessage: StringConstants.Loading())
        profileAPICall.subject_response
            .subscribe(onNext: { response in
                Helper.hideProgressIndicator()
                self.MyProfileVM_resp.onNext(.updateProfile)
            }, onError: { error in
                Helper.hideProgressIndicator()
                Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
            })
            .disposed(by:disposeBag)
        
        
    }
    
    
    /// gets user profile
    func getProfile(){
        let apiCall = ProfileAPICalls()
        apiCall.requestForGetProfle()
        //Helper.showProgressIndicator(withMessage: "")
        apiCall.subject_response
            .subscribe(onNext: { response in
                Helper.hideProgressIndicator()
                guard let statuscode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode) else { return }
                self.handleGetProfileAPIResponse(response: response)
            }, onError: { error in
                Helper.hideProgressIndicator()
                Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
            })
            .disposed(by:disposeBag)
    }
    
    func handleGetProfileAPIResponse(response:ResponseModel){
        
        switch response.statusCode{
        case 200:
            guard let statuscode:API.ErrorCode = API.ErrorCode(rawValue: response.statusCode) else { return }
            if let profileResponse = response.response["data"] as? [String:Any] {
                Helper.saveMyProfile(data: profileResponse)
                self.profileDetails = Profile.fetchprofileDataObj(userDataDictionary:profileResponse)
            }
            
            
            let dbObj = Database.shared
            if self.profileDetails.isHiddenUserAge == 0 {
                dbObj.userDataObj.showMyAge = false
            }else{
                dbObj.userDataObj.showMyAge = true
            }
            
            if self.profileDetails.isHiddenDistance == 0 {
                dbObj.userDataObj.showMyDist = false
            }else{
                dbObj.userDataObj.showMyDist = true
            }
            
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            self.MyProfileVM_resp.onNext(.getProfile)
            
            
            break
        default:
            break
        }
    }
    
}
