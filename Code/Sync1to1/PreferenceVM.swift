//
//  PreferenceVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 28/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PreferenceVM: NSObject {
    
    let disposeBag = DisposeBag()
    let preferenceVM_resp = PublishSubject<Bool>()
    var selectedPrefId = ""
    var selectedValuesObj:[String] = []
    

    //updating selectedValues to server.
    func updatePreference() {
       
        let params = [ServiceInfo.preferenceId: selectedPrefId,
                      ServiceInfo.values: selectedValuesObj] as [String:Any]
        Helper.showProgressIndicator(withMessage: StringConstants.updating())
        API.requestPatchURL(serviceName:APINAMES.Preferences, withStaticAccessToken:false, params:params, success: { (response) in
            Helper.hideProgressIndicator()
            self.preferenceVM_resp.onNext(true)
        }, failure:{ (error) in
                 Helper.hideProgressIndicator()
        })
    }
    

}
