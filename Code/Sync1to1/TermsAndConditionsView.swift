//
//  TermsAndConditionsView.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 11/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class TermsAndConditionsView: UIView {

    @IBOutlet weak var termsAndConditionsTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupTNCandPPBody()
    }
    
     func setupTNCandPPBody() {
        
        termsAndConditionsTextView.delegate = self
        
        let termsString = Message.tAndC
        
        let attributedString = NSMutableAttributedString(string: termsString)
        
        let multipleAttributes = [
            NSForegroundColorAttributeName: UIColor(hex:0x00A8FF),
            NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue] as [String : Any]
        
        let foundRange = attributedString.mutableString.range(of: Message.termsAndCondition)
        attributedString.setAttributes(multipleAttributes, range:foundRange)
        attributedString.addAttribute(NSLinkAttributeName, value: SERVICE.TermAndCondition, range: foundRange)
        termsAndConditionsTextView.attributedText = attributedString
    }
}

extension TermsAndConditionsView: UITextViewDelegate{


    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        return true
    }
    
    
}
