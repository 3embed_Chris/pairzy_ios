//
//  EnterNewpassView.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 5/15/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class EnterNewpassView: UIView {

    @IBOutlet weak var tempPasswordLabel: UILabel!
    
    @IBOutlet weak var tempPassTextfiled: UITextField!
    @IBOutlet weak var newPassLabel: UILabel!

    @IBOutlet weak var newpassTextfiled: UITextField!
    @IBOutlet weak var confirmLabel: UILabel!
    
    @IBOutlet weak var confirmTextfiled: UITextField!
}





// MARK - TextField Delegate Method

extension EnterNewpassView: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch  textField {
            
        case tempPassTextfiled:
            if (tempPassTextfiled.text?.characters.count)! > 0{
                self.tempPasswordLabel.isHidden = false
            }
            else{
                self.tempPasswordLabel.isHidden = true
            }
            tempPassTextfiled.resignFirstResponder()
            newpassTextfiled.becomeFirstResponder()
            break
            
        case newpassTextfiled:
            if (newpassTextfiled.text?.characters.count)! > 0{
                self.newPassLabel.isHidden = false
            }
            else{
                self.newPassLabel.isHidden = true
            }
            newpassTextfiled.resignFirstResponder()
            confirmTextfiled.becomeFirstResponder()
            break
            
        case confirmTextfiled:
            if (confirmTextfiled.text?.characters.count)! > 0 {
                self.confirmLabel.isHidden = false
            }else{
                self.confirmLabel.isHidden = true
            }
            confirmTextfiled.resignFirstResponder()
        default: break
            
        }
        
        return true
        
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        switch  textField {
            
        case tempPassTextfiled:
            if (newString.characters.count) > 0{
                self.tempPasswordLabel.isHidden = false;
            }
            else{
                self.tempPasswordLabel.isHidden = true
            }
            break
            
            
        case newpassTextfiled:
            if (newString.characters.count) > 0  {
                self.newPassLabel.isHidden = false
            }else{
                self.newPassLabel.isHidden = true
            }
            break
            
            
        case confirmTextfiled:
            if (newString.characters.count) > 0 {
                self.confirmLabel.isHidden = false
            }else{
                self.confirmLabel.isHidden = true
            }
            
        default: break
            
        }
        return true
        
    }
    
}
