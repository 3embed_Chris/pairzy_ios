//
//  SettingsVM.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 08/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SettingsVM: NSObject {
    
    let disposeBag = DisposeBag()
      let settingsVM_resp = PublishSubject<Bool>()
 
    
    
    /// gets the preferences
    func getPreferences(){
        let apiCall = Authentication()
        apiCall.requestForGetPreference()
        //Helper.showProgressIndicator(withMessage: StringConstants.GettiingPrefrences)
        apiCall.subject_response
            .subscribe(onNext: { response in
               // self.handleGetPreferenceResponse(responseModel: response)
            }, onError: { error in
                // Helper.hideProgressIndicator()
                
                Helper.showAlertWithMessageOnwindow(withTitle: StringConstants.error(), message: error.localizedDescription)
            })
            .disposed(by:disposeBag)
    }
    
   
    
    
}
