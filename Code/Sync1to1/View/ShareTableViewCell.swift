//
//  ShareTableViewCell.swift
//  Sync1to1
//
//  Created by Dinesh Guptha Bavirisetti on 09/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class ShareTableViewCell: UITableViewCell {

    @IBOutlet weak var shareTitleLabelOutlet: UILabel!
    @IBOutlet weak var shareIconImageOutlet: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
