//
//  MobileViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 09/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Firebase

class MobileViewController: UIViewController {
    
    struct Constant {
        static let totalTime = 30.0
        static let elapsingTime = 1.0
        static let authKeyidentifier = "authVerificationID"
        static let alertText = "Alert!!!"
        static let okayActionText = "Okay"
    }
    
    @IBOutlet weak var mobileCodeView: MobileView!
    @IBOutlet weak var resendCodeButtonOutlet: UIButton!
    
    var timer:Timer!
    var userData:UserData!
    var remainingTime:Double!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mobileCodeView.verificationCodeDelegate = self
        
        let leftBarItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back_btn_off"), style:.plain, target: self, action: #selector(backAction))
        self.navigationItem.setLeftBarButton(leftBarItem, animated: true)
    }
    
    func activateTimer()
    {
        remainingTime = Constant.totalTime
        if (timer != nil)
        {
            timer.invalidate()
        }
        timer = Timer.scheduledTimer(timeInterval: Constant.elapsingTime, target: self, selector: #selector(MobileViewController.countDown), userInfo: nil, repeats: true)
    }
    
    
    func countDown()
    {
        if remainingTime > 0{
            remainingTime = remainingTime - Constant.elapsingTime
            self.resendCodeButtonOutlet.isEnabled = false
            self.resendCodeButtonOutlet.setTitle("resend code in 00:\(Int(remainingTime))", for: .normal)
            self.resendCodeButtonOutlet.setTitleColor(UIColor.lightGray, for: .normal)
        }
        else{
            timer.invalidate()
            self.resendCodeButtonOutlet.isEnabled = true
            self.resendCodeButtonOutlet.setTitle("resend code", for: .normal)
            var color:UIColor!
            if #available(iOS 10.0, *) {
                color = UIColor(displayP3Red: 0.0, green: 0.659, blue: 1.00, alpha: 1.0)
            } else {
                color = UIColor.blue
            }
            self.resendCodeButtonOutlet.setTitleColor(color, for: .normal)
        }
    }
    
    func backAction ()
    {
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.alpha = 1.0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///  when user touches view textField editing is ended.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.mobileCodeView.firstcCodeTextField.becomeFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    
    
    func deleteUser(){
        let user = Auth.auth().currentUser
        
        user?.delete { error in
            if let error = error {
                // An error happened.
                let controller = UIAlertController(title: "Alert!!!", message: error.localizedDescription, preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (okAction) in
                    controller.dismiss(animated: true, completion: nil)
                }))
                self.present(controller, animated: true, completion: nil)
                
            } else {
                // Account deleted.
                print("account deleted")
            }
        }
    }
    
    ///When user will tap on Done button then this mithod will be called
    @IBAction func DoneButtonClicked(_ sender: Any) {
        
        let progressHUD = ProgressHUD(text: "Verifying...")
        self.activateTimer()
        self.view.addSubview(progressHUD)
        
        if let str1 = mobileCodeView.firstcCodeTextField.text, let str2 = mobileCodeView.secondCodeTextField.text, let str3 = mobileCodeView.thirdCodeTextField.text, let str4 = mobileCodeView.fourthCodeTextField.text, let str5 = mobileCodeView.fifthCodeTextField.text, let str6 = mobileCodeView.sixthCodeTextField.text
        {
            let otp = "\(str1)\(str2)\(str3)\(str4)\(str5)\(str6)"
            guard let verificationID = UserDefaults.standard.string(forKey: Constant.authKeyidentifier) else { return }
            
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID, verificationCode: otp)
            
            let prevUser = Auth.auth().currentUser
            prevUser?.link(with: credential) { (user, error) in
                if let error = error {
                    let controller = UIAlertController(title: Constant.alertText, message: error.localizedDescription, preferredStyle: .alert)
                    controller.addAction(UIAlertAction(title: Constant.okayActionText, style: .default, handler: { (okAction) in
                        controller.dismiss(animated: true, completion: nil)
                    }))
                    //self.deleteUser()
                    progressHUD.hide()
                    self.present(controller, animated: true, completion: nil)
                    UserDefaults.standard.removeObject(forKey: Constant.authKeyidentifier)
                    return
                }
            }
            print("Success")
            progressHUD.hide()
            self.performSegue(withIdentifier: IDENTIFIER.VerificationToPic, sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == IDENTIFIER.VerificationToPic
        {
            let controller = segue.destination as! CameraViewController
            controller.userData = userData
        }
    }
    
    ///When user will tap on Resend code button then this mithod will be called
    @IBAction func ResendCodeButtonclicked(_ sender: Any) {
        
        let progressHUD = ProgressHUD(text: "Sending...")
        self.view.addSubview(progressHUD)
        self.activateTimer()
        let params = ["mobileNo":userData.mobileNumber as Any]
        
        AFWrapper.requestPOSTURL(serviceName: APINAMES.RESENDCODE,
                                 params: params,
                                 success: { (response) in
                                    progressHUD.hide()
                                    let errFlag = response["errorFlag"].boolValue
                                    switch errFlag {
                                    case false:
                                        print("%@","Success")
                                        break
                                        
                                    case true:
                                        self.view.showErrorMessage(titleMsg: Message.ERROR, andMessage:response[LoginResponse.errorMessage].stringValue)
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    self.view.showErrorMessage(titleMsg: Message.ERROR, andMessage: Error.localizedDescription)
                                    progressHUD.hide()
        })
    }
}

extension MobileViewController : verificationCodeDelegate{
    
    func verificationCodeWritingCompleted() {
        self.DoneButtonClicked(UIButton())
    }
}
