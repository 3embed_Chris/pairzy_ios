//
//  LoginViewController.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 02/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Foundation
import Firebase

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginViewOutlet: LoginView!
    
    struct Constant{
        static let loggingInText = "Logging in..."
        static let constantPushToken = "iOS Simulator Push Token"
        static let deviceModelKey = "deviceModel"
        static let deviceMakeKey = "deviceMake"
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String
        {
            self.loginViewOutlet.countryCodeLabel.text = VNHCountryPicker.dialCode(code: countryCode).dialCode
            self.loginViewOutlet.countryFlagImageView.image = VNHCountryPicker.dialCode(code: countryCode).flag
        }
        let navleftItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back_btn_off"), style:.plain, target: self, action: #selector(backcliked))
        self.navigationItem.setLeftBarButton(navleftItem, animated: true)
    }
    
    func backcliked() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    
    ///  when user touches view textField editing is ended.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func forgotBtncliked(_ sender: Any)
    {
        //segue to reset password
        self.performSegue(withIdentifier: IDENTIFIER.GotoResetPasword, sender: nil)
    }
    
    //MARK:- All Button Action
    
    ///When user will tap on next button then this mithod will be called
    @IBAction func loginButtonClicked(_ sender: Any) {
        
        if self.loginViewOutlet.checkMendatoryFields()
        {
            self.sendLogInRequest()
        }
    }
    
    /// Signup web service
    func sendLogInRequest() {
        
        //Login With Firebase
        
        
        
        let progressHUD = ProgressHUD(text: Constant.loggingInText)
        self.view.addSubview(progressHUD)
        
        if let userMobileNumber = loginViewOutlet.phoneNumberTextFieldOutlet.text,
            let password = loginViewOutlet.passwordTextFieldOutlet.text,
            let countryCode = loginViewOutlet.countryCodeLabel.text
        {
            var pushToken = Constant.constantPushToken
            if let token = UserDefaults.standard.value(forKey: SyUserdefaultKeys.kNSUPushNotificationTokenKey) as? String
            {
                pushToken = token
            }
            
            let mobileNo = ("\(countryCode)\(userMobileNumber)")
            let params =    [
                ServiceInfo.MobileNumber:mobileNo as AnyObject,
                ServiceInfo.Password:password as AnyObject,
                ServiceInfo.PushToken:pushToken as AnyObject,
                ServiceInfo.DeviceType:"1" as AnyObject,
                Constant.deviceMakeKey:UIDevice.current.systemName as AnyObject,
                Constant.deviceModelKey:UIDevice.current.model as AnyObject
                
                ] as [String: AnyObject]
            
            AFWrapper.requestPOSTURL(serviceName: APINAMES.LOGIN,
                                     params: params,
                                     success: { (response) in
                                        progressHUD.hide()
                                        if (response.null != nil){
                                            return
                                        }
                                        let errFlag = response[LoginResponse.errorFlag].boolValue
                                        switch errFlag {
                                        case false:
                                            
                                            if let dict = response[LoginResponse.data].dictionaryObject{
                                                if let token = dict[ResponseKey.token] as? String, let userId = dict[ResponseKey.userID] as? String{
                                                    self.addToken(token:token, userId: userId)
                                                    self.performSegue(withIdentifier: IDENTIFIER.MobileVC, sender: self)
                                                }
                                                else{
                                                    self.view.showErrorMessage(titleMsg: Message.ERROR, andMessage:Message.tokenMissingError)
                                                }
                                            }
                                            
                                        case true:
                                            self.view.showErrorMessage(titleMsg: Message.ERROR, andMessage:response[LoginResponse.errorMessage].stringValue)
                                        }
            },
                                     failure: { (Error) in
                                        self.view.showErrorMessage(titleMsg: Message.ERROR, andMessage: Error.localizedDescription)
                                        progressHUD.hide()
            })
        }
        
    }
    
    ///When user will tap on country code button then this mithod will be called
    @IBAction func countryCodeButtonClicked(_ sender: Any) {
        self.performSegue(withIdentifier: IDENTIFIER.CountryPicker, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == IDENTIFIER.CountryPicker
        {
            let controller : VNHCountryPicker = segue.destination as! VNHCountryPicker
            controller.delegate = self
        }
        else if segue.identifier == IDENTIFIER.GotoResetPasword{
            
            let viewcontroller = segue.destination as! ResetpasswordViewController
            viewcontroller.mobileNO =  loginViewOutlet.phoneNumberTextFieldOutlet.text!
        }
    }
}

//MARK:- Country Code Picker Delegate

extension LoginViewController : VNHCountryPickerDelegate{
    
    func didPickedCountry(country: VNHCounty) {
        self.loginViewOutlet.countryCodeLabel.text = country.dialCode
        self.loginViewOutlet.countryFlagImageView.image = country.flag
    }
}
