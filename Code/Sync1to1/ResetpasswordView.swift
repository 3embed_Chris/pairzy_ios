//
//  ResetpasswordView.swift
//  Sync1to1
//
//  Created by Rahul Sharma on 5/15/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class ResetpasswordView: UIView {
    
    

    @IBOutlet weak var firstNameTextfiled: UITextField!
    
    @IBOutlet weak var firstNameLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextfiled: UITextField!

    @IBOutlet weak var countryButtonOutlet: UIButton!

    @IBOutlet weak var counrtyFlagimageicon: UIImageView!

    @IBOutlet weak var phoneNUmberTextfiledoutlet: UITextField!
    @IBOutlet weak var countrycodeLabel: UILabel!
    
    
    
    
    
    func checkMendatoryFields() -> Bool{
        
                if self.validateEmail(withString: emailTextfiled.text!) == false {
                    do {
                        //Validate email format
                        self.showErrorMessage(titleMsg: "Error", andMessage: "Please enter valid Email.")
                        emailTextfiled.text = ""
                        emailTextfiled.becomeFirstResponder()
                    }
                    return false
        
                }
                else {
                    return true
                }
        
        return true
    }

    
    
    func validateEmail(withString email: String) -> Bool {
        let emailString: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailString)
        return emailTest.evaluate(with: email)
    }

    
}



// MARK - TextField Delegate Method

extension ResetpasswordView: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        switch textField {
        case emailTextfiled:
            
            var  frame : CGRect = (self.superview?.frame)!
            frame.origin.y -= 55
            
            UIView.animate(withDuration: 0.4, animations: {
               // self.superview?.frame = frame
            })
            
            
            break
            
        
            
        default:break
            
            
            
        }
        
        
        return true
        
    }
    
    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch  textField {
            
        case firstNameTextfiled:
            if (firstNameTextfiled.text?.characters.count)! > 0{
                self.firstNameLabel.isHidden = false
            }
            else{
                self.firstNameLabel.isHidden = true
            }
            firstNameTextfiled.resignFirstResponder()
            phoneNUmberTextfiledoutlet.becomeFirstResponder()
            break
            
        case phoneNUmberTextfiledoutlet:
            phoneNUmberTextfiledoutlet.resignFirstResponder()
            emailTextfiled.becomeFirstResponder()
            break
            
        case emailTextfiled:
            if (emailTextfiled.text?.characters.count)! > 0 {
                self.emailLabel.isHidden = false
            }else{
                self.emailLabel.isHidden = true
            }
            emailTextfiled.resignFirstResponder()
            
        default: break
            
        }
        return true
        
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        switch  textField {
            
        case firstNameTextfiled:
            if (newString.characters.count) > 0{
                self.firstNameLabel.isHidden = false;
            }
            else{
                self.firstNameLabel.isHidden = true
            }
            break
            
            
        case emailTextfiled:
            if (newString.characters.count) > 0  {
                self.emailLabel.isHidden = false
            }else{
                self.emailLabel.isHidden = true
            }
            
        default: break
            
        }
        return true
        
    }
    
}

