//
//  GradientView.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 20/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class GradientView: UIView {
    override open class var layerClass: AnyClass {
        get{
            return CAGradientLayer.classForCoder()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = self.layer as! CAGradientLayer
        let lightBlackColor = UIColor.black.withAlphaComponent(0.2)
        gradientLayer.colors = [lightBlackColor.cgColor, UIColor.clear.cgColor]
    }
}
